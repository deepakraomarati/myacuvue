({
    calDestroy:function(component, event, helper){
        $("#calendar").fullCalendar('destroy');
        helper.fetchEvents(component, event, helper);
    },
    openModal : function(component,event,ui,helper) {
        component.set("v.event" ,{'sobjectType': 'Event'});
        component.set("v.event.StartDateTime", null);
        component.set("v.event.EndDateTime", null);
        component.set("v.event.IsRecurrence",false);
        component.find("everyMonth2").set("v.checked" , false);
        component.find("everyYear2").set("v.checked" , false);
        component.find("everySomeDay").set("v.checked",false);
        component.find("everyWeekDay").set("v.checked",true);
        document.getElementById("mon").checked  = false;
        document.getElementById("tue").checked  = false;
        document.getElementById("wed").checked  = false;
        document.getElementById("thu").checked  = false;
        document.getElementById("fri").checked  = false;
        document.getElementById("sat").checked  = false;
        document.getElementById("sun").checked  = false;
        document.getElementById("divDaily").style.display   = "block";
        document.getElementById("divWeekly").style.display  = "none";
        document.getElementById("divYearly").style.display  = "none";
        document.getElementById("divMonthly").style.display = "none";
        component.find("IsRecurrence").set("v.value" , false);
        document.getElementById("divhide").style.display = "none";
        let userId = $A.get("$SObjectType.CurrentUser.Id");
        let userName = $A.get("$SObjectType.CurrentUser.Name");
        component.set("v.selSubject","");
        if(document.getElementById("combobox-unique-id"))
        	document.getElementById("combobox-unique-id").value ="";
        component.set("v.event.OwnerId",userId);
        component.set("v.startDateTimeVal", event.startDateTime);
        component.set("v.endDateTimeVal", event.endDateTime);
        component.set("v.value","Daily");
        component.set("v.selectedDOWValue" , 127);
        component.set("v.selectedDOYValue" , 127);
        component.set("v.event.RecurrenceStartDateTime" , event.startDateTime);
        component.set("v.event.RecurrenceEndDateOnly" , moment.utc(event.endDateTime).format("YYYY-MM-DD"));
        let value = [{
            type: 'Event',
            id: ui.helper[0].dataset.sfid,
            label: "navigate",
        }];
        let userValue = [{
            type: 'User',
            id: userId,
            label: userName,
        }];
        component.find("whatId").get("v.body")[0].set("v.values", value);
        component.find("ownerId").get("v.body")[0].set("v.values", userValue);
    },
    closeModal : function(component, event,helper) {
        helper.calDestroy(component, event, helper);
        let modal = component.find('modal');
        $A.util.removeClass(modal, 'slds-fade-in-open');
        let backdrop = component.find('backdrop');
        $A.util.removeClass(backdrop, 'slds-backdrop--open');
    },
    renderCal : function(component, event, helper,data) {
        $(document).ready(function(){
            $('#calendar').fullCalendar({
                header: {
                    center: 'today prev,next',
                    right: ''
                },
                locale: component.get("v.locale"),
                allDayText: 'All Day',
                defaultView: 'agendaWeek',//Added to make Week default
                defaultTimedEventDuration: '01:00:00',
                ignoretimezone: true,
                minTime: "08:00:00",
                maxTime: "18:00:00",
                defaultDate: moment().format("YYYY-MM-DD"),
                navLinks: true, // can click day/week names to navigate views
                listDayFormat : true,
                editable: true,
                droppable: true,
                eventLimit: true, // allow "more" link when too many events
                weekends: component.get("v.weekEnd"),
                eventBackgroundColor: 'rgb(109, 146, 179)',
                eventTextColor: 'white',
                events: data,
                eventRender: function(event, element) {
                    if(typeof event.rec !== "undefined")
                        element.find(".fc-content").prepend("<img src='/img/recurring_activity.gif' style='filter: contrast(100%) brightness(50%);width: 0.8rem;' alt='()'/>");
                },
                eventClick: function(calEvent, jsEvent, view) {
                    component.set('v.idVal', calEvent.id);
                    window.open('/lightning/r/Event/'+calEvent.id+'/view');
                },
                eventDataTransform: function(event) {
                    let evt;
                    // Salesforce Event
                    if (event.Id) {
                        evt = this.sObjectToEvent(event);
                    }
                    // Regular Event
                    else {
                        evt = event;
                    }
                    return evt;
                },
                eventDrop: function(event, delta, revertFunc) {
                    let stDate = moment.utc(event.start._d).add(-(component.get('v.offsetVal')), 'hours');
                    let endDate = moment.utc(event.end._d).add(-(component.get('v.offsetVal')), 'hours');
                    let evObj = {
                        "Id" : event.id,
                        "StartDateTime" : stDate.format(),
                        "EndDateTime" : endDate.format()
                    };
                    helper.upsertEvent(component, event ,helper, evObj);
                },
                drop: function(event,jsEvent,ui,resourceId) {
                    let stDate = moment.utc(event._d).add(-(component.get('v.offsetVal')), 'hours');
                    let endDate = moment.utc(event._d).add(-(component.get('v.offsetVal')), 'hours');
                    let evObj = {
                        "Id" : event.id,
                        "title" : event.title,
                        "startDateTime" : stDate.format(),
                        "endDateTime" : endDate.add(1, 'hours').format(),
                        "description" : event.description
                    };
                    component.set("v.NewEvt", true);
                    helper.openModal(component,evObj,ui,helper);
                }
            });
        });
    },
    initComponent : function(component, event, helper) {
        let action = component.get('c.initComponent');
        action.setParams({
            objectType: component.get('v.object'),
            fields: component.get('v.fields').split(',')
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if(state == 'SUCCESS') {
                let fieldsList = JSON.parse(response.getReturnValue());
                component.set('v.fieldsList', fieldsList);
            }
        });
        $A.enqueueAction(action);
    },
    fetchRecords : function(component, event,helper, defaultListView) {
        component.set("v.IsSpinner",true);
        let fieldsList = component.get('v.fieldsList');
        let fields = [];
        for(let iIndex = 0; iIndex < fieldsList.length; iIndex++) {
            fields.push(fieldsList[iIndex].name.trim());
        }
        let selected;
        if(defaultListView == null){
            selected = component.find("selectedViewId").get("v.value");
        }else{
            selected = defaultListView;
        }
        let action = component.get('c.fetchRecords');
        action.setParams({
            objectType: component.get('v.object'),
            fields: fields,
            keyword: component.get('v.keyword'),
            sortField : component.get('v.sortField'),
            sortDir : component.get('v.sortDir'),
            iLimit: component.get('v.limit'),
            filterId: selected
        });
        let spinner = component.find('Spinner');
        $A.util.toggleClass(spinner, 'slds-hide');
        action.setCallback(this, function(response) {
            $A.util.toggleClass(spinner, 'slds-hide');
            let state = response.getState();
            if(state == 'SUCCESS') {
                let unsortedAccountValue = response.getReturnValue().accList;
                let records = response.getReturnValue().accList;
                component.set('v.unsortedAccountValue', unsortedAccountValue);
                let pageSize = component.get('v.pageSize');
                let pageNumber = component.get('v.pageNumber');
                let pages = this.chunkify(records, pageSize);
                component.set('v.offsetVal', response.getReturnValue().offset);
                component.set('v.pages', pages);
                component.set('v.records', pages[pageNumber]);
                setTimeout(function() {
                    $('.dummy').each(function() {
                        // store data so the calendar knows to render an event upon drop
                        $(this).data('event', {
                            title: $.trim($(this).text()), // use the element's text as the event title
                            stick: true // maintain when user navigates (see docs on the renderEvent method)
                        });
                        // make the event draggable using jQuery UI
                        $(this).draggable({
                            containment: 'document',
                            // return a custom styled elemnt being dragged
                            helper: function (event) {
                                return $('<div class="uv-planning-dragging" data-sfid="'+ $(this)[0].dataset.sfid +'"></div>').html($(this).text());
                            },
                            opacity: 0.70,
                            zIndex: 10000,
                            appendTo: 'body',
                            cursor: 'move',
                            revertDuration: 0,
                            revert: true
                        });
                    });
                    component.set("v.IsSpinner",false);
                }, 3000);
            }
        })
        $A.enqueueAction(action);
    },
    chunkify : function(data, size){
        let sets = [], chunks, i = 0;
        chunks = data.length / size;
        while(i < chunks){
            sets[i] = data.splice(0, size);
            i++;
        }
        return sets;
    },
    //----LISTVIEW---//
    getAccountHelper : function(component,event,defaultListView) {
        let selected;
        if(defaultListView == null){
            selected = component.find("selectedViewId").get("v.value");
        }else{
            selected = defaultListView;
        }
        let action = component.get("c.getFilteredAccounts");
        action.setParams({filterId : selected});
        action.setCallback(this, function(response){
            let state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.records",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    tranformToFullCalendarFormat : function(component,event,helper) {
        let eventArr = [];
        let whatName = '';
        let eventsRcvd = event;
        let allDay = false;
        for(let i = 0;i < eventsRcvd.length;i++){
            allDay = false;
            if(eventsRcvd[i].Offset === 0)
                allDay = true;
            if(typeof eventsRcvd[i].WhatId !== "undefined")
                whatName = eventsRcvd[i].WhatName + ' : ';
            eventArr.push({
                'id':eventsRcvd[i].Id,
                'start':moment(moment(eventsRcvd[i].StartDateTime)._d).add(eventsRcvd[i].Offset,'hours').toISOString(),
                'end':  moment(moment(eventsRcvd[i].EndDateTime)._d).add(eventsRcvd[i].Offset, 'hours').toISOString(),
                'title':whatName + eventsRcvd[i].Subject,
                'What':eventsRcvd[i].WhatId,
                'rec':eventsRcvd[i].RecurrenceActivityId,
                'allDay': allDay
            });
        }
        helper.renderCal(component,event,helper,eventArr);
        component.set("v.events",eventArr);
    },
    fetchEvents : function(component,event,helper) {
        let action = component.get("c.getEventsPallete");
        action.setCallback(this, function(response) {
            let state = response.getState();
            if(state === "SUCCESS"){
                helper.tranformToFullCalendarFormat(component,response.getReturnValue(),helper);
            }
        });
        $A.enqueueAction(action);
    },
    helperOnchange:function(component,event,helper){
        let checkBox = document.getElementById("weekend");
        if (checkBox.checked){
            component.set("v.weekEnd" ,true);
            helper.calDestroy(component,event,helper);
        }
        else{
            component.set("v.weekEnd" ,false);
            helper.calDestroy(component,event,helper);
        }
    },
    helperSaveEventRecords:function(component,event,helper){
        let eventInfo = {};
        if($A.util.isEmpty(component.find("ownerId").get("v.value")) ||
           $A.util.isEmpty(component.find("startTime").get("v.value")) ||
           $A.util.isEmpty(component.find("endTime").get("v.value"))){
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error Message',
                message:'Please Enter the required fields',
                messageTemplate: 'Mode is pester ,duration is 1sec and Message is overrriden',
                duration:' 1000',
                key: 'info_alt',
                type: 'error',
                mode: 'pester'
            });
            toastEvent.fire();
            return false;
        }
        else if(component.find("IsRecurrence").get("v.value") && (
            component.get("v.event.RecurrenceStartDateTime") == null ||
            component.get("v.event.RecurrenceEndDateOnly") == null )){
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error Message',
                message:'Please Enter the Recurrence Values',
                messageTemplate: 'Mode is pester ,duration is 1sec and Message is overrriden',
                duration:' 1000',
                key: 'info_alt',
                type: 'error',
                mode: 'pester'
            });
            toastEvent.fire();
            return false;
        }
            else{
                eventInfo = component.get("v.event");
                delete eventInfo.What;
                delete eventInfo.Owner;
                delete eventInfo.Who;
                let rec= component.get("v.recTypeID");
                let RECURRENCEDAYOFWEEKMASK = 0;
                eventInfo.RecordTypeId = rec;
                eventInfo.StartDateTime  = component.find("startTime").get("v.value");
                eventInfo.EndDateTime  = component.find("endTime").get("v.value");
                eventInfo.IsRecurrence = component.find("IsRecurrence").get("v.value");
                if(component.find("IsRecurrence").get("v.value")){
                    if(eventInfo.RecurrenceType === "RecursWeekly"){
                        if($("#sun:checked").length ==1){
                            RECURRENCEDAYOFWEEKMASK = 1;
                        }
                        if($("#mon:checked").length == 1){
                            RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK+2;
                        }
                        if($("#tue:checked").length == 1){
                            RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK+4;
                        }
                        if($("#wed:checked").length == 1){
                            RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK+8;
                        }
                        if($("#thu:checked").length == 1){
                            RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK+16;
                        }
                        if($("#fri:checked").length == 1){
                            RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK+32;
                        }
                        if($("#sat:checked").length == 1){
                            RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK+64;
                        }
                        eventInfo.RECURRENCEDAYOFWEEKMASK = RECURRENCEDAYOFWEEKMASK;
                        eventInfo.RecurrenceDayOfMonth = null;
                        eventInfo.RecurrenceInstance = null;
                        eventInfo.RecurrenceMonthOfYear = null;
                    }
                    else if(eventInfo.RecurrenceType === "RecursMonthly"){
                        eventInfo.RecurrenceInstance = null;
                        component.set("v.event.RecurrenceType","RecursMonthly");
                    }
                        else if(eventInfo.RecurrenceType === "RecursMonthlyNth"){
                            component.set("v.event.RecurrenceType","RecursMonthlyNth");
                            eventInfo.RecurrenceDayOfMonth = null;
                            component.set("v.event.RecurrenceDayOfWeekMask",component.get("v.selectedDOWValue"));
                        }
                            else if(eventInfo.RecurrenceType === "RecursYearly"){
                                eventInfo.RecurrenceInstance = null;
                                component.set("v.event.RecurrenceType","RecursYearly");
                            }
                                else if(eventInfo.RecurrenceType === "RecursYearlyNth"){
                                    eventInfo.RecurrenceDayOfMonth = null;
                                    component.set("v.event.RecurrenceType","RecursYearlyNth");
                                }
                                    else{
                                        if(component.find("everyWeekDay").get("v.checked")){
                                            eventInfo.RecurrenceDayOfMonth = null;
                                            eventInfo.RecurrenceMonthOfYear = null;
                                            eventInfo.RecurrenceInstance = null;
                                            component.set("v.event.RecurrenceType","RecursEveryWeekday");
                                            component.set("v.event.RecurrenceDayOfWeekMask",62);
                                        }
                                        else{
                                            eventInfo.RecurrenceDayOfMonth = null;
                                            eventInfo.RecurrenceInstance = null;
                                            eventInfo.RecurrenceMonthOfYear = null;
                                            component.set("v.event.RecurrenceType","RecursDaily");
                                        }
                                    }
                }
                let action = component.get("c.saveEvent");
                if(component.get("v.selSubject") == '' || component.get("v.selSubject") == null){
                    eventInfo.Subject = document.getElementById("combobox-unique-id").value;
                }else{
                    eventInfo.Subject=component.get("v.selSubject")["text"];
                }
                if(eventInfo.Subject == ''){
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : 'Error Message',
                        message:'Please Enter the required fields',
                        messageTemplate: 'Mode is pester ,duration is 1sec and Message is overrriden',
                        duration:' 1000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                    return false;
                }
                if(component.find("whoId").get("v.value") != undefined  && component.find("whoId").get("v.value") != "[]"){
                    eventInfo.WhoId = JSON.parse(component.find("whoId").get("v.value"))[0].id;
                }
                if(component.find("whoId").get("v.value") == "[]"){
                    eventInfo.WhoId = '';
                }
                // Save with value if field not available in layout
                for(let i in component.get("v.fieldAPIList")){
                    let eachFieldApi = component.get("v.eventRTPickList."+component.get("v.fieldAPIList")[i]);
                    for (let j=0; j<eachFieldApi.length; j++){
                        if(component.get("v.event."+component.get("v.fieldAPIList")[i]) === eachFieldApi[j].label){
                            component.set("v.event."+component.get("v.fieldAPIList")[i],eachFieldApi[j].value);
                        }    
                    }
                }
                action.setParams({ "eventInfo" : JSON.stringify(eventInfo)});
                action.setCallback(this, function(response) {
                    let state = response.getState();
                    if (state === "SUCCESS"){
                        if(response.getReturnValue().status === "SUCCESS"){
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Success!",
                                "type":"Success",
                                "message": "Event added successfully."
                            });
                            toastEvent.fire();
                            let modal = component.find('modal');
                            $A.util.removeClass(modal, 'slds-fade-in-open');
                            let backdrop = component.find('backdrop');
                            $A.util.removeClass(backdrop, 'slds-backdrop--open');
                            helper.calDestroy(component, event, helper);
                        }
                        else{
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Error!",
                                "type":"error",
                                "message": response.getReturnValue().msg
                            });
                            toastEvent.fire();
                        }
                    }
                    else if(state === "ERROR"){
                        let errors = action.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                let toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    title : 'Error Message',
                                    message:errors[0].message,
                                    messageTemplate: 'Mode is pester ,duration is 5sec and Message is overrriden',
                                    duration:' 5000',
                                    key: 'info_alt',
                                    type: 'error',
                                    mode: 'pester'
                                });
                                toastEvent.fire();
                            }
                        }
                    }
                });
                $A.enqueueAction(action);
            }
    },
    setDraggableUI : function(component, event, helper){
        setTimeout(function() {
            $('.dummy').each(function() {
                // store data so the calendar knows to render an event upon drop
                $(this).data('event', {
                    title: $.trim($(this).text()), // use the element's text as the event title
                    stick: true // maintain when user navigates (see docs on the renderEvent method)
                });
                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });
            });
        }, 3000);
    },
    upsertEvent : function(component, event, helper, evObj) {
        let action = component.get("c.upsertEvents");
        action.setParams({"sEventObj": evObj});
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS"){
                if(response.getReturnValue() === "success"){
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type":"Success",
                        "message": " Event shifted successfully. "
                    });
                    toastEvent.fire();
                }
                else{
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type":"error",
                        "message": response.getReturnValue()
                    });
                    toastEvent.fire();
                    helper.calDestroy(component, event, helper);
                }
            }
            else{
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type":"error",
                    "message": response.getReturnValue()
                });
                toastEvent.fire();
                helper.calDestroy(component, event, helper);
            }
        });
        $A.enqueueAction(action);
    },
    helperOncheck:function(component,event,helper){
        if(component.find("IsRecurrence").get("v.value") === true){
            document.getElementById("divhide").style.display = "block";
            component.set("v.event.IsRecurrence",component.find("IsRecurrence").get("v.value"));
            component.set("v.event.RecurrenceType", "RecursEveryWeekday");
            if(component.find("everyWeekDay").get("v.checked"))
                component.set("v.event.RecurrenceDayOfWeekMask",62);
        }
        else{
            document.getElementById("divhide").style.display = "none";
            component.set("v.event.IsRecurrence",component.find("IsRecurrence").get("v.value"));
        }
    },
    helperOnFreqchange:function(component,event,helper){
        if(component.get("v.value") === "Weekly"){
            document.getElementById("divWeekly").style.display = "block";
            document.getElementById("divMonthly").style.display = "none";
            document.getElementById("divYearly").style.display = "none";
            document.getElementById("divDaily").style.display = "none";
            component.set("v.event.RecurrenceType","RecursWeekly");
            document.getElementById("mon").checked  = false;
            document.getElementById("tue").checked  = false;
            document.getElementById("wed").checked  = false;
            document.getElementById("thu").checked  = false;
            document.getElementById("fri").checked  = false;
            document.getElementById("sat").checked  = false;
            document.getElementById("sun").checked  = false;
        }
        else if(component.get("v.value") === "Monthly"){
            document.getElementById("divWeekly").style.display = "none";
            document.getElementById("divMonthly").style.display = "block";
            document.getElementById("divYearly").style.display = "none";
            document.getElementById("divDaily").style.display = "none";
            component.find("everyMonth1").set("v.checked" , true);
            component.find("everyMonth2").set("v.checked" , false);
            if(component.find("everyMonth1").get("v.checked"))
                component.set("v.event.RecurrenceType","RecursMonthly");
            else
                component.set("v.event.RecurrenceType","RecursMonthlyNth");
            component.set("v.event.RecurrenceDayOfWeekMask",null);
        }
            else if(component.get("v.value") === "Yearly"){
                document.getElementById("divWeekly").style.display = "none";
                document.getElementById("divMonthly").style.display = "none";
                document.getElementById("divYearly").style.display = "block";
                document.getElementById("divDaily").style.display = "none";
                component.find("everyYear1").set("v.checked",true);
                component.find("everyYear2").set("v.checked" , false);
                if(component.find("everyYear1").get("v.checked"))
                    component.set("v.event.RecurrenceType","RecursYearly");
                else
                    component.set("v.event.RecurrenceType","RecursYearlyNth");
                component.set("v.event.RecurrenceDayOfWeekMask",null);
            }
                else{
                    document.getElementById("divWeekly").style.display = "none";
                    document.getElementById("divMonthly").style.display = "none";
                    document.getElementById("divYearly").style.display = "none";
                    document.getElementById("divDaily").style.display = "block";
                    component.find("everySomeDay").set("v.checked",false);
                    component.find("everyWeekDay").set("v.checked",true);
                    if(component.find("everyWeekDay").get("v.checked")){
                        component.set("v.event.RecurrenceType","RecursEveryWeekday");
                        component.set("v.event.RecurrenceDayOfWeekMask",62);
                    }
                    else{
                        component.set("v.event.RecurrenceType","RecursDaily");
                    }
                }
    },
    helperonDailyChange:function(component,event,helper){
        if(event.getSource().getLocalId() === "everyWeekDay"){
            component.find("everySomeDay").set("v.checked" , false);
            component.set("v.event.RecurrenceType","RecursEveryWeekday");
            component.set("v.event.RecurrenceDayOfWeekMask",62);
        }
        else if(event.getSource().getLocalId() === "everySomeDay"){
            component.find("everyWeekDay").set("v.checked" , false);
            component.set("v.event.RecurrenceType","RecursDaily");
            component.set("v.event.RecurrenceDayOfWeekMask",null);
        }
    },
    helperonMonthChange:function(component,event,helper){
        if(event.getSource().getLocalId() === "everyMonth1"){
            component.find("everyMonth2").set("v.checked" , false);
            component.set("v.event.RecurrenceType","RecursMonthly");
        }
        else if(event.getSource().getLocalId() === "everyMonth2"){
            component.find("everyMonth1").set("v.checked" , false);
            component.set("v.event.RecurrenceType","RecursMonthlyNth");
            component.set("v.event.RecurrenceDayOfWeekMask",component.get("v.selectedDOWValue"));
        }
    },
    helperonYearChange:function(component,event,helper){
        if(event.getSource().getLocalId() === "everyYear1"){
            component.find("everyYear2").set("v.checked" , false);
            component.set("v.event.RecurrenceType","RecursYearly");
        }
        else if(event.getSource().getLocalId() === "everyYear2"){
            component.find("everyYear1").set("v.checked" , false);
            component.set("v.event.RecurrenceType","RecursYearlyNth");
            component.set("v.event.RecurrenceDayOfWeekMask",component.get("v.selectedDOYValue"));
        }
    },
    helperonDOWChange:function(component,event,helper){
        component.set("v.selectedDOWValue" , component.find('RecurrenceDayOfWeekMaskID').get('v.value'));
    },
    helperonDOYChange:function(component,event,helper){
        component.set("v.selectedDOYValue" , component.find('RecurrenceDayOfYearMaskID').get('v.value'));
    },
    fetchSearchedRecords : function(component,event,helper){
        component.set("v.IsSpinner",true);
        let data = component.get("v.unsortedAccountValue");
        let searchKey = component.get('v.keyword');
        let searchedData =[];
        for(let i=0; i<data.length; i++)
        {
            let accObj = data[i];
            if((accObj['Name'].toLowerCase()).includes(searchKey.toLowerCase())  ||
               (typeof accObj['Phone'] !== "undefined" && accObj['Phone'].includes(searchKey)) || 
               (typeof accObj['OutletNumber__c'] !== "undefined" && accObj['OutletNumber__c'].includes(searchKey))){
                searchedData.push(accObj);
            }
        }
        let pageSize = component.get('v.pageSize');
        let pageNumber = component.get('v.pageNumber');
        let pages;
        if(searchKey != ''){
            pages = this.chunkify(searchedData, pageSize);
        }
        else{
            pages = this.chunkify(data, pageSize);
        }
        component.set('v.pages', pages);
        component.set('v.records', pages[pageNumber]);
        setTimeout(function() {
            $('.dummy').each(function() {
                // store data so the calendar knows to render an event upon drop
                $(this).data('event', {
                    title: $.trim($(this).text()), // use the element's text as the event title
                    stick: true // maintain when user navigates (see docs on the renderEvent method)
                });
                // make the event draggable using jQuery UI
                $(this).draggable({
                    containment: 'document',
                    // return a custom styled elemnt being dragged
                    helper: function (event) {
                        return $('<div class="uv-planning-dragging" data-sfid="'+ $(this)[0].dataset.sfid +'"></div>').html($(this).text());
                    },
                    opacity: 0.70,
                    zIndex: 10000,
                    appendTo: 'body',
                    cursor: 'move',
                    revertDuration: 0,
                    revert: true
                });
            });
            component.set("v.IsSpinner",false);
        }, 3000);
    }
})