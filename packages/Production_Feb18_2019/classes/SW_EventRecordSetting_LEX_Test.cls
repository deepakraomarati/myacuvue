@isTest(SEEALLDATA=false)
public class SW_EventRecordSetting_LEX_Test {
    public static testMethod void validateupdateEventRTSettings(){
        SW_EventRecordSetting_LEX.evtResultData evtData=new SW_EventRecordSetting_LEX.evtResultData();
        List<SW_EventRecordSetting_LEX.evtResultData> res =new  List<SW_EventRecordSetting_LEX.evtResultData>();
        evtData.label  = 'JJ_JPN_CustomerDevelopmentManagerCallJPN';
    	evtData.PK     = 'Subject';
        evtData.RT	   = 'JJ_JPN_CustomerDevelopmentManagerCallJPN';
        evtData.PKVal  = 'Study meeting,TEL';
        evtData.DefaultVal ='TEL';
        evtData.labelValuePair = 'Study meeting#Study meeting,TEL#TEL'; 
        evtData.language = 'en_US';
        res.add(evtData);
        test.startTest();
        SW_EventRecordSetting_LEX.updateEventRTSettings(res);
        test.stopTest();
        System.assertEquals(evtData.PK , 'Subject');
    }
}