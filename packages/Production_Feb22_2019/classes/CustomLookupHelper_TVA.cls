/****************************************************************************************
* Modified By     :   Ashok Kalluri
* Modified Date   :   31/08/2017
* Description     :   Custom Lookup Controller
* Modification Log:
*   -------------------------------------------------------------------------------------
*   * Developer             Date                   Description
*   *------------------------------------------------------------------------------------
*Ashok Kalluri          31/08/2017             Initial Version   
*Prabushanker K         26/10/2017             Onclick recent item beased on the recent item Object and Limit control with
`                                                  Custom Label     
*Ranjana B              26/10/2017             added logic to Restrict Account with group=0001 
except for IN10 records
*Neha Patel             04/04/2018			   Updated searchDB as part of US-3155
*****************************************************************************************/
public without sharing class CustomLookupHelper_TVA {
    //method for searching records in custom lookup
    @AuraEnabled 
    public static String searchDB(String objectName, String fldAPIText, String fldAPIVal, 
                                  Integer lim,String fldAPISearch,String searchText,List<String> lookupFilters,
                                  String recentItems, String recID,String newFilter){
                                      
                                      system.debug('search text value'+searchText);
                                      string outlet=searchText;
                                      String query='';
                                      List<ResultWrapper> lstRet = new List<ResultWrapper>();
									  String searchTextVar='\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';
                                      String lookupFilter = '';   
                                      String userId = UserInfo.getUserID();
                                      String fields=''; 
                                      String ObjName = objectName;  
                                      String Conditions='';
                                      String tempECPID = recID;
                                      String OrganizationCode='OrganizationCode_TVA__c';
                                      //String AccountGroup='AccountGroup_TVA__c'; 
                                      String cretedDate = 'CreatedDate';
                                      
                                      if(!lookupFilters.isEmpty()){
                                          for(String filterVal : lookupFilters){
                                              lookupFilter = 'AND '+filterVal;
                                          }
                                      }  
                                      
                                      if(recentItems=='true'){
                                          /******************** ADDK-3155 Start ************************/
                                          //Fetching limit value from custom metadata
                                          Integer days = 0;                                DateTime tDate = System.now().addDays(-days);
                                          /******************** ADDK-3155 End ************************/
                                          
                                          fields=fldAPIText +','+fldAPIVal+', LastViewedDate';
                                          ObjName='  RecentlyViewed' ;
                                          Conditions= 'Type='+'\''+objectName+'\' AND LastViewedDate >=: tDate ORDER BY LastViewedDate DESC';
                                          
                                          if(objectName=='Account'){
                                              searchTextVar='%' + String.escapeSingleQuotes(searchText.trim()) + '%';
                                              system.debug('Enter acccc');
                                              string country='JPN';
                                              query ='select Id, Name, CreatedDate,LastViewedDate from ' +objectName + 
                                                  ' where JJ_JPN_isBillTo__c=True and  ( Name LIKE \'' +String.escapeSingleQuotes(searchTextVar) + '\' OR OutletNumber__c LIKE \''+String.escapeSingleQuotes(searchTextVar)+
                                                 // ' where  ( Name LIKE ' +searchTextVar + ' OR OutletNumber__c LIKE '+searchTextVar+
                                                  //' OR Phone LIKE '+searchTextVar+') order by createdDate DESC limit 10';
                                                  '\') order by createdDate DESC limit 10';
                                          }
                                          if(objectName=='Account' && newFilter=='payer' ){
                                              searchTextVar='%' + String.escapeSingleQuotes(searchText.trim()) + '%';
                                              system.debug('Enter acccc123');
                                              string country='JPN';
                                              query ='select Id, Name, CreatedDate,LastViewedDate from ' +objectName + 
                                                  ' where JJ_JPN_isPayerAcc__c=True and  ( Name LIKE \'' +String.escapeSingleQuotes(searchTextVar) + '\' OR OutletNumber__c LIKE \''+String.escapeSingleQuotes(searchTextVar)+
                                                  //' OR Phone LIKE '+searchTextVar+') order by createdDate DESC limit 10';
                                                  '\') order by createdDate DESC limit 10';
                                          }
                                          else if(objectName=='CampaignMember'){
                                             
                                              Map<String,ExactTarget_Integration_Settings__c> MapcampId = ExactTarget_Integration_Settings__c.getAll();
                                              String rem_MYCampaign = MapcampId.get('MYCampaign').Value__c;
                                              String remECPID = tempECPID;
                                              String VoucherStatus = 'Not Used';
                                              List<Account> tempECPLoggedIn = [SELECT Id,Name,Phone,OutletNumber__c FROM Account WHERE Id=:remECPID LIMIT 1];
                                              String rem_OutletNumber = tempECPLoggedIn[0].OutletNumber__c;
                                              query = 'SELECT Id, Name,CreatedDate FROM CampaignMember WHERE CampaignID = \''+
                                                  String.escapeSingleQuotes(rem_MYCampaign) + '\' AND Outlet_Number__c = \'' + 
                                                  String.escapeSingleQuotes(rem_OutletNumber) + '\' and Voucher_Status__c = \''+
                                                  String.escapeSingleQuotes(VoucherStatus) + '\'';
                                              //+ '\' and Name LIKE ' + searchTextVar + 'limit 5';
                                          }
                                          else if(objectName == 'Event_RecordType_Setting__c'){
                                              query = 'SELECT Id, Field_Api__c,Record_Type__c,Values__c,Default_Value__c, Label_Value_pairs__c FROM Event_RecordType_Setting__c WHERE Field_Api__c = ' 
                                                  		+ '\'' + fldAPIVal + '\'' 
                                                  		+' AND Record_Type__c =' + '\'' + recID + '\''
                                                  		// AATB-4424 - Add language filter to show specific list of values
                                                  		+' AND SW_Language__c =' + '\'' + UserInfo.getLanguage() + '\'';
                                          }
                                      }
                                      //US ADDK-1990 start
                                      //add two more fields Organisation code and AccountGroup only for Object Account               
                                      else{  
                                          if(objectName=='Account'){
                                             system.debug('Enter acccc');
                                             string country='JPN';
                                              
                                             query ='select Id, Name, CreatedDate,LastViewedDate from ' +objectName + 
                                                  ' where JJ_JPN_isBillTo__c=True and  ( Name LIKE ' +searchTextVar + ' OR OutletNumber__c LIKE '+searchTextVar+
                                                  // ' where  ( Name LIKE ' +searchTextVar + ' OR OutletNumber__c LIKE '+searchTextVar+
                                                  //' OR Phone LIKE '+searchTextVar+') order by createdDate DESC limit 10';
                                                  ') order by createdDate DESC limit 10';
                                          }
                                          if(objectName=='Account' && newFilter=='payer' ){
                                              system.debug('Enter acccc123');
                                               string country='JPN';
                                               query ='select Id, Name, CreatedDate,LastViewedDate from ' +objectName + 
                                                  ' where JJ_JPN_isPayerAcc__c=True and  ( Name LIKE ' +searchTextVar + ' OR OutletNumber__c LIKE '+searchTextVar+
                                                 //' OR Phone LIKE '+searchTextVar+') order by createdDate DESC limit 10';
                                                  ') order by createdDate DESC limit 10';
                                          }
                                          else if(objectName=='CampaignMember'){
                                              Map<String,ExactTarget_Integration_Settings__c> MapcampId = ExactTarget_Integration_Settings__c.getAll();
                                              String rem_MYCampaign = MapcampId.get('MYCampaign').Value__c;
                                              String remECPID = tempECPID;
                                              String VoucherStatus = 'Not Used';
                                              List<Account> tempECPLoggedIn = [SELECT Id,Name,Phone,OutletNumber__c FROM Account 
                                                                               WHERE Id=:remECPID LIMIT 1];
                                              String rem_OutletNumber = tempECPLoggedIn[0].OutletNumber__c;
                                              query = 'SELECT Id, Name,CreatedDate FROM CampaignMember WHERE CampaignID = \''+ 
                                                  String.escapeSingleQuotes(rem_MYCampaign) +'\' AND Outlet_Number__c = \''+ 
                                                  String.escapeSingleQuotes(rem_OutletNumber) + '\' and Voucher_Status__c = \'' +
                                                  String.escapeSingleQuotes(VoucherStatus) + '\'';
                                              //+ '\' and Name LIKE ' + searchTextVar + 'limit 5';
                                              system.debug('query222 :: ' + query);
                                          }
                                          else if(objectName=='Event_RecordType_Setting__c'){
                                              query = 'SELECT Id, Field_Api__c,Record_Type__c,Values__c,Default_Value__c, Label_Value_pairs__c FROM Event_RecordType_Setting__c WHERE Field_Api__c = ' 
                                                  		+ '\'' + fldAPIVal + '\'' 
                                                  		+' AND Record_Type__c =' + '\'' + recID + '\'' 
                                                  		// AATB-4424 - Add language filter to show specific list of values
                                                  		+' AND SW_Language__c =' + '\'' + UserInfo.getLanguage() + '\''; 
                                          }
                                      }
                                      
                                      system.debug('seraching'+objectName);
                                      system.debug('query222 :: ' + query);
                                      
                                      List<sObject> sobjList = Database.query(query);
                                      //forming result wrappers
                                      for(SObject s : sobjList){
                                          ResultWrapper obj = new ResultWrapper();
                                          obj.objName = objectName;
                                          if(recentItems!='true' && objectName=='Account'){
                                              
                                              obj.text = String.valueOf(s.get(fldAPIText)) ;
                                              obj.val = String.valueOf(s.get(fldAPIVal))  ;
                                              obj.crDate = DateTime.valueOf(s.get('CreatedDate'));
                                              lstRet.add(obj);
                                              //US ADDK-1990 ends
                                          }
                                          else if(objectName=='Event_RecordType_Setting__c'){
                                              String values = String.valueOf(s.get(fldAPIText));
                                              List<String> lstValues = values.split(',');
                                              lstValues.add(String.valueOf(s.get('Default_Value__c')));
                                              system.debug(lstValues);
                                              for(integer i=0; i<lstValues.size(); i++){
                                                  ResultWrapper evtObj = new ResultWrapper();
                                                  evtObj.objName = objectName;
                                                  evtObj.val = String.valueOf(s.get('Field_Api__c'));
                                                  evtObj.text = lstValues[i];
                                                  if(lstValues[i].containsIgnoreCase(searchText)){
                                                      lstRet.add(evtObj);
                                                  }else if(searchText == ''){
                                                      lstRet.add(evtObj);
                                                  }
                                                  System.debug('lstRet'+lstRet);
                                              }
                                          }
                                          else if(objectName=='CampaignMember'){
                                              integer i=0;
                                              if(String.valueOf(s.get(fldAPIText)).containsIgnoreCase(searchText) && i < 6){
                                                  obj.text = String.valueOf(s.get(fldAPIText));
                                                  obj.val = String.valueOf(s.get(fldAPIVal));
                                                  obj.crDate = DateTime.valueOf(s.get('CreatedDate'));
                                                  lstRet.add(obj);
                                                  i++;
                                              }
                                          }
                                          else {
                                              obj.text = String.valueOf(s.get(fldAPIText));
                                              obj.val = String.valueOf(s.get(fldAPIVal));
                                              obj.crDate = DateTime.valueOf(s.get('CreatedDate'));
                                              lstRet.add(obj);
                                          }
                                      } 
                                      if(recentItems=='true'){
                                          lstRet.sort();
                                      }
                                      system.debug('lstRet end =====>'+lstRet.size());
                                      //Remove dupliactes from the wrapper list
                                      Set<Id> uniqueId = new Set<Id>();
                                      List<ResultWrapper> lstRet1 = new List<ResultWrapper>();
                                      if(objectName !='Event_RecordType_Setting__c'){
                                          for(ResultWrapper rsw : lstRet){
                                              if(!uniqueId.contains(rsw.val)){
                                                  uniqueId.add(rsw.val);
                                                  lstRet1.add(rsw);
                                              }
                                          }
                                      }
                                      else{
                                          lstRet1 = lstRet;
                                      }
                                      system.debug('lstRet1 end =====>'+lstRet1.size());
                                      system.debug('returnedvales'+JSON.serialize(lstRet1));
                                      return JSON.serialize(lstRet1);
                                  }
    //Wrapper class for holding result
    public class ResultWrapper implements Comparable{
        public String objName {get;set;}
        public String text{get;set;}
        public String val{get;set;}
        public datetime crDate{get;set;}
        public ResultWrapper(){
            //default constructor
        }
        
        //Method to sort the wrapper list
        public Integer compareTo(Object obj) {
            ResultWrapper rsw = (ResultWrapper)(obj);
            if (this.crDate < rsw.crDate) {
                return 1;
            }
            return -1;
        }  
    }
}