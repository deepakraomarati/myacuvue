<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>App used to tack all transactions related to Drupal Sites</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Drupal Sites</label>
    <tabs>standard-Account</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Campaign</tabs>
    <tabs>standard-Product2</tabs>
    <tabs>Etrial__c</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>MA2_SFMC_Level_1_Tracking__c</tabs>
    <tabs>MA2_SFMC_Level_2_Tracking__c</tabs>
</CustomApplication>
