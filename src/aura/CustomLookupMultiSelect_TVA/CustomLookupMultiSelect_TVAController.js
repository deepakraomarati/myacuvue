({
    doInit : function(component, event, helper) {
        helper.showComp(component,'searchScreen');
        helper.getUIThemeHelper(component, event, helper);
        helper.openRecentHelper(component, event, helper);
    },
    itemSelected : function(component, event, helper) {      
        helper.itemSelected(component, event, helper);
    },
    itemSelectedRecent : function(component, event, helper) {
        helper.itemSelectedRecent(component, event, helper);
    },
    serverCall :  function(component, event, helper) {
        helper.serverCall(component, event, helper);
    },
    clearSelection : function(component, event, helper){
        helper.clearSelectionHelper(component, event, helper);
    },
    clearSerchBox : function(component, event, helper){
        helper.clearSerchBoxHelper(component, event, helper);
    },
    openSearch : function(component, event, helper){
        helper.openSearchHelper(component, event, helper);
    },
    closeSearch : function(component, event, helper){
        helper.closeSearchHelper(component, event, helper);
    },
    saveContinue : function(component, event, helper){
        helper.nextHelper(component, event, helper);
    },
    removeSelected : function(component, event, helper){
        helper.removeSelected(component, event, helper);
    },
    removeSelectedRecent : function(component, event, helper){
        helper.removeSelectedRecent(component, event, helper);
    },
    searchRecords : function(component, event, helper){
        helper.searchRecordsHelper(component, event, helper, false);
        $A.util.addClass(component.find("searchHelper"), 'slds-hide');
        component.set("v.isRecent",false);
        component.set("v.isChoosen",false);
        component.set("v.isSearchResult",true);
    },
    openRecent : function(component, event, helper){
        helper.openRecentHelper(component, event, helper);
    },
    openSelected : function(component, event, helper){
        component.set("v.searchString","");
        $A.util.addClass(component.find("searchHelper"), 'slds-hide');
        component.set("v.isRecent",false);
        component.set("v.isChoosen",true);
        component.set("v.isSearchResult",false);
        let saveRecords=component.get("c.saveEvent");
        saveRecords.setParams({ eventRecId : component.get("v.recordId"),
                               attendeeIds:JSON.stringify(component.get("v.recentlyAddedRecords"))
                              });
        saveRecords.setCallback(this, function(response) {
            helper.handleResponse(response,component,helper,'');
        })
        $A.enqueueAction(saveRecords);
    },
    keySearch : function(cmp, event, helper) {
        if (event.getParams().which === 13 || event.getParams().keyCode === 13) {
            helper.searchRecordsHelper(cmp, event, helper, false);
            $A.util.addClass(cmp.find("searchHelper"), 'slds-hide');
            cmp.set("v.isRecent",false);
            cmp.set("v.isChoosen",false);
            cmp.set("v.isSearchResult",true);
        }else{
            helper.dokeySearch(cmp);
        }
    }
})