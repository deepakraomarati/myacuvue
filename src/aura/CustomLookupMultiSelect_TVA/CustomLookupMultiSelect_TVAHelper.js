({
    dokeySearch: function(cmp) {
        // Get the search string, input element and the selection container
        let searchString = cmp.get('v.searchString');
        if(searchString.length<25){
            cmp.set("v.searchStringButton",searchString);
        }else{
            let newsearchString=searchString.substr(0,10)+'...';
            cmp.set("v.searchStringButton",newsearchString);
        }
        if (typeof searchString === 'undefined' || !(searchString.length >0))  {
            $A.util.addClass(cmp.find("searchHelper"), 'slds-hide');
        }
        else{
            $A.util.removeClass(cmp.find("searchHelper"), 'slds-hide');
        }
    },
    clearSerchBoxHelper : function(cmp, event, helper) {
        cmp.set("v.searchString",'');
        cmp.set("v.isRecent",true);
        cmp.set("v.isChoosen",false);
        cmp.set("v.isSearchResult",false);
        $A.util.addClass(cmp.find("searchHelper"), 'slds-hide');
    },
    itemSelected : function(component, event, helper) {
        let target = event.target;
        let isValidRec = event.currentTarget.dataset.val;
        if(isValidRec!=undefined){
            let SelIndex = helper.getIndexFrmParent(target,helper,"data-selectedIndex");
            if(SelIndex){
                let serverResult = component.get("v.server_result");
                let selItem = serverResult[SelIndex];
                if(serverResult[SelIndex].isSelected){
                    serverResult[SelIndex].isSelected = false;
                    selItem.isSelected = false;
                }
                else {
                    if(selItem.val){
                        serverResult[SelIndex].isSelected = true;
                        selItem.isSelected = true;
                    }
                }
                let isExistingDB = false;
                let selList = component.get("v.selItemCopy");
                if(selList==undefined){
                    selList=[];
                }
                for(let i=0;i<selList.length;i++){
                    if(selList[i].val == selItem.val){
                        isExistingDB = true;
                        selList[i].isSelected = selItem.isSelected;
                    }
                }
                let tempSelList = [];
                let finalSelList = [];
                for(let i=0;i<selList.length;i++){
                    if(tempSelList.indexOf(selList[i].val)<0 && selList[i].isSelected) {
                        tempSelList.push(selList[i].val);
                        finalSelList.push(selList[i]);
                    }
                }
                if(isExistingDB){
                    component.set("v.selItemCopy",finalSelList);
                }
                if(!isExistingDB) {
                    let recAddRecList = component.get("v.recentlyAddedRecords");
                    if(recAddRecList==undefined){
                        recAddRecList=[];
                    }
                    let isNotExisting = true;
                    for(let i=0;i<recAddRecList.length;i++){
                        if(recAddRecList[i].val == selItem.val){
                            isNotExisting = false;
                            recAddRecList[i].isSelected = selItem.isSelected;
                        }
                    }
                    if(isNotExisting) {
                        recAddRecList.push(selItem);
                    }
                    let finalRecs = [];
                    for(let i=0;i<recAddRecList.length;i++){
                        if(recAddRecList[i].isSelected){
                            finalRecs.push(recAddRecList[i]);
                        }
                    }
                    component.set("v.recentlyAddedRecords",finalRecs);
                }
                component.set("v.last_ServerResult",serverResult);
                component.set("v.server_result",serverResult);
            }
        }
    },
    itemSelectedRecent : function(component, event, helper) {
        let target = event.target;
        let isValidRec = event.currentTarget.dataset.val;
        if(isValidRec!=undefined){
            let SelIndex = helper.getIndexFrmParent(target,helper,"data-selectedIndex");
            if(SelIndex){
                let serverResult = component.get("v.recentRecords");
                let selItem = serverResult[SelIndex];
                if(serverResult[SelIndex].isSelected){
                    serverResult[SelIndex].isSelected = false;
                    selItem.isSelected = false;
                }
                else {
                    if(selItem.val){
                        serverResult[SelIndex].isSelected = true;
                        selItem.isSelected = true;
                    }
                }
                let isExistingDB = false;
                let selList = component.get("v.selItemCopy");
                if(selList==undefined){
                    selList=[];
                }
                for(let i=0;i<selList.length;i++){
                    if(selList[i].val == selItem.val){
                        isExistingDB = true;
                        selList[i].isSelected = selItem.isSelected;
                    }
                }
                let tempSelList = [];
                let finalSelList = [];
                for(let i=0;i<selList.length;i++){
                    if(tempSelList.indexOf(selList[i].val)<0 && selList[i].isSelected) {
                        tempSelList.push(selList[i].val);
                        finalSelList.push(selList[i]);
                    }
                }
                if(isExistingDB){
                    component.set("v.selItemCopy",finalSelList);
                }
                if(!isExistingDB) {
                    let recAddRecList = component.get("v.recentlyAddedRecords");
                    if(recAddRecList==undefined){
                        recAddRecList=[];
                    }
                    let isNotExisting = true;
                    for(let i=0;i<recAddRecList.length;i++){
                        if(recAddRecList[i].val == selItem.val){
                            isNotExisting = false;
                            recAddRecList[i].isSelected = selItem.isSelected;
                        }
                    }
                    if(isNotExisting) {
                        recAddRecList.push(selItem);
                    }
                    let finalRecs = [];
                    for(let i=0;i<recAddRecList.length;i++){
                        if(recAddRecList[i].isSelected){
                            finalRecs.push(recAddRecList[i]);
                        }
                    }
                    component.set("v.recentlyAddedRecords",finalRecs);
                }
                component.set("v.last_ServerResult",serverResult);
                component.set("v.recentRecords",serverResult);
                let saveRecords=component.get("c.saveEvent");
                saveRecords.setParams({ eventRecId : component.get("v.recordId"),
                                       attendeeIds:JSON.stringify(finalRecs)
                                      });
            }
        }
    },
    removeSelected : function(component, event, helper){
        let target = event.target;
        let isValidRec = event.currentTarget.dataset.val;
        if(isValidRec!=undefined){
            let SelIndex = helper.getIndexFrmParent(target,helper,"data-selectedIndex");
            if(SelIndex){
                let serverResult = component.get("v.selItemCopy");
                delete serverResult[SelIndex];
                let tempList = [];
                let finalProductsList = [];
                if(typeof serverResult !== "undefined"){
                    for(let i=0;i<serverResult.length;i++){
                        if(serverResult[i]!=undefined && tempList.indexOf(serverResult[i].val)<0){
                            tempList.push(serverResult[i].val);
                            finalProductsList.push(serverResult[i]);
                        }
                    }
                }
                component.set("v.selItemCopy",finalProductsList);
            }
        }
    },
    removeSelectedRecent : function(component, event, helper){
        let   saveRecords;
        let target = event.target;
        let isValidRec = event.currentTarget.dataset.val;
        if(isValidRec!=undefined){
            let SelIndex = helper.getIndexFrmParent(target,helper,"data-selectedIndex");
            if(SelIndex){
                let allServerResult = component.get("v.recentlyAddedRecords");
                let serverResult = component.get("v.recentlyAddedRecords");
                let   delRecordId;
                if(allServerResult[SelIndex] != null) {
                    delRecordId = allServerResult[SelIndex].eventrelatinids;
                }
                
                let selItem = serverResult[SelIndex];
                delete serverResult[SelIndex];
                let tempList = [];
                let finalProductsList = [];
                let  recAddRecList=[];
                if(typeof serverResult !== "undefined"){
                    for(let i=0;i<serverResult.length;i++){
                        if(serverResult[i]!=undefined && tempList.indexOf(serverResult[i].val)<0){
                            tempList.push(serverResult[i].val);
                            finalProductsList.push(serverResult[i]);}
                    }
                    recAddRecList = component.get("v.recentRecords");
                    if(recAddRecList==undefined || recAddRecList==''){
                        recAddRecList=[];
                    }
                    for(let i=0;i<recAddRecList.length;i++){
                        if(recAddRecList[i].val==selItem.val){
                            recAddRecList[i].isSelected = false;
                        }
                    }
                    component.set("v.recentRecords",recAddRecList);
                }
                component.set("v.recentlyAddedRecords",finalProductsList);
                
                //delRecordId = component.get("v.recentRecords")[SelIndex].eventrelatinids;
                saveRecords=component.get("c.deleteEvent");
                saveRecords.setParams({
                    eventRecId : component.get("v.recordId"),
                    attendeeIds:JSON.stringify(recAddRecList),
                    delRecordIds : delRecordId
                });
            }
        }
        $A.enqueueAction(saveRecords);
        
    },
    serverCall : function(component, event, helper, isRecent) {
        component.set("v.server_result",[]);
        let searchText = component.get("v.searchString");
        if(searchText==undefined){
            searchText = '';
        }
        let last_SearchText = component.get("v.last_SearchText");
        if (event.keyCode == 27) {
            helper.clearSelectionHelper(component, event, helper);
        }else if(searchText.trim() != last_SearchText){
            let objectName = component.get("v.objectName");
            let lookupFilters = component.get("v.lookupFilters");
            let action = component.get('c.searchDB');
            action.setStorable();
            action.setParams({
                objectName : objectName,
                fldAPIText : "Name",
                fldAPIVal : "Id",
                lim : "4",
                fldAPISearch : "Name",
                searchText : searchText,
                lookupFilters: lookupFilters,
                eventids:component.get("v.recordId")
            });
            action.setCallback(this,function(response){
                this.handleResponse(response,component,helper,isRecent);
            });
            $A.enqueueAction(action);
            let searchTemp = searchText.trim();
            if(searchTemp==''){
                searchTemp = null;
            }
            component.set("v.last_SearchText",searchTemp);
        }
            else if(searchText && last_SearchText && searchText.trim() == last_SearchText.trim() && !isRecent){
                component.set("v.server_result",component.get("v.last_ServerResult"));
            }
    },
    handleResponse : function (responseVal,component,helper,isRecent){
        if (responseVal.getState() === 'SUCCESS') {
            let retObj = [];
            retObj = JSON.parse(responseVal.getReturnValue());
            if(retObj.length === 0){
                let msgV = component.get("v.noRecordsMsg");
                if(msgV){
                    component.set("v.noRecordsMsg","No Records");
                }
            }else{
                let tempList = [];
                let selList = component.get("v.selItemCopy");
                if(selList!=undefined){
                    for(let i=0;i<selList.length;i++){
                        if(tempList.indexOf(selList[i].val)<0){
                            tempList.push(selList[i].val);
                        }
                    }
                }
                let recSelList = component.get("v.recentlyAddedRecords");
                if(recSelList!=undefined){
                    for(let i=0;i<recSelList.length;i++){
                        if(tempList.indexOf(recSelList[i].val)<0){
                            tempList.push(recSelList[i].val);
                        }
                    }
                }
                for(let i=0;i<retObj.length;i++){
                    if(retObj[i]!=undefined && tempList.indexOf(retObj[i].val)>-1){
                        retObj[i].isSelected = true;
                    }
                }
                if(isRecent){
                    component.set("v.recentRecords",retObj);
                    component.set("v.server_result",[]);
                    if(component.get("v.recentRecords")==undefined || component.get("v.recentRecords").length<=0){
                        component.set("v.noRecordsMsg","No records");
                    }
                }
                else {
                    component.set("v.server_result",retObj);
                    if(component.get("v.server_result")==undefined || component.get("v.server_result").length<=0){
                        component.set("v.noRecordsMsg","No records");
                    }
                }
                component.set("v.last_ServerResult",retObj);
            }
        }
    },
    getIndexFrmParent : function(target,helper,attributeToFind){
        //User can click on any child element, so traverse till intended parent found
        let SelIndex = target.getAttribute(attributeToFind);
        while(!SelIndex){
            target = target.parentNode ;
            SelIndex = helper.getIndexFrmParent(target,helper,attributeToFind);
        }
        return SelIndex;
    },
    getUIThemeHelper : function(component, event, helper){
        let action = component.get("c.getUITheme");
        action.setCallback(this, function(response) {
            let state = response.getState();
            let responseValue = response.getReturnValue();
            this.hideComp(component, "loadingId");
            if (state === "SUCCESS") {
                component.set("v.uiTheme",responseValue);
            }
        });
        $A.enqueueAction(action);
    },
    clearSelectionHelper: function(component, event, helper){
        if(event.currentTarget!=undefined){
            let prodId = event.currentTarget.dataset.value;
            let selectEvent = $A.get("e.c:RemoveProdEvent_TVA");
            selectEvent.setParams({ "productId": prodId,"index":component.get("v.index") });
            selectEvent.fire();
        }
    },
    searchRecordsHelper : function(component, event, helper, isRecent){
        component.set("v.isRecent",false);
        component.set("v.isChoosen",false);
        component.set("v.isSearchResult",true);
        this.serverCall(component, event, helper, isRecent);
    },
    openSearchHelper : function(component, event, helper){
        this.showComp(component,'searchScreen');
        if(component.get("v.uiTheme")=='Theme4d'){
            let searchBox = component.find('searchBoxId');
            $A.util.addClass(searchBox,'desktopMargin');
        }
        component.set("v.isRecent",true);
        component.set("v.isChoosen",false);
        component.set("v.isSearchResult",false);
        component.set("v.isHideHeader",true);
        component.set("v.server_result",[]);
        component.set("v.recentRecords",[]);
        component.set("v.selItemCopy",component.get("v.selItem"));
        this.serverCall(component, event, helper, true);
    },
    closeSearchHelper : function(component, event, helper){
        this.hideComp(component,'searchScreen');
        if(component.get("v.uiTheme")=='Theme4d'){
            let searchBox = component.find('searchBoxId');
            $A.util.removeClass(searchBox,'desktopMargin');
        }
        component.set("v.isHideHeader",false);
        component.set("v.searchString",'');
        component.set("v.server_result",null);
        component.set("v.recentRecords",null);
        component.set("v.recentlyAddedRecords",null);
        let navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId"),
            "slideDevName": "related"
        });
        navEvt.fire();
    },
    nextHelper : function(component, event, helper){
        this.hideComp(component,'searchScreen');
        if(component.get("v.uiTheme")=='Theme4d'){
            let searchBox = component.find('searchBoxId');
            $A.util.removeClass(searchBox,'desktopMargin');
        }
        component.set("v.isHideHeader",false);
        component.set("v.searchString",'');
        component.set("v.server_result",null);
        component.set("v.recentRecords",null);
        component.set("v.selItem",component.get("v.selItemCopy"));
        let selectEvent = $A.get("e.c:AddProductEvent_TVA");
        selectEvent.setParams({ "selProduct": component.get("v.selItem"),"recSelProduct":component.get("v.recentlyAddedRecords"),"index":component.get("v.index") });
        selectEvent.fire();
        component.set("v.recentlyAddedRecords",null);
    },
    openRecentHelper : function(component,event,helper) {
        component.set("v.searchString","");
        this.searchRecordsHelper(component, event, helper, true);
        $A.util.addClass(component.find("searchHelper"), 'slds-hide');
        component.set("v.isRecent",true);
        component.set("v.isChoosen",false);
        component.set("v.isSearchResult",false);
    },
    showComp: function(component, compId) {
        let compVar = component.find(compId);
        $A.util.removeClass(compVar, 'slds-hide');
        $A.util.addClass(compVar, 'slds-show');
    },
    hideComp: function(component, compId) {
        let compVar = component.find(compId);
        $A.util.removeClass(compVar, 'slds-show');
        $A.util.addClass(compVar, 'slds-hide');
        
    }
})