({
    itemSelected : function(component, event, helper) {
        helper.itemSelected(component, event, helper);
    }, 
    serverCall :  function(component, event, helper) {
        document.getElementById("combobox-unique-id").autocomplete = "off";
        var cmpDiv = component.find('hwDiv');
        $A.util.addClass(cmpDiv,'slds-is-open');
        helper.serverCall(component, event, helper);
    },
    /*added by Diwanshu start*/
    onClickServerCall :  function(component, event, helper) {
        document.getElementById("combobox-unique-id").autocomplete = "off";
        helper.onClickServerCall(component, event, helper);
    },
    /*added by Diwanshu end*/
    clearSelection : function(component, event, helper){
        var cmpDiv = component.find('hwDiv');
        $A.util.removeClass(cmpDiv,'slds-is-open');
        helper.clearSelection(component, event, helper);
    },
    onblur : function(component,event,helper){       
        var cmpDiv = component.find('hwDiv');
        $A.util.addClass(cmpDiv, 'slds-is-close');
        $A.util.removeClass(cmpDiv, 'slds-is-open');
    },
})