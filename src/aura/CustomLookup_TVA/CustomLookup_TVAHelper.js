({
    itemSelected : function(component, event, helper) {
        let target = event.target;
        let SelIndex = helper.getIndexFrmParent(target,helper,"data-selectedIndex");
        if(SelIndex){
            let serverResult = component.get("v.server_result");
            let selItem = serverResult[SelIndex];
            if(selItem.val){
                component.set("v.selItem",selItem);
                component.set("v.last_ServerResult",serverResult);
                let SelectedOpt=$A.get("e.c:SW_PassIdForNRIC_LEX");
                SelectedOpt.setParams({
                    "SelectedOption":selItem.val
                });
                SelectedOpt.fire();
            }
            component.set("v.server_result",null);
        }
    },
    serverCall : function(component, event, helper) {
        let target = event.target;
        let searchText = target.value;
        let last_SearchText = component.get("v.last_SearchText");
        //Escape button pressed
        if (event.keyCode == 27 || !searchText.trim()) {
            helper.clearSelection(component, event, helper);
        }else if(searchText.trim() != last_SearchText) {
            this.serverCallResult(component, event, helper,searchText);
        }else if(searchText && last_SearchText && searchText.trim() == last_SearchText.trim()){
            component.set("v.server_result",component.get("v.last_ServerResult"));
        }
    },
    onClickServerCall: function (component, event, helper){
        let target = event.target;  
        let searchText = target.value;        
        let recentItems='true';
        let limit = $A.get("10");
        component.set("v.limit",limit);
        this.serverCallResult(component, event, helper,searchText,recentItems);
    },
    serverCallResult: function (component, event, helper,searchText,recentItems){
        //Save server call, if last text not changed
        //Search only when space character entered       
        let objectName = component.get("v.objectName");
        let recID = component.get("v.recID");
        let field_API_text = component.get("v.field_API_text");
        let field_API_val = component.get("v.field_API_val");
        let field_API_search = component.get("v.field_API_search");
        let limit = component.get("v.limit");
        let newValue = component.get("v.accountfilter");
        let lookupFilters = component.get("v.lookupFilters");       
        let action = component.get('c.searchDB');
        action.setStorable();
        action.setParams({
            objectName : objectName,
            fldAPIText : field_API_text,
            fldAPIVal : field_API_val,
            lim : limit,
            fldAPISearch : field_API_search,
            searchText : searchText,
            lookupFilters :lookupFilters,
            recentItems:recentItems,
            recID: recID,
            newFilter:newValue
        });        
        action.setCallback(this,function(a){
            this.handleResponse(a,component,helper);            
        });        
        component.set("v.last_SearchText",searchText.trim());
        $A.enqueueAction(action);
    },
    handleResponse : function (res,component,helper){
        if (res.getState() === 'SUCCESS')
        {
            let retObj = JSON.parse(res.getReturnValue());
            if(retObj.length <= 0)
            {
                let noResult = JSON.parse('[{"text":"No Results Found"}]');
                component.set("v.server_result",noResult);
                component.set("v.last_ServerResult",noResult);
            }
            else{
                component.set("v.server_result",retObj);
                component.set("v.last_ServerResult",retObj);
            }
        }
        else if (res.getState() === 'ERROR'){
            let errors = res.getError();
            if (errors)
            {
                if (errors[0] && errors[0].message)
                {
                    
                }
            } 
        }
    },
    getIndexFrmParent : function(target,helper,attributeToFind){
        //User can click on any child element, so traverse till intended parent found
        let SelIndex = target.getAttribute(attributeToFind);
        while(!SelIndex){
            target = target.parentNode ;
            SelIndex = helper.getIndexFrmParent(target,helper,attributeToFind);           
        }
        return SelIndex;
    },
    clearSelection: function(component, event, helper){
        component.set("v.selItem",null);
        component.set("v.server_result",null);
        let SelectedOpt	=	$A.get("e.c:SW_PassIdForNRIC_LEX");
        SelectedOpt.setParams({
            "SelectedOption": null
        });
        SelectedOpt.fire();
    }
})