({
	init : function(component, event, helper) {
		let record = component.get('v.record');
        let field = component.get('v.field');
        component.set('v.value', record[field.name]);
    },
    navigateToAccount:function(component){
        // it returns only first value of Id
        var accRec  = component.get("v.record");
        var sObectEvent = $A.get("e.force:navigateToSObject");
        sObectEvent .setParams({
            "recordId": accRec.Id,
            "slideDevName": "detail"
        });
        sObectEvent.fire();
    }
})