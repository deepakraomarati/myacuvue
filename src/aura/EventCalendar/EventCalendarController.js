({
    scriptsLoaded : function(component, event, helper){
        helper.fetchEvents(component,event,helper);
        helper.helperOncheck(component,event,helper);
    },
    doInit : function(component, event, helper) {
        helper.initComponent(component, event, helper);
        let action = component.get("c.getListViews");
        action.setCallback(this, function(response){
            let state = response.getState();
            if (state === "SUCCESS") {
                let result = response.getReturnValue();
                component.set("v.AccountListViewList",result.listviews);
                helper.fetchRecords(component,event,helper,result.listviews[0].id);
            }
        });
        $A.enqueueAction(action);
        let options = [
            {'label': $A.get("$Label.c.Daily"), 'value': 'Daily' },
            {'label': $A.get("$Label.c.Weekly"), 'value': 'Weekly' },
            {'label': $A.get("$Label.c.Monthly"), 'value': 'Monthly' },
            {'label': $A.get("$Label.c.Yearly"), 'value': 'Yearly' }
        ];
        component.set('v.options', options);
    },
    handleclickNextEvt : function(component, event,helper){
        let recordTypeID = event.getParam("recTypeId");
        let recTypeLabel = event.getParam("recTypeLabel");
        let recTypeName = event.getParam("recTypeName");
        component.set("v.recTypeID",recordTypeID);
        component.set("v.recordTypeLabel",recTypeLabel);
        component.set("v.recordTypeName",recTypeName);
        //Show All day or not
        if(recTypeName === 'TimeOffTerritoryANZ' || recTypeName ==='Time_off_Territory_CH' || 
           recTypeName ==='TimeOffTerritoryHKG' || recTypeName === 'Time_Off_Territory_IN' || 
           recTypeName ==='TimeOffTerritoryKR' || recTypeName ==='Time_Off_Territory_SG' || 
           recTypeName ==='Time_Off_Territory_TH' || recTypeName ==='Time_Off_Territory_TW' || 
           recTypeName ==='ZZ_KPI_Metrics'){
            component.set("v.showAllDay" , false);
        }
        else
            component.set("v.showAllDay" , true);
        if(recordTypeID !== null && typeof recordTypeID !== "undefined"){
            component.set("v.NewEvt" , false);
            let modal = component.find('modal');
            $A.util.addClass(modal, 'slds-fade-in-open');
            let backdrop = component.find('backdrop');
            $A.util.addClass(backdrop, 'slds-backdrop--open');
            let actionmetadata = component.get("c.getMetadataValues");
            actionmetadata.setParams({
                recTypeName: recTypeName,
                langLocKey: $A.get("$Locale.langLocale")
            });
            actionmetadata.setCallback(this, function(response){
                let state = response.getState();
                if (state === "SUCCESS"){
                    let obj = {};
                    let fieldApiList = [];
                    for(let i=0; i<response.getReturnValue().lstEvtRT.length; i++){
                        if(typeof response.getReturnValue().lstEvtRT[i].Label_Value_pairs__c !== "undefined"){
                            let arr = [{'label': 'None', 'value': ''}];
                            for(let j=0; j<response.getReturnValue().lstEvtRT[i].Label_Value_pairs__c.split(',').length; j++){
                                if(typeof response.getReturnValue().lstEvtRT[i].Label_Value_pairs__c.split(',') !== "undefined"){
                                    	arr.push({'label': response.getReturnValue().lstEvtRT[i].Label_Value_pairs__c.split(',')[j].split('#')[0],
                                              	  'value': response.getReturnValue().lstEvtRT[i].Label_Value_pairs__c.split(',')[j].split('#')[1]});
                                }
                            }
                            //let arr = response.getReturnValue().lstEvtRT[i].Values__c.split(',');
                            //arr.unshift(response.getReturnValue().lstEvtRT[i].Default_Value__c);
                            if(response.getReturnValue().lstEvtRT[i].Default_Value__c !== "None"){
                            	component.set("v.event."+response.getReturnValue().lstEvtRT[i].Field_Api__c , response.getReturnValue().lstEvtRT[i].Default_Value__c);
                            	arr.splice(0,1);
                        	}
                        	obj[response.getReturnValue().lstEvtRT[i].Field_Api__c] = arr;
                            fieldApiList.push(response.getReturnValue().lstEvtRT[i].Field_Api__c);
                           	// AATB-4424 - Default subject value
                           	if(response.getReturnValue().lstEvtRT[i].Field_Api__c == 'Subject'){
                            	var subject = {text:response.getReturnValue().lstEvtRT[i].Default_Value__c};
								component.set("v.selSubject",subject);
                           	}
                        }
                    }
                    component.set("v.eventRTPickList",obj);
                    component.set("v.fieldAPIList", fieldApiList);
                    component.set("v.isActivityTypeAccesible",response.getReturnValue().isAccessible);
                }
            });
            $A.enqueueAction(actionmetadata);
        }
        else{
            component.set("v.NewEvt" , false);
            helper.closeModal(component, event,helper);
        }
    },
    closeModal : function(component, event, helper) {
        document.getElementById("divDaily").style.display   = "block";
        document.getElementById("divWeekly").style.display  = "none";
        document.getElementById("divYearly").style.display  = "none";
        document.getElementById("divMonthly").style.display = "none";
        helper.closeModal(component, event,helper);
    },
    search : function(component, event, helper) {
        if(event.getParams().keyCode == 13){
            helper.fetchSearchedRecords(component, event, helper);
        }
    },
    handleSortEvent : function(component, event, helper) {
        let data = event.getParam('data');
        component.set('v.keyword','');
        component.set('v.sortDir', data.sortDir);
        component.set('v.sortField', data.sortField);
        helper.fetchRecords(component, event, helper);
    },
    first : function(component, event, helper) {
        let pageNumber = 0;
        let pages = component.get('v.pages');
        component.set('v.pageNumber', pageNumber);
        component.set('v.records', pages[pageNumber]);
        helper.setDraggableUI(component, event, helper);
    },
    previous : function(component, event, helper) {
        let pageNumber = component.get('v.pageNumber');
        let pages = component.get('v.pages');
        if(pageNumber > 0) {
            pageNumber--;
        }
        component.set('v.pageNumber', pageNumber);
        component.set('v.records', pages[pageNumber]);
        helper.setDraggableUI(component, event, helper);
    },
    next : function(component, event, helper) {
        let pageNumber = component.get('v.pageNumber');
        let pages = component.get('v.pages');
        if(pageNumber < pages.length - 1) {
            pageNumber++;
        }
        component.set('v.pageNumber', pageNumber);
        component.set('v.records', pages[pageNumber]);
        helper.setDraggableUI(component, event, helper);
    },
    last : function(component, event, helper) {
        let pages = component.get('v.pages');
        let pageNumber = pages.length - 1;
        component.set('v.pageNumber', pageNumber);
        component.set('v.records', pages[pageNumber]);
        helper.setDraggableUI(component, event, helper);
    },
    getFilteredAccount : function(component, event, helper) {
        component.set("v.pageNumber",0);
        helper.fetchRecords(component,event,helper,null);
    },
    onWeekendChange:function(component,event,helper){
        helper.helperOnchange(component,event,helper);
    },
    onfreqChange:function(component,event,helper){
        helper.helperOnFreqchange(component,event,helper);
    },
    SaveEventRecords:function(component,event,helper){
        helper.helperSaveEventRecords(component,event,helper);
    },
    onCheck:function(component,event,helper){
        helper.helperOncheck(component,event,helper);
    },
    onDailyChange:function(component,event,helper){
        helper.helperonDailyChange(component,event,helper);
    },
    onMonthChange:function(component,event,helper){
        helper.helperonMonthChange(component,event,helper);
    },
    onYearChange:function(component,event,helper){
        helper.helperonYearChange(component,event,helper);
    },
    onDOWChange:function(component,event,helper){
        helper.helperonDOWChange(component,event,helper);
    },
    onDOYChange:function(component,event,helper){
        helper.helperonDOYChange(component,event,helper);
    }
})