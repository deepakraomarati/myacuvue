({
    doInIt : function(component, event, helper) {
        let accessCheck = component.get("c.accessCheck");
        accessCheck.setParams({
            "recordId": component.get("v.recordId")
        });
        accessCheck.setCallback(this, function(a){
            // check the return state
            if (a.getState() ==="SUCCESS")
            {
                let result = a.getReturnValue();
                if(result == 'Access Denied'){
                    component.set("v.showComp", false);
                    component.set("v.errorMsg" , 'Access Denied');
                    component.set("v.IsSpinner",false);
                    component.set("v.IsStdSpinner",false);
                }
                else if(result == 'Custom'){
                    component.set("v.IsSpinner",false);
                    component.set("v.showComp",true);
                }
                    else{
                        component.set("v.showStdComp", true);
                        component.set("v.IsSpinner",false);
                        setTimeout(function(){
                            component.set("v.IsStdSpinner",false);
                        }, 2000);
                    }
            }
        });
        $A.enqueueAction(accessCheck);
    },
    closeModal: function(component, event, helper) {
        let wasDismissed = $A.get("e.force:closeQuickAction");
        wasDismissed.fire();
    },
    handleClick: function(component, event, helper) {
        component.find("edit").get("e.recordSave").fire();
    },
    handleSaveSuccess: function(component, event, helper) {
        component.set("v.IsSpinner", false);
        let wasDismissed = $A.get("e.force:closeQuickAction");
        wasDismissed.fire();
        document.location.reload();
    }
})