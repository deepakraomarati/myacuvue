({
    //fetchPicklistFields method to fetch and populate dat for picklist fields
    fetchPicklistFields: function(component, picklistFields)
    {
        //all picklist fields
        let taskPicklistFields = ["JJ_JPN_PaymentCondition__c","JJ_JPN_NoDeliveryCharges__c",
                                  "JJ_JPN_SubCustomerGroup__c", "JJ_JPN_IndividualDeliveryPossibleFriday__c","JJ_JPN_StatusCode__c","JJ_JPN_ADSHandlingFlag__c",
                                  "JJ_JPN_EDIFlag__c","JJ_JPN_DeadlineTime__c","JJ_JPN_IndividualDeliveryPossibleMonday__c",
                                  "JJ_JPN_ItemizedBilling__c","JJ_JPN_FestivalDelivery__c","JJ_JPN_IndividualDeliveryPossibleSat__c",
                                  "JJ_JPN_PIndividualDeliveryPossibleThur__c","JJ_JPN_IndividualDeliveryPossibleTues__c",
                                  "JJ_JPN_ReturnedGoodsOrderNoInDigits__c","JJ_JPN_CustomerOrderNoNumberofDigits__c",
                                  "JJ_JPN_FAXOrder__c","JJ_JPN_LotNumberCommunicationDocument__c",
                                  "JJ_JPN_TransactionDetailSubmission__c","JJ_JPN_BillToRTNFAX__c",
                                  "JJ_JPN_IndividualDeliveryPossibleDateSun__c","JJ_JPN_IndividualDeliveryPossibleDateWed__c",
                                  "JJ_JPN_CustomerOrderNoRequired__c","JJ_JPN_DeliveryDocumentNote__c","JJ_JPN_BillSubmission__c",
                                  "JJ_JPN_BillingSummaryTableSubmission__c","JJ_JPN_SoldToNumberOfPagesReceived__c",
                                  "JJ_JPN_DirectDelivery__c","JJ_JPN_PaymentMethod__c","JJ_JPN_SalesItem__c",
                                  "JJ_JPN_CustomerRTNFAXHowManyTimesToSend__c", "JJ_JPN_OrderBlock__c"];
        picklistFields['JJ_JPN_CustomerMasterRequest__c'] = taskPicklistFields;
    },
    //getPicklistValues method
    getPicklistValues: function(component, sobjFieldsmap) {
        //get the object
        for (let obj in sobjFieldsmap) {
            if (sobjFieldsmap.hasOwnProperty(obj)) {
                let objName = sobjFieldsmap[obj];
                let field;
                //get fields of object
                for (field in objName) {
                    if (objName.hasOwnProperty(field)) {
                        let optionValues = objName[field];
                        this.buildPicklist(component, obj + "." + field, optionValues);
                    }
                }
            }}
    },
    //buildPicklist: Map values in the UI
    buildPicklist: function(component, elementId, optionValues)
    {
        let opts = [];
        if (optionValues !== undefined && optionValues.length > 0)
        {
            for (let i = 0; i < optionValues.length; i++) {
                opts.push({
                    class: "optionClass",
                    value: optionValues[i].value,
                    label: optionValues[i].label
                });
            }
        }
        if(elementId === "JJ_JPN_CustomerMasterRequest__c.JJ_JPN_DeadlineTime__c"){
            component.set("v.deadline" , opts[0].label);
        }
        component.find(elementId).set("v.options", opts);
    },
    //autopopulate: auto populate accoutn related fieds on CMR popup
    autopopulate:function(component,picklistFields)
    {
        //get the record ID of the record
        let accId = component.get("v.recordId");
        let action = component.get("c.autoPopulateAccValues");
        //set parameters to be send to apex class
        action.setParams({
            "acc1" : accId,
            "objpicklistFieldsMap": JSON.stringify(picklistFields)
        });
        action.setCallback
        (
            this, function(a)
            {
                // check the return state
                if (a.getState() ==="SUCCESS")
                {
                    let result = a.getReturnValue();
                    component.set("v.acc", result.CMR);
                    this.getPicklistValues(component, result.objFieldPicklistMap);
                }
                else
                {
                    $A.log("Errors", a.getError());
                }
            }
        );
        $A.enqueueAction(action);
    },
    helperhandleClick : function(component, event, helper){
        component.set("v.errorMsg",'');
        let isValid=false;
        let PostalCode = component.find("textInput1").get("v.value");
        
        if (PostalCode==='' || PostalCode===undefined)
        {
            let locale = $A.get("$Locale.language");
            if(locale=='en')            
                component.set("v.errorMsg",'Postal Code Sold To : Validation Error: Value is required.\n');
            else
                component.set("v.errorMsg",'郵便番号の販売先：検証エラー：値は必須です\n');
            isValid=true;
        }
        else
        {
            if (PostalCode.length >= 8 )
            {let locale = $A.get("$Locale.language");
             if(locale=='en') 
                 component.set("v.errorMsg",'Postal Code Sold To: Postal Code Sold To: data value too large(max length=7)\n');
             else
                 component.set("v.errorMsg",'郵便番号：データ値が大きすぎます（最大長= 7）\n');
             
             isValid=true;
            }
            if (PostalCode.length <= 6 )
            {let locale = $A.get("$Locale.language");
             if(locale=='en') 
                 component.set("v.errorMsg",'Postal Code Sold To : Please provide 7 digit postal code.\n');
             else
                 component.set("v.errorMsg",'郵便番号：7桁の郵便番号を入力してください\n');
             isValid=true;
            }
        }
        if(isValid==false)
        {
            helper.handleClickHelper(component, event, helper);
        }
          
    },
    //handleClickHelper method
    handleClickHelper : function(component, event, helper)
    {
        let editedCMRVal = component.get("v.acc");
        component.set("v.errorMsg", '' );
        let toastEvent = $A.get('e.force:showToast');
        // Defining the action to save contact List ( will call the saveContactList apex controller )
        let saveAction = component.get("c.CMRSave");
        // setting the params to be passed to apex controller
        saveAction.setParams({
            toUpdCMRrec: editedCMRVal
        });
        // callback action on getting the response from server
        saveAction.setCallback(this, function(response) {
            // Getting the state from response
            let state = response.getState();
            if(state === 'SUCCESS') {
                // Getting the response from server
                let dataMap = response.getReturnValue();
                // Checking if the status is success
                if(dataMap.status==='success') {
                    // Close the action panel and refresh parent page
                    let dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();
                    $A.get('e.force:refreshView').fire();
                    // Setting the success toast which is dismissable ( vanish on timeout or on clicking X button )
                    let locale = $A.get("$Locale.language");
                    if(locale=='en'){
                        toastEvent.setParams({
                            'title': 'Success!',
                            'type': 'success',
                            'mode': 'dismissable',
                            'message': dataMap.message
                        });}
                    else{
                        toastEvent.setParams({
                            'title': '成功',
                            'type': 'success',
                            'mode': 'dismissable',
                            'message': dataMap.message
                        });
                    }
                    // Fire success toast event ( Show toast )
                    toastEvent.fire();
                    document.location.reload();
                }
                // Checking if the status is error
                else{
                    // Setting the error toast which is dismissable ( vanish on timeout or on clicking X button )
                    let locale = $A.get("$Locale.language");
                    if(locale=='en'){
                        toastEvent.setParams({
                            'title': 'Error!',
                            'type': 'error',
                            'mode': 'dismissable',
                            'message': dataMap.message
                        });
                    }
                    else
                    {
                        toastEvent.setParams({
                            'title': 'エラー',
                            'type': 'error',
                            'mode': 'dismissable',
                            'message': dataMap.message
                        });
                    }
                    // Fire error toast event ( Show toast )
                    component.set("v.errorMsg", dataMap.message);
                    toastEvent.fire();
                }
            }
            else {
                // Show an alert if the state is incomplete or error
                component.set("v.errorMsg", 'Error in getting data');
            }
        });
        // Adding the action variable to the global action queue
        $A.enqueueAction(saveAction);
    }
})