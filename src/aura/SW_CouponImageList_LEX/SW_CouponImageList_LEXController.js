({
    doInit : function(component, event, helper)
    {   component.set("v.errorMsg",'');
        helper.autopopulate(component,event, helper);
        let action = component.get("c.getCouponTypeList");
        action.setCallback(this, function(response) {
            component.set("v.couponType", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    saveCoupon: function(component, event, helper)
    {
        helper.saveHelperCoupon(component, event, helper, 'Save');
    },
    saveNewCoupon: function(component, event, helper)
    {
        helper.saveHelperCoupon(component, event, helper, 'SaveNew');
    },
    closeModal: function(component, event, helper) {
        // set "isOpen" attribute to false for hide/close model box
        component.set("v.status", false);
        let appEvent = $A.get("e.c:SW_CouponImageEvt_LEX");
        appEvent.setParams({
            "status" : false,
            "reload" : true
        });
        appEvent.fire();
    },
    handleFilesChange: function(component, event, helper){
        let fileName = 'No File Selected..';
        if (event.getSource().get("v.files").length > 0){
            fileName = event.getSource().get("v.files")[0]['name'];
        }
        component.set("v.fileName", fileName);
    },
    handleCheck : function(component, event, helper){
        let isChecked = component.find("DisclaimerCheckBox").get("v.checked");
        component.set("v.newCoupon.MA2_Status__c", isChecked);
    }
})