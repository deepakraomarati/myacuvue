({
    autopopulate:function(component, event, helper)
    {
        //get the record ID of the record
        let couponId = component.get("v.recordId");
        let action = component.get("c.autoPopulateAccValues");
        //set parameters to be send to apex class
        action.setParams({ "recordID" : couponId});
        action.setCallback
        (
            this, function(a)
            {
                // check the return state
                if (a.getState() ==="SUCCESS")
                {
                    let result = a.getReturnValue();
                    component.set("v.CouponVar.Name", result.Name);
                }
                else
                {
                    $A.log("Errors", a.getError());
                }
            }
        );
        $A.enqueueAction(action);
    },
    saveHelperCoupon: function(component, event, helper, buttonId)
    {
        component.set("v.errorMsg",'');
        let fileInput = component.find("fileId").get("v.files");
        let newImage = component.get("v.newCoupon");
        let fileName = component.get("v.fileRename");
        //When fileInput is blank
        if(fileInput === null){
            component.set("v.errorMsg", 'Please upload File. \n');
        }
        //When coupon type is blank
        if(newImage.MA2_Coupon_Type__c === ''){
            let err = '  Please Select The Coupon Type. \n';
            component.set("v.errorMsg", component.get("v.errorMsg")+ err);
        }
        //When fileName is blank
        if(fileName === ''){
            let err = '  Please Enter File Name. \n';
            component.set("v.errorMsg", component.get("v.errorMsg")+ err);
        }
        //When coupon type selected already exists, or
        //coupon type is selected and remaining mandatory fields are not selected
        if(component.get("v.errorMsg") === '' && fileInput !== null){
            let file = fileInput[0];
            let fileType = file.type;
            if(fileType.indexOf("image") === -1){
                let err = ' Please Upload Only Image File. \n';
                component.set("v.errorMsg", component.get("v.errorMsg")+ err);
            }
            else{
                let fr = new FileReader();
                let couponId = component.get("v.recordId");
                fr.onload = $A.getCallback(function() {
                    let fileContents = fr.result;
                    let base64Mark = 'base64,';
                    let dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
                    fileContents = fileContents.substring(dataStart);
                    helper.upload(component,newImage,couponId,file,fileContents,fileName,buttonId);
                });
                fr.readAsDataURL(file);
            }
        }
    },
    upload: function(component,newImage,couponId,file,fileContents,fileName,buttonId) {
        // Defining the action to save contact List ( will call the saveContactList apex controller )
        let action = component.get("c.saveCouponRecord");
        // setting the params to be passed to apex controller
        action.setParams({
            "couponImage": newImage,
            "couponId" :couponId,
            "fileName": fileName,
            "uploadFile": encodeURIComponent(fileContents),
            "fileType": file.type
        });
        // callback action on getting the response from server
        action.setCallback(this, function(response) {
            let state = response.getState();
            if(state === 'SUCCESS'){
                let result = response.getReturnValue();
               
                if(result.type === 'success'){
                   
                    let appEvent;
                    if(buttonId === 'SaveNew'){
                        //Reset all values for new modal after save
                        component.set("v.newCoupon.MA2_Coupon_Type__c", '');
                        component.set("v.DisclaimerCheckBox", true);
                        component.set("v.fileName", '');
                        component.set("v.fileRename", '');
                        appEvent = $A.get("e.c:SW_CouponImageEvt_LEX");
                        appEvent.setParams({
                            "status" : true,
                            "reload" : true
                        });
                        appEvent.fire();
                    }
                    else{
                        component.set("v.status", false);
                        appEvent = $A.get("e.c:SW_CouponImageEvt_LEX");
                        appEvent.setParams({
                            "status" : false,
                            "reload" : true
                        });
                        appEvent.fire();
                    }
                }
                else{
                    component.set("v.errorMsg", result.value[0]);
                }
            }
            else{
                component.set("v.errorMsg", 'Error in getting data');
            }
        });
        $A.enqueueAction(action);
    }
})