({
    //doInit method
    doInit : function(component, event, helper) {
        helper.getDataHelper(component, event);
    },
    handleClick : function(component, event, helper) {
        component.set("v.status",true);
    },
    handleApplicationEvent : function(component, event, helper) {
        component.set("v.status", event.getParam("status"));
        component.set("v.reload", event.getParam("reload"));
        helper.getDataHelper(component, event);
    },
    handleRowAction: function (component, event, helper) {
        let action = event.getParam('action');
        let row = event.getParam('row');
        let couponImageId = row.couponImageId;
        switch (action.name) {
            case 'view':
                window.open('/servlet/servlet.FileDownload?file='+row.fileId);
                break;
            case 'delete':
                component.set("v.couponImgId", couponImageId);
                component.set("v.isOpen", true);
                break;
            case 'edit':
                let editRecordEvent = $A.get("e.force:editRecord");
                editRecordEvent.setParams({
                    "recordId": couponImageId
                });
                editRecordEvent.fire();
                break;
            default: // default clause should be the last one
                break;
        }
    },
    okModel: function(component, event, helper) {
        let delaction = component.get("c.deleteRecord");
        delaction.setParams({
            couponImageId : component.get("v.couponImgId")
        });
        delaction.setCallback(this, function(response){
            window.location.reload();
        });
        $A.enqueueAction(delaction);
        component.set("v.isOpen", false);
    },
    closequickAction: function(component, event, helper) {
        component.set("v.isOpen", false);
    }
})