({
    //getDataHelper method
    getDataHelper : function(component, event) {
        let action = component.get("c.getAllCoupons");
        //Set the Object parameters and Field Set name
        action.setParams({
            couponId : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            let state = response.getState();
            let actions = [
                { label: 'Edit', name: 'edit' },
                { label: 'Delete', name: 'delete' },
                { label: 'View', name: 'view' }
            ];
            if(state === 'SUCCESS'){
              
                component.set("v.mycolumns", [
                    {label: 'File Name', fieldName: 'fileName', type: 'text'},
                    {label: 'Coupon Type', fieldName: 'couponType', type: 'text'},
                    {label: 'Coupon Status', fieldName: 'status', type: 'boolean'},
                    {label: 'Created Date', fieldName: 'createdDate', type: 'text'},
                    {label: 'Created By', fieldName: 'createdBy', type: 'text'},
                    { type: 'action', typeAttributes: { rowActions: actions } }
                ]);
               component.set("v.mydata", response.getReturnValue());
            }
        });
        //Invoke apex method
        $A.enqueueAction(action);
    }
})