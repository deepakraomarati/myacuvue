({  
   doInit: function(component,event,helper){
        let action = component.get("c.getEventDetails");
         action.setParams({
            "recordId": component.get("v.recordId")
        });
       action.setCallback(this, function(response) {
           component.set("v.WhatId", response.getReturnValue().WhatId);
           component.set("v.WhoId",  response.getReturnValue().WhoId);
           component.set("v.Subject",response.getReturnValue().Subject);
       });
        $A.enqueueAction(action);
   },
    /* On the component Load this function call the apex class method, 
    * which is return the list of RecordTypes of object 
    * and set it to the lstOfRecordType attribute to display record Type values
    * on ui:inputSelect component. */
    fetchListOfRecordTypes: function(component, event, helper) {
        let action = component.get("c.fetchRecordTypeValues");
        action.setCallback(this, function(response) {
            component.set("v.lstOfRecordType", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    /* In this "createRecord" function, first we have call apex class method 
    * and pass the selected RecordType values[label] and this "getRecTypeId"
    * apex method return the selected recordType ID.
    * When RecordType ID comes, we have call  "e.force:createRecord"
    * event and pass object API Name and 
    * set the record type ID in recordTypeId parameter. and fire this event
    * if response state is not equal = "SUCCESS" then display message on various situations.
    */
    createRecord: function(component, event, helper) {
        component.set("v.rtDisplay" , false);
        if(document.getElementsByClassName("forceMobileOverlay open active") !=undefined &&
           document.getElementsByClassName("forceMobileOverlay open active").length > 0)
            document.getElementsByClassName("forceMobileOverlay open active")[0].style.zIndex ='';
        let  recordId = component.get("v.recordId");
        let action = component.get("c.getRecTypeId");
        let selectCmp = component.find("selectid");
        let recordTypeLabel = selectCmp.get("v.value")
        action.setParams({
            "recordTypeLabel": recordTypeLabel
        });
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                let createRecordEvent = $A.get("e.force:createRecord");
                let RecTypeID  = response.getReturnValue();
                createRecordEvent.setParams({
                    "entityApiName": 'Event',
                    "recordTypeId": RecTypeID,
                    "defaultFieldValues"    : {
                        'WhatId' : component.get("v.WhatId"),
                        'WhoId'  :component.get("v.WhoId"),
                        'Subject':component.get("v.Subject")
                    },  
                    "panelOnDestroyCallback": function(event) {
                        $A.get("e.force:closeQuickAction").fire();
                    }
                });
                createRecordEvent.fire();
            }
        });
        
        $A.enqueueAction(action);
    },
    closeModal: function(component, event, helper) {
        // set "isOpen" attribute to false for hide/close model box
        $A.get("e.force:closeQuickAction").fire();
        
    }  
})