({
    doInit : function(component, event, helper)
    {
        helper.autopopulate(component,event, helper);
    },
    saveCoupon: function(component, event, helper)
    {
        let action = component.get("c.updateAttachment");
        //set parameters to be send to apex class
        action.setParams({"attachObj" : component.get("v.attach")});
        action.setCallback(
            this, function(a){
                // check the return state
                if (a.getState() ==="SUCCESS")
                {
                   window.location.reload();
                }
                else
                {
                    $A.log("Errors", a.getError());
                }
            }
        );
        $A.enqueueAction(action);
    },
    closeModal: function(component, event, helper) {
        // set "isOpen" attribute to false for hide/close model box
        component.set("v.status", false);
        let appEvent = $A.get("e.c:SW_CouponImageEvt_LEX");
        appEvent.setParams({
            "status" : false
        });
        appEvent.fire();
    },
    handleCheck : function(component, event, helper){
        let isChecked = component.find("DisclaimerCheckBox").get("v.checked");
        component.set("v.attach.IsPrivate", isChecked);
    }
})