({
    autopopulate:function(component, event, helper)
    {
        //get the record ID of the record
        let recordId = component.get("v.rowID");
        let action = component.get("c.autoPopulateAccValues");
        //set parameters to be send to apex class
        action.setParams({ "recordID" : recordId});
        action.setCallback(
            this, function(a){ 
                // check the return state
                if (a.getState() ==="SUCCESS")
                {
                    let result = a.getReturnValue();
                    component.set("v.attach", result);
                    component.set("v.bodyLength",result.BodyLength);
                    let bodyLength = component.get("v.bodyLength");
                    if(bodyLength < 1024){
                        let bytes = 'bytes';
                        component.set("v.bodyLength",result.BodyLength  + bytes);
                    }
                    else if(bodyLength > 1024 && bodyLength < 1048576)
                    {
                        component.set("v.bodyLength",result.BodyLength/1024);
                        component.set("v.fileSizeType"," KB");
                    }
                        else{
                            component.set("v.bodyLength",result.BodyLength/1048576);
                            component.set("v.fileSizeType"," MB");
                        }
                }
                else
                {
                    $A.log("Errors", a.getError());
                }
            }
        );
        $A.enqueueAction(action);
    }
})