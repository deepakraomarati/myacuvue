({
    //int method
    //init method, which gets invoked on page load
    doInit : function(component, event, helper) {
        let toast = component.get("v.toastType");
        let mode = component.get("v.mode");
        // apply toast css and display icon according to toast type
        if(toast === 'error'){
            component.set("v.toastIcon",'error');
            component.set("v.toastcss",'slds-notify slds-notify_toast slds-theme_error');
        }
        else if(toast === 'success'){
            component.set("v.toastIcon",'success');
            component.set("v.toastcss",'slds-notify slds-notify_toast slds-theme_success');
        }
        else if(toast === 'warning'){
            component.set("v.toastIcon",'warning');
        	component.set("v.toastcss",'slds-notify slds-notify_toast slds-theme_warning');
        }
        else{
            component.set("v.toastIcon",'info');
        	component.set("v.toastcss",'slds-notify slds-notify_toast slds-theme_info');
        }
        //close after 5 sec
        if(mode==='dismissible' || mode ==='pester')
        {
            window.setTimeout(function(){
                helper.helperMethod(component, event, helper);
            },5000);
        }
    },
    //onClick close
    closeIcon : function(component, event, helper) {
        helper.helperMethod(component, event, helper);
    }
})