({//This function loads the pick list values.
    loadValues : function(component, event, helper) {
        component.set("v.contacts.AccountId",null)
        component.set("v.accTypeValue","Field Sales(FS)");
        let loadPickList=component.get("c.getPickListValuesAccType");
        loadPickList.setParams({});
        loadPickList.setCallback(this, function(response){
            let state=response.getState();
            if(state==='SUCCESS')
            {component.set("v.accTypeValues",response.getReturnValue());
            }
        });
        $A.enqueueAction(loadPickList);
    },
    //This function will be called for the SAM manager look up
    showManager:function(component,helper,event)
    {let type=component.get("v.accTypeValuesSelect");
     let selectedValue=component.find("state_opt").get("v.value");
     component.set("v.accTypeValue",type);
     if(selectedValue==='SAM/SAM Dealer'){
         component.set("v.showLook","show");
     }
     else{component.set("v.showLook","notshow");}
    },
    //This function will be called radio buttton 2 selection
    displayAccount:function(component,helper,event)
    {   
        component.set("v.showAccount","show");
        component.set("v.showPayer","NoShow");
        component.set("v.validatinAccCheck","show");
        component.set("v.validationPayer","notshow");
        component.set("v.radio2","notshow");
        component.set("v.radio1","notshow");
        component.set("v.radio3","notshow");
        component.set("v.radio4","notshow");
        component.set("v.radio5","show");
        component.set("v.radio6","notshow");
        component.set("v.radio7","notshow");},
    //This function will be called radio buttton 1 selection
    displayAc:function(component,helper,event)
    { 
     component.set("v.radio1","show");
     component.set("v.radio2","notshow");
     component.set("v.radio3","notshow");
     component.set("v.radio4","notshow");
     component.set("v.radio5","notshow");
     component.set("v.radio6","notshow");
     component.set("v.radio7","notshow");
     component.set("v.showAccount","NoShow");
     component.set("v.showPayer","NoShow");
     component.set("v.validatinAccCheck","notshow");
     component.set("v.validationPayer","notshow");},
    //This function will be called radio buttton 3 selection
    displayAcc:function(component,helper,event)
    {  
        component.set("v.showAccount","NoShow");
        component.set("v.showPayer","NoShow");
        component.set("v.validatinAccCheck","notshow");
        component.set("v.validationPayer","notshow");
        component.set("v.radio3","notshow");
        component.set("v.radio2","notshow");
        component.set("v.radio1","notshow");
        component.set("v.radio4","show");
        component.set("v.radio5","notshow");
        component.set("v.radio6","notshow");
        component.set("v.radio7","notshow");
    },
    //This function will be called radio buttton 4 selection
    displayAcco:function(component,helper,event)
    {  
        component.set("v.showAccount","NoShow");
        component.set("v.showPayer","NoShow");
        component.set("v.radio4","notshow");
        component.set("v.validatinAccCheck","notshow");
        component.set("v.validationPayer","notshow");
        component.set("v.radio1","notshow");
        component.set("v.radio2","notshow");
        component.set("v.radio3","show");
        component.set("v.radio5","notshow");
        component.set("v.radio6","notshow");
        component.set("v.radio7","notshow");
    },
    //This function will be called radio buttton 5 selection
    displayAccou:function(component,helper,event)
    {  
        component.set("v.showAccount","NoShow");
        component.set("v.showPayer","NoShow");
        component.set("v.validatinAccCheck","notshow");
        component.set("v.validationPayer","notshow");
        component.set("v.radio5","notshow");
        component.set("v.radio1","notshow");
        component.set("v.radio2","show");
        component.set("v.radio3","notshow");
        component.set("v.radio4","notshow");
        component.set("v.radio6","notshow");
        component.set("v.radio7","notshow");
    },
    //This function will be called radio buttton 6 selection
    displayAccoun:function(component,helper,event)
    {   
        component.set("v.showAccount","NoShow");
        component.set("v.showPayer","show");
        component.set("v.validationPayer","show");
        component.set("v.validatinAccCheck","notshow");
        component.set("v.radio6","show");
        component.set("v.radio1","notshow");
        component.set("v.radio2","notshow");
        component.set("v.radio3","notshow");
        component.set("v.radio4","notshow");
        component.set("v.radio5","notshow");
        component.set("v.radio7","notshow");
    },
    //This function will be called radio buttton 7 selection
    displayAccounts:function(component,helper,event)
    {   
        component.set("v.showAccount","NoShow");
        component.set("v.showPayer","show");
        component.set("v.validationPayer","show");
        component.set("v.validatinAccCheck","notshow");
        component.set("v.radio7","show");
        component.set("v.radio1","notshow");
        component.set("v.radio2","notshow");
        component.set("v.radio3","notshow");
        component.set("v.radio4","notshow");
        component.set("v.radio6","notshow");
        component.set("v.radio5","notshow");
    },
    //This function  will be called when user clicks the continue button
    continueBut:function(component,helper,event)
    {
        let errorMessage;
        if($A.get("$Locale.language")==='en')
        {
            errorMessage='Error';
        }
        else
        {
            errorMessage="エラー:";
        }
        let conObj = component.get("v.cont");
        conObj.AccountId = null ;
        if(component.get("v.selectedLookUpRecord").Id !== undefined){
            conObj.AccountId = component.get("v.selectedLookUpRecord").Id;
        }
        let billValue=conObj.AccountId;
        let loadPayerName=component.get("c.payerUpdatedValues");
        loadPayerName.setParams({payerId:billValue});
        loadPayerName.setCallback(this, function(response){
            let state=response.getState();
            if(state==='SUCCESS')
            {
                component.set("v.billNameValue",response.getReturnValue());
            }
        });
        $A.enqueueAction(loadPayerName);
        let selectedValue=component.find("state_opt").get("v.value");
        if(selectedValue==='SAM/SAM Dealer'){
            component.set("v.showLook","show");}
        else{
            component.set("v.showLook","notshow");}
        let PayerAccount = component.get("v.selAccountPayer.val");
        let billAccount =component.get("v.selAccount.val");
        let validCheck=component.get("v.validatinAccCheck");
        let continueCheck="true";
        if(validCheck==='show')  {
            if(PayerAccount==='' || PayerAccount===null)
            { continueCheck="false";
             let toastEvent = $A.get("e.force:showToast");
             let payermessages;
             if($A.get("$Locale.language")==='en')
             {
                 payermessages="Payer Account: You must enter a value";
             }
             else
             {
                 payermessages="支払人: 値を入力してください";
             }
             toastEvent.setParams({
                 title : errorMessage,
                 message: payermessages,
                 messageTemplate: 'Record {0} created! See it {1}!',
                 duration:'100',
                 key: 'info_alt',
                 type: 'error',
                 mode: 'sticky',
             });
             toastEvent.fire();
            }
            if(billAccount==='' || billAccount===null)
            {
                continueCheck="false";
                let toastEvent = $A.get("e.force:showToast");
                let billMessage;
                if($A.get("$Locale.language")==='en')
                {
                    billMessage="Bill Account:You must enter a value";
                }
                else
                {
                    billMessage="請求先: 値を入力してください";
                }
                toastEvent.setParams({
                    title : errorMessage,
                    message: billMessage,
                    messageTemplate: 'Record {0} created! See it {1}!',
                    duration:'100',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'sticky',
                });
                toastEvent.fire();
            }
        }
        let validCheckPayer=component.get("v.validationPayer");
        if(validCheckPayer==='show')  {
            if(PayerAccount==='' || PayerAccount===null)
            { continueCheck="false";
             component.set("v.messageBill",null);
             let toastEvent = $A.get("e.force:showToast");
             let payerMessage;
             if($A.get("$Locale.language")==='en')
             {
                 payerMessage="Payer Account: You must enter a value";
             }
             else
             {
                 payerMessage="支払人: 値を入力してください";
             }
             toastEvent.setParams({
                 title : errorMessage,
                 message: payerMessage,
                 messageTemplate: 'Record {0} created! See it {1}!',
                 duration:'100',
                 key: 'info_alt',
                 type: 'error',
                 mode: 'sticky',
             });
             toastEvent.fire();
            }
        }
        let selectedValue1=component.find("state_opt").get("v.value");
        if(selectedValue1==='SAM/SAM Dealer'){
            component.set("v.showLook","show");}
        else{
            component.set("v.showLook","notshow");}
        let SamManger=component.get("v.showLook");
        let samValue=component.get("v.use.JJ_JPN_SamManager__c");
        if(SamManger==='show')
        {if(samValue==='' || samValue===null)
        {continueCheck="false";
         let toastEvent = $A.get("e.force:showToast");
         let samemessages;
         if($A.get("$Locale.language")==='en'){
             samemessages="SAM Manager: You must enter a value";
         }else
         {
             samemessages="SAMマネージャー: 値を入力してください";
         }
         toastEvent.setParams({
             title : errorMessage,
             message:samemessages,
             messageTemplate: 'Record {0} created! See it {1}!',
             duration:'100',
             key: 'info_alt',
             type: 'error',
             mode: 'sticky',
         });
         toastEvent.fire();
        }
        }
        let validationradio1=component.get("v.radio1");
        let validationradio2=component.get("v.radio2");
        let validationradio3=component.get("v.radio3");
        let validationradio4=component.get("v.radio4");
        let validationradio5=component.get("v.radio5");
        let validationradio6=component.get("v.radio6");
        let validationradio7=component.get("v.radio7");
        if(typeof  validationradio1==="undefined" && typeof validationradio2==="undefined" && typeof  validationradio3==="undefined" &&
           typeof  validationradio4==="undefined" && typeof validationradio5==="undefined" &&typeof validationradio6==="undefined" &&
           typeof  validationradio7==="undefined")
        {
            continueCheck="false";
            let toastEvent1 = $A.get("e.force:showToast");
            toastEvent1.setParams({
                title : errorMessage,
                message: "Unknown Error: Please select any of one radio button",
                messageTemplate: 'Record {0} created! See it {1}!',
                duration:'100',
                key: 'info_alt',
                type: 'error',
                mode: 'sticky',
            });
            toastEvent1.fire();
        }
        if(continueCheck==="true"){
            component.set("v.showbdyRegist","notShow");
            component.get("v.showbdyRegist","notShow");
            $A.createComponent(
                "c:SW_CustomerNewRegistration_LEX",
                { "editRadio1":component.get("v.radio1"),
                 "editRadio2":component.get("v.radio2"),
                 "editRadio3":component.get("v.radio3"),
                 "editRadio4":component.get("v.radio4"),
                 "editRadio5":component.get("v.radio5"),
                 "editRadio6":component.get("v.radio6"),
                 "editRadio7":component.get("v.radio7"),
                 "PayeeName":component.get("v.payeeNameValue"),
                 "payeeId" :component.get("v.selAccountPayer.val"),
                 "accValueType":component.get("v.accTypeValue"),
                 "userrrNam":component.get("v.userNam"),
                 "BillToIdvalue": component.get("v.biiToId")
                },function(newComp)
                {let bodyComp=component.get("v.body");
                 bodyComp.push(newComp);
                 component.set("v.body",bodyComp);
                 component.set("v.showbdy","show");
                } )
        } },
    //This function  will be called when user clicks cancel button.
    cancelBut:function(component,helper,event)
    {
        let navEvent = $A.get("e.force:navigateToObjectHome");
        navEvent.setParams({
            "scope": "JJ_JPN_CustomerMasterRequest__c"
        });
        navEvent.fire();
    },
    //This function  will be called when user changes payer account look up
    payerUpdated:function(component,helper,event)
    {let payerValue=component.get("v.contacts.AccountId");
     let loadPayerName=component.get("c.payerUpdatedValues");
     loadPayerName.setParams({payerId:payerValue});
     loadPayerName.setCallback(this, function(response){
         let state=response.getState();
         if(state==='SUCCESS')
         {
             component.set("v.payeeNameValue",response.getReturnValue());
         }
     });
     $A.enqueueAction(loadPayerName);    },
    //This function  will be called when user changes Bill To account look up
    accountUpdated:function(component,helper,event)
    {
        let billValue=component.get("v.contacts.AccountId");
        let loadPayerName=component.get("c.payerUpdatedValues");
        loadPayerName.setParams({payerId:billValue});
        loadPayerName.setCallback(this, function(response){
            let state=response.getState();
            if(state==='SUCCESS')
            {
                component.set("v.billNameValue",response.getReturnValue());
            }
        });
        $A.enqueueAction(loadPayerName);
    },
    //This function will be called when user changes SAM Manager look up
    SamUpdate:function(component,helper,event)
    { let userValue=component.get("v.use.JJ_JPN_SamManager__c");
     let userLoad=component.get("c.uerValues");
     userLoad.setParams({userIdValue:userValue});
     userLoad.setCallback(this, function(response){
         let state=response.getState();
         if(state==='SUCCESS')
         {
             component.set("v.userNam",component.get("v.use.JJ_JPN_SamManager__c"));
         }
     });
     $A.enqueueAction(userLoad);
    },
    handleAccountChange: function(component, event, helper) {
        if(component.get("v.selAccount.val") != null) {
            let accId = component.get("v.selAccount.val");
            component.set("v.biiToId",accId);
            let loadPayerName=component.get("c.payerUpdatedValues");
            loadPayerName.setParams({payerId:accId});
            loadPayerName.setCallback(this, function(response){
                let state=response.getState();
                if(state==='SUCCESS')
                {
                    component.set("v.billNameValue",response.getReturnValue());
                }
            });
            $A.enqueueAction(loadPayerName);
        } else if(component.get("v.selAccount.val") == null && component.get("v.selContact.val") != null) {
            let conId = component.get("v.selContact.val");
            helper.getLookupFilter(component, 'Contact', conId);
        } else {
            component.set("v.AcclkupFilterArr", []);
            component.set("v.ContlkupFilterArr", []);
        }
    },
    handleAccountPayerChange: function(component, event, helper) {
        if(component.get("v.selAccountPayer.val") != null) {
            let loadPayerName=component.get("c.payerUpdatedValues");
            loadPayerName.setParams({payerId:component.get("v.selAccountPayer.val")});
            loadPayerName.setCallback(this, function(response){
                let state=response.getState();
                if(state==='SUCCESS')
                {
                    component.set("v.payeeNameValue",response.getReturnValue());
                }
            });
            $A.enqueueAction(loadPayerName);
        }
        else {
            component.set("v.AcclkupFilterArr", []);
            component.set("v.ContlkupFilterArr", []);
        }
    }
})