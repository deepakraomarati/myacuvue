({//This function will be called to populate the account field values
    accValues : function(component, event, helper) {
        let accData=component.get("c.autoPopulateAccValues");
        component.set("v.showbdyNew","show");
        accData.setParams({accid:component.get("v.payeeId")});
        accData.setCallback(this,function(response)
                            {
                                let state=response.getState();
                                if(state==='SUCCESS')
                                {
                                    component.set("v.listAcc",response.getReturnValue());
                                    let accFieldValues=response.getReturnValue();
                                    component.find("tele").set("v.value",accFieldValues.Phone);
                                    component.find("fax").set("v.value",accFieldValues.Fax);
                                    component.find("payName").set("v.value",accFieldValues.Name);
                                    component.find("town").set("v.value",accFieldValues.Address2__c);
                                    component.find("street").set("v.value",accFieldValues.Address1__c);
                                    component.find("Namekana").set("v.value",accFieldValues.JJ_JPN_CustomerNameKana__c);
                                    component.find("other").set("v.value",accFieldValues.JJ_JPN_OtherAddress__c);
                                    component.find("postal").set("v.value",accFieldValues.ShippingPostalCode);
                                    component.find("state").set("v.value",accFieldValues.Address3__c);
                                    
                                    
                                    component.set("v.custom.JJ_JPN_PaymentCondition__c",accFieldValues.payment_terms__c);
                                    component.set("v.custom.JJ_JPN_PaymentMethod__c",accFieldValues.JJ_JPN_PaymentMethod__c);
                                    component.set("v.custom.JJ_JPN_DeliveryDocumentNote__c",accFieldValues.JJ_JPN_DeliveryDocumentNote__c);
                                    component.set("v.custom.JJ_JPN_BillToRTNFAX__c",accFieldValues.JJ_JPN_BillToRTNFAXHowmanytimestosend__c);
                                    component.set("v.custom.JJ_JPN_EDIFlag__c",accFieldValues.JJ_JPN_EDI_Flag__c);
                                    component.set("v.custom.JJ_JPN_FAXOrder__c",accFieldValues.JJ_JPN_FAXOrder__c);
                                    component.set("v.custom.JJ_JPN_CustomerOrderNoRequired__c",accFieldValues.JJ_JPN_PurchaseOrderNumberRequired__c);
                                    component.set("v.custom.JJ_JPN_CustomerOrderNoNumberofDigits__c",accFieldValues.JJ_JPN_CustomerOrderNoNumberofDigits__c);
                                    component.set("v.custom.JJ_JPN_ReturnedGoodsOrderNoInDigits__c",accFieldValues.JJ_JPN_DSO__c);
                                    component.set("v.custom.JJ_JPN_DirectDelivery__c",accFieldValues.JJ_JPN_DirectDeliveryReturned__c);
                                    component.set("v.custom.JJ_JPN_TransactionDetailSubmission__c",accFieldValues.JJ_JPN_TransactionDetailSubmission__c);              
                                    component.set("v.custom.JJ_JPN_SubCustomerGroup__c",accFieldValues.JJ_JPN_SubCustomerGroup__c);
                                    component.set("v.custom.JJ_JPN_FestivalDelivery__c",accFieldValues.JJ_JPN_FestivalDelivery__c);
                                    component.set("v.custom.JJ_JPN_EDIStoreCode__c",accFieldValues.JJ_JPN_EDIStoreCode__c);
                                    component.set("v.custom.JJ_JPN_BillSubmission__c",accFieldValues.JJ_JPN_BillSubmission__c);
                                    component.set("v.custom.JJ_JPN_ItemizedBilling__c",'1');
                                    component.set("v.custom.JJ_JPN_SalesItem__c" , 'blank');
                                    
                                    if(accFieldValues.JJ_JPN_ADSHandlingFlag__c!=''){
                                        component.set("v.custom.JJ_JPN_ADSHandlingFlag__c",'2');   
                                    }
                                }    
                            })
        $A.enqueueAction(accData);
        let accBillData=component.get("c.autoPopulateAccBillValues");
        accBillData.setParams({accid:component.get("v.BillToIdvalue")});
        accBillData.setCallback(this,function(response)
                                {let state=response.getState();
                                 if(state==='SUCCESS')
                                 { component.set("v.listAccBill",response.getReturnValue());
                                  let accValesList=response.getReturnValue();
                                  component.find("tele1").set("v.value",accValesList.Phone);
                                  component.find("fax1").set("v.value",accValesList.Fax);
                                  component.find("billName").set("v.value",accValesList.JJ_JPN_BillToNameKanji__c);
                                  component.find("town1").set("v.value",accValesList.Address2__c);
                                  component.find("street1").set("v.value",accValesList.Address1__c);
                                  component.find("Namekana1").set("v.value",accValesList.JJ_JPN_CustomerNameKana__c);
                                  component.find("other1").set("v.value",accValesList.JJ_JPN_OtherAddress__c);
                                  component.find("postal1").set("v.value",accValesList.ShippingPostalCode);
                                  component.find("state1").set("v.value",accValesList.Address3__c);
                                  
                                  component.set("v.custom.JJ_JPN_BillToCode__c ", accValesList.JJ_JPN_BillToCode__c);
                                  component.set("v.custom.JJ_JPN_BillToNameKanji__c ", accValesList.JJ_JPN_BillToNameKanji__c);
                                  component.set("v.custom.JJ_JPN_BillToNameKana__c ", accValesList.JJ_JPN_CustomerNameKana__c);
                                  component.set("v.custom.JJ_JPN_BillToState__c ",accValesList.Address3__c);
                                  component.set("v.custom.JJ_JPN_BillToPostalCode__c ", accValesList.ShippingPostalCode);
                                  component.set("v.custom.JJ_JPN_BillToTownshipCity__c ", accValesList.Address2__c);
                                  component.set("v.custom.JJ_JPN_BillToStreet__c ", accValesList.Address1__c);
                                  component.set("v.custom.JJ_JPN_BillToOtherAddress__c ", accValesList.JJ_JPN_OtherAddress__c);

                                  
                                 }    })
        $A.enqueueAction(accBillData);
        let getAcc=component.get("c.autoPopulateValues");
        getAcc.setParams({radiobut1:component.get("v.editRadio1"),
                          radiobut2:component.get("v.editRadio2"),
                          radiobut3:component.get("v.editRadio3"),
                          radiobut4:component.get("v.editRadio4"),
                          radiobut5:component.get("v.editRadio5"),
                          radiobut6:component.get("v.editRadio6"),
                          radiobut7:component.get("v.editRadio7"),
                          payId:component.get("v.payeeId"),
                          billToId:component.get("v.BillToIdvalue")//added by sourabh to fix bill to code issue 
                         });
        getAcc.setCallback(this,function(response)
                           {let state=response.getState();
                            if(state==='SUCCESS')
                            {
                                component.set("v.wrapperPayeeList",response.getReturnValue());
                            }
                           })
        $A.enqueueAction(getAcc);
    },
    //This function will be called when user clicks enter key
    saveEnterCustomer:function(component, event, helper)
    {
        if(event.keyCode == 13)
            helper.helpersaveCustomer(component, event, helper);
    },
    //This function will be called when user clicks save button
    saveCustomer:function(component, event, helper)
    {
       helper.helpersaveCustomer(component, event, helper);
    },
    //This function will be called  when user clicks cancel button
    handleCancel:function(component, event, helper)
    {
        helper.cancelButton(component);
    },
    doneRendering:function(component, event, helper){
        for(let i=0; i<document.getElementsByClassName("slds-form").length;i++){
            document.getElementsByClassName("slds-form")[i].addEventListener("submit", function(e) {
                e.preventDefault();
                return false;
        });
        }
    }
})