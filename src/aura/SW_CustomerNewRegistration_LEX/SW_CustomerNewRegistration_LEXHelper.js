({
    //This function will be called when user clicks save button.
    //savecustomerdata method to check which radio button is selected
    saveCustomerData : function(component,event) {
        let radioValue = '';
        //Set radio buttons
        if(component.get("v.editRadio1")==='show'){
            radioValue='radio1';
        }
        else if(component.get("v.editRadio2")==='show'){
            radioValue='radio2';
        }
            else if(component.get("v.editRadio3")==='show'){
                radioValue='radio3';
            }
                else if(component.get("v.editRadio4")==='show'){
                    radioValue='radio4';
                }
                    else if(component.get("v.editRadio5")==='show'){
                        radioValue='radio5';
                    }
                        else if(component.get("v.editRadio6")==='show'){
                            radioValue='radio6';
                        }
                            else{
                                radioValue='radio7';
                            }
        let test;
        //Capture the acctype values
        if(component.get("v.accValueType")!==null && component.get("v.accValueType")!==''){
            test=component.get("v.accValueType").toString();
        }
        //Invoke apex method
        let saveAction=component.get("c.save");
        saveAction.setParams({cusdata:component.get("v.custom"),
                              payerId:component.find("payerco").get("v.value"),
                              billId:component.find("billCode").get("v.value"),
                              solId:component.find("soldCode").get("v.value"),
                              radVal:radioValue,
                              samName:component.get("v.userrrNam"),
                              typeAcc:test
                             });
        saveAction.setCallback(this,function(response)
                               {
                                   let state=response.getState();
                                   //check if apex repsonse returned is success or not
                                   if(state==='SUCCESS')
                                   {
                                       let testing ='';
                                       for(let i=0 ; i<response.getReturnValue().length;i++){
                                           testing = testing +response.getReturnValue()[i].value+'\n';
                                       }
                                       if(response.getReturnValue().length > 0 && response.getReturnValue().length!=null){
                                           if(response.getReturnValue()[0].type==='SUCCESS')
                                           {
                                               //Navigate to Sobject
                                               let event = $A.get( 'e.force:navigateToSObject' );
                                               event.setParams({
                                                   'recordId' : response.getReturnValue()[0].value
                                               }).fire();
                                           }
                                           else{
                                               let toastEvent = $A.get("e.force:showToast");
                                               toastEvent.setParams({
                                                   title : 'Error',
                                                   message: testing,
                                                   messageTemplate: 'Record {0} created! See it {1}!',
                                                   duration:'8000',
                                                   key: 'info_alt',
                                                   type: 'error',
                                                   mode: 'sticky',
                                               });
                                               toastEvent.fire();
                                           }
                                       }
                                   }
                               });
        //enqueue apex action
        $A.enqueueAction(saveAction);
    },
    //This function will be called when user clicks cancel button.
    cancelButton:function(component,event) {
        component.set("v.showbdyNew","notshow");
        component.set("v.showbdy","show");
        //Dynamically create component
        $A.createComponent(
            "c:SW_CustomerNewRegistrationSelect_LEX",{
            },function(comp)
            { 
                let newcomp=component.get("v.body");
                newcomp.push(comp);
                component.set("v.body",newcomp);
            }
        )
        $A.get('e.force:refreshView').fire();
    },
    helpersaveCustomer:function(component,event,helper) {
        let cusdata=component.get("v.custom");
        if(typeof cusdata.JJ_JPN_PayerNameKanji__c !== 'undefined' &&
           typeof cusdata.JJ_JPN_PayerNameKana__c !=='undefined' &&
           cusdata.JJ_JPN_PayerNameKanji__c !=='' && cusdata.JJ_JPN_PayerNameKana__c !='')
        {
            helper.saveCustomerData(component,event);
        }
        else{
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error',
                message: "Please enter payer name",
                messageTemplate: 'Record {0} created! See it {1}!',
                duration:'8000',
                key: 'info_alt',
                type: 'error',
                mode: 'sticky',
            });
            toastEvent.fire();
        }
    }
})