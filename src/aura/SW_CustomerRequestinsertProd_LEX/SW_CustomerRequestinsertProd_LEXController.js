({
    //This function loads the data for the table
    loadData : function(component, event, helper) {
        let productLoad=component.get("c.loadingData");
        productLoad.setParams({});
        productLoad.setCallback(this,function(response)
                                {
                                    component.set("v.productsData",response.getReturnValue());
                                    let  accTargetList = response.getReturnValue();
                                    let maxPage=Math.ceil(accTargetList.length / 20);
                                    if(maxPage<1){
                                        maxPage=1;
                                    }
                                    component.set('v.maxPage', maxPage);
                                    component.set('v.pageNumber',1);
                                    component.set('v.productList', accTargetList.slice(0, 20));
                                })
        $A.enqueueAction(productLoad);
    },
    //this function will be called during pagination.
    renderPage : function(component, event, helper) {
        let records = component.get('v.productsData');
        let pageNumber = component.get('v.pageNumber');
        if(pageNumber !== undefined){
            let pageRecords = records.slice((pageNumber - 1) * 20, pageNumber * 20);
            component.set("v.productList", pageRecords);
        }
    },
    //This function will be called during save.
    handleSave:function(component, event, helper) {
        let prodList = component.get("v.productsData");
        for(let i=0; i<prodList.length; i++) {          
            if(prodList[i].quantitywrap === null || prodList[i].quantitywrap === '') {
                prodList[i].quantitywrap = 0;
            }
        }
        let savingprod=component.get("c.savingProduct");
        savingprod.setParams({
            prodListValues:JSON.stringify(prodList),
            customermasterreqid:component.get("v.recordId")
        })
        savingprod.setCallback(this,function(response)
                               {let state=response.getState();
                                if(state==='SUCCESS')
                                {component.set("v.message",response.getReturnValue());
                                 let validCheck=response.getReturnValue();
                                 if(validCheck==='' || validCheck===null)
                                 {
                                     sforce.one.back(true);
                                 }
                                }
                               })
        $A.enqueueAction(savingprod);
    },
    //This function will be called  during cancel
    handleCancel:function(component,event,helper)
    {
        sforce.one.navigateToSObject(component.get("v.recordId","related"))
    }
})