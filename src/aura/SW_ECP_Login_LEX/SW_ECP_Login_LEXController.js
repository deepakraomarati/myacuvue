({
    doInit : function(component, event, helper){
        // altering button default style
        component.set("v.errorMsg",'');
        setTimeout(function(){
            // call helper method with passing each ui:button aura Id
            helper.removeCls(component,'btnNeutral');
        });
    },
    clickLogin : function(component, event, helper){
        let SAPId=component.find("SAPNumber").get("v.value");
        let phone=component.find("Phone").get("v.value");
        let action=component.get("c.Login");
        action.setParams({ "SAPID":SAPId,
                          "Phone":phone
                         });
        component.set("v.errorMsg",'');
        if(SAPId==='' || phone==='')
        {
            component.set("v.errorMsg",'Fields are required !');
        }
        else
        {
            action.setCallback(this,function(response)
                               {
                                   let result=response.getReturnValue();
                                   if(result.type==='Success')
                                   {
                                       let navigateToCmp=$A.get("e.c:SW_CmpNavigate_LEX");
                                       navigateToCmp.setParams({
                                           "navigate":true,
                                           "ECPLoginID":result.value
                                       });
                                       navigateToCmp.fire();
                                   }
                                   else{
                                       component.set("v.errorMsg",result.value);
                                       component.set("v.SAPID","");
                                       component.set("v.Phone","");
                                   }
                               });
        }
        $A.enqueueAction(action);
    }
})