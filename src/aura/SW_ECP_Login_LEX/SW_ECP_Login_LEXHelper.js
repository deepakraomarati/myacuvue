({
    removeCls : function(component,btnAuraId){
        // get the ui:button component using aura:id
        let button = component.find(btnAuraId);
        // remove default ui:button style class
        $A.util.removeClass(button, "uiButton");
        $A.util.removeClass(button, "uiButton--default");
    }
})