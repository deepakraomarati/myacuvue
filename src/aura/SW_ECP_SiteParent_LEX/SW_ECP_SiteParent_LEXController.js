({
    doInit : function(component, event, helper) {
        let countryCode = component.get("v.countryCode");
        $A.createComponent("c:SW_ECP_Login_LEX", {"country_Code" :countryCode
                                                 },
                           function(newCmp){
                               if (component.isValid()){
                                   component.set('v.body', newCmp);
                               }
                           });
    },
    NavigateComponent : function(component, event, helper){
        component.set("v.body", []);
        let loginId = event.getParam("ECPLoginID");
        let countryCode = component.get("v.countryCode");
        if(event.getParam("navigate")){
            if(countryCode === 'IND'){
                window.open("/INDECP/apex/SW_IND_ECP_Redemption?ECPID="+loginId,"_self");
            }
            else if(countryCode === 'MY'){
                window.open("/MYECPSite/apex/SW_MY_ECP_Redemption?ECPID="+loginId,"_self");
            }
            else{
            	window.open("/ANZECP/apex/SW_ECP_ANZ_Redemption?ECPID="+loginId,"_self");
            }
        }
    }
 })