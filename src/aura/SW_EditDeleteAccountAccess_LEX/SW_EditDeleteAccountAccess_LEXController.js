({
    //init method
    //init method which gets invoked on component load
	doInit : function(component, event, helper) {
		let action = component.get("c.accessDenied");
        action.setCallback ( this, function(a) {
            if (a.getState() ==="SUCCESS")
            {
                let result = a.getReturnValue();
                component.set("v.IsSpinner",false);
                if(result){
                     component.set("v.showComp",true);
                }
                else{
                    component.set("v.errorMsg" , 'Access Denied');
                    component.set("v.showComp",false);
                }
            }
            else
            {
                //access denied if state return not success
                component.set("v.errorMsg" , 'Access Denied');
                component.set("v.showComp",false);
            }
        });
        //invoke apex method
        $A.enqueueAction(action);
	}
})