({
    //init method
    //init method which gets invoked on component load
    doInit : function(component, event, helper)
    {
        let picklistFields = component.get("v.picklistFields");
        helper.fetchPicklistFields(component,picklistFields);
        helper.autopopulate(component,picklistFields);
    },
    //close modal method
    closeModal: function(component, event, helper) {
        let wasDismissed = $A.get("e.force:closeQuickAction");
        wasDismissed.fire();
    },
    doneRendering:function(component, event, helper){
        for(let i=0; i<document.getElementsByClassName("slds-form--stacked").length;i++){
            document.getElementsByClassName("slds-form--stacked")[i].addEventListener("submit", function(e) {
                e.preventDefault();
                return false;
        });
        }
    },
    handleEnterClick:function(component, event, helper){
        if(event.keyCode == 13)
            helper.handleClickHelper(component, event, helper); 
    },
    handleClick : function(component, event, helper)
    {
        helper.handleClickHelper(component, event, helper);
    }
})