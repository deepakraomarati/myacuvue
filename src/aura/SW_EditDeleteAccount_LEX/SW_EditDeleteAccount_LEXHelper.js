({
    //fetchPicklistFields method to fetch and populate dat for picklist fields
    fetchPicklistFields: function(component, picklistFields)
    {
        //all picklist fields
        let taskPicklistFields = ["JJ_JPN_PaymentCondition__c","JJ_JPN_NoDeliveryCharges__c",
                                  "JJ_JPN_SubCustomerGroup__c", "JJ_JPN_IndividualDeliveryPossibleFriday__c","JJ_JPN_ADSHandlingFlag__c",
                                  "JJ_JPN_EDIFlag__c","JJ_JPN_DeadlineTime__c","JJ_JPN_IndividualDeliveryPossibleMonday__c",
                                  "JJ_JPN_ItemizedBilling__c","JJ_JPN_FestivalDelivery__c","JJ_JPN_IndividualDeliveryPossibleSat__c",
                                  "JJ_JPN_PIndividualDeliveryPossibleThur__c","JJ_JPN_IndividualDeliveryPossibleTues__c",
                                  "JJ_JPN_ReturnedGoodsOrderNoInDigits__c","JJ_JPN_CustomerOrderNoNumberofDigits__c",
                                  "JJ_JPN_FAXOrder__c","JJ_JPN_LotNumberCommunicationDocument__c",
                                  "JJ_JPN_TransactionDetailSubmission__c","JJ_JPN_BillToRTNFAX__c",
                                  "JJ_JPN_IndividualDeliveryPossibleDateSun__c","JJ_JPN_IndividualDeliveryPossibleDateWed__c",
                                  "JJ_JPN_CustomerOrderNoRequired__c","JJ_JPN_DeliveryDocumentNote__c","JJ_JPN_BillSubmission__c",
                                  "JJ_JPN_BillingSummaryTableSubmission__c","JJ_JPN_SoldToNumberOfPagesReceived__c",
                                  "JJ_JPN_DirectDelivery__c","JJ_JPN_PaymentMethod__c","JJ_JPN_SalesItem__c",
                                  "JJ_JPN_CustomerRTNFAXHowManyTimesToSend__c", "JJ_JPN_OrderBlock__c"];
        picklistFields['JJ_JPN_CustomerMasterRequest__c'] = taskPicklistFields;
    },
    //getPicklistValues method
    getPicklistValues: function(component, sobjFieldsmap) {
        //get the object
        for (let obj in sobjFieldsmap) {
            if (sobjFieldsmap.hasOwnProperty(obj)) {
                let objName = sobjFieldsmap[obj];
                let field;
                //get fields of object
                for (field in objName) {
                    if (objName.hasOwnProperty(field)) {
                        let optionValues = objName[field];
                        this.buildPicklist(component, obj + "." + field, optionValues);
                    }
                }
            }
        }
    },
    //buildPicklist: Map values in the UI
    buildPicklist: function(component, elementId, optionValues)
    {
        let opts = [];
        if (optionValues !== undefined && optionValues.length > 0)
        {
            for (let i = 0; i < optionValues.length; i++) {
                opts.push({
                    class: "optionClass",
                    value: optionValues[i].value,
                    label: optionValues[i].label
                });
            }
        }
        if(elementId === "JJ_JPN_CustomerMasterRequest__c.JJ_JPN_DeadlineTime__c"){
            component.set("v.deadline" , opts[0].label);
        }
        component.find(elementId).set("v.options", opts);
    },
    //autopopulate: auto populate accoutn related fieds on CMR popup
    autopopulate:function(component,picklistFields)
    {
        //get the record ID of the record
     let accId = component.get("v.recordId");
     let action = component.get("c.autoPopulateAccValues");
      //set parameters to be send to apex class
        action.setParams({
                "acc1" : accId,
                "objpicklistFieldsMap": JSON.stringify(picklistFields)
            });
        action.setCallback(this, function(a){
            // check the return state
            if (a.getState() ==="SUCCESS")
            {
                let result = a.getReturnValue();
                component.set("v.acc", result.CMR);
                this.getPicklistValues(component, result.objFieldPicklistMap);
            }
            else
            {
                $A.log("Errors", a.getError());
            }
        }
                          );
        $A.enqueueAction(action);
    },
    //handleClickHelper method
    handleClickHelper : function(component, event, helper)
    {
        let accId = component.get("v.recordId");
        let acc1 = component.get("v.acc");
        acc1.JJ_JPN_StatusCode__c =component.find("statusCode").get("v.value");
        acc1.JJ_JPN_EDIFlag__c =component.find("JJ_JPN_CustomerMasterRequest__c.JJ_JPN_EDIFlag__c").get("v.value");
        component.set("v.errorMsg", '' );
        let action = component.get("c.doSave");
       //set parameter in dosave method
        action.setParams({
                "CMR" : acc1 ,
                "accId" :accId
            });
        action.setCallback(
            this, function(a){
                if (a.getState() === "SUCCESS")
                {
                    if (a.getReturnValue().type==='Success')
                    {
                        component.set("v.acc", a.getReturnValue());
                        let sObjectEvent = $A.get("e.force:navigateToSObject");
                        sObjectEvent.setParams({
                            "recordId": a.getReturnValue().value,
                            "slideDevName": "related"
                        })
                        sObjectEvent.fire();
                    }
                    else
                    {
                        component.set("v.errorMsg", a.getReturnValue().value);
                    }
                }
                //return state error
                else
                {
                    component.set("v.errorMsg", a.getReturnValue().value);
                }
            }
        );
        $A.enqueueAction(action);
    }
})