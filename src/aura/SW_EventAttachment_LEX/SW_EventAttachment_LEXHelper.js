({
    //Max file size 4.5 MB
    MAX_FILE_SIZE: 4500000,
    //Chunk Max size 750Kb
    CHUNK_SIZE: 750000,
    redeemHelper: function(component, event)
    {
        let fileInput = component.find("fileId").get("v.files");
        let file = fileInput[0];
        let self = this;
        let objFileReader = new FileReader();
        objFileReader.onload = $A.getCallback(function()
                                              {
                                                  let fileContents = objFileReader.result;
                                                  let base64 = 'base64,';
                                                  let dataStart = fileContents.indexOf(base64) + base64.length;
                                                  fileContents = fileContents.substring(dataStart);
                                                  // call the redeemHelperSave method
                                                  self.redeemHelperSave(component, file, fileContents, '', '' , '');
                                              });
        objFileReader.readAsDataURL(file);
    },
    redeemHelperSave: function(component, file, fileContents, startPosition, endPosition, attachId)
    {
        let getchunk = fileContents.substring(0,  Math.min(fileContents.length, 0 + this.CHUNK_SIZE));
        let recordId = component.get("v.recordId");
        let action = component.get("c.uploadAttachment");
        action.setParams({
            base64Data: encodeURIComponent(getchunk),
            fileName: file.name,
            parentId:recordId
        });
        component.set("v.Msg",'');
        action.setCallback(this, function(response)
                           {
                               attachId = response.getReturnValue();
                               let state = response.getState();
                               if (state === "SUCCESS")
                               {
                                   $A.get('e.force:refreshView').fire();
                                   component.set("v.Msg",'File Attached Successfully !');
                               }
                               else
                               {
                                   component.set("v.Msg",'Error !');
                               }
                           });
        $A.enqueueAction(action);
    }
})