({
    //init method,gets invoked on page initiation
	doInit : function(component, event, helper) {
        setTimeout(function() {
            let quickAction = $A.get("e.force:closeQuickAction");
            quickAction.fire();
        }, 4000);
	}
})