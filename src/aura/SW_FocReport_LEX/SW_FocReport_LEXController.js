({
    //This function will load the search results
    //This function loads the data based on the serach parameters
    //this Function will be called dusring initialization.
    doInit : function(component, event, helper) {
        component.set("v.tableValues",true);
        component.set("v.showcomp","show");
        component.set("v.showPdfButt","show");
        helper.serachHandle(component,event);
    },
    //This function will be called when user cliks search button
    handleSearch:function(component,event,helper)
    {  component.set("v.clearing","Notclear");
     helper.serachHandle(component,event);
    },
    //This function will be called when user cliks download button.
    downloadDocument : function(component,event,helper){
        helper.serachHandle(component,event);
        component.set("v.showPdf","show");
        let  pdfUrl;
        let outLet=component.get("v.outletNumb");
        let zone=component.get("v.publicZone");
        let owner=component.get("v.accountOwner");
        let accNam=component.get("v.accName");
        let fromDat=component.get("v.fromDate");
        let toDat=component.get("v.toDate");
        pdfUrl='/apex/SW_TransactionPDF_LEX?AccName='+accNam+'&outnumber='+outLet+'&zonecode='+zone+'&ownerNam='+owner+'&fromDat='+fromDat+'&toDat='+toDat;
        window.open(pdfUrl);
        let focMail=component.get("c.SendPDF");
        focMail.setParams({
            FilterAcNme:accNam,
            FromDat:fromDat,
            ToDat:toDat,
            accNumber:component.get("v.outletNumb"),
            pubzone:component.get("v.publicZone"),
            AccouOwner:owner
        })
        focMail.setCallback(this,function(response) {})
        $A.enqueueAction(focMail);
    },
    //This function will be called when user clicks push to accounts button.
    pushAccounts:function(component,event,helper)
    {let outLet=component.get("v.outletNumb");
     let zone=component.get("v.publicZone");
     let owner=component.get("v.accountOwner");
     let accNam=component.get("v.accName");
     let fromDat=component.get("v.fromDate");
     let toDat=component.get("v.toDate");
     let accPush=component.get("c.pushValuesToAccount");
     accPush.setParams({
         FilterAcNme:accNam,
         FromDat:fromDat,
         ToDat:toDat,
         accNumber:outLet,
         pubzone:zone,
         AccouOwner:owner})
     accPush.setCallback(this,function(response)  {
         let state=response.getState();
         if(state=='SUCCESS')
         { component.set("v.pushAccMessage",response.getReturnValue());
          component.set("v.pushEmailMessage",null);
         } })
     $A.enqueueAction(accPush);
    },
    //This function will be called when user cliks the cancel button.
    clearSearch:function(component,event,helper)
    {  component.set("v.clearing","clear");
        component.set("v.fromDate",null);
        component.set("v.toDate",null);
        helper.serachHandle(component,event);
    },
    //This function will be called when user clicks the send email button.
    handleEmail:function(component,event,helper)
    {  let outLet=component.get("v.outletNumb");
     let zone=component.get("v.publicZone");
     let owner=component.get("v.accountOwner");
     let accNam=component.get("v.accName");
     let fromDat=component.get("v.fromDate");
     let toDat=component.get("v.toDate");
     let focMail=component.get("c.mailSending");
     focMail.setParams({
         FilterAcNme:accNam,
         FromDat:fromDat,
         ToDat:toDat,
         accNumber:component.get("v.outletNumb"),
         pubzone:component.get("v.publicZone"),
         AccouOwner:owner })
     focMail.setCallback(this,function(response)  {
         let state=response.getState();
         if(state=='SUCCESS')
         {component.set("v.pushEmailMessage",response.getReturnValue());
          component.set("v.pushAccMessage",null);
         } })
     $A.enqueueAction(focMail);
    },
})