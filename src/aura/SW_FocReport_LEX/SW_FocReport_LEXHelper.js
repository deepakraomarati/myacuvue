({    //This function will be called when user clicks the search button.
    ////this function will be called from controller.
    //this function loads the data based on the search parameters
    serachHandle : function(component,event) {
        component.set("v.IsSpinner",true);
        let outLet=component.get("v.outletNumb");
        let zone=component.get("v.publicZone");
        let owner=component.get("v.accountOwner");
        let accNam=component.get("v.accName");
        let fromDat=component.get("v.fromDate");
        let toDat=component.get("v.toDate");
        if(component.get("v.clearing")==='clear')
        {
            if(typeof component.get("v.outletNumb") ==='undefined'){outLet=null;}
            if(typeof component.get("v.accountOwner")==='undefined'){owner=null;}
            if(typeof component.get("v.publicZone")==='undefined'){zone=null;}
            if(typeof component.get("v.accName")==='undefined'){  accNam=null;}
            if(typeof component.get("v.fromDate")==='undefined'){ fromDat=null;}
            if(typeof component.get("v.toDate")==='undefined'){  toDat=null;}
        }
        let searchAction=component.get("c.getFOCReport");
        searchAction.setParams({
            accNumber:outLet,
            pubzone:zone,
            AccouOwner:owner,
            FilterAcNme:accNam,
            FromDat:fromDat,
            ToDat:toDat
        });
        searchAction.setCallback(this,function(response)
                                 {
                                     let state=response.getState();
                                     component.set("v.errorMessage" , '');
                                     component.set("v.IsSpinner",false);
                                     try{
                                         component.set("v.tableValues",true)
                                         if(state=='SUCCESS')
                                         {
                                             component.set("v.customersFoc",response.getReturnValue());
                                             component.set("v.resultWrapper",response.getReturnValue().resultWrapper);
                                             let accvaluetest=component.get("v.resultWrapper");
                                             if(accvaluetest.length===0)
                                             {
                                                 component.set("v.showPdfButt","Notshow");
                                             }
                                             else{
                                                 component.set("v.showPdfButt","show");
                                             }
                                         }
                                         else if(state=='ERROR'){
                                             component.set("v.errorMessage" , "Apex heap size too large");
                                         }
                                     }
                                     catch(e){
                                         component.set("v.tableValues",false);
                                     }
                                 });
        $A.enqueueAction(searchAction);
    }
})