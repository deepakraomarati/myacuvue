({
    doInit : function(component, event, helper){
        window.setTimeout(
            $A.getCallback(function() {
                $A.get("e.force:closeQuickAction").fire();
            }), 300
        );
        let action = component.get("c.getRelatedTo");
        let recordId = component.get("v.recordId");
        action.setParams({
            "recordID": recordId
        });
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                let createRecordEvent = $A.get("e.force:createRecord");
                var windowHash = window.location.href;
                let temp=response.getReturnValue();
                let WhoID  = temp[0].WhoId;
                let WhatID  = temp[0].WhatId;
                createRecordEvent.setParams({
                    "entityApiName": 'Task',
                    "defaultFieldValues": {
                        'WhatId': WhatID,
                        'WhoId': WhoID,
                        'Subject': temp[0].Subject
                    },
                    "panelOnDestroyCallback": function(event) {
                        window.location.href = windowHash;
                    }
                });
                createRecordEvent.fire();
            }
        });
        $A.enqueueAction(action);
    }
})