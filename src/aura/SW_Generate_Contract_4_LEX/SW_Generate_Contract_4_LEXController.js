({
    //This function will return todays date and will set account details as well.
    ////This function will return  will return Account details.
   //This function will loda the data
    loadData : function(component, event, helper){   
        try{
            let emailValue;
            let today = new Date();
            let monthDigit = today.getMonth() + 1;
            if (monthDigit <= 9) {
                monthDigit = '0' + monthDigit;
            }
            component.set('v.today', today.getFullYear() + "-" + monthDigit + "-" + today.getDate());
            let accData=component.get("c.accountDetails");
            accData.setParams({"contrctId":component.get("v.recordId")});
            accData.setCallback(this,function(response){
                let state=response.getState();
                if(state==='SUCCESS')
                { component.set("v.accValues",response.getReturnValue());
                 emailValue=component.get("v.accValues");
                }
            })
            $A.enqueueAction(accData);
            let signData=component.get("c.fetchSignature");
            signData.setParams({"contrctId":component.get("v.recordId")});
            signData.setCallback(this,function(response){
                let state=response.getState();
                if(state==='SUCCESS')
                {let imageValue=response.getReturnValue();
                    if(imageValue===null || imageValue==='')
                    {alert('Signature is invalid. Please Review and Sign the contract'); 
                    }else if(emailValue.Send_Email__c===null || emailValue.Send_Email__c==='' || typeof emailValue.Send_Email__c==="undefined")
                    {alert('Email Id is invalid. Please provide valid email ID');   
                    }else
                        {alert('Contract Generated and Emailed Successfully to '+emailValue.Send_Email__c );}
                    component.set("v.prefixURL","/servlet/servlet.FileDownload?file="+imageValue);
                }
            })
            $A.enqueueAction(signData);
        }
        catch (e) {
            component.set("v.errorMsg", 'loadData : ' + e.name +'  '+ e.message);
        }
    },
})