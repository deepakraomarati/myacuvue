({
    doInit : function(component,event,helper)
    {// On component load, use setTimeout() method which is execute after 100 milliseconds
        // call helper method with passing each ui:button aura Id
        setTimeout(function(){
            // call helper method with passing each ui:button aura Id
            helper.removeCls(component,'btnNeutral');
        });
        let ECPID =component.get("v.ECPID");
        let action=component.get("c.Onload");
        action.setParams({ "ECPId":ECPID});
        action.setCallback(this,function(response)
                           {                    
                               let result=response.getReturnValue();
                               component.set("v.StoreName",result);
                           });
        $A.enqueueAction(action);
    },
    redeembutton : function(component, event, helper){
        component.set("v.errorMsg",'');
        let isValid=false;
        let e_Voucher = component.find("eVoucherId").get("v.value");
        let vfile = component.find("fileId").get("v.files");
        let receipt_var = component.find("ReceiptId").get("v.value");
        let FirstBox='';
        let SecondBox='';
        if(document.getElementsByName("FirstBox") && document.getElementsByName("FirstBox")[0])
            FirstBox =  document.getElementsByName("FirstBox")[0].value;
        if(document.getElementsByName("SecondBox") && document.getElementsByName("SecondBox")[0])
            SecondBox =  document.getElementsByName("SecondBox")[0].value;
        if(FirstBox==='0')
        FirstBox='';
        if(SecondBox==='0')
        SecondBox='';
        if (e_Voucher==='')
        {
            component.set("v.errorMsg",'e Voucher field is required\n');
            isValid=true;
        }
        if (receipt_var==='' && (vfile===null || vfile.length === 0))
        {
            let err = 'Please select any one reimbursement method below\n';
            component.set("v.errorMsg",component.get("v.errorMsg")+ err);
            isValid=true;
        }
        if((FirstBox<=1 && SecondBox==='') || (SecondBox<=1 && FirstBox===''))
        {
            if(FirstBox==='' && SecondBox==='')
            {
                let err = 'Please select number of boxes atleast for one product with valid numeric values\n';
                component.set("v.errorMsg",component.get("v.errorMsg")+ err);
                isValid=true;
            }
            else
            {
                let err = 'Offer valid only for minimum purchase of 2 boxes\n';
                component.set("v.errorMsg",component.get("v.errorMsg")+ err);
                isValid=true;
            }
        }
        if(isValid==false)
        {
            helper.redeemHelper(component,event);
        }
    },
    handleFilesChange:function(component,event,helper)
    {
        let fileName='No File Selected..';
        if (event.getSource().get("v.files").length > 0)
        {
            fileName=event.getSource().get("v.files")[0]['name'];
        }
        component.set("v.fileName",fileName);
    }
})