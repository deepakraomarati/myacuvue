({
    removeCls : function(component,btnAuraId) {
        // get the ui:button component using aura:id
        let button = component.find(btnAuraId);
        // remove default ui:button style class
        $A.util.removeClass(button,"uiButton");
        $A.util.removeClass(button,"uiButton--default");
    },
    initHelper : function(component,event,helper) {
        component.set("v.space" , "  ");
        // altering button default style
        let today = new Date();
        component.set("v.CurrentTime",today);
        setTimeout(function(){
            // call helper method with passing each ui:button aura Id
            helper.removeCls(component,'btnNeutral');
        });
        let action = component.get("c.Refresh");
        let login= component.get("v.ECPID")
        action.setParams({"AccountId":login});
        action.setCallback(this, function(response) {
            let result = response.getReturnValue();
            for( let i in result){
                 if (result.hasOwnProperty(i)) {
                let res =result[i];
              
                if(res.DataLabel==='download'){
                    component.set("v.download",res);
                }
                if(res.DataLabel==='vredeem'){
                    component.set("v.vredeem",res);
                }
                if(res.DataLabel==='archived'){
                    component.set("v.archived",res);
                }
                if(res.DataLabel==='conversionRate'){
                    component.set("v.conversionRate",res);
                }
                if(res.DataLabel==='OasysHydraluxe'){
                    component.set("v.OasysHydraluxe",res);
                }
                if(res.DataLabel==='MoistAstigmatism'){
                    component.set("v.MoistAstigmatism",res);
                }
                if(res.DataLabel==='totalbox'){
                    component.set("v.totalbox",res);
                }
                 }}
        });
        $A.enqueueAction(action);
    }
})