({
    doInit : function(component,event,helper)
    {// On component load, use setTimeout() method which is execute after 100 milliseconds
        // call helper method with passing each ui:button aura Id
        setTimeout(function(){
            // call helper method with passing each ui:button aura Id
            helper.removeCls(component,'btnNeutral');
        });
        let ECPID =component.get("v.ECPID");
        let action=component.get("c.Onload");
        action.setParams({ "ECPId":ECPID});
        action.setCallback(this,function(response)
                           {
                               let result=response.getReturnValue();
                               component.set("v.StoreName",result);
                           });
        $A.enqueueAction(action);
    },
    submitbutton : function(component, event, helper){
        component.set("v.errorMsg",'');
        let isValid=false;
        let Name = component.get("v.Name");
        let NRIC = component.find("NRICId").get("v.value");
        let vfile = component.find("fileId").get("v.files");
        let FirstBox='';
        let SecondBox='';
        let ThirdBox='';
        if(document.getElementsByName("FirstBox") && document.getElementsByName("FirstBox")[0])
            FirstBox =  document.getElementsByName("FirstBox")[0].value;
        if(document.getElementsByName("SecondBox") && document.getElementsByName("SecondBox")[0])
            SecondBox =  document.getElementsByName("SecondBox")[0].value;
        if(document.getElementsByName("ThirdBox") && document.getElementsByName("ThirdBox")[0])
            ThirdBox =  document.getElementsByName("ThirdBox")[0].value;
        if(FirstBox==='0')
        FirstBox='';
        if(SecondBox==='0')
        SecondBox='';
        if(ThirdBox==='0')
        ThirdBox='';
        if(Name===''){
            component.set("v.errorMsg",'Name is required\n');
            isValid=true;
        }
        if(NRIC=='')
        {
            let err = 'NRIC is required\n';
            component.set("v.errorMsg",component.get("v.errorMsg")+ err);
            isValid=true;
        }
        if(NRIC!='' && (NRIC.length<4 || NRIC.length>4))
        {
            let err = 'NRIC Length must be exactly 4 characters\n';
            component.set("v.errorMsg",component.get("v.errorMsg")+ err);
            isValid=true;            
        }
        if(vfile===null || vfile==='')
        {
            let err = 'Receipt Image required\n';
            component.set("v.errorMsg",component.get("v.errorMsg")+ err);
            isValid=true;
        }
        if((FirstBox<=1 && SecondBox==='' && ThirdBox==='') || (SecondBox<=1 && FirstBox==='' && ThirdBox==='') || (SecondBox==='' && FirstBox==='' && ThirdBox<=1))
        {
            if(FirstBox==='' && SecondBox==='' && ThirdBox==='')
            {
                let err = 'Please select number of boxes atleast for one product\n';
                component.set("v.errorMsg",component.get("v.errorMsg")+ err);
                isValid=true;
            }
            else
            {
                let err = 'Offer valid only for minimum purchase of 2 boxes\n';
                component.set("v.errorMsg",component.get("v.errorMsg")+ err);
                isValid=true;
            }
        }
        if(isValid===false)
        {
            helper.submitHelper(component,event);
        }
    },
    SelectedOption : function(component, event, helper)
    {
        component.set("v.NRIC",'');
        component.set("v.errorMsg",'');
        let SelOption = event.getParam("SelectedOption");
        if(SelOption !== null){
            let action=component.get("c.setNRIC");
            action.setParams({
                SelectedID:SelOption
            });
            action.setCallback(this,function(response)
                               {let state=response.getState();
                                if (state === "SUCCESS")
                                {
                                    let result = response.getReturnValue();
                                    component.set("v.NRIC",result.Contact.NRIC__c);
                                    component.set("v.Name",result.Contact.Name);
                                }
                                else{
                                    component.set("v.errorMsg",'NRIC is Null for selected Campaign Member');
                                }
                               });
            $A.enqueueAction(action);
        }
        else{
            component.set("v.Name",'');
        }
    },
    handleFilesChange:function(component,event,helper)
    {
        let fileName='No File Selected..';
        if (event.getSource().get("v.files").length > 0)
        {
            fileName=event.getSource().get("v.files")[0]['name'];
        }
        component.set("v.fileName",fileName);
    }
})