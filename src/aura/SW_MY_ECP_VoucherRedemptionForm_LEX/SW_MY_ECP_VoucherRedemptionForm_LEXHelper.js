({
    MAX_FILE_SIZE:10485760,//Max file size 10MB
    CHUNK_SIZE:750000,//Chunk Max size 750Kb
    submitHelper:function(component, event)
    {
        component.set("v.errorMsg",'');
        component.set("v.successMsg",'');
        let fileInput=component.find("fileId").get("v.files");
        // get the first file using array index[0]
        let file=fileInput[0];
        let self=this;
        // check the selected file size, if select file size greter then MAX_FILE_SIZE,
        // then show a alert msg to user.
        if (file.size > self.MAX_FILE_SIZE)
        {
            alert("File size should not exceed 10 MB. Kindly upload file less than or equal to 10 MB");
            component.set("v.fileName",'Error : This file exceeds the maximum size limit of 10MB.');
            return;
        }
        // create a FileReader object
        let objFileReader=new FileReader();
        // set onload function of FileReader object
        objFileReader.onload=$A.getCallback(function()
                                            { 
                                             let fileContents=objFileReader.result;
                                             let base64='base64,';
                                             let dataStart=fileContents.indexOf(base64) + base64.length;
                                             fileContents=fileContents.substring(dataStart);
                                             self.submitHelperSave(component, file, fileContents);
                                            });
        objFileReader.readAsDataURL(file); 
    },
    submitHelperSave: function(component, file, fileContents)
    {
     let Name = component.get("v.Name");
     let NRIC=component.find("NRICId").get("v.value");
     let AccountId = component.get("v.ECPID");
     let FirstBox='';
     let SecondBox='';
     let ThirdBox='';
     if(document.getElementsByName("FirstBox") && document.getElementsByName("FirstBox")[0])
         FirstBox=document.getElementsByName("FirstBox")[0].value;
     if(document.getElementsByName("SecondBox") && document.getElementsByName("SecondBox")[0])
         SecondBox=document.getElementsByName("SecondBox")[0].value;
     if(document.getElementsByName("ThirdBox") && document.getElementsByName("ThirdBox")[0])
         ThirdBox=document.getElementsByName("ThirdBox")[0].value;
     let getchunk=fileContents.substring(0,Math.min(fileContents.length,0+this.CHUNK_SIZE));
     let action=component.get("c.RedeemVoucher");
     action.setParams({
         fileName:file.name,
         base64Data:encodeURIComponent(getchunk),
         contentType:file.type,
         AccountId:AccountId,
         Name:Name,
         NRIC:NRIC,
         Box1:FirstBox,
         Box2:SecondBox,
         Box3:ThirdBox
     });
     action.setCallback(this,function(response)
                        {
                         let state=response.getState();
                         if (state === "SUCCESS")
                         {
                             if (response.getReturnValue().type=='Success'){
                                 component.set("v.successMsg",response.getReturnValue().value);
                                 component.set("v.Name",'');
                                 component.set("v.NRIC",'');
                                 component.set("v.Box1",'');
                                 component.set("v.Box2",'');
                                 component.set("v.Box3",'');
                                 component.set("v.attachFile",'');
                                 component.find("fileId").set("v.files",null);
                                 component.set("v.fileName",'');
                                 document.getElementById("FirstBox").value='';
                                 document.getElementById("SecondBox").value='';
                                 document.getElementById("ThirdBox").value='';
                             }
                             else{
                                 component.set("v.errorMsg",response.getReturnValue().value);
                             }
                         }
                         else{
                             component.set("v.errorMsg",response.getReturnValue().value);
                         }
                        });
     $A.enqueueAction(action);
    },
    removeCls : function(component,btnAuraId){
        // get the ui:button component using aura:id
        let button = component.find(btnAuraId);
        // remove default ui:button style class
        $A.util.removeClass(button, "uiButton");
        $A.util.removeClass(button, "uiButton--default");
    }
})