({
    doInit : function(component, event, helper){
         let action=component.get("c.getyear");
        action.setCallback(this,function(response)
                           {
                               let result=response.getReturnValue();
                               component.set("v.previousyear",result);
                           });
        $A.enqueueAction(action);
       helper.initHelper(component,event,helper);
    },
    handleApplicationEvent : function(component, event, helper) {
       helper.initHelper(component,event,helper);
    },
    updateYear : function(component, event, helper) {
        let selectedOption = component.find("selectedyear").get("v.value");
        helper.initHelper(component,event,helper);
    }
})