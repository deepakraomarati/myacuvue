({
    removeCls : function(component,btnAuraId) {
        // get the ui:button component using aura:id
        let button = component.find(btnAuraId);
        // remove default ui:button style class
        $A.util.removeClass(button,"uiButton");
        $A.util.removeClass(button,"uiButton--default");
    },
    initHelper : function(component,event,helper) {
        component.set("v.space" , "  ");
        // altering button default style
        let today = new Date();
        let yearId = component.find("selectedyear").get("v.value");
        component.set("v.CurrentTime",today);
        setTimeout(function(){
            // call helper method with passing each ui:button aura Id
            helper.removeCls(component,'btnNeutral');
        });
        let action = component.get("c.Refresh");
        let login= component.get("v.ECPID")
        action.setParams({
            "RcvdAccountId":login,
            "selectedyear":yearId
        });
        action.setCallback(this, function(response) {
            let result = response.getReturnValue();
            for( let i in result){
                 if (result.hasOwnProperty(i)) {
                let res =result[i];
                if(res.DataLabel==='download'){
                    component.set("v.download",res);
                }
                if(res.DataLabel==='vredeem'){
                    component.set("v.vredeem",res);
                }
                if(res.DataLabel==='archivedprev'){
                    component.set("v.archivedprev",res);
                }
                if(res.DataLabel==='conversionRate'){
                    component.set("v.conversionRate",res);
                }
                if(res.DataLabel==='trueeye'){
                    component.set("v.trueeye",res);
                }
                if(res.DataLabel==='MoistAstigmatism'){
                    component.set("v.MoistAstigmatism",res);
                }
                if(res.DataLabel==='defineredeemed'){
                    component.set("v.defineredeemed",res);
                }
                if(res.DataLabel==='totalbox'){
                    component.set("v.totalbox",res);
                }
            }
            }
        });
        $A.enqueueAction(action);
    }
})