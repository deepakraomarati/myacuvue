({
    //doInit method
    doInit : function(component, event, helper) {
        helper.getDataHelper(component, event);
    },
    handleRowAction: function (component, event, helper) {
        let action = event.getParam('action');
        let row = event.getParam('row');
      
        let selectedId = row.Id;
   
        switch (action.name) {
            case 'Remove':
                component.set("v.inviteeId", selectedId);
                component.set("v.isOpen", true);
                break;
            default: // default clause should be the last one
                break;
        }
    },
     okModel: function(component, event, helper) {
        let delaction = component.get("c.deleteRecord");
        delaction.setParams({
            EventRecordId : component.get("v.inviteeId")
        });
        delaction.setCallback(this, function(response){
            window.location.reload();
        });
        $A.enqueueAction(delaction);
        component.set("v.isOpen", false);
    },
    closequickAction: function(component, event, helper) {
        component.set("v.isOpen", false);
    }
    
})