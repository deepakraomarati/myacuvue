({
    //getDataHelper method
    getDataHelper : function(component, event) {
        let action = component.get("c.getAllEventAttendees");
        
        //Set the Object parameters and Field Set name
        action.setParams({
            eventRecId : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            let state = response.getState();
            let actions = [
                { label: 'Remove', name: 'Remove' }
            ];
            if(state === 'SUCCESS'){
                component.set("v.mycolumns", [
                    //{label: 'Relation Id', fieldName: 'RelationId', type: 'text'},
                    {label: 'Name', fieldName: 'username', type: 'text'},
                    {label: 'Type', fieldName: 'RelationId', type: 'text'},
					 {label: 'Comments', fieldName: 'response', type: 'text'},
                    { type: 'action', typeAttributes: { rowActions: actions } }
                ]);
                component.set("v.mydata", response.getReturnValue());
            }
        });
        //Invoke apex method
        $A.enqueueAction(action);
}
})