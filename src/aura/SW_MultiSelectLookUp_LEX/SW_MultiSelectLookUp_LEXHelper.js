({
	searchHelper : function(component,event,getInputkeyWord) {
        // call the apex class method
        let action = component.get("c.lookupResults");
        // set param to method
        action.setParams({
            'keyword': getInputkeyWord,
            'excludeList' : component.get("v.lstSelectedRecords")
        });
        // set a callBack
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            let state = response.getState();
            if (state === "SUCCESS") {
                let storeResponse = response.getReturnValue();
                // if storeResponse size is equal 0 ,display No Records Found... message on screen.                }
                if (storeResponse.length === 0) {
                    component.set("v.Message", 'No Records Found...');
                } else {
                    component.set("v.Message", '');
                    // set searchResult list with return value from server.
                }
                component.set("v.listOfSearchRecords", storeResponse);
            }
        });
        // enqueue the Action
        $A.enqueueAction(action);
    },
    addAttendee : function(component,event,helper){
        let eventRecId = component.get("v.recordId");
        let listSelectedItems =  component.get("v.lstSelectedRecords");
        let attendeeIDList = [];
		for (let i = 0; i < listSelectedItems.length; i++) {
	        attendeeIDList.push(listSelectedItems[i].Id);
	     }
        // Initializing the toast event to show toast
            let toastEvent = $A.get('e.force:showToast');
            let createAction = component.get("c.saveAttendeeList");
            createAction.setParams({
                eventRecId: eventRecId,
                attendeeIds: attendeeIDList
            });
        createAction.setCallback(this, function(response) {
            // Getting the state from response
            let state = response.getState();
            if(state === 'SUCCESS') {
                // Getting the response from server
                let dataMap = response.getReturnValue();
                // Checking if the status is success
                if(dataMap.status ==='success') {
                    // Setting the success toast which is dismissable ( vanish on timeout or on clicking X button )
                    toastEvent.setParams({
                        'title': 'Success!',
                        'type': 'success',
                        'mode': 'dismissable',
                        'message': dataMap.message
                    });
                    // Fire success toast event ( Show toast )
                    toastEvent.fire();
                    window.location.reload();
                }
                // Checking if the status is error
                else{
                    // Setting the error toast which is dismissable ( vanish on timeout or on clicking X button )
                    toastEvent.setParams({
                        'title': 'Error!',
                        'type': 'error',
                        'mode': 'dismissable',
                        'message': dataMap.message
                    });
                    // Fire error toast event ( Show toast )
                    toastEvent.fire();
                }
            }
        });
        // Adding the action variable to the global action queue
        $A.enqueueAction(createAction);
    }
})