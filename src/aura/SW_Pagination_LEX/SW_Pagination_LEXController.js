({
    //get previous page when working on pagination
    prevPage: function (component, event, helper) {
        component.set('v.currentPageNumber', Math.max(component.get('v.currentPageNumber') - 1, 1));
    },
    //get next page when working on pagination
    nextPage: function (component, event, helper) {
        component.set('v.currentPageNumber', Math.min(component.get('v.currentPageNumber') + 1, component.get('v.maxPageNumber')));
    },
    //get refreshFirstandLastButtons
    refreshFirstandLastButtons:function (component, event, helper){
        let message = event.getParam("message");
        if(message==='Last'){
            component.set('v.currentPageNumber',component.get('v.maxPageNumber'));
        }
        else{
            component.set('v.currentPageNumber',1);
        }
    },
    First: function (component, event, helper) {
        component.set('v.currentPageNumber',1);},
    Last:function (component, event, helper) {
        component.set('v.currentPageNumber',component.get('v.maxPageNumber'));
    }
})