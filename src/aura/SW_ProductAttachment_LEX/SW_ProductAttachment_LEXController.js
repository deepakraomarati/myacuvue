({
    //doInit method
    doInit : function(component, event, helper) {
        helper.getDataHelper(component, event);
    },
    handleClick : function(component, event, helper) {
        component.set("v.status",true);
    },
    handleApplicationEvent : function(component, event) {
        component.set("v.callEdit", false);
    },
    handleRowAction: function (component, event, helper) {
        let action = event.getParam('action');
        let row = event.getParam('row');
        let cwcRowID = row.Id;
        component.set("v.cwcRowID", cwcRowID);
        switch (action.name) {
            case 'view':
                window.open('/servlet/servlet.FileDownload?file='+cwcRowID);
                break;
            case 'delete':
                component.set("v.isOpen", true);
                break;
            case 'edit':
                component.set("v.callEdit", true);
                break;
            default: // default clause should be the last one
                break;
        }
    },
    handleFilesChange: function(component, event, helper){
        component.set("v.IsSpinner",true);
        component.set("v.errorMsg",'');
        let fileName = 'No File Selected..';
        if (event.getSource().get("v.files").length > 0){
            fileName = event.getSource().get("v.files")[0]['name'];
        }
        component.set("v.fileName", fileName);
        let mydata =component.get("v.mydata");
        let fileInput = component.find("fileId").get("v.files");
        let file = fileInput[0];
        //When fileInput is blank
               if(mydata.length !== 0){
            component.set("v.errorMsg", 'Only one Attachment can be added \n');
            component.set("v.IsSpinner",false);
        }
        //When coupon type selected already exists, or
        else{
            let fr = new FileReader();
            let couponId = component.get("v.recordId");
            fr.onload = $A.getCallback(function() {
                let fileContents = fr.result;
                let base64Mark = 'base64,';
                let dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
                fileContents = fileContents.substring(dataStart);
                helper.upload(component,couponId,file,fileContents,fileName);
            });
            fr.readAsDataURL(file);
        }
    },
    okModel: function(component, event, helper) {
        let delaction = component.get("c.deleteRecord");
        delaction.setParams({
            cwcRowID : component.get("v.cwcRowID")
        });
        delaction.setCallback(this, function(response){
            window.location.reload();
        });
        $A.enqueueAction(delaction);
        component.set("v.isOpen", false);
    },
    onUpload: function(component, event, helper) {
        window.location.reload();
    },
    closequickAction: function(component, event, helper) {
        component.set("v.isOpen", false);
    }
})