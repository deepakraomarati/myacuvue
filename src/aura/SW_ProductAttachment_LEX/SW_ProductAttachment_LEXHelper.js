({
    MAX_FILE_SIZE:26214400,//Max file size 25MB
    //getDataHelper method
    getDataHelper : function(component, event) {
        let action = component.get("c.getAllAttachment");
        let locale = $A.get("$Locale.language");
        //Set the Object parameters and Field Set name
        action.setParams({
            cwcID : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            let state = response.getState();
            let actions =[];
            if(locale === 'en'){
                actions = [
                    { label: 'Edit', name: 'edit' },
                    { label: 'Delete', name: 'delete' },
                    { label: 'View', name: 'view' }
                ];
            }
            else
            {
                actions = [
                    { label: '編集', name: 'edit' },
                    { label: '削除', name: 'delete' },
                    { label: '参照', name: 'view' }
                ];
            }
            if(state === 'SUCCESS'){
                if(locale === 'en'){
                    component.set("v.mycolumns", [
                        {label: 'Type', fieldName: 'ConType', type: 'text'},
                        {label:'Title', fieldName:'titleURL', type: 'url',
                         typeAttributes: { label: { fieldName: 'Title' }, target:'_blank'}},
                        {label: 'Last Modified', fieldName: 'LastModifiedDate', type: 'text'},
                        {label: 'Created By', fieldName: 'CreatedBy', type: 'text'},
                        { type: 'action', typeAttributes: { rowActions: actions } }
                    ]);
                }
                else{
                    component.set("v.mycolumns", [
                        {label: '種別', fieldName: 'ConType', type: 'text'},
                        {label:'タイトル', fieldName:'titleURL', type: 'url',
                         typeAttributes: { label: { fieldName: 'Title' }, target:'_blank'}},
                        {label: '最終更新', fieldName: 'LastModifiedDate', type: 'text'},
                        {label: '作成者', fieldName: 'CreatedBy', type: 'text'},
                        { type: 'action', typeAttributes: { rowActions: actions } }
                    ]);
                }
                component.set("v.mydata", response.getReturnValue());
            }
        });
        //Invoke apex method
        $A.enqueueAction(action);
    },
    upload: function(component,couponId,file,fileContents,fileName) {
        // Defining the action to save contact List ( will call the saveContactList apex controller )
        let locale = $A.get("$Locale.language");
        let self=this;
        if (file.size > self.MAX_FILE_SIZE)
        {
            component.set("v.errorMsg",'');
            if(locale === 'en'){
                component.set("v.errorMsg",'The file is too big. Please specify a file of 25 MB or less.');}            
            else
            { component.set("v.errorMsg",'ファイルが大きすぎます。25MB以下のファイルを指定してください。');}
            component.set("v.IsSpinner",false);
            return;
        }
        let action = component.get("c.uploadAttachment");
        // setting the params to be passed to apex controller
        action.setParams({
            "couponId" :couponId,
            "fileName": fileName,
            "uploadFile": encodeURIComponent(fileContents),
            "fileType": file.type
        });
        // callback action on getting the response from server
        action.setCallback(this, function(response) {
            let state = response.getState();
            if(state === 'SUCCESS'){
                let result = response.getReturnValue();
                if(result.type === 'success'){
                    let locale = $A.get("$Locale.language");
                    component.set("v.IsSpinner",false);
                    component.set("v.status", false);
                    if(locale === 'en')
                    {component.set("v.successMsg" , "Upload Successful !!");}
                    else
                    {component.set("v.successMsg" , "アップロード成功 !!");}
                }
                else{
                    component.set("v.IsSpinner",false);
                    component.set("v.errorMsg", result.value[0]);
                }
            }
            else{
                component.set("v.IsSpinner",false);
                component.set("v.errorMsg", 'Error in getting data');
            }
        });
        $A.enqueueAction(action);
    }
})