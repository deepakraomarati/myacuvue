({
    doInit : function(component, event, helper)
    {
        let action = component.get("c.checkPermission");
        action.setCallback ( this, function(a){
            if (a.getState() ==="SUCCESS"){
                let result = a.getReturnValue();
                if(result){
                    helper.helperMethod(component, event, helper);
                    component.set("v.isOpen", true);
                    component.set("v.message", 'Are you sure want to recall the approval process?');
                }
                else{
                component.set("v.isOpen", false);
                    component.set("v.message", 'permission denied');
                }
            }
        });
        $A.enqueueAction(action);
    },
    okModel: function(component, event, helper) {
        let userId = $A.get("$SObjectType.CurrentUser.Id");
        let permissionSetId = component.get("v.permissionSetId");
        let recordId = component.get("v.recordId");
        let recallApprovalpermasgn = component.get("c.recallApprovalpermasgn");
        recallApprovalpermasgn.setParams({
            permsetid :permissionSetId,
            userid:userId
        });        
        recallApprovalpermasgn.setCallback(this, function(a){
            if (a.getState() ==="SUCCESS"){
                let recallApproval = component.get("c.recallApproval");
                recallApproval.setParams({
                    recId:recordId
                });
                recallApproval.setCallback(this, function(a){
                    if (a.getState() ==="SUCCESS"){
                        if(a.getReturnValue()){
                            component.set("v.message", 'Approval has been recalled');
                            $A.get('e.force:refreshView').fire();
                            component.set("v.isOpen", false);
                            document.location.reload();
                        }
                        else{
                            component.set("v.message", 'Recall Operation denied . Please submit the record for Approval');
                            $A.get('e.force:refreshView').fire();
                            component.set("v.isOpen", false);
                        }
                    }
                });
                $A.enqueueAction(recallApproval);
            }
            let recallApprovalpermdel = component.get("c.recallApprovalpermdel");
            recallApprovalpermdel.setParams({
                permsetid:permissionSetId,
                userid:userId
            });
            recallApprovalpermdel.setCallback(this, function(a){
                if (a.getState() ==="SUCCESS"){
                 
                }
            });
            $A.enqueueAction(recallApprovalpermdel);
        });
        $A.enqueueAction(recallApprovalpermasgn);
    },
    closequickAction: function(component, event, helper) {
        let wasDismissed = $A.get("e.force:closeQuickAction");
        wasDismissed.fire();
    }
})