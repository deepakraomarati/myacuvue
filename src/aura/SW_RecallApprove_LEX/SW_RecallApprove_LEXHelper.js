({
    //helperMethod gets invoked from doinit
    helperMethod : function(component, event, helper) {
        let action = component.get("c.permissionSetRecords");
        action.setCallback ( this, function(a) {
            if (a.getState() ==="SUCCESS")
            {
                component.set("v.permissionSetId",a.getReturnValue());
            }
        });
        //Invoke apex method
        $A.enqueueAction(action);
    }
})