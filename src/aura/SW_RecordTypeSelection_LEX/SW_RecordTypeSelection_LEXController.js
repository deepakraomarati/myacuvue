({
    doInit : function(component, event, helper) {
        helper.RecordTypeSelectorController(component);
    },
    clickNext : function (component, event, helper) {
        let rtDet = document.querySelector('input[name="recordTypeRadio"]:checked');
        if(rtDet != null) {
            let componentEvent = component.getEvent("RecordTypeSelectionEvent");
            componentEvent.setParams({
                "recTypeId": rtDet.id,
                "recTypeLabel":rtDet.dataset.label,
                "recTypeName":rtDet.value
            });
            componentEvent.fire();
        }
        else{
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error Message',
                message: 'Please select Record Type!!',
                duration:'2000',
                type: 'error',
                mode: 'dismissible'
            });
            toastEvent.fire();
        }
    },
    closeModal : function(component, event, helper) {
        let modal = component.find("modalRT");
        let modalBackdrop = component.find("backdrop");
        $A.util.removeClass(modal,"slds-fade-in-open");
        $A.util.removeClass(modalBackdrop,"slds-backdrop-open");
        let componentEvent = component.getEvent("RecordTypeSelectionEvent");
        componentEvent.setParams({
            "recTypeId": null
        });
        componentEvent.fire();
    }
})