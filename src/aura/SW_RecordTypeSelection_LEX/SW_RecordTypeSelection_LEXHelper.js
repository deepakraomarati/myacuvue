({
   RecordTypeSelectorController: function(component) {
    let action = component.get("c.getListOfRecordType");
    action.setCallback(this, function(actionResult) {
        let infos = actionResult.getReturnValue();
        component.set("v.recordTypes", infos);
    });
    $A.enqueueAction(action);
  }
})