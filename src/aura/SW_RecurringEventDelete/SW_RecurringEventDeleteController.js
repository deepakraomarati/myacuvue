({
    doInit:function(component, event, helper) {
        let action = component.get("c.deleteEventtoCheckRecurssion");
        action.setParams({ "eventId" : component.get("v.recordId")});
        action.setCallback(this, function(response){
            let state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.flag", response.getReturnValue());
                component.set("v.message",'It is not a Recurring Event. Hence it cannot be deleted.') 
            }
        });
        $A.enqueueAction(action);
    },
    removeItem: function(component, event, helper) {
        helper.deleteEventRecords(component, event, helper);
    },
    closeModal: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire(); 
    }
})