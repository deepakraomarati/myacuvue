({
    deleteEventRecords : function(component, event, helper) {
        let action = component.get("c.deleteEvent");
        action.setParams({ "eventId" : component.get("v.recordId"),"sObjectName" : 'Event'});
        action.setCallback(this, function(response){
            let state = response.getState();
            if (state === "SUCCESS") {
                var homeEvent = $A.get("e.force:navigateToObjectHome");
                homeEvent.setParams({
                    "scope": "Event"
                });
                homeEvent.fire();
            }
        });
        $A.enqueueAction(action);
    }
})