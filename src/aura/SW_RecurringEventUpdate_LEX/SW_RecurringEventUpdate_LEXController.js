({
    doinit : function(component , event, helper) {
        let options = [
            {'label': $A.get("$Label.c.Daily"), 'value': 'Daily' },
            {'label': $A.get("$Label.c.Weekly"), 'value': 'Weekly' },
            {'label': $A.get("$Label.c.Monthly"), 'value': 'Monthly' },
            {'label': $A.get("$Label.c.Yearly"), 'value': 'Yearly' }
        ];
        component.set('v.options', options);
        let action = component.get("c.onLoadValue");
        action.setParams({ "recordId" : component.get("v.recordId")});
        action.setCallback(this, function(response) {
            let state = response.getState();
            if(state == 'SUCCESS') {
                let retEvent = response.getReturnValue();
                component.set("v.event", retEvent.evt);
                if(typeof retEvent.evt !== "undefined"){
                    let recurStartDate = $A.localizationService.formatDate(retEvent.evt.RecurrenceStartDateTime, "YYYY-MM-DD");
                    let today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
                    if(recurStartDate >=today){
                        component.set("v.showDate",true);
                    }else{
                        component.set("v.showDate",false);
                    }
                    if(retEvent.evt.RecurrenceType === "RecursEveryWeekday" ||
                       retEvent.evt.RecurrenceType === "RecursDaily"){
                        component.set("v.value", "Daily");
                    }
                    else if(retEvent.evt.RecurrenceType === "RecursWeekly"){
                        component.set("v.value", "Weekly");
                        component.set("v.mon" , retEvent.mon);
                        component.set("v.tue" , retEvent.tue);
                        component.set("v.wed" , retEvent.wed);
                        component.set("v.thu" , retEvent.thu);
                        component.set("v.fri" , retEvent.fri);
                        component.set("v.sat" , retEvent.sat);
                        component.set("v.sun" , retEvent.sun);
                    }
                        else if(retEvent.evt.RecurrenceType === "RecursMonthly"){
                            component.set("v.value", "Monthly");
                            component.find("everyMonth1").set("v.checked" , true);
                        }
                            else if(retEvent.evt.RecurrenceType === "RecursMonthlyNth"){
                                component.set("v.value", "Monthly");
                                component.find("everyMonth2").set("v.checked" , true);
                                component.set("v.selectedDOWValue" , retEvent.recDayOfWeek);
                            }
                                else if(retEvent.evt.RecurrenceType === "RecursYearly"){
                                    component.set("v.value", "Yearly");
                                    component.find("everyYear1").set("v.checked" , true);
                                }
                                    else if(retEvent.evt.RecurrenceType === "RecursYearlyNth"){
                                        component.set("v.value", "Yearly");
                                        component.find("everyYear2").set("v.checked" , true);
                                        component.set("v.selectedDOYValue" , retEvent.recDayOfWeek);
                                    }
                    helper.helperOnFreqchange(component,event,helper);
                    component.set("v.IsRecurrence", retEvent.IsRecurrence);
                    if(retEvent.evt.RecurrenceType === "RecursEveryWeekday")
                        component.find("everyWeekDay").set("v.checked" , true);
                    else if(retEvent.evt.RecurrenceType === "RecursDaily")
                        component.find("everySomeDay").set("v.checked" , true);
                    component.set("v.messageReceived", "");
                }
                else{
                    component.set("v.messageReceived", retEvent.msg);
                }
            }
        })
        $A.enqueueAction(action);
    },
    close : function(component) {
        let dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    SaveEventRecords : function(component) {
        component.set("v.IsSpinner",true);
        let action = component.get("c.saveRecRecord");
        if(component.get("v.event.RecurrenceType") === "RecursYearlyNth"){
            component.set("v.event.RecurrenceDayOfWeekMask", component.get("v.selectedDOYValue"));
        }
        else if(component.get("v.event.RecurrenceType") === "RecursMonthlyNth"){
            component.set("v.event.RecurrenceDayOfWeekMask", component.get("v.selectedDOWValue"));
        }
        if(component.get("v.event.RecurrenceType") === "RecursWeekly"){
            action.setParams({
                "mon": component.get("v.mon"),
                "tue": component.get("v.tue"),
                "wed": component.get("v.wed"),
                "thu": component.get("v.thu"),
                "fri": component.get("v.fri"),
                "sat": component.get("v.sat"),
                "sun": component.get("v.sun"),
                "evt": component.get("v.event")
            });
        }
        else{
            action.setParams({
                "mon": false,
                "tue": false,
                "wed": false,
                "thu": false,
                "fri": false,
                "sat": false,
                "sun": false,
                "evt": component.get("v.event")
            });
        }
        action.setCallback(this, function(response) {
            component.set("v.errorMsg", '');
            let state = response.getState();
            let result = response.getReturnValue();
            if (state === "SUCCESS"){
                if(result.status === "SUCCESS"){
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type":"Success",
                        "message": "Event updated successfully."
                    });
                    toastEvent.fire();
                    setTimeout(function() {
                        component.set("v.IsSpinner",false);
                        let navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                            "recordId": result.msg
                        });
                        navEvt.fire();
                    }, 500);
                }
                else{
                    component.set("v.errorMsg", result.msg);
                    component.set("v.IsSpinner",false);
                }
            }
        });
        $A.enqueueAction(action);
    },
    onfreqChange:function(component,event,helper){
        helper.helperOnFreqchange(component,event,helper);
    },
    onDailyChange:function(component,event,helper){
        helper.helperonDailyChange(component,event,helper);
    },
    updateWeeklyDay:function(component,event,helper){
        helper.helperonWeeklyViewChange(component,event,helper);
    },
    onMonthChange:function(component,event,helper){
        helper.helperonMonthChange(component,event,helper);
    },
    onYearChange:function(component,event,helper){
        helper.helperonYearChange(component,event,helper);
    },
    onDOWChange:function(component,event,helper){
        helper.helperonDOWChange(component,event,helper);
    },
    onDOYChange:function(component,event,helper){
        helper.helperonDOYChange(component,event,helper);
    }
})