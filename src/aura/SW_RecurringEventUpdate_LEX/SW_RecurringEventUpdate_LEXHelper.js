({
	helperOnFreqchange:function(component,event,helper){
        if(component.get("v.value") === "Weekly"){
            document.getElementById("divWeekly").style.display = "block";
            document.getElementById("divMonthly").style.display = "none";
            document.getElementById("divYearly").style.display = "none";
            document.getElementById("divDaily").style.display = "none";
            component.set("v.event.RecurrenceType","RecursWeekly");
        }
        else if(component.get("v.value") === "Monthly"){
            document.getElementById("divWeekly").style.display = "none";
            document.getElementById("divMonthly").style.display = "block";
            document.getElementById("divYearly").style.display = "none";
            document.getElementById("divDaily").style.display = "none";
            if(component.find("everyMonth1").get("v.checked"))
                component.set("v.event.RecurrenceType","RecursMonthly");
            else
                component.set("v.event.RecurrenceType","RecursMonthlyNth");
            component.set("v.event.RecurrenceDayOfWeekMask",null);
        }
        else if(component.get("v.value") === "Yearly"){
            document.getElementById("divWeekly").style.display = "none";
            document.getElementById("divMonthly").style.display = "none";
            document.getElementById("divYearly").style.display = "block";
            document.getElementById("divDaily").style.display = "none";
            if(component.find("everyYear1").get("v.checked"))
                component.set("v.event.RecurrenceType","RecursYearly");
            else
                component.set("v.event.RecurrenceType","RecursYearlyNth");
            component.set("v.event.RecurrenceDayOfWeekMask",null);
        }
        else{
            document.getElementById("divWeekly").style.display = "none";
            document.getElementById("divMonthly").style.display = "none";
            document.getElementById("divYearly").style.display = "none";
            document.getElementById("divDaily").style.display = "block";
            if(component.find("everyWeekDay").get("v.checked") || 
               component.get("v.event.RecurrenceType") === "RecursEveryWeekday"){
                component.set("v.event.RecurrenceType","RecursEveryWeekday");
                component.set("v.event.RecurrenceDayOfWeekMask",62);
            }
            else{
                component.set("v.event.RecurrenceType","RecursDaily");
            }
        }
    },
     helperonDailyChange:function(component,event,helper){
        if(event.getSource().getLocalId() === "everyWeekDay"){
            component.find("everySomeDay").set("v.checked" , false);
            component.find("everyWeekDay").set("v.checked" , true);
            component.set("v.event.RecurrenceType","RecursEveryWeekday");
            component.set("v.event.RecurrenceDayOfWeekMask",62);
        }
        else if(event.getSource().getLocalId() === "everySomeDay"){
            component.find("everyWeekDay").set("v.checked" , false);
            component.find("everySomeDay").set("v.checked" , true);
            component.set("v.event.RecurrenceType","RecursDaily");
            component.set("v.event.RecurrenceDayOfWeekMask",null);
        }
    },
    helperonWeeklyViewChange:function(component,event,helper){
        if(event.target.id == "mon")
            component.set("v.mon" , event.target.checked);
        if(event.target.id == "tue")
            component.set("v.tue" , event.target.checked);
        if(event.target.id == "wed")
            component.set("v.wed" , event.target.checked);
        if(event.target.id == "thu")
            component.set("v.thu" , event.target.checked);
        if(event.target.id == "fri")
            component.set("v.fri" , event.target.checked);
        if(event.target.id == "sat")
            component.set("v.sat" , event.target.checked);
        if(event.target.id == "sun")
            component.set("v.sun" , event.target.checked);
    },
    helperonMonthChange:function(component,event,helper){
        if(event.getSource().getLocalId() === "everyMonth1"){
            component.find("everyMonth2").set("v.checked" , false);
            component.set("v.event.RecurrenceType","RecursMonthly");
        }
        else if(event.getSource().getLocalId() === "everyMonth2"){
            component.find("everyMonth1").set("v.checked" , false);
            component.set("v.event.RecurrenceType","RecursMonthlyNth");
            component.set("v.event.RecurrenceDayOfWeekMask",component.get("v.selectedDOWValue"));
        }
    },
    helperonYearChange:function(component,event,helper){
        if(event.getSource().getLocalId() === "everyYear1"){
            component.find("everyYear2").set("v.checked" , false);
            component.set("v.event.RecurrenceType","RecursYearly");
        }
        else if(event.getSource().getLocalId() === "everyYear2"){
            component.find("everyYear1").set("v.checked" , false);
            component.set("v.event.RecurrenceType","RecursYearlyNth");
            component.set("v.event.RecurrenceDayOfWeekMask",component.get("v.selectedDOYValue"));
        }
    },
    helperonDOWChange:function(component,event,helper){
        component.set("v.selectedDOWValue" , component.find('RecurrenceDayOfWeekMaskID').get('v.value'));
    },
    helperonDOYChange:function(component,event,helper){
        component.set("v.selectedDOYValue" , component.find('RecurrenceDayOfYearMaskID').get('v.value'));
    }
})