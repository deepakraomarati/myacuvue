({
    //This function will return todays date and will set account details as well.
    //This function will return  will return Account details.
    //This function will loda the data
    loadData : function(component, event, helper){
        try{
            let childComponent = component.find("SignaturePad1");
            childComponent.set("v.recordId",component.get("v.recordId"));
            let today = new Date();
            let monthDigit = today.getMonth() + 1;
            if (monthDigit <= 9) {
                monthDigit = '0' + monthDigit;
            }
            component.set('v.today', today.getFullYear() + "-" + monthDigit + "-" + today.getDate());
            let accData=component.get("c.accountDetails");
            accData.setParams({"contrctId":component.get("v.recordId")});
            accData.setCallback(this,function(response){
                let state=response.getState();
                if(state==='SUCCESS')
                {
                    component.set("v.accValues",response.getReturnValue());
                    childComponent.set("v.accValues",component.get("v.accValues"));
                }
            })
            $A.enqueueAction(accData);
           }
        catch (e) {
            component.set("v.errorMsg", 'loadData : ' + e.name +'  '+ e.message);
        }
    },
    //This function will be called when we click on the save button.
    handleSave:function(component, event, helper)
    {let childComponent = component.find("SignaturePad1");
     childComponent.set("v.recordId",component.get("v.recordId"));
     childComponent.SaveSignature(component, event, helper);
    },
    //This function will be called while  erasing the signature with clear button.
    handleClear:function(component, event, helper)
    {let childComponent = component.find("SignaturePad1");
     childComponent.ClearSignature(component, event, helper);
    }
})