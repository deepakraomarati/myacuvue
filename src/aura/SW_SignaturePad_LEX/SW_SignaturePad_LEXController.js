({//This function will call the helper function to create the signature pad.
    //This function will call the helper for loading the data.
    ////This function will call the helper function to call the apex
    doInit : function(component, event, helper) {
        helper.handleInit(component, event);
    },
    //This function will call the helper function to save the signature
    saveSignatureOnClick : function(component, event, helper,data){
        helper.handleSaveSignature(component, event,helper,component.get("v.recordId"));
    },
    //This function will call the helper function to clear the signature
    clearSignatureOnClick : function(component, event, helper){
        helper.clearCanvas(component, event);
    },
    IsCanvasEmpty: function(component, event, helper){
        return helper.checkCanvasStatus(component, event);
    }
})