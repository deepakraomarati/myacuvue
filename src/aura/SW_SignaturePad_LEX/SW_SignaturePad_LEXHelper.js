({//This function willset the signature pad on the UI
   //This function willset the values for signature pad on the UI
   //This function willset the data for  signature pad on the UI
    handleInit : function(component, event) {
        let canvas = component.find('signature-pad').getElement();
        let ratio = Math.max(window.devicePixelRatio || 1, 1);
        /*canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio;*/
        canvas.width = 378 * ratio;
        canvas.height = 150 * ratio;
        canvas.getContext("2d").scale(ratio, ratio);
        let signaturePad = new SignaturePad(canvas,{
            velocityFilterWeight : 0.1,
            minWidth: .5,
            maxWidth: 1.5,
            dotSize: 2,
            autoResize: true
        });
        component.set("v.signaturePad", signaturePad);
    },
    //This function will call the method from apex to save the signature
    handleSaveSignature : function(component, event,helper,recId) {
        let sig = component.get("v.signaturePad");
        if(!sig._isEmpty){
            let emailValue=component.get("v.accValues");
            let action = component.get("c.uploadSignature");
            action.setParams({
                "contractId": recId,
                "b64SignData": sig.toDataURL().replace(/^data:image\/(png|jpg);base64,/, "")
            });
            action.setCallback(this, function(response) {
                let state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    this.clearCanvas(component, event);
                    let errorReturned = response.getReturnValue();
                    if(errorReturned ===''){
                        let naviUrl=$A.get("e.force:navigateToSObject");
                        naviUrl.setParams({"recordId":recId});
                        naviUrl.fire();
                    } 
                }
               
            });
            $A.enqueueAction(action);
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Success Information',
                message: 'Contract Signed and will be emailed to ('+emailValue.Send_Email__c+'). Please update Email ID if required.',
                messageTemplate: 'Record {0} created! See it {1}!',
                duration:'8000',
                key: 'info_alt',
                type: 'success',
                mode: 'sticky'
            });
            toastEvent.fire();
        }
        else{
            alert("Failed to save the Contract. Please Sign the Contract");
        }
    },
    //This method will clear the signature
    clearCanvas : function(component, event){
        let sig = component.get("v.signaturePad");
        sig.clear();
    },
    //This method will check whether the signature is empty or not
    checkCanvasStatus : function(component, event){
        let data=component.get("v.signaturePad"); 
        return data._isEmpty;
    }
})