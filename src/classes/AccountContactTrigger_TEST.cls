/*
* File Name: AccountContactTrigger_TEST
* Description : Test method to cover business logic around Account Contact object
* Copyright : Johnson & Johnson
* @author : Lhawang Bhutia | lbhutia@its.jnj.com | lhawang.bhutia@cognizant.com
* Modification Log: 
* ================================================================
*    Ver  |Date         |Author                |Modification
*    1.0  |6-Feb-2017   |lbhutia@its.jnj.com   |Class Modified*   
* =============================================================== 
*
*/ 
@IsTest
public class AccountContactTrigger_TEST{
    static testMethod void testmethod1(){
            Test.startTest();
            final Profile userprofile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
            final User userrecord = new User(Alias = 'standt', Email='standarduser@testorg.com',
                                       EmailEncodingKey='UTF-8', LastName='Havish', LanguageLocaleKey='en_US',
                                       LocaleSidKey='en_US', ProfileId = userprofile.Id,
                                       TimeZoneSidKey='America/Los_Angeles', UserName='havish@abc.com',Unique_User_Id__c = 'ABCnr');
            insert userrecord;
            
            system.runas(userrecord){
                final Account testAccount1 = New Account(Name ='Test Account',PublicAddress__c='Bangalore', SalesRep__c = 'ABC', AccountNumber = '12345', OutletNumber__c = '12348', CountryCode__c = 'AUS');
                insert testAccount1;
                
                final Account testAccount2 = New Account(Name ='Test Account1', SalesRep__c = 'ABC', AccountNumber = '12346', OutletNumber__c = '54321', CountryCode__c = 'AUS');
                insert testAccount2;
                
                final Account testAccount3 = New Account(Name ='Test Account1', SalesRep__c = 'ABC', AccountNumber = '12347', OutletNumber__c = '54322', CountryCode__c = 'AUS');
                insert testAccount3;
                
                final Contact testContact = New Contact(FirstName ='Test',LastName ='Contact',AccountId=testAccount1.id, isPrimary__c = TRUE);
                insert testContact;
                
				final Contact testContact1 = New Contact(FirstName ='Test',LastName ='Contact1',AccountId=testAccount2.id);
                insert testContact1;
                
                final Contact testContact3 = New Contact(FirstName ='Test',LastName ='Contact2',AccountId=testAccount3.id, isPrimary__c = TRUE);
                insert testContact3;
                
                testContact1.isPrimary__c = TRUE;
                update testContact1;
                
                delete testContact3;
                
                update testAccount1;
                update testAccount2;
                
                testContact.isPrimary__c = FALSE;
                update testContact1;
                testAccount1.PublicAddress__c='';
                testAccount1.PublicState__c='';
                testAccount1.PublicZone__c='';
                update testAccount1;
                
                final NPS__c npsrecord = new NPS__c(Account__c = testAccount1.Id);
                insert npsrecord;
				system.assertEquals(testAccount3.Name, 'Test Account1','Success');
                Test.stopTest();
            } 
    }    
}