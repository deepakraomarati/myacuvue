/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Get, Upsert and Delete relevent sObject declared in the Lightning and Community Builder
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Randy Grewal   <rgrewal@salesfore.com>
* @modifiedBy     Randy Grewal   <rgrewal@salesfore.com>
* @maintainedBy   Randy Grewal   <rgrewal@salesfore.com>
* @version        1.1
* @created        2017-05-01
* @modified       2017-06-12
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @changes
* v1.1            rgrewal@salesfore.com
* 2017-06-08     	Added option to filter query by logged in user and removed System.debug logs
* 2017-06-12		Security Review Fixes:
*					Added isAccessible(), isCreateable(), isDeletable() for CRUD/FLS Enforcement fix
*					Added Typecasting to all injected field API names for SOQL Injection fix
*					Added "with sharing" keyword to Class for Sharing Violation Fix
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public with sharing class AnyEventCalCtrl {   
    @AuraEnabled
    public static String deleteEvent(String eventId, String sObjectName){
        Event evt=[SELECT Id, RecurrenceActivityId, IsRecurrence FROM Event Where Id =:eventId LIMIT 1];
        String recurrenceActivityId =evt.RecurrenceActivityId;
        String deleteQuery = 'SELECT Id,RecurrenceActivityId ' + 
            + ' FROM ' + 
            sObjectName + ' WHERE RecurrenceActivityId  = \'' + String.escapeSingleQuotes(recurrenceActivityId)  + '\'';
        System.debug('recurrenceActivityId'+recurrenceActivityId);
        System.debug('deleteQuery'+deleteQuery);
        List<sObject> sObjectList = Database.query(deleteQuery);
        try {
            if(isDeletable(sObjectName)) {
                System.debug('sObjectList'+sObjectList.size());
                delete sObjectList;
            }
            return eventId;
        } catch (Exception e) {
            System.debug(e.getMessage());
            return null;
        }
    }
    @AuraEnabled
    public static Boolean deleteEventtoCheckRecurssion(String eventId){
        Event evt=[SELECT Id, RecurrenceActivityId, IsRecurrence FROM Event Where Id =:eventId LIMIT 1];
        System.debug('evtdddddddddd'+evt.RecurrenceActivityId);
        String recurrenceActivityId =evt.RecurrenceActivityId;
        if(String.isEmpty(recurrenceActivityId)){
            return true;
        }
        return false;
    }
    @AuraEnabled  
    public static List<Account> getFilteredAccounts(String filterId){
        String auth_variable = 'Authorization';
        system.debug('------filterId----'+filterId);
        HttpRequest req = new HttpRequest();
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        system.debug('baseUrl'+baseUrl);
        MetadataService.MetadataPort serviceObj= createService();
        String sessionID= serviceObj.SessionHeader.sessionId;
        String endPoinURL = baseUrl+'/services/data/v32.0/sobjects/Account/listviews/'+filterId+'/describe';
        req.setEndpoint(endPoinURL);
        req.setMethod('GET');
        req.setHeader(auth_variable,  'Bearer ' + sessionID);
        Http http = new Http();
        HTTPResponse response = http.send(req);
        Map<String, Object> tokenResponse = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
        String query = (String) tokenResponse.get('query');
        List<Account> AccountList = new List<Account>();
        system.debug('query>>>>>'+ query);
        for(Account accountObj : database.query(query)){
            AccountList.add(accountObj);
        }  
        return AccountList;        
    }
    
    public class EventObj {
        @AuraEnabled
        public String Id {get;set;}
        @AuraEnabled
        public String title {get;set;}
        @AuraEnabled
        public DateTime startDateTime {get;set;}
        @AuraEnabled
        public DateTime endDateTime {get;set;}
        @AuraEnabled
        public String description {get;set;}
        @AuraEnabled
        public String owner {get;set;}
        
        
        public EventObj(String i,String t, DateTime s, DateTime e, String d, String o){
            this.Id = i;
            this.title = t;
            this.startDateTime = s;
            this.endDateTime = e;
            this.description = d;
            this.owner = o;
        }
        
    }
    public static Boolean isAccessible(String sObjectType, String fieldName){
        SObjectType schemaType = Schema.getGlobalDescribe().get(sObjectType);
        Map<String, SObjectField> fields = schemaType.getDescribe().fields.getMap();
        DescribeFieldResult fieldDescribe = fields.get(fieldName).getDescribe();
        return fieldDescribe.isAccessible();
    }
    public static Boolean isAccessible(String sObjectType){
        SObjectType schemaType = Schema.getGlobalDescribe().get(sObjectType);
        return schemaType.getDescribe().isAccessible();
    }
    public static Boolean isCreateable(String sObjectType){
        SObjectType schemaType = Schema.getGlobalDescribe().get(sObjectType);
        return schemaType.getDescribe().isCreateable();
    }
    public static Boolean isDeletable(String sObjectType){
        SObjectType schemaType = Schema.getGlobalDescribe().get(sObjectType);
        return schemaType.getDescribe().isDeletable();
    }
    @AuraEnabled
    public static String initComponent(String objectType, List<String> fields) {
        system.debug('objectType'+objectType);
        system.debug('fields'+fields);
        List<Object> fieldList = new List<Object>();
        
        Map<String,Schema.SObjectType> globalDescribeMap = Schema.getGlobalDescribe();
        if(globalDescribeMap.containsKey(objectType)) {
            Schema.DescribeSObjectResult sobjectDescribeResult = globalDescribeMap.get(objectType).getDescribe();
            Map<String,Schema.SObjectField> fieldMap = sobjectDescribeResult.fields.getMap();
            for(String field : fields) {
                if(fieldMap.containsKey(field)) {
                    Schema.DescribeFieldResult fieldResult = fieldMap.get(field).getDescribe();
                    DataColumn dataColumnNew = new DataColumn(fieldResult.getLabel(), fieldResult.getName());
                    fieldList.add(dataColumnNew);
                }
            }
        }
        
        return JSON.serialize(fieldList);
    }
    
    @AuraEnabled
    public static recordsWrapper fetchRecords(String objectType, List<String> fields, String keyword, String sortField, String sortDir, 
                                              Integer iLimit, String filterId ) {
                                                  String auth_variable = 'Authorization';
                                                  String query = '';
                                                  system.debug('------filterId----'+filterId);      
                                                  //-------------
                                                  HttpRequest req = new HttpRequest();
                                                  String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
                                                  system.debug('baseUrl'+baseUrl);
                                                  MetadataService.MetadataPort serviceObj= createService();
                                                  String sessionID= serviceObj.SessionHeader.sessionId;
                                                  String endPoinURL = baseUrl+'/services/data/v32.0/sobjects/Account/listviews/'+filterId+'/describe';
                                                  req.setEndpoint(endPoinURL);
                                                  req.setMethod('GET');
                                                  req.setHeader(auth_variable,  'Bearer ' + sessionID);
                                                  system.debug(Test.isRunningTest());
                                                  if(Test.isRunningTest()){
                                                      query = 'SELECT ID FROM Account';
                                                  }
                                                  else{
                                                      Http http = new Http();
                                                      HTTPResponse response = http.send(req);
                                                      system.debug(response);
                                                      Map<String, Object> tokenResponse = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                                                      //Modified by Lhawang to resolve sonar qube issue
													  //system.debug(tokenResponse);
                                                      query = (String) tokenResponse.get('query');
                                                      
                                                      query = query.substringBefore('ORDER BY');
                                                  }
                                                  system.debug('query::: ' + query);
                                                  List<Account> AccountList = new List<Account>();
                                                  set<id> AccountSetIDs = new set<id>();
                                                  if (sortField != '') {
                                                      query += ' order by ' + sortField;
                                                      // if isAsc is equal tp ture then set 'asc' order otherwise set 'desc' order.
                                                      if (sortDir=='ASC') {
                                                          query += ' asc';
                                                      } else {
                                                          query += ' desc';
                                                      }
                                                  }
                                                  system.debug('query::: ' + query);
                                                  for(Account accountObj : database.query(query)){
                                                      AccountList.add(accountObj);
                                                  }
                                                  system.debug('AccountList::: ' + AccountList);
                                                  //-----------------
                                                  
                                                  TimeZone tz = UserInfo.getTimeZone();
                                                  System.debug('ID: ' + tz.getID());
                                                  // During daylight saving time for the America/Los_Angeles time zone
                                                  System.debug('Offset: ' + tz.getOffset(system.now()));
                                                  Integer timezoneDiff = tz.getOffset(system.now())/(3600000);
                                                  recordsWrapper recordsWrapperVal = new recordsWrapper(AccountList , timezoneDiff);
                                                  return recordsWrapperVal;
                                              }
    
    public class recordsWrapper{
        @AuraEnabled public List<Account> accList{get;set;}
        @AuraEnabled public integer offset{get;set;}
        public recordsWrapper(List<Account> accList, integer offset){
            this.accList = accList;
            this.offset = offset;
        }
    }
    
    @AuraEnabled
    public static JSON2Apex getListViews() {
        String auth_variable = 'Authorization';
        HttpRequest req = new HttpRequest();
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        system.debug('baseUrlLLLLVVVV'+baseUrl);
        MetadataService.MetadataPort serviceObj= createService();
        String sessionID= serviceObj.SessionHeader.sessionId;
        String endPoinURL = baseUrl+'/services/data/v32.0/sobjects/Account/listviews/';
        req.setEndpoint(endPoinURL);
        req.setMethod('GET');
        req.setHeader(auth_variable,  'Bearer ' + sessionID);
        Http http = new Http();
        HTTPResponse response = http.send(req);
        JSON2Apex jsonObj = JSON2Apex.parse(response.getBody());
        return jsonObj; 
    }
    
    public static MetadataService.MetadataPort createService()
    {
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        //service.SessionHeader.sessionId = UserInfo.getSessionId();
        //system.debug('service.SessionHeader.sessionId'+service.SessionHeader.sessionId);
        service.SessionHeader.sessionId = Utils.getSessionIdFromVFPage(Page.VF_ForSessionID);
        system.debug('service'+service);
        return service;
    }
    
    // 10/1: ListRecord based on view---//
    public class DataColumn {
        public String label { get; set; }
        public String name { get; set; }
        
        public DataColumn(String label, String name) {
            this.label = label;
            this.name = name;
        }
    }
    @AuraEnabled
    public static saveResultWrapper saveEvent(String eventInfo){
        System.debug('eventInfo'+eventInfo);
        Event event=new Event();
        try{
            if(String.isNotEmpty(eventInfo))
                event = (Event)JSON.deserialize(eventInfo, Event.class);
            System.debug('Inside event'+event);
            insert event;
            saveResultWrapper saveResultWrapperData = new saveResultWrapper('SUCCESS',event.Id);
            return saveResultWrapperData;
        }
        catch(DmlException e) {
            System.debug('Inside dml'+e.getTypeName() +e.getStackTraceString());
            //get DML exception message
            saveResultWrapper saveResultWrapperData = new saveResultWrapper('ERROR',e.getDmlMessage(0));
            return saveResultWrapperData;
        }catch(Exception e){
            System.debug('Inside exception'+e.getTypeName() +e.getStackTraceString());
            //get exception message
            saveResultWrapper saveResultWrapperData = new saveResultWrapper('ERROR',e.getMessage());
            return saveResultWrapperData;
        }
    }
    
    public class saveResultWrapper{
        @AuraEnabled String status {get; set;}
        @AuraEnabled String msg {get; set;}
        public saveResultWrapper(String status, String msg){
            this.status = status;
            this.msg = msg;
        }
    }
    @AuraEnabled
    //public static String upsertEvents(String sEventObj){
    public static String upsertEvents(Event sEventObj){
        system.debug('sEventObj :: '+ sEventObj);
        //Event upsertingEvent = (Event)JSON.deserialize(sEventObj, Event.class);
        //system.debug('upsertingEvent :: '+ upsertingEvent);
        try {
            update sEventObj;
            return 'success';
        }
        catch(DmlException e) {
            //get DML exception message
            system.debug('DML :: '+ e);
            return e.getDmlMessage(0);
        }
        catch (Exception e) {
            system.debug('Exception :: '+ e);
            return e.getMessage();
        }
    }
    public class WrapperEvent {
        public String Id { get; set; }
        public WrapperEvent(String Id) {
            this.Id = Id;
        }
    }
    public class EvtWrapper {
        @AuraEnabled public List<EventObj> lstEvent { get; set; }
        public EvtWrapper(List<EventObj> lstEvent) {
            this.lstEvent = lstEvent;
        }
    }
    public class EventObjWrapper {
        @AuraEnabled public Id Id { get; set; }
        @AuraEnabled public Datetime StartDateTime { get; set; }
        @AuraEnabled public Datetime EndDateTime { get; set; }
        @AuraEnabled public String Subject { get; set; }
        @AuraEnabled public Id WhatId { get; set; }
        @AuraEnabled public String WhatName { get; set; }
        @AuraEnabled public Id RecurrenceActivityId { get; set; }
        @AuraEnabled public Integer Offset { get; set; }
        public EventObjWrapper(String Id, Datetime StartDateTime, Datetime EndDateTime, String Subject, 
                               Id WhatId, String WhatName, Id RecurrenceActivityId, Integer Offset) {
                                   this.Id = Id;
                                   this.StartDateTime = StartDateTime;
                                   this.EndDateTime = EndDateTime;
                                   this.Subject = Subject;
                                   this.WhatId = WhatId;
                                   this.WhatName = WhatName;
                                   this.RecurrenceActivityId = RecurrenceActivityId;
                                   this.Offset = Offset;
                               }
    }
    @AuraEnabled
    public static List<EventObjWrapper> getEventsPallete(){
        List<EventObjWrapper> lstEventObjWrapper = new List<EventObjWrapper>();
        TimeZone tz = UserInfo.getTimeZone();
        Integer timezoneDiff;
        for(Event evt: [SELECT Id,EndDateTime,StartDateTime,Subject,What.Name,
                        WhatId, RecurrenceActivityId,IsAllDayEvent FROM Event 
                        WHERE IsRecurrence = false
                        AND OwnerId =: UserInfo.getUserId()]){
                            if(!evt.IsAllDayEvent)
                                timezoneDiff = tz.getOffset(evt.StartDateTime)/(3600000);
                            else timezoneDiff = 0;
                            EventObjWrapper evtObjWrapper = new EventObjWrapper(evt.Id, evt.StartDateTime, evt.EndDateTime,
                                                                                evt.Subject, evt.WhatId, evt.What.Name, 
                                                                                evt.RecurrenceActivityId, timezoneDiff);
                            lstEventObjWrapper.add(evtObjWrapper);
                        }
        return lstEventObjWrapper;
    }
    
    @AuraEnabled
    public static MetaDataWrapper getMetadataValues(String recTypeName, String langLocKey){
        List<Event_RecordType_Setting__c> lstEvtStmdt=new List<Event_RecordType_Setting__c>();
        system.debug(Schema.sObjectType.Event.fields.ActivityType__c.isCreateable());
        lstEvtStmdt =[SELECT Record_Type__c,Field_Api__c,Values__c,Composite_Key__c,Default_Value__c,Label_Value_pairs__c 
                      FROM Event_RecordType_Setting__c
                      WHERE Record_Type__c =: recTypeName
                      AND SW_Language__c =: langLocKey];
        System.debug('lstEvtStmdt'+lstEvtStmdt);
        MetaDataWrapper lstMetaDataWrapper = new MetaDataWrapper(Schema.sObjectType.Event.fields.ActivityType__c.isCreateable() , lstEvtStmdt);
        return lstMetaDataWrapper;
    }
    public class MetaDataWrapper{
        @AuraEnabled public Boolean isAccessible{get;set;}
        @AuraEnabled public List<Event_RecordType_Setting__c> lstEvtRT{get;set;}
        public MetaDataWrapper(Boolean isAccessible , List<Event_RecordType_Setting__c> lstEvtRT){
            this.isAccessible = isAccessible;
            this.lstEvtRT = lstEvtRT;
        }
    }
}