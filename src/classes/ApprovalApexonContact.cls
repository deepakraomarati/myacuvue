/**
* File Name: ApprovalApexonContact
* Description : Class for MA2_ProfileAccessonECP
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | neel.kamal@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |11-Sep-2018  |nkamal8@its.jnj.com  |New Class created
*/
public class ApprovalApexonContact {
	public static void submitapproval(List<Contact> conList){ 
        List<Contact> connList = [select id,MA2_AccessStatus__c from contact where id in : conList];
        for(Integer i = 0; i< connList.size();i++){
            if(connList[i].MA2_AccessStatus__c == 'Pending'){
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Approval Request Pending.');
        req1.setObjectId(connList[i].id);
        req1.setProcessDefinitionNameOrId('MA2_Profile_Access_Approval');
        Approval.ProcessResult result = Approval.process(req1);
            }
        }
    }
}