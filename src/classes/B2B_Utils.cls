/**
 * File Name: B2B_Utils
 * Author: Sathishkumar | BICSGLBOAL
 * Date Last Modified:  18-Oct-2018
 * Description: Common class to support logics in main classes
 * Copyright (c) $2018 Johnson & Johnson
*/
public without sharing class B2B_Utils
{
	/**
	* @description  validate input password during registration and password update
	*/
	public static Boolean IsValidPassword(String Password)
	{
		String inputString = Password;
		String expression_Password = '^.*(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$';
		Pattern myPattern = Pattern.compile(Expression_Password);
		Matcher myMatcher = MyPattern.matcher(InputString);
		return (MyMatcher.matches() ? false : true);
	}

	/**
	* @description  validate input email during user registration
	*/
	public static Boolean IsValidEmailFormat(String email)
	{
		String inputString = email;
		String emailRegex = '([a-zA-Z0-9+_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
		Pattern myPattern = Pattern.compile(emailRegex);
		Matcher myMatcher = MyPattern.matcher(InputString);
		return (MyMatcher.matches() ? false : true);
	}

	/**
	 * @description  Helper method to parse the exception message to JSON format and return to main classes
	*/
	public static String getException(String errCode, String errType, String errDesc)
	{
		JSONGenerator generator = JSON.createGenerator(true);
		generator.writeStartArray();
		generator.writeStartObject();
		generator.writeStringField('errorMessage', errDesc);
		generator.writeStringField('errorCode', errCode);
		generator.writeStringField('errorType', errType);
		generator.writeEndObject();
		generator.writeEndArray();
		return generator.getAsString();
	}

	/**
	 * @description  Helper method to update username, email when user updates FROM profile service
	*/
	@future
	public static void updateUserDetails(String userName, Id userId)
	{
		User UpdateUser = [SELECT Id,Username,Email FROM User WHERE Id = :userId];
		UpdateUser.Username = userName;
		UpdateUser.Email = userName;
		Update UpdateUser;
	}

	/**
	 * @description  Helper method to generate a random text for community nick name field during user registration
	*/
	public static String generateGUID()
	{
		Blob blobbody = Crypto.GenerateAESKey(128);
		String hexacode = EncodingUtil.ConvertTohex(blobbody);
		return hexacode.SubString(0, 8) + '-' + hexacode.SubString(8, 12) + '-' + hexacode.SubString(12, 16) + '-' + hexacode.SubString(16, 20) + '-' + hexacode.substring(20);
	}

	/**
	  * @description  Helper method to invoke LMS API to create user in LMS system
	*/
	public static String createLMSUser(String requestBody, Boolean LMSIdUpdate)
	{
		HttpResponse lmsresponse;
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mock = new MockHttpResponseGenerator1();
			lmsresponse = mock.respond4();
		}
		else
		{
			String lmsEndPoint = LMS_Settings__c.getValues('EndPoint').Value__c + 'v1/users';
			String lmsSessionId = LmsUtils.authenticate();
			lmsresponse = LmsUtils.processRequest(lmsEndPoint, requestBody, 'POST', lmsSessionId);
		}
		String lmsId='';
		if (lmsresponse.getStatusCode() == 200 || lmsresponse.getStatusCode() == 201)
		{
			JSONParser responseParser = JSON.createParser(lmsresponse.getBody());
			while (responseParser.nextToken() != null)
			{
				if ((responseParser.getCurrentToken() == JSONToken.FIELD_NAME) && (responseParser.getText() == 'Id'))
				{
					responseParser.nextToken();
					lmsId = responseParser.getText();
				}
			}
			if ((lmsId != '' || lmsId != null) && LMSIdUpdate == true)
			{
				NewUser requestBodyParse = (NewUser) JSON.deserializeStrict(requestBody, NewUser.class);
				list<User> users = [SELECT Id, Email, LMS_Id__c FROM User WHERE Email = :requestBodyParse.EmailAddress];
				if (!users.isEmpty())
				{
					users[0].LMS_Id__c = lmsId;
					update users;
				}
			}
		}
		return lmsId;
	}
	/**
	 * @description  Helper method to invoke LMS API to create user in LMS system
	*/
	public void CreateApproval(List<AccountContactRole__c>listAccountContactRole)
	{
		Map<Id, Id> checkRecordTypeId = new Map<Id, Id>();
		Map<ID, string> firstNameMap = new Map<ID, string>();
		Id hcpRecordTypeId;
		Set<Id> listcontactid = new Set<Id>();
		Set<Id> listaccountId = new Set<Id>();
		List<User> managerUser = new List<User>();
		Map<Id, User> managerConIdMap = new Map<Id, User>();
		Map<ID, ID> managerConId = new Map<ID, ID>();
		Id[] unlockRecords = new Id[0];
		JJVC_Contact_Recordtype__mdt hcpRecType = [SELECT MasterLabel FROM JJVC_Contact_Recordtype__mdt WHERE DeveloperName = 'HCP_Contacts_RecordtypeId'];
		hcpRecordTypeId = hcpRecType.MasterLabel;
		for (AccountContactRole__c accountContactRole : listAccountContactRole)
		{
			listcontactid.add(accountContactRole.Contact__c);
			unlockRecords.add(accountContactRole.Id);
			listaccountId.add(accountContactRole.Account__c);
		}
		List<Account>listAccounts =
		[
				SELECT Id, Name,
				(
						SELECT Id, Contact__c, Primary__c, Account_Ownership_Flag__c, Approval_Status__c,
								Functions__c
						FROM AccountContactRoles__r
						WHERE Approval_Status__c = 'Accepted'
				)
				FROM Account
				WHERE Id IN :listaccountId
		];
		for (Account account : listAccounts)
		{
			for (AccountContactRole__c accountContactRole : account.AccountContactRoles__r)
			{
				if (accountContactRole.Account_Ownership_Flag__c)
				{
					managerConId.put(account.id, accountContactRole.Contact__c);
				}
			}
		}
		managerUser = [SELECT id,Firstname, Email, Occupation__c,Contactid,localesidkey FROM User WHERE contactID = :managerConId.values() and IsActive = true];
		for (User user : managerUser)
		{
			managerConIdMap.put(user.ContactId, user);
		}
		List<Contact> listContacts = [SELECT Id, firstname, user_Role__c, user_Role__r.Type__c, RecordTypeId FROM Contact WHERE Id IN :listcontactid];
		for (Contact contact : listContacts)
		{
			checkRecordTypeId.put(contact.RecordTypeId, contact.RecordTypeId);
			firstNameMap.put(contact.id, contact.firstname);
		}
		for (AccountContactRole__c approvalRecord : listAccountContactRole)
		{
			if (!managerUser.isempty() && checkRecordTypeId.ContainsKey(HCPRecordTypeId) && approvalRecord.Approval_Status__c == 'Pending Approval')
			{
				Approval.ProcessSubmitRequest submitRequest = new Approval.ProcessSubmitRequest();
				submitRequest.setComments('Submitted for approval. Please approve.');
				submitRequest.setObjectId(approvalRecord.Id);
				User manager = managerConIdMap.get(managerConId.get(approvalRecord.Account__c));
				Email_Triggersends__c approvalNotifyEmailKey = Email_Triggersends__c.getvalues(manager.localesidkey);
				ID approverID = Portal_Settings__c.getValues('ApproverID').Value__c;
				submitRequest.setNextApproverIds(new Id[] {approverID});
				Approval.process(submitRequest);
				ET_EmailAdministration.sendemail(manager.Email, approvalNotifyEmailKey.Practice_link_approval_notification__c, firstNameMap.get(approvalRecord.contact__c), '', manager.firstname);       // Send approval notification to owner or firstuser
			}
		}
		Approval.unlock(unlockRecords, false);
	}
	public class NewUser
	{
		String FirstName { get; set; }
		String LastName { get; set; }
		String Username { get; set; }
		String Password { get; set; }
		String EmailAddress { get; set; }
		String DepartmentId { get; set; }
		String ExternalID { get; set; }
		String JobTitle { get; set; }
		String LanguageId { get; set; }
		public NewUser()
		{
			//empty constructor
		}
		public NewUser(String FirstName, String LastName, String Username, String Password, String EmailAddress, String DepartmentId, String ExternalID, String JobTitle, String LanguageId)
		{
			this.FirstName = FirstName;
			this.LastName = LastName;
			this.Username = Username;
			this.Password = Password;
			this.EmailAddress = EmailAddress;
			this.DepartmentId = DepartmentId;
			this.ExternalID = ExternalID ;
			this.JobTitle = JobTitle ;
			this.LanguageId = LanguageId ;
		}
	}
}