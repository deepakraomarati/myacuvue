/**
* File Name: B2B_UtilsTest 
* Author : Sathishkumar | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : Unit Test class for B2B_Utils
* Copyright (c) $2018 Johnson & Johnson
*/
@isTest
public class B2B_UtilsTest
{
	static testMethod void Test_coverallhelpermethods()
	{
		//Cover IsValidpassword method
		system.assertequals(B2B_Utils.IsValidPassword('Welcome@3'), false);
		//Cover IsValidEmail format method
		B2B_Utils.IsValidEmailFormat('test123@gmail.com');
		//Cover exception method
		B2B_Utils.getException('Error', 'Record Type', 'Check the record type');
		//Cover generate GUID method
		B2B_Utils.generateGUID();

		B2B_Utils.NewUser holdsNewUser = new B2B_Utils.NewUser('TEST', 'LMS_USER', 'testuser@test.com', 'xyzabpqrst', 'testuser@test.com', 'departmentId', 'userId', 'Optician', '1');
		String userJson = JSON.serialize(holdsNewUser);

		LMS_Settings__c LmsCustomSettings = new LMS_Settings__c();
		LmsCustomSettings.Name = 'EndPoint';
		LmsCustomSettings.Value__c = 'https://jjvus.sandbox.myabsorb.com/api/Rest/';
		insert LmsCustomSettings;
		LMS_Settings__c LmsCustomSettings2 = new LMS_Settings__c();
		LmsCustomSettings2.Name = 'Password';
		LmsCustomSettings2.Value__c = 'Test.123';
		insert LmsCustomSettings2;
		LMS_Settings__c LmsCustomSettings3 = new LMS_Settings__c();
		LmsCustomSettings3.Name = 'PrivateKey';
		LmsCustomSettings3.Value__c = '58091a9c-7077-40ee-927b-143ea1af810a';
		insert LmsCustomSettings3;
		LMS_Settings__c LmsCustomSettings4 = new LMS_Settings__c();
		LmsCustomSettings4.Name = 'Username';
		LmsCustomSettings4.Value__c = 'prorestadmin';
		insert LmsCustomSettings4;
		LMS_Settings__c LmsCustomSettings5 = new LMS_Settings__c();
		LmsCustomSettings5.Name = 'JP_DEPT_ID';
		LmsCustomSettings5.Value__c = '20bf217e-41a7-44b3-a1c7-2e084a44d936';
		insert LmsCustomSettings5;
		LMS_Settings__c LmsCustomSettings8 = new LMS_Settings__c();
		LmsCustomSettings8.Name = 'Token';
		LmsCustomSettings8.Value__c = '4zCipR7qNNtCzLeBa7aJRQ==S/FmYArMKLFW2SjqkAhvaN6NLt3KxiSwblr1Ndc2Tp4OFp+Q+PPcpWM1sfIFFKNvCUJunWzZS+l2IZIfU6Z7w0vt9/jBX6XD5ZFwYRBv1U7C4sZAy9xT0gtv0lnSuUCigmsC/6fs0Ej0UnAhWYzIhg==';
		insert LmsCustomSettings8;
		B2B_Utils.createLMSUser(userJson, true);
	}
}