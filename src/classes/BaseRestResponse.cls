/**
 * File Name: BaseRestResponse 
 * Author: Sathishkumar | BICSGLBOAL
 * Date Last Modified:  18-Oct-2018
 * Description: Interface class help to return different type outputs from services.
 * Copyright (c) $2018 Johnson & Johnson
*/
global abstract with sharing class BaseRestResponse
{
	global abstract void restResponse();
}