global class BatchReminder implements Database.Batchable<sObject>,Database.AllowsCallouts
{
    global Database.Querylocator start(Database.BatchableContext BC)
    {   
        String query    = System.Label.CampaignMemberQuery; 
        return Database.getQuerylocator(query);
    }
    
    global void execute(Database.BatchableContext BC,list<CampaignMember> scope)
    {
        List<CampaignMember> lstmem = new List<CampaignMember>();
        System.debug('INSIDEFOR ###'+scope.size());
        
        for(CampaignMember cm:scope)
        {
           if(cm.Reminder_Sent__c == false)
           {
             lstmem.add(cm);
           }
        }
        
        System.debug('INSIDEFOR ###'+lstmem.size());
        
        if(!lstmem.IsEmpty() && lstmem.size()>0)
        {
            ET_AdministrationService.CampaignMemberTriggeredSMSSend(lstmem);
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
        system.debug('completed....');
    }

}