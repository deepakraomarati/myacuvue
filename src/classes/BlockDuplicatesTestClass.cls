/*
* File Name: BlockDuplicatesTestClass
* Description : Test method to cover business logic around Event Attendee object
* Copyright : Johnson & Johnson
* @author : Lhawang Bhutia | lbhutia@its.jnj.com | lhawang.bhutia@cognizant.com
* Modification Log
* ================================================================
*    Ver  |Date         |Author                |Modification
*    1.0  |6-Feb-2017   |lbhutia@its.jnj.com   |Class Modified
* =============================================================== **/ 
@isTest
public class BlockDuplicatesTestClass {
    
    static testMethod void validateBlockDuplicates() {
        Test.startTest();
        final Contact conrec = new contact();
        conrec.lastName='test';
        insert conrec;
        
        final Custom_Event__c custevent= new Custom_Event__c();
        custevent.Subject__c = 'test';
        insert custevent;
        
        final Event_Attendee__c eventatt = new Event_Attendee__c();
        eventatt.Contact__c = conrec.id;
        eventatt.Invitee_Status__c='Pending';
        eventatt.Allow_Duplicate__c=false;
        eventatt.Custom_Event__c = custevent.id;
        insert eventatt;
        Test.stopTest();
        
        final String uid = [select UniqueId__c from Event_Attendee__c where Id=:eventatt.id].UniqueId__c;
        System.assert(!String.IsBlank(uid),'success');
    }
}