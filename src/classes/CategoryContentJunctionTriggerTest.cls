/*
* File Name: CategoryContentJunctionTriggerTest
* Description : Test method to cover business logic around CategoryContentJunction
* Copyright : Johnson & Johnson
* @author : Lhawang Bhutia | lbhutia@its.jnj.com | lhawang.bhutia@cognizant.com
* Modification Log
* ================================================================
*    Ver  |Date         |Author                |Modification
*    1.0  |7-Feb-2017   |lbhutia@its.jnj.com   |Class Modified
*
*
*
*
* =============================================================== 
*
*/ 
@isTest
public class CategoryContentJunctionTriggerTest {
    
    public testmethod static void testmethod1() {
        final String contentTitle = 'test';
		Test.startTest();
        final ContentVersion contentVersion = new ContentVersion(
            Title = contentTitle,
            PathOnClient = 'test.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;
        final JnJDSF4__Category__c  category = new JnJDSF4__Category__c(
            
        );
        insert category;
        
        final ContentDocument contentDocument = [SELECT Id FROM ContentDocument LIMIT 1];
        
        
        JnJDSF4__Cat_Content_Junction__c categoryContentJunction = new JnJDSF4__Cat_Content_Junction__c(
            JnJDSF4__ContentId__c = contentDocument .Id,
            JnJDSF4__Category__c = category.Id
        );
        insert categoryContentJunction;
        Test.stopTest();
        
        categoryContentJunction = [SELECT ContentTitle__c FROM JnJDSF4__Cat_Content_Junction__c WHERE Id = :categoryContentJunction.Id];        
        System.assertEquals(contentTitle, categoryContentJunction.ContentTitle__c,'Success');
    }
}