/*
* File Name: ChangePasswordControllerTest
* Description : Test method to cover business logic around ChangePasswordController
* Copyright : Johnson & Johnson
* @author : Lhawang Bhutia | lbhutia@its.jnj.com | lhawang.bhutia@cognizant.com
* Modification Log
* ================================================================
*    Ver  |Date         |Author                |Modification
*    1.0  |7-Feb-2017   |lbhutia@its.jnj.com   |Class Modified
* =============================================================== **/ 
@IsTest public with sharing class ChangePasswordControllerTest {
    public testmethod static void testChangePasswordController() {
        // Instantiate a new controller with all parameters in the page
        final ChangePasswordController controller = new ChangePasswordController();
        controller.oldPassword = '123456';
        controller.newPassword = 'qwerty1'; 
        controller.verifyNewPassword = 'qwerty1';  
        System.assertEquals(controller.changePassword(),null,'Success');
    } 
}