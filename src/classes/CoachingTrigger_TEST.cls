@IsTest (SeeAllData=true)

public class CoachingTrigger_TEST{
    
    static testMethod void AccountContactTrigger_TEST(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        Profile p1 = [SELECT Id FROM Profile WHERE Name='ASPAC ANZ CDM/KAM'];
        User u = new User(Alias = 'standt', Email='SM@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Havish', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='testmail@testorg.com',Unique_User_Id__c = 'ABC1');
        insert u;
        User u1 = new User(Alias = 'standt', Email='CDM@testorg.com',
                           EmailEncodingKey='UTF-8', LastName='Havish', LanguageLocaleKey='en_US',
                           LocaleSidKey='en_US', ProfileId = p1.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='CDM@testorg.com',Unique_User_Id__c = 'ABC2');
        insert u1;
        
        system.runas(u){
            
            RecordType rt = [Select Id from RecordType where DeveloperName = 'X3_In_a_Car'];
            RecordType rt1 = [Select Id from RecordType where DeveloperName = 'WORK_WITH'];
            
            Coaching__c testCoaching1 = New Coaching__c(Name ='Test Account',RecordTypeId =rt.Id, Start_Date__c = datetime.newInstance(2014, 9, 15, 12, 30, 0), End_Date__c = datetime.newInstance(2014, 9, 15, 13, 30, 0), Type__c = '3-in-a-Car(Full Day)', Coachee__c = u1.Id);
            insert testCoaching1 ;
            
            Satisfaction_Surveys__c CoachingSurvey= New Satisfaction_Surveys__c(Coaching__c =testCoaching1.Id, Q1_besupportive__c = 'USED THE IDEAS AND ENCOURAGED INPUT FROM CDM',AQ1_BeSupportive__c = 'Not Met' );
            insert CoachingSurvey;
            
            CoachingSurvey.AQ1_BeSupportive__c = 'Partially Met';
            Update CoachingSurvey;
            
            Coaching__c testCoaching2 = New Coaching__c(Name ='Test Account',RecordTypeId =rt1.Id, Start_Date__c = datetime.newInstance(2014, 9, 15, 12, 30, 0), End_Date__c = datetime.newInstance(2014, 9, 15, 13, 30, 0), Type__c = 'Work*With(Full Day)', Coachee__c = u1.Id);
            insert testCoaching2 ;
            
            Call_Scripts__c Coachingfeedback= New Call_Scripts__c(Coaching__c =testCoaching2.Id, Question_4_Precall__c = 'Formulates a good mix of questions',AQ4_Precall__c = '1' );
            insert Coachingfeedback;
            
            Coachingfeedback.AQ4_Precall__c = '2';
            Update Coachingfeedback;
            system.assertEquals(Coachingfeedback.AQ4_Precall__c,'2','success');
        }
        
    }
    static testMethod void AccountContactTrigger_TEST2(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        Profile p1 = [SELECT Id FROM Profile WHERE Name='ASPAC ANZ CDM/KAM'];
        User u = new User(Alias = 'stanley', Email='SM@testorg.com',country='Australia',
                          EmailEncodingKey='UTF-8', LastName='Havish', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='stanley@dabba.com',Unique_User_Id__c = 'ABCD2');
        insert u;
        
        User u1 = new User(Alias = 'standy', Email='CDM@testorg.com',
                           EmailEncodingKey='UTF-8', LastName='Havish', LanguageLocaleKey='en_US',country='Australia',
                           LocaleSidKey='en_US', ProfileId = p1.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='CDM@testpapug.com',Unique_User_Id__c = 'ABC2');
        insert u1;
        
        system.runas(u){
            
            RecordType rt = [Select Id from RecordType where DeveloperName = 'X3_In_a_Car'];
            RecordType rt1 = [Select Id from RecordType where DeveloperName = 'WORK_WITH'];
            
            Coaching__c testCoaching1 = New Coaching__c(Name ='Test Account',RecordTypeId =rt.Id, Start_Date__c = datetime.newInstance(2014, 9, 15, 12, 30, 0), End_Date__c = datetime.newInstance(2014, 9, 15, 13, 30, 0), Type__c = '3-in-a-Car(Full Day)', Coachee__c = u1.Id);
            insert testCoaching1 ;
            
            Satisfaction_Surveys__c CoachingSurvey= New Satisfaction_Surveys__c(Coaching__c =testCoaching1.Id, Q1_besupportive__c = 'USED THE IDEAS AND ENCOURAGED INPUT FROM CDM',AQ1_BeSupportive__c = 'Not Met' );
            insert CoachingSurvey;
            
            CoachingSurvey.AQ1_BeSupportive__c = 'Partially Met';
            Update CoachingSurvey;
            
            Coaching__c testCoaching2 = New Coaching__c(Name ='Test Account',RecordTypeId =rt1.Id, Start_Date__c = datetime.newInstance(2014, 9, 15, 12, 30, 0), End_Date__c = datetime.newInstance(2014, 9, 15, 13, 30, 0), Type__c = 'Work*With(Full Day)', Coachee__c = u1.Id);
            insert testCoaching2 ;
            
            Call_Scripts__c Coachingfeedback= New Call_Scripts__c(Coaching__c =testCoaching2.Id, Question_4_Precall__c = 'Formulates a good mix of questions',AQ4_Precall__c = '1' );
            insert Coachingfeedback;
            
            Coachingfeedback.AQ4_Precall__c = '2';
            Update Coachingfeedback;
            system.assertEquals(Coachingfeedback.AQ4_Precall__c,'2','success');
        }
        
    }
    static testMethod void AccountContactTrigger_TEST3(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        Profile p1 = [SELECT Id FROM Profile WHERE Name='ASPAC ANZ CDM/KAM'];
        User u = new User(Alias = 'lafunga', Email='SM@testorg.com',country='taiwan',
                          EmailEncodingKey='UTF-8', LastName='Havish', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='sattu@dabba.com',Unique_User_Id__c = 'Afg');
        insert u;
        
        User u1 = new User(Alias = 'sanandy', Email='CDM@trajjog.com',
                           EmailEncodingKey='UTF-8', LastName='Havish', LanguageLocaleKey='en_US',country='taiwan',
                           LocaleSidKey='en_US', ProfileId = p1.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='CDM@testpapug.com',Unique_User_Id__c = 'ABC2');
        insert u1;
        
        system.runas(u){
            
            RecordType rt = [Select Id from RecordType where DeveloperName = 'X3_In_a_Car'];
            RecordType rt1 = [Select Id from RecordType where DeveloperName = 'WORK_WITH'];
            
            Coaching__c testCoaching1 = New Coaching__c(Name ='Test Account',RecordTypeId =rt.Id, Start_Date__c = datetime.newInstance(2014, 9, 15, 12, 30, 0), End_Date__c = datetime.newInstance(2014, 9, 15, 13, 30, 0), Type__c = '3-in-a-Car(Full Day)', Coachee__c = u1.Id);
            insert testCoaching1 ;
            
            Satisfaction_Surveys__c CoachingSurvey= New Satisfaction_Surveys__c(Coaching__c =testCoaching1.Id, Q1_besupportive__c = 'USED THE IDEAS AND ENCOURAGED INPUT FROM CDM',AQ1_BeSupportive__c = 'Not Met' );
            insert CoachingSurvey;
            
            CoachingSurvey.AQ1_BeSupportive__c = 'Partially Met';
            Update CoachingSurvey;
            
            Coaching__c testCoaching2 = New Coaching__c(Name ='Test Account',RecordTypeId =rt1.Id, Start_Date__c = datetime.newInstance(2014, 9, 15, 12, 30, 0), End_Date__c = datetime.newInstance(2014, 9, 15, 13, 30, 0), Type__c = 'Work*With(Full Day)', Coachee__c = u1.Id);
            insert testCoaching2 ;
            
            Call_Scripts__c Coachingfeedback= New Call_Scripts__c(Coaching__c =testCoaching2.Id, Question_4_Precall__c = 'Formulates a good mix of questions',AQ4_Precall__c = '1' );
            insert Coachingfeedback;
            
            Coachingfeedback.AQ4_Precall__c = '2';
            Update Coachingfeedback;
            system.assertEquals(Coachingfeedback.AQ4_Precall__c,'2','success');
        }      
    }
    
    static testMethod void AccountContactTrigger_TEST4(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        Profile p1 = [SELECT Id FROM Profile WHERE Name='ASPAC ANZ CDM/KAM'];
        User u = new User(Alias = 'lafunga', Email='Sdhaniya@testorg.com',country='india',
                          EmailEncodingKey='UTF-8', LastName='Havish', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='dhanno@dabba.com',Unique_User_Id__c = 'ino');
        insert u;
        
        User u1 = new User(Alias = 'saloody', Email='Cbagun@trajjog.com',
                           EmailEncodingKey='UTF-8', LastName='Haparval', LanguageLocaleKey='en_US',country='india',
                           LocaleSidKey='en_US', ProfileId = p1.Id,
                           TimeZoneSidKey='America/Los_Angeles', UserName='CDM@testpapug.com',Unique_User_Id__c = 'eno');
        insert u1;
        
        system.runas(u){
            
            RecordType rt = [Select Id from RecordType where DeveloperName = 'X3_In_a_Car'];
            RecordType rt1 = [Select Id from RecordType where DeveloperName = 'WORK_WITH'];
            
            Coaching__c testCoaching1 = New Coaching__c(Name ='Test Account',RecordTypeId =rt.Id, Start_Date__c = datetime.newInstance(2014, 9, 15, 12, 30, 0), End_Date__c = datetime.newInstance(2014, 9, 15, 13, 30, 0), Type__c = '3-in-a-Car(Full Day)', Coachee__c = u1.Id);
            insert testCoaching1 ;
            
            Satisfaction_Surveys__c CoachingSurvey= New Satisfaction_Surveys__c(Coaching__c =testCoaching1.Id, Q1_besupportive__c = 'USED THE IDEAS AND ENCOURAGED INPUT FROM CDM',AQ1_BeSupportive__c = 'Not Met' );
            insert CoachingSurvey;
            
            CoachingSurvey.AQ1_BeSupportive__c = 'Partially Met';
            Update CoachingSurvey;
            
            Coaching__c testCoaching2 = New Coaching__c(Name ='Test Account',RecordTypeId =rt1.Id, Start_Date__c = datetime.newInstance(2014, 9, 15, 12, 30, 0), End_Date__c = datetime.newInstance(2014, 9, 15, 13, 30, 0), Type__c = 'Work*With(Full Day)', Coachee__c = u1.Id);
            insert testCoaching2 ;
            
            Call_Scripts__c Coachingfeedback= New Call_Scripts__c(Coaching__c =testCoaching2.Id, Question_4_Precall__c = 'Formulates a good mix of questions',AQ4_Precall__c = '1' );
            insert Coachingfeedback;
            
            Coachingfeedback.AQ4_Precall__c = '2';
            Update Coachingfeedback;
            system.assertEquals(Coachingfeedback.AQ4_Precall__c,'2','success');
        }      
    }
}