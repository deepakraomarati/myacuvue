/**
* File Name: ContactHandler
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  18-Oct-2018
* Desciption : create Accountcontact record for primary when there is no Accountcontact record
* Copyright (c) $2018 Johnson & Johnson
*/
public with sharing class ContactHandler
{
	/**
	* @description  check and creat Accountcontact record
	*/
	public Static Void createOrUpdatePrimaryAccCon(List<Contact> newConsList)
	{
		List<AccountContactRole__c> insertACRsList = new List<AccountContactRole__c>();
		JJVC_Contact_Recordtype__mdt rectype = [SELECT MasterLabel FROM JJVC_Contact_Recordtype__mdt WHERE DeveloperName = 'HCP_Contacts_RecordtypeId'];
		list<Contact>insertcontacts =
		[
				SELECT Id,
						Name,
						AccountID,
						recordtypeid,
				(
						SELECT Id,
								Name,
								Account__c,
								Contact__c,
								Primary__c
						FROM AccountContactRoles__r
				)
				FROM Contact
				WHERE ID IN :newConsList
		];
		for (Contact con : insertcontacts)
		{
			if (con.recordtypeid == rectype.MasterLabel && con.AccountContactRoles__r.size() == 0 && con.Accountid != Portal_Settings__c.getValues('GenericAccount Id').Value__c)
			{
				AccountContactRole__c newACRRec = new AccountContactRole__c();
				newACRRec.Contact__c = con.Id;
				newACRRec.Account__c = con.AccountId;
				insertACRsList.add(newACRRec);
			}
		}
		if (!insertACRsList.Isempty())
		{
			insert insertACRsList;
		}
	}
}