public without sharing class ContactMatch {
  
  /* --------------------------------------------
   * Author      : 
   * Company     : BICS
   * Description : validating Lead or Contact record exist or not.         
   * ---------------------------------------------*/
   
   public String Email;
   public String ContactNumber;
   public String LastName;
   public String NRIC;
   public Boolean SGTYPE;
      
   public ContactMatch(String Email,String ContactNumber,string LastName,string NRIC,Boolean SGTYPE)
   {
     this.Email = Email;
     this.ContactNumber = ContactNumber;
     this.LastName = LastName;
     this.NRIC = NRIC;
     this.SGTYPE= SGTYPE;
   } 
   
   Public Map<sObject,String> findcontact()
   {
     
      Map<sObject,String> MapExist = new Map<sObject,String>();
      
      list<Contact> conExist = new list<Contact>();
      list<Lead> leadExist =new list<Lead>();
      
      try{
        
        
         if(SGTYPE == False)
         {
                     leadExist= [SELECT Id,
                                       Email,
                                       Status 
                                       FROM Lead 
                                       WHERE (Email=:Email OR Phone=:ContactNumber) AND IsConverted =false];
         }
                                
        if(leadExist.size()>0 && !leadExist.isEmpty())
        {
          for(Lead lobj:leadExist)
          {
            MapExist.put(lobj,'Lead');
          }
          System.debug('MAPEXIST ###'+MapExist);
          return MapExist;
        }else{
          
          if(SGTYPE == True){
              
              conExist = [SELECT Id,
                         FirstName,
                         LastName,
                         Email,
                         Phone
                         FROM Contact
                         WHERE (LastName =:LastName AND NRIC__c=:NRIC)];
          }else{
              
              conExist = [SELECT Id,
                         FirstName,
                         LastName,
                         Email,
                         Phone
                         FROM Contact
                         WHERE (Email=:Email or Phone=:ContactNumber)];
           }    
                     
          if(conExist.size()>0 && !conExist.isEmpty())
          {
            for(Contact con:conExist)
            {
              MapExist.put(con,'Contact');
            }
            
            System.debug('MAPEXIST ###'+MapExist);
          }
          
        }
        return MapExist;
      
      }catch(Exception e)
      {
        system.debug('The following exception has occurred: '+ e.getMessage());
      }
      
      return null;
     
   }

}