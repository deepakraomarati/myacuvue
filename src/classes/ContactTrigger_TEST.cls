/*
* File Name: ContactTrigger_TEST
* Description : Test method to cover business logic around ContactTrigger
* Copyright : Johnson & Johnson
* @author : Lhawang Bhutia | lbhutia@its.jnj.com | lhawang.bhutia@cognizant.com
* Modification Log
* ================================================================
*    Ver  |Date         |Author                |Modification
*    1.0  |9-Feb-2017   |lbhutia@its.jnj.com   |Class Modified
* =============================================================== 
*
*
*/
@IsTest
public class ContactTrigger_TEST{
    
    static testMethod void testmethod1(){
        Test.startTest();
        final Profile admprofile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        final User standuser = new User(Alias = 'standtd', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Havish', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = admprofile.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='havish@abc.com',Unique_User_Id__c = 'ABC');
        insert standuser;
        
        system.runas(standuser){
            final RecordType rectype = [Select Id from RecordType where Name = 'ECP'];
            //creating account
            final Account acc = New Account(Name ='Test Account',PublicAddress__c='Bangalore', SalesRep__c = 'standtd', AccountNumber = '12345', OutletNumber__c = '12345', CountryCode__c = 'AUS');
            insert acc;
            
            final Contact tcc = new Contact(FirstName='test',LastName='Con',AccountId = acc.Id, RecordTypeId =rectype.Id);
            insert tcc;
            
            //creating task
            final task trec= new task();
            trec.whoid=tcc.id;
            trec.WhatId=acc.id;
            trec.Subject='hii';
            trec.Priority='Normal';
            trec.Status='Open';
            insert trec;
            trec.ActivityDate=Date.today();
            trec.ActivityStatus__c='Closed';
            update trec;
            
            //creating event
            
            final event erec= new event();
            erec.StartDateTime = date.today();
            erec.EndDateTime = date.today().addDays(2);
            erec.Subject = 'Test';
            erec.WhoId = tcc.id ;
            erec.WhatId = acc.id;
            insert erec;
            erec.ActivityDate=Date.today();
            update erec;
            
            //updating Contact
            final Contact tc1 = new Contact(FirstName='test1',LastName='Con1',AccountId = acc.Id, RecordTypeId =rectype.Id);
            insert tc1;
            
            tc1.isPrimary__c=TRUE;  
            tc1.inactive__c=TRUE;
            update tc1;
            system.assertEquals(tc1.FirstName,'test1','Success');			
            Test.stopTest();       
        }
    }
}