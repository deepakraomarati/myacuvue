/**
* File Name: CourseUpdateTest
* Author : Sathishkumar | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : Unit Test class for CourseUpdate class 
* Copyright (c) $2018 Johnson & Johnson
*/
@isTest(SeeAlldata=false)
public class CourseUpdateTest
{
	static testMethod void TestCourseUpdateMethod()
	{
		LMS_Settings__c LmsCustomSettings = new LMS_Settings__c();
		LmsCustomSettings.Name = 'EndPoint';
		LmsCustomSettings.Value__c = 'https://jjvus.sandbox.myabsorb.com/api/Rest/';
		insert LmsCustomSettings;

		LMS_Settings__c LmsCustomSettings1 = new LMS_Settings__c();
		LmsCustomSettings1.Name = 'ImageURL';
		LmsCustomSettings1.Value__c = 'https://jjvci.sandbox.myabsorb.com/Files/';
		insert LmsCustomSettings1;

		LMS_Settings__c LmsCustomSettings2 = new LMS_Settings__c();
		LmsCustomSettings2.Name = 'Password';
		LmsCustomSettings2.Value__c = 'Test.123';
		insert LmsCustomSettings2;

		LMS_Settings__c LmsCustomSettings3 = new LMS_Settings__c();
		LmsCustomSettings3.Name = 'PrivateKey';
		LmsCustomSettings3.Value__c = '58091a9c-7077-40ee-927b-143ea1af810a';
		insert LmsCustomSettings3;

		LMS_Settings__c LmsCustomSettings4 = new LMS_Settings__c();
		LmsCustomSettings4.Name = 'Username';
		LmsCustomSettings4.Value__c = 'prorestadmin';
		insert LmsCustomSettings4;

		LMS_Settings__c LmsCustomSettings5 = new LMS_Settings__c();
		LmsCustomSettings5.Name = 'DEPT_ID_ja_JP';
		LmsCustomSettings5.Value__c = '20bf217e-41a7-44b3-a1c7-2e084a44d936';
		insert LmsCustomSettings5;

		Categories_Tag__c tag = new Categories_Tag__c();
		tag.Description__c = 'Test Tag1';
		tag.LMS_ID__c = '7a934842-c88a-4dfc-aa73-3c8d95074de3';
		tag.Parent_Id__c = 'ty642f42-e94d-r78t-8w2a-4r85a7t78se8';
		tag.Type__c = 'Tag';
		insert tag;

		Categories_Tag__c tag1 = new Categories_Tag__c();
		tag1.Description__c = 'Test Tag' ;
		tag1.LMS_ID__c = '7a934842-c88a-4dfc-aa73-3c8d95074de4';
		tag1.Parent_Id__c = 'ty642f42-e74d-r78t-8w2a-4r85a7t78se8';
		tag1.Type__c = 'Tag';
		insert tag1;

		LmsUtils.ResourcesDTOFrUpdate lmsr = new LmsUtils.ResourcesDTOFrUpdate();
		lmsr.ModuleId = 'c5dsfg';
		lmsr.Description = 'test';
		lmsr.Id = 'c4wf4r';

		Course__c course = new Course__c();
		course.Name = 'Test Course';
		course.ActiveStatus__c = 'Active';
		course.CategoryId__c = '15654184-afc0-48eb-8838';
		course.Main_Course_Image__c = 'dggd';
		course.Landing_page_Image__c = 'csdfsg';
		course.Online_CoursePage_Image__c = 'sdfg';
		course.Related_Content_Image__c = 'df5CWE';
		course.Description__c = 'some test data';
		course.ExpireDuration__c = 'test ExpireDuration';
		course.LMS_ID__c = '7a934842-c88a-4dfc-aa73-3c8d95074de3';
		course.Language__c = 'ja_JP';
		course.Type__c = 'OnlineCourse';
		course.Notes__C = 'tets note';
		insert course;

		Course__c course2 = new Course__c();
		course2.Name = 'Test Course2';
		course2.ActiveStatus__c = 'Active';
		course2.CategoryId__c = '15656523-afc0-48eb-8838';
		course2.Main_Course_Image__c = 'dggd';
		course2.Landing_page_Image__c = 'csdfsg';
		course2.Online_CoursePage_Image__c = 'sdfg';
		course2.Related_Content_Image__c = 'df5CWE';
		course2.Description__c = 'some test data';
		course2.ExpireDuration__c = 'test ExpireDuration';
		course2.LMS_ID__c = '7a934963-c88a-4dfc-aa73-3c8d95074de3';
		course2.Language__c = 'ja_JP';
		course2.Type__c = 'InstructorLedCourse';
		course2.Notes__C = 'tets note';
		insert course2;

		Curriculum__c course1 = new Curriculum__c();
		course1.Name = 'Test Curriculum';
		course1.ActiveStatus__c = 'Active';
		course1.CategoryId__c = '15654184-afc0-48eb-9999';
		course.Main_Course_Image__c = 'dggd';
		course1.Landing_page_Image__c = 'csdfsg';
		course1.Online_CoursePage_Image__c = 'sdfg';
		course1.Related_Content_Image__c = 'df5CWE';
		course1.Description__c = 'some test data';
		course1.ExpireDuration__c = 'test ExpireDuration';
		course1.LMS_ID__c = '7a934842-c88a-4dfc-aa73-3c8d95074de4';
		course1.Language__c = 'ja_JP';
		course1.Type__c = 'Curriculum';
		course1.Notes__C = 'tets note';
		insert course1;

		CourseTagAssignment__c coursetag = new CourseTagAssignment__c();
		coursetag.Name = 'Test Coursetag';
		coursetag.CustomTag__c = tag.id;
		coursetag.CustomCourse__c = course.id;
		coursetag.CustomCurriculum__c = course1.id;
		coursetag.courseTagAssignedId__c = 'test';
		insert coursetag;

		CourseTagAssignment__c coursetag1 = new CourseTagAssignment__c();
		coursetag1.Name = 'Test Coursetag1';
		coursetag1.CustomTag__c = tag1.id;
		coursetag1.CustomCourse__c = course2.id;
		coursetag1.CustomCurriculum__c = course1.id;
		coursetag1.courseTagAssignedId__c = 'test1';
		insert coursetag1;

		Role__c holdsUserRole = new Role__c();
		holdsUserRole.Name = '医師';
		holdsUserRole.Approval_Required__c = true;
		holdsUserRole.Country__c = 'Japan';
		holdsUserRole.Type__c = 'Staff';
		insert holdsUserRole;

		LMS_Settings__c LmsCustomSettings6 = new LMS_Settings__c();
		LmsCustomSettings6.Name = 'ja_JP-' + holdsUserRole.id;
		LmsCustomSettings6.Value__c = '20ad397c-de1d-4165-967c-84b3110566cc';
		insert LmsCustomSettings6;

		LMS_Settings__c LmsCustomSettings7 = new LMS_Settings__c();
		LmsCustomSettings7.Name = 'Token';
		LmsCustomSettings7.Value__c = 'TEST TOKEN';
		insert LmsCustomSettings7;

		Test.StartTest();
		courseUpdate sh1 = new courseUpdate();
		String sch = '0 0 23 * * ?';
		system.schedule('Test Course Update Check', sch, sh1);
		Test.stopTest();
		list<Course__c>updatedcourses = [SELECT Id FROM Course__c WHERE lastmodifieddate =: date.today()];
		system.assertNotEquals(updatedcourses,null);
	}
}