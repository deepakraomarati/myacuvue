public with sharing class CreatedStatusChartRP1 {

    /* public String getObjectCountItems() {
        return null;
    }*/


  
    
      public List<TotalObjectCounts > getObjectCountItems() {
        List<TotalObjectCounts > AnnualRecurrRevenueItems = new List<TotalObjectCounts >();
        
        /* List<String> SFObjects = new List<String>();
        SFObjects.add('Account');
        SFObjects.add('Contact');
        SFObjects.add('Product2');
        SFObjects.add('Campaign');
        SFObjects.add('CampaignMember');
        SFObjects.add('Coupon__c');
        //SFObjects.add('TransactionTd__c');
        SFObjects.add('PointHistory__c');
        // SFObjects.add('ProductPrice__c');
        SFObjects.add('BoardList__c'); 
        
        

        for(String Objs:SFObjects){
             
             String Name='';
                    Name+='DevAdmin'; 
             
             String Query ='';
                     Query += 'SELECT Id FROM '+ Objs ;
                     Query+= ' where CreatedBy.name=\''+ Name+'\'';
//                     Query+= ' AND LastModifiedBy.name=\''+ Name+'\'';
                     Query+= ' AND CreatedDate=TODAY';
             
             system.debug('Query>>'+Query  );
             //Decimal dataItems = [SELECT COUNT() FROM '\''+ Objs+'\'' where LastModifiedBy.name=\''+ Name+'\'' AND LastModifiedDate<TODAY ];
             List<sObject>  dataItems = Database.Query(Query);
        
                system.debug('Count>>'+dataItems.size() );
              /*  TotalObjectCounts AnnualRecRevenue ;

               if(Objs == 'CampaignMember')
                {
                    AnnualRecRevenue = new TotalObjectCounts ('CampMem',
                                                             dataItems.size() ); 
                }
                else
                {
                    AnnualRecRevenue = new TotalObjectCounts (''+Objs.substring(0,5),
                                                             dataItems.size() ); 
                }*/
                
             /* Objs=Objs.replace('2','');                                               
              TotalObjectCounts AnnualRecRevenue = new TotalObjectCounts (''+Objs.Replace('__c',''),
                                                             dataItems.size() ); 
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
        
        } */

      Decimal dataItems = [SELECT COUNT() FROM Account where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY ];
        
       // system.debug('Count>>'+ dataItems );

                TotalObjectCounts AnnualRecRevenue = new TotalObjectCounts ('Account',
                                                             dataItems );
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                 dataItems = [SELECT COUNT() FROM contact where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('Contact',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM Product2 where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY ];
                
                AnnualRecRevenue = new TotalObjectCounts ('Product',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                system.debug('ListCount>>'+AnnualRecurrRevenueItems.size());
                
                dataItems = [SELECT COUNT() FROM Campaign where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('Campaign',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM CampaignMember where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('CampMem',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM Coupon__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('Coupon',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM TransactionTd__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('TransTd',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM ProductLotNo__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('PdLotNo',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);

                /* dataItems = [SELECT COUNT() FROM PointHistory__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('PntHistory',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                 dataItems = [SELECT COUNT() FROM AccountPointHistory__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('ActPHistory',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM ProductPrice__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('PdtPrice',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
            dataItems = [SELECT COUNT() FROM CouponSendList__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('CpSList',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM CouponContact__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('CpContact',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                 dataItems = [SELECT COUNT() FROM BoardList__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('BoardList',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM Survey__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('Survey',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM SurveyQuestion__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('SveyQuest',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                 dataItems = [SELECT COUNT() FROM SurveyQuestionItem__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('SveyQItem',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                 dataItems = [SELECT COUNT() FROM SurveyAnswer__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('SveyAns',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM PushList__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('PushList',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                 dataItems = [SELECT COUNT() FROM OpticianBoard__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('OpticianBoard',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);

                dataItems = [SELECT COUNT() FROM OptIdList__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('OptIdList',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
    
                dataItems = [SELECT COUNT() FROM AdminInfo__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('AdminInfo',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM DxTotalQuantity__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('DxTolQty',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM DxOrder__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('DxOrder',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM DxOrderProduct__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('DxOrdPt',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                
                dataItems = [SELECT COUNT() FROM FocOrder__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('FocOrder',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM FocOrderProduct__c where CreatedBy.name='DevAdmin' AND CreatedDate=TODAY];
                
                AnnualRecRevenue = new TotalObjectCounts ('FocOrdPt',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);*/
            
            return AnnualRecurrRevenueItems;
        }
        
        
        
    public class TotalObjectCounts {
        public String QuantityName { get; set; }        
        public Decimal QuantityValue { get; set; }

        public TotalObjectCounts(String QuantityName, Decimal QuantityValue) {
            this.QuantityName = QuantityName;
            this.QuantityValue = QuantityValue;           
        }
    }


}