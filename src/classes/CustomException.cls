public with sharing class CustomException Extends Exception{

 //************** Exception Handling ***********************//
 public static String getException(String ErrorCode,String ErrorType)
 {
   Map<String,CustomException__c> ErrorMap = new Map<String,CustomException__c> (CustomException__c.getAll());
   JSONGenerator gen = JSON.createGenerator(true);
   String pretty ='';
   gen.writeStartObject();
   gen.writeStringField('message',ErrorMap.get(ErrorCode).Error_Description__c);
   gen.writeStringField('errorCode',ErrorCode);
   gen.writeStringField('errorType',ErrorType);
   gen.writeEndObject();
   return gen.getAsString();
 }
}