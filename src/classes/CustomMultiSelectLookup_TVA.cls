/****************************************************************************************
* Create By       :    Ashok Kalluri 
* Create Date     :    07/11/2017 
* Description     :    Class for multi select lookup feature
* Modification Log:
*   -----------------------------------------------------------------------------
*   * Developer             Date                Description
*   * ----------------------------------------------------------------------------
*   * Ashok Kalluri         07/11/2017          Initial Version. 
*****************************************************************************************/

public with sharing class CustomMultiSelectLookup_TVA {
   // @AuraEnabled
    //public list<ResultWrapper> rwp;
    /**
* Returns JSON of list of ResultWrapper to Lex Components
* @objectName - Name of SObject
* @fldAPIText - API name of field to display to user while searching
* @fldAPIVal - API name of field to be returned by Lookup COmponent
* @lim   - Total number of record to be returned
* @fldAPISearch - API name of field to be searched
* @searchText - text to be searched
* */
    @AuraEnabled
    public static String searchDB(String objectName, String fldAPIText, String fldAPIVal, Integer lim, String fldAPISearch, String searchText, List<String> lookupFilters, string eventids) {
        String searchTextVar = '';
        set<string>  evName=new Set<string>();
        
        List<EventRelation> eventrelatinIds=[select Id,RelationId ,Relation.Name from EventRelation where EventId=:eventids];
        
        for(EventRelation evenId:eventrelatinIds)
        {
            
            evName.add(evenId.RelationId);
            
        }
        
        system.debug('users ids'+evName);
        
        string query=' ';
        if (searchText != '') {
            searchTextVar = '%' + String.escapeSingleQuotes(searchText.trim()) + '%';
        }
        String lookupFilter = '';
        String lookupFilterRec = '';
        
        Set<Id> oIds = new Set<Id>();
        if (!lookupFilters.isEmpty()) {
            for (String filterVal : lookupFilters) {
                lookupFilter = 'AND ' + filterVal;
            }
            lookupFilterRec = ' WHERE ' + lookupFilter.removeStartIgnoreCase('AND');
        }
        if (searchTextVar != '') {
            query = 'SELECT ' + String.escapeSingleQuotes(fldAPIText) + ' ,' + String.escapeSingleQuotes(fldAPIVal) +
                ' FROM ' + String.escapeSingleQuotes(objectName) +
                ' WHERE ' + String.escapeSingleQuotes(fldAPISearch) + ' LIKE \'' + String.escapeSingleQuotes(searchTextVar) + '\'' +
                String.escapeSingleQuotes(lookupFilter) +
                ' LIMIT ' + 0;
        }
        else if (objectName=='user') {
            query = 'SELECT ' + String.escapeSingleQuotes(fldAPIText) + ' ,' + String.escapeSingleQuotes(fldAPIVal) + 
                ' FROM ' + String.escapeSingleQuotes(objectName) +
                String.escapeSingleQuotes(lookupFilterRec) +
                ' LIMIT ' + 0;
        }
        else {
            String objectNameTmp = '';
            if(objectName=='AccountContact_TVCORE__c'){
                objectNameTmp = 'Contact';
            }
            else {
                objectNameTmp = objectName;
            }
            String newFilter = '';
            if(objectName=='AccountContact_TVCORE__c'){
                newFilter = ' Contact_TVCORE__c IN :oIds';
            }
            else {
                newFilter = ' Id IN :oIds';
            }
            if(String.isEmpty(lookupFilterRec)){
                lookupFilterRec = ' WHERE '+newFilter;
            }
            else {
                lookupFilterRec = lookupFilterRec + ' AND '+newFilter;
            }
            query = 'SELECT ' + String.escapeSingleQuotes(fldAPIText) + ' ,' + String.escapeSingleQuotes(fldAPIVal) +
                ' FROM ' + String.escapeSingleQuotes(objectName) + String.escapeSingleQuotes(lookupFilterRec) + ' LIMIT ' + lim;
        }
        if (searchTextVar != '') {
            query='SELECT  Id, Name FROM ' + String.escapeSingleQuotes(objectName) +' WHERE Name LIKE \''+ String.escapeSingleQuotes(searchTextVar)+'\'and Id NOT IN :evName'; 
        }else
        {
        }
        
        List<ResultWrapper> lstRet = new List<ResultWrapper>();
        try{
            
            system.debug('queryyy'+query);
        List<sObject> sobjList = Database.query(query);
        
        //forming result wrappers
        for (SObject s : sobjList) {
            ResultWrapper obj = new ResultWrapper();
            obj.objName = objectName;
            obj.text = String.valueOf(s.get('Name') );
            obj.val = String.valueOf(s.get('Id')) ;
            obj.eventrelatinids=String.valueOf(s.get('ID'));
            lstRet.add(obj);
        }}
        catch(exception e)
        {}
        return JSON.serialize(lstRet) ;
    }
    
    /************************************************************************************
* Method       :    getUIThemeDescription
* Description  :    Current theme of logged in user
* Parameter    :    NIL
* Return Type  :    String in JSON format
*************************************************************************************/
    @AuraEnabled
    public static String getUITheme() {
        return UserInfo.getUiThemeDisplayed();
    }
    
    //Wrapper class
    public class ResultWrapper {
        @AuraEnabled
        public String objName {get; set;}
        @AuraEnabled
        public String text {get; set;}
        @AuraEnabled
        public String val {get; set;}
        @AuraEnabled
        
        public String isSelected {get; set;}
        
        @AuraEnabled
        
        public String eventrelatinids {get; set;}
    }
    
    @AuraEnabled
    public static String saveEvent ( Id eventRecId, string attendeeIds) {
        list<ResultWrapper> rwp;
        // Forming a  string map to return response
        Map<String,String> resultMap = new Map<String,String>();
        rwp= (List<ResultWrapper>)JSON.deserialize(attendeeIds, List<ResultWrapper>.class);
        List<EventRelation> eventRelationList =  new List<EventRelation>();
        List<EventRelation> eventRelationListDelete =  new List<EventRelation>();
        
        // Adding Attendee Ids to the Event
        for(ResultWrapper attendee : rwp){
            EventRelation  ea = new EventRelation();
            ea.EventId  = eventRecId ;            
            ea.RelationId = attendee.val;
            if(attendee.isSelected=='True'){
                eventRelationList.add(ea);
            }
            
            if(attendee.isSelected=='false'){
                eventRelationListDelete.add(ea);                
            }            
        }
        Set<Id> inviteesIds = new Set<Id>();
        List<ResultWrapper> lstRet = new List<ResultWrapper>();
        Database.SaveResult[] srList = Database.insert(eventRelationList,false);
        for (Database.SaveResult sr : srList) {
            inviteesIds.add(sr.getId());
        }
        if(!inviteesIds.isEmpty()) {
            List<sObject> sobjList = [Select Id, RelationId, Relation.Name From EventRelation where Id In: inviteesIds]; 
            for (SObject s : sobjList) {
                ResultWrapper obj = new ResultWrapper();
                obj.objName = 'EventRelation';
                obj.text = String.valueOf(s.getSobject('Relation').get('Name') );
                obj.val = String.valueOf(s.get('RelationId'))  ;
                obj.eventrelatinids=String.valueOf(s.get('ID'));
                lstRet.add(obj);
            }
        }
        resultMap.put('status', 'success');
        resultMap.put('message', 'Attendee Added Successfully'); 
        return JSON.serialize(lstRet) ;
        
    } 
    @AuraEnabled
    public static void deleteEvent ( Id eventRecId, string attendeeIds, String delRecordIds) {
        
        system.debug('eventRecId '+eventRecId);
        system.debug('deleting records'+delRecordIds);
        if(delRecordIds != null) {
            try{
                Database.delete(new EventRelation(Id=delRecordIds),false);
            }
            catch(Exception e){
                Delete[SELECT id ,RelationId  
                                    FROM EventRelation
                                    WHERE eventId=: eventRecId AND  RelationId  =: delRecordIds];
            }
            
        }
    } 
}