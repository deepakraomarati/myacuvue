public with sharing class CustomerRequestInsertExtension {

    Public list<Product2> prodlst{get; set;}
    Public List<productinsertwrapper> prodlstwrap {get; set;}
    private integer totalRecs = 0;
    private integer OffsetSize = 0;
    private integer LimitSize= 20;
    public string customermasterreqid;
    Public id pordids{get; set;}
    Map<id,integer> quantitymap;
    List<JJ_JPN_InitialStock__c> existingprodlst;
    Map<id,JJ_JPN_InitialStock__c> initialstockmap;
    
    public CustomerRequestInsertExtension(ApexPages.StandardController controller) { 
       quantitymap = New Map<id,integer> ();
       prodlstwrap = New List<productinsertwrapper>();
       existingprodlst = New List<JJ_JPN_InitialStock__c>();
       initialstockmap = New Map<Id,JJ_JPN_InitialStock__c> ();
       customermasterreqid = ApexPages.currentPage().getParameters().get('id');
       searchAccounts();  
    }
    
    public Void searchAccounts(){
        if(prodlst != null && !prodlst.isEmpty()){
            prodlst.clear();
        }
        string country = 'JPN';
        String pType = 'JPN POP';
        String strQuery ='SELECT Id,Name,ProductCode,BrandCode__c,BaseCurve__c,JJ_JPN_Power__c,JJ_JPN_TotalQuantity__c,JJ_JPN_CountryCode__c,JJ_JPN_AlignmentOrderSequence__c FROM Product2 WHERE ProductType__c !=: pType AND JJ_JPN_CountryCode__c=:country';
        if(totalRecs !=null && totalRecs ==0){
            List<Product2> prodTemp = Database.query(strQuery);
            totalRecs = (prodTemp !=null &&prodTemp.size()>0)?prodTemp.size():0;
        }
        system.debug('totalRecs................'+totalRecs);
        //strQuery += 'ORDER BY Name,JJ_JPN_AlignmentOrderSequence__c  ASC,CreatedDate DESC LIMIT :LimitSize OFFSET :OffsetSize'; 
        strQuery += ' ORDER BY JJ_JPN_AlignmentOrderSequence__c  ASC LIMIT :LimitSize OFFSET :OffsetSize'; 
        prodlst  =Database.query(strQuery);
        system.debug('prodlst................'+prodlst.size());
        prodlstwrap = New List<productinsertwrapper> ();
        for(Product2 prod: prodlst){
            if(quantitymap.containskey(prod.id)){
                prodlstwrap.add(New productinsertwrapper(prod,false,quantitymap.get(prod.id)));
            }
            else{
                prodlstwrap.add(New productinsertwrapper(prod,false,0));
            }   
        }
    }
    public void FirstPage()
    {
        OffsetSize = 0;
        searchAccounts();
    }
    public void previous()
    {
        OffsetSize = (OffsetSize-LimitSize);
        searchAccounts();
    }
    public void next()
    {
        
        OffsetSize = OffsetSize + LimitSize;
        system.debug('OffsetSize...............'+OffsetSize);
        searchAccounts();
    }
    
    Public pagereference Saveproduct(){
        existingprodlst =[SELECT Id,JJ_JPN_Product__c,JJ_JPN_Customer_Master_Request__c,JJ_JPN_Quantity__c FROM JJ_JPN_InitialStock__c WHERE JJ_JPN_Customer_Master_Request__c =:customermasterreqid];
        for(JJ_JPN_InitialStock__c initialstk :existingprodlst){
            initialstockmap.put(initialstk.JJ_JPN_Product__c,initialstk);    
        }
        List<JJ_JPN_InitialStock__c> initialstockinsertlst = New List<JJ_JPN_InitialStock__c>();
        List<JJ_JPN_InitialStock__c> initialstockupdatelst = New List<JJ_JPN_InitialStock__c>();
        system.debug('outside..........');
        for(id newprodmap : quantitymap.keyset()){
            if(initialstockmap.containskey(newprodmap)){            
                JJ_JPN_InitialStock__c initialstockupdate = New JJ_JPN_InitialStock__c(id = initialstockmap.get(newprodmap).id);
                initialstockupdate.JJ_JPN_Quantity__c = initialstockmap.get(newprodmap).JJ_JPN_Quantity__c+quantitymap.get(newprodmap);
                initialstockupdatelst.add(initialstockupdate );
            }
            else{
                JJ_JPN_InitialStock__c initialstockinsert = New JJ_JPN_InitialStock__c();
                initialstockinsert.JJ_JPN_Product__c = newprodmap;
                initialstockinsert.JJ_JPN_Customer_Master_Request__c = customermasterreqid;
                //system.debug('initialstock.JJ_JPN_Customer_Master_Request__c..........'+initialstock.JJ_JPN_Customer_Master_Request__c);
                initialstockinsert.JJ_JPN_Quantity__c = quantitymap.get(newprodmap);
                initialstockinsertlst.add(initialstockinsert );
            }                             
        }
        if(!initialstockinsertlst.IsEmpty()){
            database.Insert(initialstockinsertlst,false);

        }
        if(!initialstockupdatelst.IsEmpty()){
            database.Update(initialstockupdatelst,false);

        }
        
        if(initialstockinsertlst.IsEmpty() && initialstockupdatelst.IsEmpty()){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.info,'Please enter quntity for atleast one product or click cancel to close'));
            return null;
        }
        Pagereference ref = New Pagereference('/'+customermasterreqid);
        ref.setredirect(true);        
        return ref;
       
    }
    public boolean getprev()
    {    
        if(OffsetSize == 0){    
            return true;
        }
        else {    
            return false;
        }
    }

    public boolean getnxt()
    {
        if((OffsetSize + LimitSize) > totalRecs){       
            return true;
        }
        else {        
            return false;
        }
    }
    
    Public void prepareproduct(){
    system.debug('pordids..........'+pordids);
        for(productinsertwrapper selectedprod : prodlstwrap){
            if(pordids == selectedprod.prodwrap.id && selectedprod.quantitywrap!=0){
                quantitymap.put(pordids,selectedprod.quantitywrap);    
            }
        }    
    }
    
    Public class productinsertwrapper{
        public product2 prodwrap{get; set;}
        public boolean ischeckwrap{get; set;}
        public integer quantitywrap{get; set;}
        Public productinsertwrapper(){
            }
        Public productinsertwrapper(product2 prod,boolean ischeck,integer quantity){
            prodwrap = prod;
            ischeckwrap = ischeck;
            quantitywrap = quantity;      
        }
    }

}