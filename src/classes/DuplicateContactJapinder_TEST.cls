/*
* File Name: DuplicateContactJapinder_TEST
* Description : Test method to cover business logic around DuplicateContactJapinder
* Copyright : Johnson & Johnson
* @author : Lhawang Bhutia | lbhutia@its.jnj.com | lhawang.bhutia@cognizant.com
* Modification Log
* ================================================================
*    Ver  |Date         |Author                |Modification
*    1.0  |8-Feb-2017   |lbhutia@its.jnj.com   |Class Modified
* =============================================================== **/
@IsTest
public class DuplicateContactJapinder_TEST{
    
    static testMethod void testmethod1(){
       	Test.startTest();
			final Account testacc = New Account(Name ='Test Account',PublicAddress__c='Bangalore', SalesRep__c = 'standtd', AccountNumber = '12345', OutletNumber__c = '12345', CountryCode__c = 'AUS');
            insert testacc;
			final Contact testContact = New Contact(FirstName ='Test',LastName ='Contact',NRIC__c = '837Q',accountid = testacc.id, CurrencyIsoCode = 'HKD');
            insert testContact;
			system.assertEquals(testContact.FirstName,'Test','Success');
        Test.stopTest();
   }
}