/**
 * File Name: ECPLocators 
 * Description : RESTful Web Service to send ECP Locator details of Outlets to Canvas
 * Copyright : Johnson & Johnson
 * @author : Shobana K G | skg6@its.jnj.com | Shobana.KG@cognizant.com
 * 
 * Modification Log 
 * =============================================================== 
 *  Ver  |Date         |Author                |Modification
 *  1.0  |15-May-2018  |skg6@its.jnj.com      |New Class created
 *  1.1  |23-May-2018  |skg6@its.jnj.com      |Implement POST request
 *  1.2  |29-May-2018  |skg6@its.jnj.com      |Change input/output params and logic with GET request
 *  1.3  |22-Jun-2018  |skg6@its.jnj.com      |Handle request for eVoucher accounts, retreive accounts available with 
 *                                              required parameters and added parameters Email and Locality in fetched accounts
 *  1.4  |25-Jun-2018  |skg6@its.jnj.com      |Change case of response fields from camel to Pascal as per Canvas request(single codebase)
 *  1.5  |17-Jul-2018  |skg6@its.jnj.com      |Replace filter fields from loop to query
 *  1.6  |27-Aug-2018  |skg6@its.jnj.com      |Handle request for Store Locator, eTrial/eVoucher with Store Locator separately as Canvas neither 
 *                                             can send 'eVoucher' as value for parameter 'practiceType' nor create multiple request for single site.
 *  1.7  |23-Oct-2018  |skg6@its.jnj.com      |Display ECP Name instead of Account Name in AU Canvas Site store search results
 *  1.8  |31-Oct-2018  |skg6@its.jnj.com      |Display Outlet Email (OutletEmail__c) instead of Account Email (EmaiL__c) in AU Canvas Site store search results
 *  1.9  |20-Nov-2018  |skg6@its.jnj.com      |Display eligible accounts based on product-ID_Key for HK Site - Store Locator
 *  2.0  |27-Dec-2018  |skg6@its.jnj.com      |Display eligible accounts based on product-ID_Key for HK Site - Store Locator in eTrial Page
 */ 

@RestResource(urlMapping='/apex/Practices/*')
global with sharing class ECPLocators {
    
    public static final String  REQUIRED = 'B2C0022';
    public static final String  INVALID_DATA = 'B2C0021';
    public static final String  ERROR_TYPE = 'PracticesServices';
    

    /**
     * Description - Handle GET HttpRequest
     * Input - longitude, latitude, unitsOfMeasure, radius, practiceType, campaign, additionalType and isEVoucher
     * Output - JSON response with list of outlets matching input params 
     */
    @HttpGet 
    global static void getOutletDetails() {
        
        /* Request Example - For Sites with Store Locator page alone - used for SG 
         * 					 /services/apexrest/apex/Practices/longitude/latitude/unitsOfMeasure/radius/practiceType 
         *                   for example - /services/apexrest/apex/Practices/-0.1277583/51.5073509/km/5/All 
         *                                                      (or)
         *                   For Sites with Store Locator page as well as eTrial and/or eVoucher pages - used for AU/IN 
         * 					 /services/apexrest/apex/Practices/longitude/latitude/unitsOfMeasure/radius/practiceType/campaign/additionalType/isEVoucher
         *                   for example - /services/apexrest/apex/Practices/-0.1277583/51.5073509/km/5/All - IN Store Locator and eTrial pages
         *                                 /services/apexrest/apex/Practices/-0.1277583/51.5073509/km/5/All///1 - AU Store Locator, AU eVoucher and IN eVoucher pages
         * 					 									(or)
         * 					 URL is different for HK
         * 					 HK Store Locator page - /services/apexrest/apex/Practices/longitude/latitude/unitsOfMeasure/radius/practiceType
         * 					 for example - /services/apexrest/apex/Practices/-0.1277583/51.5073509/km/5/All
         * 					 HK eTrial page  - /services/apexrest/apex/Practices/longitude/latitude/unitsOfMeasure/radius/practiceType/campaign/additionalType/isEVoucher/webcampaign/country
         * 					 for example - /services/apexrest/apex/Practices/-0.1277583/51.5073509/km/5/All////01/HK					      
         */
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String isEVoucher, radius, unitsOfMeasure, latitude, longitude, webCampaign, country;
        
        // Get Request Parameters from request URL
        String[] UriParams = req.requestURI.split('/');
        system.debug('List of params in URI: ' + UriParams.size() + UriParams);
        
        // Show error message if expected size of parameters are not available
        if (UriParams.size() < 8) {
            ResponseWrapper.createErrorResponse(res, null, REQUIRED, ERROR_TYPE);
            
        } else if (UriParams.size() == 8) { // Retrieve request parameters from request URI without isEVoucher param 
            radius = UriParams[UriParams.size()-2];
            unitsOfMeasure = UriParams[UriParams.size()-3];
            latitude = UriParams[UriParams.size()-4]; 
            longitude = UriParams[UriParams.size()-5];
            fetchOutlets(isEVoucher, radius, unitsOfMeasure, latitude, longitude, webCampaign, res);
            
        } else if (UriParams.size() == 11) { // Retrieve request parameters from request URI with isEVoucher param
            isEVoucher = UriParams[UriParams.size()-1];
            radius = UriParams[UriParams.size()-5];
            unitsOfMeasure = UriParams[UriParams.size()-6];
            latitude = UriParams[UriParams.size()-7]; 
            longitude = UriParams[UriParams.size()-8];
            fetchOutlets(isEVoucher, radius, unitsOfMeasure, latitude, longitude, webCampaign, res);
            
        } else if (UriParams.size() == 13) { // Retrieve request parameters from request URI with params - webCampaign and country
            country = UriParams[UriParams.size()-1];
            webCampaign = UriParams[UriParams.size()-2];
            radius = UriParams[UriParams.size()-7];
            unitsOfMeasure = UriParams[UriParams.size()-8];
            latitude = UriParams[UriParams.size()-9]; 
            longitude = UriParams[UriParams.size()-10];
            fetchOutlets(isEVoucher, radius, unitsOfMeasure, latitude, longitude, webCampaign, res);
            
        } else {
            ResponseWrapper.createErrorResponse(res, null, INVALID_DATA, ERROR_TYPE); // Error message for unexpected/invalid request     
        }        
        
    }
    
    /**
     * Description - Fetch list of Outlets
     * Input - isEVoucher, radius, unitsOfMeasure, latitude, longitude, res
     * Output - JSON response with list of outlets matching input params 
     */
    private static void fetchOutlets(String isEVoucher, String radius, String unitsOfMeasure, String latitude, String longitude, 
                                     String webCampaign, RestResponse res) {
        
        // List of Output Fields and Google Map Required Fields
        List<String> outputFldList = new List<String>{'Id','PublicAddress__c','PublicPhone__c','ECP_Name__c','Geolocation__Longitude__s',
                                                        'Geolocation__Latitude__s','Email__c','OutletEmail__c','Locality__c','CountryCode__c',
            											'PublicZone__c', 'ProductID_Key__c'};
        String outputFields = String.join(outputFldList, ', ');
        List<String> googleMapReqdFields = new List<String>{'PublicAddress__c','ECP_Name__c','Geolocation__Longitude__s',
                                                            'Geolocation__Latitude__s','PublicZone__c'};
        // Get country codes of stores for which Outlet Email to be shown in canvas sites
        Map<String,Canvas_Outlet_Email_CountryCodes__c> countryCodeMap = Canvas_Outlet_Email_CountryCodes__c.getAll(); 
                                        
        try {
            // Get list of outlets matching input params
            String ecpQuery = 'SELECT ' + String.escapeSingleQuotes(outputFields) + ' FROM Account WHERE ActiveYN__c = true AND';
            
            for (String fld : googleMapReqdFields) {
                ecpQuery = ecpQuery + ' '+fld+' != null AND';
            }
            
            if (isEVoucher == '1') {
                ecpQuery = ecpQuery + ' Marketing_Program__c INCLUDES(\'e-voucher\') AND';    
            }
            
            ecpQuery = ecpQuery + ' DISTANCE(GeoLocation__c,GEOLOCATION('+latitude+','+longitude+'),\''+unitsOfMeasure+'\') < '+radius+'';
            ecpQuery = ecpQuery + ' ORDER BY DISTANCE(GeoLocation__c,GEOLOCATION('+latitude+','+longitude+'),\''+unitsOfMeasure+'\') ASC';
            
            // Get list of accounts filtered based on ProductID-Key for HKG and not filtered for other countries 
            List<Account> accList = getAccountsList(Database.query(ecpQuery), webCampaign);
            
            // Convert outlet parameters of type ECPLocators if account list is not empty
            List<ECPLocators> ecpLocList = new List<ECPLocators>();
            if (accList.isEmpty()) {
                ResponseWrapper.createErrorResponse(res, null, INVALID_DATA, ERROR_TYPE);    
            } else {
                for (Account acc : accList) {
                    ecpLocList.add(new ECPLocators(acc.Id, acc.PublicAddress__c, acc.PublicPhone__c, acc.ECP_Name__c, 
                                                   acc.Geolocation__Longitude__s, acc.Geolocation__Latitude__s, 
                                                   (acc.CountryCode__c != null && countryCodeMap.containsKey(acc.CountryCode__c.toUpperCase())) ? acc.OutletEmail__c : acc.Email__c,
                                                   acc.Locality__c, acc.CountryCode__c, acc.PublicZone__c));    
                }
                res.addHeader('Content-Type', 'application/json');
                res.responseBody = Blob.valueOf('{ "Practices":' + JSON.serialize(ecpLocList) + '}');                
            }                
        } catch (Exception e) {
            system.debug('Exception in ECPLocators: ' + e);
            ResponseWrapper.createErrorResponse(res, null, INVALID_DATA, ERROR_TYPE);
        }
        
    }
    
    /**
     * Description - Fetch final list of Outlets based on ProductID-Key
     * Input - accList
     * Output - Final List of accounts
     */
    private static List<Account> getAccountsList(List<Account> accList, String webCampaign) {
        List<Account> finalAccList = new List<Account>(); 
        for (Account acc : accList) {
            if (acc.CountryCode__c == 'HKG') { // for HKG Accounts
            	List<String> productKeyList = (acc.ProductID_Key__c != null) ? (acc.ProductID_Key__c.split(';')) : new List<String>();
                for (String str : productKeyList) {
                    system.debug('key: ' + str.right(1));
                    if (webCampaign != null && webCampaign == '01') { // for HK eTrial page 
                        if (str.right(1) == '2' || str.right(1) == '3') {
                            finalAccList.add(acc);
                    		break;
                        } 
                    } else { // for HK Store Locator page
                        if (str.right(1) == '1' || str.right(1) == '3') {
                    		finalAccList.add(acc);
                    		break; 
                        }                               
                    }                    
                }                                
            } else { // for other than HKG Accounts
            	finalAccList.add(acc);    
            }  
        }        
        return (finalAccList.isEmpty() ? new List<Account>() : finalAccList);        
    }
    
    
    // DTO properties to return ECPLocator details to Canvas
    public String StreetAddress {get;set;}
    public String PhoneNumber {get;set;}
    public String Name {get;set;}
    public Decimal Longi {get;set;}
    public Decimal Lat {get;set;}
    public String Id {get;set;}
    public String Email {get;set;}
    public String County {get;set;}
    public String Country {get;set;}
    public String City {get;set;}    

    // Constructor
    public ECPLocators(String id, String publicAddress, String publicPhone, String ecpName, Decimal longitude,  
                         Decimal latitude, String email, String locality, String countryCode, String publicZone) {
        this.StreetAddress = publicAddress;
        this.PhoneNumber = publicPhone;
        this.Name = ecpName;
        this.Longi = longitude;
        this.Lat = latitude;
        this.Id = id;
        this.Email = email;
        this.County = locality;
        this.Country = countryCode;
        this.City = publicZone;
    }
   
}