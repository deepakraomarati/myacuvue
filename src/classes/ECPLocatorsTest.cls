/**
 * File Name: ECPLocatorsTest 
 * Description : Test Class for ECPLocators API
 * Copyright : Johnson & Johnson
 * @author : Shobana K G | skg6@its.jnj.com | Shobana.KG@cognizant.com
 * 
 * Modification Log 
 * =============================================================== 
 *  Ver  |Date         |Author                |Modification
 *  1.0  |15-May-2018  |skg6@its.jnj.com      |New Class created
 *  1.1  |24-May-2018  |skg6@its.jnj.com      |Cover code for handling POST request
 *  1.2  |29-May-2018  |skg6@its.jnj.com      |Cover code for change in input/output params and logic with GET request
 *  1.3  |28-Aug-2018  |skg6@its.jnj.com      |Cover code for change in request for eVoucher
 *  1.4  |21-Nov-2018  |skg6@its.jnj.com      |Cover method 'getAccountsList' for HK Site - Store Locator page
 *  1.5  |28-Dec-2018  |skg6@its.jnj.com      |Cover method 'getAccountsList' for HK Site - Store Locator in eTrial page
 */

@isTest
private class ECPLocatorsTest {
    
    private static final Integer ACCMAX = 10;
    private static final String REQUIRED = 'Required Parameters are missing';
    private static List<Account> insertedAccts = new List<Account>();
    
    private static void setupTestData() {
        List<Account> accList = new List<Account>();
        for (Integer i=1; i<=ACCMAX; i++) {
            if (i<=5) {
                accList.add(new Account(Name = 'Account'+i, ECP_Name__c = 'Test Outlet'+i, PublicAddress__c = 'F2, Mahogany', 
                                        CountryCode__c = 'IN', PublicZone__c = 'Bangalore', GoogleMapX__c = 51.5074, GoogleMapY__c = -0.1278, 
                                        ActiveYN__c = true, GeoLocation__Latitude__s = 51.5074, GeoLocation__Longitude__s = -0.1278,
                                        Marketing_Program__c = 'e-voucher', ProductID_Key__c = '1-3;2-3;3-3;5-3;6-3;7-3;8-3;9-3;10-3'));
            } else {
                accList.add(new Account(Name = 'Account'+i, ECP_Name__c = 'Test Outlet'+i, PublicAddress__c = 'F2, Mahogany', 
                                        CountryCode__c = 'HKG', PublicZone__c = 'Bangalore', GoogleMapX__c = 51.5074, GoogleMapY__c = -0.1278, 
                                        ActiveYN__c = true, GeoLocation__Latitude__s = 51.5074, GeoLocation__Longitude__s = -0.1278,
                                        ProductID_Key__c = '1-3;2-3;3-3;5-3;6-3;7-3;8-3;9-3;10-3')); 
            }
        }
		insert accList;
		insertedAccts = [SELECT Id, ECP_Name__c from Account LIMIT :ACCMAX];
        
        // Create custom setting - CustomException__c
		List<CustomException__c> custExcp = new List<CustomException__c>();
        Map<String,String> custExcpMap = new Map<String,String>{'B2C0021' => 'Sorry, Accounts not available', 
            													'B2C0022' => 'Required Parameters are missing'};
        for (String ce : custExcpMap.keySet()) {
        	custExcp.add(new CustomException__c(Name = ce, Error_Description__c = custExcpMap.get(ce)));	        
        }
        insert custExcp;  
    }
    
    
    @isTest
    private static void getOutletDetails_expectErrorMsg() {
        // Setup Test Data
        setupTestData();
        
        // Execute Test
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/apex/Practices/-0.1277583/51.5073509/km/10'; 
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        ECPLocators.getOutletDetails();
		Test.stopTest();
        
        // Assert
        system.assert(res.responseBody.toString().contains(REQUIRED), 'All required parameters are provided');        
    }
    
    @isTest
    private static void getOutletDetails_expectAllResponse() {
        // Setup Test Data
        setupTestData(); 
        
        // Execute Test
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/apex/Practices/-0.1277583/51.5073509/km/5/All'; 
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        ECPLocators.getOutletDetails();
   		Test.stopTest(); 
        
        // Assert
        system.assert(res.responseBody.toString().contains(insertedAccts.get(0).ECP_Name__c), 'No records matches input criteria');       
    }
    
    @isTest
    private static void getOutletDetails_expectEVoucherResponse() {
        // Setup Test Data
        setupTestData(); 
        
        // Execute Test
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/apex/Practices/-0.1277583/51.5073509/km/5/All///1'; 
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        ECPLocators.getOutletDetails();
   		Test.stopTest(); 
        
        // Assert
        system.assert(res.responseBody.toString().contains(insertedAccts.get(0).ECP_Name__c), 'No records matches input criteria');       
    }
    
    @isTest
    private static void getOutletDetails_expectHKETrialResponse() {
        // Setup Test Data
        setupTestData(); 
        
        // Execute Test
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/apex/Practices/-0.1277583/51.5073509/km/5/All////01/HK'; 
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        ECPLocators.getOutletDetails();
   		Test.stopTest(); 
        
        // Assert
        system.assert(res.responseBody.toString().contains(insertedAccts.get(0).ECP_Name__c), 'No records matches input criteria');       
    }
    
}