Public without sharing class ETServiceUtils {
   
   static final String AUTH_ERROR = 'Error: An authentication error occurred - please check the username and password and try again.';
   public static String AUTH_TOKEN;
  
    public static Map<String, ExactTarget_Integration_Settings__c> serviceSettings {
      get {
        if (serviceSettings==null || serviceSettings.size()==0) {
          serviceSettings = new Map<String, ExactTarget_Integration_Settings__c> (ExactTarget_Integration_Settings__c.getAll());
        }
          return serviceSettings;
      }  
      set;
    }
  
  public static String getSoapHeader()
    {
        String soapMessage = '';
        soapMessage+='<soapenv:Header>';
        if (serviceSettings.get('username')==null || serviceSettings.get('username').Value__c=='' || serviceSettings.get('password')==null || serviceSettings.get('password').Value__c=='') {
          //throw new CustomException('ERROR:','ExactTarget');
        }else {
           soapMessage+='<Security xmlns="'+serviceSettings.get('SECURITY_ENDPOINT').Value__c+'">';
          soapMessage+='<UsernameToken>';
              soapMessage+='<Username>'+serviceSettings.get('username').Value__c+'</Username>';
              soapMessage+='<Password>'+serviceSettings.get('password').Value__c+'</Password>';
          soapMessage+='</UsernameToken>';
          soapMessage+='</Security>';
        }
        soapMessage+='</soapenv:Header>';
        return soapMessage;
    }
    
    
   public class restSecurity {
    public String accessToken;
    } 
     
  public static void getRESTAuthToken() {
      String requestBody = '';
      requestBody+='{';
          requestBody+='"clientId":"'+serviceSettings.get('ET_CLIENT_ID').Value__c+'",';
          requestBody+='"clientSecret":"'+serviceSettings.get('ET_CLIENT_SECRET').Value__c+'"';
      requestBody+='}';
      String requestResponse = processRESTRequest(serviceSettings.get('ET_AUTH_ENDPOINT').Value__c, requestBody, 'POST');
      System.debug('### OUTPUT >>>>> getRESTAuthToken: requestBody: '+requestBody);
      System.debug('### OUTPUT >>>>> getRESTAuthToken: requestResponse: '+requestResponse);
      restSecurity sec = (restSecurity)JSON.deserialize(requestResponse, restSecurity.class);
      if (sec!=null) AUTH_TOKEN = sec.accessToken;
  }
    
   public static String processRESTRequest(String ep, String msg, String method) {
        String doc;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod(method);
        req.setHeader('Content-Type', 'application/json');
        System.debug('utilEndPoint:'+ep);
        req.setEndpoint(ep);
         System.debug('utilMsg:'+msg);
        if (msg!=null) req.setBody(msg);       
        req.setTimeout(60000);
        
        System.debug('REQ ###'+req);
        HttpResponse res;
        if (!Test.isRunningTest()) {
       
            System.debug('### OUTPUT >>>>> WebServiceExactTarget: processRESTRequest: req: '+String.valueOf(req));
            res = h.send(req);   
            System.debug('### OUTPUT >>>>> WebServiceExactTarget: processRESTRequest: res: '+String.valueOf(res));
            doc = res.getBody();
            System.debug('DOC:'+doc);
          } else {
            // Test version of response for Apex Unit Tests
          MockHttpResponseGenerator mc = new MockHttpResponseGenerator();
          res = mc.respond(req);             
            doc = res.getBody();
        }

        return doc;
    }
    
    public static Dom.Document processRequest(String ep, String msg, String action) {
          Dom.Document doc;
        
          Http h = new Http();
          HttpRequest req = new HttpRequest();
          req.setMethod('POST');
          req.setHeader('Content-Type', 'text/xml');
          req.setHeader('SOAPAction', action);
          req.setEndpoint(ep);
          req.setBody(msg);
          req.setTimeout(60000);

          HttpResponse res;
          if (!Test.isRunningTest()) {
          	  System.debug('### OUTPUT >>>>> WebServiceExactTarget: processRequest: req: '+String.valueOf(req));
	          res = h.send(req);   
	          System.debug('### OUTPUT >>>>> WebServiceExactTarget: processRequest: res: '+String.valueOf(res));
	          doc = res.getBodyDocument();
	          System.debug('####'+res.toString());
	          System.debug('####'+res.getStatus());
	          System.debug('####'+res.getBody());	
          }else {
            // Test version of response for Apex Unit Tests
            System.debug('IsTest::');
              MockHttpResponseGenerator mc = new MockHttpResponseGenerator();
              res = mc.respond1(req);             
              doc = res.getBodyDocument();
        }
          
          
          return doc;
    }
    
       public static String cleanPhone(String phone) {
        if(phone==null || phone == '') return '';
        /*Pattern numberPattern = Pattern.compile('[^\\d,]');
        String pnumber = numberPattern.matcher(phone).replaceAll('');
    
        // we are expecting it to start with '0' or '00' or '44'
        // if it doesn't then we should return and not clean it up
        if (pnumber != null) {
            pnumber = pnumber.replaceAll('^0044','44'); 
            pnumber = pnumber.replaceAll('^0','44');
        } */  
        if(phone==null || phone == '') return '';
    else{
      String s = '';
      if(phone.contains('+')){
            System.debug('String:'+phone);
            String str = phone;
            string[] test = str.split(' ');
        String ss;
        if(test[0].contains('+')){
            s = test[0].replace('+','');
            System.debug('String:'+s);
        }else{
          s = test[0];
        }
        if(test[1].startsWith('0')){
            ss = test[1].replaceAll('^0','');
            System.debug('String1:'+ss);
        }else{
          ss = test[1];
        }
        s =s+ss;
        System.debug('New'+s); 
        return s; 
      }else{
        if(phone.startsWith('^0')){
          phone = phone.replaceAll('^0','44');
        }        
        return phone;
      }  
    }  
  } 
}