global without sharing class ET_AdministrationService
{
    /* --------------------------------------------
     * Author: 
     * Company: BICS
     * Description : Generate the ET CallWebservice.
     * ---------------------------------------------*/
    
    public static string endpoint;
    public static string channelMemberID;
    public static string PARTNER_NS = 'http://exacttarget.com/wsdl/partnerAPI';
    public static string SOAP_ENV = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:par="'+PARTNER_NS+'" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';
    
    public static String messagekey;
    public static String serviceRequest;
    public static String serviceResponse;

    //#######################################################
    //# DataEvent Send
    //#######################################################
    global static void createTriggeredDataEventSend(List<Etrial__c> lstet)
    {
        
        datavalue va=new datavalue();
        String Key;
        
        List<Etrial__c> lstetrial = new List<Etrial__c>();
  
        for(Etrial__c et:lstet)
        {
           va.ECPName = et.ecp_name__c;
           va.ECPAddress = et.ecp_address__c;
           va.ECPPhone = et.ecp_phone__c;
           va.ECPProvince = et.ecp_province__c;
           va.ECPState = et.ecp_state__c;
           va.Email = et.Account__r.Email__c;
           va.EtrialId = et.Name;
           va.OutletNumber = et.Account__r.OutletNumber__c;
           
           Key = et.Account__r.OutletNumber__c;
           va.AccountNumber= et.Account__r.AccountNumber;
           va.ActiveYN = et.Account__r.ActiveYN__c;
           va.AccountOwner = et.Account__r.Owner.Name;
           va.CountryCode = et.Account__r.CountryCode__c;
           
        }
        
         datasendrequest vl=new datasendrequest();
         vl.values = va;
        
        servicerequest = Json.serialize(vl);
        System.debug('SREQUEST ###'+servicerequest);
        
        try{
            messagekey = ETServiceUtils.serviceSettings.get('messagekey').Value__c;
        }catch(Exception e){
            
        }
        System.debug('KEY###'+key);
        
        if(key !=null)
        {
            ETServiceUtils.getRESTAuthToken();
            /*SonarQube Fix*/	   
            //System.debug('AUTHTOKEN ###'+ETServiceUtils.AUTH_TOKEN);
            
            endpoint = ETServiceUtils.serviceSettings.get('DataEvent_endpoint').Value__c+'key:'+messagekey+'/rows/OutletNumber:'+Key+'?access_token='+ETServiceUtils.AUTH_TOKEN;
            System.debug('ENDPOINT:'+endpoint);
        
            serviceResponse = ETServiceUtils.processRESTRequest(endpoint,servicerequest,'PUT');
            System.debug('SERVICE RESPONSE ###'+serviceResponse);
        }
        
        if(serviceResponse !=null && serviceResponse !='')
        {
            DataEventResponse dresponse = ET_AdministrationService.parse(serviceResponse);
            
            System.debug('DRES ###'+dresponse);
            
            if(dresponse.Keys !=null)
            {
                for(Etrial__c et:lstet)
                {
                    et.DataEvent_Last_Delivery_Datetime__c = Datetime.Now();
                    et.DataEvent_Sent__c = true;
                    lstetrial.add(et);
                }
            }
            
            if(lstetrial.size()>0 && !lstetrial.isEmpty())
            {
                update lstetrial;
            }
            
        }
        
    }
   
   //#######################################################
   //# Email Send
   //#######################################################
   public static void createTriggeredEmailSend(List<Etrial__c> lstem)
   {
       
     String TriggerSendId;
     String Displayname;
        
        List<Etrial__c> upEtrial = new List<Etrial__c>();
        
        try{
         endpoint = ETServiceUtils.serviceSettings.get('endpoint').Value__c;
        }catch(Exception e){
        }
        channelMemberID = ETServiceUtils.serviceSettings.get('Default Channel Member ID').Value__c;
        String soapMessage = '';
           soapMessage+=SOAP_ENV;
           soapMessage+=ETServiceUtils.getSoapHeader();
               soapMessage+='<soapenv:Body>';
                    soapMessage+='<CreateRequest xmlns="'+PARTNER_NS+'">';
                       soapMessage+='<Objects xsi:type="TriggeredSend">';
                      // soapMessage+='<Owner><Client><ID>'+channelMemberID+'</ID></Client></Owner>';
                     if(lstem.size()>0 && !lstem.isEmpty())
                     {
                        for(Etrial__c et:lstem)
                        {
                            if(et.Country__c !=null && et.Country__c !='')
                            {
                              ETRIAL_Trigger_Send_setting__c etsend=ETRIAL_Trigger_Send_setting__c.getInstance(et.Country__c);
                              TriggerSendId = etsend.RegistrationThankyou__c;
                            }
                              soapMessage+='<TriggeredSendDefinition>';
                              soapMessage+='<CustomerKey>'+TriggerSendId+'</CustomerKey>';
                              soapMessage+='</TriggeredSendDefinition>';
                              soapMessage+='<Subscribers>';
                               soapMessage+='<SubscriberKey>'+et.etrial_email__c+'</SubscriberKey>';
                                soapMessage+='<EmailAddress>'+et.etrial_email__c+'</EmailAddress>';
                                soapMessage+='<Attributes>';
                                soapMessage+='<Name>FirstName</Name><Value>'+et.etrial_first_name__c+'</Value>';
                                soapMessage+='</Attributes>';
                                soapMessage+='<Attributes>';
                                soapMessage+='<Name>LastName</Name><Value>'+et.etrial_last_name__c+'</Value>';
                                soapMessage+='</Attributes>';
                                soapMessage+='<Attributes>';
                                soapMessage+='<Name>Gender</Name><Value>'+et.etrial_gender__c+'</Value>';
                                soapMessage+='</Attributes>';   
                                soapMessage+='<Attributes>';
                                soapMessage+='<Name>Phone</Name><Value>'+et.etrial_phone__c+'</Value>';
                                soapMessage+='</Attributes>';   
                                soapMessage+='<Attributes>';
                                soapMessage+='<Name>Country</Name><Value>'+et.Country__c+'</Value>';
                                soapMessage+='</Attributes>';
                               
                                if(et.ecp_name__c !=null && et.ecp_name__c !=''){
                                    Displayname = et.ecp_name__c;
                                    
                                    if(Displayname.contains('&'))
                                    {
                                        Displayname = Displayname.replace('&','&amp;');
                                        System.debug('DISPALYNAME ###'+Displayname);
                                    }
                                }
                                
                                
                                if(et.Booking_date__c !=null)
                                {
                                    soapMessage+='<Attributes>';
                                        soapMessage+='<Name>BookingDate</Name><Value>'+et.Booking_date__c+'</Value>';
                                    soapMessage+='</Attributes>';
                                }
                                
                                if(et.Booking_time__c !=null && et.Booking_time__c !='')
                                {
                                    soapMessage+='<Attributes>';
                                        soapMessage+='<Name>BookingTime</Name><Value>'+et.Booking_time__c+'</Value>';
                                    soapMessage+='</Attributes>';
                                }
                                soapMessage+='<Attributes>';
                                soapMessage+='<Name>VisionCare</Name><Value>'+et.etrial_visioncare__c+'</Value>';
                                soapMessage+='</Attributes>';
                                
                                if(et.ecp_name__c !=null && et.ecp_name__c!='')
                                {
                                    soapMessage+='<Attributes>';
                                    soapMessage+='<Name>ECPName</Name><Value>'+Displayname+'</Value>';
                                    soapMessage+='</Attributes>';
                                }
                                
                                if(et.ecp_address__c !=null && et.ecp_address__c!='')
                                {
                                    soapMessage+='<Attributes>';
                                    soapMessage+='<Name>ECPAddress</Name><Value>'+et.ecp_address__c+'</Value>';
                                    soapMessage+='</Attributes>';
                                }
                                
                                if(et.ecp_phone__c !=null && et.ecp_phone__c!='')
                                {
                                    soapMessage+='<Attributes>';
                                    soapMessage+='<Name>ECPProvince</Name><Value>'+et.ecp_province__c+'</Value>';
                                    soapMessage+='</Attributes>';
                                }
                                
                                if(et.ecp_state__c !=null && et.ecp_state__c !='')
                                {
                                    soapMessage+='<Attributes>';
                                    soapMessage+='<Name>ECPState</Name><Value>'+et.ecp_state__c+'</Value>';
                                    soapMessage+='</Attributes>';
                                }

                                soapMessage+='<Attributes>';
                                soapMessage+='<Name>CreatedDate</Name><Value>'+et.CreatedDate+'</Value>';
                                soapMessage+='</Attributes>';
                                
                                if(et.Account__r.OutletNumber__c !=null && et.Account__r.OutletNumber__c!='')
                                {
                                    soapMessage+='<Attributes>';
                                    soapMessage+='<Name>outletNumber</Name><Value>'+et.Account__r.OutletNumber__c+'</Value>';
                                    soapMessage+='</Attributes>';
                                }
                                
                                 
                                if(et.Account__r.AccountNumber !=null && et.Account__r.AccountNumber !='')
                                {
                                    soapMessage+='<Attributes>';
                                    soapMessage+='<Name>AccountNumber</Name><Value>'+et.Account__r.AccountNumber+'</Value>';
                                    soapMessage+='</Attributes>';  
                                }
                                
                                if(et.Product_Name__c !=null & et.Product_Name__c !='')
                                {
                                    soapMessage+='<Attributes>';
                                    soapMessage+='<Name>Product</Name><Value>'+et.Product_Name__c+'</Value>';
                                    soapMessage+='</Attributes>';
                                }
                                
                                if(et.RedemptionCode__c !=null && et.RedemptionCode__c!='')
                                {
                                    soapMessage+='<Attributes>';
                                    soapMessage+='<Name>Redemption Code</Name><Value>'+et.RedemptionCode__c+'</Value>';
                                    soapMessage+='</Attributes>';
                                }
                                
                                if(et.ecp_phone__c !=null && et.ecp_phone__c!='')
                                {
                                    soapMessage+='<Attributes>';
                                    soapMessage+='<Name>ECPPhone</Name><Value>'+et.ecp_phone__c+'</Value>';
                                    soapMessage+='</Attributes>';
                                }
                               
                                soapMessage+='<Attributes>';
                                soapMessage+='<Name>ETrial ID</Name><Value>'+et.Id+'</Value>';
                                soapMessage+='</Attributes>';                            
                        }
                     }
                            soapMessage+='<Owner><Client><ID>'+channelMemberID+'</ID></Client></Owner>';
                            soapMessage+='</Subscribers>';
                        soapMessage+='</Objects>';
                    soapMessage+='</CreateRequest>';
              soapMessage+='</soapenv:Body>';
           soapMessage+='</soapenv:Envelope>';
           System.debug('ReqMessage:'+soapMessage);
          
          Dom.document response = ETServiceUtils.processRequest(endpoint, soapMessage, 'Create');
          System.debug('Response ####'+response);
          
          if (response!=null) {
          serviceResponse = response.toXmlString();
          System.debug('### OUTPUT >>>>> createTriggeredEmailSend: serviceResponse: '+serviceResponse);
          createTriggeredEmailSendResponse res = processResponse(response);
            if (res.overallStatus == 'OK')
              {
                  System.debug('test1::');
                  for(result re :res.result){
                    if(re.statusCode == 'OK')
                    {
                      System.debug('test2::');
                      for(Etrial__c et :lstem){         
                        et.Last_Delivery_Datetime__c = datetime.now();
                        et.Email_sent__c = true;
                        upEtrial.add(et);
                      }
                    }
                  }
                }
                if(upEtrial.size()>0 && upEtrial != null) update upEtrial;
                          
        }
           
       
   }
  
   //#######################################################
   //# SMS Send
   //#######################################################
   public static void createTriggeredSMSSend(List<Etrial__c> lstsms)
   {
    
      smsSendRequest sendReq=new smsSendRequest();
      List<String> lstmobile =new List<String>();
      List<Etrial__c> upEtrial = new List<Etrial__c>();
      
      String Phone;
      String Code;
      
        for(Etrial__c et:lstsms){
        
          if(et.Country__c !=null){
            ETRIAL_Trigger_Send_setting__c ets=ETRIAL_Trigger_Send_setting__c.getInstance(et.Country__c);
            messagekey = ets.SMSMessage__c;
            Code = ets.Code__c;
          }
          
          //lstmobile.add(ETServiceUtils.cleanPhone(et.etrial_phone__c));
           if(et.Country__c == 'TH'){
              
              if(et.etrial_phone__c.startsWith('0'))
              {
                string ph  = et.etrial_phone__c.replaceall('^0','');
                Phone = Code+ph;
                system.debug('Phone#####'+Phone);
                lstmobile.add(Phone);
              }else{
                Phone = Code+et.etrial_phone__c;
                lstmobile.add(Phone);
              }
          }
                                  
            
           if(et.Country__c == 'TW'){                           
              
              if(et.etrial_phone__c.startsWith('0'))
              {
                string ph  = et.etrial_phone__c.replaceall('^0','');
                Phone = Code+ph;
                system.debug('Phone#####'+Phone);
                lstmobile.add(Phone);
              }else{
                Phone = Code+et.etrial_phone__c;
                lstmobile.add(Phone);
              }
          }
                                  
            else{
              if(et.etrial_phone__c.contains('+'))
              {
                lstmobile.add(ETServiceUtils.cleanPhone(et.etrial_phone__c));
              }else{
                System.debug('WITHOUTSMS #####');
                Phone = Code+et.etrial_phone__c;
                System.debug('PHONE ####'+Phone);
                lstmobile.add(Phone);
              }
          }
      }
     
                                
      for(Etrial__c et :lstsms)
      {  
     /* If(et.Country__c == 'TW' ){
      system.debug('chaituCheck2');
       ETRIAL_Trigger_Send_setting__c ets=ETRIAL_Trigger_Send_setting__c.getInstance(et.Country__c);
           sendReq.boolOverride = true;
           sendreq.MessageText=System.Label.TWN_msg1 +' '+ +et.Product_Name__c+ +' '+ System.Label.TWN_msg2 + ' '+ et.ecp_name__c+ + ' '+ +System.Label.TWN;
         
       }*/
       If(et.Country__c == 'IN'){
       ETRIAL_Trigger_Send_setting__c ets=ETRIAL_Trigger_Send_setting__c.getInstance(et.Country__c);
           sendReq.boolOverride = true;
           //sendreq.MessageText=System.Label.ThankyouIndia +' '+ + et.etrial_visioncare__c + System.Label.SMSIndia1 +' '+ + et.ecp_name__c + +' '+ + et.ecp_address__c + + ' '+System.Label.SMSIndia3 + ' ' + + et.CreatedDate.addDays(30);
            Datetime dateTimestamp = et.CreatedDate;
            Date dateTemp = Date.newInstance(dateTimestamp .year(),dateTimestamp.month(),dateTimestamp.day());
            Date dateTemp1 = DateTemp.addDays(30);
            String ExpiryDate=dateTemp1.format();
          
          sendreq.MessageText=  System.Label.ThankyouIndia +' '+ + et.etrial_visioncare__c + System.Label.SMSIndia1 +' '+ + et.ecp_name__c + +' '+ + et.ecp_address__c + + '\n'+ + System.Label.SMSIndia4 + +' ' + + et.ecp_phone__c+ '\n'+System.Label.SMSIndia3 +' '+ +ExpiryDate;
           system.debug('chaituCheck1');
       }
       
       else{
           sendReq.boolOverride = false;
       }
       }
       sendReq.mobileNumbers = lstmobile;
       sendReq.Subscribe    = true;
       sendReq.keyword      = 'ETRIALTEST';
       System.debug('### OUTPUT >>>>> sendTriggeredSMS: sendReq: '+sendReq);
       serviceRequest = JSON.serialize(sendReq);
       System.debug('SMSReq:'+serviceRequest);
       
       serviceRequest = serviceRequest.replaceAll('boolOverride', 'Override');
       ETServiceUtils.getRESTAuthToken();
       endpoint = ETServiceUtils.serviceSettings.get('SMS_Endpoint').Value__c+messageKey+'/send?access_token='+ETServiceUtils.AUTH_TOKEN;
       System.debug('SMSEndPoint:'+endpoint);
       serviceResponse = ETServiceUtils.processRESTRequest(endpoint, serviceRequest, 'POST');
       System.debug('SRES:'+serviceResponse);  
       
       if (serviceResponse!=null && serviceResponse!='') {
                smsSendResponse response = new smsSendResponse();
                response = SMSprocessResponse(serviceResponse);
                /*SonarQube Fix*/	   
                //System.debug('TOKEN:'+response.tokenId);
                
                if (response.tokenId!=null && response.tokenId!='') {
                   
                    for(Etrial__c et :lstsms){         
                        et.SMS_Last_Delivery_Datetime__c = datetime.now();
                        et.SMS_Sent__c  = true;
                        upEtrial.add(et);
                      }
                }
                if(upEtrial.size()>0 && upEtrial != null) update upEtrial;
            }
   }
   
   
   
   
   //#######################################################
   //# CampaignMember SMS Send
   //#######################################################  

    public static void CampaignMemberTriggeredSMSSend(List<CampaignMember> lstsms)
   {
    
      smsSendRequest sendReq = new smsSendRequest();
      List<String> lstmobile = new List<String>();
      List<CampaignMember> upmember = new list<CampaignMember>();
      String Code;
      String Phone;
      
      for(CampaignMember et:lstsms){
        system.debug('##########'+et.contact.Phone);
          if(et.campaign.Country__c !=null){
            messagekey = et.Campaign.SMS_TriggerSend__c;
            Code = et.Campaign.Code__c;
          }
          
          if(et.contact.Phone.contains('+'))
          {
            lstmobile.add(ETServiceUtils.cleanPhone(et.contact.Phone));
          }else{
            System.debug('WITHOUTSMS #####');
            Phone = Code+et.contact.Phone;
            System.debug('PHONE ####'+Phone);
            lstmobile.add(Phone);
          }
          
      }
       sendReq.mobileNumbers = lstmobile;
       sendReq.Subscribe = true;
       sendReq.keyword = 'ETRIAL';
       sendReq.boolOverride = false;
       System.debug('### OUTPUT >>>>> sendTriggeredSMS: sendReq: '+sendReq);
       serviceRequest = JSON.serialize(sendReq);
       System.debug('SMSReq:'+serviceRequest);
       
       serviceRequest = serviceRequest.replaceAll('boolOverride', 'Override');
       ETServiceUtils.getRESTAuthToken();
       endpoint = ETServiceUtils.serviceSettings.get('SMS_Endpoint').Value__c+messageKey+'/send?access_token='+ETServiceUtils.AUTH_TOKEN;
       System.debug('SMSEndPoint:'+endpoint);
       serviceResponse = ETServiceUtils.processRESTRequest(endpoint, serviceRequest, 'POST');
       System.debug('SRES:'+serviceResponse);  
       
       if (serviceResponse!=null && serviceResponse!='') {
                smsSendResponse response = new smsSendResponse();
                response = SMSprocessResponse(serviceResponse);
                /*SonarQube Fix*/	   
                //System.debug('TOKEN:'+response.tokenId);
                
                if (response.tokenId!=null && response.tokenId!='') {
                   
                    for(CampaignMember et :lstsms){         
                        et.Reminder_Sent__c  = true;
                        upmember.add(et);
                      }
                }
                if(upmember.size()>0 && upmember != null) update upmember;
            }
   }
   
    public class smsSendRequest {
        public List<String> mobileNumbers {get;set;}
        public boolean boolOverride {get;set;}
        public boolean Subscribe    {get;set;}
        public String  keyword      {get;set;}
        Public string MessageText{get;set;}
        //public boolean Override1 {get;set;}
        
        public smsSendRequest() {}
        public smsSendRequest(List<String> subs, boolean rw,boolean sub,String key,String msgtext) {
            this.mobileNumbers = subs;
            this.boolOverride  = rw;
            this.Subscribe     = sub;
            this.keyword       = key;
            this.MessageText   = msgtext;
            //this.Override1      = override1;
        }
    }
   
   public class datasendrequest
   {
     public datavalue values {get;set;}
     
     public datasendrequest(){}
     
     public datasendrequest(datavalue values)
     {
        this.values = values;
     }
   }
   
   public class datavalue
   {
     public String ECPName {get;set;}
     public String ECPAddress {get;set;}
     public String ECPPhone {get;set;}
     public String ECPProvince {get;set;}
     public String ECPState {get;set;}
     public String Email {get;set;}
     public String EtrialId {get;set;}
     public String OutletNumber {get;set;}
     public String AccountNumber {get;set;}
     public boolean ActiveYN {get;set;}
     public String AccountOwner {get;set;}
     public String CountryCode {get;set;}
     
    
     public datavalue(){}
     public datavalue (String ECPName,String ECPAddress,String ECPPhone,String ECPProvince,String ECPState,String Email, String EtrialId,String OutletNumber, String AccountNumber,boolean ActiveYN,String AccountOwner,String CountryCode)
     {
        this.ECPName = ECPName;
        this.ECPAddress = ECPAddress;
        this.ECPPhone = ECPPhone;
        this.ECPProvince = ECPProvince;
        this.ECPState = ECPState;
        this.Email = Email;
        this.EtrialId = EtrialId;
        this.OutletNumber = OutletNumber;
        this.AccountNumber = AccountNumber;
        this.ActiveYN = ActiveYN;
        this.AccountOwner = AccountOwner;
        this.CountryCode = CountryCode;
     }
  }
  
   //#######################################################
   //# Email and SMS Response details
   //#######################################################
  
  public static createTriggeredEmailSendResponse processResponse(DOM.Document response) {
        createTriggeredEmailSendResponse Newresult = new createTriggeredEmailSendResponse();
        Newresult.result = new List<result>();
        //execute the code to process the response and return the class as a result
        DOM.XmlNode root = response.getRootElement();
        parseResponse(root, Newresult);
        return Newresult;
    }

    public static void parseResponse(DOM.XmlNode node, createTriggeredEmailSendResponse res) {
        if (node.getNodeType() == DOM.XmlNodeType.ELEMENT) {
            if (node.getName() == 'ErrorCode') res.errorCode = node.getText();
            if (node.getName() == 'OverallStatus') res.overallStatus = node.getText();
            if (node.getName() == 'Results') {
                result Send = new result();
                for (DOM.XmlNode ch: node.getChildren()) {
                    if (ch.getName() == 'statusCode') Send.statusCode = ch.getText();
                    if (ch.getName() == 'statusMessage') Send.statusMessage = ch.getText();
                    if (ch.getName() == 'OrdinalID') Send.OrdinalID = ch.getText();
                    if (ch.getName() == 'NewID') Send.NewID = ch.getText();
                }
                res.result.add(Send);
            }
            
            //check whether there are any child nodes
            for (DOM.XmlNode child: node.getChildElements()) {
                parseResponse(child, res);
            }
        }
    }

  
    public class createTriggeredEmailSendResponse {
        public String errorCode  {get;set;}
        public String overallStatus {get;set;}
        public List<result> result {get;set;}
        public createTriggeredEmailSendResponse() {} 
        public createTriggeredEmailSendResponse(String errorCode, String overallStatus, List<result> result){
            this.errorCode = errorCode;
            this.overallStatus = overallStatus;
            this.result = result;
        }    
    }
  
    public class result{
        public String statusCode {get;set;}
        public String statusMessage {get;set;}
        public String OrdinalID {get;set;}
        public String NewID {get;set;}
        public result(){}
        public result(String statusCode,String statusMessage, String OrdinalID, String NewID){
            this.statusCode = statusCode;
            this.statusMessage = statusMessage;
            this.OrdinalID = OrdinalID;
            this.NewID = NewID;
        }
    }
    
    public class smsSendResponse {
        String tokenId {get;set;}
        
        public smsSendResponse() {}
        public smsSendResponse(String t) {
            this.tokenId = t;
        }
    }
    
    public class DataEventResponse {
        
        public CL_Keys keys {get;set;}
        public datavalue2 values {get;set;}
        
        public DataEventResponse(){}        
        public DataEventResponse(CL_Keys keys,datavalue2 values)
        {
            this.keys = keys;
            this.values = values;
        }
    }
    
    
    public class CL_Keys {
        
        public String OutletNumber {get;set;}
        
        public CL_keys(String OutletNumber)
        {
            this.OutletNumber = OutletNumber;
        }
    }
    
    public static smsSendResponse SMSprocessResponse(String response) {
        smsSendResponse result = new smsSendResponse();
        result = (smsSendResponse)JSON.deserialize(response, smsSendResponse.class);
        return result;
    }
   
    public static DataEventResponse parse(String Json)
    {
        return (DataEventResponse) System.JSON.deserialize(json,DataEventResponse.class);
    }
    
    public class datavalue2
   {
     public String SubscriberKey{get;set;}
     public String FirstName {get;set;}
     public String LastName {get;set;}
     public String Email {get;set;}
     
     public datavalue2(){}
     public datavalue2 (String SubscriberKey,String FirstName,String LastName,String Email)
     {
        this.SubscriberKey = SubscriberKey;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Email  = Email;
     }
  }
}