global class ET_CallWebservice
{
    /* --------------------------------------------
     * Author: 
     * Company: BICS
     * Description : Generate the ET CallWebservice.
     * ---------------------------------------------*/
    
    @future(callout=true)
    global static void callDataEventAdmin(List<Id> etId)
    {
        System.debug('EVENT ###:'+etId);
        List<Etrial__c> et = [SELECT Id,
                                    etrial_email__c,
                                    etrial_first_name__c,
                                    etrial_last_name__c,
                                    etrial_gender__c,
                                    etrial_phone__c,
                                    Country__c,
                                    Booking_date__c,
                                    Booking_time__c,
                                    etrial_product__c,
                                    etrial_product__r.Name,
                                    ecp_name__c,
                                    ecp_address__c,
                                    ecp_phone__c,
                                    ecp_city__c,
                                    ecp_province__c,
                                    Account__r.Email__c,
                                    Account__r.Owner.Name,
                                    Name,
                                    Account__r.OutletNumber__c,
                                    Account__r.AccountNumber,
                                    Account__r.ActiveYN__c,
                                    Product_Name__c,
                                    Account__r.CountryCode__c,
                                    ecp_state__c FROM Etrial__c WHERE Id IN: etId];
        
        ET_AdministrationService.createTriggeredDataEventSend(et);
    }
    
    @future(callout=true)
    global static void callEmailAdmin(List<Id> emId)
    {
        System.debug('EMAIL ###:'+emId);
        List<Etrial__c> et = [SELECT Id,
                                    etrial_email__c,
                                    etrial_first_name__c,
                                    etrial_last_name__c,
                                    etrial_gender__c,
                                    etrial_phone__c,
                                    Country__c,
                                    Booking_date__c,
                                    Booking_time__c,
                                    etrial_visioncare__c,
                                    ecp_name__c,
                                    ecp_address__c,
                                    ecp_phone__c,
                                    ecp_province__c,
                                    ecp_state__c,
                                    CreatedDate,
                                    Account__r.OutletNumber__c,
                                    Account__r.AccountNumber,
                                    RedemptionCode__c,
                                    etrial_product__c,
                                    Product_Name__c,
                                    etrial_product__r.Name FROM Etrial__c WHERE Id IN: emId];
        
        ET_AdministrationService.createTriggeredEmailSend(et);
    }
    
   @future(callout=true)
    global static void callSMSAdmin(List<Id> smsId)
    {
        System.debug('SMS ###:'+smsId);
        
        List<Etrial__c> et = [SELECT Id,
                                    etrial_email__c,
                                    etrial_first_name__c,
                                    etrial_last_name__c,
                                    etrial_gender__c,
                                    etrial_phone__c,
                                    Country__c,
                                    Booking_date__c,
                                    Booking_time__c,
                                    etrial_visioncare__c,
                                    ecp_name__c,
                                    ecp_address__c,
                                    ecp_phone__c,
                                    ecp_province__c,
                                    ecp_state__c,
                                    CreatedDate,
                                    Account__r.OutletNumber__c,
                                    Account__r.AccountNumber,
                                    RedemptionCode__c,
                                    etrial_product__c,
                                    Product_Name__c,
                                    etrial_product__r.Name FROM Etrial__c WHERE Id IN:smsId];
        
        ET_AdministrationService.createTriggeredSMSSend(et);
    } 
}