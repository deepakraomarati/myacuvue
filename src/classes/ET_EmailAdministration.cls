/**
* File Name: ET_EmailAdministration
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  18-Oct-2018
* Description : Create or Update canpaign member record to send email to contact associated to campaign member using Marketing Cloud connector App
* Copyright (c) $2018 Johnson & Johnson  
*/
public with sharing class ET_EmailAdministration
{
	/**
    * @description  Create or update campaign member record from the input data
    */
	public static void sendeMail(String Email, String TriggersendId, String Firstname, String Practicename, String Ownername)
	{
		List<Contact>conc = [SELECT Id,firstname,lastname,Email FROM Contact WHERE Email = :Email];
		List<Campaign>camp = [SELECT Id,Name FROM Campaign WHERE Name = :TriggersendId limit 1];
		List<CampaignMember>member = new List<CampaignMember>();
		if (!camp.isempty() && !conc.isempty())
		{
			List<CampaignMember>cmb =
			[
					SELECT Id,CampaignID,ContactId
					FROM CampaignMember
					WHERE Contactid = :conc[0].Id and CampaignID = :camp[0].Id
			];
			if (cmb.isempty())
			{
				CampaignMember cmb2 = new CampaignMember();
				cmb2.ContactId = conc[0].id;
				cmb2.CampaignID = camp[0].id;
				cmb2.Status = 'Send';
				cmb2.Practice_Name__c = Practicename;
				cmb2.Owner_Name__c = Firstname;
				member.add(cmb2);
			}
			else
			{
				CampaignMember cmb1 = cmb[0];
				cmb1.Status = 'Send';
				cmb1.Practice_Name__c = Practicename;
				cmb1.Owner_Name__c = Firstname;
				member.add(cmb1);
			}
		}
		if (!member.isempty())
		{
			upsert member;
		}
	}
}