/**
* File Name: ET_EmailAdministrationTest
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  25-Oct-2018  
* Description : Unit Test class for ET_EmailAdministration
* Copyright (c) $2018 Johnson & Johnson
*/
@isTest
public class ET_EmailAdministrationTest
{
	static testMethod void Test_SendEmailMethod()
	{
		CustomException__c cep = new CustomException__c();
		cep.Name = 'ER123';
		cep.Error_Description__c = 'Error';
		insert cep ;

		Account genericAccount = new Account();
		genericAccount.Name = 'Generic consumer Account';
		genericAccount.shippingcountry = 'JAPAN';
		genericAccount.shippingstreet = 'Test Street';
		genericAccount.shippingcity = 'Test city';
		genericAccount.shippingstate = 'AICHI KEN';
		genericAccount.shippingpostalcode = 'TEST20017';
		insert genericAccount;

		Portal_Settings__c portalCustomSettings = new Portal_Settings__c();
		portalCustomSettings.Name = 'GenericAccount Id';
		portalCustomSettings.Value__c = genericAccount.Id;
		insert portalCustomSettings;

		Contact cc = new Contact();
		cc.FirstName = 'Acuvue';
		cc.LastName = 'Pro user';
		cc.Salutation = 'Mr.';
		cc.Email = 'protestuser+acuvue@gmail.com';
		cc.School_Name__c = 'Test';
		cc.Graduation_Year__c = '2014';
		cc.Degree__c = 'Test';
		cc.BirthDate = date.today();
		insert cc;

		Campaign cmp = new Campaign();
		cmp.Name = 'Test_ETEMail';
		cmp.DB_ID__c = 23;
		insert cmp;

		Test.starttest();
		ET_EmailAdministration.sendemail('protestuser+acuvue@gmail.com', 'Test_ETEMail', 'Fname', 'Pname', 'Owner');
		ET_EmailAdministration.sendemail('protestuser+acuvue@gmail.com', 'Test_ETEMail', 'Fname1', 'Pname1', 'Owner1');
		list<CampaignMember>cmbInsert = [SELECT Id FROM CampaignMember WHERE ContactId =: cc.Id];
		system.assertNotequals(cmbInsert,null);
		Test.stoptest();
		CustomException.getException('ER123', 'Error');

		Contact cc1 = new Contact();
		cc1.FirstName = 'Acuvue';
		cc1.LastName = 'Pro user';
		cc1.Salutation = 'Mr.';
		cc1.Email = 'protestuser+acuvue1@gmail.com';
		cc1.School_Name__c = 'Test';
		cc1.Graduation_Year__c = '2014';
		cc1.Degree__c = 'Test';
		cc1.BirthDate = date.today();
		cc1.MA2_Country_Code__c = 'SGP';
		insert cc1;

		CampaignMember cmb2 = new CampaignMember();
		cmb2.ContactId = cc1.id;
		cmb2.CampaignID = cmp.id;
		cmb2.Status = 'Send';
		cmb2.AWS_ContactId__c = '23';
		cmb2.CampaignKey__c = 24;
		insert cmb2;
	}
}