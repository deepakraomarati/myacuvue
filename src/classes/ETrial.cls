/**
 * File Name: ETrial 
 * Description : Handle eTrial Registrations
 * Copyright : Johnson & Johnson
 * @author : Shobana K G | skg6@its.jnj.com | Shobana.KG@cognizant.com
 * 
 * Modification Log 
 * =============================================================== 
 *  Ver  |Date         |Author                |Modification
 *  1.0  |28-May-2018  |skg6@its.jnj.com      |New Class created
 *  1.1  |04-Jul-2018  |skg6@its.jnj.com      |Remove country and countryCode from request
 *  1.2  |13-Jul-2018  |skg6@its.jnj.com      |Include validation to identify existing consumer and altered error/success response format
 *  1.3  |20-Jul-2018  |skg6@its.jnj.com      |Use name instead of firstName and lastName for India and included examFee json fields
 *  1.4  |31-Jul-2018  |skg6@its.jnj.com      |Use firstName and lastName instead of name, included dob and visionExperience
 *  1.5  |27-Dec-2018  |skg6@its.jnj.com      |Introduced WS Field 'birthdate' for HK and assign to DOB__c of Contact  
 */

public with sharing class ETrial {
    
    public static final String SUCCESS_STATUS = 'Success';
	public static final String SUCCESS_STATUS_CODE = '200';    
    public static final String GENERIC_ERROR = 'GEN0001';
    public static final String GENERIC_ERROR_TYPE = 'Generic';
    
    /**
	 * Description - Create Contact and CampaignMember for eTrial Registrations
	 * Input - JSON Request with params - firstName, lastName, email, phone, dob, promotionId, 
	 * 			accountId, question, visionExperience, examFee tnc and eNewsLetter
	 * Output - JSON response with message containing contact and campaign member created
	 */
    public static void createContactAndCampaignMember(RestRequest request, RestResponse response) {        
        RestRequest req = request;
        RestResponse res = response;
        Savepoint sp;
        
        // Get input parameters from request body
        String body = req.requestBody.toString();
        ETrial et = (ETrial)JSON.deserialize(body, ETrial.class);
        system.debug('firstName: ' + et.firstName + ' lastName: ' + et.lastName + ' phone: ' + et.phone + ' email: ' + et.email + 
                     ' dob: ' + et.dob + ' promotionId: ' + et.promotionId + ' accountId: ' + et.accountId + 
                     ' birthDate: ' + et.birthDate + ' question: ' + et.question + ' visionExperience: ' + et.visionExperience + ' examFee: ' + et.examFee + 
                     ' tnc: ' + et.tnc + ' eNewsLetter: ' + et.eNewsLetter);
        
        // Validate Required/Lookup Fields, existing consumer and create records if validation passes
        Map<String, String> requiredFields = new Map<String, String>{'lastName' => et.lastName, 'promotionId' => et.promotionId};        
        Map<String, String> lookupFields = new Map<String, String>{'promotionId' => et.promotionId, 'accountId' => et.accountId};
        Map<String,Schema.SObjectType> fieldSObjectType = new Map<String, Schema.SObjectType>{'promotionId' => Schema.Campaign.SObjectType, 
                            															      'accountId' => Schema.Account.SObjectType};                
        // Create error response if validation fails
        Boolean isError = ETrialsandEVouchers.validateFields(res, requiredFields, lookupFields, fieldSObjectType, null, et);        
        
        // Create Contact and Campaign Member records if validation passes
        if (!isError) {
            Id conId, cmId;
            sp = Database.setSavepoint();
            Id conRecTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND Name = 'eTrial'].Id;
            Campaign objCamp = [SELECT Id, Country__c FROM Campaign WHERE Id =: et.promotionId];
            Map<String,Canvas_CountryCodes__c> countryCodeMap = Canvas_CountryCodes__c.getAll(); 
            
            try {
                Contact con = new Contact(RecordTypeId = conRecTypeId, FirstName = et.firstName, LastName = et.lastName,  
                                          MobilePhone = et.phone, Email = et.email, Canvas_DOB_Range__c = et.dob,
                                          DOB__c = (et.birthDate == null) ? null : DateTime.newInstanceGmt(Date.parse(et.birthDate), Time.newInstance(00,00,00,00)), 
                                          MailingCountry = objCamp.Country__c);
                Database.SaveResult conSR = Database.insert(con, true);
                conId = (conSR.isSuccess()) ? conSR.getId() : null;               
                
                CampaignMember cm = new CampaignMember(CampaignId = et.promotionId, ContactId = con.Id, Account__c = et.accountId, 
                                                       eTrial_ACU_question__c = et.question, 
                                                       Canvas_Vision_Experience__c = et.visionExperience, 
                                                       Canvas_ExaminationFee__c = ((et.examFee == null) ? false : et.examFee), 
                                                       eTrial_TnC__c = ((et.tnc == null) ? false : et.tnc),
                                                       eTrial_ACU_eNewsletter__c = ((et.eNewsLetter == null) ? false : et.eNewsLetter), 
                                                       eTrial_Country__c = countryCodeMap.get(objCamp.Country__c).CountryCode__c);
                Database.SaveResult cmSR = Database.insert(cm, true);
                cmId = (cmSR.isSuccess()) ? cmSR.getId() : null;
                
                ResponseWrapper.createSuccessResponse(res, SUCCESS_STATUS_CODE, SUCCESS_STATUS, conId, cmId);
            } catch (DMLException de) {
                Database.rollback(sp);
                system.debug('DMLException in ETrial: ' + de);
                ResponseWrapper.createErrorResponse(res, null, GENERIC_ERROR, GENERIC_ERROR_TYPE);
            }     
        }    
    }
    
	// DTO properties to store canvas inputs
    public String firstName{get;set;}
    public String lastName{get;set;}
    public String phone{get;set;}
    public String email{get;set;}
    public String dob{get;set;}
    public String birthDate{get;set;}
    public String promotionId{get;set;}
    public String accountId{get;set;}
    public String question{get;set;}
    public String visionExperience{get;set;} 
    public Boolean examFee{get;set;}
    public Boolean tnc{get;set;}
    public Boolean eNewsLetter{get;set;}

}