/**
     * File Name: ETrialsandEVouchers 
 * Description : RESTful Web Service to create contact and campaign member with 
 *				input from Canvas for eTrial as well as eVoucher Registrations
 * Copyright : Johnson & Johnson
 * @author : Shobana K G | skg6@its.jnj.com | Shobana.KG@cognizant.com
 * 
 * Modification Log 
 * =============================================================== 
 *  Ver  |Date         |Author                |Modification
 *  1.0  |28-May-2018  |skg6@its.jnj.com      |New Class created
 *  1.1  |03-Jul-2018  |skg6@its.jnj.com      |Remove countryCode from request as code related to Campaign country will be used
 *  1.2  |13-Jul-2018  |skg6@its.jnj.com      |Include validation to identify existing consumer and altered error/success response format
 *  1.3  |31-Jul-2018  |skg6@its.jnj.com      |Use value of isEVoucher as 'yes' & 'no' to identify the eVoucher or eTrial request  
 */

@RestResource(urlMapping='/apex/Campaigns/*')
global with sharing class ETrialsandEVouchers {
    
    public static final String REQUIRED = 'B2C0012';
    public static final String INVALID_ID = 'B2C0031';
    public static final String CAMPMEMB_EXIST = 'B2C0024';
    public static final String GENERIC_ERROR = 'GEN0001';
    public static final String ERROR_TYPE = 'subscribeToPromotion';
    public static final String GENERIC_ERROR_TYPE = 'Generic';
    
    /**
	 * Description - Handle POST HttpRequest
	 * Input - JSON Request with params - firstName, lastName, email, phone, dob, promotionId, 
	 * 			accountId, question, visionExperience, examFee tnc and eNewsLetter
	 * Output - JSON response with message containing contact and campaign member created
	 */
    @HttpPost 
    global static void handlePost() {               
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        // check the type of request-eTrial or eVoucher and route to respective class/logic
        try {
            String body = req.requestBody.toString();
            ETrialsandEVouchers etv = (ETrialsandEVouchers)JSON.deserialize(body, ETrialsandEVouchers.class);
            if (etv.isEVoucher == 'yes') {
                EVoucher.createContactAndCampaignMember(req, res);	    
            } else if (etv.isEVoucher == 'no') {
                ETrial.createContactAndCampaignMember(req, res);    
            } else {
                ResponseWrapper.createErrorResponse(res, 'isEVoucher', REQUIRED, ERROR_TYPE);
            }     
       } catch (Exception e) {
        	system.debug('Exception in ETrialsandEVouchers : ' + e);
            ResponseWrapper.createErrorResponse(res, null, GENERIC_ERROR, GENERIC_ERROR_TYPE);    
       }
    }
    
    /**
	 * Description - Validate Lookup and Required fields and consumer - existing or new
	 * Input -  requiredFields - Map of Required Fields and its values
	 * 			lookupFields - Map of Lookup Fields and its values
	 * 		  	fieldSObjectType - Map of Lookup fields and its SObjectTypes
	 *          ev - EVoucher object
	 * 			et - ETrial object    
	 * Output - isError - Boolean to represent error or not
	 */
    public static Boolean validateFields(RestResponse res, Map<String, String> requiredFields, Map<String, String> lookupFields, 
                                      	 Map<String,Schema.SObjectType> fieldSObjectType, EVoucher ev, ETrial et) {    	
        String reqFields = validateReqFields(requiredFields);
        String lkpFields = validateLkpFields(lookupFields, fieldSObjectType);
        Boolean isError = false;
                                  
        if (reqFields != null) {
            ResponseWrapper.createErrorResponse(res, reqFields, 'B2C0012', ERROR_TYPE);
            isError = true;
        } else if (lkpFields != null) {
            ResponseWrapper.createErrorResponse(res, lkpFields, 'B2C0031', ERROR_TYPE);
            isError = true;
        } else if (!validateConsumer(ev,et).isEmpty()) {
        	ResponseWrapper.createErrorResponse(res, null, CAMPMEMB_EXIST, ERROR_TYPE);
            isError = true;
        }
        
        return isError;                                         
    }
    
    /**
	 * Description - Validate Required fields
	 * Input -  requiredFields - Map of Required Fields and its values 
	 * Output - reqFields - List of required fields fail for validation
	 */
    public static String validateReqFields(Map<String, String> requiredFields) {
        String reqFields;        
        for (String field : requiredFields.keySet()) {
            if (requiredFields.get(field) == '' || requiredFields.get(field) == null) {
            	reqFields = (reqFields == null) ? field : (reqFields + ',' + field);
            }
    	}        
        return reqFields;
    }
    
    /**
	 * Description - Validate Lookup fields
	 * Input - lookupFields - Map of Lookup Fields and its values
	 * 		   fieldSObjectType - Map of Lookup fields and its SObjectTypes 
	 * Output - lkpFields - List of lookup fields fail for validation
	 */
    public static String validateLkpFields(Map<String, String> lookupFields, Map<String,Schema.SObjectType> fieldSObjectType) {
        String lkpFields;        
        for (String field : lookupFields.keySet()) {
            if (lookupFields.get(field) != null && 
                ((!Pattern.compile('[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}').matcher(lookupFields.get(field)).matches()) || 
                 (Id.valueOf(lookupFields.get(field)).getSobjectType() != fieldSObjectType.get(field)))) {
					lkpFields = (lkpFields == null) ? field : (lkpFields + ',' + field);
            }                               
    	}        
        return lkpFields;
    }
    
    /**
	 * Description - Validate consumer - existing or new
	 * Input -  ev - EVoucher object
	 * 			et - ETrial object  
	 * Output - campMembList - List of Campaign Member records
	 */
    public static List<CampaignMember> validateConsumer(EVoucher ev, ETrial et) {
        List<CampaignMember> campMembList;
        system.debug('eVoucher: ' + ev);
        if (ev != null) {
            campMembList = [SELECT Id 
                            FROM CampaignMember
                            WHERE CampaignId =: ev.promotionId 
                            AND (Contact.Email =: ev.email OR Contact.MobilePhone =: ev.phone)];
        } else {
        	campMembList = [SELECT Id 
                            FROM CampaignMember
                            WHERE CampaignId =: et.promotionId 
                            AND (Contact.Email =: et.email OR Contact.MobilePhone =: et.phone)];    
        }
        return campMembList;                   
    }
    
    // DTO properties to store canvas inputs
    // isEVoucher for redirecting to ETrial or EVoucher class
    // other needed fields of ETrial and EVoucher to create related jsonRequest in test class
    public String isEVoucher{get;set;}
    public String firstName{get;set;}
    public String lastName{get;set;}
    public String phone{get;set;}
    public String email{get;set;}
    public String dob{get;set;}
    public String promotionId{get;set;}
    public String accountId{get;set;}

}