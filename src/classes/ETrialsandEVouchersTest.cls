/**
 * File Name: ETrialsandEVouchersTest 
 * Description : Test Class for ETrialsandEVouchers API
 * Copyright : Johnson & Johnson
 * @author : Shobana K G | skg6@its.jnj.com | Shobana.KG@cognizant.com
 * 
 * Modification Log 
 * =============================================================== 
 *  Ver  |Date         |Author                |Modification
 *  1.0  |28-May-2018  |skg6@its.jnj.com      |New Class created
 *  1.1  |24-Jul-2018  |skg6@its.jnj.com      |Cover unique consumer validation
 *  1.2  |12-Dec-2018  |skg6@its.jnj.com      |Included accountId in EVoucher request to pass validation 
 */

@isTest
private class ETrialsandEVouchersTest {
    private static final Integer ACC_PROD_MAX = 3;
    private static List<Account> insertedAccts = new List<Account>();
    private static Campaign inETCamp, inEVCamp, ausEVCamp;
    private static String nullETVRequest, emptyETVRequest, emptyETRequest, emptyEVRequest;
    private static String compETRequest, compEVRequest;
    
    private static void setupTestData() {
        // Create Accounts
        List<Account> accList = new List<Account>();
        for (Integer i=1; i<=ACC_PROD_MAX; i++) {
            accList.add(new Account(Name = 'Account'+i));
        }
		insert accList;
		insertedAccts = [SELECT Id FROM Account LIMIT :ACC_PROD_MAX];

        // Create Campaign
        List<Campaign> campList = new List<Campaign>(); 
        campList.add(new Campaign(Name = 'IND EVoucher Campaign', Country__c = 'INDIA'));
        campList.add(new Campaign(Name = 'AUS EVoucher Campaign', Country__c = 'AUSTRALIA'));
        campList.add(new Campaign(Name = 'IND ETrial Campaign', Country__c = 'INDIA'));
        insert campList;
        inEVCamp = campList[0];
        ausEVCamp = campList[1];
        inETCamp = campList[2];
        
        // Create custom setting - Canvas_CountryCodes__c & CustomException__c
        List<Canvas_CountryCodes__c> countryCodes = new List<Canvas_CountryCodes__c>();
        Map<String,String> countries = new Map<String,String>{'INDIA' => 'IN', 'AUSTRALIA' => 'AU'};
        for (String ctry : countries.keySet()) {
        	countryCodes.add(new Canvas_CountryCodes__c(Name = ctry, CountryCode__c = countries.get(ctry)));	        
        }
        insert countryCodes;
		
		List<CustomException__c> custExcp = new List<CustomException__c>();
        Map<String,String> custExcpMap = new Map<String,String>{'B2C0012' => 'Required field missing',
            													'B2C0021' => 'Sorry, Accounts not available', 
            													'B2C0022' => 'Required Parameters are missing',
            													'B2C0024' => 'User has already registered for this Promotion',
            													'B2C0031' => 'Id is invalid or of incorrect type',
            													'GEN0001' => 'An unexpected error has occurred. Please contact your System Administrator.'};
        for (String ce : custExcpMap.keySet()) {
        	custExcp.add(new CustomException__c(Name = ce, Error_Description__c = custExcpMap.get(ce)));	        
        }
        insert custExcp;         
        
        // Create JSON request
        // Null request to cover catch block of ETrialsandEVouchers
        nullETVRequest = JSON.serialize(null);
        
        // Partial request for ETrialsandEVouchers to cover validations
        ETrialsandEVouchers etvEmpty = new ETrialsandEVouchers();
        etvEmpty.isEVoucher = null;
        emptyETVRequest = JSON.serialize(etvEmpty); 
        
        // Partial/incorrect request for ETrial to cover Reqd validation
        ETrialsandEVouchers etEmpty = new ETrialsandEVouchers();
        etEmpty.isEVoucher = 'no';
        etEmpty.promotionId = String.valueOf(inETCamp.Id);
        emptyETRequest = JSON.serialize(etEmpty);
        
        // Partial/incorrect request for EVoucher to cover lookup validation
        ETrialsandEVouchers evEmpty = new ETrialsandEVouchers();
        evEmpty.isEVoucher = 'yes';
        evEmpty.lastName = 'User';
        evEmpty.promotionId = String.valueOf(inEVCamp.Id);
        evEmpty.accountId = 'xxxx';
        emptyEVRequest = JSON.serialize(evEmpty); 
        
        // Complete request for ETrial
        ETrialsandEVouchers et = new ETrialsandEVouchers(); 
        et.isEVoucher = 'no';
        et.firstName = 'Test'; 
        et.lastName = 'User';
        et.phone = '1234567890';
        et.email = 'tuser@abc.com';
        et.dob = '18-25';
        et.promotionId = String.valueOf(inETCamp.Id);
        et.accountId = String.valueOf(insertedAccts[0].Id);       
        compETRequest = JSON.serialize(et); 
        
        // Complete request for EVoucher
        ETrialsandEVouchers ev = new ETrialsandEVouchers();       
        ev.isEVoucher = 'yes';       
        ev.firstName = 'Test'; 
        ev.lastName = 'User';
        ev.email = 'tuser@abc.com';
        ev.phone = '1234567890';
        ev.dob = '18-25';
        ev.promotionId = String.valueOf(inEVCamp.Id);
        ev.accountId = String.valueOf(insertedAccts[0].Id);
        compEVRequest = JSON.serialize(ev);          
        
    }
    
	@isTest
    private static void handlePost_expectErrorMsg() {
        // Setup Test Data
        setupTestData();
        
        // Execute Test
        Test.startTest();
        
        // Cover req fields error msg and catch block of ETrialsandEVouchers
        RestRequest etvReq = new RestRequest(); 
        RestResponse etvRes = new RestResponse();
        etvReq.requestURI = '/services/apexrest/apex/ETrialsandEVouchers'; 
        etvReq.httpMethod = 'POST';                
        
        etvReq.requestBody = Blob.valueof(nullETVRequest); 
        etvReq.addHeader('Content-Type', 'application/json'); 
        RestContext.request = etvReq;
        RestContext.response = etvRes;
        ETrialsandEVouchers.handlePost();
        
        etvReq.requestBody = Blob.valueof(emptyETVRequest); 
        etvReq.addHeader('Content-Type', 'application/json'); 
        RestContext.request = etvReq;
        RestContext.response = etvRes;
        ETrialsandEVouchers.handlePost();
        
        // Cover reqd field validations - consider eTrial here
        RestRequest etReq = new RestRequest(); 
        RestResponse etRes = new RestResponse();
        etReq.requestURI = '/services/apexrest/apex/ETrialsandEVouchers'; 
        etReq.httpMethod = 'POST';
        etReq.requestBody = Blob.valueof(emptyETRequest); 
        etReq.addHeader('Content-Type', 'application/json'); 
        RestContext.request = etReq;
        RestContext.response = etRes;
        ETrialsandEVouchers.handlePost();
        
        // Cover lookup field validations - consider eVoucher here
        RestRequest evReq = new RestRequest(); 
        RestResponse evRes = new RestResponse();
        evReq.requestURI = '/services/apexrest/apex/ETrialsandEVouchers'; 
        evReq.httpMethod = 'POST';
        evReq.requestBody = Blob.valueof(emptyEVRequest); 
        evReq.addHeader('Content-Type', 'application/json'); 
        RestContext.request = evReq;
        RestContext.response = evRes;
        ETrialsandEVouchers.handlePost();
        
        Test.stopTest();
        
        // Assert
        system.debug('response body in handlePost_expectErrorMsg: ' + etvRes.responseBody.toString());
        system.assert(etvRes.responseBody.toString().contains('Required field missing'), 'All required fields have values');
    }
    
    @isTest
    private static void handlePost_expectETrialResponse() {
        // Setup Test Data
        setupTestData();
        
        // Execute Test
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/apex/ETrialsandEVouchers'; 
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(compETRequest);
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        ETrialsandEVouchers.handlePost(); 
   		Test.stopTest();
        
        // Assert
        List<CampaignMember> cmList = [SELECT Id, ContactId FROM CampaignMember WHERE CampaignId =: inETCamp.Id 
                                       ORDER BY CreatedDate DESC LIMIT 1];
        system.debug('response body in handlePost_expectETrialResponse: ' + res.responseBody.toString());
        system.assert(res.responseBody.toString().contains(cmList[0].ContactId), 'Contact and Campaign Member are not created');
    }
    
   @isTest
    private static void handlePost_expectEVoucherResponse() {
        // Setup Test Data
        setupTestData();
        
        // Execute Test
        Test.startTest();
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/apex/ETrialsandEVouchers'; 
        req.httpMethod = 'POST';                
        req.requestBody = Blob.valueof(compEVRequest);
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        ETrialsandEVouchers.handlePost();
        
        // Cover unique consumer validation with duplicate request
        RestRequest dupReq = new RestRequest(); 
        RestResponse dupRes = new RestResponse();
        dupReq.requestURI = '/services/apexrest/apex/ETrialsandEVouchers'; 
        dupReq.httpMethod = 'POST';                
        dupReq.requestBody = Blob.valueof(compEVRequest);
        dupReq.addHeader('Content-Type', 'application/json'); 
        RestContext.request = dupReq;
        RestContext.response = dupRes;
        ETrialsandEVouchers.handlePost();
                
   		Test.stopTest();
        
        // Assert
        List<CampaignMember> cmList = [SELECT Id, ContactId FROM CampaignMember WHERE CampaignId =: inEVCamp.Id 
                                       ORDER BY CreatedDate DESC LIMIT 1];
        system.debug('response body in handlePost_expectEVoucherResponse: ' + res.responseBody.toString());
        system.assert(res.responseBody.toString().contains(cmList[0].ContactId), 'Contact and Campaign Member are not created');
    }
      
}