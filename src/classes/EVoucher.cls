/**
 * File Name: EVoucher 
 * Description : Handle eVoucher Registrations
 * Copyright : Johnson & Johnson
 * @author : Shobana K G | skg6@its.jnj.com | Shobana.KG@cognizant.com
 * 
 * Modification Log 
 * =============================================================== 
 *  Ver  |Date         |Author                |Modification
 *  1.0  |28-May-2018  |skg6@its.jnj.com      |New Class created
 *  1.1  |04-Jul-2018  |skg6@its.jnj.com      |Remove countryCode from request
 *  1.2  |13-Jul-2018  |skg6@its.jnj.com      |Include validation to identify existing consumer and altered error/success response format
 *  1.3  |20-Jul-2018  |skg6@its.jnj.com      |Use single json field 'phone' for all countries and map to Contact.MobilePhone
 *  1.4  |31-Jul-2018  |skg6@its.jnj.com      |Included question, examFee, tnc and eNewsLetter  
 */

public with sharing class EVoucher {
    
    public static final String SUCCESS_STATUS = 'Success';
	public static final String SUCCESS_STATUS_CODE = '200';    
    public static final String GENERIC_ERROR = 'GEN0001';
    public static final String GENERIC_ERROR_TYPE = 'Generic';
    
    /**
     * Description - Create Contact and CampaignMember for eVoucher Registrations
     * Input - JSON request with params - firstName, lastName, email, phone, dob, visionExperience, 
     * 			promotionId, accountId, question, examFee, tnc and eNewsLetter    
     * Output - JSON response with message containing contact and campaign member created
     */
    public static void createContactAndCampaignMember(RestRequest request, RestResponse response) {
        RestRequest req = request;
    	RestResponse res = response;
        Savepoint sp;
        
        // Get input parameters from request body
        String body = req.requestBody.toString();
        EVoucher ev = (EVoucher)JSON.deserialize(body, EVoucher.class);
        system.debug('firstName: ' + ev.firstName + ' lastName: ' + ev.lastName + ' email: ' + ev.email + ' phone: ' + ev.phone +  
                     ' dob: ' + ev.dob + ' visionExperience: ' + ev.visionExperience + ' promotionId: ' + ev.promotionId + 
                     ' accountId: ' + ev.accountId + ' question: ' + ev.question + ' examFee: ' + ev.examFee + ' tnc: ' + ev.tnc + 
                     ' eNewsLetter: ' + ev.eNewsLetter);
        
        // Validate Required/Lookup Fields, existing consumer and create records if validation passes
        Map<String, String> requiredFields = new Map<String, String>{'lastName' => ev.lastName, 'promotionId' => ev.promotionId};
        Map<String, String> lookupFields = new Map<String, String>{'promotionId' => ev.promotionId, 'accountId' => ev.accountId};
        Map<String,Schema.SObjectType> fieldSObjectType = new Map<String, Schema.SObjectType>{'promotionId' => Schema.Campaign.SObjectType, 
                																			  'accountId' => Schema.Account.SObjectType};                                                                                                                      
        // Create error response if validation fails
        Boolean isError = ETrialsandEVouchers.validateFields(res, requiredFields, lookupFields, fieldSObjectType, ev, null);        
        
        // Create Contact and Campaign Member records if validation passes
        if (!isError) {  
            Id conId,cmId;
            sp = Database.setSavepoint();
            Id conRecTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND Name = 'Consumer'].Id;
            Campaign objCamp = [SELECT Id, Country__c FROM Campaign WHERE Id =: ev.promotionId];
            
            try {
                Contact con = new Contact(RecordTypeId = conRecTypeId, FirstName = ev.firstName, LastName = ev.lastName,  
                                          Email = ev.email, MobilePhone = ev.phone, Canvas_DOB_Range__c = ev.dob);                
                Database.SaveResult conSR = Database.insert(con, true);
                conId = (conSR.isSuccess()) ? conSR.getId() : null; 
                
                CampaignMember cm = new CampaignMember(CampaignId = ev.promotionId, ContactId = con.Id, Account__c = ev.accountId, 
                                                       Canvas_Vision_Experience__c = ev.visionExperience,
                                                       eTrial_ACU_question__c = ev.question,
                                                       Canvas_ExaminationFee__c = ((ev.examFee == null) ? false : ev.examFee), 
                                                       eTrial_TnC__c = ((ev.tnc == null) ? false : ev.tnc),
                                                       eTrial_ACU_eNewsletter__c = ((ev.eNewsLetter == null) ? false : ev.eNewsLetter));
                Database.SaveResult cmSR = Database.insert(cm, true);
                cmId = (cmSR.isSuccess()) ? cmSR.getId() : null;         	
                
                ResponseWrapper.createSuccessResponse(res, SUCCESS_STATUS_CODE, SUCCESS_STATUS, conId, cmId);
            } catch (DMLException de) {
                Database.rollback(sp);
                system.debug('DMLException in EVoucher: ' + de.getMessage());
                ResponseWrapper.createErrorResponse(res, null, GENERIC_ERROR, GENERIC_ERROR_TYPE);
            }      
        }                
    }
    
    // DTO properties to store canvas inputs
    public String firstName{get;set;}
    public String lastName{get;set;}
    public String email{get;set;}
    public String phone{get;set;}
    public String dob{get;set;}
    public String visionExperience{get;set;} 
    public String promotionId{get;set;}
    public String accountId{get;set;}
    public String question{get;set;}
    public Boolean examFee{get;set;}
    public Boolean tnc{get;set;}
    public Boolean eNewsLetter{get;set;}
    
}