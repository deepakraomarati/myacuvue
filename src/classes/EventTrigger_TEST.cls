@IsTest (SeeAllData=true)

public class EventTrigger_TEST {
    
        static testMethod void EventTrigger_TEST(){
       
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Havish', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', UserName='havish@abc.com',Unique_User_Id__c = 'ABC');
        insert u;
        Profile p1 = [SELECT Id FROM Profile WHERE Name='ASPAC ANZ CDM/KAM'];
        User u1 = new User(Alias = 'test1', Email='standardtest1@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Prasad', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p1.Id,
        TimeZoneSidKey='America/Los_Angeles', UserName='test1@test.com.staging',Unique_User_Id__c = 'test1');
        insert u1;
        
        system.runas(u){
            
            Account acc = New Account(Name ='Test Account',PublicAddress__c='Bangalore', SalesRep__c = 'ABC', AccountNumber = '12345', OutletNumber__c = '12345', CountryCode__c = 'AUS',Subscribe__c=true);
            insert acc;
            //c.Subscribe__c=true;
            //date acc;
            
            event e= new event();
            e.StartDateTime = date.today();
            e.EndDateTime = date.today().addDays(2);
            e.Subject = 'Test';
            e.WhatId = acc.id ;
            insert e;
            
            //updating event
            
            
            e.CallOutcomeCallTargetComments__c='test';
            e.CallObjectiveAchieved__c='Fully Achieved';
            e.ActivityStatus__c='Completed';
            e.ManagersComments__c = 'test on the Manager Comments fields';
            update e;
             
        }
            
        system.runas(u1){
            
            Account acc = New Account(Name ='Test Account1',PublicAddress__c='Bangalore', SalesRep__c = 'test1', AccountNumber = '828135A', OutletNumber__c = '828135A', CountryCode__c = 'AUS',Subscribe__c=true);
            insert acc;
            //c.Subscribe__c=true;
            //date acc;
            
            event e= new event();
            e.StartDateTime = date.today();
            e.EndDateTime = date.today().addDays(2);
            e.Subject = 'Test';
            e.WhatId = acc.id ;
            insert e;
            
            //updating event
            
            
            e.CallOutcomeCallTargetComments__c='test';
            e.CallObjectiveAchieved__c='Fully Achieved';
            e.ActivityStatus__c='Completed';
           // e.ManagersComments__c = 'test on the Manager Comments fields';
            update e;
            
            //deleting event
            
            //Event deletedEvent = [SELECT id FROM Event WHERE id = :e.id ALL ROWS];
            //System.assertEquals(deletedEvent.ActivityStatus__c, 'Completed');
            
            boolean checkError;

            Test.startTest();

            try {

                delete e;

                checkError = false;
            }
            catch(Exception ex) {

                checkError = true;
            }

            Test.stopTest();

            System.assert(true, 'Deletion Failed Appropriately' );
                     
        }
            
    

    }
}