public without sharing class  GenerateEventICS {
    
    Event EventObj {get;set;}
    
    public DateTime startDate{get;set;}
    public  DateTime endDate{get;set;}
    public String StartT{get;set;}
    public String EndT{get;set;}
    public final String summary;
    public final String location;
    public String eventId{get;set;}
    
    public GenerateEventICS(ApexPages.StandardController controller){
        system.debug('ApexPages.currentPage().getParameters() :: '+ApexPages.currentPage().getParameters());
        String currentid = ApexPages.currentPage().getParameters().get('id');
        system.debug('currentid'+currentid);
        EventObj=[select StartDateTime , EndDateTime from Event where id=:currentid];
        startDate =EventObj.StartDateTime;
        endDate =EventObj.EndDateTime;
        StartT = startDate.formatGMT('yyyyMMdd\'T\'HHmmss\'Z\'');
        EndT = endDate.formatGMT('yyyyMMdd\'T\'HHmmss\'Z\'');
        summary = ApexPages.currentPage().getParameters().get('summary');
        location = ApexPages.currentPage().getParameters().get('location');
        ApexPages.currentPage().getHeaders().put('content-disposition','inline; filename='+StartT+'.ics');
    }
    
}