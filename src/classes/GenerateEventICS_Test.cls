@isTest(SEEALLDATA =false)
public class GenerateEventICS_Test {
    
    static testmethod void doinsertoptidlst ()
    {   
        Account acc = new Account(Name = 'TestAccountName');
        //Insert the object virtually
        insert acc;
        Event tEent = new Event(StartDateTime = Date.newInstance(Date.today().year(),1,1),DurationInMinutes=10);
        
        //Insert the object virtually
        insert tEent;
        apexpages.currentpage().getparameters().put('id' , tEent.id);
        //Create a new instance of standard controller
        ApexPages.StandardController sc = new ApexPages.standardController(acc);
        test.startTest();
        GenerateEventICS gn=new GenerateEventICS(sc);
        test.stopTest();
        System.assertEquals(gn, gn);
    }}