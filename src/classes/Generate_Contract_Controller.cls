global class Generate_Contract_Controller
{
    public string idVal{get;set;}
    public string doSave{get;set;}
    public id contractid {get; set;}
    Attachment attach = new Attachment();
    public RecordType objRecType;
    public Contract GenContract{get;set;}
    public String atchId;
    public String sign;
    public string message{get; set;}
    
    public Contract getContract()
    {
        return GenContract;
    }
    public PageReference save()
    {
        update GenContract;
        return null;
    }
       
    public Generate_Contract_Controller(ApexPages.StandardController stdController)
    {
        idVal = stdController.getId();
        doSave = ApexPages.currentPage().getParameters().get('doSave');
        contractid = ApexPages.currentPage().getParameters().get('contractid');
        GenContract = [select id, Local_Alias__c, recordtypeid, Send_Email__c, Business_Registration_Number__c, BC_Card_Merchant_Number__c from contract where id = :contractid limit 1];
        objRecType = [select id,name from Recordtype where id = :GenContract.recordtypeid limit 1];
        atchId = 'https://jjskywalker.my.salesforce.com/servlet/servlet.FileDownload?file=';
        sign = getFileId();
    }
    
        public pagereference savePDF()
    {
        if(doSave == 'No')
        {
            return null;
        }
        if(sign != null && GenContract.Send_Email__c != null)
        {
            attach.ParentID = contractid;
            attach.ContentType = 'application/pdf';
            attach.OwnerId = UserInfo.getUserId();
            List<Attachment> attachedFiles1 = new List<Attachment>();
              
            if(objRecType.name == '아큐브 멀티포컬 취급 약정서')
            {
                pagereference Pg = Page.Generate_Contract_1;
                Pg.getParameters().put('id', idVal);
                Pg.getParameters().put('doSave', 'No');
                pg.getParameters().put('contractid', contractid);
                Blob body;
                try 
                {
                    // returns the output of the page as a PDF
                    body = Pg.getContentAsPDF();
                    // need to pass unit test -- current bug    
                }
                catch (VisualforceException e)
                {
                    body = Blob.valueOf('UNIT.TEST');
                }
                attach.Body = body;
                attach.Name = 'Acuvue Multifocal Contract.pdf';
                attachedFiles1 = [select Id from Attachment where parentId =:contractId AND Name ='Acuvue Multifocal Contract.pdf' order By LastModifiedDate DESC limit 1];
            }
            
            else if(objRecType.name == '마이아큐브 프로그램 제공 및 사용 약정서')
            {
                pagereference Pg = Page.Generate_Contract_2;
                Pg.getParameters().put('id', idVal);
                Pg.getParameters().put('doSave', 'No');
                pg.getParameters().put('contractid', contractid);
                Blob body;
                try 
                {
                    // returns the output of the page as a PDF
                    body = Pg.getContentAsPDF();
                    // need to pass unit test -- current bug    
                }
                catch (VisualforceException e)
                {
                    body = Blob.valueOf('UNIT.TEST');
                }
                attach.Body = body;
                attach.Name = 'MyAcuvue Program Contract.pdf';
                attachedFiles1 = [select Id from Attachment where parentId =:contractId AND Name ='MyAcuvue Program Contract.pdf' order By LastModifiedDate DESC limit 1];
            }
            
            else if(objRecType.name == '아큐브 매출 및 지원 약정서')
            {
                pagereference Pg = Page.Generate_Contract_3;
                Pg.getParameters().put('id', idVal);
                Pg.getParameters().put('doSave', 'No');
                pg.getParameters().put('contractid', contractid);
                Blob body;
                try 
                {
                    // returns the output of the page as a PDF
                    body = Pg.getContentAsPDF();
                    // need to pass unit test -- current bug    
                }
                catch (VisualforceException e)
                {
                    body = Blob.valueOf('UNIT.TEST');
                }
                attach.Body = body;
                attach.Name = 'Revenue Target Agreement.pdf';
                attachedFiles1 = [select Id from Attachment where parentId =:contractId AND Name ='Revenue Target Agreement.pdf' order By LastModifiedDate DESC limit 1];
            }
            
            else if(objRecType.name == '공급계약서')
            {
                pagereference Pg = Page.Generate_Contract_4;
                Pg.getParameters().put('id', idVal);
                Pg.getParameters().put('doSave', 'No');
                pg.getParameters().put('contractid', contractid);
                Blob body;
                try 
                {
                    // returns the output of the page as a PDF
                    body = Pg.getContentAsPDF();
                    // need to pass unit test -- current bug    
                }
                catch (VisualforceException e)
                {
                    body = Blob.valueOf('UNIT.TEST');
                }
                attach.Body = body;
                attach.Name = 'Supply Contract.pdf';
                attachedFiles1 = [select Id from Attachment where parentId =:contractId AND Name ='Supply Contract.pdf' order By LastModifiedDate DESC limit 1];
           }
            
            if( attachedFiles1 != null && attachedFiles1.size() > 0)
            {
                attachedFiles1[0].Body = attach.Body;
                update attachedFiles1[0];
            }
            else
            {
                insert attach;
            }
            GenContract.Email_Sent__c = sendEmail(contractid,GenContract.Send_Email__c,attach);
            GenContract.View_Contract__c = atchId + [select Id from Attachment where parentId =:contractid order By LastModifiedDate DESC limit 1].id;
            update GenContract;
            message = 'Contract Generated and Emailed Successfully to '+GenContract.Send_Email__c;
        }
        else if(sign == null)
        {
            message = 'Signature is invalid. Please Review and Sign the contract';
        }
        else if(GenContract.Send_Email__c == null)
        {
           message = 'Email Id is invalid. Please provide valid email ID';
        }
        return null;
     }
    public String getFileId() 
    {
        List<Attachment> attachedFiles = new List<Attachment>();
        String fileId = '';
        if(objRecType.name == '아큐브 멀티포컬 취급 약정서'){        
        attachedFiles = [select Id from Attachment where parentId =:contractid AND Name ='Signature Contract 1' order By LastModifiedDate DESC limit 1];
        }
        else if(objRecType.name == '마이아큐브 프로그램 제공 및 사용 약정서'){
        attachedFiles = [select Id from Attachment where parentId =:contractid AND Name ='Signature Contract 2' order By LastModifiedDate DESC limit 1];
        }
        else if(objRecType.name == '아큐브 매출 및 지원 약정서'){
        attachedFiles = [select Id from Attachment where parentId =:contractid AND Name ='Signature Contract 3' order By LastModifiedDate DESC limit 1];
        }
        else if(objRecType.name == '공급계약서'){
        attachedFiles = [select Id from Attachment where parentId =:contractid AND Name ='Signature Contract 4' order By LastModifiedDate DESC limit 1];
        }
        
        if( attachedFiles != null && attachedFiles.size() > 0 ) 
        {
            fileId = attachedFiles[0].Id;
        }
        else
        {
            fileId = null;
        }
        
        return fileId;    
    } 
    
    Webservice static boolean sendEmail(Id contractid,String emailId,Attachment attach)
    {
        //System.Debug('c---'+contractid);
        List<Messaging.SingleEmailMessage> lstMsgs = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> sendMsgs = new List<Messaging.SingleEmailMessage>();
        List<Messaging.Emailfileattachment> file_list = new List<Messaging.Emailfileattachment>();
        Messaging.Emailfileattachment file = new Messaging.Emailfileattachment();
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        List<EmailTemplate> Templates = [Select Id,HtmlValue,Body, Name, IsActive, Folder.Name From EmailTemplate where Name='Contract Signature'];
        //system.debug(Templates);
        try
        {   
            String[] toaddress = emailId.split(';');
            email.setSenderDisplayName('Do-Not-Reply-(via Salesforce)');
            email.setToAddresses(toaddress);
            email.setTemplateId(Templates[0].Id); 
            email.setTargetObjectId([select Id from Contact where Email != null limit 1].Id); 
            email.setWhatId(contractid);                    
            if(attach!=null)
            {
                file.setFileName(attach.Name);
                file.setBody(attach.body);
                file_list.add(file);
                email.setFileAttachments(file_list);
            }
            System.Debug('email---'+email);
            lstMsgs.add(email);
            //System.Debug('lstMsgs---'+lstMsgs);
            Savepoint sp = Database.setSavepoint();
            Messaging.sendEmail(lstMsgs);
            System.Debug('lstMsgs----------'+lstMsgs);
            Database.rollback(sp);
            for(Messaging.SingleEmailMessage e : lstMsgs)
            {
               Messaging.SingleEmailMessage email1 = new Messaging.SingleEmailMessage();
               email1.setToAddresses(email.getToAddresses());
               email1.setSubject(email.getSubject());
               email1.setHTMLBody(e.getPlainTextBody());
              // email1.setHTMLBody(e.getHTMLBody());
               email1.setFileAttachments(email.getFileAttachments());
               sendMsgs.add(email1);
            }
            //System.Debug('sendMsgs--'+sendMsgs);
            Messaging.sendEmail(sendMsgs);//used to send multiple emails
            return true;
        }
        catch(Exception e)
        {
            //System.Debug(e);
            return false;
        }
    }
}