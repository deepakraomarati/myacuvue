public class IND_TrialRegistrationController {

    public String StoreName { get; set; }
    public boolean DisplayOasys { get; set; }
    
    public String Phone{ get; set; }
    //public String Temp{ get; set; }
    public String SAPID { get; set; }
    
    public String status { get; set; }

    public boolean showpanel { get; set; }
    
    public String fontcolor { get; set; }
    
    public String mnthSelected { get; set; }
    public SelectOption[] monthOptions { //this is where we're going to pull the list
        public get;
        private set;
    }
    public String custmerType { get; set; }
    public SelectOption[] TrialCustomerOptions { //this is where we're going to pull the list
        public get;
        private set;
    }
    public String trialBrnd { get; set; }
    public SelectOption[] TrialBrandOptions { //this is where we're going to pull the list
        public get;
        private set;
    }
    public String PrdSold { get; set; }
    public SelectOption[] ProductBrandOptions { //this is where we're going to pull the list
        public get;
        private set;
    }
    public String BoxCount { get; set; }
    public List<eTrial__c> Records {get; set;} 
    
    Public String LoggedInEcpId;
    
    public Boolean loggedOn{get;set;}
    
    /********  Constructor  *************/
    public IND_TrialRegistrationController() {
        fontcolor = 'Red';
        status = '';
        showpanel =false;
        String[] Months = new String[]{'','Jan-18','Feb-18','Mar-18','Apr-18','May-18','Jun-18','Jul-18','Aug-18','Sep-18','Oct-18','Nov-18','Dec-18'};
        this.monthOptions = new SelectOption[]{};
        for (String c: Months) {
            this.monthOptions.add(new SelectOption(c,c));
            }
        String[] TrialCustomer = new String[]{'','New Wearer','Acuvue Wearer','Other Contact Lens Wearer'};
        this.TrialCustomerOptions = new SelectOption[]{};
        for (String c: TrialCustomer) {
            this.TrialCustomerOptions.add(new SelectOption(c,c));
            }
        String[] TrialBrand = new String[]{'','Acuvue Oasys','Acuvue Oasys for Astigmatism','Acuvue Moist','Acuvue Moist for Astigmatism','Acuvue TruEye','Acuvue 2','Acuvue Vita','Trial given Earlier'};
        this.TrialBrandOptions = new SelectOption[]{};
        for (String c: TrialBrand) {
            this.TrialBrandOptions.add(new SelectOption(c,c));
            }
        String[] ProductBrand = new String[]{'','Acuvue Oasys','Acuvue Oasys for Astigmatism','Acuvue Moist','Acuvue Moist for Astigmatism','Acuvue TruEye','Acuvue 2','Acuvue Vita','Competition','None'};
        this.ProductBrandOptions = new SelectOption[]{};
        for (String c: ProductBrand) {
            this.ProductBrandOptions.add(new SelectOption(c,c));
            }
            String ECPId = ApexPages.currentPage().getParameters().get('ECPID');
    }
    
    public PageReference initLoad() {
          try{
              String ECPId = ApexPages.currentPage().getParameters().get('ECPID');
              System.debug('ECPId ##'+ECPId);
              
              if(ECPId == null || ECPId == ''){
                  ECPId = LoggedInEcpId;
              }
              
              loggedOn =  (ECPId != null && ECPId != '');
              //Records = [select Month__c, CustomerType__c, TrialBrand__c,ProductBrand__c,BoxCount__c from eTrial__c where Account__c=:ECPId]; 
              if(loggedOn){
                                         
                    List<Account> loggedinECP = [SELECT Id,Name,Phone,OutletNumber__c,Marketing_Program__c FROM Account WHERE Id=:ECPId LIMIT 1];
                    System.debug('loggedinECP:'+loggedinECP);   
                    StoreName = loggedinECP[0].Name;
                    if(loggedinECP.isEmpty()&& loggedinECP.size()==0){
                        return Logout();  
                    }
              }
              else{
                  return Logout();
              }
           }catch(Exception e){
             return Logout();
         }
           return null;
    }
     /********  ECP Login  *************/
    public PageReference Login() {
        
        showpanel =false;
        status ='';
        fontcolor = ''; 
        
        try { 
                Map<String,ExactTarget_Integration_Settings__c> MapcampId = ExactTarget_Integration_Settings__c.getAll();
                
                String  AccountqryString = 'SELECT Id,Name,Phone,OutletNumber__c,Marketing_Program__c FROM Account WHERE OutletNumber__c=:SAPID AND Phone=:phone LIMIT 1';
                System.debug('Account qryStr:'+AccountqryString);
                List<Account> lstaccount =  Database.Query(AccountqryString);
                System.debug('Account list:'+lstaccount);
                
                if(!lstaccount.isEmpty()&& lstaccount.size()>0){
                        LoggedInEcpId = lstaccount[0].Id;
                        PageReference redeem= Page.IND_Trial_Registration_Form;
                        redeem.getParameters().put('ECPID',lstaccount[0].Id);
                        redeem.setRedirect(true);
                        return redeem;     
                    }
                else{
                        return ResultMessage(System.Label.Invalid_ECP,'Red');
                    }
        }catch (Exception e) {
             return ResultMessage(e.getMessage(),'Red');
        }
                   
        return null;
    }
    
    /********  Redeem e-Vocher  *************/
    public PageReference RedeemVoucher() {
        
        showpanel =false;
        status ='';
        fontcolor = ''; 
        
        try { 
                Map<String,ExactTarget_Integration_Settings__c> MapcampId = ExactTarget_Integration_Settings__c.getAll();
                
                eTrial__c TrialRecord = new eTrial__c();
                TrialRecord.Month__c = mnthSelected;
                TrialRecord.eTrial_Source__c = 'Ind Trial';
                TrialRecord.CustomerType__c = custmerType;
                TrialRecord.TrialBrand__c = trialBrnd;
                TrialRecord.ProductBrand__c = PrdSold;
                TrialRecord.Account__c = ApexPages.currentPage().getParameters().get('ECPID');
                TrialRecord.Name = 'IND Trial - ' + ApexPages.currentPage().getParameters().get('ECPID');
                TrialRecord.BoxCount__c = Integer.ValueOf(BoxCount);
                insert TrialRecord;
                return ResultMessage('Trial registered successfully','Green'); 
             
        }catch (Exception e) {
             return ResultMessage(e.getMessage(),'Red');
        }
      //return null;
    }
    
    /********  Refresh ECP Status Form  *************/
    public PageReference Refresh() {
        //resetform();
                       
        try { 
                Map<String,ExactTarget_Integration_Settings__c> MapcampId = ExactTarget_Integration_Settings__c.getAll();
                
                String AccountId= ApexPages.currentPage().getParameters().get('ECPID');
                System.debug('AccountId ##'+AccountId);
                
                if(AccountId==''||AccountId==Null){
                    AccountId = LoggedInEcpId; 
                }
                Records = [select Month__c, CustomerType__c, TrialBrand__c,ProductBrand__c,BoxCount__c from eTrial__c where Account__c=:AccountId]; 
         }
         catch (Exception e) {
             return ResultMessage(e.getMessage(),'Red');
        }
        return null;
    }
    public PageReference Logout() {
        PageReference login = Page.IND_ECP_Login;
        login.setRedirect(true);
        return login;   
    }
        
    /********  Reset the form  *************/
    Public void  resetform(){
        SAPID = '';
        Phone = '';
        mnthSelected = '';
        custmerType = '';
        trialBrnd = '';
        PrdSold= '';
        BoxCount= '';
    }
    
    /********  Return Result  *************/
    Public PageReference ResultMessage(String MessageDescription,String textcolor){
        status = MessageDescription;
        fontcolor = textcolor;
        showpanel = true;
        resetform();
        return null;
    }
}