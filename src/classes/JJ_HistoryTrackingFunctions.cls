/**
 *      @author Kavita Dodamani
 *      @date   27/09/2016
        @description    Class for trigger functions on History tracking 
 
        Function: Handles the logic used in custom History tracking triggers.
        Modification Log:
        ------------------------------------------------------------------------------------
        Developer                       Date                Description
        Kavita Dodamani                 27/09/2016          initial version 
        ------------------------------------------------------------------------------------
*/
Public Class JJ_HistoryTrackingFunctions{

Public Static void insertHistory(List<sobject> newList,List<Schema.FieldSetMember> trackedFields,String objectAPIName){
 final List<JJ_JPN_FieldHistoryTracking__c> fieldChanges = new List<JJ_JPN_FieldHistoryTracking__c>();
 for (sobject newObj : newList) {

        final sobject oldObj = trigger.oldmap.get(newObj.Id);
       
        for (Schema.FieldSetMember fsm : trackedFields) {

            String fieldName  = fsm.getFieldPath();
            String fieldLabel = fsm.getLabel();

            if (newObj.get(fieldName) == oldObj.get(fieldName))
                continue;

            String oldValue = String.valueOf(oldObj.get(fieldName));
            String newValue = String.valueOf(newObj.get(fieldName));

            if (oldValue != null && oldValue.length()>255) 
                oldValue = oldValue.substring(0,255);

            if (newValue != null && newValue.length()>255) 
                newValue = newValue.substring(0,255); 

            final JJ_JPN_FieldHistoryTracking__c caseHistory = 
                new JJ_JPN_FieldHistoryTracking__c();

            caseHistory.name             = fieldLabel;
            caseHistory.JJ_JPN_apiName__c    = fieldName;
            caseHistory.JJ_JPN_RecordID__c   = newObj.Id;
            if(objectAPIName=='JJ_JPN_CustomerMasterRequest__c')
            caseHistory.JJ_JPN_CustomerMasterRequest__c= newObj.Id;
            caseHistory.JJ_JPN_OldValue__c   = oldValue;
            caseHistory.JJ_JPN_NewValue__c   = newValue;
            caseHistory.JJ_JPN_Object__c     = objectAPIName;        
            fieldChanges.add(caseHistory);
        }
    }

    if (!fieldChanges.isEmpty()) {
        insert fieldChanges;
    }

}

}