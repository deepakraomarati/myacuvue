public class JJ_JPN_AccountPlanTotalForAP 
{
    List<JJ_JPN_AccountPlan__c> allAccountPlanUpdValues = new List<JJ_JPN_AccountPlan__c>();
    public void AccountPlanTotalForAP( map<id,List<Opportunity>> mapAllActionPlanAP)
    {
        for(JJ_JPN_AccountPlan__c accountPlan :[select id,JJ_JPN_AccountPlanName__c,JJ_JPN_Upside__c,JJ_JPN_PrimaryAccountPlan__c from JJ_JPN_AccountPlan__c where JJ_JPN_PrimaryAccountPlan__c IN: mapAllActionPlanAP.keySet()])
        {
            if(mapAllActionPlanAP.containsKey(accountPlan.JJ_JPN_PrimaryAccountPlan__c))
                {
                  //  accountPlan.JJ_JPN_Upside__c = '0';
                    for( Opportunity opty: mapAllActionPlanAP.get(accountPlan.JJ_JPN_PrimaryAccountPlan__c))
                    {
                        if(opty.JJ_JPN_Jan__c !=null && accountPlan.JJ_JPN_AccountPlanName__c=='Jan')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = string.valueOf(i+opty.JJ_JPN_Jan__c);
                        }
                        
                        if(opty.JJ_JPN_Feb__c !=null && accountPlan.JJ_JPN_AccountPlanName__c=='Feb')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = string.valueOf(i+opty.JJ_JPN_Feb__c);
                        }
                        
                        if(opty.JJ_JPN_Mar__c !=null && accountPlan.JJ_JPN_AccountPlanName__c=='Mar')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = string.valueOf(i+opty.JJ_JPN_Mar__c);
                        }
                        
                        if(opty.JJ_JPN_Apr__c !=null && accountPlan.JJ_JPN_AccountPlanName__c=='Apr')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = string.valueOf(i+opty.JJ_JPN_Apr__c);
                        }
                        
                        if(opty.JJ_JPN_May__c !=null && accountPlan.JJ_JPN_AccountPlanName__c=='May')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = string.valueOf(i+opty.JJ_JPN_May__c);
                        }
                        
                        if(opty.JJ_JPN_Jun__c !=null && accountPlan.JJ_JPN_AccountPlanName__c=='Jun')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = string.valueOf(i+opty.JJ_JPN_Jun__c);
                        }
                        
                        if(opty.JJ_JPN_Jul__c !=null && accountPlan.JJ_JPN_AccountPlanName__c=='Jul')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = string.valueOf(i+opty.JJ_JPN_Jul__c);
                        }
                        
                        if(opty.JJ_JPN_Aug__c !=null && accountPlan.JJ_JPN_AccountPlanName__c=='Aug')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = string.valueOf(i+opty.JJ_JPN_Aug__c);
                        }
                        
                        if(opty.JJ_JPN_Sep__c !=null && accountPlan.JJ_JPN_AccountPlanName__c=='Sep')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = string.valueOf(i+opty.JJ_JPN_Sep__c);
                        }
                        
                        if(opty.JJ_JPN_Oct__c !=null && accountPlan.JJ_JPN_AccountPlanName__c=='Oct')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = string.valueOf(i+opty.JJ_JPN_Oct__c);
                        }
                        
                        if(opty.JJ_JPN_Nov__c !=null && accountPlan.JJ_JPN_AccountPlanName__c=='Nov')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = string.valueOf(i+opty.JJ_JPN_Nov__c);
                        }
                        
                        if(opty.JJ_JPN_Dec__c !=null && accountPlan.JJ_JPN_AccountPlanName__c=='Dec')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = string.valueOf(i+opty.JJ_JPN_Dec__c);
                        }   
                        
                        if(opty.JJ_JPN_Jan__c !=null && opty.JJ_JPN_Feb__c!=null && opty.JJ_JPN_Mar__c!=null && accountPlan.JJ_JPN_AccountPlanName__c=='Quarter1')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = String.valueOf(i+opty.JJ_JPN_Jan__c+opty.JJ_JPN_Feb__c+opty.JJ_JPN_Mar__c);
                        }
                        
                        if(opty.JJ_JPN_Apr__c !=null && opty.JJ_JPN_May__c!=null && opty.JJ_JPN_Jun__c!=null && accountPlan.JJ_JPN_AccountPlanName__c=='Quarter2')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = String.valueOf(i+opty.JJ_JPN_Apr__c+opty.JJ_JPN_May__c+opty.JJ_JPN_Jun__c);
                        }
                        
                        if(opty.JJ_JPN_Jul__c !=null && opty.JJ_JPN_Aug__c!=null && opty.JJ_JPN_Sep__c!=null && accountPlan.JJ_JPN_AccountPlanName__c=='Quarter3')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = String.valueOf(i+opty.JJ_JPN_Jul__c+opty.JJ_JPN_Aug__c+opty.JJ_JPN_Sep__c);
                        }
                        
                        if(opty.JJ_JPN_Oct__c !=null && opty.JJ_JPN_Nov__c!=null && opty.JJ_JPN_Dec__c!=null && accountPlan.JJ_JPN_AccountPlanName__c=='Quarter4')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = String.valueOf(i+opty.JJ_JPN_Oct__c+opty.JJ_JPN_Nov__c+opty.JJ_JPN_Dec__c);
                        }      
                        
                        if(opty.JJ_JPN_Jan__c !=null && opty.JJ_JPN_Feb__c !=null && opty.JJ_JPN_Mar__c!=null && opty.JJ_JPN_Apr__c !=null && opty.JJ_JPN_May__c !=null && opty.JJ_JPN_Jun__c !=null && opty.JJ_JPN_Jul__c !=null && opty.JJ_JPN_Aug__c !=null && opty.JJ_JPN_Sep__c !=null && opty.JJ_JPN_Oct__c !=null && opty.JJ_JPN_Nov__c !=null  && opty.JJ_JPN_Dec__c !=null && accountPlan.JJ_JPN_AccountPlanName__c=='Year')
                        {
                            integer i = integer.valueOf(accountPlan.JJ_JPN_Upside__c!=null?accountPlan.JJ_JPN_Upside__c:string.valueOf(0));
                            accountPlan.JJ_JPN_Upside__c = String.valueOf(i+opty.JJ_JPN_Jan__c+opty.JJ_JPN_Feb__c+opty.JJ_JPN_Mar__c+opty.JJ_JPN_Apr__c+opty.JJ_JPN_May__c+opty.JJ_JPN_Jun__c+opty.JJ_JPN_Jul__c+opty.JJ_JPN_Aug__c+opty.JJ_JPN_Sep__c+opty.JJ_JPN_Oct__c+opty.JJ_JPN_Nov__c+opty.JJ_JPN_Dec__c);
                        }   
                        
                    }
                    system.debug(accountPlan+'*****accountPlan');
                    allAccountPlanUpdValues.add(accountPlan);
                }
            }
            update allAccountPlanUpdValues ;
        }
    }