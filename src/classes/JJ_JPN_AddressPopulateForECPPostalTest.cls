@isTest
private class JJ_JPN_AddressPopulateForECPPostalTest {
    @isTest static void test_method_ECPPostalCode() {
        test.StartTest();
        JJ_JPN_Address__c address= new JJ_JPN_Address__c(JJ_JPN_City__c='Bangalore',JJ_JPN_PostalCodes__c='999999',JJ_JPN_Street__c='MTP(India)');
        insert address;
        JJ_JPN_CustomerMasterRequest__c cmr=new JJ_JPN_CustomerMasterRequest__c(Name='Test',JJ_JPN_PayerCode__c='47583',JJ_JPN_AccountType__c='Field Sales(FS)',JJ_JPN_PayerNameKanji__c='漢字',JJ_JPN_PayerNameKana__c='ｶﾝｼﾞ',JJ_JPN_PayerPostalCode__c='1993891',JJ_JPN_RegistrationFor__c='435353',JJ_JPN_OfficeName__c='sdcevfe',JJ_JPN_PersonInchargeName__c='sfdcvcfd',JJ_JPN_SalesItem__c='R',JJ_JPN_Rank__c='A2',JJ_JPN_CreditPersonInchargeName__c='ksjnvf',JJ_JPN_PersonInchargeCode__c='93284',JJ_JPN_StatusCode__c='H',JJ_JPN_RepresentativeNameKana__c='scdscds',JJ_JPN_RepresentativeNameKanji__c='bvsf',JJ_JPN_PaymentCondition__c='ZJ47',JJ_JPN_PaymentMethod__c='D',JJ_JPN_ReturnFAXNo__c='9483579',JJ_JPN_SoldToPostalCode__c='1234567');
        insert cmr;
        
        JJ_JPN_ECPContact__c ecp= new JJ_JPN_ECPContact__c(Name='TestClass',JJ_JPN_PostalCode__c='999999');    
        ecp.ECP__c=cmr.Id;    
        insert ecp;
        
        JJ_JPN_Address__c addressQuery=[select id,JJ_JPN_City__c,JJ_JPN_PostalCodes__c,JJ_JPN_State__c,JJ_JPN_Street__c from JJ_JPN_Address__c where JJ_JPN_PostalCodes__c =:ecp.JJ_JPN_PostalCode__c];
        
        ecp.JJ_JPN_TownshipsCity__c=addressQuery.JJ_JPN_City__c;
        ecp.JJ_JPN_Street__c=addressQuery.JJ_JPN_Street__c;
        update ecp;
        system.assertEquals(ecp.ECP__c,cmr.Id,'success');
        test.stopTest();
    }
    @isTest Static void test_method_CMRPostalCode(){
        test.StartTest();
        JJ_JPN_Address__c address1= new JJ_JPN_Address__c(JJ_JPN_City__c='Bangalore',JJ_JPN_PostalCodes__c='47584',JJ_JPN_Street__c='MTP',JJ_JPN_StateJapan__c='UP');
        insert address1;
        JJ_JPN_Address__c address= new JJ_JPN_Address__c(JJ_JPN_City__c='New Delhi',JJ_JPN_PostalCodes__c='47583',JJ_JPN_Street__c='MTP',JJ_JPN_StateJapan__c='UP');
        insert address;
        
        JJ_JPN_CustomerMasterRequest__c cmr=new JJ_JPN_CustomerMasterRequest__c(Name='Test',JJ_JPN_PayerCode__c='47583',JJ_JPN_AccountType__c='Field Sales(FS)',JJ_JPN_PayerNameKanji__c='漢字',
                                                                                JJ_JPN_PayerNameKana__c='ｶﾝｼﾞ',JJ_JPN_PayerPostalCode__c='4758312',JJ_JPN_RegistrationFor__c='435353',JJ_JPN_OfficeName__c='sdcevfe',
                                                                                JJ_JPN_PersonInchargeName__c='sfdcvcfd',JJ_JPN_SalesItem__c='R',JJ_JPN_Rank__c='A2',JJ_JPN_CreditPersonInchargeName__c='ksjnvf',
                                                                                JJ_JPN_PersonInchargeCode__c='93284',JJ_JPN_StatusCode__c='H',JJ_JPN_RepresentativeNameKana__c='scdscds',JJ_JPN_RepresentativeNameKanji__c='bvsf',
                                                                                JJ_JPN_PaymentCondition__c='ZJ47',JJ_JPN_PaymentMethod__c='D',JJ_JPN_ReturnFAXNo__c='9483579',JJ_JPN_BillToPostalCode__c='4758312',
                                                                                JJ_JPN_SoldToPostalCode__c='4758323',JJ_JPN_PayerTownshipCity__c='New Delhi',JJ_JPN_SoldToTownshipCity__c='New Delhi',JJ_JPN_BillToTownshipCity__c='New Delhi',
                                                                                JJ_JPN_SoldToState__c='UP',JJ_JPN_BillToState__c='UP',JJ_JPN_PayerState__c='UP',JJ_JPN_PayerStreet__c='MTP',JJ_JPN_SoldToStreet__c='MTP',JJ_JPN_BillToStreet__c='MTP');
        
        insert cmr;
        // if(cmr.JJ_JPN_PayerPostalCode__c==address.JJ_JPN_PostalCodes__c)
        {   
            System.assertEquals(address.JJ_JPN_City__c,cmr.JJ_JPN_PayerTownshipCity__c);
            System.assertEquals(address.JJ_JPN_StateJapan__c,cmr.JJ_JPN_PayerState__c);
            System.assertEquals(address.JJ_JPN_Street__c,cmr.JJ_JPN_PayerStreet__c);
        }
        //  if(cmr.JJ_JPN_BillToPostalCode__c==address.JJ_JPN_PostalCodes__c)
        {   
            System.assertEquals(address.JJ_JPN_City__c,cmr.JJ_JPN_BillToTownshipCity__c);
            System.assertEquals(address.JJ_JPN_StateJapan__c,cmr.JJ_JPN_BillToState__c);
            System.assertEquals(address.JJ_JPN_Street__c,cmr.JJ_JPN_BillToStreet__c);
        }
        //  if(cmr.JJ_JPN_SoldToPostalCode__c==address.JJ_JPN_PostalCodes__c)
        {   
            System.assertEquals(address.JJ_JPN_City__c,cmr.JJ_JPN_SoldToTownshipCity__c);
            System.assertEquals(address.JJ_JPN_StateJapan__c,cmr.JJ_JPN_SoldToState__c);
            System.assertEquals(address.JJ_JPN_Street__c,cmr.JJ_JPN_SoldToStreet__c);
        }
        test.stopTest();
    }
}