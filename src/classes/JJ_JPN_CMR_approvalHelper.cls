Public  with sharing class JJ_JPN_CMR_approvalHelper{

    Public static void processapprovalstep(List<JJ_JPN_CustomerMasterRequest__c> CMRLIST){
        //set<String> CMRIDS=new set <string>();
        //map<id,string>CMRIDS= new map<id,string>();
        id cmrid=null;
        Id id1 = UserInfo.getProfileId();
        string profilename=[Select Name from Profile where Id = :id1].Name ;
        if(profilename=='System Administrator')
            return;
        try{
        for(JJ_JPN_CustomerMasterRequest__c CMR : CMRLIST ){
        //system.debug('-----JJ_JPN_SalesAdminUserCapturing__c-------'+CMR.JJ_JPN_SalesAdminUserCapturing__c);
            if(CMR.JJ_JPN_SamManager__c !=null && CMR.JJ_JPN_AccountType__c=='SAM/SAM Dealer'){
                //CMRIDS.put(CMR.id,CMR.JJ_JPN_SamManager__c);
                cmrid=CMR.JJ_JPN_SamManager__c;
                id sauid=findsauidsam(cmrid);
                system.debug('----findsauidsam(cmrid)-----'+sauid);
                CMR.JJ_JPN_SalesAdminUserCapturing__c=sauid;
            }
            else
            if(CMR.JJ_JPN_SamManager__c ==null && CMR.JJ_JPN_AccountType__c=='Field Sales(FS)'){
                //CMRIDS.put(CMR.id,string.valueof(CMR.CreatedBy));
                cmrid=userinfo.getUserId();
                id sauid=findsauid(cmrid);
                system.debug('---sauid---'+sauid);
                CMR.JJ_JPN_SalesAdminUserCapturing__c=sauid;
            }
        }
        }
        //try{
           // update CMRLIST;
        //}
        catch(exception e){
            //System.debug('Exception:'+ex);
          //  CMRLIST[0].adderror('You are nor allowed to create Customer Master Record: '+e.getmessage());
            CMRLIST[0].adderror('ログイン中のユーザーとして新規得意先申請できません。権限はありません。');       
        }
    } 
    public static ID findsauid(id CMRID){
        ID SAUID=null;
        user[] SM= new list<User>();
        user[] SSM= new list<User>();
        user[] FM= new list<User>();
        user[] SAU= new list<User>();
        //List<user> SM= new List<User>();
        //List<user> SM= new List<User>();
        
        SM=[Select ManagerId from user where id=:CMRID limit 1];
       // system.debug('-----SM-----'+SM[0].ManagerId);
        //ID SMID=SM[0].ManagerId;
        //system.debug('-----SMID-----'+SMID);
        if(SM.size()>0&&(SM[0].ManagerId!=null||SM[0].ManagerId!=''))
            SSM=[Select ManagerId from user where id=:SM[0].ManagerId limit 1];
            //system.debug('')
      //  system.debug('-----SSM-----'+SSM[0].ManagerId);
        if(SSM[0].ManagerId!=null||SSM[0].ManagerId!='')
            FM=[Select ManagerId from user where id = :SSM[0].ManagerId limit 1];
        //system.debug('-----FM-----'+FM[0].ManagerId);
        if(FM[0].ManagerId!=null||FM[0].ManagerId!='')
            SAU=[Select ManagerId from user where id = :FM[0].ManagerId limit 1];
        //system.debug('-----FM-----'+SAU[0].ManagerId);
        if(SAU[0].ManagerId!=null||SAU[0].ManagerId!='')
            SAUID=SAU[0].ManagerId;
        //system.debug('-----SAUID-----'+SAUID);                                            
        return(SAUID);
    
    }
    
    Private static ID findsauidsam(id CMRID){
        ID SAUID=null;
        List<user> SM= new List<User>();
        List<user> SSM= new List<User>();
        List<user> FM= new List<User>();
        List<user> SAU= new List<User>();
        //List<user> SM= new List<User>();
        //List<user> SM= new List<User>();
        
            SSM=[Select ManagerId from user where id = :CMRID];
            //system.debug('')

        if(!SSM.isempty())
            FM=[Select ManagerId from user where id = :SSM[0].ManagerId];

        if(!FM.isempty())
            SAU=[Select ManagerId from user where id = :FM[0].ManagerId];

        if(!SAU.isempty() && Limits.getQueries()<10)
            SAUID=SAU[0].ManagerId;
                                            
        return(SAUID);
    
    }
 
}