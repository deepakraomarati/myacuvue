@isTest
public class JJ_JPN_CMR_approvalTest {
    @isTest Static void test_method_CMR_Approval(){
        test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User SalesRep = new User(Alias = 'trep', Email='standardusersalesrep@testorg.com',EmailEncodingKey='UTF-8', LastName='Testingrep', LanguageLocaleKey='en_US', 
                                 LocaleSidKey='en_US', ProfileId = p.Id,Unique_User_Id__c='SalesRepUniqueID' ,TimeZoneSidKey='America/Los_Angeles', UserName='standardusersalesrep@testorg.com');
        insert Salesrep;
        
        System.runAs(SalesRep){        
            JJ_JPN_CustomerMasterRequest__c cmr=new JJ_JPN_CustomerMasterRequest__c(Name='Test',JJ_JPN_PayerCode__c='47583',JJ_JPN_AccountType__c='Field Sales(FS)',JJ_JPN_PayerNameKanji__c='漢字',
                                                                                    JJ_JPN_PayerNameKana__c='ｶﾝｼﾞ',JJ_JPN_PayerPostalCode__c='4758312',JJ_JPN_RegistrationFor__c='435353',JJ_JPN_OfficeName__c='sdcevfe',
                                                                                    JJ_JPN_PersonInchargeName__c='sfdcvcfd',JJ_JPN_SalesItem__c='R',JJ_JPN_Rank__c='A2',JJ_JPN_CreditPersonInchargeName__c='ksjnvf',
                                                                                    JJ_JPN_PersonInchargeCode__c='93284',JJ_JPN_StatusCode__c='H',JJ_JPN_RepresentativeNameKana__c='scdscds',JJ_JPN_RepresentativeNameKanji__c='bvsf',
                                                                                    JJ_JPN_PaymentCondition__c='ZJ47',JJ_JPN_PaymentMethod__c='D',JJ_JPN_ReturnFAXNo__c='9483579');
            insert cmr;
            system.assertEquals(cmr.JJ_JPN_PayerCode__c, '47583', 'succcess');
            test.stopTest();            
        }     
    }    
}