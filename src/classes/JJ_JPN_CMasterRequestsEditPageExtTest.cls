/** 
* File Name: JJ_JPN_CMasterRequestsEditPageExtTest
* Description : Test Class for JJ_JPN_CustomerMasterRequestsEditPageExt - Business logic applied for Customer Master Regisrtion..
* Main Class : JJ_JPN_CustomerMasterRequestsEditPageExt
* Copyright : Johnson & Johnson
* @author : Shiva Shankar | sshank10@its.jnj.com | bnshiva.shankar@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |11-July-2016 |@sshank10@its.jnj.com |New Class created
*/ 


@isTest
public class JJ_JPN_CMasterRequestsEditPageExtTest 
{
    static testMethod void createCMR()
    {
        Test.startTest(); 
        
        JJ_JPN_CustomerMasterRequest__c customerrequest =  new JJ_JPN_CustomerMasterRequest__c();
        customerrequest.Name = 'CMRAccout';
        customerrequest.JJ_JPN_AccountType__c = 'SAM/SAM Dealer';
        customerrequest.JJ_JPN_PayerCode__c='47583';
        customerrequest.JJ_JPN_PayerNameKanji__c='漢字';
        customerrequest.JJ_JPN_PayerNameKana__c='ｶﾝｼﾞ';
        customerrequest.JJ_JPN_PayerPostalCode__c='4758312';
        customerrequest.JJ_JPN_RegistrationFor__c='435353';
        customerrequest.JJ_JPN_OfficeName__c='sdcevfe';
        customerrequest.JJ_JPN_PersonInchargeName__c='sfdcvcfd';
        customerrequest.JJ_JPN_SalesItem__c='R';
        customerrequest.JJ_JPN_Rank__c='A2';
        customerrequest.JJ_JPN_CreditPersonInchargeName__c='ksjnvf';
        customerrequest.JJ_JPN_PersonInchargeCode__c='93284';
        customerrequest.JJ_JPN_StatusCode__c='H';
        customerrequest.JJ_JPN_RepresentativeNameKana__c='scdscds';
        customerrequest.JJ_JPN_RepresentativeNameKanji__c='bvsf';
        customerrequest.JJ_JPN_PaymentCondition__c='ZJ47';
        customerrequest.JJ_JPN_PaymentMethod__c='D';
        customerrequest.JJ_JPN_ReturnFAXNo__c='9483579';
        insert customerrequest;
        
        system.debug('$$$NameTest==>'+customerrequest.JJ_JPN_PayerNameKanji__c);
        system.debug('$$$Name1Test==>'+customerrequest.Name);
        
        Account acc = new Account();
        acc.Name = 'AccName';
        acc.JJ_JPN_CustomerNameKana__c = 'Kana';
        insert acc;
        
        AutoNumberCMR__c autoNum = new AutoNumberCMR__c();
        autoNum.Name = 'Account Outlet';
        autoNum.JJ_JPN_AutoNumberOutlet__c = 10001;
        Insert autoNum;
        
        system.debug('$$autoNum==>'+autoNum);
        

        // creating the ID for the loading class  and intializes id to class , for the beginnning of the page
        Apexpages.currentPage().getParameters().put('accType',customerrequest.JJ_JPN_AccountType__c);
        Apexpages.currentPage().getParameters().put('Radio','Radio1');
        Apexpages.currentPage().getParameters().put('payerAcc',acc.id);
        Apexpages.currentPage().getParameters().put('billToAcc',acc.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(customerrequest);
        JJ_JPN_CustomerMasterRequestsEditPageExt CMRCls = new JJ_JPN_CustomerMasterRequestsEditPageExt(sc);
       
        CMRCls.Radio='Radio1';
        CMRCls.autoPopulateValues();
        CMRCls.autoPopulateAccountValues();
        CMRCls.randomNumberGenerator(); 
        CMRCls.doSave();
        CMRCls.Cancel();
        
      
        PageReference pageRef = Page.CustomerMasterRequestsEditPage;
        Test.setCurrentPage(pageRef);
        
        system.assertEquals(acc.Name,acc.Name ,'HURRY ITS WRONG OUTPUT');
        system.assertNotEquals('CMR',acc.Name ,'HURRY ITS WRONG OUTPUT');
        system.assert(acc.Name==acc.Name ,'HURRY ITS WRONG OUTPUT');
        
        Test.stopTest();
    }
    
    static testMethod void Radio2()
    {
        Test.startTest(); 
        
        JJ_JPN_CustomerMasterRequest__c customerrequest =  new JJ_JPN_CustomerMasterRequest__c();
        customerrequest.Name = 'CMRAccout';
        customerrequest.JJ_JPN_AccountType__c = 'SAM/SAM Dealer';
        customerrequest.JJ_JPN_PayerCode__c='47583';
        customerrequest.JJ_JPN_PayerNameKanji__c='漢字';
        customerrequest.JJ_JPN_PayerNameKana__c='ｶﾝｼﾞ';
        customerrequest.JJ_JPN_PayerPostalCode__c='4758312';
        customerrequest.JJ_JPN_RegistrationFor__c='435353';
        customerrequest.JJ_JPN_OfficeName__c='sdcevfe';
        customerrequest.JJ_JPN_PersonInchargeName__c='sfdcvcfd';
        customerrequest.JJ_JPN_SalesItem__c='R';
        customerrequest.JJ_JPN_Rank__c='A2';
        customerrequest.JJ_JPN_CreditPersonInchargeName__c='ksjnvf';
        customerrequest.JJ_JPN_PersonInchargeCode__c='93284';
        customerrequest.JJ_JPN_StatusCode__c='H';
        customerrequest.JJ_JPN_RepresentativeNameKana__c='scdscds';
        customerrequest.JJ_JPN_RepresentativeNameKanji__c='bvsf';
        customerrequest.JJ_JPN_PaymentCondition__c='ZJ47';
        customerrequest.JJ_JPN_PaymentMethod__c='D';
        customerrequest.JJ_JPN_ReturnFAXNo__c='9483579';
        insert customerrequest;
        
        system.debug('$$$NameTest==>'+customerrequest.JJ_JPN_PayerNameKanji__c);
        system.debug('$$$Name1Test==>'+customerrequest.Name);
        
        Account acc = new Account();
        acc.Name = 'AccName';
        acc.JJ_JPN_CustomerNameKana__c = 'Kana';
        insert acc;
        
        AutoNumberCMR__c autoNum = new AutoNumberCMR__c();
        autoNum.Name = 'Account Outlet';
        autoNum.JJ_JPN_AutoNumberOutlet__c = 10001;
        insert autoNum;

        // creating the ID for the loading class  and intializes id to class , for the beginnning of the page
        Apexpages.currentPage().getParameters().put('accType',customerrequest.JJ_JPN_AccountType__c);
        Apexpages.currentPage().getParameters().put('Radio','Radio2');
        Apexpages.currentPage().getParameters().put('payerAcc',acc.id);
        Apexpages.currentPage().getParameters().put('billToAcc',acc.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(customerrequest);
        JJ_JPN_CustomerMasterRequestsEditPageExt CMRCls = new JJ_JPN_CustomerMasterRequestsEditPageExt(sc);
        CMRCls.Radio='Radio2';
        CMRCls.autoPopulateAccountValues();
        CMRCls.randomNumberGenerator(); 
        CMRCls.doSave();
        CMRCls.Cancel();
        
      
        PageReference pageRef = Page.CustomerMasterRequestsEditPage;
        Test.setCurrentPage(pageRef);
        
        system.assertEquals(acc.Name,acc.Name ,'HURRY ITS WRONG OUTPUT');
        system.assertNotEquals('CMR',acc.Name ,'HURRY ITS WRONG OUTPUT');
        system.assert(acc.Name==acc.Name ,'HURRY ITS WRONG OUTPUT');
        
        Test.stopTest();
    }
    static testMethod void Radio3()
    {
        Test.startTest(); 
        
        JJ_JPN_CustomerMasterRequest__c customerrequest =  new JJ_JPN_CustomerMasterRequest__c();
        customerrequest.Name = 'CMRAccout';
        customerrequest.JJ_JPN_AccountType__c = 'SAM/SAM Dealer';
        customerrequest.JJ_JPN_PayerCode__c='47583';
        customerrequest.JJ_JPN_PayerNameKanji__c='漢字';
        customerrequest.JJ_JPN_PayerNameKana__c='ｶﾝｼﾞ';
        customerrequest.JJ_JPN_PayerPostalCode__c='4758312';
        customerrequest.JJ_JPN_RegistrationFor__c='435353';
        customerrequest.JJ_JPN_OfficeName__c='sdcevfe';
        customerrequest.JJ_JPN_PersonInchargeName__c='sfdcvcfd';
        customerrequest.JJ_JPN_SalesItem__c='R';
        customerrequest.JJ_JPN_Rank__c='A2';
        customerrequest.JJ_JPN_CreditPersonInchargeName__c='ksjnvf';
        customerrequest.JJ_JPN_PersonInchargeCode__c='93284';
        customerrequest.JJ_JPN_StatusCode__c='H';
        customerrequest.JJ_JPN_RepresentativeNameKana__c='scdscds';
        customerrequest.JJ_JPN_RepresentativeNameKanji__c='bvsf';
        customerrequest.JJ_JPN_PaymentCondition__c='ZJ47';
        customerrequest.JJ_JPN_PaymentMethod__c='D';
        customerrequest.JJ_JPN_ReturnFAXNo__c='9483579';
        insert customerrequest;
        
        system.debug('$$$NameTest==>'+customerrequest.JJ_JPN_PayerNameKanji__c);
        system.debug('$$$Name1Test==>'+customerrequest.Name);
        
        Account acc = new Account();
        acc.Name = 'AccName';
        acc.JJ_JPN_CustomerNameKana__c = 'Kana';
        insert acc;
        
        AutoNumberCMR__c autoNum = new AutoNumberCMR__c();
        autoNum.Name = 'Account Outlet';
        autoNum.JJ_JPN_AutoNumberOutlet__c = 10001;
        insert autoNum;
        

        // creating the ID for the loading class  and intializes id to class , for the beginnning of the page
        Apexpages.currentPage().getParameters().put('accType',customerrequest.JJ_JPN_AccountType__c);
        Apexpages.currentPage().getParameters().put('Radio','Radio3');
        Apexpages.currentPage().getParameters().put('payerAcc',acc.id);
        Apexpages.currentPage().getParameters().put('billToAcc',acc.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(customerrequest);
        JJ_JPN_CustomerMasterRequestsEditPageExt CMRCls = new JJ_JPN_CustomerMasterRequestsEditPageExt(sc);
        CMRCls.Radio='Radio3';
        CMRCls.autoPopulateAccountValues();
        CMRCls.randomNumberGenerator(); 
        CMRCls.doSave();
        CMRCls.Cancel();
        
      
        PageReference pageRef = Page.CustomerMasterRequestsEditPage;
        Test.setCurrentPage(pageRef);
        
        system.assertEquals(acc.Name,acc.Name ,'HURRY ITS WRONG OUTPUT');
        system.assertNotEquals('CMR',acc.Name ,'HURRY ITS WRONG OUTPUT');
        system.assert(acc.Name==acc.Name ,'HURRY ITS WRONG OUTPUT');
        
        Test.stopTest();
    }
    
    static testMethod void Radio4()
    {
        Test.startTest(); 
        
        JJ_JPN_CustomerMasterRequest__c customerrequest =  new JJ_JPN_CustomerMasterRequest__c();
        customerrequest.Name = 'CMRAccout';
        customerrequest.JJ_JPN_AccountType__c = 'SAM/SAM Dealer';
        customerrequest.JJ_JPN_PayerCode__c='47583';
        customerrequest.JJ_JPN_PayerNameKanji__c='漢字';
        customerrequest.JJ_JPN_PayerNameKana__c='ｶﾝｼﾞ';
        customerrequest.JJ_JPN_PayerPostalCode__c='4758312';
        customerrequest.JJ_JPN_RegistrationFor__c='435353';
        customerrequest.JJ_JPN_OfficeName__c='sdcevfe';
        customerrequest.JJ_JPN_PersonInchargeName__c='sfdcvcfd';
        customerrequest.JJ_JPN_SalesItem__c='R';
        customerrequest.JJ_JPN_Rank__c='A2';
        customerrequest.JJ_JPN_CreditPersonInchargeName__c='ksjnvf';
        customerrequest.JJ_JPN_PersonInchargeCode__c='93284';
        customerrequest.JJ_JPN_StatusCode__c='H';
        customerrequest.JJ_JPN_RepresentativeNameKana__c='scdscds';
        customerrequest.JJ_JPN_RepresentativeNameKanji__c='bvsf';
        customerrequest.JJ_JPN_PaymentCondition__c='ZJ47';
        customerrequest.JJ_JPN_PaymentMethod__c='D';
        customerrequest.JJ_JPN_ReturnFAXNo__c='9483579';
        insert customerrequest;
        
        system.debug('$$$NameTest==>'+customerrequest.JJ_JPN_PayerNameKanji__c);
        system.debug('$$$Name1Test==>'+customerrequest.Name);
        
        Account acc = new Account();
        acc.Name = 'AccName';
        acc.JJ_JPN_CustomerNameKana__c = 'Kana';
        insert acc;
        
        AutoNumberCMR__c autoNum = new AutoNumberCMR__c();
        autoNum.Name = 'Account Outlet';
        autoNum.JJ_JPN_AutoNumberOutlet__c = 10001;
        insert autoNum;
        

        // creating the ID for the loading class  and intializes id to class , for the beginnning of the page
        Apexpages.currentPage().getParameters().put('accType',customerrequest.JJ_JPN_AccountType__c);
        Apexpages.currentPage().getParameters().put('Radio','Radio4');
        Apexpages.currentPage().getParameters().put('payerAcc',acc.id);
        Apexpages.currentPage().getParameters().put('billToAcc',acc.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(customerrequest);
        JJ_JPN_CustomerMasterRequestsEditPageExt CMRCls = new JJ_JPN_CustomerMasterRequestsEditPageExt(sc);
        CMRCls.Radio='Radio4';
        CMRCls.autoPopulateAccountValues();
        CMRCls.randomNumberGenerator(); 
        CMRCls.doSave();
        CMRCls.Cancel();
        
      
        PageReference pageRef = Page.CustomerMasterRequestsEditPage;
        Test.setCurrentPage(pageRef);
        
        system.assertEquals(acc.Name,acc.Name ,'HURRY ITS WRONG OUTPUT');
        system.assertNotEquals('CMR',acc.Name ,'HURRY ITS WRONG OUTPUT');
        system.assert(acc.Name==acc.Name ,'HURRY ITS WRONG OUTPUT');
        
        Test.stopTest();
    }
    
    static testMethod void Radio5()
    {
        Test.startTest(); 
        
        JJ_JPN_CustomerMasterRequest__c customerrequest =  new JJ_JPN_CustomerMasterRequest__c();
        customerrequest.Name = 'CMRAccout';
        customerrequest.JJ_JPN_AccountType__c = 'SAM/SAM Dealer';
        customerrequest.JJ_JPN_PayerCode__c='47583';
        customerrequest.JJ_JPN_PayerNameKanji__c='漢字';
        customerrequest.JJ_JPN_PayerNameKana__c='ｶﾝｼﾞ';
        customerrequest.JJ_JPN_PayerPostalCode__c='4758312';
        customerrequest.JJ_JPN_RegistrationFor__c='435353';
        customerrequest.JJ_JPN_OfficeName__c='sdcevfe';
        customerrequest.JJ_JPN_PersonInchargeName__c='sfdcvcfd';
        customerrequest.JJ_JPN_SalesItem__c='R';
        customerrequest.JJ_JPN_Rank__c='A2';
        customerrequest.JJ_JPN_CreditPersonInchargeName__c='ksjnvf';
        customerrequest.JJ_JPN_PersonInchargeCode__c='93284';
        customerrequest.JJ_JPN_StatusCode__c='H';
        customerrequest.JJ_JPN_RepresentativeNameKana__c='scdscds';
        customerrequest.JJ_JPN_RepresentativeNameKanji__c='bvsf';
        customerrequest.JJ_JPN_PaymentCondition__c='ZJ47';
        customerrequest.JJ_JPN_PaymentMethod__c='D';
        customerrequest.JJ_JPN_ReturnFAXNo__c='9483579';
        insert customerrequest;
        
        system.debug('$$$NameTest==>'+customerrequest.JJ_JPN_PayerNameKanji__c);
        system.debug('$$$Name1Test==>'+customerrequest.Name);
        
        Account acc = new Account();
        acc.Name = 'AccName';
        acc.JJ_JPN_CustomerNameKana__c = 'Kana';
        insert acc;
        
        AutoNumberCMR__c autoNum = new AutoNumberCMR__c();
        autoNum.Name = 'Account Outlet';
        autoNum.JJ_JPN_AutoNumberOutlet__c = 10001;
        insert autoNum;

        // creating the ID for the loading class  and intializes id to class , for the beginnning of the page
        Apexpages.currentPage().getParameters().put('accType',customerrequest.JJ_JPN_AccountType__c);
        Apexpages.currentPage().getParameters().put('Radio','Radio5');
        Apexpages.currentPage().getParameters().put('payerAcc',acc.id);
        Apexpages.currentPage().getParameters().put('billToAcc',acc.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(customerrequest);
        JJ_JPN_CustomerMasterRequestsEditPageExt CMRCls = new JJ_JPN_CustomerMasterRequestsEditPageExt(sc);
        CMRCls.Radio='Radio5';
        CMRCls.autoPopulateAccountValues();
        CMRCls.randomNumberGenerator(); 
        CMRCls.doSave();
        CMRCls.Cancel();
        
      
        PageReference pageRef = Page.CustomerMasterRequestsEditPage;
        Test.setCurrentPage(pageRef);
        
        system.assertEquals(acc.Name,acc.Name ,'HURRY ITS WRONG OUTPUT');
        system.assertNotEquals('CMR',acc.Name ,'HURRY ITS WRONG OUTPUT');
        system.assert(acc.Name==acc.Name ,'HURRY ITS WRONG OUTPUT');
        
        Test.stopTest();
    }
    
    static testMethod void Radio6()
    {
        Test.startTest(); 
        
        JJ_JPN_CustomerMasterRequest__c customerrequest =  new JJ_JPN_CustomerMasterRequest__c();
        customerrequest.Name = 'CMRAccout';
        customerrequest.JJ_JPN_AccountType__c = 'SAM/SAM Dealer';
        customerrequest.JJ_JPN_PayerCode__c='47583';
        customerrequest.JJ_JPN_PayerNameKanji__c='漢字';
        customerrequest.JJ_JPN_PayerNameKana__c='ｶﾝｼﾞ';
        customerrequest.JJ_JPN_PayerPostalCode__c='4758312';
        customerrequest.JJ_JPN_RegistrationFor__c='435353';
        customerrequest.JJ_JPN_OfficeName__c='sdcevfe';
        customerrequest.JJ_JPN_PersonInchargeName__c='sfdcvcfd';
        customerrequest.JJ_JPN_SalesItem__c='R';
        customerrequest.JJ_JPN_Rank__c='A2';
        customerrequest.JJ_JPN_CreditPersonInchargeName__c='ksjnvf';
        customerrequest.JJ_JPN_PersonInchargeCode__c='93284';
        customerrequest.JJ_JPN_StatusCode__c='H';
        customerrequest.JJ_JPN_RepresentativeNameKana__c='scdscds';
        customerrequest.JJ_JPN_RepresentativeNameKanji__c='bvsf';
        customerrequest.JJ_JPN_PaymentCondition__c='ZJ47';
        customerrequest.JJ_JPN_PaymentMethod__c='D';
        customerrequest.JJ_JPN_ReturnFAXNo__c='9483579';
        insert customerrequest;
        
        system.debug('$$$NameTest==>'+customerrequest.JJ_JPN_PayerNameKanji__c);
        system.debug('$$$Name1Test==>'+customerrequest.Name);
        
        Account acc = new Account();
        acc.Name = 'AccName';
        acc.JJ_JPN_CustomerNameKana__c = 'Kana';
        insert acc;
        
        AutoNumberCMR__c autoNum = new AutoNumberCMR__c();
        autoNum.Name = 'Account Outlet';
        autoNum.JJ_JPN_AutoNumberOutlet__c = 10001;
        insert autoNum;
        

        // creating the ID for the loading class  and intializes id to class , for the beginnning of the page
        Apexpages.currentPage().getParameters().put('accType',customerrequest.JJ_JPN_AccountType__c);
        Apexpages.currentPage().getParameters().put('Radio','Radio6');
        Apexpages.currentPage().getParameters().put('payerAcc',acc.id);
        Apexpages.currentPage().getParameters().put('billToAcc',acc.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(customerrequest);
        JJ_JPN_CustomerMasterRequestsEditPageExt CMRCls = new JJ_JPN_CustomerMasterRequestsEditPageExt(sc);
        CMRCls.Radio='Radio6';
        CMRCls.autoPopulateAccountValues();
        CMRCls.randomNumberGenerator(); 
        CMRCls.doSave();
        CMRCls.Cancel();
        
      
        PageReference pageRef = Page.CustomerMasterRequestsEditPage;
        Test.setCurrentPage(pageRef);
        
        system.assertEquals(acc.Name,acc.Name ,'HURRY ITS WRONG OUTPUT');
        system.assertNotEquals('CMR',acc.Name ,'HURRY ITS WRONG OUTPUT');
        system.assert(acc.Name==acc.Name ,'HURRY ITS WRONG OUTPUT');
        
        Test.stopTest();
    }
    
    static testMethod void Radio7()
    {
        Test.startTest(); 
        
        JJ_JPN_CustomerMasterRequest__c customerrequest =  new JJ_JPN_CustomerMasterRequest__c();
        customerrequest.Name = 'CMRAccout';
        customerrequest.JJ_JPN_AccountType__c = 'SAM/SAM Dealer';
        customerrequest.JJ_JPN_PayerCode__c='47583';
        customerrequest.JJ_JPN_PayerNameKanji__c='漢字';
        customerrequest.JJ_JPN_PayerNameKana__c='ｶﾝｼﾞ';
        customerrequest.JJ_JPN_PayerPostalCode__c='4758312';
        customerrequest.JJ_JPN_RegistrationFor__c='435353';
        customerrequest.JJ_JPN_OfficeName__c='sdcevfe';
        customerrequest.JJ_JPN_PersonInchargeName__c='sfdcvcfd';
        customerrequest.JJ_JPN_SalesItem__c='R';
        customerrequest.JJ_JPN_Rank__c='A2';
        customerrequest.JJ_JPN_CreditPersonInchargeName__c='ksjnvf';
        customerrequest.JJ_JPN_PersonInchargeCode__c='93284';
        customerrequest.JJ_JPN_StatusCode__c='H';
        customerrequest.JJ_JPN_RepresentativeNameKana__c='scdscds';
        customerrequest.JJ_JPN_RepresentativeNameKanji__c='bvsf';
        customerrequest.JJ_JPN_PaymentCondition__c='ZJ47';
        customerrequest.JJ_JPN_PaymentMethod__c='D';
        customerrequest.JJ_JPN_ReturnFAXNo__c='9483579';
        insert customerrequest;
        
        system.debug('$$$NameTest==>'+customerrequest.JJ_JPN_PayerNameKanji__c);
        system.debug('$$$Name1Test==>'+customerrequest.Name);
        
        Account acc = new Account();
        acc.Name = 'AccName';
        acc.JJ_JPN_CustomerNameKana__c = 'Kana';
        insert acc;
        
        AutoNumberCMR__c autoNum = new AutoNumberCMR__c();
        autoNum.Name = 'Account Outlet';
        autoNum.JJ_JPN_AutoNumberOutlet__c = 10001;
        insert autoNum;
        

        // creating the ID for the loading class  and intializes id to class , for the beginnning of the page
        Apexpages.currentPage().getParameters().put('accType',customerrequest.JJ_JPN_AccountType__c);
        Apexpages.currentPage().getParameters().put('Radio','Radio7');
        Apexpages.currentPage().getParameters().put('payerAcc',acc.id);
        Apexpages.currentPage().getParameters().put('billToAcc',acc.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(customerrequest);
        JJ_JPN_CustomerMasterRequestsEditPageExt CMRCls = new JJ_JPN_CustomerMasterRequestsEditPageExt(sc);
        CMRCls.Radio='Radio7';
        CMRCls.autoPopulateAccountValues();
        CMRCls.randomNumberGenerator(); 
        CMRCls.doSave();
        CMRCls.Cancel();
        
      
        PageReference pageRef = Page.CustomerMasterRequestsEditPage;
        Test.setCurrentPage(pageRef);
        
        system.assertEquals(acc.Name,acc.Name ,'HURRY ITS WRONG OUTPUT');
        system.assertNotEquals('CMR',acc.Name ,'HURRY ITS WRONG OUTPUT');
        system.assert(acc.Name==acc.Name ,'HURRY ITS WRONG OUTPUT');
        
        Test.stopTest();
    }
}