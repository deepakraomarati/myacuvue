@isTest
public class JJ_JPN_ContactCreationOnAcc_handlerTest{
    static TestMethod void contactHandler(){
        JJ_JPN_CustomerMasterRequest__c CMR = new JJ_JPN_CustomerMasterRequest__c (name='Test Data',JJ_JPN_PayerCode__c='12345',
                                                                                   JJ_JPN_BillToCode__c = '12346');
        insert cmr;
        Account acc = new Account(name='test data',accountNumber='12345',OutletNumber__c = '12345');
        insert acc;
        JJ_JPN_ContactCreationOnAccount_handler.accountupdate();
        system.assertEquals(acc.accountNumber,'12345','success');
    }
}