Public class JJ_JPN_ContactCreationOnAccount_handler
    {
        Public static void accountupdate()
        {   
            List<Account> accountnew;
            set<String> AccountNumberset=new set<String>();
            if(!Test.isRunningTest()){
                 accountnew = (list<Account>)trigger.new;
            }
            
           // List<Account> updates= new List(Account>();
           
           
           if(Test.isRunningTest()){
               JJ_JPN_CustomerMasterRequest__c crmRec = [select Id,JJ_JPN_PayerCode__c from JJ_JPN_CustomerMasterRequest__c
                                                                   where JJ_JPN_PayerCode__c != null limit 1];
               accountnew = new List<Account>();                                                    
               accountnew.addAll([select Id,OutletNumber__c from Account where OutletNumber__c =: crmRec.JJ_JPN_PayerCode__c ]);
           }
                    
            for (Account acts: accountnew)
            {   
                if(acts.OutletNumber__c!=null) 
                    AccountNumberset.add(acts.OutletNumber__c);                
            }
            List<JJ_JPN_CustomerMasterRequest__c> crm= new List<JJ_JPN_CustomerMasterRequest__c>();
            If(AccountNumberset!=null){
                crm= [select id,JJ_JPN_RepresentativeNameFranchise__c,JJ_JPN_BusinessLicenceOwnerName__c,JJ_JPN_OfficeName__c,JJ_JPN_Rank__c,JJ_JPN_AccountType__c,JJ_JPN_PayerCode__c,JJ_JPN_SoldToCode__c,JJ_JPN_BillToCode__c,JJ_JPN_BillingCloseDate__c,JJ_JPN_BillingSummaryTableSubmission__c,JJ_JPN_CLIssue__c,JJ_JPN_ContractDate__c,JJ_JPN_ContractType__c,JJ_JPN_CP1DOMonitorCampaign__c,
                JJ_JPN_CPNewDesignLaunchCampaign__c,JJ_JPN_CPWtAcampaign__c,JJ_JPN_DeadlineTime__c,JJ_JPN_DeliveryDocumentNote__c,JJ_JPN_EyecarePartner__c,JJ_JPN_FacilityInformation__c,JJ_JPN_FacilityInformationTelephone__c,
                JJ_JPN_FieldSalesDistrictID__c,JJ_JPN_FocusCategory__c,JJ_JPN_FT1DDefineMoist__c,JJ_JPN_FT1DDefineMoistRadiant__c,JJ_JPN_FT1DMoistMultifocal__c,JJ_JPN_FT1DOasys__c,JJ_JPN_FT1MAstigmatism__c,JJ_JPN_FT2WDefine__c,JJ_JPN_FTAdvance__c,JJ_JPN_FTMoist__c,JJ_JPN_FTOAastigmatism__c,
                JJ_JPN_FTOasys__c,JJ_JPN_FTTruEye__c,JJ_JPN_GroupName__c,JJ_JPN_InformationRemarks__c,JJ_JPN_Information1DDefineMoist__c,JJ_JPN_Information1DDefineMoist10Pack__c,JJ_JPN_Information1DDefineMoistRadiant__c,JJ_JPN_Information1DMoistMultifocal__c,
                JJ_JPN_Information1DOasys__c,JJ_JPN_Information1DOasys90P__c,JJ_JPN_Information1DAV__c,JJ_JPN_Information1DAV90Pack__c,JJ_JPN_Information1MAstigmatism__c,
                JJ_JPN_Information2WDefine__c,JJ_JPN_Information2WAV__c,JJ_JPN_InformationAcuvue__c,JJ_JPN_InformationAdvance__c,JJ_JPN_InformationMoist__c,JJ_JPN_InformationMoist90Pack__c,
                JJ_JPN_InformationOAAstigmatism__c,JJ_JPN_InformationOasys__c,JJ_JPN_InformationTruEye__c,JJ_JPN_InformationTruEye90Pack__c,JJ_JPN_InterviewSpecifiedTime__c,JJ_JPN_IntroductionPriority__c,JJ_JPN_KeyPerson__c,JJ_JPN_LicenseNumber__c,JJ_JPN_ManagementPolicy__c,JJ_JPN_OrderBlock__c,
                JJ_JPN_Parking__c,JJ_JPN_PartnershopFlag__c,JJ_JPN_PicturesPublishedFlag__c,JJ_JPN_ProductPrice__c,JJ_JPN_RelationshipwithDoctor__c,JJ_JPN_RepresentativeNameKana__c,JJ_JPN_RepresentativeNameKanji__c,
                JJ_JPN_ReturnedGoodMethod__c,JJ_JPN_CompanyName__c,JJ_JPN_ThoughtOnNetBusiness__c,JJ_JPN_TypeOfPublishedAds__c,JJ_JPN_POSDevision__c,JJ_JPN_Validity__c,JJ_JPN_VisitBan__c, JJ_JPN_Website__c,JJ_JPN_ReturnedGoodsOrderNoInDigits__c,JJ_JPN_PowerRelationShipWithCompetings__c,JJ_JPN_ORTQualifiedPerson__c,JJ_JPN_OutboundNG__c,JJ_JPN_NearestStation__c,
                JJ_JPN_Email__c,JJ_JPN_CP1DO90Refund__c,JJ_JPN_ReservedCampaignFlag1__c,JJ_JPN_MasterRegistrationDate__c,JJ_JPN_Person_Incharge_NameSAP__c from JJ_JPN_CustomerMasterRequest__c where ((JJ_JPN_PayerCode__c IN :AccountNumberset) or (JJ_JPN_BillToCode__c IN :AccountNumberset) or (JJ_JPN_SoldToCode__c IN :AccountNumberset))];
                system.debug('--------AccountNumberset----'+AccountNumberset);
            }
           If(crm!=null){ 
               for (Account acts: accountnew)
               {
               for (JJ_JPN_CustomerMasterRequest__c cmrid : crm)
               {
                  System.Debug('acts.OutletNumber__c---'+acts.OutletNumber__c);
                   System.Debug('cmrid.JJ_JPN_PayerCode__c---'+cmrid.JJ_JPN_PayerCode__c);
                 if ((acts.OutletNumber__c==cmrid.JJ_JPN_PayerCode__c )|| (acts.OutletNumber__c==cmrid.JJ_JPN_BillToCode__c) || (acts.OutletNumber__c==cmrid.JJ_JPN_SoldToCode__c ))
                 {
                     acts.JJ_JPN_OfficeName__c=cmrid.JJ_JPN_OfficeName__c ;
                     system.debug('--------JJ_JPN_OfficeName__c----'+cmrid.JJ_JPN_OfficeName__c);
                     system.debug('--------AccountNumberset----'+cmrid.JJ_JPN_BillingCloseDate__c);
              //     acts.JJ_JPN_BillingCloseDate__c = cmrid.JJ_JPN_BillingCloseDate__c;
              //     acts.JJ_JPN_BillingSummaryTableSubmission__c = cmrid.JJ_JPN_BillingSummaryTableSubmission__c;
                     acts.JJ_JPN_BusinessLicenceOwnerName__c=cmrid.JJ_JPN_BusinessLicenceOwnerName__c;
                     acts.JJ_JPN_CompanyName__c=cmrid.JJ_JPN_CompanyName__c;
             //      acts.JJ_JPN_Rank__c= cmrid.JJ_JPN_Rank__c;
                     acts.JJ_JPN_CustomerType__c=cmrid.JJ_JPN_AccountType__c;
                     acts.JJ_JPN_CLIssue__c = cmrid.JJ_JPN_CLIssue__c;
                     acts.JJ_JPN_ContractDate__c = cmrid.JJ_JPN_ContractDate__c;
                     acts.JJ_JPN_ContractType__c = cmrid.JJ_JPN_ContractType__c;
                     acts.JJ_JPN_CP1DO90RefundSecurityGuarranty__c = cmrid.JJ_JPN_CP1DO90Refund__c;
                     acts.JJ_JPN_CP1DOMonitorCampaign__c = cmrid.JJ_JPN_CP1DOMonitorCampaign__c;
                     acts.JJ_JPN_CPNewDesignLaunchCampaign__c = cmrid.JJ_JPN_CPNewDesignLaunchCampaign__c;
                     acts.JJ_JPN_CPWtAcampaign__c = cmrid.JJ_JPN_CPWtAcampaign__c;
            //       acts.JJ_JPN_DeadlineTime__c = cmrid.JJ_JPN_DeadlineTime__c;
                     acts.JJ_JPN_DeliveryDocumentNote__c = cmrid.JJ_JPN_DeliveryDocumentNote__c;
                     acts.JJ_JPN_Email__c = cmrid.JJ_JPN_Email__c;
                     acts.JJ_JPN_EyecarePartner__c = cmrid.JJ_JPN_EyecarePartner__c;
                     acts.JJ_JPN_FacilityInformation__c = cmrid.JJ_JPN_FacilityInformation__c;
                     acts.JJ_JPN_FacilityInformationTelephone__c = cmrid.JJ_JPN_FacilityInformationTelephone__c;
                     acts.JJ_JPN_FieldSalesDistrictID__c = cmrid.JJ_JPN_FieldSalesDistrictID__c;
                     acts.JJ_JPN_FocusCategory__c = cmrid.JJ_JPN_FocusCategory__c;
                     acts.JJ_JPN_FT1DDefineMoist__c = cmrid.JJ_JPN_FT1DDefineMoist__c;
                     acts.JJ_JPN_FT1DDefineMoistRadiant__c = cmrid.JJ_JPN_FT1DDefineMoistRadiant__c;
                     acts.JJ_JPN_FT1DMoistMultifocal__c = cmrid.JJ_JPN_FT1DMoistMultifocal__c;
                     acts.JJ_JPN_FT1DOasys__c = cmrid.JJ_JPN_FT1DOasys__c;
                     acts.JJ_JPN_FT1MAstigmatism__c = cmrid.JJ_JPN_FT1MAstigmatism__c;
                     acts.JJ_JPN_FT2WDefine__c = cmrid.JJ_JPN_FT2WDefine__c;
                     acts.JJ_JPN_FTAdvance__c = cmrid.JJ_JPN_FTAdvance__c;
                     acts.JJ_JPN_FTMoist__c = cmrid.JJ_JPN_FTMoist__c;
                     acts.JJ_JPN_FTOAAstigmatism__c = cmrid.JJ_JPN_FTOAastigmatism__c;
                     acts.JJ_JPN_FTOasys__c = cmrid.JJ_JPN_FTOasys__c;
                     acts.JJ_JPN_FTTruEye__c = cmrid.JJ_JPN_FTTruEye__c;
                     acts.Nearest_Station__c = cmrid.JJ_JPN_NearestStation__c;
                     acts.JJ_JPN_OfficeName__c = cmrid.JJ_JPN_OfficeName__c;
                     acts.JJ_JPN_OrderBlock__c = cmrid.JJ_JPN_OrderBlock__c;
                     acts.ORT_Qualified_Person__c = cmrid.JJ_JPN_ORTQualifiedPerson__c;
                     acts.JJ_JPN_OutboundNG__c = cmrid.JJ_JPN_OutboundNG__c;
                     acts.JJ_JPN_Parking__c = cmrid.JJ_JPN_Parking__c;
                     acts.JJ_JPN_PartnershopFlag__c = cmrid.JJ_JPN_PartnershopFlag__c;
                     acts.JJ_JPN_PicturesPublishedFlag__c = cmrid.JJ_JPN_PicturesPublishedFlag__c;
                     acts.JJ_JPN_POSDevision__c = cmrid.JJ_JPN_POSDevision__c;
                     acts.JJ_JPN_PowerRelationshipwithCompeting__c = cmrid.JJ_JPN_PowerRelationShipWithCompetings__c;
             //      acts.JJ_JPN_ProductPrice__c = cmrid.JJ_JPN_ProductPrice__c;
                     acts.JJ_JPN_RelationshipWithDoctor__c = cmrid.JJ_JPN_RelationshipwithDoctor__c;
                     acts.JJ_JPN_RepresentativeNameKana__c = cmrid.JJ_JPN_RepresentativeNameKana__c;
                     acts.JJ_JPN_RepresentativeNameKanji__c = cmrid.JJ_JPN_RepresentativeNameKanji__c;
             //      acts.JJ_JPN_ReturnedGoodMethod__c = cmrid.JJ_JPN_ReturnedGoodMethod__c;
                     acts.JJ_JPN_DSO__c = cmrid.JJ_JPN_ReturnedGoodsOrderNoInDigits__c;
                     acts.JJ_JPN_ThoughtOnNetBusiness__c = cmrid.JJ_JPN_ThoughtOnNetBusiness__c;
                     acts.JJ_JPN_TypeOfPublishedAds__c = cmrid.JJ_JPN_TypeOfPublishedAds__c;
                     acts.JJ_JPN_Validity__c = cmrid.JJ_JPN_Validity__c;
                     acts.JJ_JPN_VisitBan__c = cmrid.JJ_JPN_VisitBan__c;
                     acts.JJ_JPN_Website__c = cmrid.JJ_JPN_Website__c;
                     acts.JJ_JPN_GroupName__c=cmrid.JJ_JPN_GroupName__c;
                     acts.JJ_JPN_InformationRemarks__c=cmrid.JJ_JPN_InformationRemarks__c;
                     acts.JJ_JPN_Information1DDefineMoist__c=cmrid.JJ_JPN_Information1DDefineMoist__c;
                     acts.JJ_JPN_Information1DDefineMoist10Pack__c=cmrid.JJ_JPN_Information1DDefineMoist10Pack__c;
                     acts.JJ_JPN_Information1DDefineMoistRadiant__c=cmrid.JJ_JPN_Information1DDefineMoistRadiant__c;
                     acts.JJ_JPN_Information1DMoistMultifocal__c=cmrid.JJ_JPN_Information1DMoistMultifocal__c;
                     acts.JJ_JPN_Information1DOasys__c=cmrid.JJ_JPN_Information1DOasys__c;
                     acts.JJ_JPN_Information1DOasys90P__c=cmrid.JJ_JPN_Information1DOasys90P__c;
                     acts.JJ_JPN_Information1DAV__c=cmrid.JJ_JPN_Information1DAV__c;
                     acts.JJ_JPN_Information1DAV90Pack__c=cmrid.JJ_JPN_Information1DAV90Pack__c;
                     acts.JJ_JPN_Information1MAstigmatism__c=cmrid.JJ_JPN_Information1MAstigmatism__c;
                     acts.JJ_JPN_Information2WDefine__c=cmrid.JJ_JPN_Information2WDefine__c;
                     acts.JJ_JPN_Information2WAV__c=cmrid.JJ_JPN_Information2WAV__c;
                     acts.JJ_JPN_InformationAcuvue__c=cmrid.JJ_JPN_InformationAcuvue__c;
                     acts.JJ_JPN_InformationAdvance__c=cmrid.JJ_JPN_InformationAdvance__c;
                     acts.JJ_JPN_InformationMoist__c=cmrid.JJ_JPN_InformationMoist__c;
                     acts.JJ_JPN_InformationMoist90Pack__c=cmrid.JJ_JPN_InformationMoist90Pack__c;
                     acts.JJ_JPN_InformationOAAstigmatism__c=cmrid.JJ_JPN_InformationOAAstigmatism__c;
                     acts.JJ_JPN_InformationOasys__c=cmrid.JJ_JPN_InformationOasys__c;
                     acts.JJ_JPN_InformationTruEye__c=cmrid.JJ_JPN_InformationTruEye__c;
                     acts.JJ_JPN_InformationTruEye90Pack__c=cmrid.JJ_JPN_InformationTruEye90Pack__c;
                     acts.JJ_JPN_InterviewSpecifiedTime__c=cmrid.JJ_JPN_InterviewSpecifiedTime__c;
                     acts.IntroductionPriority__c=cmrid.JJ_JPN_IntroductionPriority__c;
                     acts.JJ_JPN_KeyPerson__c=cmrid.JJ_JPN_KeyPerson__c;
                     acts.JJ_JPN_LicenseNumber__c=cmrid.JJ_JPN_LicenseNumber__c;
                     acts.JJ_JPN_ManagementPolicy__c=cmrid.JJ_JPN_ManagementPolicy__c;
                     acts.Last_update_date__c=cmrid.JJ_JPN_MasterRegistrationDate__c;
                     acts.JJ_JPN_ReservedCampaignFlag1__c=cmrid.JJ_JPN_ReservedCampaignFlag1__c;   
                     acts.JJ_JPN_PersonInchargeName__c = cmrid.JJ_JPN_Person_Incharge_NameSAP__c;
                     acts.JJ_JPN_RepresentativeNameFranchise__c=cmrid.JJ_JPN_RepresentativeNameFranchise__c ;
                                  
    
                }
                }
                } //insert accountnew;
          }
           
    }
   }