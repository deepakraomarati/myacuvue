@isTest(seeAllData=false)
private class JJ_JPN_ContactCreationOnAccounttest{
    
    public static List<Account> AccountsList;
    public static List<JJ_JPN_CustomerMasterRequest__c > CMRList;
    public static List<JJ_JPN_ECPContact__c> ECPList;
    public static List<contact> Contactlist;
    
    public static testMethod void ContactCreationOnAccount(){
    TriggerHandler__c triggerHandler = new TriggerHandler__c(Name = 'HandleTriggers' ,
                                                             JJ_JPN_ContactCreationOnAccount__c = false);
    insert triggerHandler;
    
    Test.startTest();
    
   JJ_JPN_CustomerMasterRequest__c CMR = new JJ_JPN_CustomerMasterRequest__c (name='Test Data',JJ_JPN_PayerCode__c='12345',
                                                                                JJ_JPN_BillToCode__c = '12346');
    insert cmr;
    Account acc = new Account( name='test data',accountNumber='12345',OutletNumber__c = '12346');
    insert acc;
    
    AccountsList=[select id,accountNumber from Account where accountNumber=:acc.accountNumber ];
    
   
    
    
    CMRList=[select id,JJ_JPN_PayerCode__c from JJ_JPN_CustomerMasterRequest__c where JJ_JPN_PayerCode__c=:acc.accountNumber];
    
    system.assertEquals(acc.accountNumber,CMR.JJ_JPN_PayerCode__c);
    
    JJ_JPN_ECPContact__c ECP=new JJ_JPN_ECPContact__c (name='Test Data', ecp__c =cmr.id, JJ_JPN_Phone__c='123456789');
    insert ECP;
    
    ecplist=[select id,Name,JJ_JPN_ECPNameKana__c,JJ_JPN_University__c,JJ_JPN_Hobby__c,JJ_JPN_FamilyStructure__c,
        JJ_JPN_Birthplace__c,JJ_JPN_MedicalSocietyExecutives__c,JJ_JPN_CustomerID__c,JJ_JPN_Fax__c,JJ_JPN_JobCategory__c, JJ_JPN_Department__c, 
        JJ_JPN_Phone__c ,JJ_JPN_Street__c,JJ_JPN_Email__c, JJ_JPN_PostalCode__c , JJ_JPN_Prefectures__c, JJ_JPN_TownshipsCity__c, 
        JJ_JPN_OtherAddress__c, JJ_JPN_AccountName__c,JJ_JPN_ReportsTo__c,JJ_JPN_AffiliationSociety__c,JJ_JPN_CLHistoryOfUseDrinkingSmoking__c,JJ_JPN_S4Type__c,
        JJ_JPN_InterestInCL__c,JJ_JPN_InterestInAOS__c,JJ_JPN_OperationsCarriedOut__c,JJ_JPN_Specialist__c,JJ_JPN_KOL__c,JJ_JPN_Birthday__c,ecp__r.JJ_JPN_PayerCode__c
        from JJ_JPN_ECPContact__c where id=:ECP.id];
    
   // list<Contact> con = new list<Contact>{new contact (LastName= ecp.Name,RecordTypeId='012O00000001C5KIAU',JJ_JPN_NameKana__c=ecp.JJ_JPN_ECPNameKana__c)};
   /*  RecordType recType=new Recordtype(developername='JJ_JPN_ECPJapan',SobjectType ='Contact',Name='ECP Japan');
     insert recType;*/
     Id recordTypeId=[SELECT Id,developerName,Name,SobjectType  FROM RecordType where SobjectType ='Contact' and developerName='JJ_JPN_ECPJapan'].Id;
     system.debug('----recordtypeid----'+recordtypeid);
     for(Account acnt: AccountsList){
        for(JJ_JPN_ECPContact__c ecpl : ecplist){
        //contact conadd= new contact (LastName= 'Test',AccountId=acnt.id);  
    if(ecpl.ecp__r.JJ_JPN_PayerCode__c==acnt.accountNumber){
        //     contact conadd= new contact (LastName= 'Test');
               contact conadd= new contact();
               conadd.AccountId=acnt.id;               
               conadd.LastName= ecpl.Name;
               conadd.RecordTypeId=recordTypeId;
               conadd.JJ_JPN_NameKana__c=ecpl.JJ_JPN_ECPNameKana__c;
               conadd.MailingPostalCode=ecpl.JJ_JPN_PostalCode__c;
               conadd.JJ_JPN_Prefectures__c=ecpl.JJ_JPN_Prefectures__c;
               conadd.MailingCity= ecpl.JJ_JPN_TownshipsCity__c;
               conadd.MailingStreet= ecpl.JJ_JPN_Street__c;
               conadd.JJ_JPN_OtherAddress__c= ecpl.JJ_JPN_OtherAddress__c;
               conadd.JJ_JPN_CustomerID__c=ecpl.JJ_JPN_CustomerID__c;
               conadd.Department= ecpl.JJ_JPN_Department__c;
               conadd.Occupation__c= ecpl.JJ_JPN_JobCategory__c;
               conadd.ReportsToId = ecpl.JJ_JPN_ReportsTo__c;
               conadd.Phone= ecpl.JJ_JPN_Phone__c;
               conadd.Fax=ecpl.JJ_JPN_Fax__c;
               conadd.JJ_JPN_University__c=ecpl.JJ_JPN_University__c;
               conadd.JJ_JPN_MedicalSocietyExecutives__c=ecpl.JJ_JPN_MedicalSocietyExecutives__c;    
               conadd.JJ_JPN_AffiliationSociety__c=ecpl.JJ_JPN_AffiliationSociety__c;
               conadd.JJ_JPN_Birthplace__c=ecpl.JJ_JPN_Birthplace__c;
               conadd.JJ_JPN_FamilyStructure__c=ecpl.JJ_JPN_FamilyStructure__c;
               conadd.JJ_JPN_Hobby__c=ecpl.JJ_JPN_Hobby__c;
               conadd.Birthdate=ecpl.JJ_JPN_Birthday__c;
               conadd.Email= ecpl.JJ_JPN_Email__c;
               conadd.JJ_JPN_CLHistoryOfUseDrinkingSmoking__c=ecpl.JJ_JPN_CLHistoryOfUseDrinkingSmoking__c;
               conadd.JJ_JPN_S4Type__c=ecpl.JJ_JPN_S4Type__c;
               conadd.JJ_JPN_InterestInCL__c=ecpl.JJ_JPN_InterestInCL__c;
               conadd.JJ_JPN_InterestInAOS__c=ecpl.JJ_JPN_InterestInAOS__c;
               conadd.JJ_JPN_OperationsCarriedOut__c=ecpl.JJ_JPN_OperationsCarriedOut__c;
               conadd.JJ_JPN_Specialist__c=ecpl.JJ_JPN_Specialist__c;
               conadd.JJ_JPN_KOL__c=ecpl.JJ_JPN_KOL__c;            
               insert conadd;  
    }
    
    }}
    //Contact con=new contact (con.LastName= ecp.Name);
    //insert Contactlist;
    //system.assertEquals(Contactlist[0].LastName,ecp.name);
    //system.assertequals(Contactlist[0].AccountId,acc.id);
    //system.assertequals(Contactlist[0].JJ_JPN_NameKana__c,ecp.JJ_JPN_ECPNameKana__c);
    Test.stopTest();
    }
    
    Public static testMethod void ContactCreationOnAccountnull(){
    
    Test.startTest();
    list<Contact> con= new list<Contact>{new contact (LastName= 'Test Data')};
    
    Insert con;
    
    Contactlist=[select id,AccountId from Contact where id=:con[0].id];
    
    system.assertequals(con[0].Id,Contactlist[0].id);
    
    Test.stopTest();
    
    }


}