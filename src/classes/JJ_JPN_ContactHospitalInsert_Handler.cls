Public class JJ_JPN_ContactHospitalInsert_Handler {
    
    Public static void Insertingcallcontact (List<Contact> newcall){
   // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
    Id jjvproRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP Contacts').getRecordTypeId();
        List<JJ_JPN_HospitaltoDoctors__c> contact1list = New List<JJ_JPN_HospitaltoDoctors__c> ();
        for(Contact call : newcall){
           // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
            if(call.RecordTypeId != jjvproRecordTypeId)
            {
            JJ_JPN_HospitaltoDoctors__c contact1 = New JJ_JPN_HospitaltoDoctors__c();
            contact1.JJ_JPN_Doctors__c = call.Id;
            contact1.JJ_JPN_Hospitals__c = call.JJ_JPN_Hospital__c;
            contact1.JJ_JPN_PrimaryHospital__c=True;
            contact1list.add(contact1);
            }
        }
        // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
        if(contact1list.size()>0)
        {
            Database.Insert(contact1list,false);
        }
    }
    
    Public static void updatecallcontact(List<Contact> newcall,Map<Id,Contact> oldcallmap,Map<Id,Contact> newcallmap){
    // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
    Id jjvproRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP Contacts').getRecordTypeId();
        Set<Id> callidset = New Set<Id> ();
        Set<Id> contact1set = New Set<Id> ();
        List<JJ_JPN_HospitaltoDoctors__c> updatecontact1list = New List<JJ_JPN_HospitaltoDoctors__c> ();
        for(Contact call : newcall){   
             // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
             if(call.RecordTypeId != jjvproRecordTypeId) {        
                if(call.JJ_JPN_Hospital__c != oldcallmap.get(call.Id).JJ_JPN_Hospital__c){
                    callidset.add(call.Id);
                    contact1set.add(oldcallmap.get(call.Id).JJ_JPN_Hospital__c);        
                }
            }
        }
        
        for(JJ_JPN_HospitaltoDoctors__c cllacontact : [SELECT Id,JJ_JPN_Doctors__c,JJ_JPN_Hospitals__c FROM JJ_JPN_HospitaltoDoctors__c WHERE JJ_JPN_Doctors__c IN: callidset AND JJ_JPN_Hospitals__c IN:contact1set]){            
            if(oldcallmap.get(cllacontact.JJ_JPN_Doctors__c).JJ_JPN_Hospital__c == cllacontact.JJ_JPN_Hospitals__c && oldcallmap.get(cllacontact.JJ_JPN_Doctors__c).Id== cllacontact.JJ_JPN_Doctors__c){
                cllacontact.JJ_JPN_Hospitals__c = newcallmap.get(cllacontact.JJ_JPN_Doctors__c).JJ_JPN_Hospital__c;
                cllacontact.JJ_JPN_PrimaryHospital__c=True;
                updatecontact1list.add(cllacontact);
            }
        }
        // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
        if(updatecontact1list.size()>0)
        {
            Database.Update(updatecontact1list,false);
        }
    }
    
}