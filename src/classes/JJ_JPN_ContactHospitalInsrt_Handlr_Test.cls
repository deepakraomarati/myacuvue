@isTest
public class JJ_JPN_ContactHospitalInsrt_Handlr_Test{	
    public Static TestMethod void InsertingcallcontactTest(){
        List<Contact> con = new List<Contact>();      
        JJ_JPN_Hospital__c hospital= new JJ_JPN_Hospital__c(Name='字字字');
        insert hospital;
        Id id=[select id from JJ_JPN_Hospital__c where Name='字字字' limit 1].id;
        Id recordtype= [select id from recordtype where DeveloperName='JJ_JPN_Doctor' limit 1].id;
        
        Contact cont= new Contact(LastName='ｼ',RecordTypeId=recordtype,JJ_JPN_Hospital__c=id);
        con.add(cont);
        insert con;
        system.assertEquals(cont.LastName,'ｼ','success');
        JJ_JPN_ContactHospitalInsert_Handler.Insertingcallcontact(con);      
    }
    
    public Static TestMethod void updatecallcontactTest(){
        JJ_JPN_Hospital__c hospital= new JJ_JPN_Hospital__c(Name='字字字');
        insert hospital;
        JJ_JPN_Hospital__c hospital2= new JJ_JPN_Hospital__c(Name='字字');
        insert hospital2;
        
        Id id=[select id from JJ_JPN_Hospital__c where Name='字字字' limit 1].id;
        Id id2=[select id from JJ_JPN_Hospital__c where Name='字字' limit 1].id;
        
        List<Contact> Oldcontact= new List<Contact>();
        List<Contact> Newcontacts= new List<Contact>();        
        Map<id,Contact> oldmap=new Map<id,Contact>();
        Map<id,Contact> newmap=new Map<id,Contact>();
        
        for(Integer i=0;i<5;i++)
        {
            Oldcontact.add(new Contact(LastName='字字字',JJ_JPN_Hospital__c=id,JJ_JPN_GraduationYear__c='201'+i));
        }
        insert Oldcontact;
        
        for (Contact con: Oldcontact)
        {
            oldmap.put(con.Id,con);
        }
        
        for (Contact newcontact: Oldcontact)
        {
            newcontact.JJ_JPN_GraduationYear__c='2017';
            newcontact.JJ_JPN_Hospital__c=id2;
            Newcontacts.add(newcontact);
        }
        Update Newcontacts;
        
        for (Contact cont: Newcontacts)
        {
            newmap.put(cont.Id,cont);
        }
        
        system.assertEquals(hospital.Name,'字字字','success');
        JJ_JPN_ContactHospitalInsert_Handler.updatecallcontact(Oldcontact,oldmap,newmap);        
    }
}