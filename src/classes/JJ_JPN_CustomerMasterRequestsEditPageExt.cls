/** 
* File Name: JJ_JPN_CustomerMasterRequestsEditPageExt
* Description : Business logic applied for Customer Master Regisrtion.
* Copyright : Johnson & Johnson
* @author : Shiva Shankar | sshank10@its.jnj.com | bnshiva.shankar@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |8-July-2016 |@sshank10@its.jnj.com |New Class created
*/ 

public class JJ_JPN_CustomerMasterRequestsEditPageExt {

    private final JJ_JPN_CustomerMasterRequest__c CMR;

    Public JJ_JPN_CustomerMasterRequest__c customerrequest{get; set;}
    public String radio;
    public String accType;
    public String samMgrID;
    public String payeAccID;
    public String billToAccID;
    
    public Boolean billToShow  {get;set;}
    public Boolean soldToShow  {get;set;}
    public Boolean payerToShow {get;set;}
    
    public String autoIncrement1;
    
    public List<Account> accList {get;set;}
    
    public JJ_JPN_CustomerMasterRequestsEditPageExt(ApexPages.StandardController controller) 
    {
       
        this.CMR= (JJ_JPN_CustomerMasterRequest__c)controller.getRecord();
        
         customerrequest = New JJ_JPN_CustomerMasterRequest__c();
         radio = ApexPages.currentPage().getParameters().get('Radio');
         accType = ApexPages.currentPage().getParameters().get('accType');
         samMgrID = ApexPages.currentPage().getParameters().get('samMgrID');
         payeAccID = ApexPages.currentPage().getParameters().get('payerAcc');
         billToAccID = ApexPages.currentPage().getParameters().get('billToAcc');
         
         system.debug('radio==>'+radio);
         system.debug('$$$accType==>'+accType);
      
         showAccountSubType();
         autoPopulateValues();
         if(radio == 'Radio5' || radio == 'Radio6' || radio == 'Radio7')
         {
             autoPopulateAccountValues();
         }
        
    }

       
    public void showAccountSubType()
    {
        if(radio == 'Radio1')
        {
            billToShow = false;
            soldToShow = false;
            payerToShow = true;
        }
        else if(radio == 'Radio2')
        {
            billToShow = false;
            payerToShow = true;
            soldToShow = true;
        }
        else if(radio == 'Radio3' || radio == 'Radio6')
        {
            payerToShow = true;
            billToShow = true;
            soldToShow = false;
        }
        else if(radio == 'Radio4' || radio == 'Radio5' || radio == 'Radio7')
        {
            payerToShow = true;
            billToShow = true;
            soldToShow = true;
        }
    }
    
    public void autoPopulateAccountValues()
    {
        Account accListPyrAcc; 
        Account accListBillTo;
         if(payeAccID!='null')
         {
             accListPyrAcc = [SELECT id , name,payment_terms__c,JJ_JPN_SubCustomerGroup__c,JJ_JPN_CustomerOrderNoNumberofDigits__c,JJ_JPN_PaymentMethod__c,JJ_JPN_DeliveryDocumentNote__c,JJ_JPN_BillToRTNFAXHowmanytimestosend__c,JJ_JPN_EDI_Flag__c,JJ_JPN_FAXOrder__c,JJ_JPN_PurchaseOrderNumberRequired__c,Call_Frequency_Cycle__c,JJ_JPN_DSO__c,JJ_JPN_DirectDeliveryReturned__c,JJ_JPN_TransactionDetailSubmission__c,JJ_JPN_ADSHandlingFlag__c,Sub_Customer_Group__c,JJ_JPN_FestivalDelivery__c,JJ_JPN_EDIStoreCode__c,JJ_JPN_BillSubmission__c,JJ_JPN_ItemizedBilling__c,JJ_JPN_CreditPersonInchargeName__c, AccountNumber, Phone, Fax, JJ_JPN_CustomerNameKana__c, ShippingPostalCode,Address2__c, Address3__c, ShippingCountry, JJ_JPN_OtherAddress__c,Address1__c,JJ_JPN_isPayerAcc__c  FROM Account where id=:payeAccID ];
             
             customerrequest.JJ_JPN_PayerCode__c = String.valueof(accListPyrAcc.AccountNumber);
             customerrequest.JJ_JPN_PayerNameKanji__c = accListPyrAcc.Name;
             customerrequest.JJ_JPN_PayerNameKana__c = accListPyrAcc.JJ_JPN_CustomerNameKana__c;
             customerrequest.JJ_JPN_PayerPostalCode__c = accListPyrAcc.ShippingPostalCode; 
             customerrequest.JJ_JPN_PayerState__c = accListPyrAcc.Address3__c; 
             customerrequest.JJ_JPN_PayerTownshipCity__c = accListPyrAcc.Address2__c;
             customerrequest.JJ_JPN_PayerStreet__c = accListPyrAcc.Address1__c;
            // customerrequest.JJ_JPN_PayerState__c= accListPyrAcc.ShippingState;
             customerrequest.JJ_JPN_PayerOtherAddress__c = accListPyrAcc.JJ_JPN_OtherAddress__c;
             customerrequest.JJ_JPN_PayerTelephone__c = accListPyrAcc.Phone;
             customerrequest.JJ_JPN_PAYERFax__c = accListPyrAcc.Fax;
             
             system.debug('$$$AccList==>'+accList);
             
             //default values on CMR-START
                customerrequest.JJ_JPN_PaymentCondition__c= accListPyrAcc.payment_terms__c;
                customerrequest.JJ_JPN_PaymentMethod__c= accListPyrAcc.JJ_JPN_PaymentMethod__c;
                customerrequest.JJ_JPN_DeliveryDocumentNote__c= accListPyrAcc.JJ_JPN_DeliveryDocumentNote__c;
                customerrequest.JJ_JPN_BillToRTNFAX__c= accListPyrAcc.JJ_JPN_BillToRTNFAXHowmanytimestosend__c;
                customerrequest.JJ_JPN_EDIFlag__c= accListPyrAcc.JJ_JPN_EDI_Flag__c;
                customerrequest.JJ_JPN_FAXOrder__c=String.valueOf(accListPyrAcc.JJ_JPN_FAXOrder__c);
                customerrequest.JJ_JPN_CustomerOrderNoRequired__c=accListPyrAcc.JJ_JPN_PurchaseOrderNumberRequired__c;
                //customerrequest.JJ_JPN_CustomerOrderNoNumberofDigits__c= String.valueOf(accListPyrAcc.Call_Frequency_Cycle__c);
                customerrequest.JJ_JPN_CustomerOrderNoNumberofDigits__c= String.valueOf(accListPyrAcc.JJ_JPN_CustomerOrderNoNumberofDigits__c);
                customerrequest.JJ_JPN_ReturnedGoodsOrderNoInDigits__c= accListPyrAcc.JJ_JPN_DSO__c;
                customerrequest.JJ_JPN_DirectDelivery__c= accListPyrAcc.JJ_JPN_DirectDeliveryReturned__c;
                customerrequest.JJ_JPN_TransactionDetailSubmission__c= accListPyrAcc.JJ_JPN_TransactionDetailSubmission__c;              
                customerrequest.JJ_JPN_SubCustomerGroup__c= accListPyrAcc.JJ_JPN_SubCustomerGroup__c;
                customerrequest.JJ_JPN_FestivalDelivery__c= accListPyrAcc.JJ_JPN_FestivalDelivery__c;
                customerrequest.JJ_JPN_EDIStoreCode__c=accListPyrAcc.JJ_JPN_EDIStoreCode__c;
                customerrequest.JJ_JPN_BillSubmission__c= accListPyrAcc.JJ_JPN_BillSubmission__c;
                customerrequest.JJ_JPN_ItemizedBilling__c= '1';
                 customerrequest.JJ_JPN_SalesItem__c = 'blank';

              //  customerrequest.JJ_JPN_CreditPersonInchargeName__c= accListPyrAcc.JJ_JPN_CreditPersonInchargeName__c;
             
             if(accListPyrAcc.JJ_JPN_ADSHandlingFlag__c!=''){
                customerrequest.JJ_JPN_ADSHandlingFlag__c='2';    
             }
                          
               //default values on CMR-END 
         }
         if( billToAccID!='null')
         {
             accListBillTo = [SELECT id , name, Phone, fax, JJ_JPN_BillToCode__c,JJ_JPN_CustomerNameKana__c,ShippingPostalCode,Address2__c, Address3__c, ShippingCountry, Address1__c,JJ_JPN_OtherAddress__c,JJ_JPN_BillToNameKanji__c FROM Account where id=:billToAccID ];
             
             customerrequest.JJ_JPN_BillToCode__c = String.valueof(accListBillTo.JJ_JPN_BillToCode__c);
             customerrequest.JJ_JPN_BillToNameKanji__c = accListBillTo.JJ_JPN_BillToNameKanji__c;
             customerrequest.JJ_JPN_BillToNameKana__c = accListBillTo.JJ_JPN_CustomerNameKana__c;
             customerrequest.JJ_JPN_BillToState__c = accListBillTo.Address3__c;
             customerrequest.JJ_JPN_BillToPostalCode__c = accListBillTo.ShippingPostalCode;
             customerrequest.JJ_JPN_BillToTownshipCity__c = accListBillTo.Address2__c;
             customerrequest.JJ_JPN_BillToStreet__c = accListBillTo.Address1__c;
             customerrequest.JJ_JPN_BillToOtherAddress__c = accListBillTo.JJ_JPN_OtherAddress__c;
             customerrequest.JJ_JPN_BillToTelephone__c = accListBillTo.phone;
             customerrequest.JJ_JPN_BillToFax__c = accListBillTo.fax;
             
            // customerrequest.JJ_JPN_BillToCode__c
             
         }
         
         if(radio == 'Radio6' && accListPyrAcc!=null)
         {
            /* customerrequest.JJ_JPN_SoldToCode__c = String.valueof(accListPyrAcc.AccountNumber);
             customerrequest.JJ_JPN_SoldToNameKanji__c = accListPyrAcc.Name;
             customerrequest.JJ_JPN_SoldToNameKana__c = accListPyrAcc.JJ_JPN_CustomerNameKana__c;
             customerrequest.JJ_JPN_SoldToPostalCode__c = accListPyrAcc.ShippingPostalCode; 
             customerrequest.JJ_JPN_SoldToState__c = accListPyrAcc.Address3__c; 
             customerrequest.JJ_JPN_SoldToTownshipCity__c = accListPyrAcc.Address2__c;
             customerrequest.JJ_JPN_SoldToStreet__c = accListPyrAcc.Address1__c;
             customerrequest.JJ_JPN_SoldToState__c= accListPyrAcc.Address3__c;
             customerrequest.JJ_JPN_SoldToOtherAddress__c = accListPyrAcc.JJ_JPN_OtherAddress__c;
             customerrequest.JJ_JPN_SoldToTelephone__c = accListPyrAcc.Phone;
             customerrequest.JJ_JPN_SoldToFax__c = accListPyrAcc.Fax;*/
         }
         
    }
    
    public void autoPopulateValues()
    {
        if(radio == 'Radio1')
        {
            system.debug('InsideAutoPopulate');
            customerrequest.JJ_JPN_PayerCode__c = randomNumberGenerator();
        }
        else if(radio == 'Radio2')
        {
            customerrequest.JJ_JPN_PayerCode__c = randomNumberGenerator();
            customerrequest.JJ_JPN_SoldToCode__c = randomNumberGenerator2();
        }
        else if(radio == 'Radio3')
        {
            customerrequest.JJ_JPN_PayerCode__c = randomNumberGenerator();
            customerrequest.JJ_JPN_BillToCode__c = randomNumberGenerator2();
        }
        else if(radio == 'Radio4')
        {
            customerrequest.JJ_JPN_PayerCode__c = randomNumberGenerator();
            customerrequest.JJ_JPN_BillToCode__c = randomNumberGenerator2();
            customerrequest.JJ_JPN_SoldToCode__c = randomNumberGenerator3();
        }
        else if(radio == 'Radio5')
        {
            customerrequest.JJ_JPN_SoldToCode__c = randomNumberGenerator();
        }
        else if(radio == 'Radio6')
        {
            customerrequest.JJ_JPN_BillToCode__c = randomNumberGenerator();
        }
        else if(radio == 'Radio7')
        {
            customerrequest.JJ_JPN_SoldToCode__c = randomNumberGenerator();
            customerrequest.JJ_JPN_BillToCode__c = randomNumberGenerator2();
        }
        
    }
        
    /*public string randomNumberGenerator() 
    {
        String randomNum = String.valueOf(Math.abs(Crypto.getRandomInteger()));
        String rndNumFiveDigit = randomNum.substring(0,5);
        return rndNumFiveDigit;
    }*/
    
    // From Custom Setting
    public string randomNumberGenerator() 
    {
        //String randomNum = String.valueof(AutoNumberCMR__c.getValues('Account Outlet').JJ_JPN_AutoNumberOutlet__c);
        Integer autoNum = Integer.valueOf(AutoNumberCMR__c.getValues('Account Outlet').JJ_JPN_AutoNumberOutlet__c);
        system.debug('From Test==>'+autoNum);
        String rndNumFiveDigit;
        if(autoNum!=Null)
        {
            String autoIncrement = String.ValueOf(autoNum+1);
            rndNumFiveDigit  = autoIncrement.substring(0,5);
        }
        return rndNumFiveDigit;
    }
    
    public string randomNumberGenerator2() 
    {
        //String randomNum = String.valueof(AutoNumberCMR__c.getValues('Account Outlet').JJ_JPN_AutoNumberOutlet__c);
        Integer autoNum = Integer.valueOf(AutoNumberCMR__c.getValues('Account Outlet').JJ_JPN_AutoNumberOutlet__c);
        String rndNumFiveDigit;
        if(autoNum!=Null)
        {
            String autoIncrement = String.ValueOf(autoNum+2);
            rndNumFiveDigit  = autoIncrement.substring(0,5);
        }
        return rndNumFiveDigit;
    }
    
    public string randomNumberGenerator3() 
    {
        //String randomNum = String.valueof(AutoNumberCMR__c.getValues('Account Outlet').JJ_JPN_AutoNumberOutlet__c);
        Integer autoNum = Integer.valueOf(AutoNumberCMR__c.getValues('Account Outlet').JJ_JPN_AutoNumberOutlet__c);
        String rndNumFiveDigit;
        if(autoNum!=Null)
        {
            String autoIncrement = String.ValueOf(autoNum+3);
            rndNumFiveDigit  = autoIncrement.substring(0,5);
        }
        return rndNumFiveDigit;
    }
    
    /*public string randomNumberGenerator() 
    {
        //String randomNum = String.valueof(AutoNumberCMR__c.getValues('Account Outlet').JJ_JPN_AutoNumberOutlet__c);
        Integer autoNum = Integer.valueOf(customerrequest.JJ_JPN_AutoNumberForOutlet__c);
        String autoIncrement = String.ValueOf(autoNum+1);
        String rndNumFiveDigit = autoIncrement.substring(0,5);
        return rndNumFiveDigit;
    }*/
    
     public PageReference Cancel() {
        Pagereference p = new PageReference('/apex/CustomerNewRegistration');
        p.setredirect(true);
        return p;
   }
    
        public PageReference doSave() {
        
         customerrequest.JJ_JPN_SamManager__c = samMgrID;
         customerrequest.JJ_JPN_AccountType__c = accType;
        
        system.debug('$$$samMgrID==>'+samMgrID);
        
       /*if(!string.IsEmpty(samMgrID) && samMgrID!='null')
        {
            
            customerrequest.JJ_JPN_SamManager__c = samMgrID;
        }*/
        
        if(radio == 'Radio1')
        {
           system.debug('$$$Name==>'+customerrequest.JJ_JPN_PayerNameKanji__c);
           system.debug('$$$Name1==>'+customerrequest.JJ_JPN_PayerNameKana__c);
           if( (customerrequest.JJ_JPN_PayerNameKanji__c!=null && customerrequest.JJ_JPN_PayerNameKanji__c!='' ) && ( customerrequest.JJ_JPN_PayerNameKana__c!=null && customerrequest.JJ_JPN_PayerNameKana__c!=''))
           {
               
                customerrequest.JJ_JPN_AccountCreationType__c = System.Label.PayerBillToSoldToSameCode;
                customerrequest.JJ_JPN_InterfaceCheck__c = 1;
           
                customerrequest.Name = customerrequest.JJ_JPN_PayerNameKanji__c;
                
                customerrequest.JJ_JPN_BillToCode__c = customerrequest.JJ_JPN_PayerCode__c;
                customerrequest.JJ_JPN_BillToTownshipCity__c = customerrequest.JJ_JPN_PayerTownshipCity__c;
                customerrequest.JJ_JPN_BillToNameKanji__c = customerrequest.JJ_JPN_PayerNameKanji__c;
                customerrequest.JJ_JPN_BillToStreet__c = customerrequest.JJ_JPN_PayerStreet__c;
                customerrequest.JJ_JPN_BillToNameKana__c = customerrequest.JJ_JPN_PayerNameKana__c;
                customerrequest.JJ_JPN_BillToOtherAddress__c = customerrequest.JJ_JPN_PayerOtherAddress__c;
                customerrequest.JJ_JPN_BillToPostalCode__c= customerrequest.JJ_JPN_PayerPostalCode__c;
                customerrequest.JJ_JPN_BillToTelephone__c = customerrequest.JJ_JPN_PayerTelephone__c;
                customerrequest.JJ_JPN_BillToState__c = customerrequest.JJ_JPN_PayerState__c;
                customerrequest.JJ_JPN_BillToFax__c = customerrequest.JJ_JPN_PAYERFax__c;
                
                customerrequest.JJ_JPN_SoldToCode__c = customerrequest.JJ_JPN_PayerCode__c;
                customerrequest.JJ_JPN_SoldToTownshipCity__c = customerrequest.JJ_JPN_PayerTownshipCity__c;
                customerrequest.JJ_JPN_SoldToNameKanji__c = customerrequest.JJ_JPN_PayerNameKanji__c;
                customerrequest.JJ_JPN_SoldToStreet__c = customerrequest.JJ_JPN_PayerStreet__c;
                customerrequest.JJ_JPN_SoldToNameKana__c = customerrequest.JJ_JPN_PayerNameKana__c;
                customerrequest.JJ_JPN_SoldToOtherAddress__c = customerrequest.JJ_JPN_PayerOtherAddress__c;
                customerrequest.JJ_JPN_SoldToPostalCode__c= customerrequest.JJ_JPN_PayerPostalCode__c;
                customerrequest.JJ_JPN_SoldToTelephone__c = customerrequest.JJ_JPN_PayerTelephone__c;
                customerrequest.JJ_JPN_SoldToState__c = customerrequest.JJ_JPN_PayerState__c;
                customerrequest.JJ_JPN_SoldToFax__c = customerrequest.JJ_JPN_PAYERFax__c;
               
               //default values on CMR-START
                customerrequest.JJ_JPN_PaymentCondition__c= 'ZJ42';
                customerrequest.JJ_JPN_PaymentMethod__c= 'D';
                customerrequest.JJ_JPN_DeliveryDocumentNote__c= '1';
                customerrequest.JJ_JPN_DirectDelivery__c= '10';
                customerrequest.JJ_JPN_BillSubmission__c= 'ZYNR';
                customerrequest.JJ_JPN_FestivalDelivery__c= 'J4';
                customerrequest.JJ_JPN_SubCustomerGroup__c= '';
                customerrequest.JJ_JPN_ItemizedBilling__c= '1';
                 customerrequest.JJ_JPN_SalesItem__c = 'blank';

               //default values on CMR-END 
               
               AutoNumberCMR__c autoCMR = AutoNumberCMR__c.getInstance('Account Outlet');
               autoCMR.JJ_JPN_AutoNumberOutlet__c = Decimal.ValueOf(randomNumberGenerator());
               Update autoCMR;
               
           }
          else
           {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unknown Error: Please enter payer name'));
               return null;
           }
            
        }
        else if(radio == 'Radio2')
        {
           if( (customerrequest.JJ_JPN_PayerNameKanji__c!=null && customerrequest.JJ_JPN_PayerNameKanji__c!='' ) && ( customerrequest.JJ_JPN_PayerNameKana__c!=null && customerrequest.JJ_JPN_PayerNameKana__c!=''))
           {
                customerrequest.JJ_JPN_AccountCreationType__c = System.Label.PyrBillWithSameCodeSoldDiffCode;
                customerrequest.JJ_JPN_InterfaceCheck__c = 2;
                customerrequest.Name = customerrequest.JJ_JPN_PayerNameKanji__c;
            
                customerrequest.JJ_JPN_BillToCode__c = customerrequest.JJ_JPN_PayerCode__c;
                customerrequest.JJ_JPN_BillToTownshipCity__c = customerrequest.JJ_JPN_PayerTownshipCity__c;
                customerrequest.JJ_JPN_BillToNameKanji__c = customerrequest.JJ_JPN_PayerNameKanji__c;
                customerrequest.JJ_JPN_BillToStreet__c = customerrequest.JJ_JPN_PayerStreet__c;
                customerrequest.JJ_JPN_BillToNameKana__c = customerrequest.JJ_JPN_PayerNameKana__c;
                customerrequest.JJ_JPN_BillToOtherAddress__c = customerrequest.JJ_JPN_PayerOtherAddress__c;
                customerrequest.JJ_JPN_BillToPostalCode__c= customerrequest.JJ_JPN_PayerPostalCode__c;
                customerrequest.JJ_JPN_BillToTelephone__c = customerrequest.JJ_JPN_PayerTelephone__c;
                customerrequest.JJ_JPN_BillToState__c = customerrequest.JJ_JPN_PayerState__c;
                customerrequest.JJ_JPN_BillToFax__c = customerrequest.JJ_JPN_PAYERFax__c;
               
                //default values on CMR-START
                customerrequest.JJ_JPN_PaymentCondition__c= 'ZJ42';
                customerrequest.JJ_JPN_PaymentMethod__c= 'D';
                customerrequest.JJ_JPN_DeliveryDocumentNote__c= '1';
                customerrequest.JJ_JPN_DirectDelivery__c= '10';
                customerrequest.JJ_JPN_BillSubmission__c= 'ZYNR';
                customerrequest.JJ_JPN_FestivalDelivery__c= 'J4';
                customerrequest.JJ_JPN_SubCustomerGroup__c= '';
                customerrequest.JJ_JPN_ItemizedBilling__c= '1';
                customerrequest.JJ_JPN_OrderBlock__c='X';
                customerrequest.JJ_JPN_SalesItem__c = 'blank';
               //default values on CMR-END 
                
                AutoNumberCMR__c autoCMR = AutoNumberCMR__c.getInstance('Account Outlet');
               autoCMR.JJ_JPN_AutoNumberOutlet__c = Decimal.ValueOf(randomNumberGenerator2());
               Update autoCMR;
                
           }
           else
           {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unknown Error Please enter payer name'));
               return null;
           }

        }
        else if(radio == 'Radio3')
        {
        
           if( (customerrequest.JJ_JPN_PayerNameKanji__c!=null && customerrequest.JJ_JPN_PayerNameKanji__c!='' ) && ( customerrequest.JJ_JPN_PayerNameKana__c!=null && customerrequest.JJ_JPN_PayerNameKana__c!=''))
           {
                customerrequest.JJ_JPN_AccountCreationType__c = System.Label.BillSoldWithSameCodePyrDiffCode;
                customerrequest.JJ_JPN_InterfaceCheck__c = 3;
                customerrequest.Name = customerrequest.JJ_JPN_PayerNameKanji__c;
            
                customerrequest.JJ_JPN_SoldToCode__c = customerrequest.JJ_JPN_BillToCode__c;
                customerrequest.JJ_JPN_SoldToTownshipCity__c = customerrequest.JJ_JPN_BillToTownshipCity__c;
                customerrequest.JJ_JPN_SoldToNameKanji__c = customerrequest.JJ_JPN_BillToNameKanji__c;
                customerrequest.JJ_JPN_SoldToStreet__c = customerrequest.JJ_JPN_BillToStreet__c;
                customerrequest.JJ_JPN_SoldToNameKana__c = customerrequest.JJ_JPN_BillToNameKana__c;
                customerrequest.JJ_JPN_SoldToOtherAddress__c = customerrequest.JJ_JPN_BillToOtherAddress__c;
                customerrequest.JJ_JPN_SoldToPostalCode__c= customerrequest.JJ_JPN_BillToPostalCode__c;
                customerrequest.JJ_JPN_SoldToTelephone__c = customerrequest.JJ_JPN_BillToTelephone__c;
                customerrequest.JJ_JPN_SoldToState__c = customerrequest.JJ_JPN_BillToState__c;
                customerrequest.JJ_JPN_SoldToFax__c = customerrequest.JJ_JPN_BillToFax__c;
                
               //default values on CMR-START
                customerrequest.JJ_JPN_PaymentCondition__c= 'ZJ42';
                customerrequest.JJ_JPN_PaymentMethod__c= 'D';
                customerrequest.JJ_JPN_DeliveryDocumentNote__c= '1';
                customerrequest.JJ_JPN_DirectDelivery__c= '10';
                customerrequest.JJ_JPN_BillSubmission__c= 'ZYNR';
                customerrequest.JJ_JPN_FestivalDelivery__c= 'J4';
                customerrequest.JJ_JPN_SubCustomerGroup__c= '';
                customerrequest.JJ_JPN_ItemizedBilling__c= '1';
                customerrequest.JJ_JPN_OrderBlock__c='X';
                 customerrequest.JJ_JPN_SalesItem__c = 'blank';

               //default values on CMR-END 
               
               AutoNumberCMR__c autoCMR = AutoNumberCMR__c.getInstance('Account Outlet');
               autoCMR.JJ_JPN_AutoNumberOutlet__c = Decimal.ValueOf(randomNumberGenerator2());
               Update autoCMR;
               
           }
           else
           {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unknown Error Please enter payer name'));
               return null;
           }

        }
        else if(radio == 'Radio4')
        {
           if( (customerrequest.JJ_JPN_PayerNameKanji__c!=null && customerrequest.JJ_JPN_PayerNameKanji__c!='' ) && ( customerrequest.JJ_JPN_PayerNameKana__c!=null && customerrequest.JJ_JPN_PayerNameKana__c!=''))
           {
               customerrequest.JJ_JPN_AccountCreationType__c = System.Label.PyrBillSoldWithDiffCode;
               customerrequest.JJ_JPN_InterfaceCheck__c = 4;
               customerrequest.Name = customerrequest.JJ_JPN_PayerNameKanji__c;
               
               //default values on CMR-START
                customerrequest.JJ_JPN_PaymentCondition__c= 'ZJ42';
                customerrequest.JJ_JPN_PaymentMethod__c= 'D';
                customerrequest.JJ_JPN_DeliveryDocumentNote__c= '1';
                customerrequest.JJ_JPN_DirectDelivery__c= '10';
                customerrequest.JJ_JPN_BillSubmission__c= 'ZYNR';
                customerrequest.JJ_JPN_FestivalDelivery__c= 'J4';
                customerrequest.JJ_JPN_SubCustomerGroup__c= '';
                customerrequest.JJ_JPN_ItemizedBilling__c= '1';
                customerrequest.JJ_JPN_OrderBlock__c='X';
                customerrequest.JJ_JPN_SalesItem__c = 'blank';

               //default values on CMR-END 
               
               AutoNumberCMR__c autoCMR = AutoNumberCMR__c.getInstance('Account Outlet');
               autoCMR.JJ_JPN_AutoNumberOutlet__c = Decimal.ValueOf(randomNumberGenerator3());
               Update autoCMR;
           }
           else
           {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unknown Error Please enter payer name'));
               return null;
           }
           
        }
        else if(radio == 'Radio5')
        {
           if( (customerrequest.JJ_JPN_PayerNameKanji__c!=null && customerrequest.JJ_JPN_PayerNameKanji__c!='' ) && ( customerrequest.JJ_JPN_PayerNameKana__c!=null && customerrequest.JJ_JPN_PayerNameKana__c!=''))
           {
               customerrequest.JJ_JPN_AccountCreationType__c = System.Label.SoldToOnlyPyrBillToAlreadyRegist;
               customerrequest.JJ_JPN_InterfaceCheck__c = 5;
               customerrequest.Name = customerrequest.JJ_JPN_PayerNameKanji__c;
               
               AutoNumberCMR__c autoCMR = AutoNumberCMR__c.getInstance('Account Outlet');
               autoCMR.JJ_JPN_AutoNumberOutlet__c = Decimal.ValueOf(randomNumberGenerator());
               Update autoCMR;
           }
           else
           {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unknown Error Please enter payer name'));
               return null;
           }
           
        }
        else if(radio == 'Radio6')
        {
            if( (customerrequest.JJ_JPN_PayerNameKanji__c!=null && customerrequest.JJ_JPN_PayerNameKanji__c!='' ) && ( customerrequest.JJ_JPN_PayerNameKana__c!=null && customerrequest.JJ_JPN_PayerNameKana__c!=''))
           {
               customerrequest.JJ_JPN_AccountCreationType__c = System.Label.BillToOnlyPyrAlrdyRegSoldToSameAsPyr;
               customerrequest.JJ_JPN_InterfaceCheck__c = 6;
               
               customerrequest.Name = customerrequest.JJ_JPN_PayerNameKanji__c;
               customerrequest.JJ_JPN_SoldToCode__c = customerrequest.JJ_JPN_BillToCode__c;
               customerrequest.JJ_JPN_SoldToTownshipCity__c = customerrequest.JJ_JPN_BillToTownshipCity__c;
               customerrequest.JJ_JPN_SoldToNameKanji__c = customerrequest.JJ_JPN_BillToNameKanji__c;
               customerrequest.JJ_JPN_SoldToStreet__c = customerrequest.JJ_JPN_BillToStreet__c;
               customerrequest.JJ_JPN_SoldToNameKana__c = customerrequest.JJ_JPN_BillToNameKana__c;
               customerrequest.JJ_JPN_SoldToOtherAddress__c = customerrequest.JJ_JPN_BillToOtherAddress__c;
               customerrequest.JJ_JPN_SoldToPostalCode__c= customerrequest.JJ_JPN_BillToPostalCode__c;
               customerrequest.JJ_JPN_SoldToTelephone__c = customerrequest.JJ_JPN_BillToTelephone__c;
               customerrequest.JJ_JPN_SoldToState__c = customerrequest.JJ_JPN_BillToState__c;
               customerrequest.JJ_JPN_SoldToFax__c = customerrequest.JJ_JPN_BillToFax__c;
               
               AutoNumberCMR__c autoCMR = AutoNumberCMR__c.getInstance('Account Outlet');
               autoCMR.JJ_JPN_AutoNumberOutlet__c = Decimal.ValueOf(randomNumberGenerator());
               Update autoCMR;
             
           }
           else
           {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unknown Error Please enter payer name'));
               return null;
           }
           
        }
        else if(radio == 'Radio7')
        {
           if( (customerrequest.JJ_JPN_PayerNameKanji__c!=null && customerrequest.JJ_JPN_PayerNameKanji__c!='' ) && ( customerrequest.JJ_JPN_PayerNameKana__c!=null && customerrequest.JJ_JPN_PayerNameKana__c!=''))
           {
               customerrequest.JJ_JPN_AccountCreationType__c = System.Label.BillToSoldToPyrAlrdyReg;
               customerrequest.JJ_JPN_InterfaceCheck__c = 7;
               customerrequest.Name = customerrequest.JJ_JPN_PayerNameKanji__c;
               
               AutoNumberCMR__c autoCMR = AutoNumberCMR__c.getInstance('Account Outlet');
               autoCMR.JJ_JPN_AutoNumberOutlet__c = Decimal.ValueOf(randomNumberGenerator2());
               Update autoCMR;
           }
           else
           {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unknown Error Please enter payer name'));
               return null;
           }
           
        }
        try{
        Database.insert(customerrequest,true);
        }
        catch(exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
          
        Pagereference p = new PageReference('/'+customerrequest.Id);
        p.setredirect(true);
        return p;
    }
    
    
}