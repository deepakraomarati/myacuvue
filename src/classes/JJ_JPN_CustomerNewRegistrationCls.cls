/** 
* File Name: JJ_JPN_CustomerNewRegistrationCls
* Description : Business logic applied for Customer Master Regisrtion.
* Copyright : Johnson & Johnson
* @author : Shiva Shankar | sshank10@its.jnj.com | bnshiva.shankar@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |8-July-2016 |@sshank10@its.jnj.com |New Class created
*/ 

public class JJ_JPN_CustomerNewRegistrationCls 
{

    public JJ_JPN_CustomerNewRegistrationCls(ApexPages.StandardController controller) {

    }

    public String selectedValueStr {get;set;}
    
    public JJ_JPN_CustomerMasterRequest__c CMR {get;set;}
    public String payerIDStr {get;set;}
    public Boolean displayAccCatg {get;set;}
    public Boolean showPayerAcc {get;set;}
    public Boolean showFBillToAcc {get;set;}
    
    /**
        *Getting Entry criteria for registration
    */
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Radio1','(Register Payer, Bill To and Sold To with same code)'));
        options.add(new SelectOption('Radio2','(Register Payer and Bill To with same code but Sold To with a different code)'));
        options.add(new SelectOption('Radio3','(Register Bill To and Sold To with same code but Payer with a different code)')); 
        options.add(new SelectOption('Radio4','(Register Payer, Bill To and Sold To with different code)')); 
        options.add(new SelectOption('Radio5','(Register Sold To only (Payer/Bill To already registered))')); 
        options.add(new SelectOption('Radio6','(Register Bill To only (Payer is already registered, Sold To same as Payer))')); 
        options.add(new SelectOption('Radio7','(Register Bill To and Sold To (Payer is already registered))')); 
        return options;
    }
    
    
    /**
        *Onselection of SAM/SAM Dealer Account Type , show SAM Manger
    */
    public Boolean showSAMMgr {
        get{
            return CMR.JJ_JPN_AccountType__c == 'SAM/SAM Dealer';
        }
    }
    
    /**
        *Constructor start Here
    */
    
    public JJ_JPN_CustomerNewRegistrationCls()
    {
        CMR = new JJ_JPN_CustomerMasterRequest__c();
        
    }
    
    public void accSubValues()
    {
        system.debug('$$$ A selected Values ==>'+selectedValueStr);
        if(selectedValueStr == 'Radio5')
        {
            system.debug('$$$ A if selected Values ==>'+selectedValueStr);
            displayAccCatg = true;
            showPayerAcc = true;
            showFBillToAcc = true;
            system.debug('$$$ A if displayAccCatg  Values ==>'+displayAccCatg );
            
        }
       /* else if(selectedValueStr == 'Radio6' || selectedValueStr == 'Radio7')
        {
            displayAccCatg = true;
            showPayerAcc = true;
            showFBillToAcc = false;
        }*/
        else
        {
            displayAccCatg = false;
        }
    }
    
    public PageReference Save()
    {
        if(selectedValueStr!=Null)
        {
            system.debug('$$$ selected Values ==>'+selectedValueStr);
            system.debug('$$$ selected Values Payer==>'+CMR.JJ_JPN_PayerAccount__c);
            payerIDStr = CMR.JJ_JPN_PayerAccount__c;
            system.debug('$$$ payer ID==>'+payerIDStr);
            
            
            PageReference pref = new PageReference('/apex/CustomerMasterRequestsEditPage?Radio='+selectedValueStr+'&payerAcc='+payerIDStr);
            pref.setRedirect(true); 
            return pref;
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unknown Error: Please select any of one radio button'));
            return null;
        }
    }   
    
}