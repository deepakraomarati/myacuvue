/** 
* File Name: JJ_JPN_CustomerNewRegistrationExt
* Description : Business logic applied for Customer Master Regisrtion.
* Copyright : Johnson & Johnson
* @author : Shiva Shankar | sshank10@its.jnj.com | bnshiva.shankar@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |8-July-2016 |@sshank10@its.jnj.com |New Class created
*/ 

public class JJ_JPN_CustomerNewRegistrationExt {

    public JJ_JPN_CustomerMasterRequest__c CMR {get;set;}
    public String selectedValueStr {get;set;}
    
    public String payerIDStr {get;set;}
    public String billToIDStr {get;set;}
    public String samMangerID {get;set;}
    public Boolean displayAccCatg {get;set;}
    public Boolean showPayerAcc {get;set;}
    public Boolean showFBillToAcc {get;set;}
    
     /**
        *Onselection of SAM/SAM Dealer Account Type , show SAM Manger
    */
    public Boolean showSAMMgr {
        get{
            return CMR.JJ_JPN_AccountType__c == 'SAM/SAM Dealer';
        }
    }
    

    public JJ_JPN_CustomerNewRegistrationExt(ApexPages.StandardController controller) {
        CMR = new JJ_JPN_CustomerMasterRequest__c();
    }
    
      /**
        *Getting Entry criteria for registration
    */
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Radio1', System.Label.PayerBillToSoldToSameCode));
        options.add(new SelectOption('Radio5', System.Label.SoldToOnlyPyrBillToAlreadyRegist));
        options.add(new SelectOption('Radio3', System.Label.BillSoldWithSameCodePyrDiffCode)); 
        options.add(new SelectOption('Radio4', System.Label.PyrBillSoldWithDiffCode)); 
        options.add(new SelectOption('Radio2', System.Label.PyrBillWithSameCodeSoldDiffCode));
        options.add(new SelectOption('Radio6', System.Label.BillToOnlyPyrAlrdyRegSoldToSameAsPyr)); 
        options.add(new SelectOption('Radio7', System.Label.BillToSoldToPyrAlrdyReg)); 
        return options;
    }
    
    public void accSubValues()
    {
        CMR.JJ_JPN_PayerAccount__c= null;
        CMR.JJ_JPN_BillToAccount__c = null;
        system.debug('$$$ A selected Values ==>'+selectedValueStr);
        if(selectedValueStr == 'Radio5')
        {
            system.debug('$$$ A if selected Values ==>'+selectedValueStr);
            displayAccCatg = true;
            showPayerAcc = true;
            showFBillToAcc = true;
            system.debug('$$$ A if displayAccCatg  Values ==>'+displayAccCatg );
            
        }
        else if(selectedValueStr == 'Radio6' || selectedValueStr == 'Radio7')
        {
            displayAccCatg = true;
            showPayerAcc = true;
            showFBillToAcc = false;
        }
        else
        {
            displayAccCatg = false;
        }
    }
    
    public PageReference Save()
    {
        if(selectedValueStr!=Null)
        {
            system.debug('$$$ selected Values ==>'+selectedValueStr);
            system.debug('$$$ selected Values Payer==>'+CMR.JJ_JPN_PayerAccount__c);
            payerIDStr = CMR.JJ_JPN_PayerAccount__c;
            billToIDStr = CMR.JJ_JPN_BillToAccount__c;
            samMangerID = CMR.JJ_JPN_SamManager__c;
            
            system.debug('$$$ payer ID==>'+payerIDStr);
            
            if(samMangerID==Null)
            {
                PageReference pref = new PageReference('/apex/CustomerMasterRequestsEditPage?Radio='+selectedValueStr+'&payerAcc='+payerIDStr+'&billToAcc='+billToIDStr+'&accType='+CMR.JJ_JPN_AccountType__c);
                pref.setRedirect(true); 
                return pref;
            }
            else {
            PageReference pref = new PageReference('/apex/CustomerMasterRequestsEditPage?Radio='+selectedValueStr+'&payerAcc='+payerIDStr+'&billToAcc='+billToIDStr+'&accType='+CMR.JJ_JPN_AccountType__c+'&samMgrID='+samMangerID);
            pref.setRedirect(true); 
            return pref;
            }
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unknown Error: Please select any of one radio button'));
            return null;
        }
    }   
    
    public pageReference cancel()
    {
        pageReference pgeRef = new pageReference('/a1F/o');
        pgeRef.setRedirect(true);
        return pgeRef;
    }
    

}