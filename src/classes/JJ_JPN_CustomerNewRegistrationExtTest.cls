/** 
* File Name: JJJ_JPN_CustomerNewRegistrationExtTest 
* Description : Test Class for JJ_JPN_CustomerNewRegistrationExt - Business logic applied for Customer Master Regisrtion.
* Main Class : JJ_JPN_CustomerNewRegistrationExt 
* Copyright : Johnson & Johnson
* @author : Shiva Shankar | sshank10@its.jnj.com | bnshiva.shankar@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |11-July-2016 |@sshank10@its.jnj.com |New Class created
*/ 

@IsTest
public class JJ_JPN_CustomerNewRegistrationExtTest 
{
    static testMethod void createCMR()
    {
        Test.startTest(); 

        JJ_JPN_CustomerMasterRequest__c CMR =  new JJ_JPN_CustomerMasterRequest__c();
        CMR.Name = 'CMRAccout';
        insert CMR;
        
       
        ApexPages.StandardController sc = new ApexPages.StandardController(CMR);
        JJ_JPN_CustomerNewRegistrationExt CMRCls = new JJ_JPN_CustomerNewRegistrationExt(sc);
        CMRCls.selectedValueStr='Radio5';
        CMRCls.getItems();
        CMRCls.accSubValues();
        CMRCls.Save();
        CMRCls.cancel();
        
        system.assertEquals(CMR.Name,CMR.Name ,'HURRY ITS WRONG OUTPUT');
        system.assertNotEquals('CMR',CMR.Name ,'HURRY ITS WRONG OUTPUT');
        system.assert(CMR.Name==CMR.Name ,'HURRY ITS WRONG OUTPUT');
        
        
        Test.stopTest();
    }
    
    static testMethod void createCMR1()
    {
        Test.startTest(); 

        JJ_JPN_CustomerMasterRequest__c CMR =  new JJ_JPN_CustomerMasterRequest__c();
        CMR.Name = 'CMRAccout';
        insert CMR;
        
                
        ApexPages.StandardController sc = new ApexPages.StandardController(CMR);
        JJ_JPN_CustomerNewRegistrationExt CMRCls = new JJ_JPN_CustomerNewRegistrationExt(sc);
        CMRCls.selectedValueStr='Radio6';
        CMRCls.getItems();
        CMRCls.accSubValues();
        CMRCls.Save();
        CMRCls.cancel();
        
        system.assertEquals(CMR.Name,CMR.Name ,'HURRY ITS WRONG OUTPUT');
        system.assertNotEquals('CMR',CMR.Name ,'HURRY ITS WRONG OUTPUT');
        system.assert(CMR.Name==CMR.Name ,'HURRY ITS WRONG OUTPUT');

        Test.stopTest();
    }
    
}