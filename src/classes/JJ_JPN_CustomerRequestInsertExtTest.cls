/** 
* File Name: JJ_JPN_CustomerRequestInsertExtTest 
* Description : Test Class for CustomerRequestInsertExtension - Business logic applied for Initial Stock Order
* Main Class : CustomerRequestInsertExtension
* Copyright : Johnson & Johnson
* @author : Shiva Shankar | sshank10@its.jnj.com | bnshiva.shankar@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |11-July-2016 |@sshank10@its.jnj.com |New Class created
*/ 
@IsTest
public class JJ_JPN_CustomerRequestInsertExtTest 
{
    static testMethod void createInitialStock()
    {
        Test.startTest(); 
        
        JJ_JPN_CustomerMasterRequest__c customerrequest =  new JJ_JPN_CustomerMasterRequest__c();
        customerrequest.Name = 'CMRAccout';
        customerrequest.JJ_JPN_AccountType__c = 'SAM/SAM Dealer';
        customerrequest.JJ_JPN_PayerCode__c='47583';
        customerrequest.JJ_JPN_PayerNameKanji__c='漢字';
        customerrequest.JJ_JPN_PayerNameKana__c='ｶﾝｼﾞ';
        customerrequest.JJ_JPN_PayerPostalCode__c='4758311';
        customerrequest.JJ_JPN_RegistrationFor__c='435353';
        customerrequest.JJ_JPN_OfficeName__c='sdcevfe';
        customerrequest.JJ_JPN_PersonInchargeName__c='sfdcvcfd';
        customerrequest.JJ_JPN_SalesItem__c='R';
        customerrequest.JJ_JPN_Rank__c='A2';
        customerrequest.JJ_JPN_CreditPersonInchargeName__c='ksjnvf';
        customerrequest.JJ_JPN_PersonInchargeCode__c='93284';
        customerrequest.JJ_JPN_StatusCode__c='H';
        customerrequest.JJ_JPN_RepresentativeNameKana__c='scdscds';
        customerrequest.JJ_JPN_RepresentativeNameKanji__c='bvsf';
        customerrequest.JJ_JPN_PaymentCondition__c='ZJ47';
        customerrequest.JJ_JPN_PaymentMethod__c='D';
        customerrequest.JJ_JPN_ReturnFAXNo__c='9483579';
        insert customerrequest;
        
        Product2 prod = new Product2();
        prod.Name='Test';
        prod.ProductCode = '6';
        prod.BrandCode__c = '1DL';
        prod.BaseCurve__c = '8.5';
        prod.JJ_JPN_Power__c = 'KW';
        prod.JJ_JPN_TotalQuantity__c = '10';
        prod.JJ_JPN_CountryCode__c = 'JPN';
        prod.InternalName__c='Abc';
        insert prod;
        
        JJ_JPN_InitialStock__c initialStock = new JJ_JPN_InitialStock__c();
        initialStock.JJ_JPN_Product__c = prod.id;
        initialStock.JJ_JPN_Customer_Master_Request__c = customerrequest.id;
        initialStock.JJ_JPN_Quantity__c = 10;
        insert initialStock;
        system.assertEquals(initialStock.JJ_JPN_Quantity__c,10,'success');
        
        // creating the ID for the loading class  and intializes id to class , for the beginnning of the page
        Apexpages.currentPage().getParameters().put('id',customerrequest.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(customerrequest);
        CustomerRequestInsertExtension CMRInstCls = new CustomerRequestInsertExtension(sc);
        CMRInstCls.Saveproduct();
        CMRInstCls.prepareproduct();
        CMRInstCls.FirstPage();
        CMRInstCls.next();
        CMRInstCls.previous();
        CMRInstCls.getprev();
        CMRInstCls.getnxt();
        List<CustomerRequestInsertExtension.productinsertwrapper> listWrapperProd = new List<CustomerRequestInsertExtension.productinsertwrapper>();
        
        PageReference pageRef = Page.customerrequestinsertpage;
        Test.setCurrentPage(pageRef);
        Test.stopTest();
    }
}