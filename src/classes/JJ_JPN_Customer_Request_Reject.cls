Global class JJ_JPN_Customer_Request_Reject{
    
    webservice static void Reject_customer_Request(String Cust_ID ){
       
       Transient Approval.ProcessWorkItemRequest Appr = new Approval.ProcessWorkItemRequest();
       
       list<Approval.ProcessWorkitemRequest> apprlist= new list<Approval.ProcessWorkitemRequest>();
       list<ProcessInstanceWorkitem > Piw= new list <ProcessInstanceWorkitem> ();
       Transient Approval.ProcessResult[] result = new List<Approval.ProcessResult>();
       
       
       piw = [SELECT Id FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId=:Cust_ID and ProcessInstance.status='Pending'];
            
            If(!piw.isempty()){
                for(ProcessInstanceWorkitem PI:PIW){
                  Approval.ProcessWorkitemRequest pwr = new Approval.ProcessWorkitemRequest();
                  String piwID = string.valueof(piw[0].Id);
                  pwr.setWorkitemId(piwID );
                  pwr.setComments('Rejected');
                  pwr.setAction('Reject'); 
                  apprlist.add(pwr);                  
                }               
                result =  Approval.process(apprlist);
           }          
           
        }
        
         webservice static void closetest(String Cust_ID){
         JJ_JPN_CustomerMasterRequest__c CMR=[Select id,JJ_JPN_ApprovalStatus__c from JJ_JPN_CustomerMasterRequest__c where id=:Cust_ID];
         list<JJ_JPN_CustomerMasterRequest__c> l1 = new list<JJ_JPN_CustomerMasterRequest__c>();
         l1.add(CMR);
         l1[0].JJ_JPN_ApprovalStatus__c='Closed';
         update(l1);
         }
         
}