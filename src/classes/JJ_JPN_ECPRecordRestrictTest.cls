@isTest
private class JJ_JPN_ECPRecordRestrictTest {    
    @isTest static void test_method_one() {
        JJ_JPN_CustomerMasterRequest__c cmr=new JJ_JPN_CustomerMasterRequest__c(Name='Test',JJ_JPN_PayerCode__c='47583',JJ_JPN_AccountType__c='Field Sales(FS)',JJ_JPN_PayerNameKanji__c='漢字',
                                                                                JJ_JPN_PayerNameKana__c='ｶﾝｼﾞ',JJ_JPN_PayerPostalCode__c='4758312',JJ_JPN_RegistrationFor__c='435353',JJ_JPN_OfficeName__c='sdcevfe',
                                                                                JJ_JPN_PersonInchargeName__c='sfdcvcfd',JJ_JPN_SalesItem__c='R',JJ_JPN_Rank__c='A2',JJ_JPN_CreditPersonInchargeName__c='ksjnvf',
                                                                                JJ_JPN_PersonInchargeCode__c='93284',JJ_JPN_StatusCode__c='H',JJ_JPN_RepresentativeNameKana__c='scdscds',JJ_JPN_RepresentativeNameKanji__c='bvsf',
                                                                                JJ_JPN_PaymentCondition__c='ZJ47',JJ_JPN_PaymentMethod__c='D',JJ_JPN_ReturnFAXNo__c='9483579');
        insert cmr;
        ID CMRID=cmr.ID;
        JJ_JPN_ECPContact__c ecp1=new JJ_JPN_ECPContact__c(Name='upkar1',ECP__c=CMRID);
        JJ_JPN_ECPContact__c ecp2=new JJ_JPN_ECPContact__c(Name='upkar2',ECP__c=CMRID);
        List<JJ_JPN_ECPContact__c> ecps=new List<JJ_JPN_ECPContact__c>();
        ecps.add(ecp1);
        insert ecps;
        system.assertEquals(cmr.ID,CMRID,'success');
    }  
}