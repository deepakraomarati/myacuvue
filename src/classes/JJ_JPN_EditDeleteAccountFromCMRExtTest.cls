/** 
* File Name: JJ_JPN_EditDeleteAccountFromCMRExtTest 
* Description : Test Class for JJ_JPN_EditDeleteAccountFromCMRExt - Business logic applied for Customer Master Regisrtion.
* Main Class : JJ_JPN_EditDeleteAccountFromCMRExt 
* Copyright : Johnson & Johnson
* @author : Shiva Shankar | sshank10@its.jnj.com | bnshiva.shankar@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |01-Oct-2016  |@sshank10@its.jnj.com |New Class created
*/ 

@IsTest
public class JJ_JPN_EditDeleteAccountFromCMRExtTest 
{
    static testMethod void createCMR()
    {
        Test.startTest(); 
        
        JJ_JPN_CustomerMasterRequest__c CMR =  new JJ_JPN_CustomerMasterRequest__c();
        CMR.Name = 'CMRAccout';
        CMR.JJ_JPN_StatusCode__c = 'C';
        CMR.Account_Activation_Start_Date__c = date.today();
        insert CMR;
        system.debug('CMR List==>'+CMR);
   
        
        Account acc = new Account();
        acc.Name = 'AccName';
        acc.JJ_JPN_CustomerNameKana__c = 'Kana';
        acc.OutletNumber__c = '20001';
        acc.AccountNumber = '20001';
        acc.JJ_JPN_BillToCode__c = '10001';
        insert acc;
     
        // creating the ID for the loading class  and intializes id to class , for the beginnning of the page
        Apexpages.currentPage().getParameters().put('accID',acc.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(CMR);
        JJ_JPN_EditDeleteAccountFromCMRExt CMRCls = new JJ_JPN_EditDeleteAccountFromCMRExt(sc);
        CMRCls.getstatusCodeValues();
        CMRCls.statusCode='C';    
        CMRCls.CMR.Account_Activation_Start_Date__c = date.today();
        CMRCls.doSave();
        CMRCls.cancel();
        
        system.assertEquals(CMR.Name,CMR.Name ,'HURRY ITS WRONG OUTPUT');
        system.assertNotEquals('CMR',CMR.Name ,'HURRY ITS WRONG OUTPUT');
        system.assert(CMR.Name==CMR.Name ,'HURRY ITS WRONG OUTPUT');
        
        Test.stopTest();
    }
    
    static testMethod void createCMR1()
    {
        Test.startTest(); 
        
        JJ_JPN_CustomerMasterRequest__c CMR =  new JJ_JPN_CustomerMasterRequest__c();
        CMR.Name = 'CMRAccout';
        CMR.JJ_JPN_StatusCode__c = 'C';
        CMR.Account_Activation_Start_Date__c = date.today();
        insert CMR;
        system.debug('CMR List==>'+CMR);
   
        
        Account acc = new Account();
        acc.Name = 'AccName';
        acc.JJ_JPN_CustomerNameKana__c = 'Kana';
        acc.OutletNumber__c = '20001';
        acc.AccountNumber = '10001';
        acc.JJ_JPN_BillToCode__c = '20001';
        insert acc;
        
            
        // creating the ID for the loading class  and intializes id to class , for the beginnning of the page
        Apexpages.currentPage().getParameters().put('accID',acc.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(CMR);
        JJ_JPN_EditDeleteAccountFromCMRExt CMRCls = new JJ_JPN_EditDeleteAccountFromCMRExt(sc);
        CMRCls.getstatusCodeValues();
        CMRCls.statusCode='C';    
        CMRCls.CMR.Account_Activation_Start_Date__c = date.today();
        CMRCls.doSave();
        CMRCls.cancel();
        
        system.assertEquals(CMR.Name,CMR.Name ,'HURRY ITS WRONG OUTPUT');
        system.assertNotEquals('CMR',CMR.Name ,'HURRY ITS WRONG OUTPUT');
        system.assert(CMR.Name==CMR.Name ,'HURRY ITS WRONG OUTPUT');
        
        Test.stopTest();
    }
    
    static testMethod void createCMR2()
    {
        Test.startTest(); 
        
        JJ_JPN_CustomerMasterRequest__c CMR =  new JJ_JPN_CustomerMasterRequest__c();
        CMR.Name = 'CMRAccout';
        CMR.JJ_JPN_StatusCode__c = 'C';
        CMR.Account_Activation_Start_Date__c = date.today();
        insert CMR;
        system.debug('CMR List==>'+CMR);
   
        
        Account acc = new Account();
        acc.Name = 'AccName';
        acc.JJ_JPN_CustomerNameKana__c = 'Kana';
        acc.OutletNumber__c = '20001';
        acc.AccountNumber = '10001';
        acc.JJ_JPN_BillToCode__c = '20002';
        insert acc;
        
            
        // creating the ID for the loading class  and intializes id to class , for the beginnning of the page
        Apexpages.currentPage().getParameters().put('accID',acc.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(CMR);
        JJ_JPN_EditDeleteAccountFromCMRExt CMRCls = new JJ_JPN_EditDeleteAccountFromCMRExt(sc);
        CMRCls.getstatusCodeValues();
        CMRCls.statusCode='C';    
        CMRCls.CMR.Account_Activation_Start_Date__c = date.today();
        CMRCls.doSave();
        CMRCls.cancel();
        
        system.assertEquals(CMR.Name,CMR.Name ,'HURRY ITS WRONG OUTPUT');
        system.assertNotEquals('CMR',CMR.Name ,'HURRY ITS WRONG OUTPUT');
        system.assert(CMR.Name==CMR.Name ,'HURRY ITS WRONG OUTPUT');
        
        Test.stopTest();
    }
}