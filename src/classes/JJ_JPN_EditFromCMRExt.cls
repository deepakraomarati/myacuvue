/** 
    * File Name: JJ_JPN_EditFromCMRExt
    * Description : Edit for Customer Master Regisrtion after it is created from Edit/Delete functinoality.
    * Copyright : Johnson & Johnson
    * @author : Shiva Shankar | sshank10@its.jnj.com | bnshiva.shankar@cognizant.com
    * 
    * Modification Log 
    * =============================================================== 
    *    Ver  |Date         |Author                |Modification
    *    1.0  |19-Oct-2016  |@sshank10@its.jnj.com |New Class created
    */ 
    
public class JJ_JPN_EditFromCMRExt
{
     public JJ_JPN_CustomerMasterRequest__c CMR {get;set;}
     
     public String statusCode {get;set;}
     
     //public String PersonInchangeName {get;set;}
     
     public Id cmrUrlId;
     
     public JJ_JPN_CustomerMasterRequest__c CMRData {get;set;}
     
     public JJ_JPN_EditFromCMRExt(ApexPages.StandardController controller) 
     {
         CMR = new JJ_JPN_CustomerMasterRequest__c();
         
         try{
                    cmrUrlId = apexpages.currentpage().getparameters().get('cmrId');
            }
            catch(Exception e) 
            {
               if(cmrUrlId==null || cmrUrlId=='') 
               {
                   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a valid URL'));
                   return;
               }
           }
         
         autoPopulateAccValues();
         //getstatusCodeValues();
     }
     
     public List<SelectOption> getstatusCodeValues() {
            List<SelectOption> statusCodeOptions = new List<SelectOption>();
            statusCodeOptions.add(new SelectOption('','-None-'));
            statusCodeOptions.add(new SelectOption('C','C'));
            statusCodeOptions.add(new SelectOption('H','H'));
            statusCodeOptions.add(new SelectOption('D','D'));
            return statusCodeOptions;
     }
     
     public void autoPopulateAccValues(){
         try{
             
             CMRData = [SELECT id , Name,JJ_JPN_Description__c,JJ_JPN_StatusCodeRD__c,JJ_JPN_StatusCode__c,JJ_JPN_SoldToCode__c,JJ_JPN_SoldToNameKana__c,JJ_JPN_SoldToNameKanji__c,JJ_JPN_SubCustomerGroup__c,JJ_JPN_Person_Incharge_NameSAP__c,Account_Activation_Start_Date__c,JJ_JPN_CreditPersonInchargeName__c,JJ_JPN_PayerCode__c,JJ_JPN_PayerNameKanji__c,JJ_JPN_BillToCode__c,JJ_JPN_OfficeName__c,JJ_JPN_BillToNameKanji__c,JJ_JPN_SoldToPostalCode__c,JJ_JPN_SoldToState__c,JJ_JPN_SoldToTownshipCity__c,JJ_JPN_SoldToStreet__c,JJ_JPN_SoldToOtherAddress__c,JJ_JPN_SoldToTelephone__c,JJ_JPN_SoldToFax__c,JJ_JPN_PaymentCondition__c,JJ_JPN_OrderBlock__c,JJ_JPN_ReturnFAXNo__c,JJ_JPN_SalesItem__c,JJ_JPN_SAMDESC__c,JJ_JPN_ADSHandlingFlag__c,JJ_JPN_ShopName__c,JJ_JPN_PaymentMethod__c,JJ_JPN_CustomerRTNFAXHowManyTimesToSend__c,JJ_JPN_DirectDelivery__c,JJ_JPN_BillToRTNFAX__c,JJ_JPN_SoldToNumberOfPagesReceived__c,JJ_JPN_TransactionDetailSubmission__c,JJ_JPN_BillingSummaryTableSubmission__c,JJ_JPN_ItemizedBilling__c,JJ_JPN_BillSubmission__c,JJ_JPN_LotNumberCommunicationDocument__c,JJ_JPN_DeliveryDocumentNote__c,JJ_JPN_EDIStoreCode__c,JJ_JPN_EDIFlag__c,JJ_JPN_FAXOrder__c,JJ_JPN_CustomerOrderNoRequired__c,JJ_JPN_CustomerOrderNoNumberofDigits__c,JJ_JPN_DeadlineTime__c,JJ_JPN_ReturnedGoodsOrderNoInDigits__c,JJ_JPN_NoDeliveryCharges__c,JJ_JPN_FestivalDelivery__c,JJ_JPN_IndividualDeliveryCommentsMonday__c,JJ_JPN_IndividualDeliveryCommentsTuesday__c,JJ_JPN_IndividualDeliveryCommentsWed__c,JJ_JPN_IndividualDeliveryCommentsThurs__c,JJ_JPN_IndividualDeliveryCommentsFriday__c,JJ_JPN_IndividualDeliveryCommentsSat__c,JJ_JPN_IndividualDeliveryCommentsSun__c,JJ_JPN_CautionMemoCustomerInformation__c,JJ_JPN_IndividualDeliveryPossibleMonday__c,JJ_JPN_IndividualDeliveryPossibleTues__c,JJ_JPN_IndividualDeliveryPossibleDateWed__c,JJ_JPN_PIndividualDeliveryPossibleThur__c,JJ_JPN_IndividualDeliveryPossibleFriday__c,JJ_JPN_IndividualDeliveryPossibleSat__c,JJ_JPN_IndividualDeliveryPossibleDateSun__c  FROM JJ_JPN_CustomerMasterRequest__c where id=:cmrUrlId];
             CMRData.JJ_JPN_StatusCodeRD__c = CMRData.JJ_JPN_StatusCodeRD__c;
         }
         catch(Exception e)
         {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Record Has no values to autopopulate or Invalid Id')); 
         }
     }
        
     public PageReference doSave()
     {
        try{
                 Database.update(CMRData,true);
           }
           catch(exception e)
           {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
               return null;
           }
                
          Pagereference p = new PageReference('/apex/CMRDetailPage?id='+CMRData.Id);
          p.setredirect(true);
          return p;
     }
     
     public PageReference Cancel() {
            //Pagereference p = new PageReference('/apex/EditDeleteAccount?accID='+accountUrlId);
            Pagereference p = new PageReference('/'+cmrUrlId);
            p.setredirect(true);
            return p;
       }    
     
}