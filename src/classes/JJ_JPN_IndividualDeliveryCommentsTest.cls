@isTest()
public class JJ_JPN_IndividualDeliveryCommentsTest{
    public static TestMethod void commentsValidate(){
        Account acc=new Account();
        acc.Name='市　英太郎';
        acc.JJ_JPN_IndividualDeliveryComments__c='Mon:7jfjf8;Tue:78r9;Wed:990jkf;Thur:4468;Fri:657;Sat:%^er;Sun:%^&'; 
        insert acc;
        system.assertEquals(acc.Name,'市　英太郎','success');
    }     
}