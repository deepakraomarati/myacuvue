public class JJ_JPN_PrimaryAreaPlan
{
    public void populateAreaPlan(List<JJ_JPN_ActionPlanforAP__c> actionPlanAP)
    {
         Set<ID> primaryAccountPlanIdSet = new Set<ID>();
         Set<ID> ownerID = new Set<ID>();
         Set<String> yearOfActionPlanAp = new Set<String>();
         Map<String,ID> mapOwnerYear = new Map<String,ID>();
        
         for(JJ_JPN_ActionPlanforAP__c actPlanAP : actionPlanAP)
         {        
             primaryAccountPlanIdSet.add(actPlanAP.JJ_JPN_ActionPlanforAP__c);
             //ownerID.add(actPlanAP.JJ_JPN_OwnerID__c);
             yearOfActionPlanAp.add(actPlanAP.JJ_JPN_Year__c);    
             //    mapOwner.put();        
         }
         system.debug('$$$ownerID==>'+ownerID);
           
         ID AreaPlanID;
          
          List<JJ_JPN_PrimaryAreaPlan__c> listOfPrimaryAreaPlan = [SELECT id , name,OwnerID, JJ_JPN_Year__c from JJ_JPN_PrimaryAreaPlan__c where OwnerID =:UserInfo.getUserid() AND JJ_JPN_Year__c IN : yearOfActionPlanAp];
          
          
          for(JJ_JPN_PrimaryAreaPlan__c AreaPlan : listOfPrimaryAreaPlan )
          {
              //AreaPlanID = AreaPlan.ID;
              mapOwnerYear.put(AreaPlan.OwnerID+AreaPlan.JJ_JPN_Year__c,AreaPlan.id);
          }
          
          for(JJ_JPN_ActionPlanforAP__c actPlanAP1 : actionPlanAP)
          {
            //actPlanAP1.JJ_JPN_ActionPlanforAP__c = AreaPlanID;
            actPlanAP1.JJ_JPN_ActionPlanforAP__c = mapOwnerYear.get(UserInfo.getUserid()+actPlanAP1.JJ_JPN_Year__c);         
          }

    }
}