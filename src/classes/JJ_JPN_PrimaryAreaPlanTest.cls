@IsTest
public class JJ_JPN_PrimaryAreaPlanTest
{
    static testMethod void primaryAreaPlan ()
    {
        
        User user= [ select id ,name from User where Profile.Name='System Administrator' and isactive = True limit 1 ];
        System.runAs(user){     
            
            //  Account record Creation
            Account accnt= new Account (Name='Test Account',OutletNumber__c='1000');
            insert accnt;
            
            // Primary Area Plan Record Creation 
            JJ_JPN_PrimaryAreaPlan__c primaryAreaPlans= new JJ_JPN_PrimaryAreaPlan__c ( Name='Test Area Plan',JJ_JPN_Year__c='2017',ownerId=user.Id);
            insert primaryAreaPlans;
            Id accntId=accnt.id;
            
            //Record creation for Primary Account Plan
            JJ_JPN_PrimaryAccountPlan__c primaryAccountPlan = new JJ_JPN_PrimaryAccountPlan__c (Name='Testing P Area Plan', JJ_JPN_Year__c='2017');
            primaryAccountPlan.JJ_JPN_Outlet__c = accntId;
            insert primaryAccountPlan;
            system.assertEquals(primaryAccountPlan.JJ_JPN_Outlet__c,accntId,'success');
            
            //Record creation for Action Plan for AP
            List<JJ_JPN_ActionPlanforAP__c> ListofActionPlanforAP= new List<JJ_JPN_ActionPlanforAP__c>();
            for(Integer i=0;i<=5;i++){
                JJ_JPN_ActionPlanforAP__c ActionPlanforAP = new JJ_JPN_ActionPlanforAP__c (Name='Action Plan for AP 1'+i);
                ActionPlanforAP.JJ_JPN_Outlet__c = accntId;
                ActionPlanforAP.JJ_JPN_Owner__c= user.Id;
                ActionPlanforAP.JJ_JPN_PrimaryAccountPlan__c= primaryAccountPlan.Id;
                ActionPlanforAP.JJ_JPN_ActionPlanforAP__c= primaryAreaPlans.Id;            
                ListofActionPlanforAP.add(ActionPlanforAP);
            }
            insert ListofActionPlanforAP;
            JJ_JPN_PrimaryAreaPlan PArPl= new JJ_JPN_PrimaryAreaPlan();
            PArPl.populateAreaPlan(ListofActionPlanforAP);
        }
    }
    
}