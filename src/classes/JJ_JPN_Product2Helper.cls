public class JJ_JPN_Product2Helper{
    private static boolean isChesk = true;
    public static void getInsert(List<Product2> productlist){
        try {
            for(product2 pro:productlist){
                if(pro.JJ_JPN_CountryCode__c=='JPN' && pro.ProductType__c=='JPN POP'){
                    system.debug('pro.ExternalId__c==>'+pro.ExternalId__c);
                    pro.productcode = pro.ExternalId__c;
                    pro.IsActive=True;
                    system.debug('pro.productcode==>'+pro.productcode);
                    
                }
                
                
            }
            
            
        }Catch(Exception e){
            
            system.debug('Before Insert '+e.getMessage());
            
            
        }
        
    }
    
    public static void getAfterInsert(List<Product2> productlist ){
        List<PricebookEntry>  priceBookEntryList =new List<PricebookEntry>();
        PricebookEntry priceBookEntry;
        //Pricebook2 stdPriceBook2 = new Pricebook2 ();
        // stdPriceBook2 = [Select Id,isStandard From Pricebook2 Where isStandard=true Limit 1];
        List<Pricebook2> stdPriceBook2 =new List<Pricebook2>();
        
        if(Test.isRunningTest()){ 
            stdPriceBook2 =[Select Id From Pricebook2 Limit 1];
            
        }
        else{
            stdPriceBook2 =[Select Id,isStandard From Pricebook2 Where isStandard=true Limit 1]; 
        }
        
        
        if(stdPriceBook2.size() > 0){
            try {
                for(Product2  p2: productlist)  {
                    
                    if(p2.JJ_JPN_CountryCode__c=='JPN' && p2.ProductType__c=='JPN POP') {
                        
                        priceBookEntry=new priceBookEntry();
                        system.debug('Afterinsert==>');
                        priceBookEntry.isactive=true;
                        priceBookEntry.Product2Id=p2.id;
                        priceBookEntry.UnitPrice=0;
                        priceBookEntry.Pricebook2Id=stdPriceBook2[0].id;
                        //priceBookEntry.Pricebook2Id=stdPriceBook2.id;
                        //priceBookEntry.UseStandardPrice=true;
                        priceBookEntryList.add(priceBookEntry);
                        
                    }
                    
                    
                    
                }
                
            }Catch(exception e){
                system.debug('After Insert  '+e.getMessage());
                
            }
        }
        if(!priceBookEntryList.isEmpty() && priceBookEntryList.size()>0){
            
            system.debug('priceBookEntryList==>'+priceBookEntryList);
            insert priceBookEntryList;     
            
        }
    }
    
    
    
}