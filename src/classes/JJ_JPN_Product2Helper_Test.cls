@IsTest
public class JJ_JPN_Product2Helper_Test {
    
    static testMethod void JJ_JPN_Product2Helper_Test(){
        try{
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                              EmailEncodingKey='UTF-8', LastName='Havish', LanguageLocaleKey='en_US',
                              LocaleSidKey='en_US', ProfileId = p.Id,
                              TimeZoneSidKey='America/Los_Angeles', UserName='havish@abc.com',Unique_User_Id__c = 'ABCnr');
            insert u;
            system.runas(u){
                
                List<product2> proList = new List<product2>();
                List<Pricebook2> priceList = new List<Pricebook2>();
                product2 pro;
                Pricebook2 price;
                Id pricebookId = Test.getStandardPricebookId();
                for(Integer i=0;i<3;i++) {
                    price= new Pricebook2();
                    price.Name='Product Test'+i;
                    price.isActive=true; 
                    priceList.add(price);
                }
                
                insert priceList;
                for(Integer i=0;i<3;i++) {
                    pro= new product2();
                    pro.Name='Product Test'+i;
                    pro.JJ_JPN_CountryCode__c='JPN';
                    pro.ExternalId__c='TEST001'+i;
                    pro.ProductType__c='JPN POP';
                    proList.add(pro);
                }
                
                insert proList;
                system.assertEquals(proList.size(),3,'success');
                JJ_JPN_Product2Helper.getAfterInsert(proList);
            }
            
        }catch(Exception e){
        }           
    }
}