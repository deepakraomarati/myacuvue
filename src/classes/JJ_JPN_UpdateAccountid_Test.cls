@IsTest
public class JJ_JPN_UpdateAccountid_Test { 
    static testMethod void JJ_JPN_UpdateAccountid_Test(){
        
        TriggerHandler__c triggerHandler = new TriggerHandler__c(Name = 'HandleTriggers' ,
                                                                 JJ_JPN_ContactCreationOnAccount__c = false);
        insert triggerHandler;
        try{
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                              EmailEncodingKey='UTF-8', LastName='Havish', LanguageLocaleKey='en_US',
                              LocaleSidKey='en_US', ProfileId = p.Id,
                              TimeZoneSidKey='America/Los_Angeles', UserName='havish@abc.com',Unique_User_Id__c = 'ABCnr');
            insert u;
            system.runas(u){
                Account testAccount1 = New Account(Name ='Test Account',PublicAddress__c='Bangalore', SalesRep__c = 'ABC', AccountNumber = '12345', OutletNumber__c = '12345', CountryCode__c = 'JPN');
                insert testAccount1;
                
                JJ_JPN_Price_Information__c PI= new JJ_JPN_Price_Information__c();
                pi.JJ_JPN_Account_Name__c= testAccount1.id;
                pi.CurrencyIsoCode='USD';                
                insert PI;
                system.assertEquals(pi.JJ_JPN_Account_Name__c,testAccount1.id,'succcess');
            }            
        }catch(Exception e){
        }           
    }
}