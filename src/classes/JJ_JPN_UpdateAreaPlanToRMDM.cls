/** 
 * File Name: JJ_JPN_UpdateAreaPlanToRMDM
 * Helper class for this Trigger : JJ_JPN_AreaPlanTriger 
 * Description : Business Logic applied for certain functionalities for Action Plan object.
 * Copyright : Johnson & Johnson
 * @author : Shiva Shankar | sshank10@its.jnj.com | bnshiva.shankar@cognizant.com
 * 
 * Modification Log 
 * =============================================================== 
 *    Ver  |Date         |Author                |Modification
 *    1.0  |25-Jul-2015  |@sshank10@its.jnj.com |New Class created
 *    
 */ 
 
public class JJ_JPN_UpdateAreaPlanToRMDM
{
    
    public static void populateTotal(List<JJ_JPN_AreaPlan__c> areaPlans)
    {

        Set<Id> managerNameset = new Set<Id>();
        Set<String>priAreaPlanYear = new Set<String>();
        map<id,List<JJ_JPN_AreaPlan__c>> AllareaPlanmap = new map<id,List<JJ_JPN_AreaPlan__c>>();
        List<JJ_JPN_AreaPlan__c> allAreaPlanUpdValues = new List<JJ_JPN_AreaPlan__c>();
        
        for(JJ_JPN_AreaPlan__c newareaplan : areaPlans){
            managerNameset.add(newareaplan.Manager__c);
            priAreaPlanYear.add(newareaplan.JJ_JPN_Year__c);
            if(AllareaPlanmap.containsKey(newareaplan.Manager__c)) {
               AllareaPlanmap.get(newareaplan.Manager__c).add(newareaplan);
            }
            else {
                List<JJ_JPN_AreaPlan__c> areaPlanList = new List<JJ_JPN_AreaPlan__c>();
                areaPlanList.add(newareaplan);  // adding one action plan values
                AllareaPlanmap.put(newareaplan.Manager__c,areaPlanList);
           }
            system.debug('newareaplan.Manager__c................'+newareaplan.Manager__c);            
        }
        
        for(JJ_JPN_AreaPlan__c allareaPlanupdate : [SELECT id,name,JJ_JPN_UpsideFSPartner__c,JJ_JPN_UpsideFSRegular__c,JJ_JPN_UpsideFSOther__c,
                                    JJ_JPN_UpsideFSDealer__c,JJ_JPN_UpsideSAMDealer__c,JJ_JPN_UpsideSAMOther__c,JJ_JPN_UpsideSAMPartner__c,
                                    JJ_JPN_UpsideSAMRegular__c,Manager__c,JJ_JPN_AreaPlanName__c,JJ_JPN_PrimaryAreaPlan__r.JJ_JPN_Year__c,
                                    JJ_JPN_PrimaryAreaPlan__c,JJ_JPN_PrimaryAreaPlan__r.OwnerId,JJ_JPN_Year__c from JJ_JPN_AreaPlan__c 
                                    where JJ_JPN_PrimaryAreaPlan__r.OwnerId IN: managerNameset AND  JJ_JPN_Year__c IN: priAreaPlanYear])
        {
            for(JJ_JPN_AreaPlan__c oldarepaln : AllareaPlanmap.get(allareaPlanupdate.JJ_JPN_PrimaryAreaPlan__r.OwnerId)){
                if(allareaPlanupdate.JJ_JPN_AreaPlanName__c == oldarepaln.JJ_JPN_AreaPlanName__c){
                    allareaPlanupdate.JJ_JPN_UpsideFSPartner__c = string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideFSPartner__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideFSPartner__c));
                    allareaPlanupdate.JJ_JPN_UpsideFSRegular__c =  string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideFSRegular__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideFSRegular__c));
                    allareaPlanupdate.JJ_JPN_UpsideFSOther__c = string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideFSOther__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideFSOther__c));
                    allareaPlanupdate.JJ_JPN_UpsideFSDealer__c = string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideFSDealer__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideFSDealer__c));
                    allareaPlanupdate.JJ_JPN_UpsideSAMPartner__c = string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideSAMPartner__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideSAMPartner__c));
                    allareaPlanupdate.JJ_JPN_UpsideSAMRegular__c = string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideSAMRegular__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideSAMRegular__c));
                    allareaPlanupdate.JJ_JPN_UpsideSAMOther__c = string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideSAMOther__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideSAMOther__c));
                    allareaPlanupdate.JJ_JPN_UpsideSAMDealer__c = string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideSAMDealer__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideSAMDealer__c));                           
                }
                allAreaPlanUpdValues.add(allareaPlanupdate);
            }                            
        
        }
        if(!allAreaPlanUpdValues.IsEmpty()){
            Database.Update(allAreaPlanUpdValues,true);
        }
    }
 
    public static void DescreaseTotal(List<JJ_JPN_AreaPlan__c> areaPlans)
    {
        //JJ_JPN_UtilityClassRecursion.stoprecursionbefore = true;
        Set<Id> managerNameset = new Set<Id>();
        Set<Id> primaryareaplanset = new Set<Id>();
        Set<String>priAreaPlanYear = new Set<String>();
        map<id,List<JJ_JPN_AreaPlan__c>> AllareaPlanmap = new map<id,List<JJ_JPN_AreaPlan__c>>();
        List<JJ_JPN_AreaPlan__c> allAreaPlanUpdValues2 = new List<JJ_JPN_AreaPlan__c>();        
        //List<JJ_JPN_AreaPlan__c> areaPlans =[SELECT Id,Manager__c,JJ_JPN_PrimaryAreaPlan__c FROM JJ_JPN_AreaPlan__c WHERE Id IN:aId];
        for(JJ_JPN_AreaPlan__c newareaplan : areaPlans){
            SYSTEM.DEBUG('newareaplan.........'+newareaplan.Id);
            managerNameset.add(newareaplan.Manager__c);
            priAreaPlanYear.add(newareaplan.JJ_JPN_Year__c);
            primaryareaplanset.add(newareaplan.JJ_JPN_PrimaryAreaPlan__c);            
        }
        system.debug('managerNameset-->'+managerNameset);
        for(JJ_JPN_AreaPlan__c allareaPlanupdate : [SELECT id,name,JJ_JPN_UpsideFSPartner__c,JJ_JPN_UpsideFSRegular__c,
                        JJ_JPN_UpsideFSOther__c,JJ_JPN_UpsideFSDealer__c,JJ_JPN_UpsideSAMDealer__c,JJ_JPN_UpsideSAMOther__c,
                        JJ_JPN_UpsideSAMPartner__c,JJ_JPN_UpsideSAMRegular__c,Manager__c,JJ_JPN_AreaPlanName__c,
                        JJ_JPN_PrimaryAreaPlan__r.JJ_JPN_Year__c,JJ_JPN_PrimaryAreaPlan__c,
                        JJ_JPN_PrimaryAreaPlan__r.OwnerId,JJ_JPN_Year__c from JJ_JPN_AreaPlan__c where Manager__c IN: managerNameset AND JJ_JPN_Year__c IN: priAreaPlanYear]){
                        system.debug('JJ_JPN_UpsideFSPartner__c-->'+allareaPlanupdate.JJ_JPN_UpsideFSPartner__c);
                        system.debug('Id-->'+allareaPlanupdate.id);
           if(AllareaPlanmap.containsKey(allareaPlanupdate.Manager__c)) {
               AllareaPlanmap.get(allareaPlanupdate.Manager__c).add(allareaPlanupdate);
               for(JJ_JPN_AreaPlan__c l : AllareaPlanmap.get(allareaPlanupdate.Manager__c)){
                   system.debug('AllareaPlanmap.get(allareaPlanupdate.Manager__c).............'+l.JJ_JPN_UpsideFSPartner__c);
               }
           }
           else {
            List<JJ_JPN_AreaPlan__c> areaPlanList = new List<JJ_JPN_AreaPlan__c>();
            areaPlanList.add(allareaPlanupdate);  // adding one action plan values
            AllareaPlanmap.put(allareaPlanupdate.Manager__c,areaPlanList);
               for(JJ_JPN_AreaPlan__c l : areaPlanList ){
                   system.debug('AllareaPlanmap.get(allareaPlanupdate.Manager__c).............'+l.JJ_JPN_UpsideFSPartner__c);
               }
           }                
        }
        
        for(JJ_JPN_AreaPlan__c allareaPlanupdate : [SELECT id,JJ_JPN_AreaPlanName__c,Manager__c,JJ_JPN_UpsideFSPartner__c,
                JJ_JPN_UpsideFSRegular__c,JJ_JPN_UpsideFSDealer__c,JJ_JPN_UpsideSAMDealer__c,JJ_JPN_UpsideSAMOther__c,
                JJ_JPN_UpsideFSOther__c,JJ_JPN_UpsideSAMPartner__c,JJ_JPN_UpsideSAMRegular__c,JJ_JPN_PrimaryAreaPlan__r.JJ_JPN_Year__c,
                JJ_JPN_PrimaryAreaPlan__c,JJ_JPN_PrimaryAreaPlan__r.OwnerId,JJ_JPN_Year__c from JJ_JPN_AreaPlan__c 
                where JJ_JPN_PrimaryAreaPlan__r.OwnerId IN: managerNameset AND JJ_JPN_Year__c IN: priAreaPlanYear]){
                allareaPlanupdate.JJ_JPN_UpsideFSPartner__c = '0';
                allareaPlanupdate.JJ_JPN_UpsideFSRegular__c = '0';
                allareaPlanupdate.JJ_JPN_UpsideFSOther__c = '0';
                allareaPlanupdate.JJ_JPN_UpsideFSDealer__c = '0';
                allareaPlanupdate.JJ_JPN_UpsideSAMPartner__c = '0';
                allareaPlanupdate.JJ_JPN_UpsideSAMRegular__c = '0';
                allareaPlanupdate.JJ_JPN_UpsideSAMOther__c = '0';
                allareaPlanupdate.JJ_JPN_UpsideSAMDealer__c = '0';
                system.debug('allareaPlanupdate ................'+allareaPlanupdate.Id);
                
                system.debug('$$$Owner==>'+AllareaPlanmap.get(allareaPlanupdate.JJ_JPN_PrimaryAreaPlan__r.OwnerId));
                
            for(JJ_JPN_AreaPlan__c oldarepaln : AllareaPlanmap.get(allareaPlanupdate.JJ_JPN_PrimaryAreaPlan__r.OwnerId)){
                system.debug('$$$Second==>');
                system.debug('$$$Owner==>'+AllareaPlanmap.get(allareaPlanupdate.JJ_JPN_PrimaryAreaPlan__r.OwnerId));
                
                if(allareaPlanupdate.JJ_JPN_AreaPlanName__c == oldarepaln.JJ_JPN_AreaPlanName__c){
                        system.debug('$$$Third==>');
                    system.debug('allareaPlanupdate.JJ_JPN_UpsideFSPartner__c.................'+allareaPlanupdate.JJ_JPN_UpsideFSPartner__c); 
                    system.debug('string.valueof................'+string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideFSPartner__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideFSPartner__c)));  
                    system.debug('string.valueof 1................'+string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideFSPartner__c)));  
                    system.debug('string.valueof 2................'+string.valueof(Integer.valueof(oldarepaln.JJ_JPN_UpsideFSPartner__c)));  
                    
                      
                    allareaPlanupdate.JJ_JPN_UpsideFSPartner__c = string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideFSPartner__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideFSPartner__c));
                    allareaPlanupdate.JJ_JPN_UpsideFSRegular__c =  string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideFSRegular__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideFSRegular__c));
                    allareaPlanupdate.JJ_JPN_UpsideFSOther__c = string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideFSOther__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideFSOther__c));
                    allareaPlanupdate.JJ_JPN_UpsideFSDealer__c = string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideFSDealer__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideFSDealer__c));
                    allareaPlanupdate.JJ_JPN_UpsideSAMPartner__c = string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideSAMPartner__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideSAMPartner__c));
                    allareaPlanupdate.JJ_JPN_UpsideSAMRegular__c = string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideSAMRegular__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideSAMRegular__c));
                    allareaPlanupdate.JJ_JPN_UpsideSAMOther__c = string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideSAMOther__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideSAMOther__c));
                    allareaPlanupdate.JJ_JPN_UpsideSAMDealer__c = string.valueof(Integer.valueof(allareaPlanupdate.JJ_JPN_UpsideSAMDealer__c)+Integer.valueof(oldarepaln.JJ_JPN_UpsideSAMDealer__c));                           
                }
                
            }
            allAreaPlanUpdValues2.add(allareaPlanupdate);        
        
        }
        if(!allAreaPlanUpdValues2.IsEmpty()){
            system.debug('allAreaPlanUpdValues2..............'+allAreaPlanUpdValues2);
                        for(JJ_JPN_AreaPlan__c a: allAreaPlanUpdValues2){
                system.debug('allAreaPlanUpdValues2..............'+a.Id);
            }
            Database.Update(allAreaPlanUpdValues2,true);

        }
        

    }
}