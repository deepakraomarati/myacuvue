/** 
* File Name: JJ_JPN_UpdateAreaPlanToRMDMTest
* Description : Business Logic applied for certain functionalities for Action Plan object.
* Main Class : JJ_JPN_UpdateAreaPlanToRMDM
* Trigger : JJJ_JPN_AreaPlanTriger
* Copyright : Johnson & Johnson
* @author : Shiva Shankar | sshank10@its.jnj.com | bnshiva.shankar@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |25-Jul-2015  |@sshank10@its.jnj.com |New Class created
*    
*/ 
@IsTest
public class JJ_JPN_UpdateAreaPlanToRMDMTest
{
    static testMethod void AreaPlanToRMDM()
    {
        Test.startTest(); 
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User testUserA = new User(Alias = 'standtp', Email='testpstandard@testorg.com', 
                                  EmailEncodingKey='UTF-8', LastName='Testingp', LanguageLocaleKey='en_US', 
                                  LocaleSidKey='en_US', ProfileId = p.Id, Unique_User_Id__c='testjp',
                                  TimeZoneSidKey='America/Los_Angeles', UserName='standarduserp@testorg.com');
        insert testUserA;
        
        User testUserB = new User(Alias = 'standtp', Email='testpstandard@testorg.com', 
                                  EmailEncodingKey='UTF-8', LastName='Testingp', LanguageLocaleKey='en_US', 
                                  LocaleSidKey='en_US', ProfileId = p.Id, Unique_User_Id__c='testjpA',
                                  TimeZoneSidKey='America/Los_Angeles', UserName='standarduserp@testorgA.com');
        insert testUserB;
        
        testUserA.ManagerID = testUserB.id;
        testUserB.ManagerID = testUserA.id;
        System.runAs(testUserA)
        {
            JJ_JPN_PrimaryAreaPlan__c priAreplan = new JJ_JPN_PrimaryAreaPlan__c();
            priAreplan.Name = 'primary';
            priAreplan.JJ_JPN_Year__c='2017';
            insert priAreplan;
            
            JJ_JPN_AreaPlan__c areapln = new JJ_JPN_AreaPlan__c();
            areapln.JJ_JPN_AreaPlanName__c = 'Jan';
            areapln.JJ_JPN_PrimaryAreaPlan__c = priAreplan.id;
            areapln.JJ_JPN_UpsideFSPartner__c = '10';
            areapln.JJ_JPN_UpsideFSRegular__c = '10';
            areapln.JJ_JPN_UpsideSAMOther__c = '10';
            areapln.JJ_JPN_UpsideFSOther__c = '10';
            areapln.JJ_JPN_UpsideSAMPartner__c = '10';
            areapln.JJ_JPN_UpsideSAMRegular__c = '10';
            insert areapln;
            system.assertEquals(areapln.JJ_JPN_UpsideSAMRegular__c,'10','success');
            
            JJ_JPN_AreaPlan__c updAP= [select Id,Manager__c from JJ_JPN_AreaPlan__c where Id =: areapln.Id];
            
            list<JJ_JPN_AreaPlan__c> listofAllAp = new list<JJ_JPN_AreaPlan__c>([Select JJ_JPN_AreaPlanName__c,Manager__c,JJ_JPN_UpsideFSPartner__c,JJ_JPN_UpsideFSRegular__c,JJ_JPN_UpsideFSDealer__c,JJ_JPN_UpsideSAMDealer__c,JJ_JPN_UpsideSAMOther__c,JJ_JPN_UpsideFSOther__c,JJ_JPN_UpsideSAMPartner__c,JJ_JPN_UpsideSAMRegular__c,JJ_JPN_PrimaryAreaPlan__r.JJ_JPN_Year__c,JJ_JPN_PrimaryAreaPlan__c,JJ_JPN_PrimaryAreaPlan__r.OwnerId from JJ_JPN_AreaPlan__c where JJ_JPN_PrimaryAreaPlan__c =:priAreplan.id]);
            List<JJ_JPN_AreaPlan__c> listAreaPlan = new List<JJ_JPN_AreaPlan__c>();
            listAreaPlan.add(areapln);
            JJ_JPN_UpdateAreaPlanToRMDM.populateTotal(listAreaPlan);
        }       
        Test.stopTest();
    }
}