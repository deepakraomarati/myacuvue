Global class JJ_JPN_UpdateCustomerStatus{
    Webservice static void updatestatus(id customerid){
        system.debug('customerid.........'+customerid);
        List<JJ_JPN_CustomerMasterRequest__c> custstatus = [SELECT JJ_JPN_ApprovalStatus__c FROM JJ_JPN_CustomerMasterRequest__c WHERE id =:customerid];
        custstatus[0].JJ_JPN_ApprovalStatus__c = 'Closed';
        Update custstatus;        
    }
}