@isTest
private class JJ_JPN_UpdateCustomerStatusTest{   
    @isTest Static void testMethod_UpdateCustomerStatusTest(){
        test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User testuser = new User(Alias = 'trep10', Email='testUsersrep10@gmail.com', 
                                 EmailEncodingKey='UTF-8', LastName='Testingrep10', LanguageLocaleKey='en_US', 
                                 LocaleSidKey='en_US', ProfileId = p.Id,Unique_User_Id__c='TestUserRep', 
                                 TimeZoneSidKey='America/Los_Angeles', UserName='testreps10@gmail.com');
        insert testuser;
        System.runAs(testuser){ 
            JJ_JPN_CustomerMasterRequest__c cmrRecord=new JJ_JPN_CustomerMasterRequest__c(Name='Test',JJ_JPN_PayerCode__c='47583',JJ_JPN_AccountType__c='Field Sales(FS)',JJ_JPN_PayerNameKanji__c='漢字',
                                                                                          JJ_JPN_PayerNameKana__c='ｶﾝｼﾞ',JJ_JPN_PayerPostalCode__c='4758312',JJ_JPN_RegistrationFor__c='435353',JJ_JPN_OfficeName__c='sdcevfe',
                                                                                          JJ_JPN_PersonInchargeName__c='sfdcvcfd',JJ_JPN_SalesItem__c='R',JJ_JPN_Rank__c='A2',JJ_JPN_CreditPersonInchargeName__c='ksjnvf',
                                                                                          JJ_JPN_PersonInchargeCode__c='93284',JJ_JPN_StatusCode__c='H',JJ_JPN_RepresentativeNameKana__c='scdscds',JJ_JPN_RepresentativeNameKanji__c='bvsf',
                                                                                          JJ_JPN_PaymentCondition__c='ZJ47',JJ_JPN_PaymentMethod__c='D',JJ_JPN_ReturnFAXNo__c='9483579');
            cmrRecord.JJ_JPN_ApprovalStatus__c='Draft';
            insert cmrRecord;
            system.assertEquals(cmrRecord.JJ_JPN_ApprovalStatus__c,'Draft','success');
            JJ_JPN_UpdateCustomerStatus updateCustomerStatus= new JJ_JPN_UpdateCustomerStatus();
            JJ_JPN_UpdateCustomerStatus.updatestatus(cmrRecord.Id);
        } 
        test.stopTest();   
    }
}