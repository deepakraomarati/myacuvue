@isTest
public class JJ_JPN_UpdateProductFieldTest
{  
    static testMethod void UpdateProductFieldwithAttachmentIdTest() 
    {
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Attachment attach = new Attachment();     
        attach.Name = 'Unit Test Attachment';
        Blob bodyBlob = Blob.valueOf('Test Attachment');
        attach.body = bodyBlob;
        attach.parentId =prod.id; 
        insert attach;
        system.assertEquals(attach.Name,'Unit Test Attachment','success');        
        delete attach;
    }
}