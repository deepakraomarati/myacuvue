@isTest(seeAllData=false)
public class JJ_JPN_addressValidationonAccountTest{
    public static TestMethod void test_method_addressValidate(){
        
        Account Acc = new Account();
        acc.countrycode__c='JPN';
        acc.Address1__c='TestAddress1';
        acc.Address2__c='TestAddress2';
        acc.Address3__c='TestAddress3 TestAddress4';
        acc.Name='Test';
        Insert acc;
        system.assertEquals(acc.Name,'Test','success');
    }
    public static TestMethod void test_method_addressValidate1(){
        
        Account Acc = new Account();
        acc.countrycode__c='JPN';
        acc.Address1__c='TestAddress1';
        acc.Address2__c='TestAddress2';
        acc.Address3__c='TestAddress3 TestAddress4';
        acc.Address3__c='TestAddress3　TestAddress4';
        acc.Name='Test2';
        Insert acc;
        system.assertEquals(acc.Name,'Test2','success');
    }
    
    public static TestMethod void test_method_addressValidate3(){
        
        Account Acc = new Account();
        acc.countrycode__c='JPN';
        acc.Address1__c='';
        acc.Address2__c='TestAddress2';
        acc.Address3__c='TestAddress3 TestAddress4';
        acc.Name='Test6';
        Insert acc;
        system.assertEquals(acc.Name,'Test6','success');
    }
    public static TestMethod void test_method_addressValidate4(){
        
        Account Acc = new Account();
        acc.countrycode__c='JPN';
        acc.Address1__c='';
        acc.Address2__c='TestAddress2';
        acc.Address3__c='TestAddress3 TestAddress4';
        acc.Address3__c='TestAddress3　TestAddress4';
        acc.Name='Test5';
        Insert acc;
        system.assertEquals(acc.Name,'Test5','success');
    }
    
    public static TestMethod void test_method_subCustomerGroupValidate1(){
        
        Account Acc = new Account();
        acc.countrycode__c='JPN';
        acc.JJ_JPN_SubCustomerGroup__c ='SCG001465 Nihon Optical';        
        acc.Name='Test3';
        Insert acc;
        system.assertEquals(acc.Name,'Test3','success');
    }
}