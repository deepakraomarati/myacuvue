global class JJ_JPN_approvalRecall
{
    webservice static boolean recallApprovalpermasgn(string permsetid, string userid ){
        PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = permsetid, AssigneeId = userid);
        insert psa; 
        return(true); 
    }
    
    webservice static boolean recallApprovalpermdel(string permsetid, string userid ){
        
        string permsetids=permsetid;
        string userids=userid;
        PermissionSetAssignment psa = [select id from PermissionSetAssignment where PermissionSetId=:permsetids and  AssigneeId = :userids];
        //PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = permsetid, AssigneeId = userid);
        delete psa;
        return(true); 
    }
    
    webservice static boolean recallApproval(Id recId)    
    {        
        List<ProcessInstanceWorkitem> piwi = [SELECT Id, ProcessInstanceId, ProcessInstance.TargetObjectId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =: recId];
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setAction('Removed'); 
        if(!piwi.isEmpty()){
            req.setWorkitemId(piwi.get(0).Id);
            Approval.process(req,false);
            return true;
        }else{
            return false;    
        }       
        
    }   
}