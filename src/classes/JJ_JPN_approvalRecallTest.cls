@isTest
@TestVisible private class JJ_JPN_approvalRecallTest{
    @isTest Static void testMethod_ApprovalRecall(){
        User user= [select id from User where Profile.Name='System Administrator' and isActive=True limit 1];
        String userString=user.Id;
        Test.StartTest();   
        JJ_JPN_CustomerMasterRequest__c cmr=new JJ_JPN_CustomerMasterRequest__c(Name='Test',JJ_JPN_PayerCode__c='47583',JJ_JPN_AccountType__c='Field Sales(FS)',JJ_JPN_PayerNameKanji__c='漢字',
                                                                                JJ_JPN_PayerNameKana__c='ｶﾝｼﾞ',JJ_JPN_PayerPostalCode__c='4758312',JJ_JPN_RegistrationFor__c='435353',JJ_JPN_OfficeName__c='sdcevfe',
                                                                                JJ_JPN_PersonInchargeName__c='sfdcvcfd',JJ_JPN_SalesItem__c='R',JJ_JPN_Rank__c='A2',JJ_JPN_CreditPersonInchargeName__c='ksjnvf',
                                                                                JJ_JPN_PersonInchargeCode__c='93284',JJ_JPN_StatusCode__c='H',JJ_JPN_RepresentativeNameKana__c='scdscds',JJ_JPN_RepresentativeNameKanji__c='bvsf',
                                                                                JJ_JPN_PaymentCondition__c='ZJ47',JJ_JPN_PaymentMethod__c='D',JJ_JPN_ReturnFAXNo__c='9483579');
        
        PermissionSet perSetInsert = new PermissionSet();
        perSetInsert.Name='JJ_JPN_Recall_approval_permsetTest';
        perSetInsert.label='JJ_JPN_Recall_approval_permsetTest';
        insert perSetInsert ;
        ID ps= [select id from PermissionSet where PermissionSet.name='JJ_JPN_Recall_approval_permsetTest' limit 1].id;
        String psStr=ps;
        system.assertEquals(psStr,ps,'success');
        JJ_JPN_approvalRecall ApprovalRecall = new JJ_JPN_approvalRecall();
        JJ_JPN_approvalRecall.recallApprovalpermasgn(psStr,userString);
        JJ_JPN_approvalRecall.recallApprovalpermdel(psStr,userString);
        JJ_JPN_approvalRecall.recallApproval(cmr.Id);
        JJ_JPN_Customer_Request_Reject CustomerRequestReject = new JJ_JPN_Customer_Request_Reject();
        JJ_JPN_Customer_Request_Reject.Reject_customer_Request(cmr.Id);
    }   
}