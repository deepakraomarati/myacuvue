/**
* File Name: LMSAuthenticateScheduler
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : batch class to get LMS sesion ID and store in custom setings, later used by other LMS services
* Copyright (c) $2018 Johnson & Johnson
*/
public with sharing class LMSAuthenticateScheduler implements Schedulable, Database.AllowsCallouts, Database.Batchable<sObject>
{
	public void execute(SchedulableContext SC)
	{
		Database.executebatch(new LMSAuthenticateScheduler());
	}
	public Database.QueryLocator start(Database.Batchablecontext BC)
	{
		String query = 'SELECT Id FROM Role__c limit 1';
		return Database.getQueryLocator(query);
	}
	public void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		LMSAuthenticateScheduler.lmsAuthenticate();
	}
	public void finish(Database.BatchableContext info)
	{/*finsh method */
	}
	public static void lmsAuthenticate()
	{
		String sessionId;
		HttpResponse response;
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mockResponseObject = new MockHttpResponseGenerator1();
			response = mockResponseObject.respond1();
		}
		else
		{
			String body = 'username=' + LmsUtils.absorbSettings.get('Username').Value__c + '&password=' + LmsUtils.absorbSettings.get('Password').Value__c + '&privateKey=' + lmsUtils.absorbSettings.get('PrivateKey').Value__c;
			response = LmsUtils.processRequest(LmsUtils.absorbSettings.get('EndPoint').Value__c + 'v1/Authenticate', body, 'POST', '');
		}
		if (response.getStatusCode() == 200)
		{
			sessionId = response.getBody().replace('"', '');
		}
		else
		{
			sessionId = 'Error';
		}
		LMS_Settings__c lmsauthenticationvalue = LMS_Settings__c.getInstance('Token');
		lmsauthenticationvalue.Value__c = sessionId;
		update lmsauthenticationvalue;
	}
}