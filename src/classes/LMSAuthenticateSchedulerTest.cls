/**
* File Name: LMSAuthenticateSchedulerTest
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : Test class for LMSAuthenticateScheduler class
* Copyright (c) $2018 Johnson & Johnson
*/
@isTest
Public class LMSAuthenticateSchedulerTest
{
	Static testMethod void TestLMSMethod()
	{
		LMS_Settings__c LmsCustomSettings1 = new LMS_Settings__c();
		LmsCustomSettings1.Name = 'Username';
		LmsCustomSettings1.Value__c = 'prorestadmin';
		insert LmsCustomSettings1;

		LMS_Settings__c LmsCustomSettings = new LMS_Settings__c();
		LmsCustomSettings.Name = 'EndPoint';
		LmsCustomSettings.Value__c = 'https://jjvus.sandbox.myabsorb.com/api/Rest/';
		insert LmsCustomSettings;

		LMS_Settings__c LmsCustomSettings2 = new LMS_Settings__c();
		LmsCustomSettings2.Name = 'Password';
		LmsCustomSettings2.Value__c = 'Test.123';
		insert LmsCustomSettings2;

		LMS_Settings__c LmsCustomSettings3 = new LMS_Settings__c();
		LmsCustomSettings3.Name = 'PrivateKey';
		LmsCustomSettings3.Value__c = '58091a9c-7077-40ee-927b-143ea1af150c';
		insert LmsCustomSettings3;

		LMS_Settings__c LmsCustomSettings4 = new LMS_Settings__c();
		LmsCustomSettings4.Name = 'Token';
		LmsCustomSettings4.Value__c = 'TEST TOKEN';
		insert LmsCustomSettings4;

		Role__c holdsuserrole = new Role__c();
		holdsuserrole.Name = '医師';
		holdsuserrole.Approval_Required__c = true;
		holdsuserrole.Country__c = 'Japan';
		holdsuserrole.Type__c = 'Staff';
		insert holdsuserrole;

		Test.StartTest();
		LMSAuthenticateScheduler sh1 = new LMSAuthenticateScheduler();
		String sch = '0 0 23 * * ?';
		system.schedule('Test Tags And Categories', sch, sh1);
		Test.stopTest();
		system.assertNotEquals(LMS_Settings__c.getvalues('Token').value__c, null);
	}
}