/**
* File Name: LMSTagsandCategoriesScheduler
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : batch class to get the Tags & categories information from LMS and updates or create in salesforce
* Copyright (c) $2018 Johnson & Johnson
*/
public with sharing class LMSTagsandCategoriesScheduler implements Schedulable, Database.AllowsCallouts, Database.Batchable<sObject>
{
	public void execute(SchedulableContext SC)
	{
		Database.executebatch(new LMSTagsandCategoriesScheduler());
	}
	public Database.QueryLocator start(Database.Batchablecontext BC)
	{
		String query = 'SELECT Id FROM Role__c limit 1';
		return Database.getQueryLocator(query);
	}
	public void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		LMSTagsandCategoriesScheduler.syncTagsCategories();
	}
	public void finish(Database.BatchableContext info)
	{
		//finish
	}
	public static void syncTagsCategories()
	{
		List<Categories_Tag__c> categoriesTagUpdateList = new List<Categories_Tag__c>();
		List<LmsUtils.Categories> categoryList = LmsUtils.getCategories();
		List<LmsUtils.TagIDs> TagList = LmsUtils.getTagIDs();
		for (LmsUtils.Categories holdsCategory : categoryList)
		{
			Categories_Tag__c holdsCategoryTag = new Categories_Tag__c();
			holdsCategoryTag.Type__c = 'Category';
			holdsCategoryTag.Name = holdsCategory.Name;
			holdsCategoryTag.LMS_ID__c = holdsCategory.id;
			holdsCategoryTag.Parent_ID__c = holdsCategory.ParentId;
			holdsCategoryTag.Description__c = holdsCategory.Description;
			categoriesTagUpdateList.add(holdsCategoryTag);
		}
		for (LmsUtils.TagIDS holdsTags : TagList)
		{
			Categories_Tag__c holdsCategoryTag = new Categories_Tag__c();
			holdsCategoryTag.Type__c = 'Tag';
			holdsCategoryTag.LMS_ID__c = holdsTags.id;
			holdsCategoryTag.Name = holdsTags.Name;
			CategoriesTagUpdateList.add(holdsCategoryTag);
		}
		if (!CategoriesTagUpdateList.isEmpty())
		{
			upsert CategoriesTagUpdateList LMS_ID__c;
		}
	}
}