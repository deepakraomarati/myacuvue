/**
* File Name: LMSTagsandCategoriesSchedulerTest
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : Test class for LMSTagsandCategoriesScheduler class
* Copyright (c) $2018 Johnson & Johnson
*/
@isTest
Public class LMSTagsandCategoriesSchedulerTest
{
	Static testMethod void TesttLMSTagsandCategories()
	{
		LMS_Settings__c LmsCustomSettings1 = new LMS_Settings__c();
		LmsCustomSettings1.Name = 'Username';
		LmsCustomSettings1.Value__c = 'prorestadmin';
		insert LmsCustomSettings1;

		LMS_Settings__c LmsCustomSettings = new LMS_Settings__c();
		LmsCustomSettings.Name = 'EndPoint';
		LmsCustomSettings.Value__c = 'https://jjvus.sandbox.myabsorb.com/api/Rest/';
		insert LmsCustomSettings;

		LMS_Settings__c LmsCustomSettings2 = new LMS_Settings__c();
		LmsCustomSettings2.Name = 'Password';
		LmsCustomSettings2.Value__c = 'Test.123';
		insert LmsCustomSettings2;

		LMS_Settings__c LmsCustomSettings3 = new LMS_Settings__c();
		LmsCustomSettings3.Name = 'PrivateKey';
		LmsCustomSettings3.Value__c = '58091a9c-7077-40ee-927b-143ea1af150c';
		insert LmsCustomSettings3;

		LMS_Settings__c LmsCustomSettings4 = new LMS_Settings__c();
		LmsCustomSettings4.Name = 'Token';
		LmsCustomSettings4.Value__c = 'TEST TOKEN';
		insert LmsCustomSettings4;

		Role__c rr = new Role__c();
		rr.Name = '医師';
		rr.Approval_Required__c = true;
		rr.Country__c = 'Japan';
		rr.Type__c = 'Staff';
		insert rr;

		Test.StartTest();
		LMSTagsandCategoriesScheduler sh1 = new LMSTagsandCategoriesScheduler();
		String sch = '0 0 23 * * ?';
		system.schedule('Test Tags And Categories', sch, sh1);
		list<Categories_Tag__c>updateTags = [SELECT Id FROM Categories_Tag__c WHERE lastmodifieddate =: date.today()];
		system.assertNotequals(updateTags,null);
		Test.stopTest();
	}
}