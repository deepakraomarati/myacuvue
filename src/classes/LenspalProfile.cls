@RestResource(urlmapping='/Apex/LenspalProfile/*')
global without sharing class LenspalProfile {

 global static String INVALID_CONTACT  ='ACE0015';
 global static String CONTACT_REQUIRED = 'ACE0016';
 global static String ACCOUNT_NOT      = 'ACE0017';
 global static String PUT_ERRORTYPE    = 'Lenspalupdate';
    
 @HTTPPUT
 global static String doput()
 {
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    Blob body = req.requestBody;
    
    Map<String,Object> m=(Map<String,Object>)Json.DeserializeUntyped(body.toString());
    
    String  ContactId = String.ValueOf(m.get('ContactId'));
    String  Email    = String.ValueOf(m.get('Email'));
    String  OutLetNumber= String.ValueOf(m.get('OutLetNumber'));
    
    return ContactUpdate(ContactId,Email,OutletNumber);
 }
 
 global static String ContactUpdate(String ContactId,String Email,String OutLetNumber)
 {
    
    try{
        Account[] AccExist;
        Contact[] ConExist;
        Id AccId;
        
        if(ContactId ==null || ContactId =='')
        {
            System.debug('CONTACTID ####');
            throw new CustomException(CustomException.getException(CONTACT_REQUIRED,PUT_ERRORTYPE));
        }
        
        ConExist = [SELECT Id,FirstName,AccountId,LastName,Email FROM Contact WHERE Id=:ContactId];
        System.debug('CONEXIST ###'+ConExist.size());
        
        if(OutLetNumber !=null && OutLetNumber!='')
        {
            System.debug('OutLetNumber ###'+OutLetNumber);
            
            AccExist = [SELECT Id,Name,OutletNumber__c FROM Account WHERE OutletNumber__c=:OutLetNumber];
            System.debug('ACCOUNTEXIST ###'+AccExist.size());
            
            if(AccExist.size() ==0 && AccExist.isEmpty())
            {
                throw new CustomException(CustomException.getException(ACCOUNT_NOT,PUT_ERRORTYPE));
            }else{
                 AccId = AccExist[0].Id;
            }
        }
       
       if(ConExist.size() > 0 && !ConExist.isEmpty())
        {
           
            if(AccId !=null)
            {
               Consumer_Relation__c crobj=new Consumer_Relation__c();
               crobj.AccountId__c = AccId;
               crobj.ContactID__c = ConExist[0].Id;
               insert crobj;
            }
            
            if(Email !=null && Email !='')
            {
               ConExist[0].Email = Email;
               update ConExist[0];
            }
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeObjectField('Status','Success');
            gen.writeEndObject();
            String pretty = gen.getAsString();
            return pretty; 
             
        }else{
            throw new CustomException(CustomException.getException(INVALID_CONTACT,PUT_ERRORTYPE));
        }
       
       return null;
    }catch(Exception e)
    {
        system.debug('E ####'+e);
        return e.getMessage();
    }
 }
}