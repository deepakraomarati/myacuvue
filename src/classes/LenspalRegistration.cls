@RestResource(urlmapping='/Apex/LenspalRegistration/*')
global without sharing class LenspalRegistration {
    
    /* --------------------------------------------
     * Author: 
     * Company: BICS
     * Description : User registered Lenpal APP.
     * ---------------------------------------------*/
    
 global static final String GENERIC_ERROR  = 'GEN0001';
 global static final String USER_EXIST     = 'ACE0014';
 global static final String INPUT_REQUIRED = 'ACE0013';
 global static final String POSTERROR_TYPE = 'LenspalRegistration';
 
 @httppost
 global static String dopost()
 {
    RestRequest req=RestContext.request;
    RestResponse res = RestContext.response;
    Blob body = req.requestBody;
    system.debug('BODY ###'+body);
    
    LensDTO ls =LenspalRegistration.parse(body.toString());
    
    return Registration(ls);
    
 }
 
 global static String Registration(LensDTO ls)
 {
    try{
        
        if(ls.FirstName =='' || ls.FirstName ==null)
        {
            throw new CustomException(CustomException.getException(INPUT_REQUIRED,POSTERROR_TYPE));
        }
        
        Date DOB;
        String mydate;
        
        if(ls.DateOfBirth !=null)
        {
            system.debug('DATE ###:'+ls.DateOfBirth);
            DOB = Date.parse(ls.DateOfBirth);
        }
        
       /* List<Contact> lstcon= [SELECT Id,
                                        FirstName,
                                        LastName,
                                        Email,
                                        Phone,
                                        agree_pp__c,
                                        Communication_Agreement__c
                                        FROM Contact 
                                        WHERE LastName=:ls.FirstName];
        
        if(lstcon.size()>0 && !lstcon.isEmpty())
        {
            throw new CustomException(CustomException.getException(USER_EXIST,POSTERROR_TYPE));
        }else{*/
            
            Map<String,Configuration_Setting__c> cs = Configuration_Setting__c.getAll();
            String RecordTypeId =cs.get('Contact_RecordType_Id').Value__c;    
         
            Contact con=new Contact();
            con.LastName = ls.FirstName;
            con.Email    = ls.Email;
            con.agree_pp__c = ls.PrivacyPolicy;
            con.Communication_Agreement__c = ls.CommunicationAgreement;
            con.LensType__c=ls.LensType;
            con.RecordTypeId = RecordTypeId;
            con.Gender__c = ls.Gender;
            con.MailingCountry = ls.Country;
            if(DOB !=null){
               System.debug('MYDATE1###'+mydate);
               con.Birthdate=DOB;
             }
            System.debug('CON ####'+con);   
            insert con;
            
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeObjectField('Status','Success');
            gen.writeObjectField('ContactId',con.Id);
            gen.writeEndObject();
            String pretty = gen.getAsString();
            return pretty;  

        
    }catch(Exception e)
    {  
        if(e.getMessage().contains('Duplicate contact info'))
        {
             return String.ValueOf(CustomException.getException(GENERIC_ERROR,POSTERROR_TYPE));
        }
        return e.getMessage();
    }
 }
 
 
 global static LensDTO parse(String Json)
 {
    return (LensDTO) system.json.deserialize(json,LensDTO.class);
 }
 
 
 global class LensDTO{
    
    public String FirstName {get;set;}
    public String Email {get;set;}
    public String MobilePhone {get;set;}
    public Boolean LensType {get;set;}
    public String Gender {get;set;}
    public String Country {get;set;}
    public Boolean PrivacyPolicy {get;set;}
    public Boolean CommunicationAgreement {get;set;}
    public String DateOfBirth{get;set;}
    
    public LensDTO(String FirstName,String Email,String MobilePhone,Boolean LensType,String Gender,String Country,Boolean PrivacyPolicy,Boolean CommunicationAgreement,String DateOfBirth)
    {
        this.FirstName= FirstName;
        this.Email = Email;
        this.MobilePhone = MobilePhone;
        this.LensType = LensType;
        this.Gender = Gender;
        this.Country = Country;
        this.PrivacyPolicy = PrivacyPolicy;
        this.CommunicationAgreement = CommunicationAgreement;
        this.DateOfBirth = DateOfBirth;
        
    }
    
 }

}