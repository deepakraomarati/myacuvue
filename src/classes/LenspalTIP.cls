@RestResource(urlmapping='/Apex/LenspalTIP/*')
global without sharing class LenspalTIP {

 /* --------------------------------------------
  * Author: 
  * Company: BICS
  * Description : service return available tips to user.
  *               service create the tipmember record to user.
  * ---------------------------------------------*/

global static String TIPS_NOTAVAILABLE = 'ACE0018';
global static String TIP_REQUIRED      = 'ACE0019'; 
global static String Invalid_TIP       = 'ACE0022';     
global static String GET_TYPE          = 'getTIPInfo';
global static String POST_TYPE         = 'TipInsert';


@HTTPGet
global static String doGet()
{
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    String TIPID = req.requestURI.substring(req.requestURI.LastIndexOf('/')+1);
    System.debug('TIPID ####'+TIPID);
    return getTIPInfo(TIPID);
}

@HTTPPost
global static String doPost()
{
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    Blob body=req.requestBody;
    String jSONRequestBody=req.requestBody.toString().trim();
    //Map<String,Object> m=(Map<String,Object>)Json.deserializeUnTyped(body.toString());
    //System.debug('M ######'+m);
    //List<TIPInfo> tiplist = (List<TIPInfo>)JSON.deserialize(JSON.serialize(m.get('TIPInfo')),List<TIPInfo>.class);
    //System.debug('JSONREQUEST ####'+jSONRequestBody);
    List<TIPInfo> tiplist = (List<TIPInfo>)JSON.deserializeStrict(jSONRequestBody,List<TIPInfo>.class);
    System.debug('tiplist######'+tiplist);
    SubscribeTIP tipinstance = new SubscribeTIP(tiplist);
    System.debug('TIPINSTANCE ###'+tipinstance);
    
    return TipInsert(tipinstance); 
}

global static String TipInsert(SubscribeTIP st)
{

    try{
        
        List<TIPInfo> lstip = st.TIPInfo;
        System.debug('LSTITP ###'+lstip);
        
        List<TIP_Member__c> lstinsert = new List<TIP_Member__c>();
        
        if(lstip.size()>0 && !lstip.isEmpty())
        {
            for(TIPInfo t:lstip)
            {
                if(t.ContactId !=null && t.ContactId !='' && t.SymptomId !=null && t.SymptomId !=''&& t.TIPID !=null && t.TIPID !='')
                {
                    List<TipsRaw__c> tip = [SELECT Id,TIP_Id__c,Name, TIP_Description__c,TIP_Aggregator__c,TIP_Category__c,TIP_Image__c,TIP_LinkId__c FROM TipsRaw__c WHERE TIP_Id__c=:t.TIPID];
                    
                    System.debug('TIP ###'+tip);
                    
                    if(tip.size() == 0 && tip.isEmpty())
                    {
                        throw new CustomException(CustomException.getException(Invalid_TIP,POST_TYPE));
                           
                    }else{
                        
                        Date RDate;
        
                        if(t.RegisteredDate !=null && t.RegisteredDate!='')
                        {
                            RDate = Date.Parse(t.RegisteredDate);
                        }
                        
                         TIP_Member__c tp=new TIP_Member__c();
                         tp.Contact_Id__c = t.ContactId;
                         tp.Status__c = Boolean.ValueOf(t.Status);
                         tp.SymptomId__c = t.SymptomId;
                         tp.TIP_Id__c = tip[0].Id;
                         tp.Registered_Date__c = RDate;
                         lstinsert.add(tp);
                    }
                    
                }else{
                    throw new CustomException(CustomException.getException(TIP_REQUIRED,POST_TYPE));
                }
            }
            
            if(lstinsert.size() >0 && !lstinsert.isEmpty())
            {
                insert lstinsert;
                
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartObject();
                gen.writeObjectField('Status','Success');
                gen.writeEndObject();
                String action = gen.getAsString().normalizeSpace();
                return action;    
            }
            
        }
        
        return null;
        
    }catch(Exception e)
    {
        return e.getMessage();
    }    
}

global static String getTIPInfo(String TIPID)
{
    try{
        
        List<TIPDTO> lstdto = new List<TIPDTO>();
        TIPDTO td;
        
        System.debug('TIPID ###'+TIPID);
                
        if(TIPID =='')
        {
            List<TipsRaw__c> lsttip = [SELECT Id,Name,TIP_Aggregator__c,TIP_Category__c,TIP_Description__c,TIP_Id__c,TIP_Image__c,TIP_LinkId__c FROM TipsRaw__c];
            System.debug('TIPSIZE ###'+lsttip.size());
            
            if(lsttip.size()>0 && !lsttip.isEmpty())
            {
                for(TipsRaw__c tp:lsttip)
                {
                    td=new TIPDTO(tp.Id,tp.TIP_Id__c,tp.Name,tp.TIP_Category__c,tp.TIP_Description__c,tp.TIP_Image__c,tp.TIP_LinkId__c,tp.TIP_Aggregator__c);
                    lstdto.add(td);
                }
                System.debug('lstdto ###'+lstdto);
                return Json.serialize(lstdto);
            }else{
                throw new CustomException(CustomException.getException(TIPS_NOTAVAILABLE,GET_TYPE));
            }
        }else{
            System.debug('TIPSNOTEMPTY ###'+TIPID);
            List<TipsRaw__c> lsttip = [SELECT Id,Name,TIP_Aggregator__c,TIP_Category__c,TIP_Description__c,TIP_Id__c,TIP_Image__c,TIP_LinkId__c FROM TipsRaw__c WHERE TIP_Id__c=:TIPID];
            System.debug('TIPSIZE ###'+lsttip.size());
            
            if(lsttip.size()>0 && !lsttip.isEmpty())
            {
                for(TipsRaw__c tps:lsttip)
                {
                    td=new TIPDTO(tps.Id,tps.TIP_Id__c,tps.Name,tps.TIP_Category__c,tps.TIP_Description__c,tps.TIP_Image__c,tps.TIP_LinkId__c,tps.TIP_Aggregator__c);
                }
                return Json.serialize(td);
            }else{
                throw new CustomException(CustomException.getException(TIPS_NOTAVAILABLE,GET_TYPE));
            }
        }
        
    }catch(Exception e)
    {
        return e.getMessage();
    }
}

global class TIPDTO
{
    public String Id {get;set;}
    public String TipID {get;set;}
    public String TipName {get;set;}
    public String TipCategory {get;set;}
    public String TipBody {get;set;}
    public String TipImage {get;set;}
    public String TipLink {get;set;}
    public String TipAggregator {get;set;}
    
    public TIPDTO(String Id,String TipID,String TipName,String TipCategory,String TipBody,String TipImage,String TipLink,String TipAggregator)
    {
        this.Id = Id;
        this.TipID = TipID;
        this.TipName = TipName;
        this.TipCategory = TipCategory;
        this.TipBody = TipBody;
        this.TipImage = TipImage;
        this.TipLink = TipLink;
        this.TipAggregator = TipAggregator;
    }
}

global class SubscribeTIP
{
    public List<TIPInfo> TIPInfo {get;set;}
    
    public SubscribeTIP(List<TIPInfo> TIP)
    {
        this.TIPInfo = TIP;
    }
}

global class TIPInfo
{
    public String ContactId {get;set;}
    public String TIPID {get;set;}
    public String SymptomId {get;set;}
    public String Status {get;set;}
    public String RegisteredDate {get;set;}
    
    public TIPInfo (String ContactId,String TIPID, String SymptomId,String Status,String RegisteredDate)
    {
        this.ContactId = ContactId;
        this.TIPID = TIPID;
        this.SymptomId = SymptomId;
        this.Status = Status;
        this.RegisteredDate = RegisteredDate;
    }
}

 

}