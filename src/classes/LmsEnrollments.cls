/**
* File Name: LmsEnrollments
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  18-Oct-2018
* Description : batch class to get the Tags & categories information from LMS and updates or create in salesforce
* Copyright (c) $2018 Johnson & Johnson
*/
@RestResource(urlMapping='/apex/myaccount/v1/Enrollments')
global without sharing class LmsEnrollments
{
	global static final String PARAMETER_ERROR_TYPE = 'EnrollmentsService' ;
	global static final String STATUS_ERROR_TYPE = 'EnrollmentsService';
	global static final String GENERIC_ERROR = 'GEN0001';
	global static final String ERROR_TYPE = 'LearningService' ;
	list <CourseList> CourseList = new list <CourseList>();

	/**
	* Description : Get enrolled courses to loggedin user 
	*/
	@HttpPost
	global static String RegEnrollments()
	{
		RestRequest req = RestContext.request;
		Blob body = req.requestBody;
		CourseList Enrollments = (CourseList) System.JSON.deserialize(body.toString(), CourseList.class);
		try
		{
			String response='';
			if (Enrollments.UserId == '' && Enrollments.Courseid == '')
			{
				throw new CustomException(LmsUtils.getException('PC0028', PARAMETER_ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0028').Error_Description__c)));
			}
			else
			{
				user u = [Select id,LMS_Id__c from user where id = :Enrollments.UserId];
				if (u.LMS_Id__c != '' && u.LMS_Id__c != null)
				{
					response = LmsUtils.autoEnrollments(u.LMS_Id__c, Enrollments.Courseid);
				}
			}
			Returnresult Result = new Returnresult(response, Enrollments.UserId, Enrollments.Courseid);
			return json.serialize(Result);
		}
		catch (Exception e)
		{
			return e.getMessage();
		}
	}

	/**
	* Description : Enroll courses to used in given inputs 
	*/
	@HttpPut
	global static string RegMassEnrollments()
	{
		String Response;
		Map <string, string> lmsUserMap = new Map<string, string>();
		List<MassReturnresult> Returnlst = new List<MassReturnresult>();
		RestRequest req = RestContext.request;
		Blob body = req.requestBody;
		try
		{
			LmsEnrollments Input = LmsEnrollments.parse(body.toString());
			set <string> Contactlst = new set <string>();
			for (CourseList ReqInputs : Input.CourseList)
			{
				if (ReqInputs.ContactId == '' || ReqInputs.Courseid == '')
				{
					throw new CustomException(LmsUtils.getException('PC0028', PARAMETER_ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0028').Error_Description__c)));
				}
				Contactlst.add(ReqInputs.ContactId);
			}
			list <User> UserDetaillst = [select id,FirstName,LastName,Username,Email,Contact.User_Role__r.Name,LMS_Id__c,LocaleSidKey from user where contactid = :Contactlst];
			//LMS user Creation
			for (User SFUser : UserDetaillst)
			{
				if (SFUser.LMS_Id__c == null || SFUser.LMS_Id__c == '')
				{
					String DeptId = LMS_Settings__c.getValues('DEPT_ID_' + SFUser.LocaleSidKey).Value__c;
					string Lang = '1';
					if (LMS_Settings__c.getValues(SFUser.LocaleSidKey).Value__c != null)
					{
						Lang = LMS_Settings__c.getValues(SFUser.LocaleSidKey).Value__c;
					}
					B2B_Utils.NewUser nu = new B2B_Utils.NewUser(SFUser.FirstName, SFUser.LastName, SFUser.Username, SFUser.Id, SFUser.Email, DeptId, SFUser.Id, SFUser.Contact.User_Role__r.Name, Lang);
					String LMS_UserId = B2B_Utils.createLMSUser(JSON.serialize(nu), false);
					if (LMS_UserId == null)
					{
						throw new CustomException(B2B_Utils.getException(GENERIC_ERROR, ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('GEN0001').Error_Description__c)));
					}
					lmsUserMap.put(SFUser.id, LMS_UserId);
				}
			}
			map <Id, user> Usermap = new map<Id, user>();
			for (User SFUser1 : UserDetaillst)
			{
				Usermap.put(SFUser1.contactid, SFUser1);
			}
			for (CourseList ReqInputs1 : input.CourseList)
			{
				if ((Usermap.get(ReqInputs1.ContactId).LMS_Id__c != null && Usermap.get(ReqInputs1.ContactId).LMS_Id__c != '') || (Lmsusermap.containsKey(Usermap.get(ReqInputs1.ContactId).id)))
				{
					if (lmsUserMap.containsKey(Usermap.get(ReqInputs1.ContactId).id))
					{
						Response = LmsUtils.autoEnrollments(Lmsusermap.get(Usermap.get(ReqInputs1.ContactId).id), ReqInputs1.Courseid);
					}
					else
					{
						Response = LmsUtils.autoEnrollments(Usermap.get(ReqInputs1.ContactId).LMS_Id__c, ReqInputs1 .Courseid);
					}
					Returnlst.add(new MassReturnresult(Response, ReqInputs1.ContactId, ReqInputs1.Courseid));
				}
				else
				{
					throw new CustomException(LmsUtils.getException('PC0028', PARAMETER_ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0028').Error_Description__c)));
				}
			}
			if (!lmsUserMap.Isempty())
			{
				for (User U : userdetaillst)
				{
					if (lmsUserMap.containsKey(u.id))
					{
						U.LMS_Id__c = lmsUserMap.get(u.id);
					}
				}
				Update userdetaillst;
			}
			return (Returnlst.Isempty() ? null : JSON.serializePretty(Returnlst));
		}
		catch (Exception e)
		{
			throw(e);
		}
	}

	public class Returnresult
	{
		public string Status { get; set; }
		public String UserId { get; set; }
		public String Courseid { get; set; }

		public Returnresult(string status, String UserId, String Courseid)
		{
			this.Status = status;
			this.UserId = UserId;
			this.Courseid = Courseid;
		}
	}

	public class MassReturnresult
	{
		public string Status { get; set; }
		public String ContactId { get; set; }
		public String Courseid { get; set; }

		public MassReturnresult(string status, String ContactId, String Courseid)
		{
			this.Status = status;
			this.ContactId = ContactId;
			this.Courseid = Courseid;
		}
	}

	public class CourseList
	{
		public String UserId { get; set; }
		public String ContactId { get; set; }
		public String CourseId { get; set; }
	}
	public static LmsEnrollments parse(String json)
	{
		return (LmsEnrollments) System.JSON.deserialize(json, LmsEnrollments.class);
	}
}