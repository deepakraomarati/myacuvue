/**
* File Name: lmsUtils
* Author : Sathishkumar | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : Utility class used from parent classes.
* Copyright (c) $2018 Johnson & Johnson
*/
global with sharing class LmsUtils
{
	global static final String GENERIC_ERROR = 'GEN0001';
	global static final String GENERIC_ERROR_TYPE = 'Generic';
	global static final String ENDPOINT = 'EndPoint';
	global static final String VUSERS = 'v1/Users/';
	global static final String VCOURSES = 'v1/Courses/';
	static Integer countForAuthenticate = 1;
	static Integer countForGetAvailableCourses = 1;
	static Integer countForGetAvailableCategories = 1;
	static Integer countForGetAvailableTags = 1;
	static Integer countForGetAvailableResources = 1;
	static string Auth = 'Authorization';
	/*Method for update the LMS Userdetails when user deatails updates profile in jjvpro*/
	@future(Callout=true)
	public static void updateLMSProfileDetails(String ExternalId, String Firstname, String Lastname, String Role, String Email, String Uname, String Langkey)
	{
		String DeptId = LMS_Settings__c.getValues('DEPT_ID_' + Langkey).Value__c;
		String absorbLangKey = LMS_Settings__c.getValues(Langkey).Value__c;

		String body = '';
		body = '{';
		body += '"DepartmentId": "' + DeptId + '",';
		body += '"EmailAddress": "' + Email + '",';
		body += '"Username": "' + Uname + '",';
		body += '"FirstName": "' + Firstname + '",';
		body += '"LastName": "' + Lastname + '",';
		body += '"JobTitle": "' + Role + '",';
		body += '"LanguageId": "' + absorbLangKey + '"';
		body += '}';
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mc = new MockHttpResponseGenerator1();
			mc.respond4();

		}
		else
		{
			LmsUtils.processRequest(LMS_Settings__c.getValues(ENDPOINT).Value__c + VUSERS + ExternalId, body, 'PUT', LmsUtils.authenticate());
		}
	}
	/* Method for getting userid and userdetails using username from absorblms */
	public static String getEnrollments(String userId, String progressStatus)
	{
		Map<Integer, String> mapFrSts = new Map<Integer, String>();
		mapFrSts.put(0, 'Not Started');
		mapFrSts.put(1, 'In Progress');
		mapFrSts.put(2, 'Pending Approval');
		mapFrSts.put(3, 'Completed');
		mapFrSts.put(4, 'Not Complete');
		mapFrSts.put(5, 'Failed');
		mapFrSts.put(6, 'Declined');
		mapFrSts.put(7, 'Pending Evaluation Required');
		mapFrSts.put(8, 'OnWaitlist');
		mapFrSts.put(9, 'Absent');
		mapFrSts.put(10, 'Not Applicable');
		mapFrSts.put(11, 'Pending Proctor');

		Map<Integer, String> mapFrActiveStatus = new Map<Integer, String>();
		mapFrActiveStatus.put(0, 'Active');
		mapFrActiveStatus.put(1, 'Inactive');
		mapFrActiveStatus.put(2, 'Pending');

		Map<Integer, String> mapFrExpireType = new Map<Integer, String>();
		mapFrExpireType.put(0, 'None');
		mapFrExpireType.put(1, 'Date');
		mapFrExpireType.put(2, 'Duration');

		String statusFrAbsorb;
		if (progressStatus == 'InProgress')
		{
			statusFrAbsorb = '1';
		}
		else if (progressStatus == 'Completion')
		{
			statusFrAbsorb = '3';
		}
		else
		{
			statusFrAbsorb = '0';
		}
		String tempUserId;
		List<Role__c> ProUserRole = new List<Role__c>();
		List<User> proUser = [Select Id,Username,Email,Occupation__c,ContactId,LMS_Id__c,Localesidkey from User where Id = :userId];
		if (!proUser.isEmpty())
		{
			String ProUserId;
			String proUserContactId;
			if (proUser[0].LMS_Id__c != '' && proUser[0].LMS_Id__c != null)
			{
				ProUserId = proUser[0].LMS_Id__c;
			}
			proUserContactId = proUser[0].ContactId;
			ProUserRole = [select id,name from Role__c where name = :proUser[0].Occupation__c];
			if (ProUserRole.isEmpty())
			{
				throw new CustomException(LmsUtils.getException(GENERIC_ERROR, GENERIC_ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('GEN0001').Error_Description__c)));
			}
			else
			{
				string AbsorbdummyUser = proUser[0].Localesidkey + '-' + ProUserRole[0].id;
				if (absorbSettings.containsKey(AbsorbdummyUser))
				{
					tempUserId = absorbSettings.get(AbsorbdummyUser).Value__c;
				}
				else
				{
					throw new CustomException(LmsUtils.getException(GENERIC_ERROR, GENERIC_ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('GEN0001').Error_Description__c)));
				}
			}
			List<CourseDTOFrUpdate> AvailCrseForParticularUserBeforeFilter = getAvailabeCourses(tempUserId);
			List<CourseDTOFrUpdate> AvailCrseForParticularUser = new List<CourseDTOFrUpdate>();
			//This loop is to avoid other course types. Only Online and InstructorLed Courses will be considered 
			for (CourseDTOFrUpdate CourseDTOObjBfrFltr : AvailCrseForParticularUserBeforeFilter)
			{
				if (CourseDTOObjBfrFltr.CourseType == 'OnlineCourse' || CourseDTOObjBfrFltr.CourseType == 'InstructorLedCourse')
				{
					AvailCrseForParticularUser.add(CourseDTOObjBfrFltr);
				}
			}
			List<CourseDTO> coursesForParticularUser = new List<CourseDTO>();
			if (ProUserId != '' && ProUserId != null)
			{
				coursesForParticularUser = getCourse(ProUserId);
			}
			List<String> lstFrTagIds = new List<String>();
			List<Course__c> courseList = new List<Course__c>();
			Set<String> setCourseList = new Set<String>();
			List<String> lstFrCategoryIds = new List<String>();
			Map<String, List<String>> mapforTags = new Map<String, List<String>>();
			for (CourseDTOFrUpdate CourseDTOObj : AvailCrseForParticularUser)
			{
				mapforTags.put(CourseDTOObj.Id, CourseDTOObj.TagIds);
				for (String objFrTagIdsLoop : CourseDTOObj.TagIds)
				{
					lstFrTagIds.add(objFrTagIdsLoop);
				}
				Course__c course = new Course__c();
				course.Name = CourseDTOObj.Name;
				course.LMS_ID__c = CourseDTOObj.Id;
				course.Description__c = CourseDTOObj.Description;
				course.Duration__c = CourseDTOObj.LearnerTime;
				if (CourseDTOObj.AccessDate != null)
				{
					course.AccessDate__c = Date.valueOf(CourseDTOObj.AccessDate);
				}
				course.Notes__c = CourseDTOObj.Notes;
				course.CategoryId__c = CourseDTOObj.CategoryId;
				lstFrCategoryIds.add(CourseDTOObj.CategoryId);
				course.ExpireType__c = mapFrExpireType.get(CourseDTOObj.ExpireType);
				if (CourseDTOObj.ExpiryDate != null)
				{
					course.ExpiryDate__c = Date.valueOf(CourseDTOObj.ExpiryDate);
				}
				course.ActiveStatus__c = mapFrActiveStatus.get(CourseDTOObj.ActiveStatus);
				course.Type__c = CourseDTOObj.CourseType;
				courseList.add(course);
				setCourseList.add(CourseDTOObj.Id);
			}
			for (CourseDTO CourseDTOObj : coursesForParticularUser)
			{

				if (!setCourseList.contains(CourseDTOObj.CourseId))
				{
					CourseDTOFrUpdate objFrCourseDTOFrUpdate = getCourseType(CourseDTOObj.CourseId);
					String typeFrElmnt = objFrCourseDTOFrUpdate.CourseType;
					if (typeFrElmnt == 'OnlineCourse' || typeFrElmnt == 'InstructorLedCourse')
					{
						List<ResourcesDTOFrUpdate> availableResource = getResourcesForCourse(CourseDTOObj.CourseId);
						Map<String, String> mapFrImgs = new Map<String, String>();

						for (ResourcesDTOFrUpdate rr : availableResource)
						{
							mapFrImgs.put(rr.Name, rr.File);
						}
						mapforTags.put(objFrCourseDTOFrUpdate.Id, objFrCourseDTOFrUpdate.TagIds);
						for (String objFrTagIdsLoop : objFrCourseDTOFrUpdate.TagIds)
						{
							lstFrTagIds.add(objFrTagIdsLoop);
						}
						Course__c course = new Course__c();
						course.Name = objFrCourseDTOFrUpdate.Name;
						course.LMS_ID__c = objFrCourseDTOFrUpdate.Id;
						course.Description__c = objFrCourseDTOFrUpdate.Description;
						if (objFrCourseDTOFrUpdate.AccessDate != null)
						{
							course.AccessDate__c = Date.valueOf(objFrCourseDTOFrUpdate.AccessDate);
						}
						course.Notes__c = objFrCourseDTOFrUpdate.Notes;
						course.Duration__c = objFrCourseDTOFrUpdate.LearnerTime;
						course.CategoryId__c = objFrCourseDTOFrUpdate.CategoryId;
						lstFrCategoryIds.add(objFrCourseDTOFrUpdate.CategoryId);
						course.ExpireType__c = mapFrExpireType.get(objFrCourseDTOFrUpdate.ExpireType);
						if (objFrCourseDTOFrUpdate.ExpiryDate != null)
						{
							course.ExpiryDate__c = Date.valueOf(objFrCourseDTOFrUpdate.ExpiryDate);
						}
						course.ActiveStatus__c = mapFrActiveStatus.get(objFrCourseDTOFrUpdate.ActiveStatus);
						course.Type__c = objFrCourseDTOFrUpdate.CourseType;
						course.Main_Course_Image__c = mapFrImgs.get('Main_Course_Image');
						course.Related_Content_Image__c = mapFrImgs.get('Related_Content_Image');
						course.Landing_page_Image__c = mapFrImgs.get('Landing_page_Image');
						course.Online_CoursePage_Image__c = mapFrImgs.get('Online_CoursePage_Image');
						courseList.add(course);
						setCourseList.add(objFrCourseDTOFrUpdate.Id);
					}
				}
			}
			Savepoint changes = Database.setSavepoint();
			try
			{
				Map<String, String> mapFrCategory = new Map<String, String>();
				Map<String, String> mapFrTag = new Map<String, String>();
				if (!courseList.isEmpty())
				{
					upsert courseList LMS_ID__c;
				}
				List<Categories_Tag__c> lstFrCategory =
				[
						SELECT LMS_ID__c, Name
						FROM Categories_Tag__c
						where Type__c = 'Category' and LMS_ID__c IN :lstFrCategoryIds
				];
				for (Categories_Tag__c lstFrCategoryObj : lstFrCategory)
				{
					mapFrCategory.put(lstFrCategoryObj.LMS_ID__c, lstFrCategoryObj.Name);
				}
				List<Categories_Tag__c> lstFrTag =
				[
						SELECT LMS_ID__c, Name
						FROM Categories_Tag__c
						where Type__c = 'Tag' and LMS_ID__c IN :lstFrTagIds
				];
				for (Categories_Tag__c lstFrTagObj : lstFrTag)
				{
					mapFrTag.put(lstFrTagObj.LMS_ID__c, lstFrTagObj.Name);
				}
				Map<String, Course__c> mapForCourse = new Map<String, Course__c>();
				List<Course__c> listForCourseMapping =
				[
						select Id,
								LMS_ID__c,
								Description__c,Duration__c,
								Main_Course_Image__c,
								Related_Content_Image__c,
								Landing_page_Image__c,
								Online_CoursePage_Image__c,
								Type__c,CategoryId__c
						from Course__c
						where LMS_ID__c in :setCourseList
				];
				for (Course__c courseObjVarA : listForCourseMapping)
				{
					mapForCourse.put(courseObjVarA.LMS_ID__c, courseObjVarA);
				}
				List<CourseAssignment__c> courseAssignList = new List<CourseAssignment__c>();
				for (CourseDTO CourseDTOObjj : coursesForParticularUser)
				{
					if (setCourseList.contains(CourseDTOObjj.CourseId))
					{
						CourseAssignment__c courseAssignent = new CourseAssignment__c();
						courseAssignent.Enrollment_Id__c = CourseDTOObjj.Id;
						courseAssignent.Status__c = mapFrSts.get(Integer.valueOf(CourseDTOObjj.Status));
						if (CourseDTOObjj.DateStarted != null) courseAssignent.StartDate__c = Date.valueOf(CourseDTOObjj.DateStarted);
						courseAssignent.Score__c = CourseDTOObjj.Score;
						courseAssignent.Progress__c = CourseDTOObjj.Progress;
						courseAssignent.Course__c = mapForCourse.get(CourseDTOObjj.CourseId).Id;
						courseAssignent.Contact__c = proUserContactId;
						if (CourseDTOObjj.DateCompleted != null)
						{
							courseAssignent.CompletionDate__c = Date.valueOf(CourseDTOObjj.DateCompleted);
						}
						courseAssignList.add(courseAssignent);
					}
				}
				if (!courseAssignList.isEmpty())
				{
					upsert courseAssignList Enrollment_Id__c;
				}
				List<DTOForGetEnrollmentsSts> DTOLstForGetEnrollmentsSts = new List<DTOForGetEnrollmentsSts>();
				Set<String> setProSiteList = new Set<String>();
				for (CourseDTO CourseDTOObjj : coursesForParticularUser)
				{
					if (setCourseList.contains(CourseDTOObjj.CourseId))
					{
						setProSiteList.add(mapForCourse.get(CourseDTOObjj.CourseId).Id);
						List<tag> tagIdsWithName = preTagList(mapforTags.get(CourseDTOObjj.CourseId), mapFrTag);
						List<Category> CategoryWithName = new List<Category>();
						if (mapForCourse.get(CourseDTOObjj.CourseId).CategoryId__c != null)
						{
							Category ObjFrCategory = new Category();
							ObjFrCategory.Id = mapForCourse.get(CourseDTOObjj.CourseId).CategoryId__c;
							ObjFrCategory.Name = mapFrCategory.get(mapForCourse.get(CourseDTOObjj.CourseId).CategoryId__c);
							CategoryWithName.add(ObjFrCategory);
						}
						List<image> images = new List<image>();
						if (mapForCourse.get(CourseDTOObjj.CourseId) != null)
						{
							images = prepImages(mapForCourse, CourseDTOObjj.CourseId);
						}
						if ('0'.equals(statusFrAbsorb))
						{
							DTOForGetEnrollmentsSts DTOObjForGetEnrollmentsSts = new DTOForGetEnrollmentsSts();
							DTOObjForGetEnrollmentsSts = preDTOForGetEnrollmentsSts(mapForCourse, mapFrSts, CourseDTOObjj, proUser[0].Localesidkey);
							DTOObjForGetEnrollmentsSts.Images = images;
							DTOObjForGetEnrollmentsSts.Category = CategoryWithName;
							DTOObjForGetEnrollmentsSts.Tags = tagIdsWithName;
							DTOLstForGetEnrollmentsSts.add(DTOObjForGetEnrollmentsSts);
						}
						else if ('1'.equals(statusFrAbsorb) && (CourseDTOObjj.Status == '1'))
						{
							DTOForGetEnrollmentsSts DTOObjForGetEnrollmentsSts = new DTOForGetEnrollmentsSts ();
							DTOObjForGetEnrollmentsSts = preDTOForGetEnrollmentsSts(mapForCourse, mapFrSts, CourseDTOObjj, proUser[0].Localesidkey);
							DTOObjForGetEnrollmentsSts.Name = CourseDTOObjj.CourseName;
							DTOObjForGetEnrollmentsSts.Images = images;
							DTOObjForGetEnrollmentsSts.Category = CategoryWithName;
							DTOObjForGetEnrollmentsSts.Tags = tagIdsWithName;
							DTOLstForGetEnrollmentsSts.add(DTOObjForGetEnrollmentsSts);
						}
						else if ('3'.equals(statusFrAbsorb) && (CourseDTOObjj.Status == '3'))
						{
							DTOForGetEnrollmentsSts DTOObjForGetEnrollmentsSts = new DTOForGetEnrollmentsSts ();
							DTOObjForGetEnrollmentsSts = preDTOForGetEnrollmentsSts(mapForCourse, mapFrSts, CourseDTOObjj, proUser[0].Localesidkey);
							DTOObjForGetEnrollmentsSts.Images = images;
							DTOObjForGetEnrollmentsSts.Category = CategoryWithName;
							DTOObjForGetEnrollmentsSts.Tags = tagIdsWithName;
							DTOLstForGetEnrollmentsSts.add(DTOObjForGetEnrollmentsSts);
						}
					}

				}
				for (CourseDTOFrUpdate CourseDTOObj : AvailCrseForParticularUser)
				{
					List<tag> tagIdsWithName = preTagList(mapforTags.get(CourseDTOObj.Id), mapFrTag);
					List<Category> CategoryWithName = new List<Category>();
					if (mapForCourse.get(CourseDTOObj.Id).CategoryId__c != null)
					{
						Category ObjFrCategory = new Category();
						ObjFrCategory.Id = mapForCourse.get(CourseDTOObj.Id).CategoryId__c;
						ObjFrCategory.Name = mapFrCategory.get(mapForCourse.get(CourseDTOObj.Id).CategoryId__c);
						CategoryWithName.add(ObjFrCategory);
					}
					List<image> images = new List<image>();
					if (mapForCourse.get(CourseDTOObj.Id) != null)
					{
						images = prepImages(mapForCourse, CourseDTOObj.Id);
					}
					if ('0'.equals(statusFrAbsorb))
					{
						DTOForGetEnrollmentsSts DTOObjForGetEnrollmentsSts = new DTOForGetEnrollmentsSts ();
						DTOObjForGetEnrollmentsSts.Name = CourseDTOObj.Name;
						DTOObjForGetEnrollmentsSts.LMSID = CourseDTOObj.Id;
						DTOObjForGetEnrollmentsSts.Description = mapForCourse.get(CourseDTOObj.Id).Description__c;
						DTOObjForGetEnrollmentsSts.Status = mapFrSts.get(0);
						DTOObjForGetEnrollmentsSts.CourseId = mapForCourse.get(CourseDTOObj.Id).Id;
						DTOObjForGetEnrollmentsSts.PercentComplete = '0.0';
						DTOObjForGetEnrollmentsSts.Images = images;
						DTOObjForGetEnrollmentsSts.Duration = mapForCourse.get(CourseDTOObj.Id).Duration__c;
						DTOObjForGetEnrollmentsSts.Type = CourseDTOObj.CourseType;
						DTOObjForGetEnrollmentsSts.Language = proUser[0].Localesidkey;
						DTOObjForGetEnrollmentsSts.Category = CategoryWithName;
						DTOObjForGetEnrollmentsSts.Tags = tagIdsWithName;
						if (!setProSiteList.contains(mapForCourse.get(CourseDTOObj.Id).Id))
						{
							DTOLstForGetEnrollmentsSts.add(DTOObjForGetEnrollmentsSts);
						}
					}
				}
				return JSON.serializePretty(DTOLstForGetEnrollmentsSts);
			}
			catch (Exception e)
			{
				Database.rollback(changes);
				return LmsUtils.getException(GENERIC_ERROR, GENERIC_ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('GEN0001').Error_Description__c));
			}
		}
		else
		{
			return LmsUtils.getException(GENERIC_ERROR, GENERIC_ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('GEN0001').Error_Description__c));
		}
	}
	private static DTOForGetEnrollmentsSts preDTOForGetEnrollmentsSts(Map<String, Course__c> mapForCourse, Map<Integer, String> mapFrSts, CourseDTO CourseDTOObjj, String Locale)
	{
		DTOForGetEnrollmentsSts DTOObjForGetEnrollmentsSts = new DTOForGetEnrollmentsSts();
		DTOObjForGetEnrollmentsSts.Name = CourseDTOObjj.CourseName;
		DTOObjForGetEnrollmentsSts.LMSID = CourseDTOObjj.CourseId;
		DTOObjForGetEnrollmentsSts.Description = mapForCourse.get(CourseDTOObjj.CourseId).Description__c;
		DTOObjForGetEnrollmentsSts.Status = mapFrSts.get(Integer.valueOf(CourseDTOObjj.Status));
		DTOObjForGetEnrollmentsSts.StartDate = CourseDTOObjj.DateStarted;
		DTOObjForGetEnrollmentsSts.CompletedDate = CourseDTOObjj.DateCompleted;
		DTOObjForGetEnrollmentsSts.PercentComplete = String.valueOf(CourseDTOObjj.Progress);
		DTOObjForGetEnrollmentsSts.Duration = mapForCourse.get(CourseDTOObjj.CourseId).Duration__c;
		DTOObjForGetEnrollmentsSts.CourseId = mapForCourse.get(CourseDTOObjj.CourseId).Id;
		DTOObjForGetEnrollmentsSts.Type = mapForCourse.get(CourseDTOObjj.CourseId).Type__c;
		DTOObjForGetEnrollmentsSts.Language = Locale;
		return DTOObjForGetEnrollmentsSts;
	}
	private static List<image> prepImages(Map<String, Course__c> mapForCourse, String CourseId)
	{
		List<image> images = new List<image>();
		if (mapForCourse.get(CourseId).Main_Course_Image__c != null)
		{
			image img_1 = new image();
			img_1.Name = 'Main_Course_Image';
			img_1.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCourse.get(CourseId).Main_Course_Image__c;
			images.add(img_1);
		}
		if (mapForCourse.get(CourseId).Related_Content_Image__c != null)
		{
			image img_2 = new image();
			img_2.Name = 'Related_Content_Image';
			img_2.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCourse.get(CourseId).Related_Content_Image__c;
			images.add(img_2);
		}
		if (mapForCourse.get(CourseId).Landing_page_Image__c != null)
		{
			image img_3 = new image();
			img_3.Name = 'Landing_page_Image';
			img_3.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCourse.get(CourseId).Landing_page_Image__c;
			images.add(img_3);
		}
		if (mapForCourse.get(CourseId).Online_CoursePage_Image__c != null)
		{
			image img_4 = new image();
			img_4.Name = 'Online_CoursePage_Image';
			img_4.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCourse.get(CourseId).Online_CoursePage_Image__c;
			images.add(img_4);
		}
		return images;
	}
	private static List<tag> preTagList(List<String> tagIds, Map<String, String> mapFrTag)
	{
		List<tag> tagIdsWithName = new List<tag>();
		for (String tag : tagIds)
		{
			tag objFrTag = new tag();
			objFrTag.Id = tag;
			objFrTag.Name = mapFrTag.get(tag);
			tagIdsWithName.add(objFrTag);
		}
		return tagIdsWithName;
	}
	/* Method for authenticating Absorb LMS. Credentials are stored in Custom Settings */
	public static string authenticate()
	{
		String sessionId;
		sessionId = absorbSettings.get('Token').Value__c;
		return sessionId;
	}

	/* Generic Method for processing any rest request*/
	public static HttpResponse processRequest(String ep, String msg, String action, String SessionId)
	{
		HttpRequest req = new HttpRequest();
		req.setMethod(action);
		req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
		if (!ep.Contains('Authenticate')) req.setHeader(Auth, SessionId);
		req.setEndpoint(ep);
		if (action == 'POST')
		{
			if (ep.Contains('Authenticate'))
			{
				req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
			}
			else
			{
				req.setHeader('Content-Type', 'application/json');
			}
			req.setBody(msg);
		}
		else if (action == 'PUT')
		{
			req.setHeader('Content-Type', 'application/json');
			req.setBody(msg);
			req.setHeader('Content-length', string.ValueOf(msg.length()));
		}
		else
		{
			req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
		}
		req.setTimeout(60000);
		HttpResponse res = new HttpResponse();
		if (!Test.isRunningTest())
		{
			Http holdshttp = new Http();
			res = holdshttp.send(req);
		}
		return res;
	}
	/* Method for getting credentials from custom settings */
	public static Map<String, LMS_Settings__c> absorbSettings
	{
		get
		{
			if (absorbSettings == null || absorbSettings.isEmpty())
			{
				absorbSettings = new Map<String, LMS_Settings__c> (LMS_Settings__c.getAll());
			}
			return absorbSettings;
		}
		set;
	}

	/* Method for Custom Exceptions in Absorb */
	public static String getException(String ErrCode, String ErrType, String ErrDesc)
	{
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartArray();
		gen.writeStartObject();
		gen.writeStringField('errorMessage', ErrDesc);
		gen.writeStringField('errorCode', ErrCode);
		gen.writeStringField('errorType', ErrType);
		gen.writeEndObject();
		gen.writeEndArray();
		return gen.getAsString();
	}

	/* DTO for Course */
	public class DTOForGetEnrollmentsSts
	{
		public String Name { get; set; }
		public String CourseId { get; set; }
		public String Description { get; set; }
		public String Status { get; set; }
		public String StartDate { get; set; }
		public String CompletedDate { get; set; }
		public String PercentComplete { get; set; }
		public String Duration { get; set; }
		public String LMSID { get; set; }
		public String Type { get; set; }
		public List<tag> Tags { get; set; }
		public List<image> Images { get; set; }
		public String Language { get; set; }
		public List<Category> Category { get; set; }
	}
	public class tag
	{
		public String Id { get; set; }
		public String Name { get; set; }

	}
	public class Category
	{
		public String Id { get; set; }
		public String Name { get; set; }
	}
	public class image
	{
		public String Name { get; set; }
		public String URL { get; set; }
	}

	/* Method for getting userid and userdetails using username from absorblms */
	public static List<CourseDTOFrUpdate> getAvailabeCourses(String UserId)
	{
		HttpResponse response;
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mc = new MockHttpResponseGenerator1();
			response = mc.respond2();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues(ENDPOINT).Value__c + VUSERS + UserId + '/courses', '', 'GET', LmsUtils.authenticate());
		}
		/* This While loop is used to get Course details when session Id gets expired. This loop is not indefinite */
		List<CourseDTOFrUpdate> CourseObjLstFrAvailableCourses = new List<CourseDTOFrUpdate>();
		if (response.getStatusCode() == 200)
		{
			CourseObjLstFrAvailableCourses = (List<CourseDTOFrUpdate>) JSON.deserialize(response.getBody(), List<CourseDTOFrUpdate>.class);
		}
		return CourseObjLstFrAvailableCourses ;
	}

	/* Method for getting course Type based on individual Course from absorblms */
	public static CourseDTOFrUpdate getCourseType(String CourseId)
	{
		HttpResponse response;
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mc = new MockHttpResponseGenerator1();
			response = mc.responseFrCourseType();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues(ENDPOINT).Value__c + VCOURSES + CourseId, '', 'GET', LmsUtils.authenticate());
		}
		CourseDTOFrUpdate ObjLstFrAvailableCourse = new CourseDTOFrUpdate();
		if (response.getStatusCode() == 200)
		{
			ObjLstFrAvailableCourse = (CourseDTOFrUpdate) JSON.deserialize(response.getBody(), CourseDTOFrUpdate.class);
		}
		return ObjLstFrAvailableCourse;
	}

	/* DTO for Course Update */
	public class CourseDTOFrUpdate
	{
		public String Description { get; set; }
		public String Notes { get; set; }
		public String CategoryId { get; set; }
		public String AccessDate { get; set; }
		public Integer ExpireType { get; set; }
		public String ExpiryDate { get; set; }
		public Integer ActiveStatus { get; set; }
		public String LearnerTime { get; set; }
		public String Id { get; set; }
		public String Name { get; set; }
		public String CourseType { get; set; }
		public List<string> TagIds { get; set; }
		public CourseDTOFrUpdate()
		{ /*empty*/
		}
	}
	/* Method for getting course details based on individual userid from absorblms */
	public static List<CourseDTO> getCourse(String UserId)
	{
		HttpResponse response;
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mc = new MockHttpResponseGenerator1();
			response = mc.respondCourseDTO();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues(ENDPOINT).Value__c + VUSERS + UserId + '/enrollments', '', 'GET', LmsUtils.authenticate());
		}
		List<CourseDTO> CourseObjLst = new List<CourseDTO>();
		if (response.getStatusCode() == 200)
		{
			CourseObjLst = (List<CourseDTO>) JSON.deserialize(response.getBody(), List<CourseDTO>.class);
		}
		return CourseObjLst;
	}

	/* DTO for Course */
	public class CourseDTO
	{
		public String Id { get; set; }
		public String CourseId { get; set; }
		public String CourseName { get; set; }
		public Double Progress { get; set; }
		public Double Score { get; set; }
		public String Status { get; set; }
		public String DateCompleted { get; set; }
		public String DateStarted { get; set; }
		public CourseDTO()
		{ /*empty*/
		}
	}

	/*Method is used to enroll the course by API*/
	public static string autoEnrollments(String LMSID, String CourseId)
	{
		HttpResponse response;
		String Retunstatus;
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 MockRes = new MockHttpResponseGenerator1();
			Response = MockRes.AutoEntrollResponse();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues(ENDPOINT).Value__c + VUSERS + LMSID + '/enrollments/' + CourseId, '', 'POST', LmsUtils.authenticate());
		}
		if (response.getStatusCode() == 201)
		{
			Retunstatus = 'Success';
		}
		else
		{
			Retunstatus = 'Failure';
		}
		return Retunstatus;
	}
	/* 
	 * Method for getting Lessons based on ChapterId from absorb lms 
	 */
	public static List<LessonDTOFrUpdate> getLessonsForCourse(String CourseId)
	{
		HttpResponse response;
		List<LessonDTOFrUpdate> lessonOnjLst = new List<LessonDTOFrUpdate>();
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mc = new MockHttpResponseGenerator1();
			response = mc.respond6();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues(ENDPOINT).Value__c + VCOURSES + CourseId + '/lessons', '', 'GET', LmsUtils.authenticate());
		}
		if (response.getStatusCode() == 200)
		{
			lessonOnjLst = (List<LessonDTOFrUpdate>) JSON.deserialize(response.getBody(), List<LessonDTOFrUpdate>.class);
		}
		return lessonOnjLst ;
	}

	public class LessonDTOFrUpdate
	{
		public String Id { get; set; }
		public String Name { get; set; }
		public String Description { get; set; }
		public String Notes { get; set; }
		public String Type { get; set; }
		public String Width { get; set; }
		public String Height { get; set; }
		public String Url { get; set; }
		public String UsePopup { get; set; }
		public String PassingScore { get; set; }
		public String Weight { get; set; }
		public String RefId { get; set; }
		public String ChapterId { get; set; }
	}

	public static list<Categories> getCategories()
	{
		HttpResponse response;

		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mc = new MockHttpResponseGenerator1();
			response = mc.responseFrCategories();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues(ENDPOINT).Value__c + 'v1/Categories', '', 'GET', LmsUtils.authenticate());
		}
		List<Categories> AvailableCategories = new List<Categories>();
		if (response.getStatusCode() == 200)
		{
			AvailableCategories = (List<Categories>) JSON.deserialize(response.getBody(), List<Categories>.class);
		}
		return AvailableCategories ;
	}

	public static List<TagIDs> getTagIDs()
	{
		HttpResponse response;
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mc = new MockHttpResponseGenerator1();
			response = mc.responseFrTags();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues(ENDPOINT).Value__c + 'v1/Tags', '', 'GET', LmsUtils.authenticate());
		}
		List<TagIDs> TagIDObjLstFrAvailableTagIDs = new List<TagIDs>();
		if (response.getStatusCode() == 200)
		{
			TagIDObjLstFrAvailableTagIDs = (List<TagIDs>) JSON.deserialize(response.getBody(), List<TagIDs>.class);
		}
		return TagIDObjLstFrAvailableTagIDs ;
	}

	public class TagIDs
	{
		public string id { get; set; }
		public string Name { get; set; }
	}

	public class Categories
	{
		public string id { get; set; }
		public string Name { get; set; }
		public string ParentId { get; set; }
		public string Description { get; set; }
	}

	/* Method for getting resources for particular course fom absorb lms*/
	public static List<ResourceDTO> getResourcesforCourses(String CourseId)
	{
		List<ResourceDTO> resourceObjLst = new List<ResourceDTO>();
		HttpResponse response;
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mc = new MockHttpResponseGenerator1();
			response = mc.respond5();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues(ENDPOINT).Value__c + VCOURSES + CourseId + '/resources', '', 'GET', LmsUtils.authenticate());
		}
		if (response.getStatusCode() == 200)
		{

			resourceObjLst = (List<ResourceDTO>) JSON.deserialize(response.getBody(), List<ResourceDTO>.class);
		}
		return resourceObjLst;
	}
	/* DTO for Resource */
	public class ResourceDTO
	{
		public String Id { get; set; }
		public String Name { get; set; }
		public String Description { get; set; }
		public String File { get; set; }
		public String ModuleId { get; set; }
		public ResourceDTO()
		{/*empty*/
		}
	}
	/* Method for getting Resources based on ChapterId from absorb lms */
	public static List<ResourcesDTOFrUpdate> getResourcesForCourse(String CourseId)
	{
		HttpResponse response;
		List<ResourcesDTOFrUpdate> resourcesOnjLst = new List<ResourcesDTOFrUpdate>();
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mc = new MockHttpResponseGenerator1();
			response = mc.responseFrgetResourcesForCourse();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues(ENDPOINT).Value__c + VCOURSES + CourseId + '/resources', '', 'GET', LmsUtils.authenticate());
		}
		if (response.getStatusCode() == 200)
		{
			resourcesOnjLst = (List<ResourcesDTOFrUpdate>) JSON.deserialize(response.getBody(), List<ResourcesDTOFrUpdate>.class);
		}
		return resourcesOnjLst ;
	}

	public class ResourcesDTOFrUpdate
	{
		public String Id { get; set; }
		public String Name { get; set; }
		public String Description { get; set; }
		public String File { get; set; }
		public String ModuleId { get; set; }
	}

	webservice static void runBatchClassForCourseUpdate()
	{
		CourseUpdate batchObj = new CourseUpdate();
		Database.executeBatch(batchObj, 1);
	}
	webservice static void runClassFrCategoryandTag()
	{
		LMSTagsandCategoriesScheduler.syncTagsCategories();
	}
}