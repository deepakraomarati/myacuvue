/**
* File Name: LmsUtilsForCurriculum
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : Utility class used from parent classes.
* Copyright (c) $2018 Johnson & Johnson
*/
public with sharing class LmsUtilsForCurriculum
{
	public static final String GENERIC_ERROR = 'GEN0001';
	public static final String GENERIC_ERROR_TYPE = 'Generic';
	static Integer countForAuthenticate = 1;
	static Integer countForGetAvailableCurriculum = 1;

	/**
	 * @description  Method for getting userid and userdetails using username from absorblms
	 */
	public static String getEnrollments(String userId, String progressStatus)
	{
		List<CurriculumDTO> curriculumForParticularUser = new List<CurriculumDTO>();
		Map<Integer, String> mapForStatus = new Map<Integer, String>();
		mapForStatus.put(0, 'Not Started');
		mapForStatus.put(1, 'In Progress');
		mapForStatus.put(2, 'PendingApproval');
		mapForStatus.put(3, 'Completed');
		mapForStatus.put(4, 'NotComplete');
		mapForStatus.put(5, 'Failed');
		mapForStatus.put(6, 'Declined');
		mapForStatus.put(7, 'PendingEvaluationRequired');
		mapForStatus.put(8, 'OnWaitlist');
		mapForStatus.put(9, 'Absent');
		mapForStatus.put(10, 'NotApplicable');
		mapForStatus.put(11, 'PendingProctor');
		Map<Integer, String> mapFrActiveStatus = new Map<Integer, String>();
		mapFrActiveStatus.put(0, 'Active');
		mapFrActiveStatus.put(1, 'Inactive');
		mapFrActiveStatus.put(2, 'Pending');
		Map<Integer, String> mapFrExpireType = new Map<Integer, String>();
		mapFrExpireType.put(0, 'None');
		mapFrExpireType.put(1, 'Date');
		mapFrExpireType.put(2, 'Duration');
		String statusFrAbsorb;
		List<Curriculum__c> CurriculumList = new List<Curriculum__c>();
		Set<String> setCurriculumList = new Set<String>();

		List<CurriculumAssignment__c> curriculumAssignList = new List<CurriculumAssignment__c>();
		map<string, List<string>> mapforTags = new map<string, List<string>>();
		Map<String, String> mapFrCategory = new Map<String, String>();
		Map<String, String> mapFrTag = new Map<String, String>();
		List<String> lstFrCategoryIds = new List<String>();
		List<String> lstFrTagIds = new List<String>();
		if (progressStatus == 'InProgress')
		{
			statusFrAbsorb = '1';
		}
		else if (progressStatus == 'Completion')
		{
			statusFrAbsorb = '3';
		}
		else
		{
			statusFrAbsorb = '0';
		}
		String ProUserIdForAvailableCourses;
		List<Role__c> ProUserRole = new List<Role__c>();
		List<User> proUser = [SELECT Id,Username,Email,Occupation__c,ContactId,LMS_Id__c,Localesidkey FROM User WHERE Id = :userId];
		if (!proUser.isEmpty())
		{
			String proUserCntctId;
			String ProUserId;
			if (proUser[0].LMS_Id__c != '' && proUser[0].LMS_Id__c != null)
			{
				ProUserId = proUser[0].LMS_Id__c;
			}
			proUserCntctId = proUser[0].ContactId;
			ProUserRole = [SELECT id,name FROM Role__c WHERE name = :proUser[0].Occupation__c];
			if (ProUserRole.isEmpty())
			{
				throw new CustomException(LmsUtils.getException(GENERIC_ERROR, GENERIC_ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('GEN0001').Error_Description__c)));
			}
			else
			{
				string AbsorbdummyUser = proUser[0].Localesidkey + '-' + ProUserRole[0].id;
				if (LmsUtils.absorbSettings.containsKey(AbsorbdummyUser))
				{
					ProUserIdForAvailableCourses = LmsUtils.absorbSettings.get(AbsorbdummyUser).Value__c;
				}
				else
				{
					throw new CustomException(LmsUtils.getException(GENERIC_ERROR, GENERIC_ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('GEN0001').Error_Description__c)));
				}
			}
			List<CurriculumDTOFrUpdate> availableCurriculumForParticularUserBeforeFilter = getAvailabeCurriculum(ProUserIdForAvailableCourses);
			List<CurriculumDTOFrUpdate> availableCurriculumForParticularUser = new List<CurriculumDTOFrUpdate>();
			for (CurriculumDTOFrUpdate CurriculumDTOObjBfrFltr : availableCurriculumForParticularUserBeforeFilter)
			{
				if (CurriculumDTOObjBfrFltr.CourseType == 'Curriculum')
				{
					availableCurriculumForParticularUser.add(CurriculumDTOObjBfrFltr);
				}
			}
			if (ProUserId != '' && ProUserId != null)
			{
				curriculumForParticularUser = getcurriculum(ProUserId);
			}
			for (CurriculumDTOFrUpdate CurriculumDTOObj : availableCurriculumForParticularUser)
			{
				mapforTags.put(CurriculumDTOObj.Id, CurriculumDTOObj.TagIds);
				for (String objFrTagIdsLoop : CurriculumDTOObj.TagIds)
				{
					lstFrTagIds.add(objFrTagIdsLoop);
				}
				Curriculum__c Curriculum = new Curriculum__c();
				Curriculum.Name = CurriculumDTOObj.Name;
				Curriculum.LMS_ID__c = CurriculumDTOObj.Id;
				Curriculum.Description__c = CurriculumDTOObj.Description;
				if (CurriculumDTOObj.AccessDate != null)
				{
					Curriculum.AccessDate__c = Date.valueOf(CurriculumDTOObj.AccessDate);
				}
				Curriculum.Notes__c = CurriculumDTOObj.Notes;
				Curriculum.Duration__c = CurriculumDTOObj.LearnerTime;
				Curriculum.CategoryId__c = CurriculumDTOObj.CategoryId;
				lstFrCategoryIds.add(CurriculumDTOObj.CategoryId);
				Curriculum.ExpireType__c = mapFrExpireType.get(CurriculumDTOObj.ExpireType);
				if (CurriculumDTOObj.ExpiryDate != null)
				{
					Curriculum.ExpiryDate__c = Date.valueOf(CurriculumDTOObj.ExpiryDate);
				}
				Curriculum.ActiveStatus__c = mapFrActiveStatus.get(CurriculumDTOObj.ActiveStatus);
				Curriculum.Type__c = CurriculumDTOObj.CourseType;
				CurriculumList.add(Curriculum);
				setCurriculumList.add(CurriculumDTOObj.Id);
			}
			for (CurriculumDTO CurriculumDTOPartObj : curriculumForParticularUser)
			{
				if (!setCurriculumList.contains(CurriculumDTOPartObj.CourseId))
				{
					CurriculumDTOFrUpdate objFrCurriculumDTOFrUpdate = getCourseType(CurriculumDTOPartObj.CourseId);
					String typeForElement = objFrCurriculumDTOFrUpdate.CourseType;
					if (typeForElement == 'Curriculum')
					{
						List<LmsUtils.ResourcesDTOFrUpdate> availableResource = LmsUtils.getResourcesForCourse(CurriculumDTOPartObj.CourseId);
						Map<String, String> mapFrImgs = new Map<String, String>();
						for (LmsUtils.ResourcesDTOFrUpdate holdsResource : availableResource)
						{
							mapFrImgs.put(holdsResource.Name, holdsResource.File);
						}
						mapforTags.put(objFrCurriculumDTOFrUpdate.Id, objFrCurriculumDTOFrUpdate.TagIds);
						for (String objFrTagIdsLoop : objFrCurriculumDTOFrUpdate.TagIds)
						{
							lstFrTagIds.add(objFrTagIdsLoop);
						}
						Curriculum__c Curriculum = new Curriculum__c();
						Curriculum.Name = objFrCurriculumDTOFrUpdate.Name;
						Curriculum.LMS_ID__c = objFrCurriculumDTOFrUpdate.Id;
						Curriculum.Description__c = objFrCurriculumDTOFrUpdate.Description;
						Curriculum.Duration__c = objFrCurriculumDTOFrUpdate.LearnerTime;
						if (objFrCurriculumDTOFrUpdate.AccessDate != null)
						{
							Curriculum.AccessDate__c = Date.valueOf(objFrCurriculumDTOFrUpdate.AccessDate);
						}
						Curriculum.Notes__c = objFrCurriculumDTOFrUpdate.Notes;
						Curriculum.CategoryId__c = objFrCurriculumDTOFrUpdate.CategoryId;
						lstFrCategoryIds.add(objFrCurriculumDTOFrUpdate.CategoryId);
						Curriculum.ExpireType__c = mapFrExpireType.get(objFrCurriculumDTOFrUpdate.ExpireType);
						if (objFrCurriculumDTOFrUpdate.ExpiryDate != null)
						{
							Curriculum.ExpiryDate__c = Date.valueOf(objFrCurriculumDTOFrUpdate.ExpiryDate);
						}
						Curriculum.ActiveStatus__c = mapFrActiveStatus.get(objFrCurriculumDTOFrUpdate.ActiveStatus);
						Curriculum.Type__c = objFrCurriculumDTOFrUpdate.CourseType;
						Curriculum.Main_Course_Image__c = mapFrImgs.get('Main_Course_Image');
						Curriculum.Related_Content_Image__c = mapFrImgs.get('Related_Content_Image');
						Curriculum.Landing_page_Image__c = mapFrImgs.get('Landing_page_Image');
						Curriculum.Online_CoursePage_Image__c = mapFrImgs.get('Online_CoursePage_Image');
						CurriculumList.add(Curriculum);
						setCurriculumList.add(objFrCurriculumDTOFrUpdate.Id);
					}
				}
			}
			Savepoint holdssavepoint = Database.setSavepoint();
			try
			{
				if (!CurriculumList.isEmpty())
				{
					upsert CurriculumList LMS_ID__c;
				}
				List<Categories_Tag__c> lstFrCategory = [SELECT LMS_ID__c, Name FROM Categories_Tag__c WHERE Type__c = 'Category' and LMS_ID__c IN :lstFrCategoryIds];
				for (Categories_Tag__c lstFrCategoryObj : lstFrCategory)
				{
					mapFrCategory.put(lstFrCategoryObj.LMS_ID__c, lstFrCategoryObj.Name);
				}

				List<Categories_Tag__c> lstFrTag = [SELECT LMS_ID__c, Name FROM Categories_Tag__c WHERE Type__c = 'Tag' and LMS_ID__c IN :lstFrTagIds];
				for (Categories_Tag__c lstFrTagObj : lstFrTag)
				{
					mapFrTag.put(lstFrTagObj.LMS_ID__c, lstFrTagObj.Name);
				}
				Map<String, Curriculum__c> mapForCurriculum = new Map<String, Curriculum__c>();
				List<Curriculum__c> listForCourseMapping = [SELECT Id, LMS_ID__c,Description__c,Duration__c,Main_Course_Image__c,Related_Content_Image__c,Landing_page_Image__c,Online_CoursePage_Image__c,Type__c,CategoryId__c FROM Curriculum__c WHERE LMS_ID__c IN :setCurriculumList];
				for (Curriculum__c CurriculumObjVarA : listForCourseMapping)
				{
					mapForCurriculum.put(CurriculumObjVarA.LMS_ID__c, CurriculumObjVarA);
				}
				proUserCntctId = proUser[0].ContactId;
				for (CurriculumDTO CurriculumDTOPartObj : curriculumForParticularUser)
				{
					if (setCurriculumList.contains(CurriculumDTOPartObj.CourseId))
					{
						CurriculumAssignment__c CurriculumAssign = new CurriculumAssignment__c();
						CurriculumAssign.Enrollment_Id__c = CurriculumDTOPartObj.Id;
						CurriculumAssign.Status__c = mapForStatus.get(Integer.valueOf(CurriculumDTOPartObj.Status));
						if (CurriculumDTOPartObj.DateStarted != null)
						{
							CurriculumAssign.StartDate__c = Date.valueOf(CurriculumDTOPartObj.DateStarted);
						}
						CurriculumAssign.Score__c = CurriculumDTOPartObj.Score;
						CurriculumAssign.Progress__c = CurriculumDTOPartObj.Progress;
						CurriculumAssign.CurriculumCustom__c = mapForCurriculum.get(CurriculumDTOPartObj.CourseId).Id;
						CurriculumAssign.Contact__c = proUserCntctId;
						if (CurriculumDTOPartObj.DateCompleted != null)
						{
							CurriculumAssign.CompletionDate__c = Date.valueOf(CurriculumDTOPartObj.DateCompleted);
						}
						curriculumAssignList.add(CurriculumAssign);
					}
				}
				if (!curriculumAssignList.isEmpty())
				{
					upsert curriculumAssignList Enrollment_Id__c;
				}
				List<DTOForGetEnrollmentsSts> DTOLstForGetEnrollmentsSts = new List<DTOForGetEnrollmentsSts>();
				Set<String> setProSiteList = new Set<String>();
				for (CurriculumDTO CurriculumDTOPartObj : curriculumForParticularUser)
				{
					if (setCurriculumList.contains(CurriculumDTOPartObj.CourseId))
					{
						List<tag> tagIdsWithName = preTagList(mapforTags.get(CurriculumDTOPartObj.CourseId), mapFrTag);
						List<Category> CategoryWithName = prepCategoryDTO(mapForCurriculum, mapFrCategory, CurriculumDTOPartObj.CourseId);
						List<image> images = prepImages(mapForCurriculum, CurriculumDTOPartObj.CourseId);
						if ('0'.equals(statusFrAbsorb))
						{
							DTOForGetEnrollmentsSts DTOObjForGetEnrollmentsSts = prepDTOForGetEnrollmentsSts(CurriculumDTOPartObj, mapForCurriculum, mapForStatus, proUser[0].Localesidkey);
							DTOObjForGetEnrollmentsSts.Images = images;
							DTOObjForGetEnrollmentsSts.Category = CategoryWithName;
							DTOObjForGetEnrollmentsSts.Tags = tagIdsWithName;
							DTOLstForGetEnrollmentsSts.add(DTOObjForGetEnrollmentsSts);
						}
						else if ('1'.equals(statusFrAbsorb) && (CurriculumDTOPartObj.Status == '1'))
						{
							DTOForGetEnrollmentsSts DTOObjForGetEnrollmentsSts = prepDTOForGetEnrollmentsSts(CurriculumDTOPartObj, mapForCurriculum, mapForStatus, proUser[0].Localesidkey);
							DTOObjForGetEnrollmentsSts.Images = images;
							DTOObjForGetEnrollmentsSts.Category = CategoryWithName;
							DTOObjForGetEnrollmentsSts.Tags = tagIdsWithName;
							DTOLstForGetEnrollmentsSts.add(DTOObjForGetEnrollmentsSts);

						}
						else if ('3'.equals(statusFrAbsorb) && (CurriculumDTOPartObj.Status == '3'))
						{
							DTOForGetEnrollmentsSts DTOObjForGetEnrollmentsSts = prepDTOForGetEnrollmentsSts(CurriculumDTOPartObj, mapForCurriculum, mapForStatus, proUser[0].Localesidkey);
							DTOObjForGetEnrollmentsSts.Images = images;
							DTOObjForGetEnrollmentsSts.Category = CategoryWithName;
							DTOObjForGetEnrollmentsSts.Tags = tagIdsWithName;
							DTOLstForGetEnrollmentsSts.add(DTOObjForGetEnrollmentsSts);
						}
						setProSiteList.add(mapForCurriculum.get(CurriculumDTOPartObj.CourseId).Id);
					}
				}
				for (CurriculumDTOFrUpdate CurriculumDTOObj : availableCurriculumForParticularUser)
				{
					List<tag> tagIdsWithName = preTagList(mapforTags.get(CurriculumDTOObj.Id), mapFrTag);
					List<Category> CategoryWithName = prepCategoryDTO(mapForCurriculum, mapFrCategory, CurriculumDTOObj.Id);
					List<image> images = prepImages(mapForCurriculum, CurriculumDTOObj.Id);
					if ('0'.equals(statusFrAbsorb))
					{
						DTOForGetEnrollmentsSts DTOObjForGetEnrollmentsSts = new DTOForGetEnrollmentsSts ();
						DTOObjForGetEnrollmentsSts.Name = CurriculumDTOObj.Name;
						DTOObjForGetEnrollmentsSts.LMSID = CurriculumDTOObj.Id;
						DTOObjForGetEnrollmentsSts.Description = mapForCurriculum.get(CurriculumDTOObj.Id).Description__c;
						DTOObjForGetEnrollmentsSts.Status = mapForStatus.get(0);
						DTOObjForGetEnrollmentsSts.CurriculumId = mapForCurriculum.get(CurriculumDTOObj.Id).Id;
						DTOObjForGetEnrollmentsSts.PercentComplete = '0.0';
						DTOObjForGetEnrollmentsSts.Images = images;
						DTOObjForGetEnrollmentsSts.Type = 'Curriculum';
						DTOObjForGetEnrollmentsSts.Duration = mapForCurriculum.get(CurriculumDTOObj.Id).Duration__c;
						DTOObjForGetEnrollmentsSts.Language = proUser[0].Localesidkey;
						DTOObjForGetEnrollmentsSts.Category = CategoryWithName;
						DTOObjForGetEnrollmentsSts.Tags = tagIdsWithName;
						if (!setProSiteList.contains(mapForCurriculum.get(CurriculumDTOObj.Id).Id))
						{
							DTOLstForGetEnrollmentsSts.add(DTOObjForGetEnrollmentsSts);
						}
					}
				}
				return JSON.serializePretty(DTOLstForGetEnrollmentsSts);
			}
			catch (Exception e)
			{
				Database.rollback(holdssavepoint);
				return LmsUtils.getException(GENERIC_ERROR, GENERIC_ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('GEN0001').Error_Description__c));
			}
		}
		else
		{
			return LmsUtils.getException(GENERIC_ERROR, GENERIC_ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('GEN0001').Error_Description__c));
		}
	}
	private static List<Category> prepCategoryDTO(Map<String, Curriculum__c> mapForCurriculum, Map<String, String> mapFrCategory, string CurriculumId)
	{
		List<Category> CategoryWithName = new List<Category>();
		if (mapForCurriculum.get(CurriculumId).CategoryId__c != null)
		{
			Category ObjFrCategory = new Category();
			ObjFrCategory.Id = mapForCurriculum.get(CurriculumId).CategoryId__c;
			ObjFrCategory.Name = mapFrCategory.get(mapForCurriculum.get(CurriculumId).CategoryId__c);
			CategoryWithName.add(ObjFrCategory);
		}
		return CategoryWithName;
	}
	private static DTOForGetEnrollmentsSts prepDTOForGetEnrollmentsSts(CurriculumDTO CurriculumDTOPartObj, Map<String, Curriculum__c> mapForCurriculum, Map<Integer, String> mapForStatus, String Locale)
	{
		DTOForGetEnrollmentsSts DTOObjForGetEnrollmentsSts = new DTOForGetEnrollmentsSts ();
		DTOObjForGetEnrollmentsSts.Name = CurriculumDTOPartObj.CourseName;
		DTOObjForGetEnrollmentsSts.LMSID = CurriculumDTOPartObj.CourseId;
		DTOObjForGetEnrollmentsSts.Description = mapForCurriculum.get(CurriculumDTOPartObj.CourseId).Description__c;
		DTOObjForGetEnrollmentsSts.Status = mapForStatus.get(Integer.valueOf(CurriculumDTOPartObj.Status));
		DTOObjForGetEnrollmentsSts.StartDate = CurriculumDTOPartObj.DateStarted;
		DTOObjForGetEnrollmentsSts.CompletedDate = CurriculumDTOPartObj.DateCompleted;
		DTOObjForGetEnrollmentsSts.PercentComplete = String.valueOf(CurriculumDTOPartObj.Progress);
		DTOObjForGetEnrollmentsSts.CurriculumId = mapForCurriculum.get(CurriculumDTOPartObj.CourseId).Id;
		DTOObjForGetEnrollmentsSts.Type = 'Curriculum';
		DTOObjForGetEnrollmentsSts.Duration = mapForCurriculum.get(CurriculumDTOPartObj.CourseId).Duration__c;
		DTOObjForGetEnrollmentsSts.Language = Locale;
		return DTOObjForGetEnrollmentsSts;
	}
	private static List<image> prepImages(Map<String, Curriculum__c> mapForCurriculum, String CurriculumId)
	{
		List<image> images = new List<image>();
		if (mapForCurriculum.get(CurriculumId) != null)
		{
			if (mapForCurriculum.get(CurriculumId).Main_Course_Image__c != null)
			{
				image img1 = new image();
				img1.Name = 'Main_Course_Image';
				img1.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCurriculum.get(CurriculumId).Main_Course_Image__c;
				images.add(img1);
			}
			if (mapForCurriculum.get(CurriculumId).Related_Content_Image__c != null)
			{
				image img2 = new image();
				img2.Name = 'Related_Content_Image';
				img2.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCurriculum.get(CurriculumId).Related_Content_Image__c;
				images.add(img2);
			}
			if (mapForCurriculum.get(CurriculumId).Landing_page_Image__c != null)
			{
				image img3 = new image();
				img3.Name = 'Landing_page_Image';
				img3.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCurriculum.get(CurriculumId).Landing_page_Image__c;
				images.add(img3);
			}
			if (mapForCurriculum.get(CurriculumId).Online_CoursePage_Image__c != null)
			{
				image img4 = new image();
				img4.Name = 'Online_CoursePage_Image';
				img4.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCurriculum.get(CurriculumId).Online_CoursePage_Image__c;
				images.add(img4);
			}
		}
		return images;
	}
	private static List<tag> preTagList(List<String> tagIds, Map<String, String> mapFrTag)
	{
		List<tag> tagIdsWithName = new List<tag>();
		for (String tag : tagIds)
		{
			tag objFrTag = new tag();
			objFrTag.Id = tag;
			objFrTag.Name = mapFrTag.get(tag);
			tagIdsWithName.add(objFrTag);
		}
		return tagIdsWithName;
	}
	/* DTO for Curriculum */
	public class DTOForGetEnrollmentsSts
	{
		public String Name { get; set; }
		public String CurriculumId { get; set; }
		public String Description { get; set; }
		public String Status { get; set; }
		public String StartDate { get; set; }
		public String CompletedDate { get; set; }
		public String PercentComplete { get; set; }
		public String LMSID { get; set; }
		public String Type { get; set; }
		public List<image> Images { get; set; }
		public List<tag> Tags { get; set; }
		public String Language { get; set; }
		public String Duration { get; set; }
		public List<Category> Category { get; set; }
	}

	public class tag
	{
		public String Id { get; set; }
		public String Name { get; set; }
	}
	public class Category
	{
		public String Id { get; set; }
		public String Name { get; set; }

	}
	public class image
	{
		public String Name { get; set; }
		public String URL { get; set; }
	}

	/* Method for getting course Type based on individual Course from absorblms */
	public static CurriculumDTOFrUpdate getCourseType(String CourseId)
	{
		HttpResponse response;
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mockResponseObject = new MockHttpResponseGenerator1();
			response = mockResponseObject.responseFrGetCourseType();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues('EndPoint').Value__c + 'v1/Courses/' + CourseId, '', 'GET', LmsUtils.authenticate());
		}
		CurriculumDTOFrUpdate ObjLstFrAvailableCurriculum = new CurriculumDTOFrUpdate();
		if (response.getStatusCode() == 200)
		{
			ObjLstFrAvailableCurriculum = (CurriculumDTOFrUpdate) JSON.deserialize(response.getBody(), CurriculumDTOFrUpdate.class);
		}
		return ObjLstFrAvailableCurriculum;
	}
	/* Method for getting course details based on individual userid from absorblms */
	public static List<CurriculumDTO> getcurriculum(String UserId)
	{
		HttpResponse response;
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mockResponseObject = new MockHttpResponseGenerator1();
			response = mockResponseObject.responseForCurriculumEnroll();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues('EndPoint').Value__c + 'v1/users/' + UserId + '/enrollments', '', 'GET', LmsUtils.authenticate());
		}
		List<CurriculumDTO> CurriculumObjLst = new List<CurriculumDTO>();
		if (response.getStatusCode() == 200)
		{
			CurriculumObjLst = (List<CurriculumDTO>) JSON.deserialize(response.getBody(), List<CurriculumDTO>.class);
		}
		return CurriculumObjLst;
	}

	/* DTO for Course */
	public class CurriculumDTO
	{
		public String Id { get; set; }
		public String CourseId { get; set; }
		public String CourseName { get; set; }
		public Double Progress { get; set; }
		public Double Score { get; set; }
		public String Status { get; set; }
		public String DateCompleted { get; set; }
		public String DateStarted { get; set; }

		public CurriculumDTO()
		{/*Empty*/
		}

	}
	/* Method for getting userid and userdetails using username from absorblms */
	public static List<CurriculumDTOFrUpdate> getAvailabeCurriculum(String UserId)
	{
		HttpResponse response;
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mockResponseObject = new MockHttpResponseGenerator1();
			response = mockResponseObject.responseFrgetAvailabeCurriculum();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues('EndPoint').Value__c + 'v1/users/' + UserId + '/courses', '', 'GET', LmsUtils.authenticate());
		}
		List<CurriculumDTOFrUpdate> ObjLstFrAvailableCurriculum = new List<CurriculumDTOFrUpdate>();
		if (response.getStatusCode() == 200)
		{
			ObjLstFrAvailableCurriculum = (List<CurriculumDTOFrUpdate>) JSON.deserialize(response.getBody(), List<CurriculumDTOFrUpdate>.class);
		}
		return ObjLstFrAvailableCurriculum;
	}

	public class CurriculumDTOFrUpdate
	{
		public String Id { get; set; }
		public String Name { get; set; }
		public String Description { get; set; }
		public String Notes { get; set; }
		public String CategoryId { get; set; }
		public String AccessDate { get; set; }
		public Integer ExpireType { get; set; }
		public String ExpiryDate { get; set; }
		public String LearnerTime { get; set; }
		public Integer ActiveStatus { get; set; }
		public String CourseType { get; set; }
		public List<String> TagIds { get; set; }
		public CurriculumDTOFrUpdate()
		{/*empty*/
		}
	}
}