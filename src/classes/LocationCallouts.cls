/**
 * File Name: LocationCallouts 
 * Description : Class which calls Google API by sending address to get Latitude and longitude
 * Copyright : Johnson & Johnson
 * @author : Shobana K G | skg6@its.jnj.com | Shobana.KG@cognizant.com
 * 
 * Modification Log 
 * =============================================================== 
 *  Ver  |Date         |Author                |Modification
 *  1.0  |30-Aug-2018  |skg6@its.jnj.com      |Included call to class 'MockHttpResponseGenerator2' for test coverage 
 */
public class LocationCallouts {
    
    @future (callout=true)  // future method needed to run callouts from Triggers
    static public void getLocation(id accountId){
        // gather account info
        Account a = [SELECT PublicAddress__c, PublicState__c, PublicZone__c, CountryCode__c, Geolocation__Latitude__s, 
                     Geolocation__Longitude__s FROM Account WHERE id =: accountId];
        // Get country codes eligible to have Geolocation loaded
        Map<String,Canvas_Geolocation_CountryCodes__c> countryCodeMap = Canvas_Geolocation_CountryCodes__c.getAll(); 
        
        
        // create an address string
        String address = '';
        if (a.PublicAddress__c != null)
            address += a.PublicAddress__c +', ';
        if (a.PublicState__c != null)
            address += a.PublicState__c +', ';
        if (a.PublicZone__c != null)
            address += a.PublicZone__c;
        
        
        address = EncodingUtil.urlEncode(address, 'UTF-8');
        
        // build callout
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key=AIzaSyDcfK0wz0u8PCv6c2ufEzD4lKZ8L16VL24&sensor=false');
        req.setMethod('GET');
        req.setTimeout(60000);
        
        
        try{
            // callout 
            HttpResponse res = new HTTPResponse();
            if (!Test.isRunningTest()) {
                res = h.send(req);
            } else {
                MockHttpResponseGenerator2 mc = new MockHttpResponseGenerator2();
                res = mc.responseGoogleAPI();
            }
            System.debug('response: ' + res);
            System.debug('response body: ' + res.getBody());
            // parse coordinates from response
            JSONParser parser = JSON.createParser(res.getBody());
            double lat = null;
            double lon = null;
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                    (parser.getText() == 'location')){
                        parser.nextToken(); // object start
                        while (parser.nextToken() != JSONToken.END_OBJECT){
                            String txt = parser.getText();
                            parser.nextToken();
                            if (txt == 'lat')
                                lat = parser.getDoubleValue();
                            else if (txt == 'lng')
                                lon = parser.getDoubleValue();
                        }
                        
                    }
            }
            system.debug('Lat' + lat);
            // update coordinates if we get back
            if (lat != null){
                
                a.GoogleMapX__c = lat;
                a.Latitude__c = string.valueof(lat); 
                system.debug('Latitude' + a.Latitude__c + ' GoogleMapX: ' + a.GoogleMapX__c);
                
                a.GoogleMapY__c = lon;
                a.Longitude__c = string.valueof(lon);
                system.debug('Longitude' + a.Longitude__c + ' GoogleMapY: ' + a.GoogleMapY__c);
                
                // Update Geolocation for accounts with eligible country codes
                a.Geolocation__Latitude__s = (a.CountryCode__c != null && countryCodeMap.containsKey(a.CountryCode__c.toUpperCase())) ? lat : a.Geolocation__Latitude__s;
                a.Geolocation__Longitude__s = (a.CountryCode__c != null && countryCodeMap.containsKey(a.CountryCode__c.toUpperCase())) ? lon : a.Geolocation__Longitude__s;
                System.debug('Geolocation: ' + a.Geolocation__Latitude__s + ' , ' + a.Geolocation__Longitude__s);
                
                update a;
                
            }
            
        } catch (Exception e) {
        }
        
    }
}