public class MA2_AccountDeleteService{
  
     /* Method for creating json body 
     */
    public static void sendApigeeData(List<Account> accList){
    String jsonstringfinal;
        list<JSONstringtosend> jsendlist =new list <JSONstringtosend>();
    List<String> MA2CountryList = new List<String>();
    Map<String, MA2_CountryCode__c> countries = MA2_CountryCode__c.getAll();
    MA2CountryList.addAll(countries.keySet());
    system.debug('MA2CountryList-->>>>>>>>>>-'+MA2CountryList);
    
    list<Account> AppProfile=[select id,My_Acuvue__c,OutletNumber__c,CountryCode__c from Account where id in :accList and CountryCode__c in :MA2CountryList and My_Acuvue__c='No'];
        String jsonString  = '';
        
        if(AppProfile.size() > 0){
            system.debug('AppProfile-->>>>>>>>>>-'+AppProfile);
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            for(Account ac : AppProfile){
                if(ac.CountryCode__c != 'JPN'){
                JSONstringtosend jsend=new JSONstringtosend ();
              //gen.writeStringField('id',ac.id+'');
              jsend.storeId=ac.OutletNumber__c;
              jsend.countryCode=ac.CountryCode__c; 
               jsendlist.add(jsend);
            }
        }
        }
            if(jsendlist.size()>0){
                jsonString=json.serializePretty(jsendlist);
                jsonString = jsonString.replaceAll('null',' "null" ');
                System.Debug('jsonString->>>-'+jsonString);
            }
        
         if (jsonString != null || jsonString != '') 
            {
                	jsonstringfinal = jsonString;
                System.Debug('jsonstringfinal--'+jsonstringfinal);
            }
         
        
        	if(jsonstringfinal != '' &&  jsonstringfinal != null){
            sendtojtracker(jsonstringfinal);
        	}
        	
        
    }
    
    /*
     * Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONObject class
     */
    @future(callout = true)
    public static void sendtojtracker(String jsonBody) {
        JSONObject oauth = null;
       Credientials__c testPub = new Credientials__c();
       if(Credientials__c.getInstance('AccountDeleteAPI') != null){
           testPub  = Credientials__c.getInstance('AccountDeleteAPI');
           if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
                       final String ClientId = testPub.Client_Id__c;
                       final String ClientSecret = testPub.Client_Secret__c;
                       final String TargetUrl = testPub.Target_Url__c;
                       final String ApiKey = testPub.Api_Key__c; 
                       oauth = oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody);
           }
       }    
       /*SonarQube Fix*/	   
       //System.debug('------oauth response------>>>>'+oauth);
        
    }
    
    /*
     * Method for sending data to the Apigee system
     */
    private static JSONObject oauthLogin(String targetUrl, String clientId, String clientSecret, String apiKeyValue,String jsonBody) {
      HttpRequest loginRequest = New HttpRequest();
        loginRequest.setTimeout(120000);
      loginRequest.setMethod('POST');
      loginRequest.setEndpoint(targetUrl);
      loginRequest.setHeader('grant_type', 'authorization_code');
      loginRequest.setHeader('client_id',clientId);
      loginRequest.setHeader('client_secret',clientSecret);
      loginRequest.setHeader('apikey',apiKeyValue);
      loginRequest.setHeader('Content-Type', 'application/json');
     
      system.debug('<<<<jsonString>>>>>'+jsonBody);

      loginRequest.setBody(jsonBody);
      Http Http = New Http();
      HTTPResponse loginResponse = new HTTPResponse();
      if ( !Test.isRunningTest() ){
          loginResponse = http.send(loginRequest);
          JSONObject oAuth = (JSONObject) JSON.deserialize(loginResponse.getbody(), JSONObject.class);
          return oAuth;
      }
      
      System.Debug('loginResponse --'+loginResponse.getBody());
    
      return null;
 }
 
 // Inner class for setting value 
 public class JSONObject {
      public String id {
       get;
       set;
      }
      public String issued_at {
       get;
       set;
      }
      public String instance_url {
       get;
       set;
      }
      public String signature {
       get;
       set;
      }
      public String access_token {
       get;
       set;
      }
   }
    public class JSONstringtosend {
    public String storeId {
        get;set;
    }
    public String countryCode {
        get;set;
    }
    }
}