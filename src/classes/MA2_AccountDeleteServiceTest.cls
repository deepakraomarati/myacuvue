/* 
* File Name: MA2_AccountDeleteServiceTest
* Description : Test class for class: MA2_AccountDeleteService 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    2.0  |30-Aug-2017  |lbhutia@its.jnj.com   |Class Modified
* ===============================================================
*/
@isTest
public class MA2_AccountDeleteServiceTest{
    
    /* Method for excuting the test class */
    static Testmethod void deleteAccontMethod(){
        final Credientials__c cred = new Credientials__c(Name = 'AccountDeleteAPI' , 
                                                   Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                                                   Api_Key__c = 'gfWY5DDcngLvSDIzjM6S1ywKCTAPyUqv' , 
                                                   Target_Url__c = 'https://jnj-global-prod.apigee.net/sfdc/disableStore');
        insert cred;
        
        MA2_CountryCode__c CountryCode = new MA2_CountryCode__c(Name= 'SGP', MA2_CountryCodeValue__c = 'SGP');
        insert CountryCode;
        final TriggerHandler__c mcs = new TriggerHandler__c(name = 'HandleTriggers',CouponContact__c=true);
        insert mcs;
        Test.startTest();
        final List<Account> accList = new List<Account>();
        //accList.addAll(TestDataFactory_MyAcuvue.createAccount(1));
        for(Integer i = 0;i<200;i++){
        final Account acc=new Account();
        acc.name = 'Test Account';
       // acc.Id = accList[0].Id;
        acc.OutletNumber__c='1234'+i;
        acc.CountryCode__c='SGP';
        acc.ECP_Name__c = 'Test';
        acc.PublicAddress__c = 'Block 111, Test Drive';
        acc.PublicPhone__c = '61234567';
        acc.PublicZone__c = 'HOUGANG/ SERANGOON';
        acc.PublicState__c = 'HOUGANG/ SERANGOON';
        acc.My_Acuvue__c = 'No';
        acc.CountryCode__c = 'SGP';
            accList.add(acc);
        }
        insert accList;
        system.assertEquals(accList[0].CountryCode__c, 'SGP','Success');      
        Test.stopTest();
    }
}