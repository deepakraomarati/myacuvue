/**
* File Name: MA2_AccountService
* Description : Sending Account Info to Apigee System
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |21-Mar-2016 |hsingh53@its.jnj.com  |New Class created
*/
public class MA2_AccountService{
    
    public void accountInfo(List<Account> accountList){
        String jsonstringfinal;
        String jsonString;
        list<JSONstringtosend> jsendlist =new list <JSONstringtosend>();
        List<String> MA2CountryList = new List<String>();
        Map<String, MA2_CountryCode__c> countries = MA2_CountryCode__c.getAll();
        MA2CountryList.addAll(countries.keySet());
        system.debug('MA2CountryList-->>>>>>>>>>-'+MA2CountryList);
        
        List<Account> accList = [select id,Name,MA2_AccountPassword__c,Account_Short_Name__c,ActiveYN__c,MA2_AccountId__c,MA2_Business_Hours__c,
                                 MA2_ChinaTrialAstig__c,MA2_ChinaTrialBeauty__c,MA2_ChinaTrialCommon__c,ContactName__c,CountryCode__c,createdDate,
                                 CreditLimit__c,Currency__c,Description__c,ECP_Name__c,ECPType__c,Email__c,eTrialSMSYN__c,eTrialYN__c,fax,
                                 MA2_FeedbackLike__c,MA2_FeedbackRating__c,GoogleMapX__c,GoogleMapY__c,Mobile_Number__c,lastModifiedDate,My_Acuvue__c,
                                 OutletNumber__c,phone,PublicAddress__c,PublicPhone__c,PublicState__c,PublicZone__c,SalesRep__c,AccountNumber,
                                 shippingStreet,shippingCity,ShippingCountry,shippingPostalCode,Territory1__c,Priority__c 
                                 from Account 
                                 where id in :accountList and CountryCode__c in:MA2CountryList and ActiveYN__c = true and My_Acuvue__c = 'Yes'];
        
        if(accList.size() > 0 ){
            JSONGenerator jsonBody = JSON.createGenerator(true);
            jsonBody.writeStartObject();
            for(Account acc : accList){
                JSONstringtosend jsend=new JSONstringtosend ();
                jsend.accountName= acc.Name.replace('NULL','');
                jsend.accountPassword=acc.MA2_AccountPassword__c;
                jsend.accountShortName=acc.Account_Short_Name__c;
                jsend.activeYN=acc.ActiveYN__c;
                jsend.apigeeStoreId=acc.MA2_AccountId__c;
                jsend.businessHours=acc.MA2_Business_Hours__c;
                jsend.chinaTrialAstig=acc.MA2_ChinaTrialAstig__c;
                jsend.chinaTrialBeauty= acc.MA2_ChinaTrialBeauty__c;
                jsend.chinaTrialCommon=acc.MA2_ChinaTrialCommon__c;
                jsend.contactName= acc.ContactName__c;
                jsend.countryCode= acc.CountryCode__c;
                jsend.createdDate= acc.createdDate;
                jsend.creditLimit=acc.CreditLimit__c;
                jsend.currency1= acc.Currency__c;
                jsend.description= acc.Description__c;
                jsend.ecpName= acc.ECP_Name__c;
                jsend.ecpType= acc.ECPType__c;
                jsend.email=acc.Email__c;
                jsend.eTrialSMSYN=acc.eTrialSMSYN__c;
                jsend.eTrialYN= acc.eTrialYN__c;
                jsend.fax=acc.fax;
                jsend.feedbackLike= acc.MA2_FeedbackLike__c;
                jsend.feedbackRating=acc.MA2_FeedbackRating__c;
                jsend.googleMapX= acc.GoogleMapX__c;
                jsend.googleMapY= acc.GoogleMapY__c;
                jsend.mobileNumber= acc.Mobile_Number__c;
                jsend.lastModifiedDate= acc.lastModifiedDate;
                jsend.myAcuvue= acc.My_Acuvue__c;
                jsend.outletNumber= acc.OutletNumber__c;
                jsend.phone= acc.phone;
                jsend.publicAddress= acc.PublicAddress__c;
                jsend.publicPhone=acc.PublicPhone__c;
                jsend.publicState= acc.PublicState__c;
                jsend.PublicZone= acc.PublicZone__c;
                jsend.salesRepAlias= acc.SalesRep__c;
                jsend.SFDC_AccountNumber= acc.AccountNumber;
                jsend.shippingStreet=acc.shippingStreet;
                jsend.shippingCity= acc.shippingCity;
                jsend.ShippingCountry= acc.ShippingCountry;
                jsend.shippingPostalCode= acc.shippingPostalCode;
                jsend.territory=acc.Territory1__c;
                jsend.priority= acc.Priority__c;
                jsendlist.add(jsend);
            }  
        }
            if(jsendlist.size()>0){
                jsonString=json.serializePretty(jsendlist);
                jsonString = jsonString.replaceAll('null',' "null" ');
                System.Debug('jsonString->>>-'+jsonString);
            }
        
            if (jsonString != null || jsonString != '') 
            {
                	jsonstringfinal = jsonString;
                System.Debug('jsonstringfinal--'+jsonstringfinal);
            }
         
        
        	if(jsonstringfinal != '' &&  jsonstringfinal != null){
            sendRecord(jsonstringfinal);
        	}
        	
}

/*
* Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONObject class
*/   
@future(callout = true)
private static void sendRecord(String jsonbody){
    JSONObject oauth = null;
    Credientials__c testPub = new Credientials__c();
    if(Credientials__c.getInstance('AccountService') != null){
        testPub  = Credientials__c.getInstance('AccountService');
        if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
            final String ClientId = testPub.Client_Id__c;
            final String ClientSecret = testPub.Client_Secret__c;
            final String TargetUrl = testPub.Target_Url__c;
            final String ApiKey = testPub.Api_Key__c; 
            oauth = oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody);
        }
    }  
}

/*
* Method for sending data to the Apigee system
*/  
private static JSONObject oauthLogin(String targetUrl ,String clientSecret ,String clientId ,String apiKeyValue ,String jsonbody){
    HTTPRequest req = new HTTPRequest();
    req.setTimeout(120000);
    req.setEndpoint(targetUrl);
    req.setMethod('POST');
    req.setHeader('grant_Type','authorization_code');
    req.setHeader('client_id',clientId);
    req.setHeader('client_secret',clientSecret);
    req.setHeader('apiKey',apiKeyValue);
    req.setHeader('Content-Type', 'application/json');
    req.setBody(jsonbody);
    HTTP http = new HTTP();
    HTTPResponse response = new HTTPResponse();
    if ( !Test.isRunningTest() ){
        response = http.send(req);
        JSONObject oAuth = (JSONObject) JSON.deserialize(response.getbody(), JSONObject.class);
        return oAuth;
    }
    
    System.Debug('response--'+response.getBody());
    system.debug('<<<<jsonString>>>>>'+jsonBody);
    return null;
}

// Inner class for setting value 
public class JSONObject {
    public String id {
        get;
        set;
    }
    public String issued_at {
        get;
        set;
    }
    public String instance_url {
        get;
        set;
    }
    public String signature {
        get;
        set;
    }
    public String access_token {
        get;
        set;
    }
}
public class JSONstringtosend {
    public String accountName {
        get;set;
    }
    public String accountPassword {
        get;set;
    }
    public String accountShortName {
        get;set;
    }
    public Boolean activeYN {
        get;set;
    }
    public String apigeeStoreId {
        get;set;
    }
    public String businessHours {
        get;set;
    }
    public Boolean chinaTrialAstig {
        get;set;
    }
    public Boolean chinaTrialBeauty {
        get;set;
    }
    public Boolean chinaTrialCommon {
        get;set;
    }
    public String contactName {
        get;set;
    }
    public String countryCode {
        get;set;
    }
    public DateTime createdDate {
        get;set;
    }
    public String creditLimit {
        get;set;
    }
    public String currency1 {
        get;set;
    }
    public String description {
        get;set;
    }
    public String ecpName {
        get;set;
    }
    public String ecpType {
        get;set;
    }
    public String email {
        get;set;
    }
    public Boolean eTrialSMSYN {
        get;set;
    }
    public Boolean eTrialYN {
        get;set;
    }
    public String fax {
        get;set;
    }
    public Decimal feedbackLike {
        get;set;
    }
    public Decimal feedbackRating {
        get;set;
    }
    public Decimal googleMapX {
        get;set;
    }
    public Decimal googleMapY {
        get;set;
    }
    public String mobileNumber {
        get;set;
    }
    public DateTime lastModifiedDate {
        get;set;
    }
    public String myAcuvue {
        get;set;
    }
    public String outletNumber {
        get;set;
    }
    public String phone {
        get;set;
    }
    public String publicAddress {
        get;set;
    }
    public String publicPhone {
        get;set;
    }
    public String publicState {
        get;set;
    }
    public String PublicZone {
        get;set;
    }
    public String salesRepAlias {
        get;set;
    }
    public String SFDC_AccountNumber {
        get;set;
    }
    public String shippingStreet {
        get;set;
    }
    public String shippingCity {
        get;set;
    }
    public String ShippingCountry {
        get;set;
    }
    public String shippingPostalCode {
        get;set;
    }
    public String territory {
        get;set;
    }
    public Decimal priority {
        get;set;
    }
}

}