/* 
* File Name: MA2_AccountServiceTest
* Description : Test class for class: MA2_AccountServiceTest 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    2.0  |30-Aug-2017  |lbhutia@its.jnj.com   |Class Modified
* ===============================================================
*/
@isTest
public class MA2_AccountServiceTest{
    
    /* Method for excuting the test class */
    static Testmethod void accountMethod(){
        final Credientials__c cred = new Credientials__c(Name = 'AccountService' , 
                                                         Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                                                         Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                                                         Target_Url__c = 'https://jnj-dev.apigee.net/v1/sfdc/consumerstorerelation/add');
        insert cred;
         MA2_CountryCode__c CountryCode = new MA2_CountryCode__c(Name= 'SGP', MA2_CountryCodeValue__c = 'SGP');
        insert CountryCode;
        final TriggerHandler__c mcs = new TriggerHandler__c(name = 'HandleTriggers',CouponContact__c=true);
        insert mcs;
        List<Account> acList = new List<Account>();
        for(Integer i = 0;i<200;i++){
       final Account acc=new Account();
        acc.name = 'Test Account'+i;
       // acc.Id = accList[0].Id;
        acc.OutletNumber__c='1234'+i;
        acc.CountryCode__c='SGP';
        acc.ECP_Name__c = 'Test';
        acc.PublicAddress__c = 'Block 111, Test Drive';
        acc.PublicPhone__c = '61234567';
        acc.PublicZone__c = 'HOUGANG/ SERANGOON';
        acc.PublicState__c = 'HOUGANG/ SERANGOON';
        acc.My_Acuvue__c = 'Yes';
        acc.CountryCode__c = 'SGP';
        acc.ActiveYN__c = true;
            acList.add(acc);
        }
        
        insert acList;
        
        system.assertequals(acList[0].name,'Test Account0','Success');   
        
    }
}