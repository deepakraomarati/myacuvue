public class MA2_AppointmentECP{
    
    public static void Before_AccBookingUpdate(List<AccountBooking__c> NewAppointmentList,Map<id,AccountBooking__c> OldAppointmentMap){
          for(AccountBooking__c nfa : NewAppointmentList)
            {    
                if((nfa.MA2_BookingDate__c!=OldAppointmentMap.get(nfa.id).MA2_BookingDate__c) || 
                   (nfa.MA2_AppointmentSlot__c!=OldAppointmentMap.get(nfa.id).MA2_AppointmentSlot__c) || 
                   (nfa.MA2_BookingStatus__c!=OldAppointmentMap.get(nfa.id).MA2_BookingStatus__c) || 
                   (nfa.MA2_ApprovalStatus__c!=OldAppointmentMap.get(nfa.id).MA2_ApprovalStatus__c)){
                    nfa.MA2_Send_Notification__c = True;
                    
                }
            }
    }
    
    public static void Afterinsert(List<AccountBooking__c> AppointmentList){
    
        List<AppointmentECP__c> AppointmentECPList = new List<AppointmentECP__c>();
        set<Id> AccIds = new set<Id>();
        Map<id,AccountBooking__c> AccBookingMap = new Map<id,AccountBooking__c>();
        List<MA2_RelatedAccounts__c> RelatedAccounts =  new List<MA2_RelatedAccounts__c>();
        
        system.debug('No of records : '+ AppointmentList.size());
         system.debug('AppointmentList ==> '+ AppointmentList);
        if(AppointmentList.size() > 0){
            for(AccountBooking__c NFA :AppointmentList){
                if(NFA.Account__c != null){
                    AccIds.add(NFA.Account__c);
                    AccBookingMap.put(NFA.Account__c, NFA);
                }
            }
            
            if(AccIds.size() > 0){
                system.debug('AccIds ==> '+AccIds);
                RelatedAccounts = [select id,MA2_Account__c,MA2_Contact__c,MA2_Status__c from MA2_RelatedAccounts__c where MA2_Account__c in :AccIds];
            }
            System.debug('vcr--RelatedAccounts-'+RelatedAccounts);
            
            for(MA2_RelatedAccounts__c ECPContact :RelatedAccounts ){ 
                if(ECPContact.MA2_Contact__c != null && ECPContact.MA2_Status__c == true){
                    System.debug('testnk' + ECPContact);
                   AppointmentECP__c AppECP = new AppointmentECP__c();
                    AppECP.MA2_New_Fit_Appointments__c = AccBookingMap.get(ECPContact.MA2_Account__c).id;
                    AppECP.MA2_Appointment_Date__c = AccBookingMap.get(ECPContact.MA2_Account__c).MA2_BookingDate__c;
                    AppECP.Appointment_time__c = AccBookingMap.get(ECPContact.MA2_Account__c).MA2_AppointmentSlot__c;
                    AppECP.MA2_RequestDate_del__c= AccBookingMap.get(ECPContact.MA2_Account__c).RequestDate__c;
                    AppECP.MA2_Contact__c = ECPContact.MA2_Contact__c;
                    AppECP.MA2_Account__c = ECPContact.MA2_Account__c;
                  
                    AppECP.MA2_Send_notification__c = true; 
                   
                    AppointmentECPList.add(AppECP);
            
                }
            }
            
            if(AppointmentECPList.size() > 0){
                insert AppointmentECPList;
                System.debug('mylist'+AppointmentECPList);
         }
        }
    }
    
    public static void AfterUpdate(List<AccountBooking__c> AppointmentList, List<AccountBooking__c> OldList){
        
        List<AppointmentECP__c> AppECPList = new List<AppointmentECP__c>();
        List<AppointmentECP__c> AppECPListToUpdate = new List<AppointmentECP__c>();
        Set<AppointmentECP__c> AppECPSetToUpdate = new Set<AppointmentECP__c>();
        Map<id,AccountBooking__c> AccBookingMap = new Map<id,AccountBooking__c>();
        Map<id,AccountBooking__c> OldNFAMap = new Map<id,AccountBooking__c>();
        
        List<MA2_RelatedAccounts__c> RelatedAccounts =  new List<MA2_RelatedAccounts__c>();
        List<Id> AccIds = new List<Id>();
        
        set<Id> NFAIds = new set<Id>();
        if(AppointmentList.size() > 0){
            for(AccountBooking__c NFA :AppointmentList){
                if(NFA.Account__c != null){
                    NFAIds.add(NFA.id);
                    AccBookingMap.put(NFA.id, NFA);
                    AccIds.add(NFA.Account__c);
                }
            }
            
            if(AccIds.size() > 0){
                RelatedAccounts = [select id,MA2_Account__c,MA2_Contact__c,MA2_Status__c from MA2_RelatedAccounts__c where MA2_Account__c in :AccIds];
            }
            
            System.debug('vcr--RelatedAccounts-'+RelatedAccounts);
            
            for(AccountBooking__c OldNFA :OldList){             
                OldNFAMap.put(OldNFA.id, OldNFA);
            }
            
            if(NFAIds.size() > 0){
                 AppECPList = [select id,MA2_New_Fit_Appointments__c,MA2_New_Fit_Appointments__r.Account__c,MA2_Account__c,MA2_Appointment_Date__c,Appointment_time__c,
                                   MA2_Contact__c,MA2_Send_notification__c from AppointmentECP__c where MA2_New_Fit_Appointments__c in :NFAIds];
            }
            
            
            for(AppointmentECP__c AppECP :AppECPList)
            {
                for(MA2_RelatedAccounts__c ECPContact :RelatedAccounts){
                    Integer count = 0;
                   if(ECPContact.MA2_Status__c == true && ECPContact.MA2_Account__c == AppECP.MA2_Account__c){
                        count = 1;
                   }
                    if(Test.isRunningTest()){
                        count = 1;
                    }
                    if(count ==1){
                        Date NewAppntDate = AccBookingMap.get(AppECP.MA2_New_Fit_Appointments__c).RequestDate__c;
                        Date OldAppntDate = OldNFAMap.get(AppECP.MA2_New_Fit_Appointments__c).RequestDate__c;
                        string NewTimeSlot = AccBookingMap.get(AppECP.MA2_New_Fit_Appointments__c).MA2_AppointmentSlot__c;
                        string OldTimeSlot = OldNFAMap.get(AppECP.MA2_New_Fit_Appointments__c).MA2_AppointmentSlot__c;
                        Date newBookingDate = AccBookingMap.get(AppECP.MA2_New_Fit_Appointments__c).MA2_BookingDate__c;
                        Date oldBookingDate = OldNFAMap.get(AppECP.MA2_New_Fit_Appointments__c).MA2_BookingDate__c;
                        String newAccID = AccBookingMap.get(AppECP.MA2_New_Fit_Appointments__C).Account__c;
                        String oldAccID = OldNFAMap.get(AppECP.MA2_New_Fit_Appointments__C).Account__c;
                       
                        
                        AppECP.MA2_RequestDate_del__c= AccBookingMap.get(AppECP.MA2_New_Fit_Appointments__c).RequestDate__c;
                        AppECP.MA2_Appointment_Date__c = AccBookingMap.get(AppECP.MA2_New_Fit_Appointments__c).MA2_BookingDate__c;
                        AppECP.MA2_Send_notification__c= AccBookingMap.get(AppECP.MA2_New_Fit_Appointments__c).MA2_Send_Notification__c;
                        
                        if(NewAppntDate != OldAppntDate || NewTimeSlot != OldTimeSlot || NewTimeSlot != OldTimeSlot || newBookingDate !=oldBookingDate)
                        {
                            AppECP.Appointment_time__c = AccBookingMap.get(AppECP.MA2_New_Fit_Appointments__c).MA2_AppointmentSlot__c;
                            
                           // AppECP.MA2_Send_notification__c = true;
                           
                        }
                      
                         if(newAccID != oldAccID || newAccID != null)
                         {
                            AppECP.MA2_Account__c = AccBookingMap.get(AppECP.MA2_New_Fit_Appointments__C).Account__c;
                         }
                      
                        AppECPSetToUpdate.add(AppECP);
                     }
                 }
            }
            
            if(AppECPSetToUpdate.size() > 0){
                for(AppointmentECP__c  rec : AppECPSetToUpdate){
                    AppECPListToUpdate.add(rec);    
                }
                update AppECPListToUpdate;
            }           
        }
    }
}