@isTest
Public class MA2_AppointmentECPTest
{
    static testMethod void CurrencySetupTest()
    {
        TestDataFactory_MyAcuvue.insertCustomSetting();
        
        TriggerHandler__c record = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                         MA2_createContact__c = false);
        insert record;
        
        Account acc = new Account(Name = 'Test' , MA2_AccountId__c = '123456' ,AccountNumber = '54321',OutletNumber__c = '54321',ActiveYN__c = true,My_Acuvue__c = 'Yes',CountryCode__c = 'SGP');
        insert acc;
        
        Id ECPRecTypeId = [select Id from RecordType where Name = 'ECP' and sObjectType= 'Contact'].Id;
        
        Contact con3 = new Contact(LastName = 'testECP' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '234567',
                                   RecordTypeId  = ECPRecTypeId  ,
                                   AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = 'SGD', 
                                   MembershipNo__c = 'SGP1234', MA2_Country_Code__c = 'SGP', MA2_Grade__c='Base',
                                   MA2_PreAssessment__c = true);
        insert con3;
        
        MA2_RelatedAccounts__c relAcc = new MA2_RelatedAccounts__c();
        relAcc.MA2_Account__c = acc.Id;
        relAcc.MA2_Contact__c = con3.Id;
        relAcc.MA2_Status__c = true; 
        insert relAcc;
        
        AccountBooking__c NFA = new AccountBooking__c(Name = 'Test', Account__c = acc.id,
                                                      RequestDate__c = date.newinstance(2017,06,20), MA2_AppointmentSlot__c = '03:04');   
        insert NFA;
        
        AccountBooking__c NFA1 = new AccountBooking__c(Name = 'Test',Account__c = acc.id,
                                                       RequestDate__c = date.newinstance(2017,06,20), MA2_AppointmentSlot__c = '03:04');   
        insert NFA1;
        NFA1.MA2_AppointmentSlot__c = '04:05';
        system.assertEquals(NFA1.MA2_AppointmentSlot__c,'04:05','success');
    }
}