public class MA2_AppointmentService{
    
    public static void sendData(List<AccountBooking__c> appointmentList){
        list<AccountBooking__c> bookList=[select lastmodifiedBy.Name,Id,Account__c,MA2_BookingDate__c,MA2_AppointmentSlot__c,MA2_ApprovalStatus__c,MA2_BookingStatus__c,MA2_ReasonForCancellation__c,
                                          Account__r.OutletNumber__c,MA2_VisitReason__c,MA2_CountryCode__c,Contact__c,Contact__r.MembershipNo__c from AccountBooking__c where id in:appointmentList];
        final List<AccountBooking__c> accBookingFilterList = new List<AccountBooking__c>();    
        for(AccountBooking__c con : bookList){
            String userName = con.lastmodifiedBy.Name;
            userName = userName.toLowercase();
            if(!userName.contains('web')){
                accBookingFilterList.add(con);        
            }
        }
        
        if(accBookingFilterList.size() != 0){
            sendApigeeData(accBookingFilterList);
        }
    }
    
    /* Method for creating json body */
    public static void sendApigeeData(List<AccountBooking__c> appointmentList){
        list<AccountBooking__c> AppProfile=[select id,Name,MA2_BookingId__c,Contact__c,Contact__r.MembershipNo__c,Account__c,Account__r.OutletNumber__c,MA2_BookingDate__c,MA2_AppointmentSlot__c,MA2_ApprovalStatus__c,MA2_BookingStatus__c,MA2_ReasonForCancellation__c,MA2_VisitReason__c,MA2_CountryCode__c from AccountBooking__c where id in:appointmentList];
        String jsonString  = '';
        String firststring,secondstring,thirdstring,bookingDate,appointment;
        list<String> finaldate = new list<string>();
        Integer year,month,day; 
        Date bookingDateField ;       
        Long bookingDateUTC, startTimeUTC, endTimeUTC;
        //The offset is same for SGP, HKG, TWN. For other regions we will have to change this implementation.
        Long TimeOffset = 28800000;
        list<string> splitstring = new list<string> ();
        if(appointmentList.size() > 0){
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            for(AccountBooking__c ab : AppProfile){
                gen.writeStringField('bookingId',ab.MA2_BookingId__c+'');  
                gen.writeStringField('consumerId',ab.Contact__r.MembershipNo__c+'');
                gen.writeStringField('appointmentTime',ab.MA2_AppointmentSlot__c+'');
                gen.writeStringField('approvalStatus',ab.MA2_ApprovalStatus__c+''); 
                gen.writeStringField('bookingStatus',ab.MA2_BookingStatus__c+'');
                gen.writeStringField('rejectionReason',ab.MA2_ReasonForCancellation__c+'');
                gen.writeStringField('storeId',ab.Account__r.OutletNumber__c+'');
                gen.writeStringField('reason',ab.MA2_VisitReason__c+'');
                gen.writeStringField('region',ab.MA2_CountryCode__c+'');
                appointment = string.valueof(ab.MA2_AppointmentSlot__c);
                splitstring = appointment.split(':',2);
                secondstring = string.valueof(splitstring[0]);
                thirdstring = string.valueof(splitstring[1]);
                secondstring = secondstring.remove('(');
                thirdstring = thirdstring.remove('(');
                secondstring = secondstring.remove(')');
                thirdstring = thirdstring.remove(')');
                bookingDateField = ab.MA2_BookingDate__c;
                bookingDate = string.valueof(bookingDateField.format()) ;
                bookingDate = bookingDate.replace('/','-');
                gen.writeStringField('bookingDate',bookingDate+'');
                finaldate = bookingDate.split('-');               
                year = Integer.valueOf(finaldate[2]);
                month = Integer.valueOf(finaldate[1]);
                day = Integer.valueof(finaldate[0]);
                DateTime bookingDateDT = DateTime.newInstanceGmt(year, month, day, 0, 0, 0);             
                DateTime startTime = DateTime.newInstanceGmt(year, month, day, Integer.valueOf(secondstring), 0, 0);
                DateTime endTime = DateTime.newInstanceGmt(year, month, day, Integer.valueOf(thirdstring), 0, 0);
                bookingDateUTC = bookingDateDT.getTime();
                bookingDateUTC = bookingDateUTC-TimeOffset;
                startTimeUTC = startTime.getTime();
                startTimeUTC = startTimeUTC-TimeOffset;
                endTimeUTC = endTime.getTime(); 
                endTimeUTC = endTimeUTC-TimeOffset;                              
                gen.writeStringField('bookingDateUTC',bookingDateUTC+'');
                gen.writeStringField('bookingStartUTC',startTimeUTC+'');
                gen.writeStringField('bookingEndUTC',endTimeUTC+'');
            }
            gen.writeEndObject();
            jsonString = jsonString + gen.getAsString();
        }
        System.Debug('jsonString--'+jsonString);
        if(jsonString != ''){
            sendtojtracker(jsonString);
        }
    }
    
    /* Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONObject class */
    @future(callout = true)
    public static void sendtojtracker(String jsonBody) {
        JSONObject oauth = null;
        Credientials__c testPub = new Credientials__c();
        if(Credientials__c.getInstance('NewAppointment') != null){
            testPub  = Credientials__c.getInstance('NewAppointment');
            if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
                final String ClientId = testPub.Client_Id__c;
                final String ClientSecret = testPub.Client_Secret__c;
                final String TargetUrl = testPub.Target_Url__c;
                final String ApiKey = testPub.Api_Key__c; 
                oauth = oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody);
            }
        }   
	    /*SonarQube Fix*/	   
        //System.debug('------oauth response------>>>>'+oauth);
        
    }
    
    /* Method for sending data to the Apigee system */
    private static JSONObject oauthLogin(String targetUrl, String clientId, String clientSecret, String apiKeyValue,String jsonBody) {
        HttpRequest loginRequest = New HttpRequest();
        loginRequest.setMethod('POST');
        loginRequest.setEndpoint(targetUrl);
        loginRequest.setHeader('grant_type', 'authorization_code');
        loginRequest.setHeader('client_id',clientId);
        loginRequest.setHeader('client_secret',clientSecret);
        loginRequest.setHeader('apikey',apiKeyValue);
        loginRequest.setHeader('Content-Type', 'application/json');
        system.debug('<<<<jsonString>>>>>'+jsonBody);        
        loginRequest.setBody(jsonBody);
        Http Http = New Http();
        HTTPResponse loginResponse = new HTTPResponse();
        if ( !Test.isRunningTest() ){
            loginResponse = http.send(loginRequest);
            JSONObject oAuth = (JSONObject) JSON.deserialize(loginResponse.getbody(), JSONObject.class);
            return oAuth;
        }    
        system.Debug('loginResponse --'+loginResponse.getBody());        
        return null;
    }
    
    // Inner class for setting value 
    public class JSONObject {
        public String id {
            get;
            set;
        }
        public String issued_at {
            get;
            set;
        }
        public String instance_url {
            get;
            set;
        }
        public String signature {
            get;
            set;
        }
        public String access_token {
            get;
            set;
        }
    }
}