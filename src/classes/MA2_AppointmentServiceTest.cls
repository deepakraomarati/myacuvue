/* 
* File Name: MA2_AppointmentServiceTest
* Description : Test class for MA2_AppointmentService 
* Modification Log
* ================================================================
*    Ver  |Date         |Author                |Modification
*    2.0  |30-Aug-2017  |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_AppointmentServiceTest{
    
    /* Method for excuting the test class */
    static Testmethod void createAppointmentTest(){
        
        final List<AccountBooking__c> accList = new List<AccountBooking__c>();
        TestDataFactory_MyAcuvue.insertCustomSetting();
        Test.startTest();
        accList.addAll(TestDataFactory_MyAcuvue.createFitAppointment(1));
        final AccountBooking__c accBook = new AccountBooking__c();
        accBook.Id = accList[0].Id;
        accBook.Name = 'TestName';
        accBook.MA2_BookingId__c='123';
        accBook.MA2_AppointmentSlot__c='10:11';
        accBook.MA2_BookingDate__c= system.today();
        update accBook; 
        system.assertEquals(accBook.MA2_BookingId__c, '123','Success');              
        Test.stopTest();
    }
}