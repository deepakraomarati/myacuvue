Public class MA2_Attachment{
   
    public static void insertAttachment(list<Attachment> attachList)
    {
        Set<Id> setTextContactIds = new Set<Id>();
        Set<Id> setTextContactIdsReqd = new Set<Id>();    
        
        for(Attachment objAttachment: attachList)
        {
            setTextContactIds.add(objAttachment.ParentId);
        }
        
        for(MA2_TextContent__c objTextContent:[SELECT Id, (SELECT Id FROM Attachments) 
                                               FROM MA2_TextContent__c
                                               WHERE Id IN:setTextContactIds])
        {
            if(!objTextContent.Attachments.isEmpty())
            {
                setTextContactIdsReqd.add(objTextContent.Id);
            }
        }
        
        for(Attachment objAttachment: attachList)
        {
            if(setTextContactIdsReqd.contains(objAttachment.ParentId))
            {
                objAttachment.addError('Only one Attachment can be added for one Text Content Record');
            }
        }
    }
}