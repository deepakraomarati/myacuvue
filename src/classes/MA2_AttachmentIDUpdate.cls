public class MA2_AttachmentIDUpdate 
{
	  public static void updateAttachmentid(List<Attachment> attList) 
   		{
            List<Id> parentIdList = new List<Id>();
            List<MA2_CountryWiseConfiguration__c> cwcList = new List<MA2_CountryWiseConfiguration__c>();
            for(Attachment att : attList) 
            {
                parentIdList.add(att.Parentid);         
            }
            List<MA2_CountryWiseConfiguration__c> newsList = [select id,MA2_Value__c,(Select id from Attachments) From MA2_CountryWiseConfiguration__c Where id in :parentidList];
            for(MA2_CountryWiseConfiguration__c news : newsList)
                {
                String temp = '';
                String strpone;
                for(Attachment att : news.Attachments) 
                {
                    temp = temp + att.id + ',';
                    strpone = temp.substring(0,temp.length()-1);
                }
                news.MA2_Value__c = temp; 
                cwcList.add(news);
                }
            update cwcList;
                
    	} 
}