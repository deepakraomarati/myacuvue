@isTest
public class MA2_AttachmentIDUpdateTest 
{
	Public static Testmethod void AttachmentTest()
    {	   
        MA2_CountryWiseConfiguration__c CWC1 = new MA2_CountryWiseConfiguration__c();
        CWC1.MA2_Value__c = 'test';
        CWC1.Name = 'TestCWC1';
        CWC1.MA2_Target__c = 'TestTarget';
        insert CWC1;   
        
        Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
        attach.parentId=CWC1.id;
        insert attach;
		List<Attachment> attachments=[select id, name from Attachment where parentid=:CWC1.id];
        System.assertEquals(1, attachments.size());
        CWC1.MA2_Value__c = attach.id;
        update CWC1;
        delete attach;
    }
}