@isTest
public class MA2_Attachment_Test
{    
    static testMethod void validateAttachment() 
    {
        TestDataFactory_MyAcuvue.insertCustomSetting();
        final List<MA2_TextContent__c> textContentList = new List<MA2_TextContent__c>();
        textContentList.addAll(TestDataFactory_MyAcuvue.createTextContent(1));
        
        Attachment attach = new Attachment();     
        attach.Name = 'Unit Test Attachment';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        attach.body = bodyBlob;
        attach.parentId =textContentList[0].id; 
        insert attach;
        system.assertEquals(attach.Name,'Unit Test Attachment','success');     
    }
}