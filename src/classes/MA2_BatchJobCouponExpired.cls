/*
* File Name: MA2_BatchJobCouponExpired 
* Description : Batch job for expiring the Coupon record
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |23-Sep-2016 |hsingh53@its.jnj.com  |New Class created
*/

global class MA2_BatchJobCouponExpired implements Database.Batchable<sObject>, Database.Stateful{
    
    /* Variable declaration */
    global list<string> success_error_list = new list<string>();
    global integer initsuccesscount=0,finalsuccesscount=0,initfailurecount=0,finalfailedcount=0;
    
    /* Method for querying all the coupon record that are expired */  
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([select Id,Name,MA2_Inactive__c,MA2_CouponUsed__c,CouponId__c,CouponId__r.RecordType.Name,CouponId__r.DiscountPrice__c,ContactId__r.MA2_Country_Code__c,CouponId__r.CountdownRate__c,CouponId__r.MA2_BonusMultiplier__c,MA2_Country_Code__c from CouponContact__c where MA2_Inactive__c =: false
                                         and ExpiryDate__c <: system.today() and MA2_CouponUsed__c!=:true and MA2_Country_Code__c != 'KOR']);
    }
    
    /* Method for excuting the query record */  
    global void execute(Database.batchableContext BC,List<CouponContact__c> couponList){
        final List<CouponContact__c> couponExpired = new List<CouponContact__c>();
        final List<CouponContact__c> createCouponConRecord = new List<CouponContact__c>();
        final List<Coupon__c> coupondiminishList = new List<Coupon__c>();
        
        /* Map for Coupon Type Value(Bonus Multiplier/Cash Discount with Diminishing Value) having key as coupon country code */
        final Map<String,List<CouponContact__c>> coupondiminishMap = new Map<String,List<CouponContact__c>>();
        
        /* Map for Coupon Type Value(Bonus Multiplier Value) having key as coupon country code */
        
        final Map<String,List<CouponContact__c>> couponBonusMap = new Map<String,List<CouponContact__c>>();
        final Set<String> countDownRateList = new Set<String>();
        if(couponList.size() > 0){
            for(CouponContact__c coupon : couponList){
                coupon.MA2_Inactive__c = true;
                if(coupon.CouponId__r.RecordType.Name == 'Bonus Multiplier with Diminishing Value' || 
                   coupon.CouponId__r.RecordType.Name == 'Cash Discount with Diminishing Value'){
                       countDownRateList.add(coupon.CouponId__r.CountdownRate__c);
                       if(coupondiminishMap.containsKey(coupon.ContactId__r.MA2_Country_Code__c)){
                           coupondiminishMap.get(coupon.ContactId__r.MA2_Country_Code__c).add(coupon);    
                       }else{
                           coupondiminishMap.put(coupon.ContactId__r.MA2_Country_Code__c,new List<CouponContact__c>{coupon});
                       } 
                   }else if(coupon.CouponId__r.RecordType.Name == 'Bonus Multiplier'){
                       countDownRateList.add(coupon.CouponId__r.CountdownRate__c);
                       if(couponBonusMap.containsKey(coupon.ContactId__r.MA2_Country_Code__c)){
                           couponBonusMap.get(coupon.ContactId__r.MA2_Country_Code__c).add(coupon);    
                       }else{
                           couponBonusMap.put(coupon.ContactId__r.MA2_Country_Code__c,new List<CouponContact__c>{coupon});
                       }  
                   }
                couponExpired.add(coupon);
            }
        }
        /* Marking the Coupon Inactive field as true if the expired date is less than today date */
        
        if(couponExpired.size() > 0){
            Database.SaveResult[] updateResults = Database.update(couponExpired, false);
            for(Integer i=0;i<updateResults.size();i++){
                if (updateResults.get(i).isSuccess()){                      
                    finalsuccesscount = initsuccesscount + 1;
                    initsuccesscount = finalsuccesscount;
                    // Operation was successful, so get the ID of the record that was processed
                    success_error_list.add('Coupon Wallet ID -->' +' '+updateResults.get(i).getId()+ ': ' + 'Coupon Wallet Expired -->'+ ' ' +couponExpired.get(i).Name);                
                }
                else if (!updateResults.get(i).isSuccess())
                {
                    finalfailedcount = initfailurecount + 1;
                    initfailurecount= finalfailedcount;
                    // Operation failed, so get all errors                     
                    Database.Error error = updateResults.get(i).getErrors().get(0);
                    success_error_list.add('Coupon Wallet ID-->'+' '+ couponExpired.get(i).Name +': ' + 'Error Code -->' + ' '+ error.getStatusCode() + ': '+ 'Error Message -->' + ' ' + error.getMessage());
                }
            }
        }
        
        /* Querying all the Coupon Record having Coupon Type (Bonus Multiplier/Cash Discount with Diminishing Value) AND (Bonus Multiplier Value) and Count Down Rate should be same as already assign Coupon to the Coupon Wallet */
        
        coupondiminishList.addAll(MA2_CouponSelector.couponTypeList(countDownRateList  , coupondiminishMap)); 
        
        /* Map for Price having Key as Coupon Type and Value as Discount Price */
        
        final Map<String,List<Decimal>> priceMap = new Map<String,List<Decimal>>(); 
        
        /* Map for Price having Key as Coupon Type and Value as Bonus Multiplier */
        
        final Map<String,List<Decimal>> bonusMap = new Map<String,List<Decimal>>();  
        for(Coupon__c cp : coupondiminishList){
            if(cp.RecordType.Name == 'Bonus Multiplier with Diminishing Value' || 
               cp.RecordType.Name == 'Cash Discount with Diminishing Value'){
                   if(priceMap.containsKey(cp.CountdownRate__c)){
                       priceMap.get(cp.CountdownRate__c).add(cp.DiscountPrice__c);    
                   }else{
                       priceMap.put(cp.CountdownRate__c,new List<Decimal>{cp.DiscountPrice__c});
                   }
               }if(cp.RecordType.Name == 'Bonus Multiplier'){
                   if(bonusMap.containsKey(cp.CountdownRate__c)){
                       bonusMap.get(cp.CountdownRate__c).add(cp.MA2_BonusMultiplier__c);    
                   }else{
                       bonusMap.put(cp.CountdownRate__c,new List<Decimal>{cp.MA2_BonusMultiplier__c});
                   }    
               }
        }
        for(Coupon__c coupon  : coupondiminishList){
            if(!coupondiminishMap.isEmpty()){
                List<CouponContact__c> diminshCouponWalletList = coupondiminishMap.get(coupon.MA2_CountryCode__c);
                if(diminshCouponWalletList.size () > 0 && (coupon.RecordType.Name == 'Bonus Multiplier with Diminishing Value' || 
                                                           coupon.RecordType.Name == 'Cash Discount with Diminishing Value')){
                                                               for(CouponContact__c couponCon : diminshCouponWalletList){
                                                                   if(couponCon.CouponId__r.CountdownRate__c == coupon.CountdownRate__c && couponCon.CouponId__r.RecordType.Name == coupon.RecordType.Name){
                                                                       List<Decimal> priceValue = priceMap.get(coupon.CountdownRate__c);
                                                                       Integer index = 0;
                                                                       Integer count = 0 ;
                                                                       for(Decimal price :  priceValue){
                                                                           count++;
                                                                           if(price == couponCon.CouponId__r.DiscountPrice__c){
                                                                               index = count;    
                                                                           }
                                                                       }
                                                                       index = index - 1;
                                                                       if(index < priceValue.size() && index != priceValue.size()-1){
                                                                           if(coupon.DiscountPrice__c == priceValue.get(index+1)){
                                                                               Date dt = System.Today();
                                                                               CouponContact__c createCouponCon = new CouponContact__c();
                                                                               createCouponCon.CouponId__c = coupon.Id;
                                                                               createCouponCon.ContactId__c = couponCon.ContactId__c; 
                                                                               createCouponCon.ExpiryDate__c = dt.addDays(Integer.ValueOf(coupon.MA2_CouponValidity__c)); 
                                                                               createCouponConRecord.add(createCouponCon);   
                                                                           }
                                                                       }
                                                                   }
                                                               }
                                                           } 
            }
            if(!couponBonusMap.isEmpty() && coupon.RecordType.Name == 'Bonus Multiplier'){
                List<CouponContact__c> bonusCouponWalletList = couponBonusMap.get(coupon.MA2_CountryCode__c);
                if(bonusCouponWalletList.size () > 0){
                    for(CouponContact__c couponCon : bonusCouponWalletList){
                        if(couponCon.CouponId__r.CountdownRate__c == coupon.CountdownRate__c && couponCon.CouponId__r.RecordType.Name == coupon.RecordType.Name){
                            List<Decimal> bonusValue = bonusMap.get(coupon.CountdownRate__c);
                            Integer index = 0;
                            Integer count = 0 ;
                            for(Decimal bonus :  bonusValue){
                                count++;
                                if(bonus == couponCon.CouponId__r.MA2_BonusMultiplier__c){
                                    index = count;    
                                }
                            }
                            index = index - 1;
                            if(index < bonusValue.size()  && index != bonusValue.size()-1){
                                if(coupon.MA2_BonusMultiplier__c == bonusValue.get(index+1)){
                                    Date dt = System.Today();
                                    CouponContact__c createCouponCon = new CouponContact__c();
                                    createCouponCon.CouponId__c = coupon.Id;
                                    createCouponCon.ContactId__c = couponCon.ContactId__c; 
                                    createCouponCon.ExpiryDate__c = dt.addDays(Integer.ValueOf(coupon.MA2_CouponValidity__c)); 
                                    createCouponConRecord.add(createCouponCon);   
                                }
                            }
                        }
                    }
                } 
            }
        }
        if(createCouponConRecord.size() > 0){
            insert createCouponConRecord;
        }
    } 
    
    /* finish method */
    global void finish(Database.batchableContext BC){
        string body = '',errorbody='';
        body += 'Hello Admin,' + '\n';
        body += '\n';
        body += 'The coupon expiry date is successfully updated in salesforce' + '\n';
        body += '\n';
        body += 'There were' + ' '+ finalsuccesscount + ' ' + 'successful upserts and' + ' ' + finalfailedcount + ' ' + 'errors. ' + '\n';
        body += '\n';
        body += 'To find out more review attached success_error logs '+ ' '+ '\n';
        body += '\n';
        for(integer i=0; i< success_error_list.size(); i++){
            errorbody += success_error_list[i] + '\n';
            errorbody += '\n';
        }        
        body += '\n';	
        body += 'Regards,'+ '\n';
        body += '\n';
        body += 'Skywalker Support Team' + '\n';
        body += '\n';
        body += 'Note:- This email is an automatic email send by  SFDC Interface. Please don’t not respond to this';             
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'lbhutia@its.jnj.com','nkamal8@its.jnj.com','ledward4@ITS.JNJ.com'};
            mail.setToAddresses(toAddresses);
        mail.setSubject('Coupon Expiry Batch Job');
        if(!String.isBlank(body) && !String.isBlank(errorbody)){
        String myString = string.valueof(errorbody);
        Blob myBlob = Blob.valueof(myString);
        
        Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
        efa.setFileName('Success_Erorr Log.txt');
        efa.setBody(myblob);
        mail.setFileAttachments(new List<Messaging.Emailfileattachment>{efa});		
        mail.setPlainTextBody(body);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
	    }
    }
}