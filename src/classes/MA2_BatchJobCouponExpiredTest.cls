/**
* File Name: MA2_BatchJobCouponExpiredTest
* Description : Test class for Batch Job of Coupon Expired 
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* Modification Log 
* ================================================================
*    Ver  |Date         |Author                |Modification
*    2.0  |30-Aug-2017  |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_BatchJobCouponExpiredTest{
    
    /* Method for excuting the test class */
    static Testmethod void runBatchJobMethod(){
        
        TestDataFactory_MyAcuvue.insertCustomSetting();
        final List<Account> accList = new List<Account>();
        final List<Contact> conList = new List<Contact>();
        final List<Coupon__c> coupon = new List<Coupon__c>();
        
        accList.addAll(TestDataFactory_MyAcuvue.createAccount(1));
        system.assertEquals(accList.size(), 4,'Success');                           
        conList.addAll(TestDataFactory_MyAcuvue.createContact(1,accList));
        coupon.addAll(TestDataFactory_MyAcuvue.createCoupon(1));
        TestDataFactory_MyAcuvue.createCouponWallet(1,coupon,conList);
        Test.startTest();       
        Database.executeBatch(new MA2_BatchJobCouponExpired(),200);        
        Test.stopTest();
    }
}