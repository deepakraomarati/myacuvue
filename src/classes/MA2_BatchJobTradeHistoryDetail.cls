global class MA2_BatchJobTradeHistoryDetail implements Database.Batchable<sObject>{
    
    /*
     *Method for querying all the Trade History Record in order to create a Transaction Record 
     */
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Account__c,AccPointAmt__c,AcvPayAmt__c,AcvPayBalAf__c,AcvPayBalBf__c,
                                         AcvPayChrgAmt__c,Aws_ContactId__c,Aws_OutletNumber__c,BuyAmt__c,Contact__c,CouponDcAmt__c,
                                         CouponList__c,CurrencyIsoCode,Id,IsDeleted,LastActivityDate,
                                         LastReferencedDate,logCode__c,
                                         MembershipNo__c,Name,PointAmt__c,PointBalAf__c,PointBalBf__c,
                                         productListDesc__c,ProductList__c,productQuantity__c,SfdcId__c,
                                         SubTrxId__c,THD_Createddate__c,TransactionId__c,
                                         TrTotalAmt__c,TrxStatDesc__c,TrxStat__c,createdDate,lastModifiedDate
                                         FROM TradeHistoryDetails__c]);
    }
    
    /*
     * Method for excuting the query record.
     */
    global void execute(Database.batchableContext BC,List<TradeHistoryDetails__c> tradeList){
        List<TransactionTd__c> transDetailList = new List<TransactionTd__c>();
        List<MA2_TransactionProduct__c> transProdDetailList = new List<MA2_TransactionProduct__c>();
        Map<Id,TradeHistoryDetails__c> tradeHistoryMap = new Map<Id,TradeHistoryDetails__c>();
        List<Contact> contactList = new List<Contact>();
        Set<String> tradeIdSet = new Set<String>();
        Map<String,Id> tradeMap = new Map<String,Id>();
        Set<String> contactAwsList = new Set<String>();
        Map<String,String> contactMap = new Map<String,String>();
        for(TradeHistoryDetails__c tradeRec  : tradeList){
            tradeIdSet.add(tradeRec.Id);  
            if(tradeRec.Aws_ContactId__c != null){
                contactAwsList.add(tradeRec.Aws_ContactId__c);    
            }  
        }
        
        if(contactAwsList.size() > 0){
            contactList = [select MembershipNo__c,Aws_ContactId__c from Contact where Aws_ContactId__c in: contactAwsList];
        }
        
        if(contactList.size() > 0){
            for(Contact con :  contactList){
                contactMap.put(con.Aws_ContactId__c ,con.MembershipNo__c);    
            }
        }
        
        List<TransactionTd__c> transactionList = [select Id,MA2_TradeHistoryId__c from TransactionTd__c where 
                                                  MA2_TradeHistoryId__c in: tradeIdSet];
                                                  
        for(TransactionTd__c trans : transactionList){
            if(!Test.isRunningTest()){
                 tradeMap.put(trans.MA2_TradeHistoryId__c , trans.Id);      
            }  
        }
        if(tradeList.size() > 0){
            for(TradeHistoryDetails__c tradeRec : tradeList){
                if(!tradeMap.containsKey(tradeRec.Id)){
                    tradeHistoryMap.put(tradeRec.id,tradeRec);
                    TransactionTd__c transDetail = new TransactionTd__c();
                    transDetail.MA2_TransactionType__c = 'Manual Adjustment';
                    if(contactMap.containsKey(tradeRec.Aws_ContactId__c)){
                        transDetail.MA2_ContactId__c = contactMap.get(tradeRec.Aws_ContactId__c);
                    }
                    transDetail.MA2_AccountId__c = tradeRec.Aws_OutletNumber__c;
                    transDetail.MA2_MembershipNo__c = tradeRec.MembershipNo__c;
                    transDetail.MA2_CouponList__c = tradeRec.CouponList__c;
                    transDetail.MA2_Price__c = tradeRec.BuyAmt__c;
                    transDetail.MA2_PointAmt__c = tradeRec.PointAmt__c; 
                    transDetail.MA2_AcvPayAmt__c = tradeRec.AcvPayAmt__c;
                    transDetail.MA2_CouponDcAmt__c = tradeRec.CouponDcAmt__c;
                    //transDetail.MA2_PaymentType__c = tradeRec.TrxStatDesc__c;
                    transDetail.MA2_Points__c = tradeRec.AccPointAmt__c;   
                    transDetail.MA2_ProductList__c = tradeRec.ProductList__c;
                    transDetail.MA2_TradeHistoryId__c = tradeRec.Id;
                    transDetail.MA2_Created_Date__c = tradeRec.THD_Createddate__c;
                    transDetail.MA2_Modified_Date__c = tradeRec.lastModifiedDate;
                    transDetail.TransactionId__c = tradeRec.TransactionId__c;
                    transDetail.MA2_CountryCode__c = 'KOR';
                    transDetail.TrxStat__c = tradeRec.TrxStat__c;
                    transDetail.TrxStatDesc__c = tradeRec.TrxStatDesc__c;
                    transDetailList.add(transDetail);
                }
            }
        }
        
        if(transDetailList.size() > 0){
            insert transDetailList;
            Set<String> prodNameList = new Set<String>();
            
            for(TransactionTd__c trans : transDetailList){
                if(trans.MA2_ProductList__c != null){
                    if(trans.MA2_ProductList__c.contains(',')){
                        String[] arrayValue = trans.MA2_ProductList__c.split(',');
                        System.Debug('arrayValue ---'+arrayValue );
                        for(String rec : arrayValue){
                            prodNameList.add(rec);  
                        } 
                    }else{
                        prodNameList.add(trans.MA2_ProductList__c);    
                    }    
                }
            }
            
            Map<String,Product2> prodMapKey = new map<String,Product2>();
            List<Product2> productList = [select Name,Id,UPC_Code__c,Price__c from Product2 where UPC_Code__c in: prodNameList];
            System.Debug('productList--'+productList);
            for(Product2 prod : productList){
                prodMapKey.put(prod.UPC_Code__c,prod);    
            }
            //System.Debug('prodMapKey--'+prodMapKey );
            for(TransactionTd__c trans : transDetailList){
                if(trans.MA2_ProductList__c != null){
                    TradeHistoryDetails__c  tradeRec = tradeHistoryMap.get(trans.MA2_TradeHistoryId__c);
                    if(tradeRec.ProductList__c == trans.MA2_ProductList__c && trans.MA2_ProductList__c.contains(',')){
                        String[] arrayValue = trans.MA2_ProductList__c.split(',');
                        System.Debug('arrayValue ---'+arrayValue );
                        for(String rec : arrayValue){
                            if(prodMapKey.containsKey(rec)){
                                MA2_TransactionProduct__c transProdDetail = new MA2_TransactionProduct__c();
                                transProdDetail.MA2_Transaction__c = trans.Id;
                                transProdDetail.MA2_productQuantity__c = 1;
                                if(contactMap.containsKey(tradeRec.Aws_ContactId__c)){
                                    transProdDetail.MA2_ContactId__c = contactMap.get(tradeRec.Aws_ContactId__c);
                                }
                                transProdDetail.MA2_AccountId__c = tradeRec.Aws_OutletNumber__c;
                                transProdDetail.MA2_ProductId__c = rec;
                                transProdDetail.MA2_ProductName__c = prodMapKey.get(rec).Id;
                                transProdDetailList.add(transProdDetail);
                            }
                        }
                    }else if(tradeRec.ProductList__c == trans.MA2_ProductList__c && !trans.MA2_ProductList__c.contains(',') && prodMapKey.get(trans.MA2_ProductList__c) != null){
                                MA2_TransactionProduct__c transProdDetail = new MA2_TransactionProduct__c();
                                transProdDetail.MA2_Transaction__c = trans.Id;
                                transProdDetail.MA2_productQuantity__c = tradeRec.productQuantity__c;
                                if(contactMap.containsKey(tradeRec.Aws_ContactId__c)){
                                    transProdDetail.MA2_ContactId__c = contactMap.get(tradeRec.Aws_ContactId__c);
                                }
                                transProdDetail.MA2_AccountId__c = tradeRec.Aws_OutletNumber__c;
                                transProdDetail.MA2_ProductId__c = tradeRec.ProductList__c;
                                transProdDetail.MA2_ProductName__c = prodMapKey.get(trans.MA2_ProductList__c).Id;
                                transProdDetailList.add(transProdDetail);
                    }
                }
            }
        }
        //System.Debug('transProdDetailList--'+transProdDetailList);
        if(transProdDetailList.size() > 0){
            insert transProdDetailList;
            //System.Debug('transProdDetailList---'+transProdDetailList);
        }
        
    }
    
    /*
     * finish method
     */
    global void finish(Database.batchableContext BC){
    }
}