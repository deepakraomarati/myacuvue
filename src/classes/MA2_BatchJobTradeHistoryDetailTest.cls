/**
* File Name: MA2_BatchJobCouponExpiredTest
* Description : Test class for Batch Job of Coupon Expired 
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |8-Sep-2016  |hsingh53@its.jnj.com  |New Class created
*/
@isTest
public class MA2_BatchJobTradeHistoryDetailTest{
    static Testmethod void runBatchJobMethod(){
        
        TriggerHandler__c triggerHandler = new TriggerHandler__c(Name = 'HandleTriggers');
        insert triggerHandler;
        
        Id recordTypeIdValue = [select Id from recordType where DeveloperName = 'Consumer' and sObjectType = 'Contact' limit 1].Id;
        final List<Account> accList = new List<Account>();
        accList.addAll(TestDataFactory_MyAcuvue.createAccount(1));
        final Contact conRecord = new Contact(RecordTypeId = recordTypeIdValue ,LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',
                                              AccountId = accList[0].Id,Gender__c = 'Male', 
                                              email = 'test@gmail.com', phone = '123456789',MA2_Country_Code__c = 'KOR',
                                              Aws_ContactId__c = '123456790',MA2_PreAssessment__c = true);
        insert conRecord;
        
        Product2 prodRecord1 = new Product2();
        prodRecord1.Name = 'Test1';
        prodRecord1.UPC_Code__c = '123456789012';
        prodRecord1.ExternalId__c = 'KR11-733912345';
        prodRecord1.MA2_Plant__c ='KR11';
        insert prodRecord1;
        
        Product2 prodRecord2 = new Product2();
        prodRecord2.Name = 'Test1';
        prodRecord2.UPC_Code__c = '123456789013';
        prodRecord2.ExternalId__c = 'KR11-733912345345';
        prodRecord2.MA2_Plant__c ='KR11';
        insert prodRecord2;
        
        Product2 prodRecord = new Product2();
        prodRecord.Name = 'Test1';
        prodRecord.UPC_Code__c = '12345678901111';
        prodRecord.ExternalId__c = 'KR11-733912345333333';
        prodRecord.MA2_Plant__c ='KR11';
        insert prodRecord;
        
        TradeHistory__c tradeHistoryRecord = new TradeHistory__c();
        tradeHistoryRecord.Account__c = accList[0].Id;
        tradeHistoryRecord.AccPointAmt__c = 25;
        tradeHistoryRecord.AcvPayAmt__c = 25;
        tradeHistoryRecord.AcvPayBalAf__c = 25;
        tradeHistoryRecord.AcvPayBalBf__c = 25;
        tradeHistoryRecord.AcvPayChrgAmt__c = 25;
        tradeHistoryRecord.BuyAmt__c = 25;
        tradeHistoryRecord.Contact__c = conRecord.Id;
        tradeHistoryRecord.CouponDcAmt__c = 25;
        tradeHistoryRecord.PointAmt__c = 25;
        tradeHistoryRecord.PointBalAf__c = 25;
        tradeHistoryRecord.PointBalBf__c = 25;
        tradeHistoryRecord.ProductList__c = '123456789012,123456789013';
        tradeHistoryRecord.productQuantity__c = 10;
        tradeHistoryRecord.THD_Createddate__c = System.Now();
        tradeHistoryRecord.TrTotalAmt__c = 10;
        tradeHistoryRecord.Aws_ContactId__c = '123456790';
        tradeHistoryRecord.TrxStatDesc__c = 'test';
        insert tradeHistoryRecord;
        
        TradeHistory__c tradeHistoryRecord1 = new TradeHistory__c();
        tradeHistoryRecord1.Account__c = accList[0].Id;
        tradeHistoryRecord1.AccPointAmt__c = 25;
        tradeHistoryRecord1.AcvPayAmt__c = 25;
        tradeHistoryRecord1.AcvPayBalAf__c = 25;
        tradeHistoryRecord1.AcvPayBalBf__c = 25;
        tradeHistoryRecord1.AcvPayChrgAmt__c = 25;
        tradeHistoryRecord1.BuyAmt__c = 25;
        tradeHistoryRecord1.Contact__c = conRecord.Id;
        tradeHistoryRecord1.CouponDcAmt__c = 25;
        tradeHistoryRecord1.PointAmt__c = 25;
        tradeHistoryRecord1.PointBalAf__c = 25;
        tradeHistoryRecord1.PointBalBf__c = 25;
        tradeHistoryRecord1.ProductList__c = '12345678901111';
        tradeHistoryRecord1.productQuantity__c = 10;
        tradeHistoryRecord1.THD_Createddate__c = System.Now();
        tradeHistoryRecord1.TrTotalAmt__c = 10;
        tradeHistoryRecord1.Aws_ContactId__c = '123456790';
        tradeHistoryRecord1.TrxStatDesc__c = 'test';
        insert tradeHistoryRecord1;
        
        tradeHistoryRecord1.TrxStatDesc__c = 'testing';
        update tradeHistoryRecord1;
        system.assertEquals(tradeHistoryRecord1.TrxStatDesc__c,'testing','success');
        
        Test.startTest();
        Database.executeBatch(new MA2_BatchJobTradeHistoryDetail(),100);       
        Test.stopTest();
    }
}