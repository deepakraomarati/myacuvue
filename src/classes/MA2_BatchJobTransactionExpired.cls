/**
* File Name: MA2_BatchJobTransactionExpired 
* Description : Batch job for expiring the Transaction record
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |23-Sep-2016 |hsingh53@its.jnj.com  |New Class created
*/

global class MA2_BatchJobTransactionExpired implements Database.Batchable<sObject>{

    /*
     *Method for querying all the Transaction record having created date equal to 13 month
     */
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([select MA2_PointsExpired__c,CreatedDate from TransactionTd__c where MA2_PointsExpired__c =: false
                                         and MA2_Points__c != 0]);
    }
    
    /*
     * Method for excuting the query record.
     */
    global void execute(Database.batchableContext BC,List<TransactionTd__c> transactionList){
        Date todayDate = System.Today();
        final List<TransactionTd__c> transactionExpired = new List<TransactionTd__c>();
        if(transactionList.size() > 0){
            for(TransactionTd__c trans : transactionList){
                Date dd = Date.newinstance(trans.CreatedDate.year(),trans.CreatedDate.month(),trans.CreatedDate.day());
                Date compDate = dd.addMonths(13);
                System.Debug('todayDate.month()----'+todayDate.month());
                System.Debug('compDate.month()'+compDate.month());
                if(todayDate > compDate ){
                    System.Debug('compDate1111111 ---'+compDate);
                    trans.MA2_PointsExpired__c  = true;
                    transactionExpired.add(trans);
                }
            }
        }
        if(transactionExpired.size() > 0){
            update transactionExpired;
        }
    }
    
    /*
     * finish method
     */
    global void finish(Database.batchableContext BC){
    }
}