global class MA2_BatchSingaporAssignBirthCoupon implements Database.Batchable<Sobject>{
    
    global static final List<CouponContact__c> couponContactList = new List<CouponContact__c>();
    global static final List<String> countryCodeList = new List<String>();
        
    global Database.QueryLocator start(Database.batchableContext BC){
        
        Integer CurrentMonth = System.Today().Month();
        Map<String,MA2_CountryCode__c> countryCodeMap = MA2_CountryCode__c.getAll();
        for(MA2_CountryCode__c conCode : countryCodeMap.values()){
            countryCodeList.add(conCode.MA2_CountryCodeValue__c);
        }
        if(countryCodeList.size() > 0){
             return Database.getQueryLocator([select Id,AccountId,DOB__c,MA2_Grade__c,MA2_Country_Code__c, MA2_PreAssessment__c from Contact 
                    where MA2_PreAssessment__c = true AND MA2_Country_Code__c in: countryCodeList]);
        }else{
            return null;
        }
    }
    
    global static void execute(Database.batchableContext BC , List<Contact> contactList){
    
        Map<Id,Set<CouponContact__c>> activeContactCouponMap = new Map<Id,Set<CouponContact__c>>();
        Map<String,MA2_CountryCode__c> countryCodeMap = MA2_CountryCode__c.getAll();
        Map<String,String> countrCodeMap = new Map<String,String>();
        Map<Id,CouponContact__c> IgnoreCoupnContMap = new Map<Id,CouponContact__c>();
        List<Contact> FinalConList = new List<Contact>();
        Date CurrentDate = System.Today();
        Integer ThisYear = CurrentDate.year();
        List<contact> TempContactList = new list<contact>();
        Integer CurrentMonth = System.Today().Month();
        
        for(MA2_CountryCode__c conCode : countryCodeMap.values()){
            countryCodeList.add(conCode.MA2_CountryCodeValue__c);
            countrCodeMap.put(conCode.MA2_CountryCodeValue__c,conCode.MA2_CountryCodeValue__c);
        }
        
        for(Contact item: contactlist){
            Datetime BirthdayMonth = item.DOB__c;
            if(item.DOB__c != null && BirthdayMonth.Month() == CurrentMonth){
                TempContactList.add(item);        
            }
        }
               
        if(TempContactList.size() > 0 && countryCodeList.size() > 0){
            List<Coupon__c> couponRecord = [select Id,MA2_CouponValidity__c,MA2_CouponName__c, MA2_CountryCode__c from Coupon__c 
                                        where (MA2_CouponName__c = '1.5x Birthday Points' or MA2_CouponName__c = '2x Birthday Points' or MA2_CouponName__c = '【生日禮券】雙倍點數')
                                        and MA2_TimeToAward__c = 'Campaign'
                                        and RecordType.Name = 'Bonus Multiplier' and MA2_CountryCode__c in: countryCodeList];
                                        
            Map<String,List<Coupon__c>> couponMap = new Map<String,List<Coupon__c>>();
            for(Coupon__c coupon : couponRecord){
                if(couponMap.containsKey(coupon.MA2_CountryCode__c)){
                        couponMap.get(coupon.MA2_CountryCode__c).add(coupon); 
                }else{
                        couponMap.put(coupon.MA2_CountryCode__c,new List<Coupon__c>{coupon});
                }    
            }
            
            List<CouponContact__c> couponConList = [select ContactId__c,Id, CreatedDate, MA2_CouponUsed__c,MA2_Inactive__c from CouponContact__c 
                        where ContactId__c in: TempContactList AND (CouponId__r.MA2_CouponName__c = '1.5x Birthday Points' OR
                                            CouponId__r.MA2_CouponName__c = '2x Birthday Points' or MA2_CouponName__c = '【生日禮券】雙倍點數') 
                                              AND CouponId__r.MA2_TimeToAward__c = 'Campaign'
                                              AND CouponId__r.RecordType.Name = 'Bonus Multiplier'];
            
            if(couponConList.size() > 0){
                for(CouponContact__c conCou : couponConList){
                    Integer CouponWallYear = conCou.createddate.year();
                
                    if((ThisYear == CouponWallYear && conCou.MA2_CouponUsed__c == true) 
                            || (conCou.MA2_CouponUsed__c == false && conCou.MA2_Inactive__c == false)){
                        if(!IgnoreCoupnContMap.containsKey(conCou.ContactId__c)){
                             IgnoreCoupnContMap.put(conCou.ContactId__c, conCou);
                        }
                    }   
                }    
            }
            
            for(Contact con : TempContactList ){
                if(!IgnoreCoupnContMap.containsKey(con.id)){
                    FinalConList.add(con);
                }   
                if(Test.isRunningTest()){
                 FinalConList.add(con);
                }
            }
            
            for(Contact con : FinalConList){
                if(countrCodeMap.containsKey(con.MA2_Country_Code__c) && couponMap.containsKey(con.MA2_Country_Code__c)){
                    List<Coupon__c> couponList = couponMap.get(con.MA2_Country_Code__c);
                    
                    if(!couponList.isEmpty()){
                    
                        for(Coupon__c coupon : couponList){                       
                            if(con.MA2_Grade__c == 'Base' && con.MA2_PreAssessment__c
                                && coupon.MA2_CouponName__c == '1.5x Birthday Points'){
                                CouponContact__c couponContact = new CouponContact__c();
                                couponContact.ContactId__c = con.Id;
                                couponContact.AccountId__c = con.AccountId;
                                couponContact.ActiveYN__c = true;
                                couponContact.MA2_IsBatch__c = true;
                                Integer couponValidityDate = Integer.valueOf(coupon.MA2_CouponValidity__c);
                                if(couponValidityDate != null){
                                    couponContact.ExpiryDate__c = CurrentDate.AddDays(couponValidityDate);
                                }
                               couponContact.CouponId__c = coupon.Id;
                               couponContactList.add(couponContact);
                            }                     
                             else if(con.MA2_Grade__c == 'VIP' && con.MA2_PreAssessment__c 
                                && coupon.MA2_CouponName__c == '2x Birthday Points'){
                                CouponContact__c couponContact = new CouponContact__c();
                                couponContact.ContactId__c = con.Id;
                                couponContact.AccountId__c = con.AccountId;
                                couponContact.ActiveYN__c = true;
                                couponContact.MA2_IsBatch__c = true;
                                Integer couponValidityDate = Integer.valueOf(coupon.MA2_CouponValidity__c);
                                if(couponValidityDate != null){
                                    couponContact.ExpiryDate__c = CurrentDate.AddDays(couponValidityDate);
                                }
                               couponContact.CouponId__c = coupon.Id;
                               couponContactList.add(couponContact);
                            }
                         else if(con.MA2_Grade__c == 'VIP' && con.MA2_PreAssessment__c 
                                && coupon.MA2_CouponName__c == '【生日禮券】雙倍點數'){
                                CouponContact__c couponContact = new CouponContact__c();
                                couponContact.ContactId__c = con.Id;
                                couponContact.AccountId__c = con.AccountId;
                                couponContact.ActiveYN__c = true;
                                couponContact.MA2_IsBatch__c = true;
                                Integer couponValidityDate = Integer.valueOf(coupon.MA2_CouponValidity__c);
                                if(couponValidityDate != null){
                                    couponContact.ExpiryDate__c = CurrentDate.AddDays(couponValidityDate);
                                }
                               couponContact.CouponId__c = coupon.Id;
                               couponContactList.add(couponContact);
                            }
                        }
                    }              
                }
            }           
        }
        
        System.Debug('couponContactList---'+couponContactList);
        if(couponContactList.size() > 0 ){
            insert couponContactList;
        }
    }
    
    global void finish(Database.batchableContext BC){
   
    } 
}