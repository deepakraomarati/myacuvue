/*
* File Name: MA2_BirthCouponExpiryTest
* Description : Test class for MA2_BirthCouponExpiry 
* Copyright : Johnson & Johnson 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    2.0  |30-Aug-2017  |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_BirthCouponExpiryTest
{
    static testMethod void BCTestMethod() 
    {
        Test.StartTest();
        final MA2_BirthCouponExpiry BirthCoup = new MA2_BirthCouponExpiry ();
        final String sch = '0 0 1 1/1 * ? *';
        system.assertEquals(sch, '0 0 1 1/1 * ? *','Success');                           
        final String schduledId = system.schedule('Test Check', sch, BirthCoup);
        system.assertEquals(system.isScheduled(),false,'Success');	
        Test.stopTest();
    }
}