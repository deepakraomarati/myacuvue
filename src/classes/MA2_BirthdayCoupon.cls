public class MA2_BirthdayCoupon{

    public static void updateContact(List<Contact> conList,List<Contact> oldConList){
        
        Map<Id,Contact> oldConMap = new Map<Id,Contact>();
        Map<Id,Contact> usedCoupnContMap = new Map<Id,Contact>();
        set<CouponContact__c> inActiveOldCoupon = new set<CouponContact__c>();
        Map<Id,Set<CouponContact__c>> CouponConMap = new Map<Id,Set<CouponContact__c>>();
        List<Contact> usedCouponList = new List<Contact>();
        List<Contact> AssnBirthCpnFirstTime = new List<Contact>();
        Set<Contact> REassignBirthCouponSet = new Set<Contact>();
        Date TodaysDate = System.Today();
        Integer ThisMonth = TodaysDate.month();
        Integer ThisYear = TodaysDate.year();
                
        for(Contact con : oldConList){
            oldConMap.put(con.Id, con);
        }
        
        List<CouponContact__c> couponConList = [select Id, createddate, ContactId__c,ActiveYN__c,MA2_CouponUsed__c,MA2_Inactive__c,ExpiryDate__c 
                                                    from CouponContact__c where ContactId__c in: conList
                                                    and (CouponId__r.MA2_CouponName__c = '1.5x Birthday Points'
                                                     or CouponId__r.MA2_CouponName__c = '2x Birthday Points' OR MA2_CouponName__c = '【生日禮券】雙倍點數')];
        
        for(CouponContact__c con : couponConList){
            if(CouponConMap.containsKey(con.ContactId__c)){
                CouponConMap.get(con.ContactId__c).add(con);
            }else{
                CouponConMap.put(con.ContactId__c,new Set<CouponContact__c>{con});
            }
        }
        
        for(Contact con : conList){
                 Integer dobMonth = 0;
          
                 if(con.DOB__c != null){
                    dobMonth = con.DOB__c.month();
                 }
                 
                if(oldConMap.containsKey(con.Id) && CouponConMap.containsKey(con.Id)){
                    Set<CouponContact__c> couponConSet = CouponConMap.get(con.Id);
                    
                    for(CouponContact__c conRec : couponConSet){
                        
                        Integer CouponWallYear = conRec.createddate.year();
                        system.debug('Entered For');
                        system.debug('conRec ==>' +ConRec);
                        system.debug('con.MA2_Grade__c ==>' +con.MA2_Grade__c );
                        system.debug('oldConMap.get(con.Id).MA2_Grade__c ==>' +oldConMap.get(con.Id).MA2_Grade__c );
                        if(ThisYear == CouponWallYear && conRec.MA2_CouponUsed__c){
                         if(usedCoupnContMap.containsKey(con.id)){
                             usedCoupnContMap.get(con.id);
                         }else{ 
                            usedCoupnContMap.put(con.id, con);
                         }
                        }
                        if(con.MA2_Grade__c != oldConMap.get(con.Id).MA2_Grade__c && conRec.MA2_CouponUsed__c){
                            system.debug('Entered Couponused If');                             
                            break;  
                        }else if(con.MA2_Grade__c != oldConMap.get(con.Id).MA2_Grade__c 
                                            && !conRec.MA2_CouponUsed__c && !conRec.MA2_Inactive__c && conRec.ExpiryDate__c >= TodaysDate){
                            system.debug('Entered Reassigned elseIf');   
                            CouponContact__c couponConRec = conRec;
                            couponConRec.MA2_Inactive__c = true;
                            couponConRec.MA2_IsBatch__c = false;
                            inActiveOldCoupon.add(couponConRec);  
                            REassignBirthCouponSet.add(con);
                        }else if(con.DOB__c != oldConMap.get(con.Id).DOB__c && ThisMonth != dobMonth 
                                                && !conRec.MA2_CouponUsed__c && !conRec.MA2_Inactive__c ){
                            system.debug('Entered DOB Change elseIf');  
                            CouponContact__c couponConRec = conRec;
                            couponConRec.MA2_Inactive__c = true;
                            couponConRec.MA2_IsBatch__c = false;
                            inActiveOldCoupon.add(couponConRec);
                            
                        }else if(!conRec.MA2_CouponUsed__c && !conRec.MA2_Inactive__c && conRec.ExpiryDate__c <= TodaysDate){
                             system.debug('3rd');
                            CouponContact__c couponConRec = conRec;
                            couponConRec.MA2_Inactive__c = true;
                            couponConRec.MA2_IsBatch__c = false;
                            inActiveOldCoupon.add(couponConRec);  
                        }
                    }
                }else if(oldConMap.containsKey(con.Id) && !CouponConMap.containsKey(con.Id)){
                    AssnBirthCpnFirstTime.add(con);    
                }     
        }
        system.debug('inActiveOldCoupon ==>' +inActiveOldCoupon);
        system.debug('REassignBirthCouponSet==>' +REassignBirthCouponSet);
     
        if(inActiveOldCoupon.size() > 0){
            List<CouponContact__c> FinalInActiveCWList = new List<CouponContact__c>();
            FinalInActiveCWList.addAll(inActiveOldCoupon);
            system.debug('FinalInActiveCWList==>' +FinalInActiveCWList);
            update FinalInActiveCWList;
        }
        
        if(REassignBirthCouponSet.size() > 0){
            system.debug('Entered reassign if==>' +REassignBirthCouponSet);
            List<Contact> FinalListtoAdd = new List<Contact>();
            
            for(contact con: REassignBirthCouponSet ){
                if(! usedCoupnContMap.containsKey(con.id)){
                    FinalListtoAdd.add(con);
                }
            }
            insertCoupon(FinalListtoAdd, true);        
        }
        
        if(AssnBirthCpnFirstTime.size() > 0){
            insertCoupon(AssnBirthCpnFirstTime, false);  
        }    
    }
    
    public static void insertCoupon(List<Contact> contactList, boolean flag){
        
        final List<String> countryCodeList = new List<String>();            
        final List<CouponContact__c> couponContactList = new List<CouponContact__c>();
        Date dt = System.Today();
        
        Map<String,String> countrCodeMap = new Map<String,String>();
        Map<String,MA2_CountryCode__c> countryCodeMap = MA2_CountryCode__c.getAll();
        for(MA2_CountryCode__c conCode : countryCodeMap.values()){
            countryCodeList.add(conCode.MA2_CountryCodeValue__c);
            countrCodeMap.put(conCode.MA2_CountryCodeValue__c,conCode.MA2_CountryCodeValue__c);
        }
       
        if(countryCodeList.size() > 0){
            List<Coupon__c> couponRecord = [select Id,MA2_CouponValidity__c,MA2_CouponName__c,MA2_CountryCode__c from Coupon__c 
                                        where (MA2_CouponName__c = '1.5x Birthday Points' or MA2_CouponName__c = '2x Birthday Points' or MA2_CouponName__c = '【生日禮券】雙倍點數')
                                        and MA2_TimeToAward__c = 'Campaign'
                                        and RecordType.Name = 'Bonus Multiplier' and MA2_CountryCode__c in: countryCodeList];
                                        
            Map<String,List<Coupon__c>> couponMap = new Map<String,List<Coupon__c>>();
            for(Coupon__c coupon : couponRecord){
                if(couponMap.containsKey(coupon.MA2_CountryCode__c)){
                        couponMap.get(coupon.MA2_CountryCode__c).add(coupon); 
                }else{
                        couponMap.put(coupon.MA2_CountryCode__c,new List<Coupon__c>{coupon});
                }    
            }
            
            
            for(Contact con : contactList){
                if(con.DOB__c != null){
                Integer dobDay = con.DOB__c.day();
                Integer todayDay = dt.day();
                Integer dobMonth = con.DOB__c.month();
                Integer todayMonth = dt.month();
                System.Debug('Entered For Loop' );
                if(countrCodeMap.containsKey(con.MA2_Country_Code__c) && couponMap.containsKey(con.MA2_Country_Code__c)){
                    List<Coupon__c> couponList = couponMap.get(con.MA2_Country_Code__c);
                    if(!couponList.isEmpty()){
                    
                        for(Coupon__c coupon : couponList){
                        
                            if((dobMonth == todayMonth || flag)&& con.MA2_Grade__c == 'Base' && con.MA2_PreAssessment__c
                                && coupon.MA2_CouponName__c == '1.5x Birthday Points'){
                                CouponContact__c couponContact = new CouponContact__c();
                                couponContact.ContactId__c = con.Id;
                                couponContact.AccountId__c = con.AccountId;
                                couponContact.ActiveYN__c = true;
                                couponContact.MA2_IsBatch__c = false;
                                Integer couponValidityDate = Integer.valueOf(coupon.MA2_CouponValidity__c);
                                if(couponValidityDate != null){
                                    couponContact.ExpiryDate__c = dt.AddDays(couponValidityDate);
                                }
                               couponContact.CouponId__c = coupon.Id;
                               couponContactList.add(couponContact);
                            } 
                    
                             else if((dobMonth == todayMonth || flag) && con.MA2_Grade__c == 'VIP' && 
                             con.MA2_PreAssessment__c 
                             && coupon.MA2_CouponName__c == '2x Birthday Points'){
                                CouponContact__c couponContact = new CouponContact__c();
                                couponContact.ContactId__c = con.Id;
                                couponContact.AccountId__c = con.AccountId;
                                couponContact.ActiveYN__c = true;
                                couponContact.MA2_IsBatch__c = false;
                                Integer couponValidityDate = Integer.valueOf(coupon.MA2_CouponValidity__c);
                                if(couponValidityDate != null){
                                    couponContact.ExpiryDate__c = dt.AddDays(couponValidityDate);
                                }
                               couponContact.CouponId__c = coupon.Id;
                               couponContactList.add(couponContact);
                            }
                             else if((dobMonth == todayMonth || flag) && con.MA2_Grade__c == 'VIP' && 
                             con.MA2_PreAssessment__c 
                             && coupon.MA2_CouponName__c == '【生日禮券】雙倍點數'){
                                CouponContact__c couponContact = new CouponContact__c();
                                couponContact.ContactId__c = con.Id;
                                couponContact.AccountId__c = con.AccountId;
                                couponContact.ActiveYN__c = true;
                                couponContact.MA2_IsBatch__c = false;
                                Integer couponValidityDate = Integer.valueOf(coupon.MA2_CouponValidity__c);
                                if(couponValidityDate != null){
                                    couponContact.ExpiryDate__c = dt.AddDays(couponValidityDate);
                                }
                               couponContact.CouponId__c = coupon.Id;
                               couponContactList.add(couponContact);
                            }
                        }
                    }
                   
                }
                }
            }
        }
        System.Debug('couponContactList---'+couponContactList);
        if(couponContactList.size() > 0 ){
            insert couponContactList;
        }    
    }   
}