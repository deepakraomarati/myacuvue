public class MA2_BlockHours{

    public static void mapBatch(List<MA2_BlockHours__c> blockList)
    {
    
         List<String> lstAccId= new List<String>();
         List<MA2_BlockHours__c> lstBlockhoursobj= new List<MA2_BlockHours__c>();
      
         Map<id,Account> MapAccount=new map<id,Account>();
    
         for(MA2_BlockHours__c Bhobj:blockList)
         {
            lstBlockhoursobj.add(Bhobj);
        
            if(Bhobj.MA2_Account__c!=null){
                lstAccId.add(Bhobj.MA2_Account__c);
            }
         }
      
        if(lstAccId.size()>0 && !lstAccId.isEmpty())
        {    
            MapAccount=new Map<Id,Account>([select id,Name,OutletNumber__c from account where OutletNumber__c IN:lstAccId]);
        } 
         
        Map<String,Id> mapAccountIds= new Map<String,Id>();
        for(Account acc:MapAccount.Values()){
            mapAccountIds.put(acc.OutletNumber__c,acc.Id);
        }
         
        List<MA2_BlockHours__c> lstBlockHours = new List<MA2_BlockHours__c>();

        for(MA2_BlockHours__c bh:lstBlockhoursobj)
        {
            if(bh.MA2_Account__c!=null && mapAccountIds.get(bh.MA2_Account__c)!=null){
                 bh.MA2_AccountId__c= mapAccountIds.get(bh.MA2_Account__c);
                   
            }
            lstBlockHours.add(bh);
        }
           
    }
}