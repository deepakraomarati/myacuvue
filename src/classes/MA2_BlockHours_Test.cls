@isTest
public with sharing class MA2_BlockHours_Test 
{  
    static testmethod void insertBlockHours()
    {
        
        account ac = new account();
        ac.name='test';
        ac.OutletNumber__c ='123';
        insert ac;
        
        MA2_BlockHours__c bhours = new MA2_BlockHours__c();
        bhours.MA2_AccountId__c=ac.id;
        bhours.MA2_Account__c=ac.OutletNumber__c;
        bhours.MA2_BlockDate__c = System.Today();
        insert bhours;
        system.assertEquals(bhours.MA2_Account__c,ac.OutletNumber__c,'success');
    }
}