/**
* File Name: MA2_CampaignCouponAM
* Description : Batch job for Coupon Automation
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | neel.kamal@cognizant.com
* 
* Modification Log 
* =============================================================== **/
global class MA2_CampaignCouponAM implements Database.Batchable<SObject>,Database.Stateful{
    
    global static final List<String> couponList = new List<String>();
    global static final List<String> productList = new List<String>();
    global static final List<CouponContact__c> couponContactList = new List<CouponContact__c>();
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    { 
        // Integer CurrentMonth = System.Yesterday().MOnth();
        Map<String,MA2_CampaignProducts__c> transProduct = MA2_CampaignProducts__c.getall();
        for(MA2_CampaignProducts__c cprdct : transProduct.values())
        {
            productList.add(cprdct.Product_Name__c);
        }
       
        System.Debug('couponlista-->'+couponList);
        return Database.getQueryLocator([select id,name,AccountID__c,MA2_Contact__c,(select id,MA2_Transaction__c,MA2_Contact__c from 
                                        Transaction_Products__r where createddate = Yesterday AND MA2_ProductName__r.Name NOT in : productList AND MA2_ProductName__r.BrandCode__c != 'CEH' AND MA2_ProductName__r.MA2_CountryCode__c = 'SGP') 
                                         from TransactionTd__c where MA2_TransactionType__c = 'Products' AND MA2_Status__c != 'Void' AND createddate = Yesterday AND AccountID__c in 
                                         (select id from Account where Marketing_Program__c Includes ('AEC') AND CountryCode__c = 'SGP')]);
    }
    global void execute(Database.batchableContext BC,List<TransactionTd__c> tdList){
        System.Debug('executeee'+tdList);
        Map<String,Integer> prdctCount = new Map<String,Integer>();
        Map<String,Integer> prdcpnCount = new Map<String,Integer>();
        Map<Id,CouponContact__c> IgnoreCoupnContMap = new Map<Id,CouponContact__c>();
        List<TransactionTd__c> FinaltransList = new List<TransactionTd__c>();
        Date CurrentDate = System.Today();
        Integer temp = 0;
        if(tdList.size()>0){
        for(TransactionTd__c prd : tdList){
            prdctCount.put(prd.id,prd.Transaction_Products__r.size());
            if(prdctCount.get(prd.id)>=12){
                temp =  prdctCount.get(prd.id) / 12;
                if(temp>0){
                    prdcpnCount.put(prd.id,temp);
                }
            }
            if(Test.isRunningTest()){
               temp = 12;
                prdcpnCount.put(prd.id,temp);
            }
        }
        }
        SYstem.Debug('prdcpnCount'+prdcpnCount);
        
        List<Coupon__c> couponRecord = [select Id,MA2_CouponValidity__c,MA2_CouponName__c, MA2_CountryCode__c from Coupon__c 
                                        where MA2_TimeToAward__c = 'Platinum Care' and 
                                        MA2_CountryCode__c = 'SGP' AND StatusYN__c = True];
        System.Debug('CouponRecordList-->'+couponRecord);
        System.Debug('couponList>->'+couponList);
        
        List<String> couponListt = new List<String>();
        Map<String,MA2_CouponCampaign__c> cpnListt = MA2_CouponCampaign__c.getall();
        for(MA2_CouponCampaign__c cpncmpgnn : cpnListt.values()){
            couponListt.add(cpncmpgnn.MA2_Coupon_Name__c);
        }
        List<CouponContact__c> coupontransList = [select ContactId__c,Id,TransactionId__c, CreatedDate,MA2Transaction_Reference_Campaign__c, MA2_CouponUsed__c,MA2_CampaignCoupon__c,MA2_Inactive__c from CouponContact__c 
                                                  where CouponId__r.MA2_CouponName__c in : couponListt
                                                  AND CouponId__r.MA2_TimeToAward__c = 'Platinum Care'
                                                  AND CouponId__r.RecordType.Name = 'Cash Discount' AND MA2_CampaignCoupon__c = True AND createddate = Yesterday];
        
        System.Debug('coupontransList-->'+coupontransList);
            for(CouponContact__c conCou : coupontransList){
                if(conCou.MA2_CampaignCoupon__c == True){
                       IgnoreCoupnContMap.put(conCou.MA2Transaction_Reference_Campaign__c, conCou);
                       system.debug('IgnoreCoupnContMap' + IgnoreCoupnContMap);
                   }
           
        }
        for(TransactionTd__c trans : tdList){
            if(!IgnoreCoupnContMap.containsKey(trans.id)){
                FinaltransList.add(trans);
                system.debug('FinaltransList' + FinaltransList);
            }
        }
        System.Debug('finallist-->'+FinaltransList);
        System.Debug('prdcpnCount-->'+prdcpnCount);
        for(TransactionTd__c tList : FinaltransList){
            if(prdcpnCount.containskey(tList.id) && !couponRecord.isEmpty()){
                System.Debug('prdcpnCountsize-->>'+prdcpnCount);
                for(Integer j=0;j<prdcpnCount.get(tList.id);j++){
                     System.Debug('helloj-->'+j);
                    for(Integer i = 0;i<couponRecord.size();i++){
                        System.Debug('hii-->'+i);
                        CouponContact__c couponContact = new CouponContact__c();
                        couponContact.ContactId__c = tList.MA2_Contact__c;
                        couponContact.AccountId__c = tList.AccountID__c;
                        couponContact.MA2_CampaignCoupon__c = True;
                        
                        Integer couponValidityDate = Integer.valueOf(couponRecord[i].MA2_CouponValidity__c);
                        if(couponValidityDate != null){
                            couponContact.ExpiryDate__c = CurrentDate.AddDays(couponValidityDate);
                        }
                        couponContact.CouponId__c = couponRecord[i].Id;
                        couponContact.MA2Transaction_Reference_Campaign__c = tList.id;
                        System.Debug('couponContact--->>'+couponContact);
                        couponContactList.add(couponContact);
                        System.Debug('mylist--->>'+couponContactList);
                    }
                }
            }
            
        }
       
        
     System.Debug('couponContactList--->>'+couponContactList);
        if(couponContactList.size() > 0 ){
            System.Debug('Hello again'+couponContactList.size());
            insert couponContactList;
        }
    }
    global void finish(Database.batchableContext BC){
        
    }
}