/*
* File Name: MA2_BatchSingaporAssignBirthCouponTest
* Description : Test class for Batch Job of Coupon Expired 
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | neel.kamal@cognizant.com
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |02-May-2018  |nkamal8@its.jnj.com  |New Class created
*/
@isTest
public class MA2_CampaignCouponAMTest{
    
    /* Method for excuting the test class */
    static Testmethod void runBatchJobMethod(){
        
        final string Name = 'SGP';
        
        final string CountryCurrency = 'SGD';
        Datetime yesterday = Datetime.now().addDays(-1);
        final string AccountId = '123456';
        final string AccountId2 = '123455';
        List<MA2_TransactionProduct__c> tpr = new List<MA2_TransactionProduct__c>();
        
        final TriggerHandler__c cred = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                             GetTransactionTd__c = true, MA2_createUpdateTransaction__c = true, CouponContact__c= true, MA2_CouponInactiveTrigger__c = true,
                                                             MA2_CouponToApigeeTrigger__c= true, MA2_CouponWalletForApigee__c= true, MA2_createUpdateTransactionProd__c= true );
        insert cred;
        
        final MA2_CampaignProducts__c cp= new MA2_CampaignProducts__c(Name = 'ABC',Product_Id__c = '123445',Product_Name__c = 'ABC');
        insert cp;
        
        MA2_CountryCode__c CountryCode = new MA2_CountryCode__c(Name= Name, MA2_CountryCodeValue__c = Name);
        insert CountryCode;
        
        final MA2_Country_Currency_Map__c CCM = new MA2_Country_Currency_Map__c(Name = Name, Currency__c = CountryCurrency);
        insert CCM;
        
        
        
        final Id Bday1xId = [select Id from RecordType where Name = 'Cash Discount' and sObjectType = 'Coupon__c'].Id;        
        
        final Coupon__c coupon2 = new Coupon__c(MA2_CouponName__c = 'test',RecordTypeID = Bday1xId ,MA2_CountryCode__c =Name
                                                ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Platinum Care',StatusYN__c = True);  
        insert coupon2;
        
        
        system.assertEquals(coupon2.MA2_CouponName__c, 'test');                           
        
        
        final Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id;
        final Id consumerRecTypeId2 = [select Id from RecordType where Name = 'ECP' and sObjectType= 'Contact'].Id;
        
        final Account acc = new Account(Name = 'Test' , MA2_AccountId__c = AccountId ,AccountNumber = '54321',
                                        Marketing_Program__c = 'AEC',OutletNumber__c = '54321',ActiveYN__c = true,
                                        My_Acuvue__c = 'Yes',CountryCode__c = Name,PublicZone__c = 'Testtt');
        
        insert acc;
        
        
         system.assertEquals(acc.Name, 'Test');                           
        final Contact con1 = new Contact(LastName = 'test1' ,MA2_AccountId__c = AccountId ,MA2_ContactId__c = '23456',
                                         RecordTypeId  = consumerRecTypeId2  ,
                                         AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = CountryCurrency, 
                                         MembershipNo__c = 'SGP1235', MA2_Country_Code__c = Name, MA2_Grade__c='Base', DOB__c = date.newinstance(2012,03,03),
                                         MA2_PreAssessment__c = true);
        insert con1;
        
        final Contact con2 = new Contact(LastName = 'test' ,MA2_AccountId__c = AccountId ,MA2_ContactId__c = '23456',
                                         RecordTypeId  = consumerRecTypeId  ,
                                         AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = CountryCurrency, 
                                         MembershipNo__c = 'SGP123', MA2_Country_Code__c = Name, MA2_Grade__c='Base', DOB__c = date.newinstance(2012,03,03),
                                         MA2_PreAssessment__c = true);
        insert con2;
        
			TransactionTd__c trans = new TransactionTd__c(AccountID__c = acc.Id, MA2_Contact__c = con2.Id,MA2_Status__c = '',
                                                      MA2_AccountId__c = '123456',MA2_ContactId__c = '23456' , MA2_Points__c = 10 ,MA2_Redeemed__c = 10,
                                                      MA2_TransactionType__c = 'Products',MA2_Price__c = 200,MA2_CountryCode__c = 'SGP',MA2_ECPContact__c = con1.id,MA2_TransactionId__c = '1502265411415'
                                                      );
        insert trans;					
        Test.setCreatedDate(trans.Id, yesterday);
        
        Product2 pro = new Product2();
        pro.Name = 'test-21';
        pro.UPC_Code__c = '9876546';
        pro.BrandCode__c = '1DL';
        pro.MA2_Plant__c = 'SG-11';
        insert pro;
        
        Map<String,String> TestMap = new Map<String,String>();
        
        List<MA2_TransactionProduct__c> tList = new List<MA2_TransactionProduct__c>();
        for(Integer i=0;i<15;i++){
        MA2_TransactionProduct__c tproduct = new MA2_TransactionProduct__c(
            MA2_Account__c = acc.Id,
            MA2_Contact__c = con2.Id,
            MA2_ProductName__c = pro.id,
            MA2_ProductQuantity__c = 1,
            MA2_TransactionId__c = '150839283',
            MA2_Transaction__c = trans.Id);
            tpr.add(tproduct);
           
        }
        Insert tpr; 
        
        SYstem.Debug('transid'+ trans.id + 'tpr0-->'+tpr.size());
        for(MA2_TransactionProduct__c t: tpr){
            Test.setCreatedDate(t.Id, yesterday);
            tList.add(t);
        }
        update tList;
        
        Test.startTest();
        Database.executeBatch(new MA2_CampaignCouponAM(),10);
        Test.stopTest();
    }

}