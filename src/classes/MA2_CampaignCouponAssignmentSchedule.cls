global class MA2_CampaignCouponAssignmentSchedule implements Schedulable{

    public void execute(SchedulableContext sc){
        
        MA2_CampaignCouponAssignment.sendData([select Id from CouponContact__c where MA2_Campaign_Coupon_Batch__c = false and createdDate = Today],'schedule','insert');    
    }
}