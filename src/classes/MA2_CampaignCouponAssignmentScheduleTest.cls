/**
* File Name: MA2_CampaignCouponAssignmentScheduleTest
* Description : Test class for MA2_CampaignCouponAssignmentSchedule
* Copyright : Johnson & Johnson
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |14-May-2018 |nkamal8@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_CampaignCouponAssignmentScheduleTest
{
    static testMethod void expiryTestMethod() 
    {
        Test.StartTest();
        final MA2_CampaignCouponAssignmentSchedule sh1 = new MA2_CampaignCouponAssignmentSchedule();
        final String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, sh1);
        system.assertEquals(system.isScheduled(),false);  
        
        Test.stopTest();
    }
}