/**
* File Name: MA2_CampaignCouponAssignment
* Description : Test class for MA2_CampaignCouponAssignmentTest
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | neel.kamal@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |02-May-2018 |nkamal8@its.jnj.com  |New Class created
*/
@isTest
public class MA2_CampaignCouponAssignmentTest{
    
    /* Method for excuting the test class */
    static Testmethod void MA2_CWTestMethod(){
        final Credientials__c cred = new Credientials__c(Name = 'MyAcuvue_global_sfdc_coupon_Insert' , 
                                                   Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                                                   Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                                                   Target_Url__c = 'https://jnj-dev.apigee.net/v1/consumer/catalog/addcoupon');
        insert cred;
        final string countrycode = 'SGP';
        final string prName = 'ACUVUE OASYS For Astigmatism';
        Datetime yesterday = Datetime.now().addDays(-1);
        TestDataFactory_MyAcuvue.insertCustomSetting();
        
        MA2_CampaignProducts__c campagnProd = new MA2_CampaignProducts__c(Name = prName,Product_Id__c= '01tN0000003evQp', Product_Name__c = prName);
        insert campagnProd;
        TriggerHandler__c o = new TriggerHandler__c();
        o.Name = 'HandleTriggers';
        o.CouponContact__c = True;
        Insert o;
        MA2_Country_Currency_Map__c ccm = new MA2_Country_Currency_Map__c();
        ccm.Name = 'SGP';
        ccm.Currency__c = 'SGD';
        Insert ccm;
        
        Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id;
        final List<Account> accList = new List<Account>();
        final List<Contact> conList = new List<Contact>();
        final List<Contact> couponListNew = new List<Contact>();
        for(Integer i=0;i<1;i++){
        Account ob = new Account();
        ob.Name = 'Test';
        ob.MA2_AccountId__c = '123456' ;
        ob.AccountNumber = '54321'; 
        ob.OutletNumber__c = '54321';
        ob.ActiveYN__c = true ;
        ob.My_Acuvue__c = 'Yes';
        ob.PublicZone__c = 'Testtt';
            accList.add(ob);
        }
        insert accList;
        
        system.assertEquals(accList.size(), 1);                              
       // conList.addAll(TestDataFactory_MyAcuvue.createECPContact(1,accList));
        for(Integer i=0;i<1;i++){
        Contact co = new Contact();
        co.LastName = 'test' ;
        co.MA2_AccountId__c = '123456';
        co.MA2_ContactId__c = '23456';
        co.RecordTypeId  = consumerRecTypeId ;
        co.AccountId = accList[0].Id;
        co.email = 'test@gmail.com';
        co.phone = '123456789' ;
        co.MA2_Country_Code__c = 'SGP';
        co.MA2_PreAssessment__c = true;
        conList.add(co);
        }
        insert conList;
        
        
        final Id etrialRecordTypeId = [select Id from RecordType where Name = 'Cash Discount' and sObjectType = 'Coupon__c'].Id;
        final Coupon__c coupon = new Coupon__c(MA2_CouponName__c = 'Cash Discount',RecordTypeID = etrialRecordTypeId ,MA2_CountryCode__c =countrycode
                                               ,MA2_CouponValidity__c = 30);  
        insert coupon;
        
        final CouponContact__c couponWallet = new CouponContact__c(ExpiryDate__c = System.Today() - 10 ,ContactId__c = conList[0].Id ,
                                                                   CouponId__c = coupon.Id , MA2_AccountId__c = '12345' , Apigee_Update__c = false, MA2_ContactId__c = '5555',CurrencyIsoCode=ccm.Currency__c
            														,MA2_CampaignCoupon__c = True, MA2_Campaign_Coupon_Batch__c = false);   
        
        insert couponWallet;
        Test.setCreatedDate(couponWallet.id, yesterday);
        Test.startTest();
        MA2_CampaignCouponAssignmentSchedule ob = new MA2_CampaignCouponAssignmentSchedule();
        ob.execute(null);
        Test.stopTest();
    }
}