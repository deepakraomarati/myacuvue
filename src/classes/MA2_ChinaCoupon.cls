public class MA2_ChinaCoupon{
    final public static DateTime dt = System.Now();
    public static List<CouponContact__c> assignCouponPreasseesmentChecked(Contact con , List<Coupon__c> couponETPreList){
        List<CouponContact__c> createCouponList = new List<CouponContact__c>();
        CouponContact__c couponContact = new CouponContact__c();
        for(Coupon__c cou : couponETPreList){
            System.Debug('cou---'+cou);
            System.Debug('cou.MA2_CountryCode__c--'+cou.MA2_CountryCode__c);
            System.Debug('con.MA2_Country_Code__c---'+con.MA2_Country_Code__c);
            if(cou.MA2_CountryCode__c == con.MA2_Country_Code__c && cou.RecordType.Name == 'Etrial'
               && con.MA2_Country_Code__c == 'CHN' && cou.MA2_CountryCode__c == 'CHN'){
               //&& cou.MA2_TimeToAward__c != 'Pre-Assessment'){
                   couponContact.ContactId__c = con.Id;
                   couponContact.AccountId__c = con.AccountId;
                   couponContact.ActiveYN__c = true;
                   Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                   couponContact.ExpiryDate__c = dt.AddDays(count);
                   couponContact.CouponId__c = cou.Id;
                   createCouponList.add(couponContact);
               }
        }    
        return createCouponList;
    }
    
    /*public static List<CouponContact__c> assignCouponPreasseesmentNoChecked(Contact con , List<Coupon__c> couponETPreList){
        List<CouponContact__c> createCouponList = new List<CouponContact__c>();
        CouponContact__c couponContact = new CouponContact__c();
        for(Coupon__c cou : couponETPreList){
            System.Debug('cou---'+cou);
            System.Debug('cou.MA2_CountryCode__c--'+cou.MA2_CountryCode__c);
            System.Debug('con.MA2_Country_Code__c---'+con.MA2_Country_Code__c);
            if(cou.MA2_CountryCode__c == con.MA2_Country_Code__c && cou.RecordType.Name == 'Etrial' &&
               con.MA2_AssignCoupon__c == 'Changed' && cou.MA2_TimeToAward__c == 'Pre-Assessment'){
                   System.Debug('check');
                   System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                   CouponContact__c couponOtherContact = new CouponContact__c();
                   couponOtherContact.ContactId__c = con.Id;
                   couponOtherContact.AccountId__c = con.AccountId;
                   couponOtherContact.ActiveYN__c = true;
                   Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                   if(count != null){
                       couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                   }
                   couponOtherContact.CouponId__c = cou.Id;
                   createCouponList.add(couponOtherContact);    
               }
        }
        return createCouponList;           
    }*/
}