public class MA2_Consumer
{
    String consumerId;
    public static void consumerActive(List<Consumer_Relation__c> newConsumer, Boolean isUpdate, Boolean isInsert, Boolean isBefore)
    {
        
        if(isBefore && isInsert)
            System.Debug('hi'+newConsumer);
        {
            set<id> setids=new set<id>();
            List<Consumer_Relation__c> lstConsumerRelationToUpdate = new List<Consumer_Relation__c>();
            Set<Id> setActiveContactIds = new Set<Id>();
            
            for(Consumer_Relation__c objConsumerRelation:newConsumer)
            {
                if((objConsumerRelation.ContactID__c == null ) || (objConsumerRelation.AccountId__c == null)){
                    objConsumerRelation.adderror('ContactID and AccountID Mandatory');
                }
                else{
                    if(objConsumerRelation.MA2_MyECP__c)
                    {
                        setActiveContactIds.add(objConsumerRelation.ContactID__c);
                        System.Debug('contactlistttt---->'+setActiveContactIds);
                    }
                }
            }
            
            
            /*for(Consumer_Relation__c cr:newConsumer)
            {
                setids.add(cr.ContactID__c);
            }*/
            
            System.debug('setids-----------------------'+setids);  
            
            list<Consumer_Relation__c> consumerlist=[select id, MA2_MyECP__c,ContactID__c,AccountId__c,MA2_AccountId__c,MA2_ContactId__c from Consumer_Relation__c where ContactID__c IN:setActiveContactIds];
            
            System.debug('consumerlist--->>--------------------'+consumerlist);
            
            //for(Consumer_Relation__c cons:newConsumer){
                //if(cons.MA2_MyECP__c==true){
            for(Consumer_Relation__c consumer: consumerlist)
            {
                if(consumer.MA2_MyECP__c)
                {
                    consumer.MA2_MyECP__c=false;
                    consumer.MA2_ApigeeUpdate__c = True;
                    System.debug('inside update if loop-----------------------'); 
                    lstConsumerRelationToUpdate.add(consumer);
                }
            }
                //}
            update lstConsumerRelationToUpdate;    
        }
            
        //}
        
        /*if(isUpdate && isBefore)
        {
            set<id> setids=new set<id>();
            for(Consumer_Relation__c cr:newConsumer){
                setids.add(cr.ContactID__c);
            }
            System.debug('setids-----------------------'+setids);  
            list<Consumer_Relation__c> consumerlist=[select id, MA2_MyECP__c from Consumer_Relation__c where ContactID__c IN:setids];

            for(Consumer_Relation__c cons:newConsumer)
            {
                if(cons.MA2_MyECP__c==true)
                {
                    for(Consumer_Relation__c consumer: consumerlist)
                    {
                        if(consumer.MA2_MyECP__c==true)
                        {
                            ID consumerIdd = consumer.id;
                            updateConsumer(consumerIdd);
                            System.debug('inside update if loop-----------------------'); 
                        }
                    }
                }
                
            }*/
            //update consumer;
        //}
    }
    
    /*public static void updateConsumer(Id conId)
    {
        Consumer_Relation__c updateConsumerlst = [Select id, MA2_MyECP__c from Consumer_Relation__c where id =: conId];
        if(updateConsumerlst.MA2_MyECP__c == true)
        {
            updateConsumerlst.MA2_MyECP__c = false;
            update updateConsumerlst;
        }
    }*/
    
    public static void apigeeMap(list<Consumer_Relation__c> consList)
    {
        List<String> lstAccId= new List<String>();
        List<String> lstConId= new List<String>();
     
        List<Consumer_Relation__c> Consumer= new List<Consumer_Relation__c>();
      
        Map<id,Account> MapAccount=new map<id,Account>();
        Map<id,Contact> MapContact=new map<id,Contact>();
        
        for(Consumer_Relation__c Crobj:consList)
        {
            Consumer.add(Crobj);
        
            if(Crobj.MA2_AccountId__c!=null){
                lstAccId.add(Crobj.MA2_AccountId__c);
            }
        
            if(Crobj.MA2_ContactId__c!=null){
                lstConId.add(Crobj.MA2_ContactID__c);
            }
        
        }
      
        if(lstAccId.size()>0 && !lstAccId.isEmpty())
        {    
            MapAccount=new Map<Id,Account>([select id,Name,OutletNumber__c from account where OutletNumber__c IN:lstAccId]);
        } 
         
        if(lstConId.size()>0 && !lstConId.isEmpty())
        {
            MapContact =new Map<Id,Contact>([select id,LastName,MembershipNo__c from contact where MembershipNo__c IN:lstConId]);
        }
       
        Map<String,Id> mapAccountIds= new Map<String,Id>();
        for(Account acc:MapAccount.Values()){
            mapAccountIds.put(acc.OutletNumber__c,acc.Id);
        }
         
         
        Map<String,Id> mapContactIds= new Map<String,Id>();
        for(Contact con: MapContact.Values()){
            mapContactIds.put(con.MembershipNo__c,con.Id);
        }
         
         
        List<Consumer_Relation__c> lstConsumerBooking = new List<Consumer_Relation__c>();

        for(Consumer_Relation__c cr:Consumer)
        {
            if(cr.MA2_AccountId__c!=null && mapAccountIds.get(cr.MA2_AccountId__c)!=null){
                 cr.AccountId__c= mapAccountIds.get(cr.MA2_AccountId__c);
                   
            }
            if(cr.MA2_ContactId__c!=null && mapContactIds.get(cr.MA2_ContactId__c)!=null){
                 cr.ContactID__c= mapContactIds.get(cr.MA2_ContactId__c);
            }   
            lstConsumerBooking.add(cr);
        }
    }
}