/**
* File Name: MA2_ConsumerAge
* Description : Calculating Age
* Copyright : Johnson & Johnson
* @author : Sourabh Tayal | stayal@its.jnj.com | sourabh.tayal@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |28-aug-2018 |stayal@its.jnj.com  |New Class created
*/
public class MA2_ConsumerAge {
    
    public static void sendAgeData(List<Contact> contactList){
        
        try{
            for(Contact con : contactList){               
                if(con.DOB__c!=NULL) {
                    
                    date birthdaydt=date.valueof(con.DOB__c);
                    integer leapyear = 2000;
                    
                    integer birthyear = birthdaydt.year();
                    integer birthmonth = birthdaydt.month();
                    integer birthdate = birthdaydt.day();
                    
                    date currentdt = system.today();
                    integer currentyear = currentdt.year();
                    integer currentmonth = currentdt.month();
                    integer currentdate = currentdt.day();
                    
                    date birthmonthandday = date.newInstance(leapyear,birthmonth,birthdate);
                    date currentmonthandday = date.newInstance(leapyear,currentmonth,currentdate);
                    
                    //TO FIND THE AGE
                    if(birthmonthandday <= currentmonthandday){
                        con.AgeCal__c = string.valueof((integer)(currentyear - birthyear));
                    }else{
                        con.AgeCal__c = string.valueof((integer)((currentyear - birthyear)-1));
                    }
                }                
            }           
        }
        catch(Exception e) {
            system.debug('The following exception has occurred: ' + e.getMessage());
        }
    }
}