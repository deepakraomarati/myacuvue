@istest
public class MA2_ConsumerAgeTest {
    
    static testmethod void doinsertConsumerAge()
    { 
        final TriggerHandler__c cred = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                             GetTransactionTd__c = true, MA2_createUpdateTransaction__c = true, CouponContact__c= true, MA2_CouponInactiveTrigger__c = true,
                                                             MA2_CouponToApigeeTrigger__c= true, MA2_CouponWalletForApigee__c= true, MA2_createUpdateTransactionProd__c= true );
        insert cred;
        contact con = new contact();
        con.LastName='test';
        con.DOB__c=datetime.newInstance(2000, 9, 23, 12, 30, 0);
        system.assertequals(con.LastName,'test');   
        list<Contact>contacts=new list<Contact>();
        list<Contact>updatecontacts=new list<Contact>();       
        contacts.add(con);
        insert contacts;
        MA2_ConsumerAge.sendAgeData(contacts);
        for(contact item: contacts){
            item.lastname = 'retest';
            updatecontacts.add(item);
        }
        update updatecontacts;
    }
}