/**
* File Name: MA2_ConsumerECPSGPCheck
* Description : Logic to identify how many SGP consumers have same ECP profiles.
* Copyright : Johnson & Johnson
* @author : Lhawang Bhutia | lbhutia@its.jnj.com | lhawang.bhutia@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |5-May-2018  |lbhutia@its.jnj.com   |New Class created
*/
public class MA2_ConsumerECPSGPCheck{   
    public static void MA2_ConsumerSGPCheck(List<Contact> contactList){
        list<Contact> SGPECPlist = new list<Contact>();
        set<string> ConsumerMobilePhone = new set<string>();
        set<string> InactiveECPMobilePhone = new set<string>();
        set<string> ActiveECPMobilePhone = new set<string>();        
        try{            
            for(Contact con : contactList){
                string recordtypename = Schema.SObjectType.Contact.getRecordTypeInfosById().get(con.recordtypeid).getname();
                if(con.MA2_Country_Code__c == 'SGP' && recordtypename == 'Consumer' && con.MobilePhone!=''){
                    ConsumerMobilePhone.add(string.valueof(con.MobilePhone));                    
                }
            }
            
            SGPECPlist = [select Id,MobilePhone,Inactive__c from Contact where MA2_Country_Code__c = 'SGP' and recordtype.name = 'ECP' and MobilePhone IN: ConsumerMobilePhone];            
            for(Contact con : SGPECPlist){
                if(con.Inactive__c == true){
                    InactiveECPMobilePhone.add(string.valueof(con.MobilePhone));
                }
                if(con.Inactive__c == false){
                    ActiveECPMobilePhone.add(string.valueof(con.MobilePhone));
                }
                
            }
            if(!SGPECPlist.isEmpty()){
                for(Contact con : contactList){
                    string recordtypename = Schema.SObjectType.Contact.getRecordTypeInfosById().get(con.recordtypeid).getname();
                    if(con.MA2_Country_Code__c == 'SGP' && recordtypename == 'Consumer' && con.MobilePhone!='' && ActiveECPMobilePhone.contains(con.MobilePhone)){
                        con.Is_ECP_Data_ECP__c = true;
                    }
                    if(con.MA2_Country_Code__c == 'SGP' && recordtypename == 'Consumer' && con.MobilePhone!='' && con.Inactive__c == true){
                        con.Is_ECP_Data_ECP__c = false;
                    }
                }          
            }
            
            if(!SGPECPlist.isEmpty()){
                for(Contact con : contactList){
                    string recordtypename = Schema.SObjectType.Contact.getRecordTypeInfosById().get(con.recordtypeid).getname();
                    if(con.MA2_Country_Code__c == 'SGP' && recordtypename == 'Consumer' && con.MobilePhone!='' && InactiveECPMobilePhone.contains(con.MobilePhone)){
                        con.Is_ECP_Data_ECP__c = false;
                    }
                    if(con.MA2_Country_Code__c == 'SGP' && recordtypename == 'Consumer' && con.MobilePhone!='' && con.Inactive__c == true){
                        con.Is_ECP_Data_ECP__c = false;
                    }
                }          
            }           
        }catch(exception e){system.debug('Exception'+ e);}     
    }
    
    public static void MA2_ECPSGPCheck(List<Contact> contactList){
        list<Contact> SGPUniqueConsumerlist = new list<Contact>();
        list<Contact> SGPUpdateContactlist = new list<Contact>();
        set<string> InactiveECPMobile = new set<string>();
        set<string> ActiveECPMobile = new set<string>();
        list<Contact> SGPConsumerlist = new list<Contact>();
        list<Contact> SGPContactlist = new list<Contact>();       
        try{          
            for(Contact con : contactList){
                string recordtypename = Schema.SObjectType.Contact.getRecordTypeInfosById().get(con.recordtypeid).getname();                      
                if(con.MA2_Country_Code__c == 'SGP' && recordtypename == 'ECP' && con.MobilePhone !='' && con.Inactive__c == false){
                    ActiveECPMobile.add(con.MobilePhone);
                }
                if(con.MA2_Country_Code__c == 'SGP' && recordtypename == 'ECP' && con.MobilePhone !='' && con.Inactive__c == true){
                    InactiveECPMobile.add(con.MobilePhone);
                }
            }  
            
            SGPUniqueConsumerlist = [select Id,MobilePhone,Is_ECP_Data_ECP__c from Contact where MA2_Country_Code__c = 'SGP' and recordtype.name = 'Consumer' and MobilePhone IN : ActiveECPMobile order by lastmodifieddate desc limit 1];           
            if(!SGPUniqueConsumerlist.isEmpty()){
                for(Contact con: SGPUniqueConsumerlist){
                    con.Is_ECP_Data_ECP__c = true;
                    SGPUpdateContactlist.add(con);
                }
            } 
            
            SGPConsumerlist = [select Id,MobilePhone,Is_ECP_Data_ECP__c from Contact where MA2_Country_Code__c = 'SGP' and recordtype.name = 'Consumer' and MobilePhone IN : InactiveECPMobile order by lastmodifieddate desc limit 1];
            if(!SGPConsumerlist.isEmpty()){
                for(Contact con: SGPConsumerlist){
                    con.Is_ECP_Data_ECP__c = false;
                    SGPContactlist.add(con);
                }
            }         
            update SGPUpdateContactlist;
            update SGPContactlist;          
        }catch(exception e){system.debug('Exception' + e);}
    }
    
    public static void MA2_ECPSGPDelete(List<Contact> contactList){
        list<Contact> SGPUniqueConsumerlist = new list<Contact>();
        list<Contact> SGPUpdateContactlist = new list<Contact>();
        set<string> ECPMobile = new set<string>();        
        try{            
            for(Contact con : contactList){
                string recordtypename = Schema.SObjectType.Contact.getRecordTypeInfosById().get(con.recordtypeid).getname();                      
                if(con.MA2_Country_Code__c == 'SGP' && recordtypename == 'ECP' && con.MobilePhone !='' && con.Inactive__c == true){
                    ECPMobile.add(con.MobilePhone);
                }
            }              
            SGPUniqueConsumerlist = [select Id,MobilePhone,Is_ECP_Data_ECP__c from Contact where MA2_Country_Code__c = 'SGP' and recordtype.name = 'Consumer' and MobilePhone IN : ECPMobile order by lastmodifieddate desc limit 1];            
            if(!SGPUniqueConsumerlist.isEmpty()){
                for(Contact con: SGPUniqueConsumerlist){
                    con.Is_ECP_Data_ECP__c = false;
                    SGPUpdateContactlist.add(con);
                }
            }            
            update SGPUpdateContactlist;            
        }catch(exception e){system.debug('Exception' + e);}
    }
}