@isTest
public class MA2_ConsumerECPSGPTest {
    static testMethod void MA2_ConsumerECPSGPTest1() {
        TriggerHandler__c testvar = new TriggerHandler__c();
        testvar.Name = 'HandleTriggers';
        testvar.MA2_createContact__c = true;
        insert testvar;
        
        Id RecordTypeIdECP = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('ECP').getRecordTypeId();
        Id RecordTypeIdConsumer = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
        
        Contact con1 = new Contact();
        con1.LastName = 'Test ECP';
        con1.MA2_Country_Code__c = 'SGP';
        con1.MobilePhone ='123456789';
        con1.RecordTypeId = RecordTypeIdECP;
        insert con1;
        
        Contact con2 = new Contact();
        con2.LastName = 'Test Consumer';
        con2.MA2_Country_Code__c = 'SGP';
        con2.MobilePhone ='987654321';
        con2.RecordTypeId = RecordTypeIdConsumer;
        insert con2;
        
        con2.MobilePhone ='123456789';
        update con2;
        delete con1;
        System.assertEquals(con1.LastName,'Test ECP','success');
    }
    
    static testMethod void MA2_ConsumerECPSGPTest2() {
        TriggerHandler__c testvar = new TriggerHandler__c();
        testvar.Name = 'HandleTriggers';
        testvar.MA2_createContact__c = true;
        insert testvar;
        Id RecordTypeIdECP = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('ECP').getRecordTypeId();
        Id RecordTypeIdConsumer = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();       
        Contact con1 = new Contact();
        con1.LastName = 'Test Consumer';
        con1.MA2_Country_Code__c = 'SGP';
        con1.MobilePhone ='123456789';
        con1.RecordTypeId = RecordTypeIdConsumer;
        insert con1;
        
        Contact con2 = new Contact();
        con2.LastName = 'Test Consumer';
        con2.MA2_Country_Code__c = 'SGP';
        con2.MobilePhone ='987654321';
        con2.RecordTypeId = RecordTypeIdECP;
        insert con2;
        
        con2.MobilePhone ='123456789';
        update con2;
        delete con2;     
        System.assertEquals(con1.LastName,'Test Consumer','success');
    }
}