/**
* File Name: MA2_ConsumerLadderPointVerification
* Description : Batch job for scheduling consumer with transaction report
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | neel.kamal@cognizant.com
* 
* Modification Log 
* =============================================================== **/

global class MA2_ConsumerLadderPointVerification implements Database.Batchable<SObject>, Database.Stateful{
    
    global List<Contact> contactList = new List<Contact>();
    global string header = 'Region, Consumer Id , Consumer Name ,Apigee Transaction Id,Transaction Points Awarded , Points Redeemed, Transaction Total Points, Ladder Points, Total Points , Ladder ,Expiry Points\n';
    global string finalstr{get; set;}
    global string RecString;
    
    /* Method for querying all the Consumer Records with Transactions */
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([select id,MA2_Country_Code__c,MembershipNo__c,name,MA2_GrandPoint__c,MA2_UsePoint__c,MA2_Grade__c,
                                         MA2_ExpiryPoints__c,(select id,ContactId__c,MA2_Contact__c,MA2_Points__c,MA2_Redeemed__c,MA2_TransactionId__c,MA2_ExpiryPoints__c
                                                              from Transaction_Details__r) from contact where( MA2_Country_Code__c = 'HKG' OR 
                                                                                                              MA2_Country_Code__c = 'SGP' OR MA2_Country_Code__c = 'TWN') and MA2_GrandPoint__c != null and MA2_UsePoint__c != null
                                         and MA2_Grade__c != null and lastmodifieddate = Today] 
                                       );
    }
    
    /*Method for excuting the query record */
    
    global void execute(Database.batchableContext BC,List<Contact> contactList){
        System.Debug('Executestarted');
        finalstr = '';
        list<Contact> transactionwithwrongpoints = new list<Contact>();
        decimal transactionpoints=0.0,redeemedpoints=0.0,expirypoints = 0.0;
        decimal transactiontotalpoints;
        set<Id> contactIds = new set<Id>();
        set<Id> transactionIds = new set<Id>();
        map<string,string> contacttransaction = new Map<string,string>();
        if(contactlist.size()>0){
            for(Contact k : contactList){
                for(TransactionTd__c l : k.Transaction_Details__r){
                    if(l.MA2_Points__c != null && l.MA2_Points__c != 0.0){
                        transactionpoints= transactionpoints + l.MA2_Points__c;                            
                    }    
                    
                    if(l.MA2_Redeemed__c != null && l.MA2_Redeemed__c != 0.0){
                        redeemedpoints=redeemedpoints + l.MA2_Redeemed__c;
                    }
                    if(l.MA2_ExpiryPoints__c != null && l.MA2_ExpiryPoints__c != 0.0){
                        expirypoints= expirypoints + l.MA2_ExpiryPoints__c;
                    }
                    
                   
                }
               
                    if(integer.valueof(k.MA2_UsePoint__c) != integer.valueof(transactionpoints-(redeemedpoints-expirypoints))){
                        transactiontotalpoints = integer.valueof(transactionpoints-(redeemedpoints-expirypoints));
                        contacttransaction.put(string.valueof(k.id),string.valueof(transactiontotalpoints));
                        contactIds.add(k.id);
                        system.debug('Check 1'+ contactIds);                                                            
                  //  }
                }
                //
                if(integer.valueof(transactionpoints)!= integer.valueof(k.MA2_GrandPoint__c )){
                    transactiontotalpoints = integer.valueof(transactionpoints-redeemedpoints-expirypoints);
                    contacttransaction.put(string.valueof(k.id),string.valueof(transactiontotalpoints));
                    contactIds.add(k.id);                    
                } 
                
                
                if((k.MA2_Country_Code__c == 'SGP' && integer.valueof(k.MA2_GrandPoint__c)>= 6000 && k.MA2_Grade__c == 'Base') || (k.MA2_Country_Code__c == 'SGP' && integer.valueof(k.MA2_GrandPoint__c)< 6000 && k.MA2_Grade__c == 'VIP')){
                    transactiontotalpoints = integer.valueof(transactionpoints-redeemedpoints-expirypoints);
                    contacttransaction.put(string.valueof(k.id),string.valueof(transactiontotalpoints));                                   
                    contactIds.add(k.id);
                    system.debug('Check 3'+ contactIds);                    
                }
                
                if((k.MA2_Country_Code__c == 'TWN' && integer.valueof(k.MA2_GrandPoint__c)>= 150 && k.MA2_Grade__c == 'Base') || (k.MA2_Country_Code__c == 'TWN' && integer.valueof(k.MA2_GrandPoint__c)< 150 && k.MA2_Grade__c == 'VIP')){
                    transactiontotalpoints = integer.valueof(transactionpoints-redeemedpoints-expirypoints);
                    contacttransaction.put(string.valueof(k.id),string.valueof(transactiontotalpoints));               
                    contactIds.add(k.id);
                    system.debug('Check 4'+ contactIds);                    
                }  
                //
                transactionpoints=0.0;
                redeemedpoints=0.0;    
            }             
        }
        
        transactionwithwrongpoints = [select id,MembershipNo__c,MA2_Country_Code__c,name,MA2_GrandPoint__c,MA2_UsePoint__c,MA2_Grade__c,MA2_ExpiryPoints__c,(select id,ContactId__c,MA2_Contact__c,MA2_Points__c,MA2_Redeemed__c,MA2_ExpiryPoints__c,MA2_TransactionId__c from Transaction_Details__r)
                                      from contact where( MA2_Country_Code__c = 'HKG' OR MA2_Country_Code__c = 'SGP' OR MA2_Country_Code__c = 'TWN') and MA2_GrandPoint__c != null and MA2_UsePoint__c != null
                                      and MA2_Grade__c != null and Id IN: contactIds order by MembershipNo__c];    
        
        for(Contact k : transactionwithwrongpoints){
            for(TransactionTd__c l : k.Transaction_Details__r){
                RecString = k.MA2_Country_Code__c+','+k.MembershipNo__c +','+k.Name+','+l.MA2_TransactionId__c+','+l.MA2_Points__c+','+l.MA2_Redeemed__c+','+string.valueof(contacttransaction.get(k.id))+','+k.MA2_GrandPoint__c+','+k.MA2_UsePoint__c+','+k.MA2_Grade__c+','+l.MA2_ExpiryPoints__c+'\n';                                                     
                finalstr = finalstr +RecString; 
            }           
        }        
    }
    
    global void finish(Database.batchableContext BC){  
        finalstr = header + finalstr;
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>(); 
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string datestamp = string.valueof(system.now());
        string csvname= 'Consumer Ladder Point Verification'+datestamp+'.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        fileAttachments.add(csvAttc);        
        string body = '';
        body += 'Hello Team,' + '\n';
        body += '\n';
        body += 'Attached the Consumer details extracted for Ladder/ VIP points validation.' + '\n';
        body += '\n';
        body += 'Regards' + '\n';
        body += 'Skywalker Support'+ ' '+ '\n';
        body += '\n';
        body += 'Note:- This email has been sent automatically by SFDC Interface.'+ ' '+ '\n';
        body += '\n';
        
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();        
        string emailAddress = System.Label.MA2_ToEmailAddress;
        string[] toAddresses = emailAddress.split(',');     
        string subject ='Ladder Point Verification File'+string.valueof(system.now())+'.csv';
        email.setSubject(subject);
        email.setToAddresses(toAddresses);
        email.setPlainTextBody(body);
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});        
    }
    
}