/**
* File Name: MA2_ConsumerLadderPointVerificationTest
* Description : Test class for MA2_ConsumerLadderPointVerification
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | nkamal8@cognizant.com
* Modification Log 
* ================================================================
*    Ver  |Date         |Author                |Modification
*    2.0  |10-OCT-2017  |nkamal8@its.jnj.com   |Class created
*/
@isTest
public class MA2_ConsumerLadderPointVerificationTest{
    /* Method for excuting the test class */
    static Testmethod void runBatchJobMethod(){
        Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id; 
        TriggerHandler__c mcs = new TriggerHandler__c(Name='HandleTriggers',MA2_createUpdateTransaction__c =true,
                                                      MA2_createUpdateTransactionProd__c = True);
        insert mcs;
        final List<Account> accList = new List<Account>();
        accList.addAll(TestDataFactory_MyAcuvue.createAccount(1)); 
        Contact conRec = new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',
                                     RecordTypeId = consumerRecTypeId,MembershipNo__c = 'HKG-192839',MA2_GrandPoint__c=10,MA2_UsePoint__c = 20,
                                     MA2_Grade__c = 'BASE',MA2_ExpiryPoints__c = 200,
                                     AccountId = accList[0].Id, email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'HKG',
                                     
                                     MA2_Contact_lenses__c='No',
                                     DOB__c  = System.Today(),
                                     MA2_PreAssessment__c = true);
        insert conRec;
        
        TransactionTd__c trans = new TransactionTd__c(AccountID__c = accList[0].Id, MA2_Contact__c = conRec.Id,MA2_ExpiryPoints__c = 200,
                                                      MA2_AccountId__c = '123456',MA2_ContactId__c = '23456' , MA2_Points__c = 10 ,MA2_Redeemed__c = 10,
                                                      MA2_PointsExpired__c = true,CreatedDateTest__c = date.newinstance(2015,01,12));
        insert trans;
        
        final List<TransactionTd__c> transactionList = new List<TransactionTd__c>();
        transactionList.add(trans);
        
        Test.startTest();
        Database.executeBatch(new MA2_ConsumerLadderPointVerification());
        System.assertEquals(trans.MA2_AccountId__c,'123456','Success');
        Test.stopTest();
    }
}