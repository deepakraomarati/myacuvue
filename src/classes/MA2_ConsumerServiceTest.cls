@isTest
public class MA2_ConsumerServiceTest{
   
    /*
     * Method for excuting the test class
     */
    static Testmethod void updateConsumerMethod(){
       Credientials__c cred = new Credientials__c(Name = 'consumerstorerelation' , 
                    Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                    Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                    Target_Url__c = 'https://jnj-dev.apigee.net/v1/sfdc/consumerstorerelation/add');
        insert cred;
            final List<Account> accList = new List<Account>();
            final List<Contact> conList = new List<Contact>();
            final List<Consumer_Relation__c> consumerList=new List<Consumer_Relation__c>();
        final string Name = 'SGP';
        final string AccountId = '123456';
        final Account acc = new Account(Name = 'Test' , MA2_AccountId__c = AccountId ,AccountNumber = '54321',
                                        Marketing_Program__c = 'AEC',OutletNumber__c = '54321',ActiveYN__c = true,
                                        My_Acuvue__c = 'Yes',CountryCode__c = Name,PublicZone__c = 'Testtt');
        
        insert acc;
            accList.add(acc);
            conList.addAll(TestDataFactory_MyAcuvue.createContact(1,accList));
            Consumer_Relation__c cons=new Consumer_Relation__c();
            cons.AccountId__c= accList[0].Id;
            cons.ContactID__c=conList[0].Id;
            cons.MA2_MyECP__c=true;
            insert cons;
        	cons.MA2_MyECP__c = False;
        	Update cons;
            system.assertEquals(acc.Name, 'Test', 'success');
    }
}