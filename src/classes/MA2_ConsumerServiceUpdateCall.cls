public class MA2_ConsumerServiceUpdateCall{
    
     public static void sendData(List<Consumer_Relation__c> consumerList){
         String jsonstringfinal;
         String jsonString;
         Set<JSONstringtosend> jsendlist =new Set<JSONstringtosend>();
         List<Consumer_Relation__c> updateList=[select Id,lastmodifiedBy.Name,ContactID__r.MembershipNo__c,AccountId__r.OutletNumber__c,AccountId__r.ECP_Name__c from 
                                                Consumer_Relation__c where id in:consumerList
                                                     ];
        final Set<String> consumerFilterList= new Set<String>();
        for(Consumer_Relation__c cons : updateList){
            String userName = cons.lastmodifiedBy.Name;
            userName = userName.toLowercase();
            if(!userName.contains('web')){
                consumerFilterList.add(cons.ContactID__r.MembershipNo__c);        
            }
        }
      
     if(consumerList.size() > 0){
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            for(String cr : consumerFilterList){
                JSONstringtosend jsend=new JSONstringtosend ();
              jsend.consumerId = cr;
                jsendlist.add(jsend);
            }
            if(jsendlist.size()>0){
                jsonString=json.serializePretty(jsendlist);
            }
            if (jsonString != null || jsonString != '') {
                //jsonstringfinal = '\n';
                jsonstringfinal = jsonString;
                //jsonstringfinal += '\n'  ;
            } 
        }
        System.Debug('jsonString--'+jsonString);
         if(jsonstringfinal != '' &&  jsonstringfinal != null){
            sendtojtracker(jsonstringfinal);
        }
    }
    
    /*
     * Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONObject class
     */
    @future(callout = true)
    public static void sendtojtracker(String jsonBody) {
        JSONObject oauth = null;
       Credientials__c testPub = new Credientials__c();
       if(Credientials__c.getInstance('consumerstorerelation') != null){
           testPub  = Credientials__c.getInstance('consumerstorerelation');
           if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
                       final String ClientId = testPub.Client_Id__c;
                       final String ClientSecret = testPub.Client_Secret__c;
                       final String TargetUrl = testPub.Target_Url__c;
                       final String ApiKey = testPub.Api_Key__c; 
                       oauth = oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody);
           }
       }    
       /*SonarQube Fix*/	   
       //System.debug('------oauth response------>>>>'+oauth);
        
    }
    
    /*
     * Method for sending data to the Apigee system
     */
    private static JSONObject oauthLogin(String targetUrl, String clientId, String clientSecret, String apiKeyValue,String jsonBody) {
      HttpRequest loginRequest = New HttpRequest();
        loginRequest.setTimeout(120000);
      loginRequest.setMethod('POST');
      loginRequest.setEndpoint(targetUrl);
      loginRequest.setHeader('grant_type', 'authorization_code');
      loginRequest.setHeader('client_id',clientId);
      loginRequest.setHeader('client_secret',clientSecret);
      loginRequest.setHeader('apikey',apiKeyValue);
      loginRequest.setHeader('Content-Type', 'application/json');
     
      system.debug('<<<<jsonString>>>>>'+jsonBody);

      loginRequest.setBody(jsonBody);
      Http Http = New Http();
      HTTPResponse loginResponse = new HTTPResponse();
      if ( !Test.isRunningTest() ){
          loginResponse = http.send(loginRequest);
          JSONObject oAuth = (JSONObject) JSON.deserialize(loginResponse.getbody(), JSONObject.class);
          return oAuth;
      }
      
      System.Debug('loginResponse --'+loginResponse.getBody());
    
      return null;
 }
 
 // Inner class for setting value 
 public class JSONObject {
      public String id {
       get;
       set;
      }
      public String issued_at {
       get;
       set;
      }
      public String instance_url {
       get;
       set;
      }
      public String signature {
       get;
       set;
      }
      public String access_token {
       get;
       set;
      }
 }
 public class JSONstringtosend {
        public String consumerId {
            get;set;
        }
    }

}