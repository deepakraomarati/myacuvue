@isTest
public class MA2_Consumer_Test
{
    static testMethod void consumerTestMethod() 
    {
        final string countrycode = 'SGP';
        Final TriggerHandler__c record = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                               MA2_ConsumerRelationship__c = true,MA2_createContact__c = True);
        insert record;
        final Credientials__c cred = new Credientials__c(Name = 'consumerstorerelation' , 
                                                         Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                                                         Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                                                         Target_Url__c = 'https://jnj-dev.apigee.net/v1/consumer/catalog/addcoupon');
        insert cred;
        
        TestDataFactory_MyAcuvue.insertCustomSetting();
        List<Consumer_Relation__c> cnsList = new List<Consumer_Relation__c>();
        List<Consumer_Relation__c> cnsList2 = new List<Consumer_Relation__c>();
        account ac = new account();
        ac.name='test';
        ac.OutletNumber__c ='123';
        insert ac;
        MA2_CountryCode__c CountryCodeC = new MA2_CountryCode__c(Name= countrycode, MA2_CountryCodeValue__c = countrycode);
        insert CountryCodeC;
        MA2_Country_Currency_Map__c CountryMap = new MA2_Country_Currency_Map__c(Name='SGP',Currency__c='SGD');
        insert CountryMap;
        
        contact con =new contact();
        con.lastname='test1';
        con.MembershipNo__c ='1234';
        con.MA2_Country_Code__c ='SGP';
        con.RecordTypeId = [select Id from RecordType where sObjectType = 'Contact' and Name = 'Consumer'].Id;
        con.MA2_AssignCoupon__c = 'Changed';
        con.MA2_PreAssessment__c=true;
        insert con;
                
        for(integer i =0;i<=200;i++){
            Consumer_Relation__c cons= new Consumer_Relation__c();
            cons.AccountId__c=ac.id;
            cons.ContactID__c=con.id;
            cons.MA2_AccountId__c=ac.OutletNumber__c ;
            cons.MA2_ContactId__c=con.MembershipNo__c;
            cons.MA2_MyECP__c=true;
            cnsList.add(cons);
        }
        insert cnsList;
        
        for(Consumer_Relation__c co :cnsList ){
            co.MA2_MyECP__c = False;
            cnsList2.add(co);
        }
        system.assertEquals(cnsList2[0].MA2_MyECP__c,false,'success');
        update cnsList2;
    }
}