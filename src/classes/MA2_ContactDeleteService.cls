public class MA2_ContactDeleteService{
    
    /* Method for creating json body 
*/
    public static void sendContactDataToApigee(List<Contact> ConList){
        
        String jsonString  = '';
        // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
        Set<id> recordTypIDs=new Set<id>();
        Id jjvproRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP Contacts').getRecordTypeId();
        
        for(contact c: ConList)
        {
            // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
            if(c.RecordTypeId != jjvproRecordTypeId)
            {
                recordTypIDs.add(c.RecordTypeId);
            }
        }
        map<id,recordType> mapIdRecordType=new map<id,recordType>([select id, name from Recordtype where id in :recordTypIDs]);
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        for(Contact con : ConList){  
            // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
            if(con.RecordTypeId != jjvproRecordTypeId) 
            {                    
                gen.writeStringField('mobileNumber',con.MobilePhone+'');                         
                gen.writeStringField('userType',mapIdRecordType.get(con.RecordTypeId).name+'');    
            }  
        }
        gen.writeEndObject();
        jsonString = jsonString + gen.getAsString();
        
        System.Debug('jsonString--'+jsonString);
        if(jsonString != ''){
            sendContactDatatojtracker(jsonString);
        }
    }
    
    /*
* Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONContactObject class
*/
    @future(callout = true)
    public static void sendContactDatatojtracker(String jsonBody) {
        JSONContactObject oauth = null;
        Credientials__c testPub = new Credientials__c();
        if(Credientials__c.getInstance('ContactDeleteAPI') != null){
            testPub  = Credientials__c.getInstance('ContactDeleteAPI');
            if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
                final String ClientId = testPub.Client_Id__c;
                final String ClientSecret = testPub.Client_Secret__c;
                final String TargetUrl = testPub.Target_Url__c;
                final String ApiKey = testPub.Api_Key__c; 
                oauth = oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody);
            }
        }    
    }
    
    /*
* Method for sending data to the Apigee system
*/
    private static JSONContactObject oauthLogin(String targetUrl, String clientId, String clientSecret, String apiKeyValue,String jsonBody) {
        HttpRequest loginRequest = New HttpRequest();
        loginRequest.setMethod('POST');
        loginRequest.setEndpoint(targetUrl);
        loginRequest.setHeader('grant_type', 'authorization_code');
        loginRequest.setHeader('client_id',clientId);
        loginRequest.setHeader('client_secret',clientSecret);
        loginRequest.setHeader('apikey',apiKeyValue);
        loginRequest.setHeader('Content-Type', 'application/json');
        
        system.debug('<<<<jsonString>>>>>'+jsonBody);
        
        loginRequest.setBody(jsonBody);
        Http Http = New Http();
        HTTPResponse loginResponse = new HTTPResponse();
        if ( !Test.isRunningTest() ){
            loginResponse = http.send(loginRequest);
            JSONContactObject oAuth = (JSONContactObject) JSON.deserialize(loginResponse.getbody(), JSONContactObject.class);
            return oAuth;
        }
        
        System.Debug('loginResponse --'+loginResponse.getBody());
        
        return null;
    }
    
    // Inner class for setting value 
    public class JSONContactObject {
        public String id {get;set;}
        public String issued_at {get;set;}
        public String instance_url {get;set;}
        public String signature {get;set;}
        public String access_token {get;set;}
    }
}