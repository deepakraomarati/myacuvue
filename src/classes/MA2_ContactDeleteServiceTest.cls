/*
* File Name: MA2_ContactDeleteServiceTest
* Description : Test class for MA2_ContactDeleteService 
* Copyright : Johnson & Johnson 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    2.0  |30-Aug-2017  |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_ContactDeleteServiceTest{
    
    /* Method for excuting the test class */
    static Testmethod void deleteContactMethod(){
        final Credientials__c cred = new Credientials__c(Name = 'ContactDeleteAPI' , 
                                                   Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                                                   Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                                                   Target_Url__c = 'https://jnj-global-test.apigee.net/v1/sfdc/profile/delete');
        insert cred;
        
        
        Test.startTest();
        final List<Account> accList = new List<Account>();
        accList.addAll(TestDataFactory_MyAcuvue.createAccount(1));
        system.assertEquals(acclist.size(), 4,'Success');                       
        final List<Contact> ConList = new List<Contact>();
        ConList.addAll(TestDataFactory_MyAcuvue.createContact(1, accList));
        
        final Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id;
        final contact con = new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',RecordTypeId = consumerRecTypeId,
                                  AccountId = accList[0].Id, email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'HKG',MA2_Contact_lenses__c='No',
                                  DOB__c  = System.Today(),
                                  MA2_PreAssessment__c = true);
        insert con;
        
        MA2_ContactDeleteService.sendContactDataToApigee(ConList);      
        Test.stopTest();
    }
}