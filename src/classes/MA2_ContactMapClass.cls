public class MA2_ContactMapClass{

    public static void mapContact(list<contact> contList)
    {
        List<String> lstAccId =new List<String>();
        List<Contact> lstcon =new List<Contact>();   
        Map<Id,Account> MapAccount = new Map<Id,Account>();
        
        if(Trigger.isBefore){
            for(Contact Conobj:contList)
            {
                lstcon.add(Conobj);
            
                if(Conobj.MA2_AccountID__c!=null){
                    lstAccId.add(Conobj.MA2_AccountID__c);
                }
            }
          
            if(lstAccId.size()>0 && !lstAccId.isEmpty())
            {    
                MapAccount=new Map<Id,Account>([select id,Name,OutletNumber__c from account where OutletNumber__c IN:lstAccId]);
            } 
            Map<String,Id> mapAccountIds= new Map<String,Id>();
            for(Account acc:MapAccount.Values()){
                mapAccountIds.put(acc.OutletNumber__c,acc.Id);
            }
            List<Contact> lstContact = new List<Contact>();
    
            for(Contact c:lstcon )
            {
                if(c.MA2_AccountID__c!=null && mapAccountIds.get(c.MA2_AccountID__c)!=null){
                     c.AccountId= mapAccountIds.get(c.MA2_AccountID__c);
                       
                }
                else 
                if(c.Aws_AccountId__c!=null && c.MA2_Country_Code__c == 'KOR')
                {
                    c.MA2_AccountID__c = c.Account.OutletNumber__c;
                    system.debug('---------**********---------'+c.MA2_AccountID__c);
                    system.debug('---------**********---------'+c.Account.OutletNumber__c);                   
                    c.MA2_ContactID__c = c.MembershipNo__c;
                    system.debug('---------**********---------'+c.MembershipNo__c);
                }
                lstContact.add(c);
            }
               
        }
    }
}