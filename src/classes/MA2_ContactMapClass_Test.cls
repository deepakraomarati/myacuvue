@isTest
public with sharing class MA2_ContactMapClass_Test 
{    
    static testmethod void insertContact()
    {
        
        account ac = new account();
        ac.name='test';
        ac.OutletNumber__c ='123';
        insert ac;
        
        Contact con=new Contact();
        con.AccountId__c=ac.id;
        con.LastName='Ram';
        con.MA2_AccountID__c=ac.OutletNumber__c;
        insert con;
        system.assertEquals(con.AccountId__c,ac.id,'success');
    }
}