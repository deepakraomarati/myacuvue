/**
* File Name: MA2_ContactSendtoApigee
* Description : Sending Contact to Apigee System
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |14-Oct-2016 |hsingh53@its.jnj.com  |New Class created
*/
public class MA2_ContactSendtoApigee{
    
    public static void sendData(List<Contact> contactList){
		// Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
        Id jjvproRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP Contacts').getRecordTypeId();
        List<Contact> conRecordList = [select LastModifiedBy.Name,Id,MembershipNo__c,AccountId,UserPwd__c,UserID__c,lastName,firstName,MobilePhone,Email,
        MA2_FriendsReferralCode__c,MA2_Gender__c,DOB__c,MA2_Uuid__c,NRIC__c,HasOptedOutOfEmail,smsaccept__c,MA2_Grade__c,agree_tc__c ,MA2_GrandPoint__c,
        MA2_UsePoint__c,DeviceType__c,AccessToken__c,DiopterL__c,DiopterR__c,AstiYN_L__c,AstiYN_R__c,CylinderInfoL__c,CylinderInfoR__c,AxisInfoL__c,
        AxisInfoR__c,MA2_spectacles__c,MA2_Contact_lenses__c,createdDate,lastModifiedDate,MA2_QualificationCertifcate__c,isPrimary__c,RecordTypeId,
        MA2_Twitter__c,MA2_WeChat__c,MA2_Facebook__c,MA2_Google__c,MA2_Country_Code__c,MA2_Subtype__c,MA2_ParentContact__c,MA2_Language__c,MA2_TermsCondition__c,
        MA2_PreAssessment__c,MA2_FeedbackRating__c,MA2_FeedbackLike__c,UserProfile__c,MA2_ReferralCode__c,Inactive__c,MA2_GraduationYear__c,
        MA2_Institute_of_Graduation__c,Educational_Qualifications__c,MA2_OOB_Registration_Number__c,MA2_OOB_Classification__c,MA2_AccessStatus__c
         from Contact where Id in: contactList];
        //System.Debug('conRecord --'+conRecord[0].createdBy.Name);
        final List<Contact> contactFilterList = new List<Contact>();  
        System.Debug('conRecordList--'+conRecordList);
        Map<String,MA2_CountryCode__c> countryCodeMap = MA2_CountryCode__c.getAll();
        System.Debug('countryCodeMap--'+countryCodeMap);
        for(Contact con : conRecordList){
             System.Debug('con.createdBy.Name--'+con.LastModifiedBy.Name);
             System.Debug('con.createdBy.Name--'+con.LastModifiedBy);
            String userName = con.LastModifiedBy.Name;
            userName = userName.toLowercase();
            System.Debug('userName--'+userName);
            System.Debug('countryCodeMap--'+countryCodeMap);
			// Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
            if(!userName.contains('web') && countryCodeMap.containsKey(con.MA2_Country_Code__c) && con.RecordTypeId != jjvproRecordTypeId){
                System.Debug('testt--');
                contactFilterList.add(con);        
            }
        }
        System.Debug('contactFilterList--'+contactFilterList);
        if(contactFilterList.size() != 0){
            sendApigeeData(contactFilterList);
        }
    }
    
    /*
     * Method for creating json body 
     */
    public static void sendApigeeData(List<Contact> contactList){
    
        String jsonString  = '{"profiles":[';
        
        if(contactList.size() > 0){
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            for(Contact con : contactList){
                  gen.writeStringField('contact Id',con.Id+'');
                  gen.writeStringField('contactId',con.MembershipNo__c+'');
                  gen.writeStringField('myStoreID', con.AccountId+'');
                  gen.writeStringField('membershipNo', con.MembershipNo__c+'');
                  gen.writeStringField('userID', con.UserID__c+''); 
                  gen.writeStringField('userPassword', con.UserPwd__c+'');
                  gen.writeStringField('lastName', con.lastName+'');
                  gen.writeStringField('firstName', con.firstName+'');
                  String mobileNum = con.MobilePhone+'';
                  if(mobileNum.contains('(')){
                      mobileNum  = mobileNum.subString(1,mobileNum.length());
                  }
                  if(mobileNum.contains(')')){
                      mobileNum  = mobileNum.replaceAll('\\)','');
                  }
                  if(mobileNum.contains('-')){
                      mobileNum  = mobileNum.replaceAll('\\-','');
                  }
                  mobileNum  = mobileNum.replaceAll(' ','');
                  
                  
                  gen.writeStringField('mobile', mobileNum);
                  gen.writeStringField('email', con.Email+'');
                  gen.writeStringField('FriendsReferralCode', con.MA2_FriendsReferralCode__c+'');
                  
                  gen.writeStringField('gender', con.MA2_Gender__c+'');
                  gen.writeStringField('DOB', con.DOB__c+'');
                  gen.writeStringField('uuid', con.MA2_Uuid__c+'');
                  gen.writeStringField('NRIC', con.NRIC__c+'');  
                  gen.writeStringField('emailOptOut',con.HasOptedOutOfEmail+'');  
                  /*if(con.HasOptedOutOfEmail){
                       gen.writeStringField('emailOptOut','Yes'); 
                  }else{
                       gen.writeStringField('emailOptOut','No'); 
                  }*/ 
                   gen.writeStringField('smsAccept',con.smsaccept__c+'');  
                  /*if(con.smsaccept__c){
                       gen.writeStringField('smsAccept','Yes'); 
                  }else{
                       gen.writeStringField('smsAccept','No'); 
                  }*/
                  if(con.MA2_Grade__c != null){
                      gen.writeStringField('ladder', con.MA2_Grade__c.toUpperCase());
                  }else{
                      gen.writeStringField('ladder', '');
                  }  
                  gen.writeStringField('tncStatus', con.agree_tc__c +'');   
                  gen.writeStringField('ladderPoint', con.MA2_GrandPoint__c+'');   
                  gen.writeStringField('totalPoints', con.MA2_UsePoint__c+'');   
                  gen.writeStringField('deviceType', con.DeviceType__c+'');   
                  gen.writeStringField('accessToken', con.AccessToken__c+'');   
                  gen.writeStringField('diopterL', con.DiopterL__c+'');   
                  gen.writeStringField('diopterR', con.DiopterR__c+'');
                  gen.writeStringField('astiYN_L', con.AstiYN_L__c+'');
                  gen.writeStringField('astiYN_R', con.AstiYN_R__c+'');
                  gen.writeStringField('cylinderInfoL', con.CylinderInfoL__c+'');
                  gen.writeStringField('cylinderInfoR', con.CylinderInfoR__c+'');
                  gen.writeStringField('axisInfoL', con.AxisInfoL__c+'');
                  gen.writeStringField('axisInfoR', con.AxisInfoR__c+'');
                  gen.writeStringField('spectacles',con.MA2_spectacles__c+'');
                /*  if(con.MA2_spectacles__c){
                       gen.writeStringField('spectacles','Yes'); 
                  }else{
                       gen.writeStringField('spectacles','No'); 
                  }  */
                  gen.writeStringField('Contactlenses', con.MA2_Contact_lenses__c+'');
                  gen.writeStringField('createdDate', con.createdDate+''); 
                  gen.writeStringField('lastModifiedDate', con.lastModifiedDate+''); 
                  gen.writeStringField('qualificationCertifcate', con.MA2_QualificationCertifcate__c+'');
                  gen.writeStringField('isPrimary',con.isPrimary__c+''); 
                  /*if(con.isPrimary__c){
                       gen.writeStringField('isPrimary','Yes'); 
                  }else{
                       gen.writeStringField('isPrimary','No'); 
                  }*/
                  gen.writeStringField('contactRecordType', con.RecordTypeId+''); 
                  gen.writeStringField('twitter', con.MA2_Twitter__c+''); 
                  gen.writeStringField('weChat', con.MA2_WeChat__c+''); 
                  gen.writeStringField('facebook', con.MA2_Facebook__c+''); 
                  gen.writeStringField('google', con.MA2_Google__c+''); 
                  gen.writeStringField('countryCode', con.MA2_Country_Code__c+''); 
                  gen.writeStringField('contactSubType', con.MA2_Subtype__c+''); 
                  gen.writeStringField('parentContact', con.MA2_ParentContact__c+''); 
                  gen.writeStringField('languagePreference', con.MA2_Language__c+''); 
                  gen.writeStringField('termsAndConditions',con.MA2_TermsCondition__c+''); 
                 // gen.writeStringField('termsAndConditions', con.MA2_TermsCondition__c+''); 
                 // gen.writeStringField('myAcuvueStatus', '');
                  gen.writeStringField('PreAssessment',String.ValueOf(con.MA2_PreAssessment__c)); 
                 /*if(con.MA2_PreAssessment__c){
                       gen.writeStringField('PreAssessment','Yes'); 
                  }else{
                       gen.writeStringField('PreAssessment','No'); 
                  }   */
                  gen.writeStringField('feedbackRating', con.MA2_FeedbackRating__c+''); 
                  gen.writeStringField('feedbackLike', con.MA2_FeedbackLike__c+''); 
                  gen.writeStringField('userProfile', con.UserProfile__c+'');
                  gen.writeStringField('referralCode', con.MA2_ReferralCode__c+'');
                  gen.writeStringField('highestQualification', con.MA2_QualificationCertifcate__c+'');
                  gen.writeStringField('graduationYear', con.MA2_GraduationYear__c+'');
                  gen.writeStringField('graduationInstitute', con.MA2_Institute_of_Graduation__c+'');
                  gen.writeStringField('educationalQualifications', con.Educational_Qualifications__c+'');
                  gen.writeStringField('oobRegistrationNumber', con.MA2_OOB_Registration_Number__c+'');
                  gen.writeStringField('oobClassification', con.MA2_OOB_Classification__c+'');
                  gen.writeStringField('accessStatus', con.MA2_AccessStatus__c+'');
                  gen.writeStringField('status',con.Inactive__c+''); 
                 /* if(con.MA2_Status__c){
                       gen.writeStringField('status','Yes'); 
                  }else{
                       gen.writeStringField('status','No'); 
                  }   */
            }
            gen.writeEndObject();
            jsonString = jsonString + gen.getAsString() + ']}';
        }
        System.Debug('jsonString--'+jsonString);
        if(jsonString != ''){
            sendtojtracker(jsonString);
        }
    }
    
    /*
     * Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONObject class
     */
    @future(callout = true)
    public static void sendtojtracker(String jsonBody) {
       JSONObject oauth = null;
       Credientials__c testPub = new Credientials__c();
       if(Credientials__c.getInstance('SendContact') != null){
           testPub  = Credientials__c.getInstance('SendContact');
           if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
                       final String ClientId = testPub.Client_Id__c;
                       final String ClientSecret = testPub.Client_Secret__c;
                       final String TargetUrl = testPub.Target_Url__c;
                       final String ApiKey = testPub.Api_Key__c; 
                       oauth = oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody);
           }
       }           
       //Modified by Lhawang to resolve sonar qube issue
	   //System.debug('------oauth response------>>>>'+oauth);      
    }
    
    /*
     * Method for sending data to the Apigee system
     */
    private static JSONObject oauthLogin(String targetUrl, String clientId, String clientSecret, String apiKeyValue,String jsonBody) {
      HttpRequest loginRequest = New HttpRequest();
      loginRequest.setMethod('POST');
      loginRequest.setEndpoint(targetUrl);
      loginRequest.setHeader('grant_type', 'authorization_code');
      loginRequest.setHeader('client_id',clientId);
      loginRequest.setHeader('client_secret',clientSecret);
      loginRequest.setHeader('apikey',apiKeyValue);
      loginRequest.setHeader('Content-Type', 'application/json');
     
      system.debug('<<<<jsonString>>>>>'+jsonBody);

      loginRequest.setBody(jsonBody);
      Http Http = New Http();
      HTTPResponse loginResponse = new HTTPResponse();
      if ( !Test.isRunningTest() ){
          loginResponse = http.send(loginRequest);
          JSONObject oAuth = (JSONObject) JSON.deserialize(loginResponse.getbody(), JSONObject.class);
          return oAuth;
      }
      
      System.Debug('loginResponse --'+loginResponse.getBody());
    
      return null;
 }
 
 // Inner class for setting value 
 public class JSONObject {
      public String id {
       get;
       set;
      }
      public String issued_at {
       get;
       set;
      }
      public String instance_url {
       get;
       set;
      }
      public String signature {
       get;
       set;
      }
      public String access_token {
       get;
       set;
      }
 }
 

}