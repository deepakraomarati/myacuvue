/*
* File Name: MA2_ContactTest
* Description : Test class for Contact Records send to Apigee
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |8-Sep-2016  |hsingh53@its.jnj.com  |New Class created
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    2.0  |30-Aug-2017  |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_ContactTest{
    
    /* Method for excuting the test class */
    static Testmethod void runBatchJobMethod(){
        final TriggerHandler__c cred = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                             GetTransactionTd__c = true, MA2_createUpdateTransaction__c = true, CouponContact__c= true, MA2_CouponInactiveTrigger__c = true,
                                                             MA2_CouponToApigeeTrigger__c= true, MA2_CouponWalletForApigee__c= true, MA2_createUpdateTransactionProd__c= true );
        insert cred;
        Final MA2_CountryCode__c cntrycode = new MA2_CountryCode__c(Name = 'SGP',MA2_CountryCodeValue__c = 'SGP');
        Insert cntrycode;
        
        TestDataFactory_MyAcuvue.insertCustomSetting();
        final List<Contact> contactList = new List<Contact>();
        final string AccountId = '123456';
        final string CountryCurrency = 'SGD';
        final Id consumerRecTypeId2 = [select id from recordtype where name = 'Consumer' and sObjectType = 'Contact'].id;
        List<User> ids = [select id,alias,email,emailencodingkey,lastname,languagelocalekey,localesidkey,profileid,timezonesidkey,
                          username from user where name like '%Lemu Edward%' limit 1];
        Test.startTest();
        final Account acc = new Account(Name = 'Test' , MA2_AccountId__c = AccountId ,AccountNumber = '54321', 
                                        Marketing_Program__c = 'AEC',OutletNumber__c = '54321',ActiveYN__c = true,
                                        My_Acuvue__c = 'Yes',CountryCode__c = 'SGP',PublicZone__c = 'Testtt');
        
        insert acc;        
        System.runAs(ids[0]){
            final Contact con2 = new Contact(LastName = 'test' ,MA2_AccountId__c = acc.id ,MA2_ContactId__c = '23456',
                                             RecordTypeId  = consumerRecTypeId2  ,
                                             AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = CountryCurrency, 
                                             MembershipNo__c = 'SGP123', MA2_Country_Code__c = 'SGP', MA2_Grade__c='Base', DOB__c = date.newinstance(2012,03,03),
                                             MA2_PreAssessment__c = true,MA2_AccessStatus__c = 'Approved',MailingPostalCode = '1234567');
            
            insert con2;
            final JJ_JPN_Address__c address = new JJ_JPN_Address__c(JJ_JPN_PostalCodes__c = '1234567', JJ_JPN_State__c = 'HOKKAIDO',JJ_JPN_Street__c = 'Test');
            insert address;
            update con2;
            system.assertEquals(con2.email, 'test@gmail.com');                       
        }
        
        Test.stopTest();
    }
}