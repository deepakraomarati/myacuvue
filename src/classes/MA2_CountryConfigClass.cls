public class MA2_CountryConfigClass
{
    public static void mapConfig(list<MA2_CountryWiseConfiguration__c> configList)
    {
        Map<String, Id> mapConfig = new Map<String, Id>();
        
        Set<String> setConfigtypr = new Set<String>();
        Set<String> setContcode = new Set<String>();
        Set<String> setcombined = new Set<String>();
        for(MA2_CountryWiseConfiguration__c Conconfig: configList)
        {
            setConfigtypr.add(Conconfig.MA2_ConfigurationType__c);
            setContcode.add(Conconfig.MA2_CountryCode__c);
            setcombined.add(string.valueof(setConfigtypr));
            setcombined.add(string.valueof(setContcode));
        }   
        for(
            MA2_CountryWiseConfiguration__c  Conconfig:
            [
                SELECT Id, Name,MA2_ConfigurationType__c,MA2_CountryCode__c,MA2_Description__c
                FROM   MA2_CountryWiseConfiguration__c
                WHERE  MA2_ConfigurationType__c IN :setConfigtypr and MA2_CountryCode__c IN :setContcode 
            ]
        )
            mapConfig.put(string.valueof(setcombined), Conconfig.Id);
        //mapConfig.put(Conconfig.MA2_CountryCode__c, Conconfig.Id);
        
        for(MA2_CountryWiseConfiguration__c Conconfig: configList)
            if(
                mapConfig.containsKey(string.valueof(setcombined)) &&
                mapConfig.get(string.valueof(setcombined)) != Conconfig.Id
            )
            Conconfig.addError('There is already another Country Wise Configurations with the same Configuration Type and Country Code.');
    }
    /*
Method is created to check the count of colons in description and match it with count of commas in value and target field for 
specific tables(Records).
*/
    public static void mapConfig_validation(list<MA2_CountryWiseConfiguration__c> configList)
    {
        String descColonsCount,valueCommaCount,targetCommaCount;
        Integer colonCount=0,valueCount=0,targetCount=0;
        list<String> strList = new List<String>();
        Set<String> strList2 = new Set<String>();
        String temp ;
        if(configList.size()>0){
        for(MA2_CountryWiseConfiguration__c cwc : configList)
        { 
            if((cwc.MA2_Description__c != null && cwc.MA2_Value__c != null && cwc.MA2_Target__c != null)){
                temp  = cwc.MA2_Description__c;
                String[] arrTest = temp.split(';');
                for(String s :arrTest ){
                    strList.add(s.trim());
                    strList2.add(s.trim());
                    if(strList2.size() != strList.size()){
                    cwc.adderror('Description cannot be Same');
                    }
                }
                }
                if((cwc.MA2_Description__c != null || cwc.MA2_Value__c != null || cwc.MA2_Target__c != null) && (cwc.name == 'Dynamic Menu Consumer Chinese ' || cwc.name == 'Dynamic Menu Consumer English' || cwc.name == 'Dynamic Menu Pro English' || cwc.name == 'Dynamic Menu Pro Chinese')){
                    descColonsCount = cwc.MA2_Description__c ;
                    valueCommaCount = cwc.MA2_Value__c ; 
                    targetCommaCount = cwc.MA2_Target__c ;
                    colonCount = descColonsCount.countMatches(';');
                    valueCount = valueCommaCount.countMatches(',');
                    targetCount = targetCommaCount.countMatches(',');
                    if(colonCount != valueCount || colonCount != targetCount || valuecount != targetcount || valuecount != colonCount)
                    {
                        cwc.addError('Count should be same for Colon in Desccription, comma count in value and target fields');
                    }
                }}
        }
    }
}