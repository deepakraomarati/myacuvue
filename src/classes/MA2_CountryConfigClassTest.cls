/**
* File Name: MA2_CountryConfigClassTest
* Description : Test class for MA2_CountryConfigClass
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | neel.kamal@cognizant.com
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |10-Oct-2016  |nkamal8@its.jnj.com  |New Class created
*/
@isTest
public class MA2_CountryConfigClassTest {
    static Testmethod void createCountryConfigTest(){
        TestDataFactory_MyAcuvue.insertCustomSetting();
        List<MA2_CountryWiseConfiguration__c> cwc = new List<MA2_CountryWiseConfiguration__c>();
        for(Integer i=0;i<1;i++){
            MA2_CountryWiseConfiguration__c ob = new MA2_CountryWiseConfiguration__c();
            ob.Name = 'Dynamic Menu Consumer English';
            ob.MA2_ConfigurationType__c = 'Dynamic Menu Consumer English';
            ob.MA2_CountryCode__c = 'HKG';
            ob.MA2_DeviceType__c = 'IOS';
            ob.MA2_Target__c = 'ttsst,';
            ob.MA2_Value__c = 'okejh,';
            ob.MA2_Description__c = 'hkgg;';
            cwc.add(ob);
        }
        insert cwc;
        system.assertEquals(cwc[0].MA2_ConfigurationType__c,'Dynamic Menu Consumer English','success');
        Test.startTest();
        MA2_CountryConfigClass.mapConfig(cwc);
        Test.Stoptest();
    }
}