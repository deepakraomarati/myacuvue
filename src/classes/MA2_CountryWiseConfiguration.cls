/**
* File Name: MA2_CountryWiseConfiguration
* Description : Sending CountryWiseConfiguration to Apigee System
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |14-Oct-2016 |hsingh53@its.jnj.com  |New Class created
*/
public class MA2_CountryWiseConfiguration{
    
    public static void sendData(List<MA2_CountryWiseConfiguration__c> configurationList){
        list<MA2_CountryWiseConfiguration__c> configurList=[select lastmodifiedBy.Name,Id,MA2_ConfigurationType__c,Name,MA2_CountryCode__c,MA2_Value__c,MA2_Target__c,
         MA2_Description__c,MA2_DeviceType__c,CreatedDate,LastmodifiedDate from MA2_CountryWiseConfiguration__c where Id in: configurationList];
        final List<MA2_CountryWiseConfiguration__c> countryFilterList = new List<MA2_CountryWiseConfiguration__c>();    
        for(MA2_CountryWiseConfiguration__c con : configurList){
            
            String userName = con.lastmodifiedBy.Name;
            userName = userName.toLowercase();
            if(!userName.contains('web')){
                countryFilterList.add(con);        
            }
        }
        if(countryFilterList.size() != 0){
            sendApigeeData(countryFilterList);
        }
    }
    
    
    /*
     * Method for creating json body 
     */
     
    public static void sendApigeeData(List<MA2_CountryWiseConfiguration__c> configurationList){
    
    
    
    
        String jsonString  = '';
        
        if(configurationList.size() > 0){
            jsonString  =  '{"configDetails": [';
        system.debug('configurationList-->>>>>>>>>>-'+configurationList);
            JSONGenerator gen = JSON.createGenerator(true);           
            for(MA2_CountryWiseConfiguration__c config : configurationList){
              if(config.MA2_Value__c == null || config.MA2_Value__c == '')
              {
                  config.MA2_Value__c ='';
              }
              if(config.MA2_Target__c == null || config.MA2_Target__c == '')
              {
                  config.MA2_Target__c ='';
              }
              gen.writeStartObject();
              gen.writeStringField('configName',config.Name);
              gen.writeStringField('configId',config.id+'');
              gen.writeStringField('configType', config.MA2_ConfigurationType__c+'');
              gen.writeStringField('countryCode', config.MA2_CountryCode__c+'');
              gen.writeStringField('deviceType', config.MA2_DeviceType__c+'');
              gen.writeStringField('configValue', config.MA2_Value__c+''); 
              gen.writeStringField('configTarget', config.MA2_Target__c+'');
              gen.writeStringField('configDescription',config.MA2_Description__c+'');
              gen.writeStringField('created',config.CreatedDate+'');
              gen.writeStringField('lastModifiedDate',config.LastmodifiedDate+'');   
              gen.writeEndObject();
            }
            
            jsonString = jsonString + gen.getAsString()+']}';
        }
        System.Debug('jsonString--'+jsonString);
        if(jsonString != ''){
            sendtojtracker(jsonString);
        }
    }
    
    /*
     * Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONObject class
     */
    @future(callout = true)
    public static void sendtojtracker(String jsonBody) {
       JSONObject oauth = null;
       Credientials__c testPub = new Credientials__c();
       if(Credientials__c.getInstance('CountryWiseConfiguration') != null){
           testPub  = Credientials__c.getInstance('CountryWiseConfiguration');
           if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
                       final String ClientId = testPub.Client_Id__c;
                       final String ClientSecret = testPub.Client_Secret__c;
                       final String TargetUrl = testPub.Target_Url__c;
                       final String ApiKey = testPub.Api_Key__c; 
                       oauth = oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody);
           }
       }     
       /*SonarQube Fix*/	   
       //System.debug('------oauth response------>>>>'+oauth);
        
    }
    
    /*
     * Method for sending data to the Apigee system
     */
    private static JSONObject oauthLogin(String TargetUrl, String ClientId, String ClientSecret, String ApiKey ,String jsonBody) {
      HttpRequest loginRequest = New HttpRequest();
      loginRequest.setMethod('POST');
      loginRequest.setEndpoint(TargetUrl);
      loginRequest.setHeader('grant_type', 'authorization_code');
      loginRequest.setHeader('client_id',ClientId);
      loginRequest.setHeader('client_secret',ClientSecret);
      loginRequest.setHeader('apikey',ApiKey);
      loginRequest.setHeader('Content-Type', 'application/json');
     
      system.debug('<<<<jsonString>>>>>'+jsonBody);

      loginRequest.setBody(jsonBody);
      Http Http = New Http();
      HTTPResponse loginResponse = new HTTPResponse();
      if ( !Test.isRunningTest() ){
          loginResponse = http.send(loginRequest);
          JSONObject oAuth = (JSONObject) JSON.deserialize(loginResponse.getbody(), JSONObject.class);
          return oAuth;
      }
      
      System.Debug('loginResponse --'+loginResponse.getBody());
    
      return null;
 }
 
 // Inner class for setting value 
 public class JSONObject {
      public String id {
       get;
       set;
      }
      public String issued_at {
       get;
       set;
      }
      public String instance_url {
       get;
       set;
      }
      public String signature {
       get;
       set;
      }
      public String access_token {
       get;
       set;
      }
 }
 

}