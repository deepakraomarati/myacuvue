/**
* File Name: MA2_CountryWiseConfigurationTest
* Description : Test class for CountryWise Configuration
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |10-Oct-2016  |hsingh53@its.jnj.com  |New Class created
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    2.0  |30-Aug-2017  |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_CountryWiseConfigurationTest{
    
    /* Method for excuting the test class */
    static Testmethod void createCountryConfigTest(){
        TestDataFactory_MyAcuvue.insertCustomSetting();
        Test.startTest();
        TestDataFactory_MyAcuvue.createCountryConfig(1);
        system.assertEquals(TestDataFactory_MyAcuvue.createCountryConfig(1).size(), 1, 'Success');                              
        Test.stopTest();
    }
}