/**

* File Name: MA2_Coupon
* Description : class for assigning Coupons based on Country Code
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |25-Aug-2016  |hsingh53@its.jnj.com  |New Class created
*/

public class MA2_Coupon{
    
    //global variable
    public static Map<String,Contact> contactMap = new Map<String,Contact>();
    public static List<CampaignMember> campaignList = new List<CampaignMember>();
    public static List<Contact> contactWithRedList = new List<Contact>();
    
    /*
*before update
*/
    public static void beforeInsert(List<Contact> contactList){
        
       // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
        Id jjvproRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP Contacts').getRecordTypeId();
        
        for(Contact con : contactList){
          // Added this condition for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
            if(con.RecordTypeId != jjvproRecordTypeId){
                con.MA2_AssignCoupon__c = 'Changed';
            }
        }
    }
    
    /*
*before update
*/
    public static void beforeUpdate(List<Contact> contactList,List<Contact> oldContactList){
      // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
      Id jjvproRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP Contacts').getRecordTypeId();
         for(Contact con : contactList){
              // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
               if(con.RecordTypeId != jjvproRecordTypeId)
             {
                 for(Contact conOld : oldContactList){
                     if(con.MA2_PreAssessment__c != conOld.MA2_PreAssessment__c || con.MA2_Country_Code__c != conOld.MA2_Country_Code__c
                     || con.MA2_Contact_lenses__c != conOld.MA2_Contact_lenses__c || con.RecordTypeId != conOld.RecordTypeId ){
                         con.MA2_AssignCoupon__c = 'Changed';
                          System.Debug('con---'+con);
                     }else{
                         con.MA2_AssignCoupon__c = 'Not Changed';
                          System.Debug('con---'+con);
                     }
                 }
             }
         }
     }
    /*
* after update 
*/
    public static void afterUpdate(List<Contact> contactList){
     // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
      Id jjvproRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP Contacts').getRecordTypeId();
        final List<CouponContact__c> couponList = new List<CouponContact__c>();
        Map<Id,CouponContact__c>  couponContactMap = new Map<Id,CouponContact__c>();
        couponList.addAll(MA2_CouponContactSelector.couponContactList(contactList));
        System.Debug('couponList---'+couponList);
        for(CouponContact__c cc : couponList){
            if(cc.ContactId__r.MA2_Country_Code__c == 'CHN' && cc.CouponId__r.MA2_CountryCode__c == 'CHN'){
                couponContactMap.put(cc.ContactId__c,cc);
            }
        }
        List<Contact> conList = new List<Contact>();
        for(Contact con : contactList){
          // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
            if(con.MA2_AssignCoupon__c == 'Changed' && !couponContactMap.containsKey(con.Id) && con.RecordTypeId != jjvproRecordTypeId){
                conList.add(con);    
            }
        }
        if(conList.size() != 0){
            deleteCoupon(conList);
            checkCampaignRecord(conList);
        }
    }  
    
    /*
* after insert
*/
    public static void afterInsert(List<Contact> contactList){
        checkCampaignRecord(contactList);
    }
    
    /*
* delete Coupon based on Contact List
*/
    public static void deleteCoupon(List<Contact> contactList){
        List<CouponContact__c> couponContactList = [select Id from CouponContact__c where ContactId__c in: contactList];
        if(couponContactList.size() > 0){
            //delete couponContactList;
        }
    }
    
    /*
*checking the campaign Member record based on contact Id and voucher status
*/
    public static void checkCampaignRecord(List<Contact> contactList){
        List<String> contactNRICList = new List<String>();
        List<Contact> contactupdateList = new List<Contact>();
         // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
          Id jjvproRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP Contacts').getRecordTypeId();
        
        for(Contact con : contactList){
          // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
            if(con.NRIC__c != null && con.RecordTypeId != jjvproRecordTypeId){
                contactNRICList.add(con.NRIC__c);
            }
            if(con.MA2_Country_Code__c != 'SGP' && con.RecordTypeId != jjvproRecordTypeId){
                //contactWithoutSGPList.add(con);
            }
        }
        System.Debug('contactList---'+contactList);
        if(!contactList.isEmpty()){
            //campaignList = [select Voucher_Status__c,ContactId from CampaignMember where ContactId in: contactList];
            System.Debug('campaignList --'+campaignList);
            
            //if(campaignList.isEmpty()){
            for(Contact con : contactList){
                if(con.RecordTypeId != jjvproRecordTypeId){
                    contactupdateList.add(con);       
                }
            }    
            /* }else if(campaignList.size()>0){
for(Contact con : contactList){
for(CampaignMember cmpMem : campaignList){
if(con.Id == cmpMem.ContactId && cmpMem.Voucher_Status__c == 'Redeemed'){
contactWithRedList.add(con);    
}else if(con.Id == cmpMem.ContactId && cmpMem.Voucher_Status__c != 'Redeemed'){
contactupdateList.add(con);    
}
}
//contactupdateList.add(cmpMem.);    
}
} */
        }
        //System.Debug('contactWithSGPList--'+contactWithSGPList);
        //if(!contactWithSGPList.isEmpty()){
        //contactupdateList.addAll(contactWithSGPList);
        //  }
        //if(contactWithRedList.size() > 0){
        //    createCoupon(contactWithRedList);    
        // }
        System.Debug('contactupdateList--'+contactupdateList);
        if(contactupdateList.size() > 0){
            createCoupon(contactupdateList);
        }
    }
    
    
    /* 
* creating coupon Record based on contact list if the contact is not expired(condition created date should be less than 6 month)
*/
    public static void createCoupon(List<Contact> contactRecordList){
        Id consumerRecTypeId = [select Id from RecordType where sObjectType = 'Contact' and Name = 'Consumer'].Id;
        System.Debug('contactRecordList---'+contactRecordList);
        DateTime dt = System.Now();
        List<CouponContact__c> createCouponList = new List<CouponContact__c>();
        Map<String,List<Coupon__c>> couponContactList = new Map<String,List<Coupon__c>>();
        couponContactList.putAll(MA2_CouponSelector.couponContactList());
        System.Debug('couponContactList--'+couponContactList);
        Map<Id,CouponContact__c>  couponContactMap = new Map<Id,CouponContact__c>();
        //List<CouponContact__c> couponList = [select Id,CouponType_MyAcuvue__c,ContactId__c from CouponContact__c where CouponType_MyAcuvue__c =: 'ETrial' and ContactId__c in: contactRecordList];
        List<CouponContact__c> couponList = MA2_CouponContactSelector.couponContactList(contactRecordList);
        System.Debug('couponList---'+couponList);
        for(CouponContact__c cc : couponList){
            couponContactMap.put(cc.ContactId__c,cc);
        }
        System.Debug('couponContactMap---'+couponContactMap);
        final List<Coupon__c> couponETPreList = new List<Coupon__c>();
        if(couponContactList.containsKey('Etrial')){
            if(couponContactList.get('Etrial').size() > 0 ){
                couponETPreList.addAll(couponContactList.get('Etrial'));
            }
        }
        /*if(couponContactList.get('Etrial Premium').size() > 0 ){
couponETPreList.addAll(couponContactList.get('Etrial Premium'));
}*/
        if(couponContactList.containsKey('Bonus Multiplier')){
            if(couponContactList.get('Bonus Multiplier').size() > 0 ){
                couponETPreList.addAll(couponContactList.get('Bonus Multiplier'));
            }
        }
        if(couponContactList.containsKey('Cash Discount')){
            if(couponContactList.get('Cash Discount').size() > 0 ){
                couponETPreList.addAll(couponContactList.get('Cash Discount'));
            }
        }
        System.debug('couponETPreList---'+couponETPreList);
        for(Contact con : contactRecordList){
            System.Debug('con.MA2_PreAssessment__c--'+con.MA2_PreAssessment__c);
            System.Debug('couponContactMap--'+couponContactMap);
            System.Debug('con.Id--'+con.Id);
            System.Debug('con.RecordType.Name---'+con.RecordType.Name);
            System.Debug('con.MA2_Country_Code__c---'+con.MA2_Country_Code__c);
            System.Debug('con.MA2_PreAssessment__c---'+con.MA2_PreAssessment__c);
            System.Debug('con.MA2_AssignCoupon__c---'+con.MA2_AssignCoupon__c);
            System.Debug('con.RecordTypeId---'+con.RecordTypeId);
            System.Debug('consumerRecTypeId---'+consumerRecTypeId);
            System.Debug('con---'+con);
            if(con.MA2_PreAssessment__c && con.MA2_AssignCoupon__c == 'Changed' && con.RecordTypeId == consumerRecTypeId ){
                /*if(con.MA2_Country_Code__c == 'CHN'){
createCouponList.addAll(MA2_ChinaCoupon.assignCouponPreasseesmentChecked(con,couponETPreList));     
}else */
                System.Debug('con.MA2_Country_Code__c---'+con.MA2_Country_Code__c);
                if(con.MA2_Country_Code__c == 'HKG'){
                    createCouponList.addAll(MA2_HongKongCouponCS.assignCouponPreasseesmentChecked(con,couponETPreList));     
                }else if(con.MA2_Country_Code__c == 'SGP'){
                    createCouponList.addAll(MA2_SingaporeCouponCS.assignCouponPreasseesmentChecked(con,couponETPreList));
                }else if(con.MA2_Country_Code__c == 'TWN'){
                    createCouponList.addAll(MA2_TWNCoupon.assignCouponPreasseesmentChecked(con,couponETPreList));
                }       
            }else if(!con.MA2_PreAssessment__c && !couponContactMap.containsKey(con.Id) && con.RecordTypeId == consumerRecTypeId){
                /*if(con.MA2_Country_Code__c == 'CHN'){
createCouponList.addAll(MA2_ChinaCoupon.assignCouponPreasseesmentNoChecked(con,couponETPreList));     
}else
if(con.MA2_Country_Code__c == 'HKG'){
createCouponList.addAll(MA2_HongKongCoupon.assignCouponPreasseesmentNoChecked(con,couponETPreList));     
} */ 
            }
            if(con.MA2_Country_Code__c == 'CHN' && !couponContactMap.containsKey(con.Id)){
                createCouponList.addAll(MA2_ChinaCoupon.assignCouponPreasseesmentChecked(con,couponETPreList));     
            }
        }
        System.Debug('createCouponList---'+createCouponList);
        if(createCouponList.size()>0){
            System.Debug('createCouponList check---'+createCouponList);
            insert createCouponList;
        }
    }
}