/**
* File Name: CouponContactSelector__c
* Description : Selector class for CouponContact object
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |25-Aug-2016  |hsingh53@its.jnj.com  |New Class created
*/

public without sharing class MA2_CouponContactSelector{
    /*
     *getting couponcontact list having coupon type as etrial and created contactId
     */
    public static List<CouponContact__c> couponContactList(List<Contact> contactRecordList){
        System.Debug('contactRecordListHarsh---'+contactRecordList);
        return [select Id,CouponType_MyAcuvue__c,ContactId__c,
                ContactId__r.MA2_Country_Code__c,CouponId__r.MA2_CountryCode__c
                 from CouponContact__c 
                where (CouponId__r.RecordType.Name =: 'Etrial')and ContactId__c in: contactRecordList];
    }
}