/**
* File Name: MA2_CouponImagesList 
* Description : class for showing image list for the current Coupon
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |23-Sep-2016 |hsingh53@its.jnj.com  |New Class created
*/
public class MA2_CouponImagesList {
    
    //Global Variables
    public Id couponId{get; set;}
    public List<WrapperClass> wrapperList{get; set;}
    public Set<Id> couponImgIdList{get; set;}
    
    //Inner class for setting and getting all the file info along with file Type
    public class WrapperClass{
        public Id fileId{get; set;}
        public String fileName{get; set;}
        public Id couponImageId{get; set;}
        public String couponType{get; set;}
        public Boolean status{get;set;}
        public String createdDate{get; set;} 
        public String createdBy{get; set;}
        public Id createdById{get; set;}
        public WrapperClass(Id fileId,String fileName,Id couponImageId,String couponType,Boolean status,String createdDate,String createdBy,Id createdById){
            this.fileId = fileId;
            this.fileName = fileName;
            this.couponType = couponType;
            this.status = status;
            this.createdDate = createdDate;
            this.createdBy = createdBy;
            this.createdById = createdById;
            this.couponImageId = couponImageId;
        }
    }
    
    /*
     * Constructor for querying all the image files and display on page
     */
    public MA2_CouponImagesList(ApexPages.StandardController controller) {
        wrapperList = new List<WrapperClass>();
        couponId = ApexPages.currentPage().getParameters().get('id');
        couponImgIdList = new Set<Id>();
        
        List<MA2_CouponImage__c> couponImageList = [select Id,MA2_Coupon_Type__c,MA2_Status__c,MA2_Coupon__c from MA2_CouponImage__c where MA2_Coupon__c =: couponId];
        for(MA2_CouponImage__c ci : couponImageList){
            couponImgIdList.add(ci.Id);    
        }
        List<Attachment> attachmentList = [select Name , ParentId , createdDate , createdBy.Name , CreatedById from Attachment where ParentId in: couponImgIdList];
        for(Attachment attach : attachmentList){
            for(MA2_CouponImage__c couponImage : couponImageList){    
                if(couponImage.Id == attach.ParentId){
                    wrapperList.add(new WrapperClass(attach.Id,attach.Name,couponImage.Id,couponImage.MA2_Coupon_Type__c,couponImage.MA2_Status__c,attach.createdDate.format(),attach.createdBy.Name,attach.CreatedById));    
                }
            }    
        }
        System.Debug('wrapperList---'+wrapperList);
    }
    
    public List<WrapperClass> getAttachmentList(){
        
        return wrapperList;
    }
    
     
    /*
     * Method for editing file Information
     */
    public PageReference editFile(){
        PageReference page = null;
        if(ApexPages.currentPage().getParameters().get('fileId') != null){
            page = new PageReference('/'+ApexPages.currentPage().getParameters().get('fileId')+'/e?retURL=%2F'+couponId);    
            page.setRedirect(true);
            return page;
        }
        return null;
    }
    
    /*
     *Method for redirecting to the uploading of new file for current coupon 
     */
    public PageReference newRecord(){
        return new PageReference('/apex/MA2_CouponTypeImages?id='+couponId);
    }
    
    /*
     * Method for showing file Information for current coupon
     */
    public PageReference viewRecord(){
        if(ApexPages.currentPage().getParameters().get('recordId') != null){
            return new PageReference('/'+ApexPages.currentPage().getParameters().get('recordId'));
        }
        return null;
    }
    
    /*
     * Method for deleting filefor current coupon
     */
    public PageReference deleteRecord(){
        if(ApexPages.currentPage().getParameters().get('imageId') != null){
            MA2_CouponImage__c attachRecord = new MA2_CouponImage__c();
            attachRecord.Id = ApexPages.currentPage().getParameters().get('imageId');
            delete attachRecord;
            return new PageReference('/'+couponId); 
        }
        return null;    
    }
}