/**
* File Name: MA2_CouponImagesListTest
* Description : Test class for MA2_CouponImagesList
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |8-Sep-2016  |hsingh53@its.jnj.com  |New Class created
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    2.0  |30-Aug-2017  |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_CouponImagesListTest{
    
    /* Method for excuting the test class */
    static Testmethod void createCouponImgTest(){
        
        TestDataFactory_MyAcuvue.insertCustomSetting();
        final List<Coupon__c> couponList = new List<Coupon__c>();
        final List<MA2_CouponImage__c> couponImgList = new List<MA2_CouponImage__c>();
        
        couponList.addAll(TestDataFactory_MyAcuvue.createCoupon(1));
        system.assertEquals(couponList.size(), 18, 'Success');                              
        couponImgList.addAll(TestDataFactory_MyAcuvue.createCouponImage(1,couponList));
        TestDataFactory_MyAcuvue.insertAttachment(couponImgList);
        Test.startTest();
        
        final Coupon__c couponRecord = new Coupon__c();
        couponRecord.Id = couponList[0].Id;
        couponRecord.MA2_CouponValidity__c = 40;
        update couponRecord; 
        
        final ApexPages.StandardController stdCon = new ApexPages.StandardController(couponList[0]);
        ApexPages.currentPage().getParameters().put('id',couponList[0].Id);
        ApexPages.currentPage().getParameters().put('recordId',couponImgList[0].Id);
        final MA2_CouponImagesList couponImageList = new MA2_CouponImagesList(stdCon);
        couponImageList.editFile();
        couponImageList.newRecord();
        couponImageList.viewRecord();
        couponImageList.deleteRecord();
        Test.stopTest();
    }
}