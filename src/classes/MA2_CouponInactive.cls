public class MA2_CouponInactive{
    public static void updateCouponWallet(List<CouponContact__c> couponContactList){
        List<CouponContact__c> productCouponList = [select CouponId__r.MA2_ProductName__c from CouponContact__c where Id in: couponContactList];
        List<CouponContact__c> deactiveCouponList = new List<CouponContact__c>();
        Set<Id> contactId = new Set<Id>();
        Set<String> productId = new Set<String>();
        for(CouponContact__c con : couponContactList){
            if(con.MA2_CouponUsed__c && !con.MA2_Inactive__c){
                contactId.add(con.ContactId__c);
            }
        }
    
        List<CouponContact__c> couponConList = new List<CouponContact__c>();
        System.Debug('contactId--'+contactId);
        Set<String> couponListone = new Set<String>();
        Map<String,MA2_InactiveCouponExclusion__c> cpnListt = MA2_InactiveCouponExclusion__c.getall();
        for(MA2_InactiveCouponExclusion__c hkc : cpnListt.values()){
            couponListone.add(hkc.name);
        }
        System.Debug('couponListone'+couponListone);
        couponConList.addAll([select Id,CouponId__r.Name,CouponId__r.MA2_CouponName__c,MA2_CouponUsed__c,ContactId__r.MA2_Country_Code__c,MA2_Inactive__c,
                                                     CouponId__r.RecordType.Name
                                                    from CouponContact__c where ContactId__c in: contactId
                                                    and ContactId__r.MA2_Country_Code__c = 'HKG' 
                                                    and CouponId__r.MA2_AvailableInCatalog__c = false
                                                     and (CouponId__r.MA2_CouponName__c NOT in : couponListone)]);
        
        
        System.Debug('couponConList --'+couponConList);
        Map<String,List<CouponContact__c>> couponMap = new map<String,List<CouponContact__c>>();
        if(couponConList.size() > 0){
            for(CouponContact__c con : couponConList){
                if(couponMap.containsKey(con.CouponId__r.RecordType.Name)){
                    couponMap.get(con.CouponId__r.RecordType.Name).add(con);
                }else{
                    couponMap.put(con.CouponId__r.RecordType.Name,new List<CouponContact__c>{ con });
                }    
            }
        
            Integer count = 0;
            System.Debug('couponMap--'+couponMap);
            for(String con : couponMap.keySet()){
                if(couponMap.containsKey(con)){
                    count = 0;
                    System.Debug('con--'+con);
                    List<CouponContact__c> couConLsit = couponMap.get(con);
                    for(CouponContact__c couRec : couConLsit){
                        System.Debug('couRec.MA2_CouponUsed__c--'+couRec.MA2_CouponUsed__c);
                        System.Debug('couRec.Id--'+couRec.Id);
                        if(couRec.MA2_CouponUsed__c){
                            count++;
                        }     
                    }
                    System.Debug('count--'+count);
                    if(count > 0){
                        for(CouponContact__c couRec : couConLsit){
                            if(!couRec.MA2_CouponUsed__c){
                                couRec.MA2_Inactive__c = true; 
                                deactiveCouponList.add(couRec);
                             }
                        }
                    }
                }     
            }
        }
        System.Debug('deactiveCouponList--'+deactiveCouponList);
        if(deactiveCouponList.size() > 0){
            update deactiveCouponList;
        }
    }
}