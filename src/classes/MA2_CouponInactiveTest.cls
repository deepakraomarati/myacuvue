@isTest
public class MA2_CouponInactiveTest{
    static Testmethod void inactiveCouponWallet(){
        
        TestDataFactory_MyAcuvue.createCoupon(1);
        Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id;
        Id caseDiscRecTypeId1 = [select Id from RecordType where Name = 'Cash Discount' and sObjectType = 'Coupon__c'].Id;
        final List<CouponContact__c> deactiveCouponList = new List<CouponContact__c>();
        Final TriggerHandler__c record = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                               MA2_createContact__c = true);
        
        insert record;
        final MA2_InactiveCouponExclusion__c records = new MA2_InactiveCouponExclusion__c(Name = 'ABC');
        Insert records;  
        
        final MA2_Country_Currency_Map__c record2 = new MA2_Country_Currency_Map__c(Name='HKG',Currency__c='HKD');
        insert record2;
        
        final Contact contactRecord = new Contact(LastName = 'test' ,MA2_ContactId__c = '23456',RecordTypeId = consumerRecTypeId,
                                                  email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'HKG',MA2_Contact_lenses__c='No',CurrencyIsoCode = 'HKD',
                                                  DOB__c  = System.Today(),
                                                  MA2_PreAssessment__c = true);
        
        insert contactRecord;
         system.assertEquals(contactRecord.LastName,'test','success');
        final List<Coupon__c> coupon = new List<Coupon__c>(); 
        coupon.add(new Coupon__c(MA2_CouponName__c = 'Test',RecordTypeID = caseDiscRecTypeId1 ,MA2_CountryCode__c = 'HKG'
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
        coupon.add(new Coupon__c(MA2_CouponName__c = 'Test2',RecordTypeID = caseDiscRecTypeId1 ,MA2_CountryCode__c = 'HKG'
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
        
        insert coupon;
       
        Final List<CouponCOntact__c> cpncn = new List<CouponContact__c>();
        cpncn.add(new CouponCOntact__c(ContactId__c =contactRecord.id,CouponId__c = coupon[0].id,ExpiryDate__c = System.Today()));
        cpncn.add(new CouponCOntact__c(ContactId__c =contactRecord.id,CouponId__c = coupon[1].id,ExpiryDate__c = System.Today()));
        insert cpncn;
        deactiveCouponList.addAll([select Id , MA2_CouponUsed__c from CouponContact__c where ContactId__c =: contactRecord.Id]);
        
        CouponContact__c couponContact = deactiveCouponList[0];
        couponContact.MA2_CouponUsed__c = true;
        update couponContact;        
    }
}