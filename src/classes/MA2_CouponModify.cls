@isTest
public class MA2_CouponModify{
    static Testmethod void couponImgTest(){
        
        Id etrialRecordTypeId = [select Id from RecordType where Name = 'Etrial' and sObjectType = 'Coupon__c'].Id;
        final List<Coupon__c> coupon = new List<Coupon__c>();
        coupon.add(new Coupon__c(MA2_CouponName__c = 'Etrial Multifocal',RecordTypeID = etrialRecordTypeId ,MA2_CountryCode__c ='HKG'
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
        
        insert coupon;
        
        MA2_CouponImage__c couponImg = new MA2_CouponImage__c();
        couponImg.MA2_Coupon_Type__c = 'Coupon Banner';
        couponImg.MA2_Coupon__c = coupon[0].Id;
        
        insert couponImg;
        system.assertEquals(couponImg.MA2_Coupon_Type__c,'Coupon Banner','success');
    }
}