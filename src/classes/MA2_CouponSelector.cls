/**
* File Name: MA2_CouponSelector
* Description : Selector class for Coupon object
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |29-Aug-2016  |hsingh53@its.jnj.com  |New Class created
*/

public without sharing class MA2_CouponSelector{
    
    /*
     * getting coupon map having coupon type as etrial or Etrial Premium
     */
    public static Map<String,List<Coupon__c>> couponContactList(){
        Map<String,List<Coupon__c>> couponMap = new Map<String,List<Coupon__c>>();
        List<Coupon__c> couponList = [select Id,Name,RecordType.Name,MA2_CountryCode__c,MA2_CouponName__c,
                                        MA2_TimeToAward__c  ,MA2_CouponValidity__c,MA2_WelcomeCoupon__c  
                                        from Coupon__c where RecordType.Name = 'Etrial' or RecordType.Name = 'Etrial Premium'
                                     or RecordType.Name = 'Bonus Multiplier' or RecordType.Name = 'Cash Discount'];
        for(Coupon__c cp : couponList){
            if(couponMap.containsKey(cp.RecordType.Name)){
                couponMap.get(cp.RecordType.Name).add(cp);
            }else{
                couponMap.put(cp.RecordType.Name,new List<Coupon__c>{cp});
            }
                
        }
        return couponMap; 
    }
    
   /*
    *Method for fetching coupon having coupon Type Bonus Multiplier/Cash Discount with Diminishing Value
    * and Bonus Multiplier
    */
    public static List<Coupon__c> couponTypeList(Set<String> countDownRateList , 
                                                    Map<String,List<CouponContact__c>> coupondiminishMap){
        return [select Id,RecordType.Name,CountdownRate__c,MA2_BonusMultiplier__c,MA2_CouponValidity__c ,
                    MA2_CountryCode__c,DiscountPrice__c from Coupon__c 
                    where MA2_CountryCode__c in: coupondiminishMap.keySet()
                    and (RecordType.Name = 'Bonus Multiplier with Diminishing Value' or 
                    RecordType.Name = 'Cash Discount with Diminishing Value' or RecordType.Name = 'Bonus Multiplier')
                    and CountdownRate__c in: countDownRateList order by DiscountPrice__c desc];
    }
}