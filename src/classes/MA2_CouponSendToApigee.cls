/**
* File Name: MA2_CouponSendToApigee
* Description : class for sending Coupon Info. to Apigee System
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |23-Sep-2016 |hsingh53@its.jnj.com  |New Class created
*/
public class MA2_CouponSendToApigee{
    
    /*
     * Method for creating json body 
     */
    public static void sendData(List<Attachment> attachmentList){
        Set<Id> couponImgIdList = new Set<Id>();
        Set<Id> couponIdList = new Set<Id>();
        for(Attachment att : attachmentList){
                couponImgIdList.add(att.ParentId);
        }
        String jsonString  = '{"coupons":[';
        final String imageURL = '/services/data/v37.0/sobjects/Attachment/';
        List<MA2_CouponImage__c> couponImageList = [select Id , MA2_Coupon__c,MA2_Coupon_Type__c,MA2_Status__c,createdDate,lastModifiedDate
                                                     from MA2_CouponImage__c where Id in: couponImgIdList];
      for(MA2_CouponImage__c couponImg : couponImageList){
          couponIdList.add(couponImg.MA2_Coupon__c);    
      }
      List<Coupon__c> couponList = [select Id,Name,CouponDescription__c,
                                                     RecordType.Name,createdDate,lastModifiedDate,
                                                     MA2_CouponName__c,
                                                     DiscountRate__c,EndDate__c,
                                                     DiscountMethod__c,
                                                     Price__c,MA2_CountryCode__c,
                                                     StartDate__c,StatusYN__c,MA2_ClubOtherProducts__c,
                                                     MA2_Quantity__c,MA2_BonusMultiplier__c,
                                                     MA2_EquivalentPoints__c,MA2_PointsCap__c,MA2_Product__c,
                                                     MA2_AvailableInCatalog__c,MA2_Product__r.UPC_Code__c,MA2_BrandCode__c,
                                                     MA2_Product__r.Name,RecordTypeId,MA2_CouponValidity__c
                                                     from Coupon__c where Id in: couponIdList limit 100];
    //    List<Attachment> imageFileList = [select Id,
     //                                       ParentId,Name,BodyLength from Attachment where ParentId in: couponImageList];
        
        if(attachmentList.size() > 0){
             for(Coupon__c couponImg : couponList){
                jsonString = jsonString  + '{\n';
                jsonString = jsonString  + '"couponCode":"'+couponImg.Id+'",\n';
                jsonString = jsonString  + '"couponDescription":"'+couponImg.CouponDescription__c+'",\n';
                jsonString = jsonString  + '"couponName":"'+couponImg.MA2_CouponName__c+'",\n';
                jsonString = jsonString  + '"couponType":"'+couponImg.RecordTypeId+'",\n';
                jsonString = jsonString  + '"recordTypeName":"'+couponImg.RecordType.Name+'",\n';
                jsonString = jsonString  + '"createdDate":"'+couponImg.createdDate+'",\n';
                jsonString = jsonString  + '"lastModifiedDate":"'+couponImg.lastModifiedDate+'",\n'; 
                jsonString = jsonString  + '"discountMethod":"'+couponImg.DiscountMethod__c+'",\n';
                jsonString = jsonString  + '"discountRate":"'+couponImg.DiscountRate__c+'",\n';
                jsonString = jsonString  + '"endDate":"'+couponImg.EndDate__c+'",\n';
                jsonString = jsonString  +  ' "imageDetails":[';
                for(MA2_CouponImage__c imgRecord : couponImageList){                    
                    //jsonString = jsonString  + '"imageName":"'+file.Name+'",\n';
                    if(imgRecord.MA2_Coupon__c == couponImg.Id){
                        for(Attachment file : attachmentList){
                            if(file.ParentId == imgRecord.Id){
                                jsonString = jsonString  + '{\n';
                                jsonString = jsonString  + '"couponImageId":"'+imgRecord.Id+'",\n';
                                jsonString = jsonString  + '"couponType":"'+imgRecord.MA2_Coupon_Type__c+'",\n';
                                jsonString = jsonString  + '"status":'+imgRecord.MA2_Status__c+',\n';
                                jsonString = jsonString  + '"createdDate":"'+imgRecord.createdDate+'",\n';
                                jsonString = jsonString  + '"lastModifiedDate":"'+imgRecord.lastModifiedDate+'",\n';
                               // jsonString = jsonString  + '"imageName":"'+file.Name+'",\n';
                               // jsonString = jsonString  + '"imageSize":"'+file.BodyLength+'",\n';
                               // jsonString = jsonString  + '"body":"'+imageURL+file.Id+'/Body'+'?file='+'file.Id+"\n';
                                jsonString = jsonString  + '"body":"'+imageURL+file.Id+'/Body'+'?file='+file.Id+'"\n';
                                jsonString = jsonString  + '},';
                            }
                        }
                    }
                }
                jsonString = jsonString.subString(0,jsonString.Length() - 1)+'\n';
                jsonString = jsonString  + '],\n';
                jsonString = jsonString  + '"price":"'+couponImg.Price__c+'",\n';
                jsonString = jsonString  + '"regionCode":"'+couponImg.MA2_CountryCode__c+'",\n';
                jsonString = jsonString  + '"startDate":"'+couponImg.StartDate__c+'",\n';
               // jsonString = jsonString  + '"regionCode":"'+couponImg.MA2_CountryCode__c+'",\n';
                if(couponImg.StatusYN__c){
                     jsonString = jsonString  + '"status":"Yes",\n';
                }else{
                     jsonString = jsonString  + '"status":"No",\n';
                }
                jsonString = jsonString  + '"quantity":"'+couponImg.MA2_Quantity__c+'",\n';
                jsonString = jsonString  + '"bonusMultiplier":"'+couponImg.MA2_BonusMultiplier__c+'",\n';
                jsonString = jsonString  + '"equivalentPoints":"'+couponImg.MA2_EquivalentPoints__c+'",\n';
                jsonString = jsonString  + '"clubOtherProducts":'+couponImg.MA2_ClubOtherProducts__c+',\n';
                jsonString = jsonString  + '"fixedBonusPoints":'+couponImg.MA2_PointsCap__c+',\n';
                jsonString = jsonString  + '"freeProduct":"'+couponImg.MA2_Product__c+'",\n';
                jsonString = jsonString  + '"productName":"'+couponImg.MA2_Product__r.Name+'",\n';
                jsonString = jsonString  + '"upcCode":"'+couponImg.MA2_Product__r.UPC_Code__c+'",\n';
                jsonString = jsonString  + '"brandCode":"'+couponImg.MA2_BrandCode__c+'",\n';
                jsonString = jsonString  + '"status":true,\n';
                jsonString = jsonString  + '"couponValidity":"'+couponImg.MA2_CouponValidity__c+'"\n'; 
                jsonString = jsonString  + '},'; 
            }
            jsonString = jsonString.subString(0,jsonString.Length() - 1)+'\n';
            //gen.writeEndObject();
            jsonString = jsonString  + ']}';
            //jsonString = jsonString + gen.getAsString() + ']}';
        }
        System.Debug('jsonString--'+jsonString);
        if(jsonString != ''){
            sendtojtracker(jsonString);
        }
    }
    
    /*
     * Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONObject class
     */
    @future(callout = true)
    public static void sendtojtracker(String jsonBody) {
       
      JSONObject oauth = null;
      Credientials__c testPub = new Credientials__c();
       if(Credientials__c.getInstance('CouponSendToApigee') != null){
           testPub  = Credientials__c.getInstance('CouponSendToApigee');
           if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
                       final String ClientId = testPub.Client_Id__c;
                       final String ClientSecret = testPub.Client_Secret__c;
                       final String TargetUrl = testPub.Target_Url__c;
                       final String ApiKey = testPub.Api_Key__c; 
                       oauth = oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody);
           }
       }     
       /*SonarQube Fix*/	   
       //System.debug('------oauth response------>>>>'+oauth);
        
    }
    
    /*
     * Method for sending data to the Apigee system
     */
    private static JSONObject oauthLogin(String targetUrl, String clientId, String clientSecret, String apiKeyValue,String jsonBody) {
      HttpRequest loginRequest = New HttpRequest();
      loginRequest.setMethod('POST');
      loginRequest.setTimeout(120000);
      loginRequest.setEndpoint(targetUrl);
      loginRequest.setHeader('grant_type', 'authorization_code');
      loginRequest.setHeader('client_id',clientId);
      loginRequest.setHeader('client_secret',clientSecret);
      loginRequest.setHeader('apikey',apiKeyValue);
      loginRequest.setHeader('Content-Type', 'application/json');
     
      system.debug('<<<<jsonString>>>>>'+jsonBody);

      loginRequest.setBody(jsonBody);
      Http Http = New Http();
      HTTPResponse loginResponse = new HTTPResponse();
      if ( !Test.isRunningTest() ){
          loginResponse = http.send(loginRequest);
          JSONObject oAuth = (JSONObject) JSON.deserialize(loginResponse.getbody(), JSONObject.class);
          return oAuth;
      }
      
      System.Debug('loginResponse --'+loginResponse.getBody());
    
      return null;
 }
 
 // Inner class for setting value 
 public class JSONObject {
      public String id {
       get;
       set;
      }
      public String issued_at {
       get;
       set;
      }
      public String instance_url {
       get;
       set;
      }
      public String signature {
       get;
       set;
      }
      public String access_token {
       get;
       set;
      }
 }
 

}