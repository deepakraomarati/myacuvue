/**
* File Name: MA2_CouponTest
* Description : Test class for Coupon
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* Test Class for MA2_Coupon(Coupon Object)
*
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |8-Sep-2016  |hsingh53@its.jnj.com  |New Class created
*=============================================================== 
*    Ver  |Date        |Author                |Modification
*    2.0  |8-Sep-2016  |nkamal8@its.jnj.com  |existing class modified
*================================================================
*/
@isTest
public class MA2_CouponTest{
    
    /* Method for excuting the test class */
    static Testmethod void runBatchJobMethod(){
        /* Custom Setting Validation for test class */
        Final TriggerHandler__c record = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                         MA2_CouponToApigeeTrigger__c = true);
        insert record;
        String countryname_SG = 'SGP';
        String countryname_HK = 'HKG';
        String postassesment = 'Post-Assessment';
        final Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id;
        Final MA2_Country_Currency_Map__c countryCode = new MA2_Country_Currency_Map__c(Name = 'SGP' , Currency__c = 'SGD');
        insert countryCode;
        
        Final MA2_SGCouponList__c cpnlst = new MA2_SGCouponList__c(Name = 'Test3');
        insert cpnlst;
        
        Final MA2_HKCouponList__c cpnlst1 = new MA2_HKCouponList__c(Name = 'Test');
        insert cpnlst1;
        Final MA2_HKCouponsList__c cpnlst2 = new MA2_HKCouponsList__c(Name = 'Test2');
        insert cpnlst2; 

        TestDataFactory_MyAcuvue.insertCustomSetting();
        List<Contact> conList = new List<Contact>();
        Test.startTest();
        Id etrialRecordTypeId = [select Id from RecordType where Name = 'Etrial' and sObjectType = 'Coupon__c'].Id;
        Id caseDiscRecTypeId = [select Id from RecordType where Name = 'Bonus Multiplier' 
                                and sObjectType = 'Coupon__c'].Id;
        Id caseDiscRecTypeId1 = [select Id from RecordType where Name = 'Cash Discount' 
                                 and sObjectType = 'Coupon__c'].Id;
        
                                 
        /* Test Record Creation */
        final List<Coupon__c> coupon = new List<Coupon__c>();        
        coupon.add(new Coupon__c(MA2_CouponName__c = 'Test',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c = countryname_HK
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = postassesment));
        coupon.add(new Coupon__c(MA2_CouponName__c = 'Test' ,RecordTypeID = caseDiscRecTypeId1 ,MA2_CountryCode__c = countryname_HK
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = postassesment));
        coupon.add(new Coupon__c(MA2_CouponName__c = 'Test2',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c = countryname_HK
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = postassesment));
        coupon.add(new Coupon__c(MA2_CouponName__c = 'Test2' ,RecordTypeID = caseDiscRecTypeId1 ,MA2_CountryCode__c = countryname_HK
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = postassesment));
        coupon.add(new Coupon__c(MA2_CouponName__c = 'Test3',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c = countryname_SG
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = postassesment, MA2_WelcomeCoupon__c = true, StatusYN__c = true));
        coupon.add(new Coupon__c(MA2_CouponName__c = 'Test3' ,RecordTypeID = caseDiscRecTypeId1 ,MA2_CountryCode__c = countryname_SG
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = postassesment, MA2_WelcomeCoupon__c = true, StatusYN__c = true));
        
        
        coupon.add(new Coupon__c(MA2_CouponName__c = '$30 Voucher(All Brand)',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c = countryname_SG
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = postassesment));
        
        coupon.add(new Coupon__c(MA2_CouponName__c = '$20 Oasys 1 Day Series Reward',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c =countryname_SG
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = postassesment));
        
        coupon.add(new Coupon__c(MA2_CouponName__c = 'Moist for Astigmatism',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c =countryname_SG
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = postassesment));
        
        coupon.add(new Coupon__c(MA2_CouponName__c = '2000 Bonus Points (Fixed Points)',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c =countryname_SG
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Pre-Assessment'));
        
        coupon.add(new Coupon__c(MA2_CouponName__c = '$20 Oasys 1 Day Series Reward',RecordTypeID = caseDiscRecTypeId1 ,MA2_CountryCode__c =countryname_SG
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = postassesment));
        
        coupon.add(new Coupon__c(MA2_CouponName__c = '$30 Welcome Reward',RecordTypeID = caseDiscRecTypeId1 ,MA2_CountryCode__c =countryname_SG
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = postassesment));
        
        coupon.add(new Coupon__c(MA2_CouponName__c = '50% bonus points (DEFINE)',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c =countryname_SG
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Pre-Assessment'));
        
        coupon.add(new Coupon__c(MA2_CouponName__c = '20% bonus points (ANY ACUVUE)',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c =countryname_SG
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Pre-Assessment'));
        
        coupon.add(new Coupon__c(MA2_CouponName__c = '2X Bonus Points (Define)',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c =countryname_SG
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));

        insert  coupon;
       
       
        final Account acc = new Account(Name = 'Test' , MA2_AccountId__c = '978394' ,AccountNumber = '54321',
                                        OutletNumber__c = '54321',ActiveYN__c = true,
                                        My_Acuvue__c = 'Yes',CountryCode__c = countryname_SG ,PublicZone__c = 'Testtt');
        
        insert acc;
        final Account accc = new Account(Name = 'Test' , MA2_AccountId__c = '9783945' ,AccountNumber = '54322',
                                        OutletNumber__c = '54322',ActiveYN__c = true,
                                        My_Acuvue__c = 'Yes',CountryCode__c = countryname_SG ,PublicZone__c = 'Testtt');
        
        insert accc;
        final Contact con1 = new Contact(LastName = 'test1' ,MA2_AccountId__c = '978394' ,MA2_ContactId__c = '23456',
                                         RecordTypeId  = consumerRecTypeId  ,
                                         AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = 'HKD', 
                                         MembershipNo__c = 'SGP1235', MA2_Country_Code__c = countryname_SG, MA2_Grade__c='Base', DOB__c = date.newinstance(2012,03,03),
                                         MA2_PreAssessment__c = true,MA2_Contact_lenses__c = 'Acuvue Brand');
        insert con1;
        final Contact con2 = new Contact(LastName = 'test2' ,MA2_AccountId__c = '978394' ,MA2_ContactId__c = '23456',
                                         RecordTypeId  = consumerRecTypeId  ,
                                         AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = 'HKD', 
                                         MembershipNo__c = 'SGP12345', MA2_Country_Code__c = countryname_SG, MA2_Grade__c='Base', DOB__c = date.newinstance(2012,03,03),
                                         MA2_PreAssessment__c = true,MA2_Contact_lenses__c = 'No');
        insert con2;
        final Contact conn = new Contact(LastName = 'test2' ,MA2_AccountId__c = '978394' ,MA2_ContactId__c = '23456',
                                         RecordTypeId  = consumerRecTypeId  ,
                                         AccountId = accc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = 'SGD', 
                                         MembershipNo__c = 'SGP12345', MA2_Country_Code__c = countryname_SG, MA2_Grade__c='Base', DOB__c = date.newinstance(2012,03,03),
                                         MA2_PreAssessment__c = true,MA2_Contact_lenses__c = 'No');
        insert conn;
        final Contact con_1 = new Contact(LastName = 'test2' ,MA2_AccountId__c = '978394' ,MA2_ContactId__c = '23456',
                                         RecordTypeId  = consumerRecTypeId  ,
                                         AccountId = accc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = 'SGD', 
                                         MembershipNo__c = 'SGP12345', MA2_Country_Code__c = countryname_SG, MA2_Grade__c='Base', DOB__c = date.newinstance(2012,03,03),
                                         MA2_PreAssessment__c = true,MA2_Contact_lenses__c = 'Acuvue Brand');
        insert con_1;
        final Contact con_2 = new Contact(LastName = 'test2' ,MA2_AccountId__c = '978394' ,MA2_ContactId__c = '23456',
                                         RecordTypeId  = consumerRecTypeId  ,
                                         AccountId = accc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = 'SGD', 
                                         MembershipNo__c = 'SGP12345', MA2_Country_Code__c = countryname_SG, MA2_Grade__c='Base', DOB__c = date.newinstance(2012,03,03),
                                         MA2_PreAssessment__c = true,MA2_Contact_lenses__c = 'Other Brand');
        insert con_2;
        
        
        
        TestDataFactory_MyAcuvue.createCoupon(1);
        //conList.addAll(TestDataFactory_MyAcuvue.createContact(1,TestDataFactory_MyAcuvue.createAccount(1)));
        Contact con = new Contact();
        con.Id = conn.Id;
        con.MA2_PreAssessment__c  = false;
        update con;
        Test.stopTest();
        //System.Assert Check 
        System.assertequals(con.MA2_PreAssessment__c,false,'Success');
		MA2_CouponUpdateService.sendData(coupon);
    }
}