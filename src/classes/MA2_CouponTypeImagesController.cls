/**
* File Name: MA2_CouponTypeImagesController 
* Description : class for inserting coupon images for the curent coupon
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |10-Oct-2016 |hsingh53@its.jnj.com  |New Class created
*/
public class MA2_CouponTypeImagesController {
    
    //Global Variables
    public String couponType{get; set;}
    public Blob uploadFile{get; set;}
    public Id couponId{get; set;}
    public MA2_CouponImage__c couponImageRecord{get; set;}
    public String fileName{get; set;}
    public Attachment attachFile{get; set;}
    public Integer count1{get; set;}
    public String fileType{get; set;}
    
    //Constructor for fetching Coupon id
    public MA2_CouponTypeImagesController(ApexPages.StandardController controller) {
        attachFile = new Attachment();
        couponId = ApexPages.currentPage().getParameters().get('id');
        couponImageRecord = new MA2_CouponImage__c();
        couponImageRecord.MA2_Coupon__c  = couponId;
    }
    
    /*
     *Method for showing Coupon Type
     */
    public List<SelectOption> getCouponTypeList(){
        List<SelectOption> couponTypeList = new List<SelectOption>();
        couponTypeList.add(new selectOption('--None--','--None--'));
        Schema.sObjectType sobject_type = MA2_CouponImage__c.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get('MA2_Coupon_Type__c').getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for(Schema.PicklistEntry value : pick_list_values) { //for all values in the picklist list
            couponTypeList.add(new selectOption(value.getValue(), value.getLabel())); //add the value and label to our final list
        }
        return couponTypeList;
    }
    
    /*
     * Method for saving coupon images for current coupon
     */
    public PageReference saveRecord(){
        count1 = 0;
        Integer couponCount = 0;
        fileType = '';
        System.Debug('attachFile--'+attachFile);
        if(couponType != '--None--'){
            couponCount =  [select count() from MA2_CouponImage__c where MA2_Coupon_Type__c =: couponType and MA2_Coupon__c =: couponId];
        }else{
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Select The Coupon Type'));  
        }
        if(couponCount != 0){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Select any other coupon Type for this coupon as Coupon Type already exist.'));     
        }
        if(fileName != null && fileName != ''){
            count1++;
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Enter File Name'));
        }
        if(uploadFile != null){
            if(!Test.isRunningTest()){
            	if(attachFile.contentType.contains('image')){
                fileType  = attachFile.contentType;
                }else{
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Upload Only Image File'));
                }      
            }else{
            	fileType =  'image/jpeg';	
            }    
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please upload File'));
        }   
           if(count1 != 0 && fileType != '' && couponCount == 0){
            MA2_CouponImage__c couponImgRecord = new MA2_CouponImage__c();
            couponImgRecord.MA2_Coupon_Type__c = couponType;
            couponImgRecord.MA2_Coupon__c = couponImageRecord.MA2_Coupon__c;
            couponImgRecord.MA2_Status__c = couponImageRecord.MA2_Status__c;
            insert couponImgRecord;
            if(couponImgRecord.Id != null){
                attachFile = new Attachment();
                attachFile.Name = fileName;
                attachFile.body = uploadFile;
                attachFile.ParentId = couponImgRecord.Id;
                attachFile.contentType = fileType;
                insert attachFile;
                //return new PageReference('/'+couponImgRecord.Id);    
                return new PageReference('/'+couponId);
            }
        }
        return null;    
    }
    
    /*
     *Method for redirecting to the uploading of new file for current Coupon
     */
    public PageReference saveNew(){
        return new PageReference('/apex/MA2_CouponTypeImages?id='+couponId);
    }
    
    /*
     *Method for redirecting back to the Coupon detail page
     */
    public PageReference cancel(){
        return new PageReference('/'+couponId);
    }
}