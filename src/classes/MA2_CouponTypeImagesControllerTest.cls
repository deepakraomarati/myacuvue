/**
* File Name: MA2_CouponTypeImagesControllerTest
* Description : Test class for MA2_CouponTypeImagesController 
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |8-Sep-2016  |hsingh53@its.jnj.com  |New Class created
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    2.0  |15-Sep-2017  |hsingh53@its.jnj.com  |Class modified
*/
@isTest
public class MA2_CouponTypeImagesControllerTest{
   
    /* Method for excuting the test class */
    static Testmethod void createCouponImgTest(){
        
        TestDataFactory_MyAcuvue.insertCustomSetting();
        final List<Coupon__c> couponList = new List<Coupon__c>(); 
        couponList.addAll(TestDataFactory_MyAcuvue.createCoupon(1));
        Test.startTest();
        
        final ApexPages.StandardController stdCon = new ApexPages.StandardController(couponList[0]);
        ApexPages.currentPage().getParameters().put('id',couponList[0].Id);
        final MA2_CouponTypeImagesController couponImageList = new MA2_CouponTypeImagesController (stdCon);
        couponImageList.couponType = 'Coupon Banner';
        couponImageList.uploadFile = Blob.valueOf('Coupon Banner');
        couponImageList.fileName = 'File';
        system.assertequals(couponImageList.couponType,'Coupon Banner','Success');
        couponImageList.getCouponTypeList();
        couponImageList.saveRecord();
        couponImageList.saveNew();
        couponImageList.cancel();
        Test.stopTest();
    }
}