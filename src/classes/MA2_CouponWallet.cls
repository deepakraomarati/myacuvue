/**
* File Name: MA2_CouponWallet
* Description : Sending Coupon Wallet Info to Apigee System
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |09-Oct-2016 |hsingh53@its.jnj.com  |New Class created
*/
public class MA2_CouponWallet{
    
    /* Method for creating json body */
    public static void sendData(List<CouponContact__c> couponWallet , String flag, String operation){
        String jsonString,jsonstringfinal,CouponSize;
        Set<ID> CouponIds = new Set<ID>();
        Set<String> FinalCouponNames = new Set<String>();
        list<JSONstringtosend>jsendlist=new list <JSONstringtosend>();
        List<String> MA2CountryList = new List<String>();
        Map<String, MA2_CountryCode__c> countries = MA2_CountryCode__c.getAll();
        MA2CountryList.addAll(countries.keySet());
        List<CouponContact__c> couponList = [select Name,ExpiryDate__c,CouponId__c,ContactId__c,AccountId__c,ContactId__r.MembershipNo__c,ContactId__r.MA2_Country_Code__c,
                                             MA2_CouponCode__c,RegisteredFrom__c,MA2_Inactive__c,MA2_CouponUsed__c,
                                             createdDate,lastmodifiedDate
                                             from CouponContact__c where Id in: couponWallet
                                             and Apigee_Update__c!=true and MA2_Country_Code__c  in :MA2CountryList and MA2_Country_Code__c  != 'KOR' and (CouponId__r.RecordType.Name = 'Etrial' 
                                                                                                                                                           or CouponId__r.RecordType.Name = 'Cash Discount'
                                                                                                                                                           or CouponId__r.RecordType.Name = 'Bonus Multiplier')];
        if(couponList.size() > 0){
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            for(CouponContact__c couponCon : couponList){
                CouponIds.add(couponCon.Id);
                CouponSize = string.valueof(CouponIds.size());
                FinalCouponNames.add('Coupon Wallet ID -->' + ' ' + couponCon.Id + ':' + 'Coupon Wallet Name'+ ' '+ CouponCon.name);
                JSONstringtosend jsend=new JSONstringtosend ();
                jsend.ParentCouponID=couponCon.CouponId__c;jsend.accountId=couponCon.AccountId__c;
                jsend.couponCode=couponCon.Id;jsend.expiryDate=couponCon.ExpiryDate__c;
                jsend.region=couponCon.ContactId__r.MA2_Country_Code__c;jsend.consumerId=couponCon.ContactId__r.MembershipNo__c;
                if(couponCon.MA2_CouponUsed__c)
                    jsend.couponUsed=true;
                else
                    jsend.couponUsed=false;
                if(couponCon.MA2_Inactive__c)
                    jsend.couponInactiveStatus=true;
                else
                    jsend.couponInactiveStatus=false;
                jsend.RegisteredFrom=couponCon.RegisteredFrom__c;jsend.CreatedDate=couponCon.createdDate;
                jsend.LastModifiedDate=couponCon.lastmodifiedDate;jsendlist.add(jsend);
            }
            
            if(jsendlist.size()>0){
                jsonString=json.serializePretty(jsendlist);
            }
            if (jsonString != null || jsonString != '') {
                jsonString = jsonString.replace(']', '');
				jsonString = jsonString.replace('[', '');
                jsonstringfinal = '{' + '"couponEtrial":[' + '\n';
                jsonstringfinal += jsonString;
                jsonstringfinal += '\n' + ']' + '}';
            }    
        }
        if(jsonstringfinal != '' &&  jsonstringfinal != null){
            sendtojtracker(jsonstringfinal, CouponSize , flag, FinalCouponNames,operation);
        }
    }
    
    /* Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONObject class */    
    @future(callout = true)
    public static void sendtojtracker(String jsonBody, String CouponSize ,String flag, Set<String> FinalCouponNames,String operation) {
        JSONObject oauth = null;
        Credientials__c testPub = new Credientials__c();
		 if(operation == 'insert'){
            testPub  = Credientials__c.getInstance('MyAcuvue_global_sfdc_coupon_Insert');
        }else if(operation == 'update'){
            testPub  = Credientials__c.getInstance('MyAcuvue_global_sfdc_coupon_update');
        }
        if(testPub != null){
            if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
                final String ClientId = testPub.Client_Id__c;final String ClientSecret = testPub.Client_Secret__c;
                final String TargetUrl = testPub.Target_Url__c;final String ApiKey = testPub.Api_Key__c; 
                oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody, CouponSize , flag, FinalCouponNames,operation);
            }
        }         
    }
    
    /* Method for sending data to the Apigee system */    
    private static void oauthLogin(String targetUrl, String clientId, String clientSecret, String apiKeyValue,String jsonBody,String CouponSize, String flag, Set<String> FinalCouponNames,string operation) {
        HttpRequest loginRequest = New HttpRequest();
		if(operation == 'insert'){
            loginRequest.setMethod('POST');
        }
        else{
          loginRequest.setMethod('PUT');
        }
        loginRequest.setTimeout(120000);loginRequest.setEndpoint(targetUrl);
        loginRequest.setHeader('grant_type', 'authorization_code');loginRequest.setHeader('client_id',clientId);
        loginRequest.setHeader('client_secret',clientSecret);loginRequest.setHeader('apikey',apiKeyValue);
        loginRequest.setHeader('Content-Type', 'application/json');loginRequest.setBody(jsonBody);
        Http Http = New Http();
        HTTPResponse loginResponse = new HTTPResponse();
        string body = '',errorbody=''; 
        if(!Test.isRunningTest()){
             loginResponse = http.send(loginRequest);
            JSONObject oAuth = (JSONObject) JSON.deserialize(loginResponse.getbody(), JSONObject.class);
        }
                             
            if(loginResponse.getStatus() == 'OK' && loginResponse.getStatusCode() == 200){
                body += 'Hello Admin,' + '\n';body += '\n';body += 'The coupon expiry date is successfully updated in Apigee' + '\n';body += '\n';body += 'There were' + ' '+ CouponSize + ' ' + 'successful upserts and' + ' ' + '0' + ' ' + 'errors.' + '\n';
                body += '\n';body += 'To find out more review attached success_error logs '+ ' ' + '\n';body += '\n';
                for(String item: FinalCouponNames){
                    errorbody += item + '\n';errorbody += '\n';
                }
                body += '\n';body += 'Regards,'+ '\n';body += '\n';body += 'Skywalker Support Team' + '\n';body += '\n';
                body += 'Note:- This email is an automatic email send by SFDC Interface. Please don’t not respond to this';                
            }else{
                body += 'Hello Admin,' + '\n';body += '\n';body += 'The coupon expiry date is successfully updated in Apigee' + '\n';
                body += '\n';body += 'There were' + ' '+ '0' + ' ' + 'successful upserts and' + ' ' + CouponSize + ' ' + 'errors.' + '\n';
                body += '\n';body += 'To find out more review attached success_error logs '+' ' + '\n';body += '\n';               
                for(String item: FinalCouponNames){
                    errorbody += item + '\n';errorbody += '\n';
                } 
                body += '\n';body += 'Regards,'+ '\n';body += '\n';body += 'Skywalker Support Team' + '\n';body += '\n';
                body += 'Note:- This email is an automatic email send by SFDC Interface. Please don’t not respond to this';                               
            }
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {'lbhutia@its.jnj.com','nkamal8@its.jnj.com','ledward4@ITS.JNJ.com'};
            mail.setToAddresses(toAddresses);mail.setSubject('Data Send to Apigee');            
            String myString = string.valueof(errorbody);Blob myBlob = Blob.valueof(myString);            
            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
            efa.setFileName('Success_Erorr Log.txt');efa.setBody(myblob);
            mail.setFileAttachments(new List<Messaging.Emailfileattachment>{efa});mail.setPlainTextBody(body);
            if(flag == 'schedule'){
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            } 
	}
    
    public class JSONObject {
        public String id {
            get;set;
        }
        public String issued_at {
            get;set;
        }
        public String instance_url {
            get;set;
        }
        public String signature {
            get;set;
        }
        public String access_token {
            get;set;
        }        
        public string status {
            get;set;
        }
    }
    
    public class JSONstringtosend {
        public String ParentCouponID {
            get;set;
        }
        public String accountId {
            get;set;
        }
        public String couponCode {
            get;set;
        }
        public Datetime expiryDate {
            get;set;
        }
        public String region {
            get;set;
        }
        public String consumerId{
            get;set;
        }
        public Boolean couponUsed{
            get;set;
        }
        public Boolean couponInactiveStatus{
            get;set;
        }      
        public String RegisteredFrom{
            get;set;
        }
        public Datetime CreatedDate{
            get;set;
        }
        public Datetime LastModifiedDate{
            get;set;
        }
    }
}