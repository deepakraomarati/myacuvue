Public class MA2_CouponWalletMapping{

    public static void walletMap(list<CouponContact__c> walletList)
    {
        System.Debug('walletList--'+walletList);
         List<String> lstAccId= new List<String>();
         List<String> lstConId= new List<String>();
         List<String> lstTransId= new List<String>();
         List<String> lstCouponId= new List<String>();
     
         List<CouponContact__c> Consumer= new List<CouponContact__c>();
      
         Map<id,Account> MapAccount=new map<id,Account>();
         Map<id,Contact> MapContact=new map<id,Contact>();
         Map<id,TransactionTd__c> MapTransaction=new map<id,TransactionTd__c>();
         Map<id,Coupon__c> MapCoupon=new map<id,Coupon__c>();
        
        for(CouponContact__c Crobj: walletList)
        {
            Consumer.add(Crobj);
        
            if(Crobj.MA2_AccountId__c!=null){
                lstAccId.add(Crobj.MA2_AccountId__c);
            }
        
            if(Crobj.MA2_ContactId__c!=null){
                lstConId.add(Crobj.MA2_ContactId__c);
            }
            
            if(Crobj.MA2_TransactionId__c!=null){
                lstTransId.add(Crobj.MA2_TransactionId__c);
            }
            if(Crobj.MA2_CouponId__c!=null){
                lstCouponId.add(Crobj.MA2_CouponId__c);
            }
        
        }
      
        if(lstAccId.size()>0 && !lstAccId.isEmpty())
        {    
            MapAccount=new Map<Id,Account>([select id,Name,OutletNumber__c from account where OutletNumber__c IN:lstAccId]);
        } 
         
        if(lstConId.size()>0 && !lstConId.isEmpty())
        {
            MapContact =new Map<Id,Contact>([select id,LastName,MembershipNo__c from contact where MembershipNo__c IN:lstConId]);
        }
        
        if(lstTransId.size()>0 && !lstTransId.isEmpty())
        {
            MapTransaction=new Map<Id,TransactionTd__c>([select id,Name from TransactionTd__c  where Id IN:lstTransId]);
        }
        
        if(lstCouponId.size()>0 && !lstCouponId.isEmpty())
        {
            MapCoupon=new Map<Id,Coupon__c>([select id,Name from Coupon__c where Id IN:lstCouponId]);
        }
       
        Map<String,Id> mapAccountIds= new Map<String,Id>();
        for(Account acc:MapAccount.Values()){
            mapAccountIds.put(acc.OutletNumber__c,acc.Id);
        }
         
         
        Map<String,Id> mapContactIds= new Map<String,Id>();
        for(Contact con: MapContact.Values()){
            mapContactIds.put(con.MembershipNo__c,con.Id);
        }
        
        Map<Id,Id> mapTransIds= new Map<Id,Id>();
        for(TransactionTd__c tr: MapTransaction.Values()){
            mapTransIds.put(tr.Id,tr.Id);
        }
        
        Map<Id,Id> mapCouponIds= new Map<Id,Id>();
        for(Coupon__c co: MapCoupon.Values()){
            mapCouponIds.put(co.Id,co.Id);
        }
         
         
        List<CouponContact__c> lstConsumerBooking = new List<CouponContact__c>();

        for(CouponContact__c cc:Consumer)
        {
            if(cc.MA2_AccountId__c!=null && mapAccountIds.get(cc.MA2_AccountId__c)!=null){
                 cc.AccountId__c= mapAccountIds.get(cc.MA2_AccountId__c);
                   
            }
            if(cc.MA2_ContactId__c!=null && mapContactIds.get(cc.MA2_ContactId__c)!=null){
                 cc.ContactId__c= mapContactIds.get(cc.MA2_ContactId__c);
            } 
            
            if(cc.MA2_TransactionId__c!=null && mapTransIds.get(cc.MA2_TransactionId__c)!=null){
                 cc.TransactionId__c= mapTransIds.get(cc.MA2_TransactionId__c);
            } 
            
            if(cc.MA2_CouponId__c!=null && mapCouponIds.get(cc.MA2_CouponId__c)!=null){
                 cc.CouponId__c= mapCouponIds.get(cc.MA2_CouponId__c);
            }   
            //cc.TransactionId__c = 'a19p0000000ghQoAAI';
            System.Debug('cc---'+cc.TransactionId__c);
            lstConsumerBooking.add(cc);
        }
        System.Debug('lstConsumerBooking------'+lstConsumerBooking);
    }
}