@isTest
public class MA2_CouponWalletMappingTest{
    static Testmethod void createCouponImgTest(){
        
        final List<Contact> contactList = new List<Contact>();
        contactList.addAll(TestDataFactory_MyAcuvue.createContact(1,TestDataFactory_MyAcuvue.createAccount(1)));
        TestDataFactory_MyAcuvue.insertCustomSetting();
        final List<Coupon__c> couponList = new List<Coupon__c>();
        couponList.addAll(TestDataFactory_MyAcuvue.createCoupon(1));
        system.assertEquals(couponList.size(),18,'success');
        TestDataFactory_MyAcuvue.createCouponWallet(1,couponList,contactList);
    }
}