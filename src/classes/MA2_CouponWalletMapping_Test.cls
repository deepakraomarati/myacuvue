@isTest
public with sharing class MA2_CouponWalletMapping_Test 
{
    static testmethod void couponWalletMap()
    {
        TestDataFactory_MyAcuvue.insertCustomSetting();
        TriggerHandler__c triggerHandler = new TriggerHandler__c(Name = 'HandleTriggers',MA2_createUpdateTransaction__c=true);
        insert triggerHandler;
        final List<Account> accList = new List<Account>();
        final List<Contact> contactList = new List<Contact>();
        final list<TransactionTd__c> transList=new List<TransactionTd__c >();
        final list<Coupon__c> couponList=new list<Coupon__c>();
        final list<CouponContact__c> couponWalletList=new list<CouponContact__c>();
        accList.addAll(TestDataFactory_MyAcuvue.createAccount(1));
        contactList.addAll(TestDataFactory_MyAcuvue.createContact(1,accList));
        transList.addAll(TestDataFactory_MyAcuvue.createTransaction(1,accList,contactList));
        couponList.addAll(TestDataFactory_MyAcuvue.createCoupon(1));
        system.assertEquals(couponList.size(),18,'success');
        couponWalletList.addAll(TestDataFactory_MyAcuvue.createCouponWallet(1,couponList,contactList));     
    }
}