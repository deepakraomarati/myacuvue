/**
* File Name: MA2_CouponWalletReport
* Description : Batch job for scheduling Coupon Wallet Report
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | neel.kamal@cognizant.com
* 
* Modification Log 
* =============================================================== **/
global class MA2_CouponWalletReport implements Database.Batchable<sObject>, Database.Stateful{
    global List<CouponContact__c> couponList = new List<Couponcontact__c>();
    global string header = 'Coupon Wallet Code,Coupon Name,Country Code,18 Digit Id,Apigee Coupon Id,Apigee Contact Id,Apigee Account Id,Apigee Transaction Id,Apigee Update,Coupon Code,Account,Contact,Coupon,Transaction ID,Expiry Date,Coupon Used,Coupon Inactive,Coupon Wallet : Created Date,Coupon Wallet : Modified Date,Coupon Wallet : Created By,Coupon Wallet : LastModified By \n';
    global string finalstr{get; set;}
    global string RecString;
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id,Name,MA2_Country_Code__c,MA2_18_Digit_Id__c,MA2_CouponId__c,MA2_ContactId__c,MA2_AccountId__c
                                         ,MA2_TransactionId__c,Apigee_Update__c,MA2_CouponCode__c,AccountId__r.name,ContactId__r.name,CouponId__r.name,TransactionId__r.name,ExpiryDate__c,
                                         MA2_CouponUsed__c,MA2_Inactive__c,CreatedDate,LastModifiedDate,CreatedBy.name,LastModifiedBy.name FROM CouponContact__c where lastmodifiedDate = Today AND
                                         (MA2_Country_Code__c = 'HKG' OR MA2_Country_Code__c = 'SGP' OR MA2_Country_Code__c = 'TWN') ]);
    }
    
    global void execute(Database.batchableContext BC,List<CouponContact__c> couponList){
        finalstr = '';
        for(CouponContact__c a : couponList){
            RecString = a.Id+','+a.Name+','+a.MA2_Country_Code__c+','+a.MA2_18_Digit_Id__c+','+a.MA2_CouponId__c+','+a.MA2_ContactId__c+','+a.MA2_AccountId__c+','+a.MA2_TransactionId__c+','+a.Apigee_Update__c+','+a.MA2_CouponCode__c+','+a.AccountId__r.name+','+a.ContactId__r.name+','+a.CouponId__r.name+','+a.TransactionId__r.name+','+a.ExpiryDate__c+','+a.MA2_CouponUsed__c+','+a.MA2_Inactive__c+','+a.CreatedDate+','+a.LastModifiedDate+','+a.CreatedBy.name+','+a.LastModifiedBy.name+'\n';
            finalstr = finalstr +RecString;
        }        
    }
    
    global void finish(Database.batchableContext BC){  
        finalstr = header + finalstr;
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string datestamp = string.valueof(system.now());
        string csvname= 'Coupon Wallet Report'+datestamp+'.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        fileAttachments.add(csvAttc);
        
        string body = '';
        body += 'Hello Team,' + '\n';
        body += '\n';
        body += 'Attached the Coupon Wallet details extracted from SFDC for data validation..' + '\n';
        body += '\n';
        body += 'Regards' + '\n';
        body += 'Skywalker Support'+ ' '+ '\n';
        body += '\n';
        body += 'Note:- This email has been sent automatically by SFDC Interface.'+ ' '+ '\n';
        body += '\n';
        
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        string emailAddress = System.Label.MA2_ToEmailAddress;
        string[] toAddresses = emailAddress.split(',');
        string subject ='Coupon Wallet Report'+string.valueof(system.now())+'.csv';
        email.setSubject(subject);
        email.setToAddresses(toAddresses);
        email.setPlainTextBody(body);
        email.setFileAttachments(fileAttachments);
        try{
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        }
        catch (exception e){
        }
    }  
}