/*
* File Name: MA2_CouponWalletTest
* Description : Test class for MA2_CouponWallet
* Copyright : Johnson & Johnson
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    2.0  |30-Aug-2017  |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_CouponWalletTest{
    
    /* Method for excuting the test class */
    static Testmethod void MA2_CWTestMethod(){
        final Credientials__c cred = new Credientials__c(Name = 'MyAcuvue_global_sfdc_coupon_Insert' , 
                                                   Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                                                   Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                                                   Target_Url__c = 'https://jnj-dev.apigee.net/v1/consumer/catalog/addcoupon');
        insert cred;
        final TriggerHandler__c mcs = new TriggerHandler__c(name = 'HandleTriggers',CouponContact__c=true);
        insert mcs;
        final string countrycode = 'SGP';
        TestDataFactory_MyAcuvue.insertCustomSetting();
        Test.startTest();
        MA2_CountryCode__c CountryCodeC = new MA2_CountryCode__c(Name= countrycode, MA2_CountryCodeValue__c = countrycode);
        insert CountryCodeC;
        MA2_Country_Currency_Map__c CountryMap = new MA2_Country_Currency_Map__c(Name='SGP',Currency__c='SGD');
        insert CountryMap;
        
        final List<Account> accList = new List<Account>();
        final List<Contact> conList = new List<Contact>();
        final List<Contact> couponListNew = new List<Contact>();
        accList.addAll(TestDataFactory_MyAcuvue.createAccount(1));
        system.assertEquals(accList.size(), 4);                              
        conList.addAll(TestDataFactory_MyAcuvue.createECPContact(2,accList));
        
        for(contact con :conList){
            con.MA2_Country_Code__c = countrycode;
            couponListNew.add(con);
        }
        update couponListNew;
        
        final Id etrialRecordTypeId = [select Id from RecordType where Name = 'Etrial' and sObjectType = 'Coupon__c'].Id;
        final Coupon__c coupon = new Coupon__c(MA2_CouponName__c = 'Etrial',RecordTypeID = etrialRecordTypeId ,MA2_CountryCode__c =countrycode
                                               ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Pre-Assessment');  
        insert coupon;
        
        final CouponContact__c couponWallet = new CouponContact__c(ExpiryDate__c = System.Today() - 10 ,ContactId__c = conList[0].Id ,
                                                                   CouponId__c = coupon.Id , MA2_AccountId__c = '12345' , Apigee_Update__c = false, MA2_ContactId__c = '5555',CurrencyIsoCode='SGD');   
        
        insert couponWallet;
        couponWallet.MA2_AccountId__c='23456';
        update couponWallet;
        Test.stopTest();
    }
}