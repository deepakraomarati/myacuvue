/**
* File Name: MA2_CouponWithTransactions
* Description : Batch job for scheduling consumer with transactions report
* Copyright : Johnson & Johnson
* @author : Lhawang Bhutia | lbhutia@its.jnj.com | lhawang.bhutia@cognizant.com
* 
* Modification Log 
* =============================================================== **/

global class MA2_CouponWithTransactions implements Database.Batchable<sObject>,Database.stateful{
    
    global list<CouponContact__c> CouponWallet = new list<CouponContact__c>();
    global string header = 'Id, Transaction Type, Apigee Transaction ID, Consumer Id, Store Id , Coupon Id, Coupon Wallet Id, Transaction Created Date \n';
    global string finalstr{get; set;}
    global string RecString;
    
    /*Method for querying all the Consumer Records with Transactions */ 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String userName = '%Webservice%';
        User userRecord = [select Id from user where Name like '%Webservice API MyAcuvue%' limit 1];
        return Database.getQueryLocator([select TransactionId__r.Id,TransactionId__r.Name,TransactionId__r.MA2_TransactionType__c,
                                         TransactionId__r.MA2_TransactionId__c,ContactId__r.MembershipNo__c,TransactionId__r.AccountID__r.OutletNumber__c,
                                         CouponId__r.Id,TransactionId__r.CreatedById,TransactionId__r.CreatedDate,Id 
                                         from CouponContact__c where (MA2_Country_Code__c = 'HKG' OR MA2_Country_Code__c = 'SGP' OR MA2_Country_Code__c = 'TWN') 
                                         and TransactionId__r.lastmodifiedbyId !=: userRecord.Id and TransactionId__r.lastmodifieddate = today]);    
    }
    /* Method for excuting the query record */
    global void execute(Database.batchableContext BC,List<CouponContact__c> CouponWallet){    
        finalstr = '';
        for(CouponContact__c l : CouponWallet){
            RecString = l.TransactionId__r.Id+','+l.TransactionId__r.MA2_TransactionType__c+','+l.TransactionId__r.MA2_TransactionId__c+','+l.ContactId__r.MembershipNo__c+','+l.TransactionId__r.AccountID__r.OutletNumber__c+','+l.CouponId__r.Id+','+l.Id+','+l.TransactionId__r.CreatedDate+'\n';
            finalstr = finalstr + RecString;
        }        
    }
    
    global void finish(Database.batchableContext BC){
        finalstr = header + finalstr;
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        system.debug('Final Str' + finalstr);
        blob csvBlob = Blob.valueOf(finalstr);
        string datestamp = string.valueof(system.now());
        string csvname= 'Transaction With Coupons'+datestamp+'.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        fileAttachments.add(csvAttc);
        
        string body = '';
        body += 'Hello Team,' + '\n';
        body += '\n';
        body += 'Attached the Manual Transaction details  extracted from SFDC for data validation..' + '\n';
        body += '\n';
        body += 'Regards' + '\n';
        body += 'Skywalker Support'+ ' '+ '\n';
        body += '\n';
        body += 'Note:- This email has been sent automatically by SFDC Interface.'+ ' '+ '\n';
        body += '\n';
        
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        string emailAddress = System.Label.MA2_ToEmailAddress;
        string[] toAddresses = emailAddress.split(',');
        
        system.debug('Email Address' + toAddresses);    
        string subject ='Transaction With Coupons'+string.valueof(system.now())+'.csv';
        email.setSubject(subject);
        email.setToAddresses(toAddresses);
        email.setPlainTextBody(body);
        email.setFileAttachments(fileAttachments);
        try{
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        }catch (exception e){
            system.debug('Exception -->' + e);
        }  
        
    }       
}