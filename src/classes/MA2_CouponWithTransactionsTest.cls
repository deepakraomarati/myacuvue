/**
* File Name: MA2_CouponWithTransactionsTest
* Description : Test class for MA2_CouponWithTransactions
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | nkamal8@cognizant.com
* Modification Log 
* ================================================================
*    Ver  |Date         |Author                |Modification
*    2.0  |10-OCT-2017  |nkamal8@its.jnj.com   |Class created
*/
@isTest
public class MA2_CouponWithTransactionsTest{
    /* Method for excuting the test class */
    static Testmethod void runBatchJobMethod(){
        Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id;
        TriggerHandler__c mcs = new TriggerHandler__c(Name='HandleTriggers',MA2_createUpdateTransaction__c =true,MA2_createContact__c = True,CouponContact__c=true);
        insert mcs;
        
        MA2_Country_Currency_Map__c customcurrency = new MA2_Country_Currency_Map__c();
        customcurrency.name='HKG';
        customcurrency.Currency__c = 'HKD';
        insert customcurrency;
        
        final List<Account> accList = new List<Account>();
        final Account acc = new Account(Name = 'Test' , MA2_AccountId__c = '1234' ,AccountNumber = '54321',
                                        Marketing_Program__c = 'AEC',OutletNumber__c = '54321',ActiveYN__c = true,
                                        My_Acuvue__c = 'Yes',CountryCode__c = 'SGP',PublicZone__c = 'Testtt',ECP_Name__c = 'ABC',PublicAddress__c = 'Hi5',PublicPhone__c = '998978',PublicState__c = 'XYZA');
        
        insert acc;
        accList.add(acc);
        Contact conRec = new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',
                                     RecordTypeId = consumerRecTypeId,
                                     AccountId = accList[0].Id, email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'HKG',
                                     MA2_Contact_lenses__c='No',
                                     DOB__c  = System.Today(),
                                     MA2_PreAssessment__c = true);
        insert conRec;
        
        TransactionTd__c trans = new TransactionTd__c(AccountID__c = accList[0].Id, MA2_Contact__c = conRec.Id,
                                                      MA2_AccountId__c = '123456',MA2_ContactId__c = '23456' , MA2_Points__c = 10 ,MA2_Redeemed__c = 10,
                                                      MA2_PointsExpired__c = true,MA2_Price__c = 2000,
                                                      CreatedDateTest__c = date.newinstance(2015,01,12));
        insert trans;
        final List<Contact> conList = new List<Contact>();
        final List<Coupon__c> coupon = new List<Coupon__c>();
        final List<CouponContact__c> couponwallet = new List<CouponContact__c>();
        final List<TransactionTd__c> transactionList = new List<TransactionTd__c>();
        final List<MA2_TransactionProduct__c> tpList = new List<MA2_TransactionProduct__c>();
        
        coupon.addAll(TestDataFactory_MyAcuvue.createCoupon(1));
        transactionList.add(trans);
        
        CouponContact__c con = new CouponContact__c();
        con.ExpiryDate__c = System.Today() - 10;
        con.ContactId__c = conRec.Id ;
        con.CouponId__c =  coupon[0].id;
        con.MA2_AccountId__c = accList[0].id;
        con.TransactionId__c = transactionList[0].id;
        insert con;    
        
        Test.startTest();
        Database.executeBatch(new MA2_CouponWithTransactions()); 
        system.assertEquals(accList.size(), 1); 
        Test.stopTest();
    }
}