/*
* File Name: MA2_ECPDeleteServiceTest
* Description : Test class for MA2_ECPDeleteService
* Copyright : Johnson & Johnson
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    2.0  |30-Aug-2017  |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_ECPDeleteServiceTest{
    
    /* Method for excuting the test class */
    static Testmethod void deleteECPMethod(){
        final Credientials__c cred = new Credientials__c(Name = 'ECPrelationshipDeleteAPI' , 
                                                   Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                                                   Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                                                   Target_Url__c = 'https://jnj-global-test.apigee.net/v1/sfdc/ecpRelationship/delete');
        insert cred;
        
        Test.startTest();
        final List<Account> accList = new List<Account>();
        accList.addAll(TestDataFactory_MyAcuvue.createAccount(1));
        system.assertEquals(accList.size(), 4,'Success');                              
        
        final List<Contact> ConList = new List<Contact>();
        ConList.addAll(TestDataFactory_MyAcuvue.createContact(1, accList));
        
        final List<MA2_UserProfile__c> MA2_UsrpfList = new List<MA2_UserProfile__c>();
        MA2_UsrpfList.addAll(TestDataFactory_MyAcuvue.insertUserProfile(1));
        
        final List<MA2_RelatedAccounts__c> RAList = new List<MA2_RelatedAccounts__c>();
        RAList.addAll(TestDataFactory_MyAcuvue.insertRelatedAccount(1, accList, ConList, MA2_UsrpfList));
        
        MA2_ECPrelationshipDeleteService.sendECPRelDataToApigee(RAList);      
        Test.stopTest();
    }
}