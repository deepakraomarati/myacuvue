public class MA2_ECPrelationshipDeleteService{
  
     /* Method for creating json body 
     */
    public static void sendECPRelDataToApigee(List<MA2_RelatedAccounts__c> RAList){
    
    String jsonString  = '';
  
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            for(MA2_RelatedAccounts__c RA : RAList){                       
              gen.writeStringField('prouserId',RA.MA2_ContactId__c+'');                         
              gen.writeStringField('accountId',RA.MA2_AccountId__c+'');     
              gen.writeStringField('userProfile',RA.MA2_ProfileId__c+''); 
            }
            gen.writeEndObject();
            jsonString = jsonString + gen.getAsString();
        
        System.Debug('jsonString--'+jsonString);
        if(jsonString != ''){
            sendECPDatatojtracker(jsonString);
        }
    }
    
    /*
     * Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONECPObject class
     */
    @future(callout = true)
    public static void sendECPDatatojtracker(String jsonBody) {
        JSONECPObject oauth = null;
       Credientials__c testPub = new Credientials__c();
       if(Credientials__c.getInstance('ECPrelationshipDeleteAPI') != null){
           testPub  = Credientials__c.getInstance('ECPrelationshipDeleteAPI');
           if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
                       final String ClientId = testPub.Client_Id__c;
                       final String ClientSecret = testPub.Client_Secret__c;
                       final String TargetUrl = testPub.Target_Url__c;
                       final String ApiKey = testPub.Api_Key__c; 
                       oauth = oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody);
           }
       }    
       /*SonarQube Fix*/	   
       //System.debug('------oauth response------>>>>'+oauth);
        
    }
    
    /*
     * Method for sending data to the Apigee system
     */
    private static JSONECPObject oauthLogin(String targetUrl, String clientId, String clientSecret, String apiKeyValue,String jsonBody) {
      HttpRequest loginRequest = New HttpRequest();
      loginRequest.setMethod('POST');
      loginRequest.setEndpoint(targetUrl);
      loginRequest.setHeader('grant_type', 'authorization_code');
      loginRequest.setHeader('client_id',clientId);
      loginRequest.setHeader('client_secret',clientSecret);
      loginRequest.setHeader('apikey',apiKeyValue);
      loginRequest.setHeader('Content-Type', 'application/json');
     
      system.debug('<<<<jsonString>>>>>'+jsonBody);

      loginRequest.setBody(jsonBody);
      Http Http = New Http();
      HTTPResponse loginResponse = new HTTPResponse();
      if ( !Test.isRunningTest() ){
          loginResponse = http.send(loginRequest);
          JSONECPObject oAuth = (JSONECPObject) JSON.deserialize(loginResponse.getbody(), JSONECPObject.class);
          return oAuth;
      }
      
      System.Debug('loginResponse --'+loginResponse.getBody());
    
      return null;
 }
 
 // Inner class for setting value 
 public class JSONECPObject {
      public String id {get;set;}
      public String issued_at {get;set;}
      public String instance_url {get;set;}
      public String signature {get;set;}
      public String access_token {get;set;}
   }
}