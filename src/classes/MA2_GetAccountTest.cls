@isTest
public with sharing class MA2_GetAccountTest
{    
    static testmethod void mapContact()
    {
        List<Contact> conList=new List<Contact>();
        account ac = new account();
        ac.name='test';
        ac.Aws_AccountId__c='1234';
        insert ac;
        
        Contact con=new Contact();
        con.Aws_AccountId__c='1234';
        con.Aws_ContactId__c='123';
        con.LastName='Ram';
        con.Gender__c = 'm';
        con.Status__c  = 'N';
        conList.add(con);
        insert conList;
        system.assertEquals(conList.size(),1,'success');
    }
}