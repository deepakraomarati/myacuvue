public class MA2_HongKongCoupon{
    final public static DateTime dt = System.Now();
    public static List <CouponContact__c> assignCouponPreasseesmentChecked(Contact con , List<Coupon__c> couponETPreList){
        List <CouponContact__c> createCouponList = new List <CouponContact__c>();
        CouponContact__c couponContact = new CouponContact__c();
        for(Coupon__c cou : couponETPreList){
            if(con.MA2_Country_Code__c == 'HKG' && cou.MA2_CountryCode__c == 'HKG' && con.MA2_Contact_lenses__c != null){
                CouponContact__c couponOtherContact = new CouponContact__c();
                couponOtherContact.ContactId__c = con.Id;
                couponOtherContact.AccountId__c = con.AccountId;
                couponOtherContact.ActiveYN__c = true;
                if(con.MA2_Contact_lenses__c == 'Acuvue Brand' || con.MA2_Contact_lenses__c == '有，ACUVUE®品牌' || con.MA2_Contact_lenses__c == '有，Acuvue 安視優' || con.MA2_Contact_lenses__c == '有，安視優®ACUVUE®'){
                    if(cou.RecordType.Name == 'Etrial' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                       && cou.MA2_CouponName__c == '5 Days Trial for Existing Wearer'){
                           Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                           if(count != null){
                               couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                           }
                           couponOtherContact.CouponId__c = cou.Id;
                           createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Etrial' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                            && cou.MA2_CouponName__c == 'Etrial Moist for Astigmatism'){
                            Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                            if(count != null){
                                couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                            }
                            couponOtherContact.CouponId__c = cou.Id;
                            createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Etrial' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                            && cou.MA2_CouponName__c == 'Etrial Define'){
                             Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                             if(count != null){
                                 couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                             }
                             couponOtherContact.CouponId__c = cou.Id;
                             createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Etrial' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                            && cou.MA2_CouponName__c == 'Etrial Multifocal'){
                              Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                              if(count != null){
                                  couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                              }
                              couponOtherContact.CouponId__c = cou.Id;
                              createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Etrial' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                            && cou.MA2_CouponName__c == 'Etrial Oasys 1 Day'){
                               Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                               if(count != null){
                                   couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                               }
                               couponOtherContact.CouponId__c = cou.Id;
                               createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                            && cou.MA2_CouponName__c == 'Moist for Astigmatism Existing Wearer'){
                                Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                if(count != null){
                                    couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                }
                                couponOtherContact.CouponId__c = cou.Id;
                                createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                             && cou.MA2_CouponName__c == 'Oasys 1-Day'){
                                 Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                 if(count != null){
                                     couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                 }
                                 couponOtherContact.CouponId__c = cou.Id;
                                 createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                              && cou.MA2_CouponName__c == 'Multifocal for Existing Wearer'){
                                  Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                  if(count != null){
                                      couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                  }
                                  couponOtherContact.CouponId__c = cou.Id;
                                  createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                               && cou.MA2_CouponName__c == 'Oasys 1-Day Astigmatism'){
                                   Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                   if(count != null){
                                       couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                   }
                                   couponOtherContact.CouponId__c = cou.Id;
                                   createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                && cou.MA2_CouponName__c == 'Multifocal - Existing Wearer'){
                                    Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                    if(count != null){
                                        couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                    }
                                    couponOtherContact.CouponId__c = cou.Id;
                                    createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                 && cou.MA2_CouponName__c == 'Define - Existing Wearer'){
                                     Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                     if(count != null){
                                         couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                     }
                                     couponOtherContact.CouponId__c = cou.Id;
                                     createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                && cou.MA2_CouponName__c == 'Define® Radiant Sweet™'){
                                    Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                    if(count != null){
                                        couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                    }
                                    couponOtherContact.CouponId__c = cou.Id;
                                    createCouponList.add(couponOtherContact);
                        }else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                && cou.MA2_CouponName__c == 'Premium Reward - OASYS-1 DAY'){
                                    Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                    if(count != null){
                                        couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                    }
                                    couponOtherContact.CouponId__c = cou.Id;
                                    createCouponList.add(couponOtherContact);
                        }        
                }else if(con.MA2_Contact_lenses__c == 'Other Brand' || con.MA2_Contact_lenses__c == '有，其他品牌' ||con.MA2_Contact_lenses__c == 'No' || con.MA2_Contact_lenses__c == '不是'||con.MA2_Contact_lenses__c == '沒有'){
                    if(cou.RecordType.Name == 'Etrial' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                           && cou.MA2_CouponName__c == '5 Days Trial for New Wearer'){
                               Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                               if(count != null){
                                   couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                               }
                               couponOtherContact.CouponId__c = cou.Id;
                               createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Etrial' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                && cou.MA2_CouponName__c == 'Etrial Acuvue Moist'){
                                    Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                    if(count != null){
                                        couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                    }
                                    couponOtherContact.CouponId__c = cou.Id;
                                    createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Etrial' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                 && cou.MA2_CouponName__c == 'Etrial Moist for Astigmatism'){
                                     Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                     if(count != null){
                                         couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                     }
                                     couponOtherContact.CouponId__c = cou.Id;
                                     createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Etrial' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                  && cou.MA2_CouponName__c == 'Etrial Define'){
                                      Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                      if(count != null){
                                          couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                      }
                                      couponOtherContact.CouponId__c = cou.Id;
                                      createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Etrial' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                   && cou.MA2_CouponName__c == 'Etrial Multifocal'){
                                       Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                       if(count != null){
                                           couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                       }
                                       couponOtherContact.CouponId__c = cou.Id;
                                       createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Etrial' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                && cou.MA2_CouponName__c == 'Etrial Oasys 1 Day'){
                                    Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                    if(count != null){
                                        couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                    }
                                    couponOtherContact.CouponId__c = cou.Id;
                                    createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                 && cou.MA2_CouponName__c == 'Oasys 1-Day'){
                                     Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                     if(count != null){
                                         couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                     }
                                     couponOtherContact.CouponId__c = cou.Id;
                                     createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                && cou.MA2_CouponName__c == 'Define® Radiant Sweet™'){
                                    Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                    if(count != null){
                                        couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                    }
                                    couponOtherContact.CouponId__c = cou.Id;
                                    createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                && cou.MA2_CouponName__c == 'Moist for Astigmatism'){
                                    Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                    if(count != null){
                                        couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                    }
                                    couponOtherContact.CouponId__c = cou.Id;
                                    createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                 && cou.MA2_CouponName__c == 'Multifocal'){
                                     Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                     if(count != null){
                                         couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                     }
                                     couponOtherContact.CouponId__c = cou.Id;
                                     createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                  && cou.MA2_CouponName__c == 'Oasys 1-Day Astigmatism'){
                                      Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                      if(count != null){
                                          couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                      }
                                      couponOtherContact.CouponId__c = cou.Id;
                                      createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                && cou.MA2_CouponName__c == 'Define'){
                                    Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                    if(count != null){
                                        couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                    }
                                    couponOtherContact.CouponId__c = cou.Id;
                                    createCouponList.add(couponOtherContact);
                       }else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                                && cou.MA2_CouponName__c == 'Premium Reward - OASYS-1 DAY'){
                                    Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                    if(count != null){
                                        couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                    }
                                    couponOtherContact.CouponId__c = cou.Id;
                                    createCouponList.add(couponOtherContact);
                       }      
                }
            }
        }
        return createCouponList;
    } 
}