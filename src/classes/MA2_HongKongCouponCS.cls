/**
* File Name: MA2_HongKongCouponCS
* Description : class for MA2_HongKongCouponCS
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | neel.kamal@cognizant.com
* 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |05-Jun-2018  |nkamal8@its.jnj.com  |New Class created
*=============================================================== 
*/
public class MA2_HongKongCouponCS {
     final public static DateTime dt = System.Now();
    public static List <CouponContact__c> assignCouponPreasseesmentChecked(Contact con , List<Coupon__c> couponETPreList){
        List<String> couponListone = new List<String>();
        List<String> couponListtwo = new List<String>();
        Map<String,MA2_HKCouponList__c> cpnListt = MA2_HKCouponList__c.getall();
        Map<String,MA2_HKCouponsList__c> cpnList = MA2_HKCouponsList__c.getall();
        id couponRecTypename = [select id from RecordType where sObjectType = 'Coupon__c' and Name ='Etrial'].id;
        id couponRecTypename2 = [select id from RecordType where sObjectType = 'Coupon__c' and Name = 'Cash Discount'].id;
        for(MA2_HKCouponList__c hkc : cpnListt.values()){
            couponListone.add(hkc.name);
        }
        for(MA2_HKCouponsList__c hkcpn : cpnList.values()){
            couponListtwo.add(hkcpn.name);
        }
        List<Coupon__c> cpnslistfirst = [select id,Name,MA2_CouponName__c,MA2_TimeToAward__c,MA2_CouponValidity__c,MA2_CountryCode__c,RecordType.Id from Coupon__c where (Name in : couponListone OR MA2_CouponName__c in : couponListone)];
        List<Coupon__c> cpnslistsecond = [select id,Name,MA2_CouponName__c,MA2_TimeToAward__c,MA2_CouponValidity__c,MA2_CountryCode__c,RecordType.Id from Coupon__c where (Name in : couponListtwo OR MA2_CouponName__c in : couponListtwo)];
        List <CouponContact__c> createCouponList = new List <CouponContact__c>();
        CouponContact__c couponContact = new CouponContact__c();
        for(Coupon__c cou : cpnslistfirst){
            System.Debug('mycpnlist-->'+cpnslistfirst);
            if(con.MA2_Country_Code__c == 'HKG' && cou.MA2_CountryCode__c == 'HKG' && con.MA2_Contact_lenses__c != null){
                CouponContact__c couponOtherContact = new CouponContact__c();
                couponOtherContact.ContactId__c = con.Id;
                couponOtherContact.AccountId__c = con.AccountId;
                couponOtherContact.ActiveYN__c = true; 
                if(con.MA2_Contact_lenses__c == 'Acuvue Brand' || con.MA2_Contact_lenses__c == '有，ACUVUE®品牌' || con.MA2_Contact_lenses__c == '有，Acuvue 安視優' || con.MA2_Contact_lenses__c == '有，安視優®ACUVUE®'){
                    if((cou.RecordType.Id == couponRecTypename2 || cou.RecordType.Id == couponRecTypename) && cou.MA2_TimeToAward__c == 'Post-Assessment'){  
                        Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                           if(count != null){
                               couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                               System.Debug('ExpiryDatthmm--'+(23-couponOtherContact.ExpiryDate__c.hour()));
                               SYstem.Debug('ExpiryDattteee-->'+(couponOtherContact.ExpiryDate__c.addhours(23-couponOtherContact.ExpiryDate__c.hour()).addminutes(59-couponOtherContact.ExpiryDate__c.minute()).addSeconds(-1 * couponOtherContact.ExpiryDate__c.second())));
                           }
                           couponOtherContact.CouponId__c = cou.Id;
                           createCouponList.add(couponOtherContact);
                       }   
                }
            }
        }
        for(Coupon__c cou : cpnslistsecond){
            if(con.MA2_Country_Code__c == 'HKG' && cou.MA2_CountryCode__c == 'HKG' && con.MA2_Contact_lenses__c != null){
                CouponContact__c couponOtherContact = new CouponContact__c();
                couponOtherContact.ContactId__c = con.Id;
                couponOtherContact.AccountId__c = con.AccountId;
                couponOtherContact.ActiveYN__c = true;
                if(con.MA2_Contact_lenses__c == 'Other Brand' || con.MA2_Contact_lenses__c == '有，其他品牌' ||con.MA2_Contact_lenses__c == 'No' || con.MA2_Contact_lenses__c == '不是'||con.MA2_Contact_lenses__c == '沒有'){
                    if((cou.RecordType.Id == couponRecTypename2 || cou.RecordType.Id == couponRecTypename)  && cou.MA2_TimeToAward__c == 'Post-Assessment'){
                           Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                           if(count != null){
                               couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                           }
                           couponOtherContact.CouponId__c = cou.Id;
                           createCouponList.add(couponOtherContact);
                       }   
                }
            }
        }
        
        return createCouponList;
    } 
    
}