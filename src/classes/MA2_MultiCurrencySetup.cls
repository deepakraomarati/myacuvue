@isTest
Public class MA2_MultiCurrencySetup
{
    static testMethod void CurrencySetupTest()
    {
        TestDataFactory_MyAcuvue.insertCustomSetting();
        
        Credientials__c cred2 = new Credientials__c(Name = 'SendTransactionProduct' , 
                                                    Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                                                    Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                                                    Target_Url__c = 'https://jnj-global-test.apigee.net/sfdc/transaction/products');
        insert cred2; 
        
        TriggerHandler__c cred = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                       GetTransactionTd__c = true, MA2_createUpdateTransaction__c = true, CouponContact__c= true, MA2_CouponInactiveTrigger__c = true,
                                                       MA2_CouponToApigeeTrigger__c= true, MA2_CouponWalletForApigee__c= true, MA2_createUpdateTransactionProd__c= true );
        insert cred;
        
        MA2_CountryCode__c cc = new MA2_CountryCode__c(Name= 'SGP', MA2_CountryCodeValue__c = 'SGP');
        insert cc;
        
        MA2_Country_Currency_Map__c CCM = new MA2_Country_Currency_Map__c(Name = 'SGP', Currency__c = 'SGD');
        insert CCM;
        
        Account acc = new Account(Name = 'Test' , MA2_AccountId__c = '123456' ,AccountNumber = '54321',OutletNumber__c = '54321',ActiveYN__c = true,My_Acuvue__c = 'Yes',CountryCode__c = 'SGP');
        insert acc;
        
        Id Bday1xId = [select Id from RecordType where Name = 'Bonus Multiplier' and sObjectType = 'Coupon__c'].Id;        
        
        Coupon__c coupon2 = new Coupon__c(MA2_CouponName__c = '1.5x Birthday Points',RecordTypeID = Bday1xId ,MA2_CountryCode__c ='SGP'
                                          ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Campaign');  
        insert coupon2;
        
        Coupon__c coupon3 = new Coupon__c(MA2_CouponName__c = '2x Birthday Points',RecordTypeID = Bday1xId ,MA2_CountryCode__c ='SGP'
                                          ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Campaign');  
        insert coupon3;
        
        Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id;
        
        Contact con2 = new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',
                                   RecordTypeId  = consumerRecTypeId  ,
                                   AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = 'SGD', 
                                   MembershipNo__c = 'SGP123', MA2_Country_Code__c = 'SGP', MA2_Grade__c='Base', DOB__c = date.newinstance(2017,06,12),
                                   MA2_PreAssessment__c = true);
        insert con2;
        
        Contact con3 = new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',
                                   RecordTypeId  = consumerRecTypeId  ,
                                   AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = 'SGD', 
                                   MembershipNo__c = 'SGP123', MA2_Country_Code__c = 'SGP', MA2_Grade__c='Base', DOB__c = date.newinstance(2017,05,12),
                                   MA2_PreAssessment__c = true);
        insert con3;
        
        con3.MA2_Grade__c='VIP';
        Update con3;
        
        con3.MA2_Grade__c='Base';
        Update con3;
        
        con2.MA2_Grade__c='VIP';
        Update con2;
        system.assertEquals(con2.MA2_Grade__c,'VIP','success');
    }
}