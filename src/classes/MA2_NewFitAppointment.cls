public class MA2_NewFitAppointment{
    public static void apigeeMap(list<AccountBooking__c> appList)
    {
         List<String> lstAccId = new List<String>();
         List<String> lstConId = new List<String>();
         List<String> lstApigeeConId = new List<String>();
         List<String> lstApigeeAccId = new List<String>();
         
         
         List<Account> accList = new List<Account>();
         List<Contact> conList = new List<Contact>();
         List<AccountBooking__c> lstaccbokobj= new List<AccountBooking__c>();
      
         Map<id,Account> MapAccount=new map<id,Account>();
         Map<id,Contact> MapContact=new map<id,Contact>();
        
         for(AccountBooking__c Abobj: appList){
                lstaccbokobj.add(Abobj);
                
                if(Abobj.Account__c != null){
                    lstAccId.add(Abobj.Account__c);    
                }else if(Abobj.MA2_AccountId__c!=null){
                    lstApigeeAccId.add(Abobj.MA2_AccountId__c);
                }
                
                if(Abobj.Contact__c !=null ){
                    lstConId.add(Abobj.Contact__c);
                }
                else if(Abobj.MA2_ConatctId__c!=null ){
                    lstApigeeConId.add(Abobj.MA2_ConatctId__c);
                }
        
        }
        
        if(lstAccId.size() > 0 || lstApigeeAccId.size() > 0){
            accList.addAll([select Id,OutletNumber__c from Account where Id in: lstAccId or OutletNumber__c =: lstApigeeAccId]);    
        }
        if(lstConId.size() > 0 || lstApigeeConId.size() > 0){
            conList.addAll([select Id,MembershipNo__c from Contact where Id in: lstConId or MembershipNo__c =: lstApigeeConId]);    
        }
        
        Map<String,Account> accMap = new Map<String,Account>();
        Map<String,Contact> conMap = new Map<String,Contact>();
        if(accList.size() > 0){
            for(Account acc : accList){
                if(acc.OutletNumber__c != null){
                    accMap.put(acc.OutletNumber__c , acc);
                }
                accMap.put(acc.Id , acc);
            }
        }
        
        if(conList.size() > 0){
            for(Contact con : conList){
                if(con.MembershipNo__c != null){
                    conMap.put(con.MembershipNo__c , con);
                }
                conMap.put(con.Id , con);
            }
        }
        
        for(AccountBooking__c Abobj: appList){
                lstaccbokobj.add(Abobj);
                
                if(Abobj.Account__c != null && accMap.containsKey(Abobj.Account__c) && accMap.get(Abobj.Account__c).OutletNumber__c!= null){
                    Abobj.MA2_AccountId__c = accMap.get(Abobj.Account__c).OutletNumber__c; 
                    System.Debug('check1');   
                }else if(Abobj.MA2_AccountId__c != null && accMap.containsKey(Abobj.MA2_AccountId__c)){
                    System.Debug('check2');
                    Abobj.Account__c = accMap.get(Abobj.MA2_AccountId__c).Id;
                }
                
                if(Abobj.Contact__c !=null && conMap.containsKey(Abobj.Contact__c) && conMap.get(Abobj.Contact__c).MembershipNo__c != null){
                    System.Debug('check3');
                    Abobj.MA2_ConatctId__c= conMap.get(Abobj.Contact__c).MembershipNo__c;
                }else if(Abobj.MA2_ConatctId__c!=null  && conMap.containsKey(Abobj.MA2_ConatctId__c)){
                    System.Debug('check4');
                    Abobj.Contact__c = conMap.get(Abobj.MA2_ConatctId__c).Id;
                }
                System.Debug('Abobj-----'+Abobj);
        
        }
        
        
        
           
    }
}