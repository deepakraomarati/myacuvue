@isTest
public with sharing class MA2_NewFitAppointment_Test 
{
    static testmethod void insertAccountBooking()
    {
    
        List<AccountBooking__c> lstaccbk =new List<AccountBooking__c>();
        
        account ac = new account();
        ac.name='test';
        ac.OutletNumber__c ='123';
        insert ac;
        
        contact con =new contact();
        con.lastname='test1';
        con.MembershipNo__c ='1234';
        insert con;
        
        AccountBooking__c accbk = new AccountBooking__c();
        accbk.MA2_AccountId__c='123';
        accbk.MA2_ConatctId__c='1234';
        lstaccbk.add(accbk);
        
        AccountBooking__c accbk1 = new AccountBooking__c();
        accbk1.MA2_AccountId__c='12345';
        accbk1.MA2_ConatctId__c='123467';
        lstaccbk.add(accbk1);
        insert lstaccbk;
        
        AccountBooking__c accbk2 = new AccountBooking__c();
        accbk2.MA2_AccountId__c='123';
        accbk2.MA2_ConatctId__c='1234';
        insert accbk2;     
        system.assertEquals(accbk2.MA2_AccountId__c,'123','success');
    }
}