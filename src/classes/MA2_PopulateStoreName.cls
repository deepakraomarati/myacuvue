/**
* File Name: MA2_PopulateStoreName
* Description : Logic to populate active store from Consumer Relation onto Consumer table
* Copyright : Johnson & Johnson
* @author : Lhawang Bhutia | lbhutia@its.jnj.com | lhawang.bhutia@cognizant.com
* 
* Modification Log 
* =============================================================== **/

public class MA2_PopulateStoreName{
    
    public static void StoreName(Set<Id> ContactIds,Set<Id> AccountIds){
        
        //Query to retrieve active ECP for given consumer
        list<contact> ConsumerModifiedList = [select id, (select Id, MA2_MyECP__c,AccountId__c from CustomerRelations__r where MA2_MyECP__c = true) from Contact where Id IN: ContactIds];
        list<contact> ConsumerList = new list<Contact>();
        Map<Id,String> AccountMap = new Map<Id,String>();
        
        //Query to retrieve store name for given consumer
        for(Account item: [select Id,Name from Account where Id IN: AccountIds]){
            AccountMap.put(item.Id,item.Name);
        }
        
        //Logic to assign store name from consumer relation onto consumer table
        if(ConsumerModifiedList.size()>0){
            for(Contact Consumer: ConsumerModifiedList){
                for(Consumer_Relation__c Consumerrel: Consumer.CustomerRelations__r){
                    if(Consumerrel.MA2_MyECP__c){
                        Consumer.MA2_StoreName__c = AccountMap.get(Consumerrel.AccountId__c);
                        ConsumerList.add(Consumer);
                    }
                }
            }
        }
        
        //Logic to update consumer data
        Map<Id,Contact> contactmap = new Map<Id,Contact>();
        contactmap.putall(Consumerlist);
        
        if(contactmap.size()>0){
            update contactmap.values();
        }
    }
}