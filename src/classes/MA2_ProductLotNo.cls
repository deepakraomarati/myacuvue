public class MA2_ProductLotNo{

    public static void lotMap(list<ProductLotNo__c> lotList)
    {
        List<ProductLotNo__c> lstProduct= new List<ProductLotNo__c>();
    
        List<string> PrdId = new List<string>();
        List<string> transId = new List<string>();
        List<string> BachId = new List<string>();
        
        Map<Id,Product2 > MapProduct = new Map<Id,Product2 >();
        Map<Id,TransactionTd__c> MapTransaction = new Map<Id,TransactionTd__c>();
        Map<Id,Batch_Details__c> MapBatch = new Map<Id,Batch_Details__c>();
   
        for(ProductLotNo__c  PL:lotList){
             lstProduct.add(PL);
                 
             if(PL.MA2_ProductId__c!=null){
                 PrdId .add(PL.MA2_ProductId__c);
             }
                 
             if(PL.MA2_TransactionId__c!=null){
                 transId.add(PL.MA2_TransactionId__c);
             }
                
             if(PL.MA2_BatchId__c!=null){
                 BachId.add(PL.MA2_BatchId__c);
             }
         }
             
         if(PrdId.size()>0 && !PrdId.isEmpty())
         {
             MapProduct = new Map<Id,Product2 >([select id,UPC_Code__c from Product2 where UPC_Code__c IN:PrdId]);
         }
             
         if(transId.size()>0 && !transId.isEmpty())
         {
             MapTransaction = new Map<Id,TransactionTd__c>([select id,TransactionId__c  from TransactionTd__c where id IN:transId]);
         }
             
         if(BachId.size()>0 && !BachId.isEmpty())
         {
              MapBatch = new Map<Id,Batch_Details__c>([select id,Batch_Number__c from Batch_Details__c where Batch_Number__c IN:BachId]);
         }
             
                  
         for(ProductLotNo__c  PNL:lstProduct)
         {
              if(MapProduct.size()>0 && !MapProduct.isEmpty() && PNL.MA2_ProductId__c!=null){
                   for(Product2  pr: MapProduct.Values()){
                        if(pr.UPC_Code__c !=null && pr.UPC_Code__c == PNL.MA2_ProductId__c){
                              PNL.MA2_Product__c= pr.Id;
                        }
                   }
               }
                 
                        
              if(MapTransaction.size()>0 && !MapTransaction.isEmpty() && PNL.MA2_TransactionId__c!=null){
                  for(TransactionTd__c tr: MapTransaction.values()){
                        // if(tr.TransactionId__c !=null && tr.TransactionId__c ==PNL.MA2_TransactionId__c)
                        if(tr.id !=null && tr.id ==PNL.MA2_TransactionId__c){
                              PNL.MA2_Transaction__c= tr.Id;
                           }
                   }
              }
                 
              if(MapBatch.size()>0 && !MapBatch.isEmpty() && PNL.MA2_BatchId__c!=null){
                   for(Batch_Details__c B: MapBatch.values()){
                       if(B.Batch_Number__c !=null && B.Batch_Number__c ==PNL.MA2_BatchId__c){
                          PNL.MA2_BatchDetails__c= B.Id;
                       }
                   }
              }
         }

    }
}