@isTest
public with sharing class MA2_ProductLotNo_Test 
{
    static testmethod void insertBlockHours()
    {
        Product2 prod=new Product2();
        prod.name='test';
        prod.UPC_Code__c='12345';
        
        Batch_Details__c btc=new Batch_Details__c();
        btc.Batch_Number__c ='1234';
        
        TransactionTd__c trans=new TransactionTd__c();
        trans.TransactionId__c='123';
        
        ProductLotNo__c pln=new ProductLotNo__c();
        pln.MA2_BatchDetails__c=btc.id;
        pln.MA2_Transaction__c=trans.id;
        pln.MA2_Product__c=prod.id;
        pln.MA2_ProductId__c=prod.UPC_Code__c;
        pln.MA2_TransactionId__c=trans.TransactionId__c;
        pln.MA2_BatchId__c=btc.Batch_Number__c;
        insert pln;
        
        system.assertEquals(pln.MA2_BatchId__c,btc.Batch_Number__c,'success');
    }
}