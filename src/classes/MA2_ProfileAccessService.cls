public class MA2_ProfileAccessService{
    
    /*
     * Method for creating json body 
     */
    public static void sendData(List<MA2_ProfileAccess__c> profileList){
    list<MA2_ProfileAccess__c> ListPofile=[select id,MA2_AccessLevel__c,MA2_UserProfile__c,CreatedDate,LastmodifiedDate,MA2_FunctionAccess__c,MA2_UserProfile__r.id from MA2_ProfileAccess__c where id in:profileList];
        String jsonString  = '{"profileAccess": [';
        
        if(profileList.size() > 0){
        
        system.debug('ListPofile-->>>>>>>>>>-'+ListPofile);
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            for(MA2_ProfileAccess__c pa : ListPofile){
             // gen.writeStringField('Id',pa.ID);
              gen.writeStringField('accessLevel',pa.MA2_AccessLevel__c+'');
              gen.writeStringField('functionAccess',pa.MA2_FunctionAccess__c+'');
              gen.writeStringField('userProfileId',pa.MA2_UserProfile__r.id+'');
              gen.writeStringField('profileAccessId',pa.id+''); 
              gen.writeStringField('createddate',pa.CreatedDate+'');
              gen.writeStringField('lastmodifieddate',pa.LastmodifiedDate+'');   
            }
            gen.writeEndObject();
            jsonString = jsonString + gen.getAsString() + ']}';
        }
        System.Debug('jsonString--'+jsonString);
        if(jsonString != ''){
            sendtojtracker(jsonString);
        }
    }
    
    /*
     * Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONObject class
     */
    @future(callout = true)
    public static void sendtojtracker(String jsonBody) {
        JSONObject oauth = null;
       Credientials__c testPub = new Credientials__c();
       if(Credientials__c.getInstance('ProfileAccess') != null){
           testPub  = Credientials__c.getInstance('ProfileAccess');
           if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
                       final String ClientId = testPub.Client_Id__c;
                       final String ClientSecret = testPub.Client_Secret__c;
                       final String TargetUrl = testPub.Target_Url__c;
                       final String ApiKey = testPub.Api_Key__c; 
                       oauth = oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody);
           }
       }  
       /*SonarQube Fix*/	   
       //System.debug('------oauth response------>>>>'+oauth);
        
    }
    
    /*
     * Method for sending data to the Apigee system
     */
    private static JSONObject oauthLogin(String targetUrl, String clientId, String clientSecret, String apiKeyValue,String jsonBody) {
      HttpRequest loginRequest = New HttpRequest();
      loginRequest.setMethod('POST');
      loginRequest.setEndpoint(targetUrl);
      loginRequest.setHeader('grant_type', 'authorization_code');
      loginRequest.setHeader('client_id',clientId);
      loginRequest.setHeader('client_secret',clientSecret);
      loginRequest.setHeader('apikey',apiKeyValue);
      loginRequest.setHeader('Content-Type', 'application/json');
     
      system.debug('<<<<jsonString>>>>>'+jsonBody);

      loginRequest.setBody(jsonBody);
      Http Http = New Http();
      HTTPResponse loginResponse = new HTTPResponse();
      if ( !Test.isRunningTest() ){
          loginResponse = http.send(loginRequest);
          JSONObject oAuth = (JSONObject) JSON.deserialize(loginResponse.getbody(), JSONObject.class);
          return oAuth;
      }
      
      System.Debug('loginResponse --'+loginResponse.getBody());
    
      return null;
 }
 
 // Inner class for setting value 
 public class JSONObject {
      public String id {
       get;
       set;
      }
      public String issued_at {
       get;
       set;
      }
      public String instance_url {
       get;
       set;
      }
      public String signature {
       get;
       set;
      }
      public String access_token {
       get;
       set;
      }
 }
 

}