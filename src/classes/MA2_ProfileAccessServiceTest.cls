@isTest
public class MA2_ProfileAccessServiceTest
{
    static testMethod void checktestMethod() 
    {
        TestDataFactory_MyAcuvue.insertCustomSetting();
        final List<MA2_UserProfile__c> userProfList = new List<MA2_UserProfile__c>();
        userProfList.addAll(TestDataFactory_MyAcuvue.insertUserProfile(1));
        MA2_ProfileAccess__c profile = new MA2_ProfileAccess__c();
        profile.MA2_AccessLevel__c = 'Read';
        profile.MA2_FunctionAccess__c = 'User Management';
        profile.MA2_UserProfile__c = userProfList[0].Id;
        insert profile;
        
        profile.MA2_AccessLevel__c = 'Create';
        update profile;
		system.assertEquals(profile.MA2_AccessLevel__c, 'Create', 'success');
        
    }
 }