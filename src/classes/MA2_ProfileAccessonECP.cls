/**
* File Name: MA2_ProfileAccessonECP
* Description : Class for Triggering "MA2_Profile_Access_Approval" Approval Process on Contact
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | neel.kamal@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |11-Sep-2018  |nkamal8@its.jnj.com  |New Class created
*/
public class MA2_ProfileAccessonECP {
    Public Static Void ProfileAccessonECP(List<MA2_RelatedAccounts__c> ecpList){
        Set<String> ecpContact = new Set<String>();
        Set<String> ecpAccount = new Set<String>();
        List<Contact> prouserList = new List<Contact>();
        Map<Id,Integer> storeecpMap = new Map<Id,Integer>();
        Map<String,String> accownerMap = new Map<String,String>();
        List<Contact> conList = new List<Contact>();
        
        //Webservice MyAcuvue user as requestee
        String ids = [select id from user where name like '%Webservice API MyAcuvue%' limit 1].id;
        if(ecpList.size()>0){
            for(MA2_RelatedAccounts__c ecp : ecpList){
                if(ecp.CreatedById == ids && ecp.MA2_Contact__c != null && ecp.MA2_Account__c != null && ecp.MA2_Status__c)
                {
                    ecpContact.add(ecp.MA2_Contact__c);
                    ecpAccount.add(ecp.MA2_Account__c);
                    System.Debug('ecpContact-->>'+ecpContact);
                    System.Debug('ecpAccount-->>'+ecpAccount);
                }
            } 
        }
        Id ecpRecord = [select id from recordtype where name = 'ECP' and sObjectType = 'Contact'].id;
        System.Debug('ecpRecord-->>'+ecpRecord);
        prouserList = [select id,AccountId,(select id,MA2_Account__c,MA2_Contact__c,MA2_CountryCode__c,MA2_Status__c from Related_Accounts__r
                                                       where (MA2_CountryCode__c = 'SGP' OR MA2_CountryCode__c = 'TWN' OR MA2_CountryCode__c = 'HKG')) from contact where 
                       RecordTypeId =:ecpRecord AND id in : ecpContact];
        
        System.Debug('prouserList-->>'+prouserList);
        list<Account> Acclist = [select id,ownerid from Account where id in : ecpAccount];
        for(Account acc : Acclist){
            accownerMap.put(acc.id,acc.OwnerId);
            System.Debug('accownerMap-->>'+accownerMap);
        }
        if(prouserList.size()>0){
            
            for(Contact con : prouserList){
                storeecpMap.put(con.id,con.Related_Accounts__r.size());
                System.Debug('storeecpMap-->>'+storeecpMap);
            }
        }
        for(MA2_RelatedAccounts__c coList : ecpList){
            if(storeecpMap.containskey(coList.MA2_Contact__c)){
                if(accownerMap.containskey(coList.MA2_Account__c)){
                    System.Debug('storeecpMap2--->>'+storeecpMap);
                    if(!(storeecpMap.get(coList.MA2_Contact__c)>=2)){
                        Contact co = new Contact();
                        co.id = coList.MA2_Contact__c;
                        co.AccountID = coList.MA2_Account__c;
                        //co.MA2_AccessStatus__c = 'Sent for Approval';
                        co.MA2_Approver__c = accownerMap.get(coList.MA2_Account__c);
                        conList.add(co);
                        System.Debug('conList>>-->>'+conList);
                    }
                }
            }
        }
        if(conList.size()>0){
            update conList;
            ApprovalApexonContact.submitapproval(conList);
        }
    }
    
}