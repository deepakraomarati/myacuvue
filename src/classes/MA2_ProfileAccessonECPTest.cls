/**
* File Name: MA2_ProfileAccessonECPTest
* Description : Test Class for MA2_ProfileAccessonECP
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | neel.kamal@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |11-Sep-2018  |nkamal8@its.jnj.com  |New Class created
*/
@isTest
public class MA2_ProfileAccessonECPTest {
    static Testmethod void ecpAccessTest(){
        final string Name = 'SGP';
        final string CountryCurrency = 'SGD';
        final string AccountId = '123456';
        Map<String,ID> profiles = new Map<String,ID>();
        List<User> ids = [select id,alias,email,emailencodingkey,lastname,languagelocalekey,localesidkey,profileid,timezonesidkey,
                      username from user where name like '%Webservice API MyAcuvue%' limit 1];
        List<MA2_RelatedAccounts__c> raList = new List<MA2_RelatedAccounts__c>();
        final TriggerHandler__c cred = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                             GetTransactionTd__c = true, MA2_createUpdateTransaction__c = true, CouponContact__c= true, MA2_CouponInactiveTrigger__c = true,
                                                             MA2_CouponToApigeeTrigger__c= true, MA2_CouponWalletForApigee__c= true, MA2_createUpdateTransactionProd__c= true );
        insert cred;
        
        MA2_CountryCode__c CountryCode = new MA2_CountryCode__c(Name= Name, MA2_CountryCodeValue__c = Name);
        insert CountryCode;
        final MA2_Country_Currency_Map__c CCM = new MA2_Country_Currency_Map__c(Name = Name, Currency__c = CountryCurrency);
        insert CCM;
        final Id consumerRecTypeId2 = [select id from recordtype where name = 'ECP' and sObjectType = 'Contact'].id;
        final Account acc = new Account(Name = 'Test' , MA2_AccountId__c = AccountId ,AccountNumber = '54321', ownerid = ids[0].id,
                                        Marketing_Program__c = 'AEC',OutletNumber__c = '54321',ActiveYN__c = true,
                                        My_Acuvue__c = 'Yes',CountryCode__c = 'SGP',PublicZone__c = 'Testtt');
        
        insert acc;
        system.assertEquals(acc.Name, 'Test'); 
        System.runAs(ids[0]){
        final Contact con2 = new Contact(LastName = 'test' ,MA2_AccountId__c = acc.id ,MA2_ContactId__c = '23456',
                                         RecordTypeId  = consumerRecTypeId2  ,
                                         AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = CountryCurrency, 
                                         MembershipNo__c = 'SGP123', MA2_Country_Code__c = Name, MA2_Grade__c='Base', DOB__c = date.newinstance(2012,03,03),
                                         MA2_PreAssessment__c = true,MA2_AccessStatus__c = 'Pending');
        
        insert con2;
        final MA2_RelatedAccounts__c ra = new MA2_RelatedAccounts__c(MA2_Account__c = acc.id,MA2_Contact__c = con2.id,MA2_Status__c = True);
        insert ra;
        raList.add(ra);
        }
        Test.startTest();
        MA2_ProfileAccessonECP.ProfileAccessonECP(raList);
        Test.stopTest();
    }
}