/**
* File Name: MA2_RelatedAccountTest
* Description : Test class for Related Account
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |8-Sep-2016  |hsingh53@its.jnj.com  |New Class created
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    2.0  |30-Aug-2017 |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_RelatedAccountTest{
    
    /* Method for excuting the test class */
    static Testmethod void runBatchJobMethod(){
        TestDataFactory_MyAcuvue.insertCustomSetting();
        final List<Contact> contactList = new List<Contact>();
        final List<Account> accountList = new List<Account>();
        final List<MA2_RelatedAccounts__c> relatedAccList = new List<MA2_RelatedAccounts__c>();
        Test.startTest();
        accountList.addAll(TestDataFactory_MyAcuvue.createAccount(1));
        system.assertEquals(accountList.size(), 4, 'Success');                              
        
        contactList.addAll(TestDataFactory_MyAcuvue.createECPContact(1,accountList));
        relatedAccList.addAll(TestDataFactory_MyAcuvue.insertRelatedAccount(1,accountList,
                                                                            contactList,TestDataFactory_MyAcuvue.insertUserProfile(1)));
        final MA2_RelatedAccounts__c relAcc = new MA2_RelatedAccounts__c();
        relAcc.Id = relatedAccList[0].Id;
        relAcc.MA2_Account__c = accountList[2].Id;
        update relAcc; 
        
        Test.stopTest();
    }
    
    static Testmethod void runBatchJobMethod1(){
        TestDataFactory_MyAcuvue.insertCustomSetting();
        final List<Contact> contactList = new List<Contact>();
        final List<Account> accountList = new List<Account>();
        final List<MA2_RelatedAccounts__c> relatedAccList = new List<MA2_RelatedAccounts__c>();
        Test.startTest();
        accountList.addAll(TestDataFactory_MyAcuvue.createAccount(1));
        system.assertEquals(accountList.size(), 4, 'Success');                              
        
        contactList.addAll(TestDataFactory_MyAcuvue.createECPContact(1,accountList));
        relatedAccList.addAll(TestDataFactory_MyAcuvue.insertRelatedAccount(1,accountList,
                                                                            contactList,TestDataFactory_MyAcuvue.insertUserProfile(1)));
        
        delete relatedAccList[0]; 
        
        Test.stopTest();
    }
}