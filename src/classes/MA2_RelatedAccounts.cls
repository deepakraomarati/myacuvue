public class MA2_RelatedAccounts{

    public static void apigeeMap(list<MA2_RelatedAccounts__c> relatedList)
    {
         List<String> lstAccId= new List<String>();
         List<String> lstConId= new List<String>();
         List<String> lstUserId=new List<String>();
         
         List<MA2_RelatedAccounts__c> lstaccbokobj= new List<MA2_RelatedAccounts__c >();
          
         Map<id,Account> MapAccount=new map<id,Account>();
         Map<id,Contact> MapContact=new map<id,Contact>();
         Map<id,MA2_UserProfile__c> MapUserProfile=new map<id,MA2_UserProfile__c>();
            
            for(MA2_RelatedAccounts__c  Abobj:relatedList)
            {
                lstaccbokobj.add(Abobj);
            
                if(Abobj.MA2_AccountId__c!=null){
                    lstAccId.add(Abobj.MA2_AccountId__c);
                }
            
                if(Abobj.MA2_ContactId__c!=null){
                    lstConId.add(Abobj.MA2_ContactId__c);
                }
                
                if(Abobj.MA2_ProfileId__c!=null){
                    lstUserId.add(Abobj.MA2_ProfileId__c);
                }  
            }
          
            if(lstAccId.size()>0 && !lstAccId.isEmpty())
            {    
                MapAccount=new Map<Id,Account>([select id,Name,OutletNumber__c from account where OutletNumber__c IN:lstAccId]);
            } 
             
            if(lstConId.size()>0 && !lstConId.isEmpty())
            {
                MapContact =new Map<Id,Contact>([select id,LastName,MembershipNo__c from contact where MembershipNo__c IN:lstConId]);
            }
            
            if(lstUserId.size()>0 && !lstUserId.isEmpty())
            {
                MapUserProfile=new Map<Id,MA2_UserProfile__c>([select Id,Name from MA2_UserProfile__c where Id IN:lstUserId]);
            }   
           
            Map<String,Id> mapAccountIds= new Map<String,Id>();
            for(Account acc:MapAccount.Values()){
                mapAccountIds.put(acc.OutletNumber__c,acc.Id);
            }
             
             
            Map<String,Id> mapContactIds= new Map<String,Id>();
            for(Contact con: MapContact.Values()){
                mapContactIds.put(con.MembershipNo__c,con.Id);
            }
            
            Map<Id,Id> mapProfileIds= new Map<Id,Id>();
            for(MA2_UserProfile__c prof: MapUserProfile.Values()){
                mapProfileIds.put(prof.Id,prof.Id);
            }   
             
             
            List<MA2_RelatedAccounts__c> lstAccountBooking = new List<MA2_RelatedAccounts__c>();
    
            for(MA2_RelatedAccounts__c  ab:lstaccbokobj)
            {
                if(ab.MA2_AccountId__c!=null && mapAccountIds.get(ab.MA2_AccountId__c)!=null){
                     ab.MA2_Account__c= mapAccountIds.get(ab.MA2_AccountId__c);
                       
                }
               if(ab.MA2_ContactId__c!=null && mapContactIds.get(ab.MA2_ContactId__c)!=null){
                     ab.MA2_Contact__c= mapContactIds.get(ab.MA2_ContactId__c); 
                }  
                if(ab.MA2_ProfileId__c!=null && mapProfileIds.get(ab.MA2_ProfileId__c)!=null){
                     ab.MA2_UserProfile__c= mapProfileIds.get(ab.MA2_ProfileId__c);
                }  
                lstAccountBooking.add(ab);
            }
        }
    }