@isTest
public class MA2_RelatedAccountsTest{   
    static Testmethod void createAppointmentTest(){
        
        TestDataFactory_MyAcuvue.insertCustomSetting();
        account ac = new account();
        ac.name='test';
        ac.OutletNumber__c ='123';
        insert ac;
        
        contact con =new contact();
        con.lastname='test1';
        con.MembershipNo__c ='1234';
        insert con;
        
        contact con1 =new contact();
        con1.lastname='test2';
        con1.MembershipNo__c ='12345';
        insert con1;
        
        MA2_RelatedAccounts__c relAcc = new MA2_RelatedAccounts__c();
        relAcc.MA2_Account__c = ac.Id;
        relAcc.MA2_Contact__c = con.Id;
        relAcc.MA2_ContactId__c = con.MembershipNo__c;       
        insert relAcc;
        system.assertEquals(relAcc.MA2_ContactId__c,con.MembershipNo__c,'success');
    }
}