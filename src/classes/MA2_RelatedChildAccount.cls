public class MA2_RelatedChildAccount{
    public static void afterInsert(List<MA2_RelatedAccounts__c> relatedAccList){
        insertChildAccount(relatedAccList);    
    }
    
    public static void afterUpdate(List<MA2_RelatedAccounts__c> relatedAccList){
        deleteRelatedChildAcc(relatedAccList);
        insertChildAccount(relatedAccList);    
    }
    
    public static void beforeDelete(List<MA2_RelatedAccounts__c> relatedAccList){
        List<MA2_RelatedAccounts__c> relatedAccountList = [select MA2_Contact__c,
                                                                MA2_Account__c,MA2_Account__r.OutletNumber__c,
                                                                MA2_Account__r.AccountNumber,
                                                                MA2_UserProfile__c from 
                                                                MA2_RelatedAccounts__c where Id in: relatedAccList
                                                                and MA2_UserProfile__r.MA2_ProfileName__c =: 'HQ'
                                                                and MA2_Account__r.ActiveYN__c = true and MA2_Account__r.My_Acuvue__c = 'Yes'];
        List<String> relAccountNumList = new List<String>();
        List<String> relContactList = new List<String>();
        List<String> relAccountNumEqualList = new List<String>();
        for(MA2_RelatedAccounts__c relAcc : relatedAccountList){
            System.debug('relAcc.MA2_Account__r.OutletNumber__c--'+relAcc.MA2_Account__r.OutletNumber__c);
            System.debug('relAcc.MA2_Account__r.AccountNumber--'+relAcc.MA2_Account__r.AccountNumber);
            if(relAcc.MA2_Account__r.OutletNumber__c != relAcc.MA2_Account__r.AccountNumber){
                relAccountNumList.add(relAcc.MA2_Account__r.AccountNumber);
            }else if(relAcc.MA2_Account__r.OutletNumber__c == relAcc.MA2_Account__r.AccountNumber){
                relAccountNumEqualList.add(relAcc.MA2_Account__r.OutletNumber__c);
            }
            if(relAcc.Ma2_contact__c != null){
                relContactList.add(relAcc.Ma2_contact__c);
            }
        }
        System.Debug('relAccountNumList--'+relAccountNumList);
        if(relAccountNumEqualList.size() != 0 && relContactList.size() != 0){
            deleteChildAccountRecord(relAccountNumEqualList,relContactList);
        }
        if(relAccountNumList.size() != 0 && relContactList.size() != 0){
            checkParentRelatedAccount(relAccountNumList,relContactList,relatedAccountList,relatedAccList);  
        }  
    }
    
    
    private static void deleteChildAccountRecord(List<String> relAccountNumEqualList,List<String> relContactList){
        List<MA2_RelatedAccounts__c> relatedAccountDeleteList = new List<MA2_RelatedAccounts__c>();
        List<MA2_RelatedAccounts__c> relatedAccountList = [select MA2_Contact__c,
                                                                MA2_Account__c,MA2_Account__r.OutletNumber__c,
                                                                MA2_Account__r.AccountNumber,
                                                                MA2_UserProfile__c from 
                                                                MA2_RelatedAccounts__c 
                                                                where MA2_Account__r.AccountNumber in: relAccountNumEqualList
                                                                and MA2_Contact__c in: relContactList
                                                                and MA2_UserProfile__r.MA2_ProfileName__c =: 'HQ' and MA2_Account__r.ActiveYN__c = true and MA2_Account__r.My_Acuvue__c = 'Yes'];  
        for(MA2_RelatedAccounts__c relAcc : relatedAccountList){
            if(relAcc.MA2_Account__r.OutletNumber__c != relAcc.MA2_Account__r.AccountNumber){
                relatedAccountDeleteList.add(relAcc);
            }        
        }  
        if(relatedAccountDeleteList.size() != 0 ){
            delete relatedAccountDeleteList;
        }
    }
    
    private static void checkParentRelatedAccount(List<String> relatedAccNumberList,List<String> relContactList,List<MA2_RelatedAccounts__c> relatedAccList,List<MA2_RelatedAccounts__c> deletedAccList){
        List<MA2_RelatedAccounts__c> relatedAccountList = [select MA2_Contact__c,
                                                                MA2_Account__c,MA2_Account__r.OutletNumber__c,
                                                                MA2_Account__r.AccountNumber,
                                                                MA2_UserProfile__c from 
                                                                MA2_RelatedAccounts__c where MA2_Account__r.OutletNumber__c in: relatedAccNumberList
                                                                and MA2_Contact__c in: relContactList
                                                                and MA2_UserProfile__r.MA2_ProfileName__c =: 'HQ' and MA2_Account__r.ActiveYN__c = true and MA2_Account__r.My_Acuvue__c = 'Yes'];
                                                                
       Map<String,String> relatedAccMap = new  Map<String,String>();
       for(MA2_RelatedAccounts__c relAcc : relatedAccountList){
           if(relAcc.MA2_Account__r.OutletNumber__c == relAcc.MA2_Account__r.AccountNumber){
               relatedAccMap.put(relAcc.MA2_Account__r.OutletNumber__c,'Same');
           }
       }
       System.Debug('relatedAccMap--'+relatedAccMap);
       for(MA2_RelatedAccounts__c relAcc : deletedAccList){
           for(MA2_RelatedAccounts__c relAccount : relatedAccList){
               if(relAcc.Id ==  relAccount.Id){
                   System.debug('relAcc.MA2_Account__r.OutletNumber__c--'+relAccount.MA2_Account__r.OutletNumber__c);
                    System.debug('relAcc.MA2_Account__r.AccountNumber--'+relAccount.MA2_Account__r.AccountNumber);
                   if(relAccount.MA2_Account__r.OutletNumber__c != relAccount.MA2_Account__r.AccountNumber){
                       if(relatedAccMap.containsKey(relAccount.MA2_Account__r.AccountNumber)){
                           relAcc.addError('Cannot remove the User from child accounts');
                       }    
                   }
               }  
           }
       }    
    }
    
    private static void deleteRelatedChildAcc(List<MA2_RelatedAccounts__c> relatedAccList){
        List<MA2_RelatedAccounts__c> deleteRelatedChildAccount = new List<MA2_RelatedAccounts__c>();
        List<MA2_RelatedAccounts__c> relatedAccountNumberList = [select MA2_Contact__c,
                                                                MA2_Account__c,MA2_Account__r.OutletNumber__c,
                                                                MA2_Account__r.AccountNumber,
                                                                MA2_UserProfile__c from 
                                                                MA2_RelatedAccounts__c where Id in: relatedAccList
                                                                and MA2_UserProfile__r.MA2_ProfileName__c =: 'HQ' and MA2_Account__r.ActiveYN__c = true and MA2_Account__r.My_Acuvue__c = 'Yes'];    
        for(MA2_RelatedAccounts__c relAcc : relatedAccountNumberList){
            if(relAcc.MA2_Account__r.OutletNumber__c != relAcc.MA2_Account__r.AccountNumber){
                deleteRelatedChildAccount.add(relAcc);    
            }
        }
        if(deleteRelatedChildAccount.size() != 0){
            delete deleteRelatedChildAccount;
        }
    }
    
    private static void insertChildAccount(List<MA2_RelatedAccounts__c> relatedAccList){
        List<MA2_RelatedAccounts__c> createRelatedChildAccount = new List<MA2_RelatedAccounts__c>();
        
        List<MA2_RelatedAccounts__c> relatedAccountNumberList = [select MA2_Contact__c,
                                                                MA2_Account__c,MA2_Account__r.OutletNumber__c,
                                                                MA2_UserProfile__c from 
                                                                MA2_RelatedAccounts__c where Id in: relatedAccList
                                                                and MA2_UserProfile__r.MA2_ProfileName__c =: 'HQ' and MA2_Account__r.ActiveYN__c = true and MA2_Account__r.My_Acuvue__c = 'Yes'];
        List<String> outletNumAccList = new List<String>();
        List<String> accountList = new List<String>();
        
        if(relatedAccountNumberList.size() != 0){
            for(MA2_RelatedAccounts__c relAcc : relatedAccountNumberList){
                if(relAcc.MA2_Account__r.OutletNumber__c != null){
                    accountList.add(relAcc.MA2_Account__r.OutletNumber__c);    
                }
            }
        }
        
        if(accountList.size() != 0){
            createRelatedChildAccount.addAll(insertBasedAccountNumber(accountList,relatedAccountNumberList));    
        }
        System.Debug('createRelatedChildAccount--'+createRelatedChildAccount);
        if(createRelatedChildAccount.size() != 0){
            insert createRelatedChildAccount;
        }
    }
     
    private static List<MA2_RelatedAccounts__c> insertBasedAccountNumber(List<String> accountList,List<MA2_RelatedAccounts__c> relatedAccountNumberList){
        List<MA2_RelatedAccounts__c> createRelatedChildAccount = new List<MA2_RelatedAccounts__c>();
        List<Account> accList = [select Id,AccountNumber from Account where AccountNumber in: accountList and OutletNumber__c not in: accountList
                                 and ActiveYN__c = true and My_Acuvue__c = 'Yes'];
        if(accList.size() != 0){
            for(MA2_RelatedAccounts__c relAcc : relatedAccountNumberList){
                for(Account acc : accList){
                    if(relAcc.MA2_Account__r.OutletNumber__c == acc.AccountNumber){
                        MA2_RelatedAccounts__c relChildAccount =  new MA2_RelatedAccounts__c();
                        relChildAccount.MA2_UserProfile__c =  relAcc.MA2_UserProfile__c;
                        relChildAccount.MA2_Contact__c =  relAcc.MA2_Contact__c;
                        relChildAccount.MA2_Account__c =  acc.Id;
                        relChildAccount.MA2_Status__c =  true;
                        //relChildAccount.MA2_UserProfile__c =  relAcc.MA2_UserProfile__c;
                        //relChildAccount.MA2_UserProfile__c =  relAcc.MA2_UserProfile__c;
                        createRelatedChildAccount.add(relChildAccount);
                    }
                }
            }   
        }
        return createRelatedChildAccount;
    }
}