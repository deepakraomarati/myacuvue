public class MA2_SFDCUpdateRoleService{
    
    /*
     * Method for creating json body 
     */
    public static void sfdcsendData(List<MA2_RelatedAccounts__c> relatedList){
        List<MA2_RelatedAccounts__c> relatedAccConList = new List<MA2_RelatedAccounts__c>();
        List<MA2_RelatedAccounts__c> relatedAccConFilterList = new List<MA2_RelatedAccounts__c>();
        Set<MA2_RelatedAccounts__c> filteRelatedAccConList = new Set<MA2_RelatedAccounts__c>();
        
        for(MA2_RelatedAccounts__c relatedAcc : relatedList){
            if(relatedAcc.MA2_AccountId__c != null){
                filteRelatedAccConList.add(relatedAcc);
            }
        }
        relatedAccConList = [select MA2_Contact__r.Name,MA2_Contact__r.MobilePhone,MA2_Account__r.OutletNumber__c,MA2_Contact__c,MA2_Contact__r.MembershipNo__c,MA2_Account__r.Name,
                            MA2_Account__c,LastmodifiedBy.Name,MA2_UserProfile__c,MA2_Status__c,MA2_UserProfile__r.id,
                            MA2_UserProfile__r.MA2_ProfileName__c,CreatedBy.Name
                            from MA2_RelatedAccounts__c where Id in: filteRelatedAccConList limit 1000];
                                
        System.Debug('relatedAccConList---------'+relatedAccConList);                     
        if(relatedAccConList.size() > 0){
            for(MA2_RelatedAccounts__c relAcc : relatedAccConList){
                String userName = relAcc.LastmodifiedBy.Name.tolowerCase();
                if(!userName.contains('web')){
                    relatedAccConFilterList.add(relAcc);
                }
            }
        }
        String jsonString  = '';
        if(relatedAccConFilterList.size() > 0){
            jsonString  = '[';
        system.debug('relatedList-->>>>>>>>>>-'+relatedAccConFilterList);
            for(MA2_RelatedAccounts__c ra : relatedAccConFilterList){
             JSONGenerator gen = JSON.createGenerator(true);
             gen.writeStartObject();
              //gen.writeStringField('userId',ra.MA2_Contact__c+'');
              gen.writeStringField('Id',ra.id+'');
              gen.writeStringField('userId',ra.MA2_Contact__r.MembershipNo__c+'');
              gen.writeStringField('mobile',ra.MA2_Contact__r.MobilePhone+'');
               gen.writeStringField('userName',ra.MA2_Contact__r.Name+'');
               gen.writeStringField('userProfile',ra.MA2_UserProfile__r.id+'');
               gen.writeStringField('role',ra.MA2_UserProfile__r.MA2_ProfileName__c+'');
               gen.writeStringField('storeId',ra.MA2_Account__r.OutletNumber__c+'');
               gen.writeStringField('storeName',ra.MA2_Account__r.Name+'');
               gen.writeStringField('status',ra.MA2_Status__c+'');
          
               //gen.writeStringField('createdDate',ra.CreatedDate+'');
               //gen.writeStringField('lastModifiedDate',ra.LastmodifiedDate+''); 
               gen.writeEndObject();
               jsonString = jsonString + gen.getAsString() + ',';
            }
                jsonString = jsonString.subString(0,jsonString.length() - 1);
                jsonString = jsonString +  + ']';
        }
        System.Debug('jsonString--'+jsonString);
        if(jsonString != ''){
            sendtojtracker(jsonString);
        }
    }
    
    /*
     * Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONObject class
     */
    @future(callout = true)
    public static void sendtojtracker(String jsonBody) {
            JSONObject oauth = null;
       Credientials__c testPub = new Credientials__c();
       if(Credientials__c.getInstance('SFDCUpdateRole') != null){
           testPub  = Credientials__c.getInstance('SFDCUpdateRole');
           if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
                       final String ClientId = testPub.Client_Id__c;
                       final String ClientSecret = testPub.Client_Secret__c;
                       final String TargetUrl = testPub.Target_Url__c;
                       final String ApiKey = testPub.Api_Key__c; 
                       oauth = oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody);
           }
       }    
       /*SonarQube Fix*/	   
       //System.debug('------oauth response------>>>>'+oauth);
        
    }
    
    /*
     * Method for sending data to the Apigee system
     */
    private static JSONObject oauthLogin(String targetUrl, String clientId, String clientSecret, String apiKeyValue,String jsonBody) {
      HttpRequest loginRequest = New HttpRequest();
      loginRequest.setMethod('POST');
      loginRequest.setEndpoint(targetUrl);
      loginRequest.setHeader('grant_type', 'authorization_code');
      loginRequest.setHeader('client_id',clientId);
      loginRequest.setHeader('client_secret',clientSecret);
      loginRequest.setHeader('apikey',apiKeyValue);
      loginRequest.setHeader('Content-Type', 'application/json');
     
      system.debug('<<<<jsonString>>>>>'+jsonBody);

      loginRequest.setBody(jsonBody);
      Http Http = New Http();
      HTTPResponse loginResponse = new HTTPResponse();
      if ( !Test.isRunningTest() ){
          loginResponse = http.send(loginRequest);
          JSONObject oAuth = (JSONObject) JSON.deserialize(loginResponse.getbody(), JSONObject.class);
          return oAuth;
      }
      
      System.Debug('loginResponse --'+loginResponse.getBody());
    
      return null;
 }
 
 // Inner class for setting value 
 public class JSONObject {
      public String id {
       get;
       set;
      }
      public String issued_at {
       get;
       set;
      }
      public String instance_url {
       get;
       set;
      }
      public String signature {
       get;
       set;
      }
      public String access_token {
       get;
       set;
      }
 }
 

}