/**
* File Name: MA2_SFDCUpdateRoleServiceTest
* Description : Test class for MA2_SFDCUpdateRoleService
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | neel.kamal@cognizant.com
* 
* Modification Log 
* Test Class for MA2_SFDCUpdateRoleService(User Profile Object)
*    Ver  |Date        |Author                |Modification
*    1.0  |27-Jun-2018  |nkamal8@its.jnj.com  |existing class modified
*================================================================
*/
@isTest
public class MA2_SFDCUpdateRoleServiceTest {
    /*
* Method for Testing  MA2_SFDCUpdateRoleService Class
*/
    static Testmethod void userprofMethod(){
        final Credientials__c cred = new Credientials__c(Name = 'SFDCUpdateRole' , 
                                                   Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                                                   Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                                                   Target_Url__c = 'https://jnj-dev.apigee.net/v1/consumer/catalog/addcoupon');
        insert cred;
		final Credientials__c cred1 = new Credientials__c(Name = 'SFDCRoleMaster' , 
                                                         Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                                                         Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                                                         Target_Url__c = 'https://jnj-dev.apigee.net/v1/consumer/catalog/addcoupon');
        insert cred1;
        final TriggerHandler__c mcs = new TriggerHandler__c(name = 'HandleTriggers',CouponContact__c=true);
        insert mcs;
        
       List<MA2_RelatedAccounts__c> relAcc=new List<MA2_RelatedAccounts__c>();
        String countryname_HK = 'HKG';
        final Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id;
        final Account acc = new Account(Name = 'Test' , MA2_AccountId__c = '978394' ,AccountNumber = '54321',
                                        OutletNumber__c = '54321',ActiveYN__c = true,
                                        My_Acuvue__c = 'Yes',CountryCode__c = countryname_HK ,PublicZone__c = 'Testtt');
        insert acc;
        
        final Contact con2 = new Contact(LastName = 'test2' ,MA2_AccountId__c = '978394' ,MA2_ContactId__c = '23456',
                                         RecordTypeId  = consumerRecTypeId  ,
                                         AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = 'HKD', 
                                         MembershipNo__c = 'HKG12345', MA2_Country_Code__c = countryname_HK, MA2_Grade__c='Base', DOB__c = date.newinstance(2012,03,03),
                                         MA2_PreAssessment__c = true,MA2_Contact_lenses__c = 'No');
        insert con2;
        
        final MA2_UserProfile__c userpr = new MA2_UserProfile__c(MA2_CountryCode__c=countryname_HK, MA2_ProfileName__c='ECP');
        insert userpr;
        
        final MA2_RelatedAccounts__c raccnt= new MA2_RelatedAccounts__c(MA2_Account__c = acc.Id,MA2_Contact__c = con2.id,MA2_UserProfile__c = userpr.id);
        
        insert raccnt;
        relAcc.add(raccnt);
        
        Test.startTest();
        MA2_SFDCUpdateRoleService.sfdcsendData(relAcc);
		system.assertEquals(acc.Name, 'Test', 'success');
        Test.stopTest();
        
        
        
    }
}