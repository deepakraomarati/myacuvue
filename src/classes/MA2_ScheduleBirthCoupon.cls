global class MA2_ScheduleBirthCoupon implements Schedulable{

    public void execute(SchedulableContext sc){
        
        MA2_CouponWallet.sendData([select Id from CouponContact__c where MA2_IsBatch__c = true and createdDate = Today],'schedule','insert');    
    }
}