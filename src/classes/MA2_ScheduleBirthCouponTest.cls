/*
* File Name: MA2_BirthCouponExpiryTest
* Description : Test class for MA2_BirthCouponExpiry 
* Copyright : Johnson & Johnson 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    2.0  |30-Aug-2017  |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_ScheduleBirthCouponTest
{
    static testMethod void BCTestMethod() 
    {
        Test.StartTest();
        final MA2_ScheduleBirthCoupon ScheduleCoup_new = new MA2_ScheduleBirthCoupon();
        final String sch_new = '1 1 1 1/1 * ? *';
        system.assertEquals(sch_new, '1 1 1 1/1 * ? *');                           
        final String schduledId_1 = system.schedule('Test Check_1', sch_new, ScheduleCoup_new);
        system.assertEquals(system.isScheduled(),false);	
        Test.stopTest();
    }
}