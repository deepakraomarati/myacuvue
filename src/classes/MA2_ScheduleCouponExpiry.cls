global class MA2_ScheduleCouponExpiry implements Schedulable{

    public void execute(SchedulableContext sc){
        
        MA2_CouponWallet.sendData([select Id,MA2_Country_Code__c  from CouponContact__c where lastmodifiedDate = Today and MA2_Country_Code__c != 'KOR'],'schedule','update');    
    }
    
}