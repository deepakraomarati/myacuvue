/**
* File Name: MA2_ScheduleCouponExpiryTest
* Description : Test class for MA2_ScheduleCouponExpiry
* Copyright : Johnson & Johnson
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    2.0  |30-Aug-2017 |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_ScheduleCouponExpiryTest
{
    static testMethod void expiryTestMethod() 
    {
        Test.StartTest();
        final MA2_ScheduleCouponExpiry sh1 = new MA2_ScheduleCouponExpiry();
        final String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, sh1);
        system.assertEquals(system.isScheduled(),false);	
        
        Test.stopTest();
    }
}