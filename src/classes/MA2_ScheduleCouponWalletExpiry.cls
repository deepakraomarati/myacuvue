global class MA2_ScheduleCouponWalletExpiry implements schedulable
{
    global void execute(SchedulableContext sc)
    {
        MA2_BatchJobCouponExpired batCouExp=new MA2_BatchJobCouponExpired();
        database.executebatch(batCouExp);
    }
}