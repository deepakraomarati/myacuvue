/**
* File Name: MA2_ScheduleCouponWalletExpiryTest
* Description : Test class for MA2_ScheduleCouponWalletExpiry
* Copyright : Johnson & Johnson
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    2.0  |30-Aug-2017 |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_ScheduleCouponWalletExpiryTest
{
    static testMethod void expiryTestMethod() 
    {
        Test.StartTest();
        final MA2_ScheduleCouponWalletExpiry schwallet = new MA2_ScheduleCouponWalletExpiry();
        final String sch = '0 0 22 * * ?'; 
        system.schedule('Test Check', sch, schwallet);
        system.assertEquals(system.isScheduled(),false,'Success');	
        
        Test.stopTest();
    }
 }