/**
* File Name: MA2_ScheduleConsumerWithPointsTest
* Description : Test class for MA2_ScheduleConsumerWithPoints
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | nkamal8@cognizant.com
* Modification Log 
* ================================================================
*    Ver  |Date         |Author                |Modification
*    1.0  |10-OCT-2017  |nkamal8@its.jnj.com   |Class created
*/
@isTest
public class MA2_ScheduleReportsTest {
    
    static testMethod void expiryTestMethod() {
        Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id;
        TriggerHandler__c mcs = new TriggerHandler__c(Name='HandleTriggers',MA2_createUpdateTransaction__c =true,MA2_createContact__c = True);
        insert mcs;
        Test.StartTest();
        final MA2_ScheduleReports_1stTime sh1 = new MA2_ScheduleReports_1stTime();
        final String sch1 = '0 0 23 * * ?'; 
        system.schedule('MA2_ScheduleReports_1stTime', sch1, sh1);
        system.assertEquals(system.isScheduled(),false,'success');  
        final MA2_ScheduleReports_2ndTime sh2 = new MA2_ScheduleReports_2ndTime();
        final String sch2 = '0 0 11 * * ?'; 
        system.schedule('MA2_ScheduleReports_2ndTime', sch2, sh2);
        system.assertEquals(system.isScheduled(),false,'success'); 
        Test.stopTest();
    }
}