/**
* File Name: MA2_ScheduleReports_1stTime
* Description : Batch job Schedule class for MA2_CouponWalletReport,MA2_ConsumerLadderPointVerification,MA2_CouponWithTransactions
* MA2_TransactionDetailReport,MA2_transactionproductreport
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | neel.kamal@cognizant.com
* 
* Modification Log 
* =============================================================== **/
global class MA2_ScheduleReports_1stTime implements schedulable
{
    global void execute(SchedulableContext sc)
    {
        MA2_CouponWalletReport batCouExp=new MA2_CouponWalletReport();
        database.executebatch(batCouExp);
        MA2_ConsumerLadderPointVerification batccon = new MA2_ConsumerLadderPointVerification();
        database.executebatch(batccon);
        MA2_CouponWithTransactions batctran = new MA2_CouponWithTransactions();
        database.executebatch(batctran);
        MA2_TransactionDetailReport batctran2 = new MA2_TransactionDetailReport();
        database.executebatch(batctran2);
        MA2_transactionproductreport batctran3 = new MA2_transactionproductreport();
        database.executebatch(batctran3);
    }
}