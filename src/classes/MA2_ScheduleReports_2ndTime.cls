/**
* File Name: MA2_ScheduleReports_2ndTime
* Description : Batch job Schedule class for MA2_ConsumerLadderPointVerification,MA2_CouponWithTransactions
* MA2_transactionproductreport
* Copyright : Johnson & Johnson
* @author : Lhawang Bhutia | lbhutia@its.jnj.com | lhawang.bhutia@cognizant.com
* 
* Modification Log 
* =============================================================== **/
global class MA2_ScheduleReports_2ndTime implements schedulable
{
    global void execute(SchedulableContext sc)
    {
        MA2_ConsumerLadderPointVerification first_report = new MA2_ConsumerLadderPointVerification();
        database.executebatch(first_report);
        MA2_CouponWithTransactions second_report = new MA2_CouponWithTransactions();
        database.executebatch(second_report);
        MA2_transactionproductreport third_report = new MA2_transactionproductreport();
        database.executebatch(third_report);
    }
}