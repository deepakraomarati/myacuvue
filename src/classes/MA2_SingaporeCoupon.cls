public class MA2_SingaporeCoupon{
    final public static DateTime dt = System.Now();
    public static List<CouponContact__c> assignCouponPreasseesmentChecked(Contact con , List<Coupon__c> couponETPreList){
        List<CouponContact__c> createCouponList = new List<CouponContact__c>();
        CouponContact__c couponContact = new CouponContact__c();
        System.Debug('couponETPreList--->>'+couponETPreList);
        for(Coupon__c cou : couponETPreList){
            System.Debug('cou---'+cou);
            System.Debug('cou.MA2_CountryCode__c--'+cou.MA2_CountryCode__c);
            System.Debug('con.MA2_Country_Code__c---'+con.MA2_Country_Code__c);
            if(con.MA2_Country_Code__c == 'SGP' && cou.MA2_CountryCode__c == 'SGP' && con.MA2_Contact_lenses__c != null ){
                System.Debug('check1Ali');
                CouponContact__c couponOtherContact = new CouponContact__c();
                couponOtherContact.ContactId__c = con.Id;
                couponOtherContact.AccountId__c = con.AccountId;
                couponOtherContact.ActiveYN__c = true;
                couponOtherContact.MA2_IsBatch__c = False;
                if(con.MA2_Contact_lenses__c == 'No'){
                    
                    if(cou.RecordType.Name == 'Bonus Multiplier' && cou.MA2_TimeToAward__c == 'Pre-Assessment'
                       && cou.MA2_CouponName__c == '2000 Bonus Points (Fixed Points)'){
                           System.Debug('check2');
                           System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                           Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                           if(count != null){
                               couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                           }
                           couponOtherContact.CouponId__c = cou.Id;
                           createCouponList.add(couponOtherContact);
                       }else 
                           if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                              && cou.MA2_CouponName__c == '$30 Voucher(2 Boxes)'){
                                  System.Debug('check3');
                                  System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                                  Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                  if(count != null){
                                      couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                  }
                                  couponOtherContact.CouponId__c = cou.Id;
                                  createCouponList.add(couponOtherContact);
                              }  
                    else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                            && cou.MA2_CouponName__c == '$20 Oasys 1 Day Series Reward'){
                                System.Debug('check3');
                                System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                                Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                if(count != null){
                                    couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                }
                                couponOtherContact.CouponId__c = cou.Id;
                                createCouponList.add(couponOtherContact);
                            }
                    else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                            && cou.MA2_CouponName__c == '$30 Welcome Reward'){
                                System.Debug('check3');
                                System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                                Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                if(count != null){
                                    couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                }
                                couponOtherContact.CouponId__c = cou.Id;
                                createCouponList.add(couponOtherContact);
                            }
                    else if(cou.RecordType.Name == 'Bonus Multiplier' && cou.MA2_TimeToAward__c == 'Pre-Assessment'
                            && cou.MA2_CouponName__c == '50% bonus points (DEFINE)'){
                                System.Debug('check2');
                                System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                                Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                if(count != null){
                                    couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                }
                                couponOtherContact.CouponId__c = cou.Id;
                                createCouponList.add(couponOtherContact);
                            }  
                    else if(cou.RecordType.Name == 'Bonus Multiplier' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                            && cou.MA2_CouponName__c == '2X Bonus Points (Define)'){
                                System.Debug('check2');
                                System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                                Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                if(count != null){
                                    couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                }
                                couponOtherContact.CouponId__c = cou.Id;
                                createCouponList.add(couponOtherContact);
                            }  
                }else if(con.MA2_Contact_lenses__c == 'Acuvue Brand'){
                    if(cou.RecordType.Name == 'Bonus Multiplier' && cou.MA2_TimeToAward__c == 'Pre-Assessment'
                       && cou.MA2_CouponName__c == '2000 Bonus Points (Fixed Points)'){
                           System.Debug('check2');
                           System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                           Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                           if(count != null){
                               couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                           }
                           couponOtherContact.CouponId__c = cou.Id;
                           createCouponList.add(couponOtherContact);
                       }
                    else
                        if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                           && cou.MA2_CouponName__c == '$30 Voucher(2 Boxes)'){
                               System.Debug('check2');
                               System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                               Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                               if(count != null){
                                   couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                               }
                               couponOtherContact.CouponId__c = cou.Id;
                               createCouponList.add(couponOtherContact);
                           } 
                    else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                            && cou.MA2_CouponName__c == '$20 Oasys 1 Day Series Reward'){
                                System.Debug('check3');
                                System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                                Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                if(count != null){
                                    couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                }
                                couponOtherContact.CouponId__c = cou.Id;
                                createCouponList.add(couponOtherContact);
                            }
                    else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                            && cou.MA2_CouponName__c == '$30 Welcome Reward'){
                                System.Debug('check3');
                                System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                                Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                if(count != null){
                                    couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                }
                                couponOtherContact.CouponId__c = cou.Id;
                                createCouponList.add(couponOtherContact);
                            }
                    else
                        if(cou.RecordType.Name == 'Bonus Multiplier' && cou.MA2_TimeToAward__c == 'Pre-Assessment'
                           && cou.MA2_CouponName__c == '20% bonus points (ANY ACUVUE)'){
                               System.Debug('check2');
                               System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                               Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                               if(count != null){
                                   couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                               }
                               couponOtherContact.CouponId__c = cou.Id;
                               createCouponList.add(couponOtherContact);
                           } 
                    else
                        if(cou.RecordType.Name == 'Bonus Multiplier' && cou.MA2_TimeToAward__c == 'Pre-Assessment'
                           && cou.MA2_CouponName__c == '50% bonus points (DEFINE)'){
                               System.Debug('check2');
                               System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                               Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                               if(count != null){
                                   couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                               }
                               couponOtherContact.CouponId__c = cou.Id;
                               createCouponList.add(couponOtherContact);
                           } 
                    else if(cou.RecordType.Name == 'Bonus Multiplier' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                            && cou.MA2_CouponName__c == '2X Bonus Points (Define)'){
                                System.Debug('check2');
                                System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                                Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                if(count != null){
                                    couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                }
                                couponOtherContact.CouponId__c = cou.Id;
                                createCouponList.add(couponOtherContact);
                            }  
                }else if(con.MA2_Contact_lenses__c == 'Other Brand'){
                    if(cou.RecordType.Name == 'Bonus Multiplier' && cou.MA2_TimeToAward__c == 'Pre-Assessment'
                       && cou.MA2_CouponName__c == '2000 Bonus Points (Fixed Points)'){
                           System.Debug('check2');
                           System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                           Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                           if(count != null){
                               couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                           }
                           couponOtherContact.CouponId__c = cou.Id;
                           createCouponList.add(couponOtherContact);
                       }else 
                           if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                              && cou.MA2_CouponName__c == '$20 Oasys 1 Day Series Reward'){
                                  System.Debug('check3');
                                  System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                                  Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                  if(count != null){
                                      couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                  }
                                  couponOtherContact.CouponId__c = cou.Id;
                                  createCouponList.add(couponOtherContact);
                              }
                    else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                            && cou.MA2_CouponName__c == '$30 Voucher(2 Boxes)'){
                                System.Debug('check2');
                                System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                                Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                if(count != null){
                                    couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                }
                                couponOtherContact.CouponId__c = cou.Id;
                                createCouponList.add(couponOtherContact);
                            }  
                    else if(cou.RecordType.Name == 'Cash Discount' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                            && cou.MA2_CouponName__c == '$30 Welcome Reward'){
                                System.Debug('check3');
                                System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                                Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                if(count != null){
                                    couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                }
                                couponOtherContact.CouponId__c = cou.Id;
                                createCouponList.add(couponOtherContact);
                            }  
                    else if(cou.RecordType.Name == 'Bonus Multiplier' && cou.MA2_TimeToAward__c == 'Pre-Assessment'
                            && cou.MA2_CouponName__c == '50% bonus points (DEFINE)'){
                                System.Debug('check2');
                                System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                                Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                if(count != null){
                                    couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                }
                                couponOtherContact.CouponId__c = cou.Id;
                                createCouponList.add(couponOtherContact);
                            }  
                    else if(cou.RecordType.Name == 'Bonus Multiplier' && cou.MA2_TimeToAward__c == 'Pre-Assessment'
                            && cou.MA2_CouponName__c == '20% bonus points (ANY ACUVUE)'){
                                System.Debug('check2');
                                System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                                Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                if(count != null){
                                    couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                }
                                couponOtherContact.CouponId__c = cou.Id;
                                createCouponList.add(couponOtherContact);
                            }  
                    else if(cou.RecordType.Name == 'Bonus Multiplier' && cou.MA2_TimeToAward__c == 'Post-Assessment'
                            && cou.MA2_CouponName__c == '2X Bonus Points (Define)'){
                                System.Debug('check2');
                                System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                                Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                                if(count != null){
                                    couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                                }
                                couponOtherContact.CouponId__c = cou.Id;
                                createCouponList.add(couponOtherContact);
                            }  
                }
            }
        }
        return createCouponList;
    } 
}