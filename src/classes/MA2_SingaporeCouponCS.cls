/**
* File Name: MA2_SingaporeCouponCS
* Description : class for MA2_SingaporeCouponCS
* Copyright : Johnson & Johnson
* @author : Lemu Edward | ledward4@its.jnj.com | lemu.edwardc@cognizant.com
* 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |18-Jun-2018  |ledward4@its.jnj.com  |New Class created cloned from MA2_HongKongCouponCS
*=============================================================== 
*/
public class MA2_SingaporeCouponCS {
     final public static DateTime dt = System.Now();
    public static List <CouponContact__c> assignCouponPreasseesmentChecked(Contact con , List<Coupon__c> couponETPreList){
        List<String> couponListone = new List<String>();
        Map<String,MA2_SGCouponList__c> cpnListt = MA2_SGCouponList__c.getall();
        for(MA2_SGCouponList__c sgc : cpnListt.values()){
            couponListone.add(sgc.name);
        }
        List<Coupon__c> cpnslistfirst = [select id,Name,MA2_CouponName__c,MA2_TimeToAward__c,MA2_CouponValidity__c,MA2_CountryCode__c,MA2_WelcomeCoupon__c,StatusYN__c,RecordType.Id from Coupon__c where (Name in : couponListone OR MA2_CouponName__c in : couponListone)];
        List <CouponContact__c> createCouponList = new List <CouponContact__c>();
        CouponContact__c couponContact = new CouponContact__c();
        for(Coupon__c cou : cpnslistfirst){
            System.Debug('mycpnlist-->'+cpnslistfirst);
            if(con.MA2_Country_Code__c == 'SGP' && cou.MA2_CountryCode__c == 'SGP' && con.MA2_Contact_lenses__c != null){
                CouponContact__c couponOtherContact = new CouponContact__c();
                couponOtherContact.ContactId__c = con.Id;
                couponOtherContact.AccountId__c = con.AccountId;
                couponOtherContact.ActiveYN__c = true; 
                if(con.MA2_Contact_lenses__c == 'Acuvue Brand' || con.MA2_Contact_lenses__c == 'No' || con.MA2_Contact_lenses__c == 'Other Brand'){
                    if((cou.MA2_WelcomeCoupon__c == true || cou.StatusYN__c == true) && cou.MA2_TimeToAward__c == 'Post-Assessment'){  
                        Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                           if(count != null){
                               couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                               System.Debug('ExpiryDatthmm--'+(23-couponOtherContact.ExpiryDate__c.hour()));
                               SYstem.Debug('ExpiryDattteee-->'+(couponOtherContact.ExpiryDate__c.addhours(23-couponOtherContact.ExpiryDate__c.hour()).addminutes(59-couponOtherContact.ExpiryDate__c.minute()).addSeconds(-1 * couponOtherContact.ExpiryDate__c.second())));
                           }
                           couponOtherContact.CouponId__c = cou.Id;
                           createCouponList.add(couponOtherContact);
                       }   
                }
            }
        }
        return createCouponList;
    } 
    
}