public class MA2_TWNCoupon{
        final public static DateTime dt = System.Now();
        
        public static List<CouponContact__c> assignCouponPreasseesmentChecked(Contact con , List<Coupon__c> couponETPreList){
            List<CouponContact__c> createCouponList = new List<CouponContact__c>();
            CouponContact__c couponContact = new CouponContact__c();
            for(Coupon__c cou : couponETPreList){
                System.Debug('cou---'+cou);
                System.Debug('cou.MA2_CountryCode__c--'+cou.MA2_CountryCode__c);
                System.Debug('con.MA2_Country_Code__c---'+con.MA2_Country_Code__c);
                if(con.MA2_Country_Code__c == 'TWN' && cou.MA2_CountryCode__c == 'TWN' && con.MA2_Contact_lenses__c != null){
                        System.Debug('check1Ravi');
                        CouponContact__c couponOtherContact = new CouponContact__c();
                        couponOtherContact.ContactId__c = con.Id;
                        couponOtherContact.AccountId__c = con.AccountId;
                        couponOtherContact.ActiveYN__c = true;                   
                            if(cou.MA2_TimeToAward__c == 'Post-Assessment' && cou.MA2_WelcomeCoupon__c == true){
                               System.Debug('check2');
                               System.Debug('con.MA2_Country_Code__c--'+con.MA2_Country_Code__c);
                                Integer count = Integer.valueOf(cou.MA2_CouponValidity__c);
                               if(count != null){
                                   couponOtherContact.ExpiryDate__c = dt.AddDays(count);
                               }
                               couponOtherContact.CouponId__c = cou.Id;
                               createCouponList.add(couponOtherContact);
                           }
                    
                    }
            }
            return createCouponList;
        } 
        
    }