/*
* File Name: MA2_BirthdayCoupon
* Description : Test class for MA2_BirthdayCoupon
* Copyright : Johnson & Johnson
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |17-Nov-2017  |lbhutia@its.jnj.com   |Class Modified
*/
@isTest

Public class MA2_TestTWN
{
    static testMethod void CurrencySetupTest()
    {
        TestDataFactory_MyAcuvue.insertCustomSetting();
        
        Credientials__c cred2 = new Credientials__c(Name = 'SendTransactionProduct' , 
                                                    Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                                                    Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                                                    Target_Url__c = 'https://jnj-global-test.apigee.net/sfdc/transaction/products');
        insert cred2; 
        
        TriggerHandler__c cred = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                       GetTransactionTd__c = true, MA2_createUpdateTransaction__c = true, CouponContact__c= true, MA2_CouponInactiveTrigger__c = true,
                                                       MA2_CouponToApigeeTrigger__c= true, MA2_CouponWalletForApigee__c= true, MA2_createUpdateTransactionProd__c= true );
        insert cred;
        
        MA2_CountryCode__c cc = new MA2_CountryCode__c(Name= 'TWN', MA2_CountryCodeValue__c = 'TWN');
        insert cc;
        
        MA2_CountryCode__c cc1 = new MA2_CountryCode__c(Name= 'SGP', MA2_CountryCodeValue__c = 'SGP');
        insert cc1;
        
        MA2_Country_Currency_Map__c CCM = new MA2_Country_Currency_Map__c(Name = 'TWN', Currency__c = 'TWD');
        insert CCM;
        
        MA2_Country_Currency_Map__c countryCode = new MA2_Country_Currency_Map__c(Name = 'SGP' , Currency__c = 'SGD');
        insert countryCode;
        
        Account acc = new Account(Name = 'Test' , MA2_AccountId__c = '123456' ,AccountNumber = '54321',OutletNumber__c = '54321',ActiveYN__c = true,My_Acuvue__c = 'Yes',CountryCode__c = 'SGP');
        insert acc;
        
        Id caseDiscRecTypeId = [select Id from RecordType where Name = 'Cash Discount' 
                                and sObjectType = 'Coupon__c'].Id;
        Id etrialRecordTypeId = [select Id from RecordType where Name = 'Etrial' and sObjectType = 'Coupon__c'].Id;
        Id etrialRecordTypeId2 = [select Id from RecordType where Name = 'Bonus Multiplier' and sObjectType = 'Coupon__c'].Id;
        final List<Coupon__c> coupon = new List<Coupon__c>();   
        
        coupon.add(new Coupon__c(MA2_CouponName__c = 'Etrial Multifocal',RecordTypeID = etrialRecordTypeId ,MA2_CountryCode__c ='HKG'
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
        
        coupon.add(new Coupon__c(MA2_CouponName__c = 'Etrial Moist for Astigmatism',RecordTypeID = etrialRecordTypeId ,MA2_CountryCode__c ='HKG'
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
        
        coupon.add(new Coupon__c(MA2_CouponName__c = 'Etrial Acuvue Moist',RecordTypeID = etrialRecordTypeId ,MA2_CountryCode__c ='HKG'
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
        
        coupon.add(new Coupon__c(MA2_CouponName__c = 'Etrial Define',RecordTypeID = etrialRecordTypeId ,MA2_CountryCode__c ='HKG'
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
        
        coupon.add(new Coupon__c(MA2_CouponName__c = 'Etrial Oasys 1 Day',RecordTypeID = etrialRecordTypeId ,MA2_CountryCode__c ='HKG'
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
        
        coupon.add(new Coupon__c(MA2_CouponName__c = '$20 Oasys 1 Day Series Reward',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c ='SGP'
                                 ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
        
        insert  coupon;
        
        Coupon__c coupon2 = new Coupon__c(MA2_CouponName__c = '2x Birthday Points',RecordTypeID = etrialRecordTypeId2 ,MA2_CountryCode__c ='SGP'
                                          ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Campaign', MA2_WelcomeCoupon__c = false);  
        insert coupon2;
        
        Coupon__c coupon3 = new Coupon__c(MA2_CouponName__c = '【生日禮券】雙倍點數',RecordTypeID = etrialRecordTypeId2 ,MA2_CountryCode__c ='TWN'
                                          ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Campaign', MA2_WelcomeCoupon__c = false);  
        insert coupon3;
        
        Coupon__c coupon4 = new Coupon__c(MA2_CouponName__c = '1.5x Birthday Points',RecordTypeID = etrialRecordTypeId2 ,MA2_CountryCode__c ='SGP'
                                          ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Campaign', MA2_WelcomeCoupon__c = false);  
        insert coupon4;
        
        Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id;
        
        Contact con1 = new Contact(LastName = 'test1' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',
                                   RecordTypeId  = consumerRecTypeId  ,
                                   AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = 'SGD', 	
                                   MembershipNo__c = 'SGP1234', MA2_Country_Code__c = 'SGP', MA2_Grade__c='VIP', DOB__c = date.newinstance(2017,11,11),
                                   MA2_PreAssessment__c = true);
        insert con1;
        
        Contact con2 = new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',
                                   RecordTypeId  = consumerRecTypeId  ,
                                   AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = 'TWD', 
                                   MembershipNo__c = 'SGP1236', MA2_Country_Code__c = 'TWN', MA2_Contact_lenses__c = 'Acuvue Brand', MA2_Grade__c='VIP', DOB__c = date.newinstance(2017,11,11),
                                   MA2_PreAssessment__c = true);
        insert con2;
        
        Contact con3 = new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',
                                   RecordTypeId  = consumerRecTypeId  ,
                                   AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = 'SGD', 
                                   MembershipNo__c = 'SGP1235', MA2_Country_Code__c = 'SGP', MA2_Grade__c='Base', DOB__c = date.newinstance(2017,11,11),
                                   MA2_PreAssessment__c = true);
        insert con3;
        
        CouponContact__c couponWallet3 = new CouponContact__c(ExpiryDate__c = System.Today() + 10 ,ContactId__c = con3.Id ,
                                                              CouponId__c = coupon2.Id , MA2_AccountId__c = '12345' , Apigee_Update__c = false, MA2_ContactId__c = '5555', MA2_CouponUsed__c = true, MA2_Inactive__c = true);   
        insert couponWallet3;
        
        couponWallet3.MA2_CouponUsed__c = false;
        couponWallet3.MA2_Inactive__c = false;
        
        update couponWallet3;
        List<CouponContact__c> CCList = new List<CouponContact__c>();
        CCList.add(couponWallet3);
        
        con3.MA2_PreAssessment__c  = false;
        con3.MA2_Grade__c = 'VIP';
        con3.DOB__c = system.now();
        update con3;
        
        CouponContact__c couponWallet4 = new CouponContact__c(ExpiryDate__c = System.Today() + 10 ,ContactId__c = con3.Id ,
                                                              CouponId__c = coupon2.Id , MA2_AccountId__c = '12345' , Apigee_Update__c = false, MA2_ContactId__c = '5555', MA2_CouponUsed__c = true, MA2_Inactive__c = true);   
        insert couponWallet4;
        
        couponWallet4.MA2_CouponUsed__c = false;
        couponWallet4.MA2_Inactive__c = false;
        
        update couponWallet4;
        system.assertEquals(couponWallet4.MA2_CouponUsed__c,false,'success');
        
        Set<String> countDownRateList  = new Set<String>();
        countDownRateList.add('test');
        
        Map<String,List<CouponContact__c>> coupondiminishMap = new Map<String,List<CouponContact__c>> ();
        coupondiminishMap.put('Test', CCList);
        
        MA2_CouponSelector.couponTypeList(countDownRateList,coupondiminishMap); 
        
    }    
}