/**
* File Name: MA2_TextContentService
* Description : Sending TextContent to Apigee System
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |27-Sep-2016 |hsingh53@its.jnj.com  |New Class created
*/

public class MA2_TextContentService{
    /*
     * Method for creating json body 
     */
    public static void createJsonBody(List<MA2_TextContent__c> textContentList){
        System.debug('textContentList---'+textContentList);
        if(textContentList.size() > 0){
             JSONGenerator gen = JSON.createGenerator(true);
             gen.writeStartObject();
             for(MA2_TextContent__c tc : textContentList){
                    gen.writeStringField('Id',tc.Id+'');
                    gen.writeStringField('language',tc.MA2_Language__c+'');
                    gen.writeStringField('deviceType',tc.MA2_DeviceType__c+'');
                    gen.writeStringField('version',tc.MA2_Version__c+'');
                    gen.writeStringField('contentData',tc.MA2_ContentData__c+'');
                    gen.writeStringField('contentName',tc.Name+'');
                    gen.writeStringField('contentType',tc.MA2_ContentType__c+'');
                    gen.writeStringField('countryCode',tc.MA2_CountryCode__c+'');  
                    gen.writeStringField('status',tc.MA2_Status__c+'');
                    gen.writeStringField('createdDate',tc.CreatedDate+'');
                    gen.writeStringField('lastModifiedDate',tc.LastmodifiedDate+'');    
              }
              gen.writeEndObject();
              String jsonbody = gen.getAsString();
              sendRecord(jsonbody);
              System.debug('jsonbody---'+jsonbody);
        }
    }
    
    /*
     * Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONObject class
     */   
    @future(callout = true)
    private static void sendRecord(String jsonbody){
        JSONObject oauth = null;
       Credientials__c testPub = new Credientials__c();
       if(Credientials__c.getInstance('TextContentService') != null){
           testPub  = Credientials__c.getInstance('TextContentService');
           if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
                       final String ClientId = testPub.Client_Id__c;
                       final String ClientSecret = testPub.Client_Secret__c;
                       final String TargetUrl = testPub.Target_Url__c;
                       final String ApiKey = testPub.Api_Key__c; 
                       oauth = oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody);
           }
       }  
    }
    
    /*
     * Method for sending data to the Apigee system
     */  
    private static JSONObject oauthLogin(String targetUrl ,String clientSecret ,String clientId ,String apiKeyValue ,String jsonbody){
        HTTPRequest req = new HTTPRequest();
        req.setEndpoint(targetUrl);
        req.setMethod('POST');
        req.setHeader('grant_Type','authorization_code');
        req.setHeader('client_id',clientId);
        req.setHeader('client_secret',clientSecret);
        req.setHeader('apiKey',apiKeyValue);
        req.setHeader('Content-Type', 'application/json');
        req.setBody(jsonbody);
        HTTP http = new HTTP();
        HTTPResponse response = new HTTPResponse();
        if ( !Test.isRunningTest() ){
            response = http.send(req);
            JSONObject oAuth = (JSONObject) JSON.deserialize(response.getbody(), JSONObject.class);
            return oAuth;
        }
        
        System.Debug('response--'+response.getBody());
    
      return null;
 }
 
 // Inner class for setting value 
 public class JSONObject {
      public String id {
       get;
       set;
      }
      public String issued_at {
       get;
       set;
      }
      public String instance_url {
       get;
       set;
      }
      public String signature {
       get;
       set;
      }
      public String access_token {
       get;
       set;
      }
 }
 

}