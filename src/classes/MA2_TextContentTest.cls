/**
* File Name: MA2_TextContentTest
* Description : Test class for Text Content
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |25-Sep-2016 |hsingh53@its.jnj.com  |New Class created
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    2.0  |30-Aug-2017 |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_TextContentTest{
    
    /* Method for excuting the test class */
    static Testmethod void runBatchJobMethod(){
        TestDataFactory_MyAcuvue.insertCustomSetting();
        Test.startTest();
        TestDataFactory_MyAcuvue.createTextContent(1);
        system.assertEquals(TestDataFactory_MyAcuvue.createTextContent(1).size(),2, 'Success');
        Test.stopTest();
    }
}