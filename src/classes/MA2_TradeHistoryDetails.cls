public class MA2_TradeHistoryDetails{

    public static void createTransaction(List<TradeHistory__c> tradeList){
        List<TransactionTd__c> transDetailList = new List<TransactionTd__c>();
        List<MA2_TransactionProduct__c> transProdDetailList = new List<MA2_TransactionProduct__c>();
        Map<Id,TradeHistory__c> tradeHistoryMap = new Map<Id,TradeHistory__c>();
        List<Contact> contactList = new List<Contact>();
        Set<String> tradeIdSet = new Set<String>();
        Map<String,Id> tradeMap = new Map<String,Id>();
        Set<String> contactAwsList = new Set<String>();
        Map<String,String> contactMap = new Map<String,String>();
        for(TradeHistory__c tradeRec  : tradeList){
            tradeIdSet.add(tradeRec.Id);  
            if(tradeRec.Aws_ContactId__c != null){
                contactAwsList.add(tradeRec.Aws_ContactId__c);    
            }  
        }
        
        if(contactAwsList.size() > 0){
            contactList = [select MembershipNo__c,Aws_ContactId__c from Contact where Aws_ContactId__c in: contactAwsList];
        }
        
        if(contactList.size() > 0){
            for(Contact con :  contactList){
                contactMap.put(con.Aws_ContactId__c ,con.MembershipNo__c);    
            }
        }
        
        List<TransactionTd__c> transactionList = [select Id,MA2_TradeHistoryId__c from TransactionTd__c where 
                                                  MA2_TradeHistoryId__c in: tradeIdSet];
                                                  
        for(TransactionTd__c trans : transactionList){
            tradeMap.put(trans.MA2_TradeHistoryId__c , trans.Id);    
        }
        if(tradeList.size() > 0){
            for(TradeHistory__c tradeRec : tradeList){
                if(!tradeMap.containsKey(tradeRec.Id)){
                    tradeHistoryMap.put(tradeRec.id,tradeRec);
                    TransactionTd__c transDetail = new TransactionTd__c();
                    if(tradeRec.AWS_CreatedBy__c != null && tradeRec.AWS_CreatedBy__c == 'ECP'){
                    transDetail.MA2_TransactionType__c = 'ACE_ECP';
                    }else if(tradeRec.AWS_CreatedBy__c != null && tradeRec.AWS_CreatedBy__c == 'ACMS'){
                    transDetail.MA2_TransactionType__c = 'ACE_ACMS';
                    }else if(tradeRec.AWS_CreatedBy__c != null && tradeRec.AWS_CreatedBy__c.substring(0, 5) == 'ACMS_'){
                    transDetail.MA2_TransactionType__c = 'ACE_ACMS_XXXX';
                    }
                    if(contactMap.containsKey(tradeRec.Aws_ContactId__c)){
                        transDetail.MA2_ContactId__c = contactMap.get(tradeRec.Aws_ContactId__c);
                    }
                    transDetail.MA2_AccountId__c = tradeRec.Aws_OutletNumber__c;
                    transDetail.MA2_MembershipNo__c = tradeRec.MembershipNo__c;
                    transDetail.MA2_CouponList__c = tradeRec.CouponList__c;
                    transDetail.MA2_Price__c = tradeRec.BuyAmt__c;
                    transDetail.MA2_GrossPrice__c = tradeRec.TrTotalAmt__c;
                    transDetail.MA2_PointAmt__c = tradeRec.PointAmt__c; 
                    transDetail.MA2_AcvPayAmt__c = tradeRec.AcvPayAmt__c;
                    transDetail.MA2_CouponDcAmt__c = tradeRec.CouponDcAmt__c;
                    transDetail.MA2_PaymentType__c = tradeRec.TrxStatDesc__c;
                    transDetail.MA2_Points__c = tradeRec.AccPointAmt__c;   
                    transDetail.MA2_ProductList__c = tradeRec.MA2_ProductExternalId__c;
                    transDetail.MA2_TradeHistoryId__c = tradeRec.Id;
                    transDetail.MA2_Created_Date__c = tradeRec.THD_Createddate__c;
                    transDetail.MA2_Modified_Date__c = tradeRec.lastModifiedDate;
                    transDetail.TransactionId__c = tradeRec.TransactionId__c;
                    transDetail.MA2_CountryCode__c = 'KOR';
                    transDetail.TrxStat__c = tradeRec.TrxStat__c;
                    transDetail.TrxStatDesc__c = tradeRec.TrxStatDesc__c;
                    transDetail.MA2_OutletPoints__c= tradeRec.PointBalAf__c;
                    transDetail.Aws_CombinedId__c = tradeRec.Aws_CombinedId__c;
                    transDetail.MA2_Modified_Date__c= tradeRec.AWS_Last_Modified_date__c;
                    transDetailList.add(transDetail);
                }
            }
        }
        System.Debug('transDetailList---'+transDetailList);
        if(transDetailList.size() > 0){
            insert transDetailList;
            Set<String> prodNameList = new Set<String>();
            
            for(TransactionTd__c trans : transDetailList){
                if(trans.MA2_ProductList__c != null){
                    if(trans.MA2_ProductList__c.contains(',')){
                        String[] arrayValue = trans.MA2_ProductList__c.split(',');
                        System.Debug('arrayValue ---'+arrayValue );
                        for(String rec : arrayValue){
                            prodNameList.add(rec);  
                        } 
                    }else{
                        prodNameList.add(trans.MA2_ProductList__c);    
                    }    
                }
            }
            
            Map<String,Product2> prodMapKey = new map<String,Product2>();
            List<Product2> productList = [select Name,Id,Price__c,ExternalId__c from Product2 where ExternalId__c in: prodNameList];
            System.Debug('productList--'+productList);
            for(Product2 prod : productList){
                prodMapKey.put(prod.ExternalId__c,prod);    
            }
            System.Debug('prodMapKey--'+prodMapKey );
            for(TransactionTd__c trans : transDetailList){
                if(trans.MA2_ProductList__c != null){
                    TradeHistory__c tradeRec = tradeHistoryMap.get(trans.MA2_TradeHistoryId__c);
                    if(tradeRec.MA2_ProductExternalId__c == trans.MA2_ProductList__c && trans.MA2_ProductList__c.contains(',')){
                        String[] arrayValue = trans.MA2_ProductList__c.split(',');
                        System.Debug('arrayValue ---'+arrayValue );
                        for(String rec : arrayValue){
                            if(prodMapKey.containsKey(rec)){
                                MA2_TransactionProduct__c transProdDetail = new MA2_TransactionProduct__c();
                                transProdDetail.MA2_Transaction__c = trans.Id;
                                transProdDetail.MA2_productQuantity__c = 1;
                                if(contactMap.containsKey(tradeRec.Aws_ContactId__c)){
                                    transProdDetail.MA2_ContactId__c = contactMap.get(tradeRec.Aws_ContactId__c);
                                }
                                transProdDetail.MA2_AccountId__c = tradeRec.Aws_OutletNumber__c;
                                transProdDetail.MA2_ProductId__c = rec;
                                transProdDetail.MA2_ProductName__c = prodMapKey.get(rec).Id;
                                transProdDetailList.add(transProdDetail);
                            }
                        }
                    }else if(tradeRec.MA2_ProductExternalId__c == trans.MA2_ProductList__c && !trans.MA2_ProductList__c.contains(',') && prodMapKey.get(trans.MA2_ProductList__c) != null){
                                MA2_TransactionProduct__c transProdDetail = new MA2_TransactionProduct__c();
                                transProdDetail.MA2_Transaction__c = trans.Id;
                                transProdDetail.MA2_productQuantity__c = tradeRec.productQuantity__c;
                                if(contactMap.containsKey(tradeRec.Aws_ContactId__c)){
                                    transProdDetail.MA2_ContactId__c = contactMap.get(tradeRec.Aws_ContactId__c);
                                }
                                transProdDetail.MA2_AccountId__c = tradeRec.Aws_OutletNumber__c;
                                transProdDetail.MA2_ProductId__c = tradeRec.ProductList__c;
                                transProdDetail.MA2_ProductName__c = prodMapKey.get(trans.MA2_ProductList__c).Id;
                                transProdDetailList.add(transProdDetail);
                    }
                }
            }
        }
        //System.Debug('transProdDetailList--'+transProdDetailList);
        if(transProdDetailList.size() > 0){
            insert transProdDetailList;
            //System.Debug('transProdDetailList---'+transProdDetailList);
        }
    }
    
    
    
    public static void updateTransaction(List<TradeHistory__c> tradeList){
        List<TransactionTd__c> transDetailList = new List<TransactionTd__c>();
        List<MA2_TransactionProduct__c> transProdDetailList = new List<MA2_TransactionProduct__c>();
        List<Contact> contactList = new List<Contact>();
        Set<String> tradeIdSet = new Set<String>();
        Map<String,Id> tradeMap = new Map<String,Id>();
        Set<String> contactAwsList = new Set<String>();
        Map<String,String> contactMap = new Map<String,String>();
        Map<Id,TradeHistory__c> tradeRecordMap = new Map<Id,TradeHistory__c>();
        for(TradeHistory__c tradeRec  : tradeList){
            tradeIdSet.add(tradeRec.Id);  
            if(tradeRec.Aws_ContactId__c != null){
                contactAwsList.add(tradeRec.Aws_ContactId__c);    
            }  
            tradeRecordMap.put(tradeRec.Id,tradeRec);
        }
        
        if(contactAwsList.size() > 0){
            contactList = [select MembershipNo__c,Aws_ContactId__c from Contact where Aws_ContactId__c in: contactAwsList];
        }
        
        if(contactList.size() > 0){
            for(Contact con :  contactList){
                contactMap.put(con.Aws_ContactId__c ,con.MembershipNo__c);    
            }
        }
        
        List<TransactionTd__c> transactionList = [select Id,MA2_TradeHistoryId__c from TransactionTd__c where 
                                                  MA2_TradeHistoryId__c in: tradeIdSet];
                                                  
        for(TransactionTd__c trans : transactionList){
            tradeMap.put(trans.Id,trans.MA2_TradeHistoryId__c);    
        }
        System.Debug('tradeMap--'+tradeMap);
        System.Debug('tradeRecordMap--'+tradeRecordMap);
        if(transactionList.size() > 0){
            for(TransactionTd__c transDetail : transactionList){
                if(tradeMap.containsKey(transDetail.Id)){
                    TradeHistory__c tradeRec = tradeRecordMap.get(tradeMap.get(transDetail.Id));
                    System.Debug('tradeRec---'+tradeRec);
                    if(tradeRec.AWS_CreatedBy__c != null && tradeRec.AWS_CreatedBy__c == 'ECP'){
                    transDetail.MA2_TransactionType__c = 'ACE_ECP';
                    }else if(tradeRec.AWS_CreatedBy__c != null && tradeRec.AWS_CreatedBy__c == 'ACMS'){
                    transDetail.MA2_TransactionType__c = 'ACE_ACMS';
                    }else if(tradeRec.AWS_CreatedBy__c != null && tradeRec.AWS_CreatedBy__c.substring(0, 4) == 'ACMS_'){
                    transDetail.MA2_TransactionType__c = 'ACE_ACMS_XXXX';
                    }
                    if(contactMap.containsKey(tradeRec.Aws_ContactId__c)){
                        transDetail.MA2_ContactId__c = contactMap.get(tradeRec.Aws_ContactId__c);
                    }
                    transDetail.MA2_AccountId__c = tradeRec.Aws_OutletNumber__c;
                    transDetail.MA2_MembershipNo__c = tradeRec.MembershipNo__c;
                    transDetail.MA2_CouponList__c = tradeRec.CouponList__c;
                    transDetail.MA2_Price__c = tradeRec.BuyAmt__c;
                    transDetail.MA2_GrossPrice__c = tradeRec.TrTotalAmt__c;
                    transDetail.MA2_PointAmt__c = tradeRec.PointAmt__c; 
                    transDetail.MA2_AcvPayAmt__c = tradeRec.AcvPayAmt__c;
                    transDetail.MA2_CouponDcAmt__c = tradeRec.CouponDcAmt__c;
                    //transDetail.MA2_PaymentType__c = tradeRec.TrxStatDesc__c;
                    transDetail.MA2_Points__c = tradeRec.AccPointAmt__c;   
                    transDetail.MA2_ProductList__c = tradeRec.ProductList__c;
                    //transDetail.MA2_TradeHistoryId__c = tradeRec.Id;
                    transDetail.MA2_Created_Date__c = tradeRec.THD_Createddate__c;
                    transDetail.MA2_Modified_Date__c = tradeRec.lastModifiedDate;
                    transDetail.TransactionId__c = tradeRec.TransactionId__c;
                    transDetail.MA2_CountryCode__c = 'KOR';
                    transDetail.TrxStat__c = tradeRec.TrxStat__c;
                    transDetail.TrxStatDesc__c = tradeRec.TrxStatDesc__c;
                    transDetail.MA2_OutletPoints__c= tradeRec.PointBalAf__c;
                    transDetail.Aws_CombinedId__c = tradeRec.Aws_CombinedId__c;
                    transDetail.MA2_Modified_Date__c= tradeRec.AWS_Last_Modified_date__c;
                    transDetailList.add(transDetail);
                }
            }
        }
        System.Debug('transDetailList--'+transDetailList);
        if(transDetailList.size() > 0){
            update transDetailList;
            List<MA2_TransactionProduct__c> transProdList = [select Id,MA2_Transaction__c from MA2_TransactionProduct__c where 
                                                  MA2_Transaction__c  in: transDetailList];
            for(MA2_TransactionProduct__c transProdDetail : transProdList){
                 if(tradeMap.containsKey(transProdDetail.MA2_Transaction__c)){
                    TradeHistory__c tradeRec = tradeRecordMap.get(tradeMap.get(transProdDetail.MA2_Transaction__c));
                     transProdDetail.MA2_productQuantity__c = 1;
                     if(contactMap.containsKey(tradeRec.Aws_ContactId__c)){
                         transProdDetail.MA2_ContactId__c = contactMap.get(tradeRec.Aws_ContactId__c);
                     }
                     transProdDetail.MA2_AccountId__c = tradeRec.Aws_OutletNumber__c;
                     transProdDetailList.add(transProdDetail);
                 }
            }
        }
        System.Debug('transProdDetailList--'+transProdDetailList);
        if(transProdDetailList.size() > 0){
            update transProdDetailList;
            System.Debug('transProdDetailList---'+transProdDetailList);
        }
    }
}