public class MA2_TransactionAPI{
    
    /*
     * Method for creating json body 
     */
     public static void sendData(List<TransactionTd__c> transactionList){
        
        final List<TransactionTd__c> transactionFilterList= new List<TransactionTd__c>(); 
         final List<Id> translist1= new List<Id>(); 
        String jsonString  =  '';       
        List<String> MA2CountryList = new List<String>();
        Map<String, MA2_CountryCode__c> countries = MA2_CountryCode__c.getAll();
        MA2CountryList.addAll(countries.keySet());
        Map<String,Decimal> cpnpointonTransaction = new Map<String,Decimal>();
        system.debug('MA2CountryList-->>>>>>>>>>-'+MA2CountryList);
         
        list<TransactionTd__c> transList=[select id,lastmodifiedBy.Name,(select id,CouponId__r.Price__c,TransactionId__c,CouponId__r.MA2_CouponType__c,CouponId__r.RecordTypeId,CouponId__r.MA2_BonusMultiplier__c from CouponContacts__r)
                                          , MA2_Contact__r.MembershipNo__c,MA2_CountryCode__c,MA2_Points__c,MA2_TransactionType__c,MA2_TransactionId__c,
                                          MA2_Price__c,MA2_AccountId__c, AccountID__r.OutletNumber__c, MA2_ECPId__c, MA2_ECPContact__r.MembershipNo__c from TransactionTd__c
                                          where Id IN :transactionList and MA2_CountryCode__c  in :MA2CountryList ];
           String recordtypeIds = [select id from recordtype where name like 'Bonus Multiplier'].id;
        for(TransactionTd__c trans : transList){
            String userName = trans.lastmodifiedBy.Name;
            userName = userName.toLowercase();
            if(!userName.contains('web')){
                transactionFilterList.add(trans);        
            }
            if(trans.CouponContacts__r != null){
                Decimal bonuscalculator = 1;
                for(CouponContact__c cpn : trans.CouponContacts__r){
                    if(cpn.CouponId__r.RecordTypeId == recordtypeIds && (cpn.CouponId__r.MA2_BonusMultiplier__c != null)){
                            bonuscalculator = bonuscalculator * cpn.CouponId__r.MA2_BonusMultiplier__c;
                        }
                    cpnpointonTransaction.put(cpn.TransactionId__c,bonuscalculator);
                    
                    // translist1.add(cpn.TransactionId__c);
                	}
           		 }
        }
        // update translist1;
        
        system.debug('transactionFilterList ==> '+transactionFilterList);
        if(transactionFilterList.size() > 0){
         jsonString  =  '{"transactionDetails": [';
         JSONGenerator gen = JSON.createGenerator(true); 
         gen.writeStartObject();
            for(TransactionTd__c config : transactionFilterList){
               gen.writeStringField('salesforceTransactionId',config.id+'');
               gen.writeStringField('region', config.MA2_CountryCode__c+'');
               gen.writeStringField('transactionType', config.MA2_TransactionType__c+'');
               gen.writeStringField('pointsAwarded', config.MA2_Points__c+'');             
               gen.writeStringField('consumerId',config.MA2_Contact__r.MembershipNo__c+'');
               gen.writeStringField('finalTransactionValue',config.MA2_Price__c+'');
               gen.writeStringField('storeId',config.AccountID__r.OutletNumber__c+'');
               gen.writeStringField('proUserId',config.MA2_ECPContact__r.MembershipNo__c+'');  
               gen.writeStringField('transactionId',config.MA2_TransactionId__c+'');   
                if(cpnpointonTransaction.containskey(config.id)){
                    gen.writeStringField('bonusMultiplier',cpnpointonTransaction.get(config.id)+'');
                }
                else if(!(cpnpointonTransaction.containskey(config.id))){
                    gen.writeStringField('bonusMultiplier',1+'');
                }
            }
            gen.writeEndObject();
            jsonString = jsonString + gen.getAsString()+']}';
        }
        System.Debug('jsonString--> '+jsonString);    
          if(jsonString != '' && jsonString != '{}'){
            sendtojtracker(jsonString);
          }
    }
    
    /*
     * Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONObject class
     */
    @future(callout = true)
    public static void sendtojtracker(String jsonBody) {
      JSONObject oauth = null;
       Credientials__c testPub = new Credientials__c();
       if(Credientials__c.getInstance('SendTransaction') != null){
           testPub  = Credientials__c.getInstance('SendTransaction');
           if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
                       final String ClientId = testPub.Client_Id__c;
                       final String ClientSecret = testPub.Client_Secret__c;
                       final String TargetUrl = testPub.Target_Url__c;
                       final String ApiKey = testPub.Api_Key__c; 
                       oauth = oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody);
           }
       }
       /*SonarQube Fix*/	   
       //System.debug('------oauth response------>>>>'+oauth);
    }
    
    /*
     * Method for sending data to the Apigee system
     */
    private static JSONObject oauthLogin(String targetUrl, String clientId, String clientSecret, String apiKeyValue,String jsonBody) {
      HttpRequest loginRequest = New HttpRequest();
      loginRequest.setMethod('POST');
      loginRequest.setTimeout(120000);
      loginRequest.setEndpoint(targetUrl);
      loginRequest.setHeader('grant_type', 'authorization_code');
      loginRequest.setHeader('client_id',clientId);
      loginRequest.setHeader('client_secret',clientSecret);
      loginRequest.setHeader('apikey',apiKeyValue);
      loginRequest.setHeader('Content-Type', 'application/json');
     
      system.debug('<<<<jsonString>>>>>'+jsonBody);
      loginRequest.setBody(jsonBody);
      Http Http = New Http();
      Long timeDiff = 0;
        Long milliSeconds = 1000;
        DateTime firstTime = System.now();
      HTTPResponse loginResponse = new HTTPResponse();
      if ( !Test.isRunningTest() ){
          do
        {
            timeDiff = System.now().getTime() - firstTime.getTime();
        }
          while(timeDiff <= milliSeconds);{}
         // System.Debug('loginResponse --'+loginResponse.getBody());
          loginResponse = http.send(loginRequest);
          JSONObject oAuth = (JSONObject) JSON.deserialize(loginResponse.getbody(), JSONObject.class);
          return oAuth;
      }
      
      
    
      return null;
 }
 
 // Inner class for setting value 
 public class JSONObject {
      public String id {
       get;
       set;
      }
      public String issued_at {
       get;
       set;
      }
      public String instance_url {
       get;
       set;
      }
      public String signature {
       get;
       set;
      }
      public String access_token {
       get;
       set;
      }
 }
 

}