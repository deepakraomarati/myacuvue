/**
* File Name: MA2_TransactionAPITest
* Description : Test class for MA2_TransactionAPI
* Copyright : Johnson & Johnson
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    2.0  |30-Aug-2017 |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_TransactionAPITest{
    
    /* Method for excuting the test class */
    static Testmethod void createTransactionMethod(){
        TestDataFactory_MyAcuvue.insertCustomSetting();
        final TriggerHandler__c cred = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                       GetTransactionTd__c = true, MA2_createUpdateTransaction__c = true);
        insert cred;
        
        Test.startTest();
        final List<Account> accList = new List<Account>();
        final List<Contact> conList = new List<Contact>();
        final List<TransactionTd__c> transactionList = new List<TransactionTd__c>();
        final TransactionTd__c transac =new TransactionTd__c(MA2_CountryCode__c='SGP');
        transactionList.add(transac);
        insert transactionList ;
        accList.addAll(TestDataFactory_MyAcuvue.createAccount(1));
        system.assertEquals(accList.size(), 4, 'Success');
        conList.addAll(TestDataFactory_MyAcuvue.createECPContact(2,accList));
        
        MA2_TransactionAPI.sendData(transactionList);
        
        Test.stopTest();
    }
}