/**
* File Name: MA2_TransactionCreatedDateupdate 
* Description : Class to convert Epoch Time Zone from Apigee Transaction Id and update in Transaction Created Date 
                 and Transaction Modified Date                
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | neel.kamal@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |01-Nov-2017 |nkamal8@its.jnj.com  |New Class created
* =============================================================== 
*/
public class MA2_TransactionCreatedDateupdate {
    public static void updatecreatedate(List<TransactionTd__c> tdList){
        List<TransactionTd__c> transList = new List<TransactionTd__c>();
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        for(TransactionTd__c td : tdList){
            if((td.MA2_TransactionType__c == 'Manual Adjustment' || td.MA2_TransactionType__c == 'Products' ) && !String.isBlank(td.MA2_TransactionId__c) && profilename != 'API User Profile')
            {
                long l = long.valueof(td.MA2_TransactionId__c);
                DateTime dt = DateTime.newInstance(l);
                string customerTimeZoneSidId = 'GMT';
                TimeZone customerTimeZone = TimeZone.getTimeZone(customerTimeZoneSidId);
                integer offsetToCustomersTimeZone = customerTimeZone.getOffset(dt);
                TimeZone tz = UserInfo.getTimeZone();
                integer offsetToUserTimeZone = tz.getOffset(dt);
                integer correction = offsetToUserTimeZone - offsetToCustomersTimeZone;
                DateTime correctedDateTime = dt.addMinutes(correction / (1000 * 60));
                Datetime newDate = correctedDateTime.addhours(-8);
                System.debug('correctedDateTime: ' + correctedDateTime);
                td.MA2_Created_Date__c = newDate ;
                td.MA2_Modified_Date__c = newDate ;
            }
        }
    }
}