@isTest
public class MA2_TransactionCreatedDateupdateTest {
    static Testmethod void TransactionCreatedDateupdateTest(){
        TriggerHandler__c mcs = new TriggerHandler__c(Name='HandleTriggers',MA2_createUpdateTransaction__c =true,
                                                      MA2_createUpdateTransactionProd__c = True);
        insert mcs;
        Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id;
        final List<Account> accList = new List<Account>();
        accList.addAll(TestDataFactory_MyAcuvue.createAccount(1));
        Contact conRec = new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',
                                     RecordTypeId = consumerRecTypeId,MembershipNo__c = 'HKG-192839',
                                     AccountId = accList[0].Id, email = 'test@gmail.com', phone = '123456789' ,
                                     MA2_Country_Code__c = 'HKG',
                                     MA2_Contact_lenses__c='No',
                                     MA2_PreAssessment__c = true);
        TransactionTd__c trans = new TransactionTd__c(AccountID__c = accList[0].Id, MA2_Contact__c = conRec.Id,
                                                      MA2_AccountId__c = '123456',MA2_ContactId__c = '23456' , 
                                                      MA2_Points__c = 10 ,MA2_Redeemed__c = 10,
                                                      MA2_TransactionType__c = 'Manual Adjustment',MA2_Price__c = 200,
                                                      MA2_CountryCode__c = 'HKG',MA2_PointsExpired__c = true,
                                                      CreatedDateTest__c = date.newinstance(2015,01,12),
                                                      MA2_TransactionId__c = String.Valueof(157890234));
        insert trans;
        system.assertEquals(trans.MA2_AccountId__c,'123456','success');
    }
}