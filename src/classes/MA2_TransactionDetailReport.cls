/**
* File Name: MA2_TransactionDetailReport
* Description : Batch job for scheduling consumer with transaction report
* Copyright : Johnson & Johnson
* @author : Lhawang Bhutia | lbhutia@its.jnj.com | lhawang.bhutia@cognizant.com
* 
* Modification Log 
* =============================================================== **/

global class MA2_TransactionDetailReport implements Database.Batchable<sObject>, Database.stateful{
    
    global list<TransactionTd__c> TransactionDetailList = new list<TransactionTd__c>();
    global string header = 'Transaction Details: ID, Apigee AccountId,Apigee ContactId,Apigee ECP Id,Apigee Transaction Id,Transaction Name,Transaction Type,Membership No,Coupon List,Price,Gross Price,Account Price,Point Amount,Acv Pay Amount,Coupon Discount Amount,Cancel Date,Payment Type,Points Awarded,Points Redeemed,Expired,Feedback Rating,Feedback Like,Consumer Contact,ECP Contact,Transaction Detail: Last Modified Date,Transaction Detail: Created Date,Country Code,Discount,Transaction Details: Created By,Transaction Details: Last Modified By \n';
    global string finalstr{get; set;}
    global string RecString;
    /*Method for querying all the Consumer Records with Transactions */ 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([select Id,MA2_AccountId__c,MA2_ContactId__c,MA2_ECPId__c,MA2_TransactionId__c,Name,MA2_TransactionType__c,MA2_MembershipNo__c,MA2_CouponList__c,MA2_Price__c,MA2_GrossPrice__c,MA2_AccountPrice__c,MA2_PointAmt__c,MA2_AcvPayAmt__c,MA2_CouponDcAmt__c,MA2_CancelDate__c,MA2_PaymentType__c,MA2_Points__c,MA2_Redeemed__c,MA2_PointsExpired__c,MA2_FeedbackRating__c,MA2_FeedbackLike__c,MA2_Contact__c,MA2_ECPContact__c,LastModifiedDate,CreatedDate,MA2_CountryCode__c,MA2_Discount__c,CreatedBy.name,Lastmodifiedby.name from TransactionTd__c where (MA2_CountryCode__c = 'HKG' OR MA2_CountryCode__c = 'SGP' OR MA2_CountryCode__c = 'TWN') and lastmodifieddate = today]);    
    }
    
    /* Method for excuting the query record */
    global void execute(Database.batchableContext BC,List<TransactionTd__c> TransactionDetailList){
        finalstr = '';        
        for(TransactionTd__c l : TransactionDetailList){
            RecString = l.MA2_AccountId__c+','+l.MA2_ContactId__c+','+l.MA2_ECPId__c+','+l.MA2_TransactionId__c+','+l.Name+','+l.MA2_TransactionType__c+','+l.MA2_MembershipNo__c+','+l.MA2_CouponList__c+','+l.MA2_Price__c+','+l.MA2_GrossPrice__c+','+l.MA2_AccountPrice__c+','+l.MA2_PointAmt__c+','+l.MA2_AcvPayAmt__c+','+l.MA2_CouponDcAmt__c+','+l.MA2_CancelDate__c+','+l.MA2_PaymentType__c+','+l.MA2_Points__c+','+l.MA2_Redeemed__c+','+l.MA2_PointsExpired__c+','+l.MA2_FeedbackRating__c+','+l.MA2_FeedbackLike__c+','+l.MA2_Contact__c+','+l.MA2_ECPContact__c+','+l.LastModifiedDate+','+l.CreatedDate+','+l.MA2_CountryCode__c+','+l.MA2_Discount__c+','+l.CreatedBy.name+','+l.Lastmodifiedby.name+'\n';
            finalstr = finalstr +RecString;
        }
    }    
        
    global void finish(Database.batchableContext BC){
        finalstr = header + finalstr;
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string datestamp = string.valueof(system.now());
        string csvname= 'Transaction Detail Report'+datestamp+'.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        fileAttachments.add(csvAttc);
        
        string body = '';
        body += 'Hello Team,' + '\n';
        body += '\n';
        body += 'Attached the Transaction details extracted from SFDC for data validation..' + '\n';
        body += '\n';
        body += 'Regards' + '\n';
        body += 'Skywalker Support'+ ' '+ '\n';
        body += '\n';
        body += 'Note:- This email has been sent automatically by SFDC Interface.'+ ' '+ '\n';
        body += '\n';
        
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        string emailAddress = System.Label.MA2_ToEmailAddress;
        string[] toAddresses = emailAddress.split(',');
        string subject ='Transaction Detail Report'+string.valueof(system.now())+'.csv';
        email.setSubject(subject);
        email.setToAddresses(toAddresses);
        email.setPlainTextBody(body);
        email.setFileAttachments(fileAttachments);
        try{
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        }catch (exception e){
            system.debug('Exception -->' + e);
        }
    }       
}