/**
* File Name: MA2_TransactionDetailTest
* Description : Test class for Transaction detail
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |9-Sep-2016  |hsingh53@its.jnj.com  |New Class created
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    2.0  |30-Aug-2017 |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_TransactionDetailTest{
    
    /* Method for excuting the test class */
    static Testmethod void createTransactionMethod(){
        TestDataFactory_MyAcuvue.insertCustomSetting();
        Test.startTest();
        final TriggerHandler__c cred = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                             GetTransactionTd__c = true, MA2_createUpdateTransaction__c = true, CouponContact__c= true, MA2_CouponInactiveTrigger__c = true,
                                                             MA2_CouponToApigeeTrigger__c= true, MA2_CouponWalletForApigee__c= true, MA2_createUpdateTransactionProd__c= true );
        insert cred;
        final List<Account> accList = new List<Account>();
        final List<Contact> conList = new List<Contact>();
        
        accList.addAll(TestDataFactory_MyAcuvue.createAccount(1));
        system.assertEquals(accList.size(), 4, 'Success');

        conList.addAll(TestDataFactory_MyAcuvue.createContact(1,accList));
        
        MA2_TransactionAPI.sendData(TestDataFactory_MyAcuvue.createTransaction(1,accList,conList));
        Test.stopTest();
    }
}