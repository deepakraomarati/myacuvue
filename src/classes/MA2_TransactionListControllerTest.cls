/* 
* File Name: MA2_TransactionListControllerTest
* Description : Test class for MA2_TransactionListController 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |15-Sep-2017  |lbhutia@its.jnj.com   |Class Modified
* =============================================================== 
*    Ver  |       |                |
*         |       |                |
*
*/
@istest(seealldata=true)
public class MA2_TransactionListControllerTest 
{
    static Testmethod void createTransList()
    {
        Test.startTest();
        final TriggerHandler__c cred = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                             GetTransactionTd__c = true, MA2_createUpdateTransaction__c = true, CouponContact__c= true, MA2_CouponInactiveTrigger__c = true,
                                                             MA2_CouponToApigeeTrigger__c= true, MA2_CouponWalletForApigee__c= true, MA2_createUpdateTransactionProd__c= true );
       // insert cred;       
        RecordType rec = [Select id from RecordType where Name = 'Cash Discount' limit 1];
        RecordType rec1 = [Select id from RecordType where Name = 'Consumer' limit 1];
          RecordType rec2 = [Select id from RecordType where Name = 'ECP' limit 1];
        Account acc = new Account();
        acc.Name='Test Accout';
        acc.ECP_Name__c='Test ECP';
        acc.OutletNumber__c='123006';
        acc.MA2_PriceGroup__c='Group A';
        acc.My_Acuvue__c = 'Yes';
        insert acc;
       
        Contact con = new Contact();
        con.FirstName='Test';
        con.LastName='Contact';
        con.accountId=con.id;
        con.RecordTypeId=rec1.id;
        con.MembershipNo__c='eC1666N';
        insert con;
        
        Contact con2 = new Contact();
        con2.FirstName='Test';
        con2.LastName='Contact';
        con2.accountId=acc.id;
        con2.RecordTypeId=rec2.id;
        con2.MembershipNo__c='eC1666N';
        insert con2;
        //con2=[select id, Name from contact where RecordTypeId=:rec2.id  limit 1];
        
        
        Transaction__c transact = new Transaction__c();
        transact.AccountID__c=acc.Id;
        transact.ContactID__c=con.Id;
        insert transact;
        
        TransactionTd__c trans = new TransactionTd__c();
        trans.AccountID__c=acc.Id;
        trans.TransTD_createddate__c = system.now();
        trans.MA2_CancelDate__c= system.now();
        trans.MA2_CountryCode__c='HKG';
        trans.MA2_TransactionType__c='Products';
        trans.MA2_Price__c=120.50;
        trans.MA2_Contact__c=con.id;
        trans.MA2_Points__c=120;  
        trans.MA2_ContactId__c=con.id;
        trans.MA2_ECPContact__c=con2.id;
        insert trans;
        
        Coupon__c coupon = new Coupon__c();
        coupon.MA2_CountryCode__c='HKG';
        coupon.CouponName__c='test Coupon';
        coupon.CouponType__c='Cash Discount';
        coupon.Price__c=50;
        coupon.RecordTypeId=rec.id;
        coupon.MA2_CouponValidity__c=40;
        coupon.StartDate__c=System.Today();
        coupon.EndDate__c=System.Today()+1;
        insert coupon; 
        
        CouponContact__c coupwalt = new CouponContact__c();
        coupwalt.ContactId__c=con.id;
        coupwalt.AccountId__c=acc.Id;
        coupwalt.CouponId__c=coupon.Id;
        coupwalt.MA2_ContactId__c='0088ddd';
        coupwalt.TransactionId__c= trans.id;
        insert coupwalt; 
        
        MA2_Account_Price_Group__c testrecord = new MA2_Account_Price_Group__c();
        testrecord.Name = 'Group A';
        testrecord.Oasys__c = 123.45;
        testrecord.Moist__c = 234.56;
        testrecord.Define__c = 345.67;
       // insert testrecord;
        PageReference pageRef = Page.MA2_TransactionListPage;
        Test.setCurrentPage(pageRef);
        MA2_TransactionListController ctr = new MA2_TransactionListController();
        ctr.Trans = trans;
        ctr.FilterAccName = 'Test';
        ctr.FilterAccNme = 'Test';
        ctr.publiczone = 'HKG';
        ctr.AccountOwner = 'Test';
        ctr.FromDate = system.now();
        ctr.ToDate = system.now();
        MA2_TransactionListController.Wrapper wrap = new MA2_TransactionListController.Wrapper('Test ECP',string.valueof(con.id),
                                                                                               string.valueof(system.today()),'test Coupon','GHJM766990m',string.valueof(transact.name),'Acuvue Moist','30', '1DM','2720.00','0.0','Test Contact','1234567891', 'TestProduct', 'TestECP',string.valueof(acc.OutletNumber__c),'', 'Group A', 30.0);       
        system.assertEquals(ctr.FilterAccName,'Test','Success');
        ctr.GetAllList();
        ctr.pushToAccount();
        ctr.SendEmail();  
        ctr.clear();
        ctr.SendPDF();
       
        acc.Name='Test Accout1';
        acc.ECP_Name__c='Test ECP1';
        acc.OutletNumber__c=null;
        acc.MA2_PriceGroup__c='Group A';
        acc.My_Acuvue__c = 'Yes';
        Update acc;
        ctr.GetAllList();
        MA2_TransactionListController.FOCResultWrapper foc=new MA2_TransactionListController.FOCResultWrapper();
       List<string> st01=new lIST<string>();
        list<decimal> dec=new List<decimal>();
            dec.add(10.05);
        st01.add('tEST01');
   foc.key='Test';
        foc.AccountECPName='Test ECP1';
        
        foc.lstAccountNameOL=st01;
         foc.dectotalCpnValue=10.05;
         foc.dectotalNEFTAmt=11.05;
         foc.lstInvoicePrice=dec;
         foc.lstFOCCurrentMonth=dec;
         foc.dectotalGrossAmt=12.09;
        apexpages.currentPage().getparameters().put('fromDat','2018-09-01T08:12:00.000Z');
        apexpages.currentPage().getparameters().put('toDat','2018-09-01T08:12:00.000Z');
        ctr.fetchAccouns();
        
        
         MA2_TransactionListController.FOCFinalWrapper focfinal=new MA2_TransactionListController.FOCFinalWrapper();
        Map<string,string> mapfinal=new Map<string,string>();
        focfinal.mapAccountECPName=mapfinal;
         map<string,list<string>> maplist=new  map<string,list<string>>();
          focfinal.mapAccountNameOL=maplist;
        map<string,decimal> mapdeci=new map<string,decimal>();
        focfinal.totalCpnValue=mapdeci;
        
         focfinal.totalNEFTAmt=mapdeci;
         focfinal.totalGrossAmt=mapdeci;
        
        
        map<string,list<decimal>> listdesimal=new map<string,list<decimal>>();
        
         focfinal.MapInvoicePrice=listdesimal;
         focfinal.MapFOCCurrentMonth=listdesimal;
        
        
        Test.stopTest();
        System.assertEquals(coupon.CouponType__c,'Cash Discount','Cash Discount');      
    }
}