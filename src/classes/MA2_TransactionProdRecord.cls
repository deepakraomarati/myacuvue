/**
* File Name: MA2_TransactionProdRecord
* Description : Class for mapping MYACUVUE Ids of Contact/Account with Contact/Account Object
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |9-Sep-2016  |hsingh53@its.jnj.com  |New Class created
*/
public class MA2_TransactionProdRecord{
    
    /*
* Method for mapping MYACUVUE Ids of Contact/Account with Contact/Account Object
*/
    public static void beforeCreateUpdate(List<MA2_TransactionProduct__c> recordList){
        if(recordList.size()>0){
            createUpdateRecord(recordList);
        }
    }
    
    /* 
* Method for fetching AccountIds/ContactIds or MYACUVUE AccountIds and ContactIds for mapping
*/
    public static void createUpdateRecord(List<MA2_TransactionProduct__c> recordList){
        List<String> contactList = new List<String>();
        List<String> contactAPGList = new List<String>();
        List<String> accountId = new List<String>();
        List<String> apgAccountId = new List<String>();
        List<String> contactId = new List<String>();
        List<String> apgContactId = new List<String>();
        Map<String,Account> accMap = new Map<String,Account>();
        Map<String,Contact> conMap = new Map<String,Contact>();
        Map<String,Contact> ecpMap = new Map<String,Contact>();
        
        for(MA2_TransactionProduct__c tr : recordList){
            if(tr.MA2_AccountId__c != null && tr.MA2_Account__c == null){
                apgAccountId.add(tr.MA2_AccountId__c);
            }else{
                accountId.add(tr.MA2_Account__c);
            }
            if(tr.MA2_ContactId__c != null && tr.MA2_Contact__c == null){
                apgContactId.add(tr.MA2_ContactId__c);
            }else{
                contactId.add(tr.MA2_Contact__c);
            }
            if(tr.MA2_ECPId__c != null  && tr.MA2_ECPContact__c == null){
                apgContactId.add(tr.MA2_ECPId__c);
            }else{
                contactId.add(tr.MA2_ECPContact__c);
            }
        }
        accMap = getAccountMap(apgAccountId,accountId);
        conMap = getContactMap(apgcontactId,contactId);
        System.Debug('accMap---'+accMap);
        System.Debug('conMap---'+conMap);
        if(!accMap.isEmpty() || !conMap.isEmpty()){
            for(MA2_TransactionProduct__c tr : recordList){
                Account acc = new Account();
                if(tr.MA2_AccountId__c != null && accMap.get(tr.MA2_AccountId__c) != null){
                    acc = accMap.get(tr.MA2_AccountId__c);  
                    System.Debug('accMap.get(tr.MA2_AccountId__c)---'+accMap.get(tr.MA2_AccountId__c));
                    tr.MA2_Account__c =  acc.Id; 
                }else{
                    acc = accMap.get(tr.MA2_Account__c);
                    if(acc.OutletNumber__c != null){
                        tr.MA2_AccountId__c = acc.OutletNumber__c ;
                    }  
                }
                
                System.debug('conMap.get(tr.MA2_ContactId__c)--'+conMap.get(tr.MA2_ContactId__c));
                Contact con = new Contact();
                if(tr.MA2_ContactId__c != null && conMap.get(tr.MA2_ContactId__c) != null){
                    con = conMap.get(tr.MA2_ContactId__c);
                    if(con != null){
                        if(con.RecordType.Name == 'Consumer'){
                            tr.MA2_Contact__c = con.Id;
                        } 
                    }   
                }else{
                    con = conMap.get(tr.MA2_Contact__c);
                    if(con != null){
                        if(con.RecordType.Name == 'Consumer'){
                            tr.MA2_ContactId__c = con.MembershipNo__c; 
                        }  
                    } 
                }
                if(tr.MA2_ECPId__c != null){
                    con = conMap.get(tr.MA2_ECPId__c);  
                    if(con != null){
                        if(con.RecordType.Name == 'ECP'){
                            tr.MA2_ECPContact__c = con.Id;
                        }  
                    }  
                }else{
                    con = conMap.get(tr.MA2_ECPContact__c);
                    if(con != null){
                        if(con.RecordType.Name == 'ECP'){
                            tr.MA2_ECPId__c = con.MembershipNo__c; 
                        } 
                    }
                }
            }   
        }
    }
    
    /*
* Method for getting Account Info from Account Object
*/
    public static Map<String,Account> getAccountMap(List<String> apgAccountId,List<String> accountId){
        Map<String,Account> accMap = new Map<String,Account>();
        Map<String,String> accAPGMap = new Map<String,String>();
        List<Account> accList = [select Id,OutletNumber__c from Account where OutletNumber__c in: apgAccountId or Id in: accountId];
        
        for(Account acc  : accList){
            accMap.put(acc.OutletNumber__c,acc);
            accMap.put(acc.Id,acc);
        }
        System.Debug('accMap---'+accMap);
        return accMap;
    }
    
    /*
* Method for getting Contact Info from Contact Object
*/
    public static Map<String,Contact> getContactMap(List<String> apgContactId,List<String> ContactId){
        Map<String,Contact> conMap = new Map<String,Contact>();
        Map<String,Contact> conAPGMap = new Map<String,Contact>();
        List<Contact> conList = [select Id,MembershipNo__c,RecordType.Name from Contact where MembershipNo__c in: apgContactId or Id in: ContactId];
        System.Debug('conList---'+conList);
        for(Contact con  : conList){
            conMap.put(con.MembershipNo__c,con);
            conMap.put(con.Id,con);
        }
        System.Debug('conMap---'+conMap);
        return conMap;
    }
    
    
    public static  void updateTransactionRecord(List<MA2_TransactionProduct__c> recordList )
    {
        system.debug('FunctionName : updateTransactionRecord');
        set<Id> TransactionIds = new set<Id>();
        //
        List<CouponContact__c> cpnPrice = new List<CouponContact__c>();
        Map<String,Decimal> cpnpriceonTransaction = new Map<String,Decimal>();
        //
        for ( MA2_TransactionProduct__c temp: recordlist){
            TransactionIds.add(temp.MA2_Transaction__c);
        }
        if(!TransactionIds.isempty())
        {    
            map<Id,TransactionTd__c> TrasactionUpdate = new map<Id,TransactionTd__c> ([select id, MA2_GrossPrice__c,MA2_Price__c from TransactionTd__c where Id in :TransactionIds ]) ;
            
            /*
* Coupon Price Calculation for Transaction Object to update on transaction Price field.
*/
            List<TransactionTd__c> transactionPrice = [select id,MA2_Contact__c, (select id,CouponId__r.Price__c from CouponContacts__r) from TransactionTd__c where id in : TransactionIds];
            System.Debug('transactionPrice----->>'+transactionPrice);
            for(TransactionTd__c tds : transactionPrice){
                if(tds.CouponContacts__r != null){
                    Decimal temp = 0;
                    for(CouponContact__c cpn : tds.CouponContacts__r){
                        if(cpn.CouponId__r.Price__c != null){
                        temp = temp + cpn.CouponId__r.Price__c ;
                        }
                        else{
                            temp = temp + 0;
                        }
                    }
                    cpnpriceonTransaction.put(tds.id,temp);
                }
            }
            
            for(TransactionTd__c temp : TrasactionUpdate.values())
                temp.MA2_GrossPrice__c = 0.0;
            
            for( MA2_TransactionProduct__c TransProd : [select MA2_Transaction__c ,MA2_ProductName__r.Price__c ,MA2_ProductQuantity__c,lastmodifiedBy.Name
                                                        from MA2_TransactionProduct__c 
                                                        where MA2_Transaction__c in :TransactionIds])   
            {
             String userName = TransProd.lastmodifiedBy.Name;
            
                if((TransProd.MA2_ProductName__r.Price__c != null &&  TransProd.MA2_ProductName__r.Price__c >0) &&
                   (TransProd.MA2_ProductQuantity__c  != null && TransProd.MA2_ProductQuantity__c > 0 ))      
                    if(TrasactionUpdate.containsKey(TransProd.MA2_Transaction__c ))
                {
                    Decimal tempCalc = TransProd.MA2_ProductName__r.Price__c * TransProd.MA2_ProductQuantity__c ;
                    Decimal OldVal = TrasactionUpdate.get(TransProd.MA2_Transaction__c).MA2_GrossPrice__c;
                    TrasactionUpdate.get(TransProd.MA2_Transaction__c).MA2_GrossPrice__c =  OldVal + tempCalc;
                    if(cpnpriceonTransaction.containskey(TransProd.MA2_Transaction__c)){
                       userName = userName.toLowercase();
            if(!userName.contains('web')){  
                    TrasactionUpdate.get(TransProd.MA2_Transaction__c).MA2_Price__c = TrasactionUpdate.get(TransProd.MA2_Transaction__c).MA2_GrossPrice__c - cpnpriceonTransaction.get(TransProd.MA2_Transaction__c);
                        }
                }
                   
            }
            }
            try{ update TrasactionUpdate.values(); }
            catch (Exception e){    system.debug('Error Handle');    }
        } 
    }
}