/**
* File Name: MA2_TransactionProdRecordTest
* Description : Test class for Transaction Product Record
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |9-Sep-2016  |hsingh53@its.jnj.com  |New Class created
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    2.0  |30-Aug-2017 |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_TransactionProdRecordTest{
    
    /* Method for excuting the test class */
    static Testmethod void createTransactionProdMethod(){
        
        Final TriggerHandler__c record = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                         MA2_createUpdateTransactionProd__c = true,MA2_ProductCurrencyISOUpdate__c = True,MA2_createUpdateTransaction__c = True);
        insert record;
        Final Credientials__c crdntrans = new Credientials__c(Name = 'SendTransaction',Api_Key__c = 'zuCgB84PsVi8GVZcGyaD6GpM52soQeQx',
                                                          Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY',
                                                          Target_Url__c = 'https://jnj-global-test.apigee.net/sfdc/transaction/products');
        insert crdntrans;
        Final Credientials__c crdnt = new Credientials__c(Name = 'SendTransactionProduct',Api_Key__c = 'zuCgB84PsVi8GVZcGyaD6GpM52soQeQx',
                                                          Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY',
                                                          Target_Url__c = 'https://jnj-global-test.apigee.net/sfdc/transaction/products');
        insert crdnt;
        String countryname_SG = 'SGP';
         String countryname_SG2 = 'HKG';
        MA2_Country_Currency_Map__c CountryMap = new MA2_Country_Currency_Map__c(Name='SGP',Currency__c='SGD');
        insert CountryMap;
        MA2_CountryCode__c cntry = new MA2_CountryCode__c(Name = 'SGP',MA2_CountryCodeValue__c = 'SGP');
        Insert cntry;
        
    final Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id;
        final Id consumerRecTypeId1 = [select Id from RecordType where Name = 'ECP' and sObjectType= 'Contact'].Id;
        final List<Account> accList = new List<Account>();
        final List<Contact> conList = new List<Contact>();
        final List<TransactionTd__c> transList = new List<TransactionTd__c>();
        Test.startTest();
        //accList.addAll(TestDataFactory_MyAcuvue.createAccount(1));
        final Account accc = new Account(Name = 'Test' , MA2_AccountId__c = '9783945' ,AccountNumber = '54322',
                                        OutletNumber__c = '54322',ActiveYN__c = true,
                                        My_Acuvue__c = 'Yes',CountryCode__c = countryname_SG ,PublicZone__c = 'Testtt');
        
        insert accc;
        final Id etrialRecordTypeId = [select Id from RecordType where Name = 'Cash Discount' and sObjectType = 'Coupon__c'].Id;
        final Coupon__c coupon = new Coupon__c(MA2_CouponName__c = 'Cash Discount',RecordTypeID = etrialRecordTypeId ,MA2_CountryCode__c =countryname_SG2
                                               ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Pre-Assessment',Price__c = 100);  
        insert coupon;
        
        final Contact con1 = new Contact(LastName = 'test1' ,MA2_AccountId__c = '978394' ,MA2_ContactId__c = '23456',
                                         RecordTypeId  = consumerRecTypeId  ,
                                         AccountId = accc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = 'HKD', 
                                         MembershipNo__c = 'SGP1235', MA2_Country_Code__c = countryname_SG2, MA2_Grade__c='Base', DOB__c = date.newinstance(2012,03,03),
                                         MA2_PreAssessment__c = true,MA2_Contact_lenses__c = 'Acuvue Brand');
        insert con1;
        
        final Contact con2 = new Contact(LastName = 'test1' ,MA2_AccountId__c = '978394' ,MA2_ContactId__c = '23456',
                                         RecordTypeId  = consumerRecTypeId1  ,
                                         AccountId = accc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = 'HKD', 
                                         MembershipNo__c = 'SGP1235', MA2_Country_Code__c = countryname_SG, MA2_Grade__c='Base', DOB__c = date.newinstance(2012,03,03),
                                         MA2_PreAssessment__c = true,MA2_Contact_lenses__c = 'Acuvue Brand');
        insert con2;        
        final transactiontd__c tra1 = new transactiontd__c (MA2_TransactionType__c= 'products', MA2_CountryCode__c= 'SGP', AccountID__c=accc.Id,MA2_Created_Date__c = System.today(),MA2_Modified_Date__c = System.today(),
                                                            MA2_Contact__c=con1.Id, MA2_ECPContact__c= con2.Id, MA2_Price__c=200, MA2_Points__c= 2000,CreatedDateTest__c = System.Today(),MA2_TransactionId__c = '1542686077');
        insert tra1;
        
        final product2 pr2 =new product2(Name='test', MA2_Plant__c='SG11',UPC_Code__c='123654789',CurrencyIsoCode = 'SGD',Price__c = 1000,ExternalId__c = 'SGP-123654789',InternalName__c = 'testpro',
        Description__c = 'test product');
        insert pr2;
        
        final MA2_TransactionProduct__c trpr1 = new MA2_TransactionProduct__c(MA2_Transaction__c=tra1.id, MA2_Account__c=accc.id, MA2_ProductName__c=pr2.Id,
                                                                              MA2_ProductQuantity__c = 1,MA2_LotNo__c = '103jdue',MA2_ECPId__c = '092893',MA2_CreatedDate__c = System.today(),MA2_ModifiedDate__c = System.Today());
        insert trpr1;
        final MA2_TransactionProduct__c trpr12 = new MA2_TransactionProduct__c(MA2_Transaction__c=tra1.id, MA2_Account__c=accc.id, MA2_ProductName__c=pr2.Id,
                                                                              MA2_ProductQuantity__c = 1,MA2_LotNo__c = '103jdue',MA2_ECPId__c = '092893',MA2_CreatedDate__c = System.today(),MA2_ModifiedDate__c = System.Today());
        insert trpr12;
        
        final CouponContact__c couponWallet = new CouponContact__c(ExpiryDate__c = System.Today() - 10 ,ContactId__c = con1.Id ,
                                                                   CouponId__c = coupon.Id , MA2_AccountId__c = '12345' , Apigee_Update__c = false,
                                                                   MA2_ContactId__c = '5555',CurrencyIsoCode='HKD');   
        
        insert couponWallet;
        system.assertEquals(tra1.MA2_CountryCode__c, 'SGP');
        //update trpr1;
        Delete trpr1;
        //conList.addAll(TestDataFactory_MyAcuvue.createContact(1,accList));
        //transList.addAll(TestDataFactory_MyAcuvue.createTransaction(1,accList,conList));
        //TestDataFactory_MyAcuvue.createTransactionProduct(1,accList,conList,transList); 
        Test.stopTest();
    }
}