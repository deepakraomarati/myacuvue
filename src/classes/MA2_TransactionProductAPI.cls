public class MA2_TransactionProductAPI{
    
    /*
     * Method for creating json body 
     */
    public static void sendProductData(List<MA2_TransactionProduct__c> TPList){
    
        final List<MA2_TransactionProduct__c> TPFilterList= new List<MA2_TransactionProduct__c>(); 
        String jsonString  = '';
        List<String> MA2CountryList = new List<String>();
        Map<String, MA2_CountryCode__c> countries = MA2_CountryCode__c.getAll();
        MA2CountryList.addAll(countries.keySet());
        system.debug('MA2CountryList-->>>>>>>>>>-'+MA2CountryList);
        
        list<MA2_TransactionProduct__c> TtansProdList=[select id, lastmodifiedBy.Name, MA2_CountryCode__c, MA2_ProductQuantity__c, UPC_Code__c,MA2_LotNo__c, MA2_ProductId__c, MA2_Transaction__r.MA2_AccountId__c, MA2_Transaction__r.MA2_ECPId__c, MA2_Transaction__r.MA2_TransactionId__c, MA2_Transaction__r.MA2_Contact__r.MembershipNo__c from MA2_TransactionProduct__c where Id IN :TPList and  MA2_CountryCode__c  in :MA2CountryList];
  		System.Debug('myTest--->'+TtansProdList);
        for(MA2_TransactionProduct__c tp : TtansProdList){
            System.Debug('myTest2--->');
            String userName = tp.lastmodifiedBy.Name;
            userName = userName.toLowercase();
            if(!userName.contains('web')){
                TPFilterList.add(tp);        
            }
        }
        
        if(TPFilterList.size() > 0){
            jsonString  =  '{"productDetails": [';
            JSONGenerator gen = JSON.createGenerator(true); 
            gen.writeStartObject();
            for(MA2_TransactionProduct__c config : TPFilterList){
              if(!config.lastmodifiedBy.Name.toLowercase().contains('web')){
               gen.writeStringField('transactionId',config.MA2_Transaction__r.MA2_TransactionId__c+'');
               gen.writeStringField('productQuantity', config.MA2_ProductQuantity__c+'');
               gen.writeStringField('productId', config.UPC_Code__c+'');
               gen.writeStringField('productLotId', config.MA2_LotNo__c+'');               
               gen.writeStringField('storeId',config.MA2_Transaction__r.MA2_AccountId__c+'');
               gen.writeStringField('proUserId',config.MA2_Transaction__r.MA2_ECPId__c+'');
               gen.writeStringField('consumerId',config.MA2_Transaction__r.MA2_Contact__r.MembershipNo__c+'');
               gen.writeStringField('salesforceTransactionProductId',config.id+'');
                  gen.writeStringField('action','add' +'');
              }                
            }
            gen.writeEndObject();
            jsonString = jsonString + gen.getAsString()+']}';
        }
        System.Debug('jsonString--'+jsonString);    
          if(jsonString != '' && jsonString != '{}'){
            sendTPtojtracker(jsonString);
          }
    }
    
    /*
     * Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONObject class
     */
    @future(callout = true)
    public static void sendTPtojtracker(String jsonBody) {
      JSONObject oauth = null;
       Credientials__c testPub = new Credientials__c();
       if(Credientials__c.getInstance('SendTransactionProduct') != null){
           testPub  = Credientials__c.getInstance('SendTransactionProduct');
           if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
                       final String ClientId = testPub.Client_Id__c;
                       final String ClientSecret = testPub.Client_Secret__c;
                       final String TargetUrl = testPub.Target_Url__c;
                       final String ApiKey = testPub.Api_Key__c; 
                       oauth = oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody);
           }
       }
       /*SonarQube Fix*/	   
       //System.debug('------oauth response------>>>>'+oauth);
        
    }
    
    /*
     * Method for sending data to the Apigee system
     */
    private static JSONObject oauthLogin(String targetUrl, String clientId, String clientSecret, String apiKeyValue,String jsonBody) {
      HttpRequest loginRequest = New HttpRequest();
      loginRequest.setMethod('POST');
      loginRequest.setTimeout(120000);
      loginRequest.setEndpoint(targetUrl);
      loginRequest.setHeader('grant_type', 'authorization_code');
      loginRequest.setHeader('client_id',clientId);
      loginRequest.setHeader('client_secret',clientSecret);
      loginRequest.setHeader('apikey',apiKeyValue);
      loginRequest.setHeader('Content-Type', 'application/json');
     
      system.debug('<<<<jsonString>>>>>'+jsonBody);
      loginRequest.setBody(jsonBody);
      Http Http = New Http();
      
      HTTPResponse loginResponse = new HTTPResponse();
      if ( !Test.isRunningTest() ){
          loginResponse = http.send(loginRequest);
         // System.Debug('loginResponse --'+loginResponse.getBody());
          JSONObject oAuth = (JSONObject) JSON.deserialize(loginResponse.getbody(), JSONObject.class);
          return oAuth;
      }
      
      
    
      return null;
 }
 
 // Inner class for setting value 
 public class JSONObject {
      public String id {
       get;
       set;
      }
      public String issued_at {
       get;
       set;
      }
      public String instance_url {
       get;
       set;
      }
      public String signature {
       get;
       set;
      }
      public String access_token {
       get;
       set;
      }
 }
 

}