/**
* File Name: MA2_TransactionProductAPITest
* Description : Test class for MA2_TransactionProductAPI
* Copyright : Johnson & Johnson
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    2.0  |21-Mar-2018 |pbheeman@its.jnj.com   |Class Created
*/
@isTest
public class MA2_TransactionProductAPITest {
    /* Method for excuting the test class */
    static Testmethod void createTransactionProductMethod(){
        final Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id; 
        final Id ECPRecTypeId = [select Id from RecordType where Name = 'ECP' and sObjectType= 'Contact'].Id; 
        final TriggerHandler__c mcs = new TriggerHandler__c(Name='HandleTriggers',MA2_createUpdateTransaction__c =true,
                                                      MA2_createUpdateTransactionProd__c = True,CouponContact__c=true);
        insert mcs;
        final MA2_CountryCode__c msn = new MA2_CountryCode__c(Name='HKG',MA2_CountryCodeValue__c ='HKG'
                                                       );
        insert msn;
        final Credientials__c cd = new Credientials__c(Name = 'SendTransactionProduct',Api_Key__c = 'zuCgB',Client_Id__c = 'b3U65',Client_Secret__c = 'b3U6xFG',Target_Url__c = 'gthf');
        insert cd;
        final Credientials__c cd1 = new Credientials__c(Name = 'SendTransaction',Api_Key__c = 'zuCgB',Client_Id__c = 'b3U65',Client_Secret__c = 'b3U6xFG',Target_Url__c = 'gthf');
        insert cd1;
        final List<Account> accList = new List<Account>();
        final List<MA2_TransactionProduct__c> tpList = new List<MA2_TransactionProduct__c>();
        accList.addAll(TestDataFactory_MyAcuvue.createAccount(1)); 
        Contact conRec = new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',
                                     RecordTypeId = consumerRecTypeId,MembershipNo__c = 'HKG-192839',
                                     AccountId = accList[0].Id, email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'HKG',
                                     
                                     MA2_Contact_lenses__c='No',
                                     DOB__c  = System.Today(),
                                     MA2_PreAssessment__c = true);
        insert conRec;
        
        Contact conRec2 = new Contact(LastName = 'test1' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '234561',
                                      RecordTypeId = ECPRecTypeId,MembershipNo__c = 'HKG-1928392',
                                      AccountId = accList[0].Id, email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'HKG',
                                      
                                      MA2_Contact_lenses__c='No',
                                      DOB__c  = System.Today(),
                                      MA2_PreAssessment__c = true);
        insert conRec2;
        
        TransactionTd__c trans = new TransactionTd__c(AccountID__c = accList[0].Id, MA2_Contact__c = conRec.Id,MA2_ECPContact__c = conRec2.Id,MA2_TransactionId__c = '150839283',
                                                      MA2_AccountId__c = '123456',MA2_ContactId__c = '23456' , MA2_Points__c = 10 ,MA2_Redeemed__c = 10,
                                                      MA2_TransactionType__c = 'Products',MA2_Price__c = 200,MA2_CountryCode__c = 'HKG',
                                                      MA2_PointsExpired__c = true,CreatedDateTest__c = date.newinstance(2015,01,12));
        insert trans;
        final List<TransactionTd__c> transactionList = new List<TransactionTd__c>();
        transactionList.add(trans);
        
        Product2 pro = new Product2();
        pro.Name = 'test-21';
        pro.UPC_Code__c = '9876546';
        insert pro;
        MA2_TransactionProduct__c tproduct = new MA2_TransactionProduct__c(
            MA2_Account__c = accList[0].Id,
            MA2_Contact__c = conRec.Id,
            MA2_ProductName__c = pro.id,
            MA2_ProductQuantity__c = 1,
            MA2_TransactionId__c = '150839283',
            MA2_Transaction__c = trans.Id,
            MA2_LotNo__c = '103jdue',
            MA2_ProductId__c = pro.id,
            MA2_AccountId__c = '343532',
            MA2_ECPId__c = '092893'
        );
        Insert tproduct;
        tpList.add(tproduct);
        
        Test.startTest();
        system.assertEquals(conRec.LastName, 'test', 'success');
        MA2_TransactionProductAPI.sendProductData(tpList);
        Test.stopTest();
    }
}