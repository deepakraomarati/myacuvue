/**
* File Name: MA2_TransactionProductReport
* Description : Batch job for scheduling consumer with transaction and Product report
* Copyright : Johnson & Johnson
* @author : Lhawang Bhutia | lbhutia@its.jnj.com | lhawang.bhutia@cognizant.com
* 
* Modification Log 
* =============================================================== **/

global class MA2_TransactionProductReport implements Database.Batchable<sObject>, Database.Stateful{
    global list<MA2_TransactionProduct__c> TransactionProductList = new list<MA2_TransactionProduct__c>();
    global string header = 'Id,Product Name,Product UPC Code,Quantity,Consumer Id,Store Id,Transaction Product Created Date,Apigee Transaction Id,Product Lot No \n';
    global    string finalstr{get; set;}
    global    string RecString;
        
    /*Method for querying all the Consumer Records with Transactions */ 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String userName = '%Webservice%';
        User userRecord = [select Id from user where Name like '%Webservice API MyAcuvue%' limit 1];       
        return Database.getQueryLocator([select Id,MA2_ProductName__r.Name,UPC_Code__c,MA2_ProductQuantity__c,MA2_Contact__r.MembershipNo__c,MA2_Account__r.OutletNumber__c,CreatedDate,MA2_TransactionId__c,MA2_LotNo__c from MA2_TransactionProduct__c where MA2_Transaction__r.lastmodifiedbyId != : userRecord.Id and MA2_Transaction__r.MA2_TransactionType__c = 'Products' and MA2_Transaction__r.lastmodifiedDate = today ]);    
    }
    
    /* Method for excuting the query record */
    
    global void execute(Database.batchableContext BC,List<MA2_TransactionProduct__c> TransactionProductList){
        finalstr = '';
        for(MA2_TransactionProduct__c k : TransactionProductList){
            RecString = k.Id+','+k.MA2_ProductName__r.Name+','+k.UPC_Code__c+','+k.MA2_ProductQuantity__c+','+k.MA2_Contact__r.MembershipNo__c+','+k.MA2_Account__r.OutletNumber__c+','+k.CreatedDate+','+k.MA2_TransactionId__c+','+k.MA2_LotNo__c +'\n';
            finalstr = finalstr +RecString;                                  
        }        
    }
    
    global void finish(Database.batchableContext BC){
        finalstr = header + finalstr;
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string datestamp = string.valueof(system.now());
        string csvname= 'Transaction Product Report'+datestamp+'.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        fileAttachments.add(csvAttc);
        
        string body = '';
        body += 'Hello Team,' + '\n';
        body += '\n';
        body += 'Attached the Manual Product details  extracted from SFDC for data validation..' + '\n';
        body += '\n';
        body += 'Regards' + '\n';
        body += 'Skywalker Support'+ ' '+ '\n';
        body += '\n';
        body += 'Note:- This email has been sent automatically by SFDC Interface.'+ ' '+ '\n';
        body += '\n';
        
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        string emailAddress = System.Label.MA2_ToEmailAddress;
        string[] toAddresses = emailAddress.split(',');
        string subject ='Transaction Product Report'+string.valueof(system.now())+'.csv';
        email.setSubject(subject);
        email.setToAddresses(toAddresses);
        email.setPlainTextBody(body);
        email.setFileAttachments(fileAttachments);
        try{
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        }catch (exception e){
        }

    }       
}