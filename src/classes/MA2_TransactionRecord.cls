/**
* File Name: 
TransactionRecord
* Description : class for updating points in Contact and Account along with assigning the account and contact
                based on Apigee external Id for the transaction Record
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |25-Sep-2016 |hsingh53@its.jnj.com  |New Class created
*/

public class MA2_TransactionRecord{

    /*
     * Update the transaction Record 
     */
    public static void beforeCreateUpdate(List<TransactionTd__c> recordList){
        if(recordList.size()>0){
            createUpdateRecord(recordList);
        }
    }
    
    /*
     *Update Contact /Account Point field if transaction record is not expired 
     */
    public static void afterCreateUpdate(List<TransactionTd__c> recordList){
        if(recordList.size()>0){
           filterContactAccount(recordList);
        }
    }
    
    /*
     * Filtering Contact / Account Record Based on 
     */
    public static void filterContactAccount(List<TransactionTd__c> recordList){
        final Map<Id,TransactionTd__c> transMap = new Map<Id,TransactionTd__c>();
         for(TransactionTd__c tr : recordList){
             if(tr.MA2_TransactionId__c == null){
                if(tr.MA2_ECPContact__c != null && tr.MA2_Contact__c == null){
                    transMap.put(tr.MA2_ECPContact__c,tr);    
                }else if(tr.MA2_Contact__c != null){
                    transMap.put(tr.MA2_Contact__c,tr);
                }else if(tr.AccountID__c != null){
                    transMap.put(tr.AccountID__c,tr);
                }
            }
         }
         if(!transMap.isEmpty()){ 
           updateContactAccountList(transMap); 
        }
    }
    
    /*
     * Assign the Contact Id / Account Id based on Apigee Contact Id / Apigee Account Id
     */
    public static void createUpdateRecord(List<TransactionTd__c> recordList){
        List<String> contactList = new List<String>();
        List<String> contactAPGList = new List<String>();
        List<String> accountId = new List<String>();
        List<String> apgAccountId = new List<String>();
        List<String> contactId = new List<String>();
        List<String> apgContactId = new List<String>();
        Map<String,Account> accMap = new Map<String,Account>();
        Map<String,Contact> conMap = new Map<String,Contact>();
        Map<String,Contact> ecpMap = new Map<String,Contact>();
        
        for(TransactionTd__c tr : recordList){
            if(!tr.MA2_PointsExpired__c){
                if(tr.MA2_AccountId__c != null && tr.AccountID__c == null){
                    apgAccountId.add(tr.MA2_AccountId__c);
                }else{
                    accountId.add(tr.AccountID__c);
                }
                //System.Debug('tr.MA2_ContactId__c--'+tr.MA2_ContactId__c);
                //System.Debug('tr.MA2_Contact__c--'+tr.MA2_Contact__c);
                if(tr.MA2_ContactId__c != null && tr.MA2_Contact__c == null){
                    apgContactId.add(tr.MA2_ContactId__c);
                }else{
                    contactId.add(tr.MA2_Contact__c);
                }
                if(tr.MA2_ECPId__c != null  && tr.MA2_ECPContact__c == null){
                    apgContactId.add(tr.MA2_ECPId__c);
                }else{
                    contactId.add(tr.MA2_ECPContact__c);
                }
            } 
        }
        System.Debug('apgAccountId--'+apgAccountId);
        System.Debug('accountId--'+accountId);
        System.Debug('apgcontactId--'+apgcontactId);
        System.Debug('contactId--'+contactId);
        accMap = getAccountMap(apgAccountId,accountId);
        conMap = getContactMap(apgcontactId,contactId);
        System.Debug('accMap---'+accMap);
        System.Debug('conMap---'+conMap);
        for(TransactionTd__c tr : recordList){
                if(!accMap.isEmpty()){
                    Account acc = new Account();
                    if(tr.MA2_AccountId__c != null && accMap.get(tr.MA2_AccountId__c) != null){
                        acc = accMap.get(tr.MA2_AccountId__c);  
                        System.Debug('accMap.get(tr.MA2_AccountId__c)---'+accMap.get(tr.MA2_AccountId__c));
                        tr.AccountID__c =  acc.Id; 
                    }else{
                         System.Debug('accMap.get(tr.AccountID__c)---'+accMap.get(tr.AccountID__c));
                         if(tr.AccountID__c != null){
                             acc = accMap.get(tr.AccountID__c);
                             tr.AccountID__c = acc.Id;
                         }
                    }
                 }
                  if(!conMap.isEmpty()){  
                    System.debug('conMap.get(tr.MA2_ContactId__c)--'+conMap.get(tr.MA2_ContactId__c));
                    Contact con = new Contact();
                    if(tr.MA2_ContactId__c != null){
                        con = conMap.get(tr.MA2_ContactId__c);
                        if(con != null){
                            if(con.RecordType.Name == 'Consumer'){
                                tr.MA2_Contact__c = con.Id;
                            } 
                        }   
                    }else{
                        con = conMap.get(tr.MA2_Contact__c);
                        if(con != null){
                            if(con.RecordType.Name == 'Consumer'){
                                tr.MA2_ContactId__c = con.MembershipNo__c; 
                            }  
                        } 
                    }
                    if(tr.MA2_ECPId__c != null){
                        con = conMap.get(tr.MA2_ECPId__c);  
                        if(con != null){
                            if(con.RecordType.Name == 'ECP'){
                                tr.MA2_ECPContact__c = con.Id;
                            }  
                        }  
                    }else{
                        con = conMap.get(tr.MA2_ECPContact__c);
                        if(con != null){
                            if(con.RecordType.Name == 'ECP'){
                                tr.MA2_ECPId__c = con.MembershipNo__c; 
                            } 
                        }
                    }
            }
        }
    }
    
    /*
     * Get the Account Ids based on Apigee Account Id
     */
    public static Map<String,Account> getAccountMap(List<String> apgAccountId,List<String> accountId){
        Map<String,Account> accMap = new Map<String,Account>();
        Map<String,String> accAPGMap = new Map<String,String>();
        List<Account> accList = [select Id,OutletNumber__c from Account where OutletNumber__c in: apgAccountId or Id in: accountId];
        
        for(Account acc  : accList){
           accMap.put(acc.OutletNumber__c,acc);
           accMap.put(acc.Id,acc);
        }
        System.Debug('accMap---'+accMap);
        return accMap;
    }
    
    /*
     * Get the Contact Ids based on Apigee Contact Id
     */
    public static Map<String,Contact> getContactMap(List<String> apgContactId,List<String> ContactId){
        Map<String,Contact> conMap = new Map<String,Contact>();
        Map<String,Contact> conAPGMap = new Map<String,Contact>();
        List<Contact> conList = [select Id,MembershipNo__c,RecordType.Name from Contact where MembershipNo__c in: apgContactId or Id in: ContactId];
        System.Debug('conList---'+conList);
        for(Contact con  : conList){
                conMap.put(con.MembershipNo__c,con);
                conMap.put(con.Id,con);
        }
        System.Debug('conMap---'+conMap);
        return conMap;
    }
    
    /*
     * Update the Contact /Account Points fields based on Transaction Points fields if Transaction is not expired
     */
    public static void updateContactAccountList(Map<Id,TransactionTd__c> transMap){
        List<Contact> updateContactList = new List<Contact>();
        List<Account> updateAccountList = new List<Account>();
        List<Contact> contactRecord = [select Id,MA2_MembershipPoint__c,MA2_UsePoint__c,MA2_GrandPoint__c 
                                      from Contact where Id in: transMap.keySet()];
        if(contactRecord.size() > 0){
            for(Contact con : contactRecord){
                System.Debug('con---'+contactRecord);
                TransactionTd__c trans = new TransactionTd__c();
                trans = transMap.get(con.Id);
                if(trans.MA2_Points__c != null && !trans.MA2_PointsExpired__c){
                    if(con.MA2_MembershipPoint__c != null){
                        con.MA2_MembershipPoint__c = con.MA2_MembershipPoint__c + trans.MA2_Points__c;
                    }else{
                        con.MA2_MembershipPoint__c = trans.MA2_Points__c;
                    }
                    if(con.MA2_UsePoint__c != null){
                        con.MA2_UsePoint__c = con.MA2_UsePoint__c + trans.MA2_Points__c;
                    }else{
                        con.MA2_UsePoint__c = trans.MA2_Points__c;
                    }
                    if(con.MA2_GrandPoint__c != null){
                        con.MA2_GrandPoint__c = con.MA2_GrandPoint__c + trans.MA2_Points__c;
                    }else{
                        con.MA2_GrandPoint__c = trans.MA2_Points__c;
                    }
                    /*if(con.MA2_TotalPoint__c != null){
                        con.MA2_TotalPoint__c = con.MA2_TotalPoint__c + trans.MA2_Points__c;
                    }else{
                        con.MA2_TotalPoint__c = trans.MA2_Points__c;
                    }*/
                }
                if(trans.MA2_Redeemed__c != null && !trans.MA2_PointsExpired__c){
                    if(con.MA2_UsePoint__c != null){
                        con.MA2_UsePoint__c = con.MA2_UsePoint__c - trans.MA2_Redeemed__c;
                        if(con.MA2_UsePoint__c < 0){
                            con.MA2_UsePoint__c = 0;    
                        }
                    }else{
                        con.MA2_UsePoint__c = 0;
                    }
                }
                if(trans.MA2_Points__c != null && trans.MA2_PointsExpired__c){
                     if(con.MA2_UsePoint__c != null){
                        con.MA2_UsePoint__c = con.MA2_UsePoint__c - trans.MA2_Points__c;
                        if(con.MA2_UsePoint__c < 0){
                            con.MA2_UsePoint__c = 0;    
                        }
                    }else{
                        con.MA2_UsePoint__c = 0;
                    }
                }
                updateContactList.add(con);
                System.debug('updateContactList---'+updateContactList);
            }    
        }else{
            List<Account> accountRecord = [select Id,MA2_Points__c from Account where Id in: transMap.keySet()]; 
            System.Debug('acc---'+accountRecord);   
            if(accountRecord.size() > 0){
                for(Account acc : accountRecord){
                    TransactionTd__c trans = new TransactionTd__c();
                    trans = transMap.get(acc.Id);
                    if(trans.MA2_Points__c != null && !trans.MA2_PointsExpired__c){
                        if(acc.MA2_Points__c != null){
                            acc.MA2_Points__c = acc.MA2_Points__c + trans.MA2_Points__c;
                        }else{
                            acc.MA2_Points__c = trans.MA2_Points__c;
                        }
                        updateAccountList.add(acc);
                    }  
                }  
            }
        }
        if(updateContactList.size() > 0){
            //update updateContactList;
        }
        System.Debug('updateAccountList---'+updateAccountList);
        if(updateAccountList.size() > 0){
            //update updateAccountList;
        }
        
    }
}