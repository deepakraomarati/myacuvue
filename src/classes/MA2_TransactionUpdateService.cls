public class MA2_TransactionUpdateService{
    
     public static void sendData(List<TransactionTd__c> transactionList){
         list<TransactionTd__c> updateList=[select lastmodifiedBy.Name,Id,MA2_TransactionId__c,MA2_Contact__c,MA2_Contact__r.MembershipNo__c,MA2_Status__c from TransactionTd__c where id in:transactionList];
        final List<TransactionTd__c> transactionFilterList= new List<TransactionTd__c>();    
        for(TransactionTd__c trans : updateList){
            String userName = trans.lastmodifiedBy.Name;
            userName = userName.toLowercase();
            if(!userName.contains('web')){
                transactionFilterList.add(trans);        
            }
        }
        if(transactionFilterList.size() != 0){
            sendApigeeData(transactionFilterList);
        }
    }
    /*
     * Method for creating json body 
     */
    public static void sendApigeeData(List<TransactionTd__c> transactionList){
    list<TransactionTd__c> AppProfile=[select id,Name,MA2_TransactionId__c,MA2_Contact__c,MA2_Contact__r.MembershipNo__c,MA2_Status__c from TransactionTd__c where id in:transactionList];
        String jsonString  = '';
        
        if(transactionList.size() > 0){
        
            system.debug('AppProfile-->>>>>>>>>>-'+AppProfile);
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            for(TransactionTd__c td : AppProfile){
              gen.writeStringField('id',td.id+'');
              gen.writeStringField('transactionId',td.MA2_TransactionId__c+'');
              gen.writeStringField('consumerId',td.MA2_Contact__r.MembershipNo__c+'');
              gen.writeStringField('transactionStatus',td.MA2_Status__c+'');   
            }
            gen.writeEndObject();
            jsonString = jsonString + gen.getAsString();
        }
        System.Debug('jsonString--'+jsonString);
        if(jsonString != ''){
            sendtojtracker(jsonString);
        }
    }
    
    /*
     * Method for sending endpointurl/secretkey/clientId/apikey/json body to the JSONObject class
     */
    @future(callout = true)
    public static void sendtojtracker(String jsonBody) {
        JSONObject oauth = null;
       Credientials__c testPub = new Credientials__c();
       if(Credientials__c.getInstance('UpdateTransaction') != null){
           testPub  = Credientials__c.getInstance('UpdateTransaction');
           if(testPub.Client_Id__c != null && testPub.Client_Secret__c != null && testPub.Target_Url__c != null && testPub.Api_Key__c != null){
                       final String ClientId = testPub.Client_Id__c;
                       final String ClientSecret = testPub.Client_Secret__c;
                       final String TargetUrl = testPub.Target_Url__c;
                       final String ApiKey = testPub.Api_Key__c; 
                       oauth = oauthLogin(targetUrl , clientId , clientsecret , apiKey , jsonBody);
           }
       }    
       /*SonarQube Fix*/	   
       //System.debug('------oauth response------>>>>'+oauth);
        
    }
    
    /*
     * Method for sending data to the Apigee system
     */
    private static JSONObject oauthLogin(String targetUrl, String clientId, String clientSecret, String apiKeyValue,String jsonBody) {
      HttpRequest loginRequest = New HttpRequest();
      loginRequest.setMethod('POST');
      loginRequest.setEndpoint(targetUrl);
      loginRequest.setHeader('grant_type', 'authorization_code');
      loginRequest.setHeader('client_id',clientId);
      loginRequest.setHeader('client_secret',clientSecret);
      loginRequest.setHeader('apikey',apiKeyValue);
      loginRequest.setHeader('Content-Type', 'application/json');
     
      system.debug('<<<<jsonString>>>>>'+jsonBody);

      loginRequest.setBody(jsonBody);
      Http Http = New Http();
      HTTPResponse loginResponse = new HTTPResponse();
      if ( !Test.isRunningTest() ){
          loginResponse = http.send(loginRequest);
          JSONObject oAuth = (JSONObject) JSON.deserialize(loginResponse.getbody(), JSONObject.class);
          return oAuth;
      }
      
      System.Debug('loginResponse --'+loginResponse.getBody());
    
      return null;
 }
 
 // Inner class for setting value 
 public class JSONObject {
      public String id {
       get;
       set;
      }
      public String issued_at {
       get;
       set;
      }
      public String instance_url {
       get;
       set;
      }
      public String signature {
       get;
       set;
      }
      public String access_token {
       get;
       set;
      }
 }
 

}