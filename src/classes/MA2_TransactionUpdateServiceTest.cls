/**
* File Name: MA2_TransactionUpdateServiceTest
* Description : Test class for Transaction detail
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |9-Sep-2016  |hsingh53@its.jnj.com  |New Class created
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    2.0  |30-Aug-2017 |lbhutia@its.jnj.com   |Class Modified
*/
@isTest
public class MA2_TransactionUpdateServiceTest{
    
    /* Method for excuting the test class */
    static Testmethod void createTransactionMethod(){        
        Test.startTest();
        final Credientials__c cred = new Credientials__c(Name = 'UpdateTransaction' , 
                                                         Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                                                         Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                                                         Target_Url__c = 'https://jnj-dev.apigee.net/v1/consumer/catalog/addcoupon');
        insert cred;
		final TriggerHandler__c trghandler = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                             GetTransactionTd__c = true, MA2_createUpdateTransaction__c = true, CouponContact__c= true, MA2_CouponInactiveTrigger__c = true,
                                                             MA2_CouponToApigeeTrigger__c= true, MA2_CouponWalletForApigee__c= true, MA2_createUpdateTransactionProd__c= true );
        insert trghandler;
        final List<Account> accList = new List<Account>();
        final List<Contact> conList = new List<Contact>();
        
        accList.addAll(TestDataFactory_MyAcuvue.createAccount(1));
        system.assertEquals(accList.size(), 4, 'Success');
        
        conList.addAll(TestDataFactory_MyAcuvue.createContact(1,accList));
        final List<Transactiontd__c> transList = new List<Transactiontd__c>();
        transList.addAll(TestDataFactory_MyAcuvue.createTransaction(1,accList,conList));
        final Transactiontd__c transRecord = new Transactiontd__c();
        transRecord.Id = transList[0].Id;
        update transRecord;
        MA2_TransactionUpdateService.sendData(translist);    
        Test.stopTest();
    }
}