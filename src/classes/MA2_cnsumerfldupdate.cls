public class MA2_cnsumerfldupdate {
    Public static void cnsmrupdate(List<Consumer_Relation__c> newConsumer){
        
        Set<String> IdsList = new Set<String>();
        List<Consumer_Relation__c> conListTrueCount = new List<Consumer_Relation__c>();
        List<Consumer_Relation__c> cnsfinalList = new List<Consumer_Relation__c>();
        Set<String> cnsfinalfirst = new Set<String>();
        for(Consumer_Relation__c cn :newConsumer){
            IdsList.add(cn.ContactID__c); 
        }
        conListTrueCount = [select id,MA2_MyECP__c,ContactID__c,MA2_ContactId__c from Consumer_Relation__c where (MA2_MyECP__c = True AND 
                                                                                                                  ContactID__c In : IdsList)];
        System.Debug('conListTrueCount->>'+conListTrueCount.size());
        for(Consumer_Relation__c cnr : newConsumer){
            System.Debug('cnr.MA2_MyECP__c--->>'+cnr.MA2_MyECP__c);
            System.Debug('conListTrueCount--->>'+conListTrueCount.size());
            if(cnr.MA2_MyECP__c == False && conListTrueCount.size() == 0){
                    cnsfinalfirst.add(cnr.ContactID__c);
            }
        }
        cnsfinalList = [select id,MA2_MyECP__c,ContactID__c,MA2_ContactId__c,lastmodifiedBy.Name,ContactID__r.MembershipNo__c,AccountId__r.OutletNumber__c,AccountId__r.ECP_Name__c
                                    from Consumer_Relation__c where ContactID__c IN : IdsList and ContactID__c In : cnsfinalfirst];
        
         if(cnsfinalList.size()>0 ){
                    MA2_ConsumerServiceUpdateCall.sendData(cnsfinalList);
            
         }
    }
    Public static void cnsmrupdateValidation(List<Consumer_Relation__c> newConsumer){
        for(Consumer_Relation__c con : newConsumer){
            if(con.MA2_MyECP__c == True && con.MA2_ApigeeUpdate__c == True){
                con.adderror('Cannot update the record. Please create a new consumer relation for this activity');
            }
        }
    }
}