/**
* File Name: MA2_transactionproductreportTest
* Description : Test class for MA2_transactionproductreportTest
* Copyright : Johnson & Johnson
* @author : Neel Kamal | nkamal8@its.jnj.com | nkamal8@cognizant.com
* Modification Log 
* ================================================================
*    Ver  |Date         |Author                |Modification
*    2.0  |10-OCT-2017  |nkamal8@its.jnj.com   |Class created
*/
@isTest
public class MA2_transactionproductreportTest {
    /* Method for excuting the test class */
    static Testmethod void runBatchJobMethod(){
        TriggerHandler__c mcs = new TriggerHandler__c(Name='HandleTriggers',GetTransactionTd__c = true, MA2_createUpdateTransaction__c = true,
                                                      CouponContact__c= true, MA2_CouponInactiveTrigger__c = true,
                                                             MA2_CouponToApigeeTrigger__c= true, MA2_CouponWalletForApigee__c= true,
                                                      MA2_createUpdateTransactionProd__c= true);
        insert mcs;
        final string CountryCurrency = 'SGD';
        Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id; 
        final Id consumerRecTypeId2 = [select Id from RecordType where Name = 'ECP' and sObjectType= 'Contact'].Id;
        //final List<Account> accList = new List<Account>();
        final Account accList = new Account(Name = 'Test' , MA2_AccountId__c = '12345' ,AccountNumber = '54321',
                                        Marketing_Program__c = 'AEC',OutletNumber__c = '54321',ActiveYN__c = true,
                                        My_Acuvue__c = 'Yes',CountryCode__c = 'SGP',PublicZone__c = 'Testtt',ECP_Name__c = 'ABC',PublicAddress__c = 'Hi5',PublicPhone__c = '998978',PublicState__c = 'XYZA');
        
        insert accList;
        
        Contact conRec = new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',
                                     RecordTypeId = consumerRecTypeId,MembershipNo__c = 'HKG-192839',
                                     AccountId = accList.Id, email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'HKG',
                                     
                                     MA2_Contact_lenses__c='No',
                                     DOB__c  = System.Today(),
                                     MA2_PreAssessment__c = true);
        insert conRec;
        final Contact con1 = new Contact(LastName = 'test1' ,MA2_AccountId__c = '123456' ,MA2_ContactId__c = '23456',
                                         RecordTypeId  = consumerRecTypeId2 ,
                                         AccountId = accList.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = CountryCurrency, 
                                         MembershipNo__c = 'SGP1235', MA2_Country_Code__c = 'SGP', MA2_Grade__c='Base', DOB__c = date.newinstance(2012,03,03),
                                         MA2_PreAssessment__c = true);
        insert con1;
        TransactionTd__c trans = new TransactionTd__c(AccountID__c = accList.Id, MA2_Contact__c = conRec.Id,
                                                      MA2_AccountId__c = '123456',MA2_ContactId__c = '23456' , MA2_Points__c = 10 ,MA2_Redeemed__c = 10,
                                                      MA2_TransactionType__c = 'Products',MA2_Price__c = 200,MA2_CountryCode__c = 'SGP',MA2_ECPContact__c = con1.id,MA2_TransactionId__c = '1502265411415'
                                                      );
        insert trans;
        
        final List<TransactionTd__c> transactionList = new List<TransactionTd__c>();
        transactionList.add(trans);
        
        Product2 pro = new Product2();
        pro.Name = 'test21';
        pro.UPC_Code__c = '9876546';
        pro.InternalName__c = 'testpro';
        pro.Description__c = 'test product';
        insert pro;
        
        MA2_TransactionProduct__c tproduct = new MA2_TransactionProduct__c(
            MA2_Account__c = accList.Id,
            MA2_Contact__c = conRec.Id,
            MA2_ProductName__c = pro.id,
            MA2_ProductQuantity__c = 1,
            MA2_TransactionId__c = '150839283',
            MA2_Transaction__c = trans.Id,
            CreatedDate = System.today());
        Insert tproduct;
        
        Test.startTest();
        Database.executeBatch(new MA2_transactionproductreport(),100);
        System.assertEquals(pro.name,'test21','Success');
        Test.stopTest();
    }
}