public class MemberQuickList {

    Private Integer OffsetValue = 0;
    Public Integer queryLimit { get; set; } 
    Public  Integer TotalMemberSize{get;set;}
    Public Integer CurrentMemberSize{get;set;}
    public Boolean firstLoad;
    Public ID thisCampaignID;
    public String soql;
    public string SelectedValue { get; set; }
    public string SelectedMemberId { get; set; }
    
    private String column;
    public String sortDirection;
    public final static String SORT_ASC = 'ASC';
    public final static String SORT_DESC = 'DESC';
    
    Public boolean Collapse{ get; set; }
    Public String filterValue1{ get; set; }
    public string SelectedField1 { get; set; }
    public string SelectedOperation1 { get; set; }
    Public String filterValue2{ get; set; }
    public string SelectedField2 { get; set; }
    public string SelectedOperation2 { get; set; }
    private string filterQuery;
    private Date dt1;
    private Date dt2;
    Public Integer PageNumber{get;set;}
    
    public List<Memberwrapper> MemberwrapperList = new List<Memberwrapper>();
    List<campaignMember> selectedMembers = new List<campaignMember>();
    
    public MemberQuickList(ApexPages.StandardController controller) {
        thisCampaignID = ApexPages.currentPage().getParameters().get('id');
        TotalMemberSize = [SELECT count() FROM campaignMember WHERE CampaignID = :thisCampaignID];
        firstLoad = true;
        column='CreatedDate';
        sortDirection='ASC';
        Collapse=false;
        queryLimit = 100;
        PageNumber=1;
        CurrentMemberSize=TotalMemberSize;
    }
    
    /************** Get Memberlist to display in page  ****************/ 
    public List<Memberwrapper> getCampaignMemberList()
    {      
        List<Memberwrapper> MembersList = new List<Memberwrapper>();
        if(firstLoad){
            soql ='SELECT ID, CampaignID, Campaign.Name,Contact.LastName,Contact.FirstName,Status,Campaign.Type,Approval_by_Campaign_Admin__c,Receipt_number__c,LastModifiedDate,CreatedDate,acu_CreatedDate__c,acu_LastModifiedDate__c FROM CampaignMember WHERE CampaignID = :thisCampaignID LIMIT : queryLimit OFFSET : OffsetValue';
            filterQuery = ' WHERE CampaignID = :thisCampaignID';
        }
        System.debug('soql'+soql); 
        System.debug('thisCampaignID = ' + thisCampaignID );
        for(campaignMember c : Database.Query(soql))
        {
            MembersList.add(new Memberwrapper(c));
        }       
        System.debug('Values are ' + MembersList);
        MemberwrapperList = MembersList;
        return MembersList;       
    }  
    
    /************** Get selected Custom view data ****************/ 
    public void Load() {
        soql = 'SELECT ID, CampaignID, Campaign.Name,Contact.LastName,Contact.FirstName,Status,Campaign.Type,Approval_by_Campaign_Admin__c,Receipt_number__c,LastModifiedDate,CreatedDate,acu_CreatedDate__c,acu_LastModifiedDate__c FROM CampaignMember';
        OffsetValue = 0;
        PageNumber=1;
        firstLoad = false;
        ClearFilter();
        
        if(SelectedValue=='All'){
             soql += ' WHERE CampaignID = :thisCampaignID LIMIT : queryLimit OFFSET : OffsetValue';
             CurrentMemberSize = TotalMemberSize ;
             filterQuery = ' WHERE CampaignID = :thisCampaignID';
        }
        else if(SelectedValue=='Today'){
            
            soql += ' WHERE CampaignID = :thisCampaignID AND CreatedDate =Today LIMIT : queryLimit  OFFSET : OffsetValue';
            filterQuery = ' WHERE CampaignID = :thisCampaignID AND CreatedDate =Today';
            CurrentMemberSize = [SELECT count() FROM campaignMember WHERE CampaignID = :thisCampaignID AND CreatedDate =Today];
            
        }
        else if(SelectedValue=='Yesterday'){
            
            soql += ' WHERE CampaignID = :thisCampaignID AND CreatedDate = YESTERDAY LIMIT : queryLimit  OFFSET : OffsetValue';
            filterQuery = ' WHERE CampaignID = :thisCampaignID AND CreatedDate = YESTERDAY';
            CurrentMemberSize = [SELECT count() FROM campaignMember WHERE CampaignID = :thisCampaignID AND CreatedDate =YESTERDAY];
        }
        else if(SelectedValue=='ThisWeek'){
            
            soql += ' WHERE CampaignID = :thisCampaignID AND CreatedDate = THIS_WEEK LIMIT : queryLimit  OFFSET : OffsetValue';
            filterQuery = ' WHERE CampaignID = :thisCampaignID AND CreatedDate = THIS_WEEK';
            CurrentMemberSize = [SELECT count() FROM campaignMember WHERE CampaignID = :thisCampaignID AND CreatedDate = THIS_WEEK];
            
        }
        else if(SelectedValue=='LastWeek'){
            
            soql += ' WHERE CampaignID = :thisCampaignID AND CreatedDate = LAST_WEEK LIMIT : queryLimit  OFFSET : OffsetValue';
            filterQuery = ' WHERE CampaignID = :thisCampaignID AND CreatedDate = LAST_WEEK';
            CurrentMemberSize = [SELECT count() FROM campaignMember WHERE CampaignID = :thisCampaignID AND CreatedDate = LAST_WEEK];
            
        }
        else if(SelectedValue=='Last3days'){
            
            soql += ' WHERE CampaignID = :thisCampaignID AND acu_CreatedDate__c = LAST_N_DAYS:3 LIMIT : queryLimit  OFFSET : OffsetValue';
            filterQuery = ' WHERE CampaignID = :thisCampaignID AND CreatedDate = LAST_N_DAYS:3';
            CurrentMemberSize = [SELECT count() FROM campaignMember WHERE CampaignID = :thisCampaignID AND CreatedDate = LAST_N_DAYS:3];
            
        }
    }
    
    /************** Get Filter Data   ****************/ 
    public void GO()
    {
        soql = 'SELECT ID, CampaignID, Campaign.Name,Contact.LastName,Contact.FirstName,Status,Campaign.Type,Approval_by_Campaign_Admin__c,Receipt_number__c,LastModifiedDate,CreatedDate,acu_CreatedDate__c,acu_LastModifiedDate__c FROM CampaignMember';
        OffsetValue = 0;
        PageNumber=1;
        firstLoad = false;
             
        if((SelectedField1!='FieldNone' && SelectedOperation1 !='OperationNone' && filterValue1 !='') && (SelectedField2!='FieldNone' && SelectedOperation2 !='OperationNone' && filterValue2 !='')){
            
            System.debug('User entered Date1'+filterValue1+'User entered Date2'+filterValue2);
            try{
                    //dt1 = date.newinstance(Integer.ValueOf(dmy[0]),Integer.ValueOf(dmy[1]),Integer.ValueOf(dmy[2]));
                    dt1= formateInputDate(filterValue1);
                    System.debug('Date1@@@'+dt1);
                    
                    dt2= formateInputDate(filterValue2);
                    System.debug('Date2@@@'+dt2);
                    
                    soql += ' WHERE CampaignID = :thisCampaignID AND '+SelectedField1+SelectedOperation1+':dt1 AND '+SelectedField2+SelectedOperation2+':dt2 LIMIT : queryLimit  OFFSET : OffsetValue';
                    filterQuery = ' WHERE CampaignID = :thisCampaignID AND '+SelectedField1+SelectedOperation1+':dt1 AND '+SelectedField2+SelectedOperation2+':dt2';
                    System.debug('soql'+soql);
                    
                    CurrentMemberSize = Database.countQuery('SELECT count() FROM campaignMember WHERE CampaignID = :thisCampaignID AND '+SelectedField1+SelectedOperation1+':dt1 AND '+SelectedField2+SelectedOperation2+':dt2');
                    System.debug('CurrentMemberSize '+CurrentMemberSize );
            }
            Catch(Exception e){
                 EmptyList();    
            }
        }
        
        else if(SelectedField1!='FieldNone' && SelectedOperation1 !='OperationNone' && filterValue1 !=''){
          
            System.debug('User entered Value'+filterValue1);
            try{
                    dt1= formateInputDate(filterValue1);
                    System.debug('Date@@@'+dt1);
                    
                    soql += ' WHERE CampaignID = :thisCampaignID AND '+SelectedField1+SelectedOperation1+':dt1 LIMIT : queryLimit  OFFSET : OffsetValue';
                    filterQuery = ' WHERE CampaignID = :thisCampaignID AND '+SelectedField1+SelectedOperation1+':dt1';
                    System.debug('soql'+soql);
                    
                    CurrentMemberSize = Database.countQuery('SELECT count() FROM campaignMember WHERE CampaignID = :thisCampaignID AND '+SelectedField1+SelectedOperation1+':dt1');
                    System.debug('CurrentMemberSize '+CurrentMemberSize );
            }
            catch(Exception e){
                EmptyList();
            }
            
        }
        
      else if(SelectedField2!='FieldNone' && SelectedOperation2 !='OperationNone' && filterValue2 !=''){
           
            System.debug('User entered Date2'+filterValue2);
            try{
                    dt2= formateInputDate(filterValue2);
                    System.debug('Date@@@'+dt2);
                    
                    soql += ' WHERE CampaignID = :thisCampaignID AND '+SelectedField2+SelectedOperation2+':dt2 LIMIT : queryLimit  OFFSET : OffsetValue';
                    filterQuery = ' WHERE CampaignID = :thisCampaignID AND '+SelectedField2+SelectedOperation2+':dt2';
                    System.debug('soql'+soql);
                    
                    CurrentMemberSize = Database.countQuery('SELECT count() FROM campaignMember WHERE CampaignID = :thisCampaignID AND '+SelectedField2+SelectedOperation2+':dt2');
                    System.debug('CurrentMemberSize '+CurrentMemberSize );
            }
            catch(Exception e){
                EmptyList();        
            }
        }
        else if((SelectedField1=='FieldNone' && SelectedOperation1 =='OperationNone' && filterValue1 =='') && (SelectedField2=='FieldNone' && SelectedOperation2 =='OperationNone' && filterValue2 =='')){
            SelectedValue='All';
            Load();
        } 
        else{
            EmptyList();    
        }
    }
    
    /************** Display empty list when user select wrong data to filter  ****************/ 
    public void EmptyList(){
            soql += ' WHERE CampaignID = :thisCampaignID AND ID = null';
            filterQuery = ' WHERE CampaignID = :thisCampaignID AND ID = null';
            CurrentMemberSize=0;
    }
    
    /************** Formate the given input date  ****************/ 
    public Date formateInputDate(String input){
            String[] dmy=input.split('/');  
            System.debug('Date Split::'+dmy+' Year::'+dmy[0]+'Month::'+dmy[1]+'Date::'+dmy[2]);
            Date fdate= date.newinstance(Integer.ValueOf(dmy[0]),Integer.ValueOf(dmy[1]),Integer.ValueOf(dmy[2])); 
            return fdate; 
    }
    
    /************** Get Selected Campaign Member Records ****************/ 
    public void getSelected()
    {
        selectedMembers.clear();
        
        System.debug('CampaignMemberList::'+MemberwrapperList);
        
        for(Memberwrapper memwrapper : MemberwrapperList)
        if(memwrapper.selected == true)
        selectedMembers.add(memwrapper.cmember);
        
        System.debug('selected Members::'+selectedMembers);
        
    }
    
    /************** Set Approve flag true for selected records ****************/
    public PageReference Approve() {
        List<campaignMember> approvedMembers =new List<campaignMember>();
        getSelected();
        System.debug('Selected Members To Approve@@'+selectedMembers);
        if(selectedMembers.isEmpty()){
            ApexPages.Message alertMsg = new ApexPages.Message(ApexPages.Severity.Info,'You have not selected any record to approve.');
            ApexPages.addMessage(alertMsg); 
        }      
        for(campaignMember cmem: selectedMembers){
            cmem.Approval_by_Campaign_Admin__c = true;
            approvedMembers.add(cmem); 
        }
        UpdateCampaignMember(approvedMembers);
        selectedMembers.clear();
        return null;
    }
    
    /************** Set Approve flag false for selected records ****************/
    Public PageReference Reject(){
       
        List<campaignMember> RejectedMembers =new List<campaignMember>();
        getSelected();
        System.debug('Selected Members To Reject@@'+selectedMembers);
        
        if(selectedMembers.isEmpty()){
            ApexPages.Message alertMsg = new ApexPages.Message(ApexPages.Severity.Info,'You have not selected any record to reject.');
            ApexPages.addMessage(alertMsg); 
        } 
        
        for(campaignMember cmem: selectedMembers){
            cmem.Approval_by_Campaign_Admin__c = false;
            RejectedMembers.add(cmem); 
        }
        UpdateCampaignMember(RejectedMembers);
        selectedMembers.clear();
        return null;
    }
    
    /************** Update selected campaign member records ****************/ 
    Public void UpdateCampaignMember(List<campaignMember> Members)
    {
        if(!Members.isEmpty() && Members.size()>0){
            update Members;
        }   
    }
    
    /************** To Get Previous Page Records ****************/ 
    public void previous() {
        PageNumber -=1;
        OffsetValue -= queryLimit;
    }
    
    /************** To Get Next Page Records ****************/
    public void next() {
        PageNumber +=1;
         OffsetValue += queryLimit;
    }
    
    /************** To Enable/Disable Previous Button ****************/
    public boolean getPreviousDisable()
    {
        if(OffsetValue == 0)
        return true;
        else
        return false;
    }
    
    /************** To Enable/Disable Next Button  ****************/
    public boolean getNextDisable()
    {
        if((OffsetValue + queryLimit) >= CurrentMemberSize)
        return true;
        else
        return false;
    }     
    
    /************** Column Sorting  ****************/
    public void toggleSort() {
        soql = 'SELECT ID, CampaignID, Campaign.Name,Contact.LastName,Contact.FirstName,Status,Campaign.Type,Approval_by_Campaign_Admin__c,Receipt_number__c,LastModifiedDate,CreatedDate,acu_CreatedDate__c,acu_LastModifiedDate__c FROM CampaignMember';        
        soql += filterQuery+' ORDER BY '+getColumn()+' '+getSortDirection()+' LIMIT : queryLimit  OFFSET : OffsetValue';
        String queryString = 'SELECT count() FROM campaignMember'+filterQuery;
        CurrentMemberSize = Database.countQuery(queryString);
        firstLoad = false;
    }
    
    /************** Get sort direction ASC/DESC ****************/
    public String getSortDirection() {
         return this.sortDirection;
    }
    
    /************** Get Column to sort  ****************/
    public String getColumn() {
        return this.column;
    }
 
    public void setColumn(String columnName) {
        if (column.equalsIgnoreCase(columnName)) {
            sortDirection = (sortDirection.equals(SORT_ASC)) ? SORT_DESC : SORT_ASC;
        } else {
            this.sortDirection = SORT_ASC;
            this.column = columnName;
         }
    }
    
    /************** Display Show Filter/Hide Filter text dynamically  ****************/
    Public Void CollapseExpand(){
        if(Collapse){
            Collapse = false;   
        }
        else{
           Collapse = true;   
       }
    }
    
    /************** Clear Filter Data  ****************/
    Public void ClearFilter(){
        SelectedField1='FieldNone';
        SelectedOperation1 ='OperationNone';
        filterValue1 ='';  
        SelectedField2='FieldNone';
        SelectedOperation2 ='OperationNone';
        filterValue2 ='';    
    }
    
    /************** Wrapper class to get selected campaign member record  ****************/
    public class Memberwrapper
    {
        public campaignMember cmember{get; set;}
        public Boolean selected {get; set;}
        public Memberwrapper(campaignMember cm)
        {
            cmember= cm;
            selected = false; 
        }
    }
    
}