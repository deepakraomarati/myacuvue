/**
* File Name: MockHttpResponseGenerator1
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  18-Oct-2018
* Description : Mock class to support callouts
* Copyright (c) $2018 Johnson & Johnson
*/
@isTest
public class MockHttpResponseGenerator1
{
	public HTTPResponse respond1()
	{
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setBody('sampleToken');
		res.setStatusCode(200);
		return res;
	}

	public HTTPResponse respond2()
	{
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String responsee = '[{"CourseType":"InstructorLedCourse","Id":"15654184-afc0-48eb-8838-12c14bf51a567","Name":"Pro.com Optician Course ILT","Description":"<p>This is a professional&nbsp;event such as an ACUVUE Eye Health Advisor event.\n</p>","Notes":null,"ExternalId":null,"AccessDate":null,"ExpireType":0,"ExpireDuration":{"Years":0,"Months":0,"Days":0,"Hours":0},"ExpiryDate":null,"ActiveStatus":0,"TagIds":["7a934842-c88a-4dfc-aa73-3c8d95074de3"],"ResourceIds":["b1c58310-16f3-4907-9b39-f0c138771303"],"EditorIds":[],"Prices":[],"CompetencyDefinitionIds":[],"PrerequisiteCourseIds":[],"PostEnrollmentCourseIds":[],"AllowCourseEvaluation":false,"CategoryId":"7f685e32-f1bd-42b3-932e-41ea40fd7b7b","CertificateUrl":null,"Audience":null,"Goals":null,"Vendor":null,"CompanyCost":null,"LearnerCost":null,"CompanyTime":null,"LearnerTime":null},{"CourseType":"Curriculum","Id":"15654184-afc0-48eb-8838-12c14bf51a56","Name":"Pro.com Optician Course ILT","Description":"<p>This is a professional&nbsp;event such as an ACUVUE Eye Health Advisor event.\n</p>","Notes":null,"ExternalId":null,"AccessDate":null,"ExpireType":0,"ExpireDuration":{"Years":0,"Months":0,"Days":0,"Hours":0},"ExpiryDate":null,"ActiveStatus":0,"TagIds":["7a934842-c88a-4dfc-aa73-3c8d95074de3"],"ResourceIds":["b1c58310-16f3-4907-9b39-f0c138771303"],"EditorIds":[],"Prices":[],"CompetencyDefinitionIds":[],"PrerequisiteCourseIds":[],"PostEnrollmentCourseIds":[],"AllowCourseEvaluation":false,"CategoryId":"7f685e32-f1bd-42b3-932e-41ea40fd7b7b","CertificateUrl":null,"Audience":null,"Goals":null,"Vendor":null,"CompanyCost":null,"LearnerCost":null,"CompanyTime":null,"LearnerTime":null}]';
		String replaceIllegal = responsee.replaceAll('\n', '').replaceAll('\r', '');
		res.setBody(replaceIllegal);
		res.setStatusCode(200);
		return res;
	}

	public HTTPResponse respond3()
	{
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String responsee = '[{"Id":"8b6c64e1-6869-4713-9eaa-e7d84bb45212","CourseId":"7f685e32-f1bd-42b3-932e-41ea40fd7b7b","CourseName":"Slit Lamp Technique Videos1","Progress":0.00,"Score":null,"Status":1,"DateCompleted":null,"DateExpires":null,"FullName":"B2B Site Guest User","JobTitle":null,"CourseVersionId":null,"UserId":"69656d88-a626-4aee-ae36-45a93cbfd00c","AcceptedTermsAndConditions":false,"TimeSpentTicks":0,"TimeSpent":"00:00:00","DateStarted":"2017-12-09T11:04:39.22","EnrollmentKeyId":null,"CertificateId":null,"Credits":null,"IsActive":true,"CourseCollectionId":"00000000-0000-0000-0000-000000000000","AccessDate":null,"DateDue":null,"Avatar":null},{"Id":"8b6ce1-6869-4713-9eaa-e7d8475bb452","CourseId":"7f685e32-f1bd-42b3-932e-41ea40fd7b7b","CourseName":"Slit Lamp Technique Videos3","Progress":0.00,"Score":null,"Status":0,"DateCompleted":null,"DateExpires":null,"FullName":"B2B Site Guest User","JobTitle":null,"CourseVersionId":null,"UserId":"69656d88-a626-4aee-ae36-45a93cbfd00c","AcceptedTermsAndConditions":false,"TimeSpentTicks":0,"TimeSpent":"00:00:00","DateStarted":"2017-12-09T11:04:39.22","EnrollmentKeyId":null,"CertificateId":null,"Credits":null,"IsActive":true,"CourseCollectionId":"00000000-0000-0000-0000-000000000000","AccessDate":null,"DateDue":null,"Avatar":null},{"Id":"8b6c64e1-68wr-4713-9eaa-e7d84bb45212","CourseId":"7f685e32-f1bd-42b3-932e-41ea40fd7b7b","CourseName":"Slit Lamp Technique Videos4","Progress":0.00,"Score":null,"Status":3,"DateCompleted":null,"DateExpires":null,"FullName":"B2B Site Guest User","JobTitle":null,"CourseVersionId":null,"UserId":"69656d88-a626-4aee-ae36-45a93cbfd00c","AcceptedTermsAndConditions":false,"TimeSpentTicks":0,"TimeSpent":"00:00:00","DateStarted":"2017-12-09T11:04:39.22","EnrollmentKeyId":null,"CertificateId":null,"Credits":null,"IsActive":true,"CourseCollectionId":"00000000-0000-0000-0000-000000000000","AccessDate":null,"DateDue":null,"Avatar":null}]';
		String replaceIllegal = responsee.replaceAll('\n', '').replaceAll('\r', '');
		res.setBody(replaceIllegal);
		res.setStatusCode(200);
		return res;
	}

	public HTTPResponse respond4()
	{
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String responsee = '{"Id":"0cbd99f0-ed03-4d68-8c48-3a647de66e4b","Username":"optician+user+545@bicsglobal.com"}';
		String replaceIllegal = responsee.replaceAll('\n', '').replaceAll('\r', '');
		res.setBody(replaceIllegal);
		res.setStatusCode(200);
		return res;
	}

	public HTTPResponse respond5()
	{
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String responsee = '[{"Id":"fec68548-1047-472d-b427-b54b2facd2ba","Name":"image","Description":null,"File":"thumbnails/ClinicialComms229x173.jpg","ModuleId":null},{"Id":"85522a4a-0c77-4f01-8d4a-7debfad6e47c","Name":"thumbnail","Description":null,"File":"thumbnails/ClinicalComms64x48.jpg","ModuleId":null}]';
		String replaceIllegal = responsee.replaceAll('\n', '').replaceAll('\r', '');
		res.setBody(replaceIllegal);
		res.setStatusCode(200);
		return res;
	}

	public HTTPResponse respond6()
	{
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String responsee = '[{"Id":"0d1c1f3f-3d8f-4cff-9429-4c887f0fb255","ChapterId":"2b9cfe63-31ef-40d5-8d2c-79cd6825940d","Name":"Golf Explained","Description":null,"Notes":null,"Type":"ThirdPartyLesson","Width":null,"Height":null,"Url":"Private/Courses/1cabb42f-54c3-4e50-815a-1a64330a9691/0kdi3ura/shared/launchpage.html","UsePopup":false,"PassingScore":null,"Weight":10.00,"RefId":null}]';
		String replaceIllegal = responsee.replaceAll('\n', '').replaceAll('\r', '');
		res.setBody(replaceIllegal);
		res.setStatusCode(200);
		return res;
	}

	public HTTPResponse respond7()
	{
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String responsee = '{"Id":"ff194dbd-8b9b-4aa3-b03c-f8c00a34EEEE","Name":"Test_Curriculum_Fr_Optician","Description":null,"Notes":null,"CategoryId":"cfbd1ea8-4e97-4f27-9ae7-7ff39167a4b7","AccessDate":null,"ExpireType":0,"ExpiryDate":null,"ActiveStatus":0,"CourseType":"Curriculum","TagIds":["3a2a09f7-ff97-43d7-847f-231746b70b60","2004b821-7954-408a-9db1-59d197c0bc67"]}';
		String replaceIllegal = responsee.replaceAll('\n', '').replaceAll('\r', '');
		res.setBody(replaceIllegal);
		res.setStatusCode(200);
		return res;
	}

	public HTTPResponse AutoEntrollResponse()
	{

		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setBody('{"EnrollmentId":"485645ee-5499-4b97-99c7-cab8cfd800c7","CourseId":"303c368f-5ea1-45e9-bd25-2bf19bd57480","UserId":"369714c2-c4e7-46e4-b3e1-0ff531678744"}');
		res.setStatusCode(201);
		return res;
	}

	public HTTPResponse responseFrCourseType()
	{
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String response = '{"CourseType": "OnlineCourse","Id": "7f685e32-f1bd-42b3-932e-41ea40fd7b7b","Name": "Fundamentals of Contact Lenses","Description": "This course introduces the different types of contact lenses, what materials they","Notes": null,"ExternalId": null,"AccessDate": "2018-07-20T06:36:32.77","ExpireType": 0,"ExpireDuration": {"Years": 0,"Months": 0,"Days": 0,"Hours": 0},"ExpiryDate": "2018-07-20T06:36:32.77","ActiveStatus": 0,"TagIds":["4efc53d3-63ed-45e2-bd5c-ed1c02c19430"],"ResourceIds": ["bb9a340d-e5c6-4e21-99db-440be0c192a3"],"EditorIds": [],"Prices": [],"CompetencyDefinitionIds": [],"PrerequisiteCourseIds": [],"PostEnrollmentCourseIds": [],"AllowCourseEvaluation": true,"CategoryId": null,"CertificateUrl": null,"Audience": "US","Goals": null,"Vendor": null,"CompanyCost": null,"StartDate":null,"CompletedDate":null,"LearnerCost": null,"CompanyTime": null,"LearnerTime": null}';
		String replaceIllegal = response.replaceAll('\n', '').replaceAll('\r', '');
		res.setBody(replaceIllegal);
		res.setStatusCode(200);
		return res;
	}
	public HTTPResponse responseFrCategories() // for Category
	{
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String response = '[{"Id":"bd689a24-2e71-4797-8bd9-114d64d8f63b","ParentId":"3c9cfe68-37rf-40d5-8d2c-79cd6855841e","Name":"Golf Category","Description":null}]';
		String replaceIllegal = response.replaceAll('\n', '').replaceAll('\r', '');
		res.setBody(replaceIllegal);
		res.setStatusCode(200);
		return res;
	}

	public HTTPResponse responseFrTags() // for Tags
	{
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String response = '[{"Id":"kj689h24-2e71-4592-1gr9-3b1c12f14a75","Name":"Golf Tags"}]';
		String replaceIllegal = response.replaceAll('\n', '').replaceAll('\r', '');
		res.setBody(replaceIllegal);
		res.setStatusCode(200);
		return res;
	}
	public HTTPResponse responseFrgetAvailabeCurriculum()
	{
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String responsee = '[{"CourseType":"Curriculum","Id":"ff194dbd-8b9b-4aa3-b03c-f8c00a34718d","Name":"Test_Curriculum_Fr_Optician -1","Description":"","Notes":null,"ExternalId":null,"AccessDate":null,"ExpireType":0,"ExpireDuration":{"Years":0,"Months":0,"Days":0,"Hours":0},"ExpiryDate":null,"ActiveStatus":0,"TagIds":["e8647744-74c6-4509-8b68-5f72d1a5cdb5","4efc53d3-63ed-45e2-bd5c-ed1c02c19430"],"ResourceIds":[],"EditorIds":[],"Prices":[],"CompetencyDefinitionIds":[],"PrerequisiteCourseIds":[],"PostEnrollmentCourseIds":[],"AllowCourseEvaluation":false,"CategoryId":"e8647744-74c6-4509-8b68-5f72d1a5cdb5","CertificateUrl":null,"Audience":null,"Goals":null,"Vendor":null,"CompanyCost":null,"LearnerCost":null,"CompanyTime":null,"LearnerTime":null},{"CourseType":"Curriculum","Id":"ff194dbd-8b9b-4aa3-b03c-f8c00a34956S","Name":"Test_Curriculum_Fr_Optician -1","Description":"","Notes":null,"ExternalId":null,"AccessDate":null,"ExpireType":0,"ExpireDuration":{"Years":0,"Months":0,"Days":0,"Hours":0},"ExpiryDate":null,"ActiveStatus":0,"TagIds":["e8647744-74c6-4509-8b68-5f72d1a5cdb5","4efc53d3-63ed-45e2-bd5c-ed1c02c19430"],"ResourceIds":[],"EditorIds":[],"Prices":[],"CompetencyDefinitionIds":[],"PrerequisiteCourseIds":[],"PostEnrollmentCourseIds":[],"AllowCourseEvaluation":false,"CategoryId":"e8647744-74c6-4509-8b68-5f72d1a5cdb5","CertificateUrl":null,"Audience":null,"Goals":null,"Vendor":null,"CompanyCost":null,"LearnerCost":null,"CompanyTime":null,"LearnerTime":null}]';
		String replaceIllegal = responsee.replaceAll('\n', '').replaceAll('\r', '');
		res.setBody(replaceIllegal);
		res.setStatusCode(200);
		return res;
	}
	public HTTPResponse responseFrGetCourseType()
	{
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String responsee = '{"Id":"ff194dbd-8b9b-4aa3-b03c-f8c00a34956d","Name":"Test_Curriculum_Fr_Optician","Description":null,"Notes":null,"CategoryId":"e8647744-74c6-4509-8b68-5f72d1a5cdb5","AccessDate":null,"ExpireType":0,"ExpiryDate":null,"ActiveStatus":0,"CourseType":"Curriculum","TagIds":["e8647744-74c6-4509-8b68-5f72d1a5cdb5","2004b821-7954-408a-9db1-59d197c0bc67"]}';
		String replaceIllegal = responsee.replaceAll('\n', '').replaceAll('\r', '');
		res.setBody(replaceIllegal);
		res.setStatusCode(200);
		return res;
	}
	public HTTPResponse responseForCurriculumEnroll()
	{
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String responsee = '[{"Id":"81eb0c5f-61a1-46a3-ad20-a0c96f745c68","CourseId":"ff194dbd-8b9b-4aa3-b03c-f8c00a34956d","CourseName":"Test_Curriculum_Fr_Optician -1","Progress":0,"Score":null,"Status":3,"DateCompleted":null,"DateStarted":"2018-07-20T06:36:32.77"},{"Id":"81eb0c5f-61a1-46a3-ad20-a0c96f745c63","CourseId":"ff194dbd-8b9b-4aa3-b03c-f8c00a34956d","CourseName":"Test_Curriculum_Fr_Optician -1","Progress":0,"Score":null,"Status":1,"DateCompleted":null,"DateStarted":"2018-07-20T06:36:32.77"}]';
		String replaceIllegal = responsee.replaceAll('\n', '').replaceAll('\r', '');
		res.setBody(replaceIllegal);
		res.setStatusCode(200);
		return res;
	}
	public HTTPResponse responseFrgetResourcesForCourse()
	{
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String responsee = '[{"Id":"f6349ff8-4fd1-4433-9556-65df95e39828","Name":"Online_CoursePage_Image","Description":null,"File":"Koala.jpg","ModuleId":null},{"Id":"7a7ddd2d-b5b3-4edc-8014-e46bc7f48e3b","Name":"Related_Content_Image","Description":null,"File":"Hydrangeas.jpg","ModuleId":null},{"Id":"a210e44a-3ab1-44a8-8b42-b05dc3417038","Name":"Landing_page_Image","Description":null,"File":"Jellyfish.jpg","ModuleId":null},{"Id":"e6f2639a-6de7-4e0b-909c-d2d19d3996c3","Name":"Main_Course_Image","Description":null,"File":"Penguins.jpg","ModuleId":null}]';
		String replaceIllegal = responsee.replaceAll('\n', '').replaceAll('\r', '');
		res.setBody(replaceIllegal);
		res.setStatusCode(200);
		return res;
	}
	public HTTPResponse respondCourseDTO()
	{
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		String responsee = '[{"Id": "8b6c64e1-6869-4713-9eaa-e7d84bb45212","CourseId": "7f685e32-f1bd-42b3-932e-41ea40fd7b7b","CourseName": "Slit Lamp Technique Videos2","Progress": 0.00,"Score": null,"Status": 1,"DateCompleted": null,"DateExpires": null,"FullName": "B2B Site Guest User","JobTitle": null,"CourseVersionId": null,"UserId": "69656d88-a626-4aee-ae36-45a93cbfd00c","AcceptedTermsAndConditions": false,"TimeSpentTicks": 0,"TimeSpent": "00:00:00","DateStarted": "2017-12-09T11:04:39.22","EnrollmentKeyId": null,"CertificateId": null,"Credits": null,"IsActive": true,"CourseCollectionId": "00000000-0000-0000-0000-000000000000","AccessDate": null,"DateDue": null,"Avatar": null}]';
		String replaceIllegal = responsee.replaceAll('\n', '').replaceAll('\r', '');
		res.setBody(replaceIllegal);
		res.setStatusCode(200);
		return res;
	}
}