/**
 * File Name: MockHttpResponseGenerator2 
 * Description : Test Class to generate HTTP Response for class LocationCallouts
 * Copyright : Johnson & Johnson
 * @author : Shobana K G | skg6@its.jnj.com | Shobana.KG@cognizant.com
 * 
 * Modification Log 
 * =============================================================== 
 *  Ver  |Date         |Author                |Modification
 *  1.0  |30-Aug-2018  |skg6@its.jnj.com      |New Class created
 */

@isTest
global class MockHttpResponseGenerator2 {
    
    global HTTPResponse responseGoogleAPI()
    {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        String responseBody = '[{"geometry" : {"location" : {"lat" : 1.320446,"lng" : 103.84445}}}]';
        res.setBody(responseBody);
        res.setStatusCode(200);
        return res;    
    }
    
}