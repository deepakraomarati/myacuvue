/**
* File Name: MyAccountCurriculumService
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : Get curriculums information and related data from LMS system associated to user role
* Copyright (c) $2018 Johnson & Johnson
*/
@RestResource(urlMapping='/apex/myaccount/v1/CurriculumService/*')
global without sharing class MyAccountCurriculumService
{
	global static final String EMAIL_ERROR = 'PC0011';
	global static final String ERROR_TYPE = 'CurriculumService' ;
	global static final String STATUS_ERROR = 'PC0013';
	global static final String PARAMETER_ERROR = 'PC0014';
	global static final String GENERIC_ERROR = 'GEN0001';
	global static final String GENERIC_ERROR_TYPE = 'Generic';
	global static final String USERID_ERROR = 'PC0019';
	/**
	* Description : When this method called, System will get curriculums information from LMS system. 
	*/
	@HttpGet
	global static String doGet()
	{
		RestRequest req = RestContext.request;
		try
		{
			Set<String> setForStatusCheck = new Set<String>();
			setForStatusCheck.add('all');
			setForStatusCheck.add('completion');
			setForStatusCheck.add('inprogress');
			String status;
			List<String> listURI = req.requestURI.split('/');
			if (listURI.size() != 7)
			{
				throw new CustomException(LmsUtils.getException(PARAMETER_ERROR, ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0014').Error_Description__c)));
			}
			String userId = listURI[5];
			if (userId == '' || userId == null)
			{
				throw new CustomException(LmsUtils.getException(PARAMETER_ERROR, ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0014').Error_Description__c)));
			}
			else if (setForStatusCheck.contains(listURI[6]))
			{
				status = LmsUtilsForCurriculum.getEnrollments(userId, listURI[6]);
			}
			else
			{
				throw new CustomException(LmsUtils.getException(STATUS_ERROR, ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0013').Error_Description__c)));
			}
			return status;
		}
		catch (Exception e)
		{
			return e.getMessage();
		}
	}
}