/**
* File Name: MyAccountCurriculumServiceList
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : Get curriculums & related information based on input language
* Copyright (c) $2018 Johnson & Johnson
*/
@RestResource(urlMapping='/apex/myaccount/v1/CurriculumServiceList/*')
global without sharing class MyAccountCurriculumServiceList
{
	global static final String ERROR_TYPE = 'CurriculumService' ;
	global static final String REQUIRED = 'PC0014';
	global static final String GENERIC_ERROR = 'GEN0001';
	global static final String GENERIC_ERROR_TYPE = 'Generic';
	global static final String PARAMETER_ERROR_TYPE = 'CurriculumService';

	/**
	* Description : When this method called, System will get the curriculums,Tags & images information for the given input language. 
	*/
	@HttpGet
	global static string doGet()
	{
		RestRequest holdsrequest = RestContext.request;
		try
		{
			String language = holdsrequest.requestURI.substring(holdsrequest.requestURI.lastIndexOf('/') + 1);
			if (language == null || language == '')
			{
				throw new CustomException(B2B_Utils.getException(REQUIRED, ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0014').Error_Description__c)));
			}
			else
			{
				List<DTOForListvalues> listForCurriculum = new List<DTOForListvalues >();
				Map<String, Curriculum__c> mapForCurriculum = new Map<String, Curriculum__c>();
				List<Curriculum__c> listForCurriculumMapping = [SELECT Id,Name, LMS_ID__c,Description__c,Duration__c,Main_Course_Image__c,Related_Content_Image__c,Landing_page_Image__c,Online_CoursePage_Image__c,Type__c,CategoryId__c,Language__c, (SELECT id,courseTagAssignedId__c,CustomCourse__c,CustomCurriculum__c,CustomTag__c FROM CourseTagAssignments__r) FROM Curriculum__c WHERE Language__c = :language AND ActiveStatus__c = 'Active'];
				for (Curriculum__c CurriculumObjVarA : listForCurriculumMapping)
				{
					mapForCurriculum.put(CurriculumObjVarA.LMS_ID__c, CurriculumObjVarA);
				}
				Map<String, String> mapFrCategory = new Map<String, String>();
				Map<String, tag> mapFrTags = new Map<String, tag>();
				List<Categories_Tag__c> lstFrCategory = [SELECT LMS_ID__c, Name FROM Categories_Tag__c where Type__c = 'Category'];
				List<Categories_Tag__c> lstFrTag = [SELECT LMS_ID__c, Name FROM Categories_Tag__c where Type__c = 'Tag'];
				for (Categories_Tag__c lstFrCategoryObj : lstFrCategory)
				{
					mapFrCategory.put(lstFrCategoryObj.LMS_ID__c, lstFrCategoryObj.Name);
				}
				for (Categories_Tag__c lstFrTagObj : lstFrTag)
				{
					Tag holdsTag = new Tag();
					holdsTag.Id = lstFrTagObj.LMS_ID__c;
					holdsTag.Name = lstFrTagObj.Name;
					mapFrTags.put(lstFrTagObj.Id, holdsTag);
				}
				for (Curriculum__c CurriculumDTOObjj : listForCurriculumMapping)
				{
					List<image> images = new List<image>();
					List<Category> CategoryWithName = new List<Category>();
					List<tag> TagWithName = new List<tag>();
					if (mapForCurriculum.get(CurriculumDTOObjj.LMS_ID__c) != null)
					{
						if (mapForCurriculum.get(CurriculumDTOObjj.LMS_ID__c).Main_Course_Image__c != null)
						{
							image img1 = new image();
							img1.Name = 'Main_Course_Image';
							img1.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCurriculum.get(CurriculumDTOObjj.LMS_ID__c).Main_Course_Image__c;
							images.add(img1);
						}
						if (mapForCurriculum.get(CurriculumDTOObjj.LMS_ID__c).Related_Content_Image__c != null)
						{
							image img2 = new image();
							img2.Name = 'Related_Content_Image';
							img2.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCurriculum.get(CurriculumDTOObjj.LMS_ID__c).Related_Content_Image__c;
							images.add(img2);
						}
						if (mapForCurriculum.get(CurriculumDTOObjj.LMS_ID__c).Landing_page_Image__c != null)
						{
							image img3 = new image();
							img3.Name = 'Landing_page_Image';
							img3.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCurriculum.get(CurriculumDTOObjj.LMS_ID__c).Landing_page_Image__c;
							images.add(img3);
						}
						if (mapForCurriculum.get(CurriculumDTOObjj.LMS_ID__c).Online_CoursePage_Image__c != null)
						{
							image img4 = new image();
							img4.Name = 'Online_CoursePage_Image';
							img4.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCurriculum.get(CurriculumDTOObjj.LMS_ID__c).Online_CoursePage_Image__c;
							images.add(img4);
						}
					}
					if (mapForCurriculum.get(CurriculumDTOObjj.LMS_ID__c).CategoryId__c != null)
					{
						Category ObjFrCategory = new Category();
						ObjFrCategory.Id = mapForCurriculum.get(CurriculumDTOObjj.LMS_ID__c).CategoryId__c;
						ObjFrCategory.Name = mapFrCategory.get(mapForCurriculum.get(CurriculumDTOObjj.LMS_ID__c).CategoryId__c);
						CategoryWithName.add(ObjFrCategory);
					}
					if (!CurriculumDTOObjj.CourseTagAssignments__r.isEmpty() && CurriculumDTOObjj.CourseTagAssignments__r.size() > 0)
					{
						for (CourseTagAssignment__c holdsCourseTagAssignment : CurriculumDTOObjj.CourseTagAssignments__r)
						{
							Tag ObjFrtag = mapFrTags.get(holdsCourseTagAssignment.CustomTag__c);
							TagWithName.add(ObjFrtag);
						}
					}
					DTOForListvalues DTOForListvalue = new DTOForListvalues();
					DTOForListvalue.Name = CurriculumDTOObjj.Name;
					DTOForListvalue.LMSID = CurriculumDTOObjj.LMS_ID__c;
					DTOForListvalue.Description = CurriculumDTOObjj.Description__c;
					DTOForListvalue.Images = images;
					DTOForListvalue.Duration = CurriculumDTOObjj.Duration__c;
					DTOForListvalue.CurriculumId = CurriculumDTOObjj.Id;
					DTOForListvalue.Type = CurriculumDTOObjj.Type__c;
					DTOForListvalue.Language = CurriculumDTOObjj.Language__c;
					DTOForListvalue.Category = CategoryWithName;
					DTOForListvalue.Tags = TagWithName;
					listForCurriculum.add(DTOForListvalue);
				}
				return JSON.serializePretty(listForCurriculum);
			}
		}
		catch (Exception e)
		{
			return e.getmessage();
		}
	}
	public class DTOForListvalues
	{
		public String Type { get; set; }
		public List<tag> Tags { get; set; }
		public String Name { get; set; }
		public String LMSID { get; set; }
		public String Language { get; set; }
		public List<image> Images { get; set; }
		public String Duration { get; set; }
		public String Description { get; set; }
		public String CurriculumId { get; set; }
		public List<Category> Category { get; set; }
	}
	public class tag
	{
		public String Id { get; set; }
		public String Name { get; set; }
	}
	public class image
	{
		public String Name { get; set; }
		public String URL { get; set; }
	}
	public class Category
	{
		public String Id { get; set; }
		public String Name { get; set; }
	}
}