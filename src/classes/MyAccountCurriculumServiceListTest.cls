/**
* File Name: MyAccountCurriculumServiceListTest
* Description : Test class for MyAccountCurriculumServiceList class
* Copyright (c) $2018 Johnson & Johnson
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
*/
@isTest(SeeAlldata=false)
public class MyAccountCurriculumServiceListTest
{

	static testmethod void test_getCurriculumbylocale()
	{
		B2B_Custom_Exceptions__c holdsCustomException = new B2B_Custom_Exceptions__c();
		holdsCustomException.Name = 'PC0014';
		holdsCustomException.Error_Description__c = 'Required parameters missing.';
		insert holdsCustomException;

		LMS_Settings__c LmsCustomSettings1 = new LMS_Settings__c();
		LmsCustomSettings1.Name = 'ImageURL';
		LmsCustomSettings1.Value__c = 'https://jjvus.sandbox.myabsorb.com/Files/';
		insert LmsCustomSettings1;

		Categories_Tag__c tag = new Categories_Tag__c();
		tag.Description__c = 'Test Tag5';
		tag.LMS_ID__c = '7a934842-c88a-4dfc-aa73-3c8d55074de3';
		tag.Parent_Id__c = 'ty642f42-e49d-r78t-8w2a-4r85a7t78se8';
		tag.Type__c = 'Tag';
		insert tag;

		Categories_Tag__c tag1 = new Categories_Tag__c();
		tag1.Description__c = 'Test Tag' ;
		tag1.LMS_ID__c = '7a934842-c88a-4dfc-aa73-3c8d74174de4';
		tag1.Parent_Id__c = 'ty642f24-e74d-r78t-8w2a-4r85a7t78se8';
		tag1.Type__c = 'Tag';
		insert tag1;

		Categories_Tag__c holdsCategory = new Categories_Tag__c();
		holdsCategory.Description__c = 'Test cat' ;
		holdsCategory.LMS_ID__c = '7a934842-c99a-4dfc-aa73-3c8d74174de4';
		holdsCategory.Parent_Id__c = 'ty642f24-e74d-r78t-8w2a-4r85a7t78se8';
		holdsCategory.Type__c = 'Category';
		insert holdsCategory;

		Curriculum__c curriculumObj = new Curriculum__c();
		curriculumObj.Name = 'Test Course1';
		curriculumObj.ActiveStatus__c = 'Active';
		curriculumObj.CategoryId__c = '15654144-afc0-48eb-8838';
		curriculumObj.Main_Course_Image__c = 'dgdddgd';
		curriculumObj.Landing_page_Image__c = 'csdfsfffg';
		curriculumObj.Online_CoursePage_Image__c = 'sddfffg';
		curriculumObj.Related_Content_Image__c = 'df5CdffWE';
		curriculumObj.Description__c = 'some test data';
		curriculumObj.ExpireDuration__c = 'test ExpireDuration';
		curriculumObj.LMS_ID__c = '7a934842-c88a-4dfc-aa33-3c8d95074de3';
		curriculumObj.Language__c = 'ja_JP';
		curriculumObj.Type__c = 'OnlineCourse';
		curriculumObj.Notes__C = 'tets note';
		insert curriculumObj;

		CourseTagAssignment__c coursetag = new CourseTagAssignment__c();
		coursetag.Name = 'Test1';
		coursetag.CustomTag__c = tag1.id;
		coursetag.CustomCurriculum__c = curriculumObj.id;
		coursetag.courseTagAssignedId__c = 'Test';
		insert coursetag;

		MyAccountCurriculumServiceList.DTOForListvalues holdsCurriculum = new MyAccountCurriculumServiceList.DTOForListvalues();
		holdsCurriculum.Type = 'Curriculum';
		holdsCurriculum.Name = 'Test';
		holdsCurriculum.LMSID = '7a934842-c99a-4dfc-aa73-3c8d74174de4';
		holdsCurriculum.Language = 'ja_JP';
		holdsCurriculum.Duration = '10.00';
		holdsCurriculum.Description = 'test';
		holdsCurriculum.CurriculumId = curriculumObj.id;

		MyAccountCurriculumServiceList.image holdsimage = new MyAccountCurriculumServiceList.image();
		holdsimage.Name = 'test';
		holdsimage.URL = 'testurl';

		RestRequest firstRequest = new RestRequest();
		RestResponse firstResponse = new RestResponse();
		firstRequest.requestURI = '/apex/myaccount/v1/CurriculumServiceList/' + 'ja_JP';
		firstRequest.httpMethod = 'GET';
		RestContext.request = firstRequest;
		RestContext.response = firstResponse ;
		string curriculumresponse = MyAccountCurriculumServiceList.doGet();
		system.assertNotEquals(curriculumresponse, null);

		RestRequest secondRequest = new RestRequest();
		RestResponse secondResponse = new RestResponse();
		secondRequest.requestURI = '/apex/myaccount/v1/CurriculumServiceList/';
		secondRequest.httpMethod = 'GET';
		RestContext.request = secondRequest;
		RestContext.response = secondResponse;
		string errorresponse = MyAccountCurriculumServiceList.doGet();
		system.assertNotEquals(errorresponse, null);
	}
}