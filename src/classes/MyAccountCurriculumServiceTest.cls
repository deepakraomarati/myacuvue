/**
* File Name: MyAccountCurriculumServiceTest
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : Test class for MyAccountCurriculumService class
* Copyright (c) $2018 Johnson & Johnson
*/
@isTest(SeeAlldata=false)
private class MyAccountCurriculumServiceTest
{
	static testmethod void Test_Getcurriculumsbyallstatus()
	{
		User holdsTestUser = insertSettings();

		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/apex/myaccount/v1/CurriculumService/' + holdsTestUser.id + '/all';
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;

		string responseGet = MyAccountCurriculumService.doGet();
		system.assertNotEquals(responseGet, null);

		RestRequest req_progress = new RestRequest();
		RestResponse res_progress = new RestResponse();
		req_progress.requestURI = '/apex/myaccount/v1/CurriculumService/' + holdsTestUser.id + '/inprogress';
		req_progress.httpMethod = 'GET';
		RestContext.request = req_progress;
		RestContext.response = res_progress;
		System.runAs(holdsTestUser)
		{
			string response = MyAccountCurriculumService.doGet();
			system.assertNotEquals(response, null);
		}

		RestRequest req_compl = new RestRequest();
		RestResponse res_compl = new RestResponse();
		req_compl.requestURI = '/apex/myaccount/v1/CurriculumService/' + holdsTestUser.id + '/completion';
		req_compl.httpMethod = 'GET';
		RestContext.request = req_compl;
		RestContext.response = res_compl;
		System.runAs(holdsTestUser)
		{
			string response = MyAccountCurriculumService.doGet();
			system.assertNotEquals(response, null);
		}

		RestRequest req_catchScenario_1 = new RestRequest();
		RestResponse res_catchScenario_1 = new RestResponse();
		req_catchScenario_1.requestURI = '/apex/myaccount/v1/CurriculumService/' + holdsTestUser.id;
		req_catchScenario_1.httpMethod = 'GET';
		RestContext.request = req_catchScenario_1;
		RestContext.response = res_catchScenario_1;
		System.runAs(holdsTestUser)
		{
			string response = MyAccountCurriculumService.doGet();
			system.assertNotEquals(response, null);
		}

		RestRequest req_catchScenario_2 = new RestRequest();
		RestResponse res_catchScenario_2 = new RestResponse();
		req_catchScenario_2.requestURI = '/apex/myaccount/v1/CurriculumService//completion';
		req_catchScenario_2.httpMethod = 'GET';
		RestContext.request = req_catchScenario_2;
		RestContext.response = res_catchScenario_2;
		System.runAs(holdsTestUser)
		{
			string response = MyAccountCurriculumService.doGet();
			system.assertNotEquals(response, null);
		}

		RestRequest req_catchScenario_3 = new RestRequest();
		RestResponse res_catchScenario_3 = new RestResponse();
		req_catchScenario_3.requestURI = '/apex/myaccount/v1/CurriculumService/' + holdsTestUser.id + '/DummyStatus';
		req_catchScenario_3.httpMethod = 'GET';
		RestContext.request = req_catchScenario_3;
		RestContext.response = res_catchScenario_3;
		System.runAs(holdsTestUser)
		{
			string response = MyAccountCurriculumService.doGet();
			system.assertNotEquals(response, null);
		}
	}

	static User insertSettings()
	{
		List<sObject> B2BCE = Test.loadData(B2B_Custom_Exceptions__c.sObjectType, 'B2BCustomexceptions');
		List<sObject> PLS = Test.loadData(Portal_Settings__c.sObjectType, 'portalsettings');
		TestinsertSettings();

		Role__c RL = new Role__c();
		RL.Name = '医師';
		RL.Approval_Required__c = true;
		RL.Country__c = 'Japan';
		RL.Type__c = 'Staff';
		insert RL;

		LMS_Settings__c LmsCustomSettings6 = new LMS_Settings__c();
		LmsCustomSettings6.Name = 'ja_JP-' + RL.id;
		LmsCustomSettings6.Value__c = ' 20ad397c-de1d-4165-967c-84b3110566cc';
		insert LmsCustomSettings6;

		Account A = new Account();
		A.Name = 'Generic consumer Account';
		A.shippingcountry = 'United States';
		A.shippingstreet = 'Test Street';
		A.shippingcity = 'Test city';
		A.shippingpostalcode = 'TEST20017';
		insert A;

		new LmsUtilsForCurriculum.CurriculumDTO();

		Portal_Settings__c GA = new Portal_Settings__c();
		GA.Name = 'GenericAccount Id';
		GA.Value__c = A.Id;
		insert GA;

		Contact cc = new Contact();
		cc.FirstName = 'Acuvue';
		cc.LastName = 'Pro user';
		cc.Salutation = 'Mr.';
		cc.Email = 'Protestuser+curriculum@gmail.com';
		cc.School_Name__c = 'Test';
		cc.Graduation_Year__c = '2014';
		cc.Degree__c = 'Test';
		cc.BirthDate = date.today();
		cc.AccountId = A.iD;
		cc.User_Role__c = RL.Id;
		insert cc;

		User u = new User();
		u.Title = 'Mr.';
		u.FirstName = 'Acuvue';
		u.LastName = 'Pro user';
		u.Email = 'Protestuser+acuvue_Learning@gmail.com';
		u.UserName = 'Protestuser+acuvue_Learning@gmail.com';
		u.Occupation__c = RL.Name;
		u.Secret_Question__c = 'birth_month';
		u.SecretAnswerSalt__c = RegistrationService.generateSalt();
		Blob hash = Crypto.generateDigest('SHA-512', Blob.valueOf('09/09/1992' + u.SecretAnswerSalt__c));
		u.Secret_Answer__c = EncodingUtil.base64Encode(hash);
		u.Communication_Agreement__c = true;
		u.Localesidkey = 'ja_JP';
		u.Languagelocalekey = 'ja';
		u.EmailEncodingKey = 'ISO-8859-1';
		u.Alias = (u.LastName.length() > 7) ? u.LastName.substring(0, 7) : u.LastName;
		u.TimeZoneSidKey = 'America/New_York';
		u.CommunityNickname = B2B_Utils.generateGUID();
		u.NPINumber__c = 'TESTPRO01235';
		u.ContactId = cc.Id;
		u.ProfileId = Portal_Settings__c.getValues('ProfileId').Value__c;
		u.LMS_ID__c = '0cbd99f0-ed03-4d68-8c48-3a647de66e4b';
		u.Unique_User_Id__c = cc.id;
		insert u;
		return u;
	}

	static void TestinsertSettings()
	{
		Categories_Tag__c holdsCategory = new Categories_Tag__c();
		holdsCategory.Name = 'Categorytag';
		holdsCategory.LMS_ID__c = 'e8647744-74c6-4509-8b68-5f72d1a5cdb5';
		holdsCategory.Type__c = 'Category';
		insert holdsCategory;

		Categories_Tag__c holdsTag = new Categories_Tag__c();
		holdsTag.Name = 'Category1';
		holdsTag.LMS_ID__c = 'e8647744-74c6-4509-8b68-5f72d1a5cdb5';
		holdsTag.Type__c = 'Tag';
		insert holdsTag;

		Curriculum__c Curriculum = new Curriculum__c();
		Curriculum.ActiveStatus__c = 'Active';
		Curriculum.CategoryId__c = 'tyth7j18j4';
		Curriculum.Main_Course_Image__c = 'dggd';
		Curriculum.Landing_page_Image__c = 'csdfsg';
		Curriculum.Online_CoursePage_Image__c = 'sdfg';
		Curriculum.Related_Content_Image__c = 'df5CWE';
		Curriculum.Description__c = 'some test data';
		Curriculum.ExpireDuration__c = 'test ExpireDuration';
		Curriculum.LMS_ID__c = 'ff194dbd-8b9b-4aa3-b03c-f8c00a34718d';
		Curriculum.Type__c = 'OnlineCourse';
		Curriculum.Notes__C = 'tets note';
		insert Curriculum;

		LMS_Settings__c LmsCustomSettings = new LMS_Settings__c();
		LmsCustomSettings.Name = 'EndPoint';
		LmsCustomSettings.Value__c = 'https://jjvus.sandbox.myabsorb.com/api/Rest/';
		insert LmsCustomSettings;

		LMS_Settings__c LmsCustomSettings1 = new LMS_Settings__c();
		LmsCustomSettings1.Name = 'ImageURL';
		LmsCustomSettings1.Value__c = 'https://jjvci.sandbox.myabsorb.com/Files/';
		insert LmsCustomSettings1;

		LMS_Settings__c LmsCustomSettings2 = new LMS_Settings__c();
		LmsCustomSettings2.Name = 'Password';
		LmsCustomSettings2.Value__c = 'Test.123';
		insert LmsCustomSettings2;

		LMS_Settings__c LmsCustomSettings3 = new LMS_Settings__c();
		LmsCustomSettings3.Name = 'PrivateKey';
		LmsCustomSettings3.Value__c = '58091a9c-7077-40ee-927b-143ea1af810a';
		insert LmsCustomSettings3;

		LMS_Settings__c LmsCustomSettings4 = new LMS_Settings__c();
		LmsCustomSettings4.Name = 'Username';
		LmsCustomSettings4.Value__c = 'prorestadmin';
		insert LmsCustomSettings4;

		LMS_Settings__c LmsCustomSettings5 = new LMS_Settings__c();
		LmsCustomSettings5.Name = 'DEPT_ID_ja_JP';
		LmsCustomSettings5.Value__c = '20bf217e-41a7-44b3-a1c7-2e084a44d936';
		insert LmsCustomSettings5;

		LMS_Settings__c LmsCustomSettings7 = new LMS_Settings__c();
		LmsCustomSettings7.Name = 'ja_JP';
		LmsCustomSettings7.Value__c = '1';
		insert LmsCustomSettings7;

		LMS_Settings__c LmsCustomSettings8 = new LMS_Settings__c();
		LmsCustomSettings8.Name = 'Token';
		LmsCustomSettings8.Value__c = 'TEST TOKEN';
		insert LmsCustomSettings8;

		B2B_Custom_Exceptions__c BCE1 = new B2B_Custom_Exceptions__c();
		BCE1.Name = 'PC0013';
		BCE1.Error_Description__c = 'Invalid filter parameter.';
		insert BCE1;

		B2B_Custom_Exceptions__c BCE2 = new B2B_Custom_Exceptions__c();
		BCE2.Name = 'PC0014';
		BCE2.Error_Description__c = 'Required parameters missing.';
		insert BCE2;

		B2B_Custom_Exceptions__c BCE3 = new B2B_Custom_Exceptions__c();
		BCE3.Name = 'PC0011';
		BCE3.Error_Description__c = 'Required field UserId missing.';
		insert BCE3;

		B2B_Custom_Exceptions__c BCE4 = new B2B_Custom_Exceptions__c();
		BCE4.Name = 'PC0019';
		BCE4.Error_Description__c = 'The given UserId is invalid';
		insert BCE4;

		B2B_Custom_Exceptions__c BCE5 = new B2B_Custom_Exceptions__c();
		BCE5.Name = 'GEN0001';
		BCE5.Error_Description__c = 'An unexpected error has occurred. Please contact your System Administrator.';
		insert BCE5;
	}

	static testmethod void test_invokehttpmethosd()
	{
		insertSettings();
		HttpResponse response = LmsUtils.processRequest('https://jjvus.sandbox.myabsorb.com/api/Rest/', 'Msg', 'action', 'sessionId');
		system.assertNotEquals(response, null);
		LmsUtils.processRequest('https://jjvus.sandbox.myabsorb.com/api/Rest/', 'Msg', 'POST', 'sessionId');
		LmsUtils.processRequest('https://jjvus.sandbox.myabsorb.com/api/Rest/', 'Msg', 'GET', 'sessionId');
	}
}