/**
* File Name: MyAccountInvitation
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  18-Oct-2018
* Description : Allow CMS Admins & Account Owners to create staff users with LMS access directly without aksing them to register.
* Copyright (c) $2018 Johnson & Johnson
*/
@RestResource(URLMapping='/apex/myaccount/v1/MyAccountInvitation/*')
global without sharing class MyAccountInvitation extends BaseRestResponse
{
	/**
    * @description  create users based on input data
    */
	@HttpPost
	global static BaseRestResponse uploadusers()
	{
		String sessionId = LmsUtils.authenticate();
		Savepoint rollbackPoint = Database.setSavepoint();
		try
		{
			Map<String, Integer> checkDupEmails = new Map<String, Integer>();
			Map<String, String> failureRecords = new Map<String, String>();
			List<ID> contactIds = new List<ID>();
			List<Contact> insertcon = new List<Contact>();
			List<User> usersBuild = new List<User>();
			List<User> insertUsers = new List<User>();
			Map<String, Contact> contactEmailMap = new Map<String, Contact>();
			List<Useruploadresult> resultBuild = new List<Useruploadresult>();
			List<String> allEmails = new List<String>();
			List<String> allRoles = new List<String>();
			Map<String, User> checkUsersExist = new Map<String, User>();
			Map<String, Integer> checkAccountExist = new Map<String, Integer>();
			Map<String, Account> accountNumberMap = new Map<String, Account>();
			Map<String, Role__c> roleMap = new Map<String, Role__c>();
			List<String> sapNumber = new List<String>();
			List<User> lmsEnableUsers = new List<User>();
			Map<String, Boolean> lmsAccess = new Map<String, Boolean>();
			Map<String, Boolean> hasManager = new Map<String, Boolean>();
			Map<String, Integer> checkManagerExist = new Map<String, Integer>();
			Results uploadResult = new Results();
			RestRequest req = RestContext.request;
			Blob Body = req.requestBody;
			Massusers users = parse(Body.tostring());
			for (UsersDTO user : users.UsersDTO)
			{
				/*Storing all data into variables in order use them in next steps*/
				allEmails.add(user.EmailAddress);
				if (user.SAPAccountNumber != null && user.SAPAccountNumber != '')
				{
					sapNumber.add(user.SAPAccountNumber);
				}
				allRoles.add(user.Role);
				lmsAccess.put(user.EmailAddress, user.EnableLMS);
				if (checkDupEmails.containskey(user.EmailAddress))
				{
					checkDupEmails.put(user.EmailAddress, 2);
				}
				else
				{
					checkDupEmails.put(user.EmailAddress, 1);
				}
			}
			List<Role__c> roleList = [SELECT Id, Name, Country__c, Type__c FROM Role__c WHERE Name IN :allRoles];
			for (Role__c role : roleList)
			{
				roleMap.put(role.Name, role);
			}
			for (UsersDTO user : users.UsersDTO)
			{
				if (roleMap.get(user.Role).Type__c == 'Manager')
				{
					if (checkManagerExist.containskey(user.SAPAccountNumber))
					{
						checkManagerExist.put(user.SAPAccountNumber, 2);
					}
					else
					{
						checkManagerExist.put(user.SAPAccountNumber, 1);
					}
				}
			}
			List<Account> accountList =
			[
					SELECT ID, OutletNumber__c, shippingpostalcode, shippingstreet, shippingcity, shippingcountry, shippingstate,
					(SELECT ID, Contact__c, Account__c, Account_Ownership_Flag__c FROM AccountContactRoles__r WHERE Account_Ownership_Flag__c = True)
					FROM Account
					WHERE OutletNumber__c IN :sapNumber
			];
			for (Account account : accountList)
			{
				accountNumberMap.put(account.OutletNumber__c, account);
				if (account.AccountContactRoles__r.size() > 0)
				{
					hasManager.put(account.OutletNumber__c, true);
				}
				if (checkAccountExist.containskey(account.OutletNumber__c))
				{
					checkAccountExist.put(account.OutletNumber__c, 2);
				}
				else
				{
					checkAccountExist.put(account.OutletNumber__c, 1);
				}
			}
			List<User> userList = [SELECT ID, Name, email, Username, LMS_id__C, contactid FROM User WHERE username IN :allEmails];
			for (User user : userList)
			{
				checkUsersExist.put(user.username, user);
			}
			JJVC_Contact_Recordtype__mdt recType = [SELECT MasterLabel FROM JJVC_Contact_Recordtype__mdt WHERE DeveloperName = 'HCP_Contacts_RecordtypeId'];
			for (UsersDTO massUserData : users.UsersDTO)
			{
				Boolean userexist = false;
				Boolean throwerror = false;
				if ((massUserData.FirstName == '' || massUserData.FirstName == null) && throwerror == false)
				{
					throwerror = true;
					failureRecords.put(massUserData.username, 'PC0005');
				}
				if ((massUserData.LastName == '' || massUserData.LastName == null) && throwerror == false)
				{
					throwerror = true;
					failureRecords.put(massUserData.username, 'PC0006');
				}
				if (((massUserData.Username == '' || massUserData.Username == null) || (massUserData.EmailAddress == '' || massUserData.EmailAddress == null)) && throwerror == false)
				{
					throwerror = true;
					failureRecords.put(massUserData.username, 'PC0015');
				}
				if (checkUsersExist.containskey(massUserData.EmailAddress))
				{
					if (checkUsersExist.get(massUserData.EmailAddress).Lms_id__c != '' && checkUsersExist.get(massUserData.EmailAddress).Lms_id__c != null)
					{
						throwerror = true;
						failureRecords.put(massUserData.username, 'PC0001');
					}
					else
					{
						if (massUserData.EnableLMS)
						{
							userexist = true;
							lmsEnableUsers.add(checkUsersExist.get(massUserData.EmailAddress));
						}
						else
						{
							throwerror = true;
							failureRecords.put(massUserData.username, 'PC0001');
						}
					}
				}
				if (checkDupEmails.containskey(massUserData.EmailAddress) && checkDupEmails.get(massUserData.EmailAddress) == 2 && throwerror == false)
				{
					throwerror = true;
					failureRecords.put(massUserData.username, 'PC0032');
				}
				if ((massUserData.SAPAccountNumber != null && massUserData.SAPAccountNumber != '') && throwerror == false)
				{
					if (checkAccountExist.containskey(massUserData.SAPAccountNumber))
					{
						if (checkAccountExist.get(massUserData.SAPAccountNumber) == 2 && throwerror == false)
						{
							throwerror = true;
							failureRecords.put(massUserData.username, 'PC0031');
						}
					}
					else
					{
						throwerror = true;
						failureRecords.put(massUserData.username, 'PC0030');
					}
				}
				if (!roleMap.containskey(massUserData.role) && throwerror == false)
				{
					throwerror = true;
					failureRecords.put(massUserData.username, 'PC0016');
				}
				if (roleMap.get(massUserData.Role).Type__c == 'Manager' && checkManagerExist.containskey(massUserData.SAPAccountNumber) && checkManagerExist.get(massUserData.SAPAccountNumber) == 2)
				{
					throwerror = true;
					failureRecords.put(massUserData.username, 'PC0034');
				}
				if (roleMap.get(massUserData.Role).Type__c == 'Manager' && hasManager.containskey(massUserData.SAPAccountNumber) && hasManager.get(massUserData.SAPAccountNumber))
				{
					throwerror = true;
					failureRecords.put(massUserData.username, 'PC0020');
				}
				if (roleMap.get(massUserData.Role).Type__c != 'Manager' && !hasManager.containskey(massUserData.SAPAccountNumber) && (!checkManagerExist.containskey(massUserData.SAPAccountNumber) || checkManagerExist.get(massUserData.SAPAccountNumber) == 2))
				{
					throwerror = true;
					failureRecords.put(massUserData.username, 'PC0012');
				}
				if (!throwerror && !userexist)
				{
					Contact contact = buildcontact(massUserData, accountNumberMap, roleMap, recType.MasterLabel);
					insertcon.add(contact);
					User user = builduser(massUserData, accountNumberMap, roleMap, contact);
					usersBuild.add(user);
				}
			}
			insert insertcon;
			String localekey = '';
			for (Contact contact : insertcon)
			{
				contactIds.add(contact.id);
				contactEmailMap.put(contact.Email, contact);
			}
			for (User user : usersBuild)
			{
				user.contactid = contactEmailMap.get(user.username).id;
				user.Unique_User_Id__c = contactEmailMap.get(user.username).id;
				localekey = user.localesidkey;
				insertUsers.add(user);
			}
			insert insertUsers;
			List<AccountContactRole__c> accountContactRoleList = new List<AccountContactRole__c>();
			List<AccountContactRole__c> insertedAccountContactRoleList = [SELECT ID,Approval_Status__c,Account_Ownership_Flag__c,functions__c FROM AccountContactRole__c WHERE Contact__c IN :contactIds];
			for (AccountContactRole__c acr : insertedAccountContactRoleList)
			{
				acr.Approval_Status__c = 'Pending Invitation';
				if (acr.Account_Ownership_Flag__c)
				{
					acr.functions__c = 'Ordering;Rewards;Administrator;ECPLocator';
				}
				accountContactRoleList.add(acr);
			}
			update accountContactRoleList;
			insertUsers.addAll(lmsEnableUsers);
			Map<ID, String>cemailconid = new Map<ID, String>();
			for (User successUsers : insertUsers)
			{
				if (successUsers.id != null)
				{
					if (!lmsEnableUsers.contains(successUsers))
					{
						cemailconid.put(successUsers.contactid, successUsers.Account.Name);
					}
					if (lmsAccess.get(successUsers.Email))
					{
						MyAccountInvitation.callLMSService(successUsers.id, sessionid);
					}
					Useruploadresult resl = new Useruploadresult();
					resl.username = successUsers.username;
					resl.message = 'Success';
					resl.errorCode = '';
					resl.errorType = '';
					resultBuild.add(resl);
				}
			}
			if (!cemailconid.isempty())
			{
				sendemailinvitation(cemailconid, localekey);
			}
			for (String fail : failureRecords.keyset())
			{
				Useruploadresult resl = new Useruploadresult();
				resl.username = fail;
				resl.message = B2B_Custom_Exceptions__c.getValues(failureRecords.get(fail)).Error_Description__c;
				resl.errorCode = B2B_Custom_Exceptions__c.getValues(failureRecords.get(fail)).Name;
				resl.errorType = 'MassActivation';
				resultBuild.add(resl);
			}
			uploadResult.Result = resultBuild;
			return uploadResult;
		}
		catch (exception e)
		{
			Database.rollback(rollbackPoint);
			List<Useruploadresult> resultBuild = new List<Useruploadresult>();
			Results uploadResult = new Results();
			Useruploadresult resl = new Useruploadresult();
			resl.username = 'None';
			resl.message = e.getmessage();
			resl.errorType = 'MassActivation';
			resultBuild .add(resl);
			uploadResult.Result = resultBuild;
			return uploadResult;
		}
	}

	/**
	* @description  Invoke LMS API to create users in LMS system
	*/
	@future(callout=true)
	public static void callLMSService(ID Userid, String sessionid)
	{
		HttpResponse response;
		User lmsUser = [SELECT id,FirstName,LastName,Username,Email,Contact.user_Role__r.Name,LocaleSidKey,LMS_id__c FROM User WHERE ID = :Userid];
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mc = new MockHttpResponseGenerator1();
			response = mc.respond4();
		}
		else
		{
			String Lang = '1';
			if (LMS_Settings__c.getValues(lmsUser.LocaleSidKey).Value__c != null)
			{
				Lang = LMS_Settings__c.getValues(lmsUser.LocaleSidKey).Value__c;
			}
			String endpoint = LMS_Settings__c.getValues('EndPoint').Value__c + 'v1/users';
			String DeptId = LMS_Settings__c.getValues('DEPT_ID_' + lmsUser.LocaleSidKey).Value__c;
			B2B_Utils.NewUser nu = new B2B_Utils.NewUser(lmsUser.FirstName, lmsUser.LastName, lmsUser.Username, lmsUser.Id, lmsUser.Email, DeptId, lmsUser.id, lmsUser.Contact.user_Role__r.Name, Lang);
			response = LmsUtils.processRequest(endpoint, JSON.serialize(nu), 'POST', sessionId);
		}
		if (response.getStatusCode() == 200 || response.getStatusCode() == 201)
		{
			String lmsUserid;
			JSONParser parser = JSON.createParser(response.getBody());
			while (parser.nextToken() != null)
			{
				if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'Id'))
				{
					parser.nextToken();
					lmsUserid = parser.getText();
					lmsUser.LMS_Id__c = lmsUserid;
				}
			}
			update lmsUser;
		}
	}

	/**
	* @description  Invoke ET class to resend invitation to created user
	*/
	@httpput
	global static String ResendInvitation()
	{
		RestRequest req = RestContext.request;
		String status;
		Blob Body = req.requestBody;
		Set<String> inviteUserIds = new Set<String>();
		SendInvitationDto invitationDto = (SendInvitationDto) JSON.deserialize(body.tostring(), SendInvitationDto.class);
		for (SendInvitation invite : invitationDto.userid)
		{
			inviteUserIds.add(invite.ID);
		}
		List<User> users = [SELECT ID, Email,Firstname, Localesidkey FROM User WHERE ID = :inviteUserIds];
		if (users.isempty())
		{
			status = '{"Status":"Fail"}';
		}
		else
		{
			Map<String, Email_Triggersends__c> emailTriggerSendKeyMap = Email_Triggersends__c.getAll();
			for (user us : users)
			{
				/*Send Email*/
				Email_Triggersends__c resendInvitationEmailKey = emailTriggerSendKeyMap.get(us.LocaleSidKey);
				ET_EmailAdministration.sendemail(us.Email, resendInvitationEmailKey.Account_Invitation_Resend__c, us.Firstname, '', '');
				status = '{"Status":"Success"}';
			}
		}
		return status;
	}

	/**
	* @description  Helper to method to build contact record to create
	*/
	private static Contact buildcontact(UsersDTO userbd, Map<String, Account> accountNumberMap, Map<String, Role__c> roleMap, ID HCPRecordTypeId)
	{
		Contact cc = new contact();
		cc.Salutation = userbd.Salutation;
		cc.FirstName = userbd.FirstName;
		cc.LastName = userbd.LastName;
		cc.Email = userbd.EmailAddress;
		if (userbd.SAPAccountNumber == null || userbd.SAPAccountNumber == '')
		{
			cc.AccountId = Portal_Settings__c.getValues('GenericAccount Id').Value__c;
		}
		else
		{
			cc.AccountId = accountNumberMap.get(userbd.SAPAccountNumber).id;
		}
		cc.RecordTypeId = HCPRecordTypeId;
		cc.user_Role__c = roleMap.get(userbd.role).Id;
		if (userbd.IsTestRecord != null)
		{
			cc.is_Test_User__c = userbd.IsTestRecord;
		}
		if (cc.AccountId != Portal_Settings__c.getValues('GenericAccount Id').Value__c)
		{
			cc.MailingStreet = accountNumberMap.get(userbd.SAPAccountNumber).shippingStreet;
			cc.MailingCity = accountNumberMap.get(userbd.SAPAccountNumber).shippingCity;
			cc.MailingPostalCode = accountNumberMap.get(userbd.SAPAccountNumber).shippingPostalCode;
			cc.MailingState = accountNumberMap.get(userbd.SAPAccountNumber).shippingState;
			cc.MailingCountry = accountNumberMap.get(userbd.SAPAccountNumber).shippingCountry;
		}
		return cc;
	}

	/**
	* @description  Helper method to build user record for creation
	*/
	private static User builduser(UsersDTO userbd, Map<String, Account>accountNumberMap, Map<String, Role__c>roleMap, Contact cc)
	{
		User ur = new User();
		ur.Title = userbd.Salutation;
		ur.FirstName = userbd.FirstName;
		ur.LastName = userbd.LastName;
		ur.Email = userbd.EmailAddress;
		ur.UserName = userbd.Username;
		ur.Occupation__c = roleMap.get(userbd.role).Name;
		ur.Localesidkey = userbd.UserLocale;
		ur.Languagelocalekey = userbd.UserLanguage;
		ur.EmailEncodingKey = 'ISO-8859-1';
		ur.Alias = (userbd.LastName.length() > 7) ? userbd.LastName.substring(0, 7) : userbd.LastName;
		ur.TimeZoneSidKey = userbd.Timezone;
		ur.CommunityNickname = B2B_Utils.generateGUID();
		ur.ProfileId = Portal_Settings__c.getValues('ProfileId').Value__c;
		if (cc.AccountId != Portal_Settings__c.getValues('GenericAccount Id').Value__c)
		{
			ur.Street = accountNumberMap.get(userbd.SAPAccountNumber).ShippingStreet;
			ur.City = accountNumberMap.get(userbd.SAPAccountNumber).ShippingCity;
			ur.State = accountNumberMap.get(userbd.SAPAccountNumber).ShippingState;
			ur.Postalcode = accountNumberMap.get(userbd.SAPAccountNumber).ShippingPostalCode;
			ur.Country = accountNumberMap.get(userbd.SAPAccountNumber).ShippingCountry;
		}
		ur.Registration_Type__c = userbd.Regtype;
		return ur;
	}

	/**
	* @description  Helper method to send invitation email to created user
	*/
	private static void sendemailinvitation(Map<ID, String> conid, String locale)
	{
		Map<String, Email_Triggersends__c> emailTriggerSendKeyMap = Email_Triggersends__c.getAll();
		Email_Triggersends__c invitationEmailKey = emailTriggerSendKeyMap.get(locale);
		List<Campaign>emailInvitationCamp = [SELECT Id,Name FROM Campaign WHERE Name = :invitationEmailKey.Account_Invitation__c limit 1];
		if (!emailInvitationCamp.isempty())
		{
			List<CampaignMember> campaignMembers = new List<CampaignMember>();
			for (ID cid : conid.keyset())
			{
				CampaignMember cmb1 = new CampaignMember();
				cmb1.ContactId = cid;
				cmb1.CampaignID = emailInvitationCamp[0].id;
				cmb1.Status = 'Send';
				cmb1.Practice_Name__c = conid.get(cid);
				campaignMembers.add(cmb1);
			}
			insert campaignMembers;
		}
	}

	public override void restResponse()
	{
		/*override method*/
	}
	public class SendInvitationDto
	{
		List<SendInvitation> userID { get; set; }
	}

	public class SendInvitation
	{
		String ID { get; set; }
	}
	public static Massusers parse(String reg)
	{
		return(Massusers) system.json.deserialize(reg, Massusers.class);
	}

	public class Massusers
	{
		public List<UsersDTO> UsersDTO { get; set; }
	}

	public class UsersDTO
	{
		String EmailAddress { get; set; }
		Boolean EnableLMS { get; set; }
		String FirstName { get; set; }
		Boolean IsTestRecord { get; set; }
		String LastName { get; set; }
		String Regtype { get; set; }
		String Role { get; set; }
		String Salutation { get; set; }
		String SAPAccountNumber { get; set; }
		String Timezone { get; set; }
		String UserLanguage { get; set; }
		String UserLocale { get; set; }
		String Username { get; set; }
	}

	public class Useruploadresult extends BaseRestResponse
	{
		String Username { get; set; }
		String message { get; set; }
		String errorCode { get; set; }
		String errorType { get; set; }
		public override void restResponse()
		{/*override method*/
		}
	}
	public class Results extends BaseRestResponse
	{
		Useruploadresult[] Result { get; set; }
		public override void restResponse()
		{/*override method*/
		}
	}
}