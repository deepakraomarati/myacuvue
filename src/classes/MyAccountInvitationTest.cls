/**
* File Name: TestMyAccountInvitation
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  18-Oct-2018
* Description : Unit Test class for MyAccountInvitation
* Copyright (c) $2018 Johnson & Johnson
*/
@isTest(SeeAlldata=false)
public class MyAccountInvitationTest
{
	private static void inserttestdata()
	{
		List<sObject> b2bCustomExceptions = Test.loadData(B2B_Custom_Exceptions__c.sObjectType, 'B2BCustomexceptions');
		List<sObject> portalSettings = Test.loadData(Portal_Settings__c.sObjectType, 'portalsettings');
		List<sObject> emailTriggerSends = Test.loadData(Email_Triggersends__c.sObjectType, 'ET_Triggersends');
		List<Campaign> campaignList = new List<Campaign>();
		List<LMS_Settings__c> lmsSettingsList = new List<LMS_Settings__c>();
		//Insert Role
		Role__c role = new Role__c();
		role.Name = 'Store Manager';
		role.Approval_Required__c = true;
		role.Country__c = 'Japan';
		role.Type__c = 'Manager';
		insert role;
		//Insert Campaigns
		Campaign campaign = new Campaign();
		campaign.Name = Email_Triggersends__c.getvalues('ja_JP').Account_Invitation__c;
		campaignList.add(campaign);
		Campaign campaign1 = new Campaign();
		campaign1.Name = Email_Triggersends__c.getvalues('ja_JP').Account_Invitation_Resend__c;
		campaignList.add(campaign1);
		insert campaignList;
		//Insert LMS settings
		LMS_Settings__c lmsCustomSettings = new LMS_Settings__c();
		lmsCustomSettings.Name = 'EndPoint';
		lmsCustomSettings.Value__c = 'https://jjvus.sandbox.myabsorb.com/api/Rest/';
		lmsSettingsList.add(lmsCustomSettings);
		LMS_Settings__c lmsCustomSettings1 = new LMS_Settings__c();
		lmsCustomSettings1.Name = 'ImageURL';
		lmsCustomSettings1.Value__c = 'https://jjvci.sandbox.myabsorb.com/Files/';
		lmsSettingsList.add(lmsCustomSettings1);
		LMS_Settings__c lmsCustomSettings2 = new LMS_Settings__c();
		lmsCustomSettings2.Name = 'Password';
		lmsCustomSettings2.Value__c = 'Test.123';
		lmsSettingsList.add(lmsCustomSettings2);
		LMS_Settings__c lmsCustomSettings3 = new LMS_Settings__c();
		lmsCustomSettings3.Name = 'PrivateKey';
		lmsCustomSettings3.Value__c = '58091a9c-7077-40ee-927b-143ea1af810a';
		lmsSettingsList.add(lmsCustomSettings3);
		LMS_Settings__c lmsCustomSettings4 = new LMS_Settings__c();
		lmsCustomSettings4.Name = 'Username';
		lmsCustomSettings4.Value__c = 'prorestadmin';
		lmsSettingsList.add(lmsCustomSettings4);
		LMS_Settings__c lmsCustomSettings5 = new LMS_Settings__c();
		lmsCustomSettings5.Name = 'DEPT_ID_ja_JP';
		lmsCustomSettings5.Value__c = '20bf217e-41a7-44b3-a1c7-2e084a44d936';
		lmsSettingsList.add(lmsCustomSettings5);
		LMS_Settings__c lmsCustomSettings6 = new LMS_Settings__c();
		lmsCustomSettings6.Name = 'ja_JP-' + role.id;
		lmsCustomSettings6.Value__c = '154378c7-49da-4ca8-850f-471d59e64b96';
		lmsSettingsList.add(lmsCustomSettings6);
		LMS_Settings__c lmsCustomSettings7 = new LMS_Settings__c();
		lmsCustomSettings7.Name = 'ja_JP';
		lmsCustomSettings7.Value__c = '1';
		lmsSettingsList.add(lmsCustomSettings7);
		LMS_Settings__c lmsCustomSettings8 = new LMS_Settings__c();
		lmsCustomSettings8.Name = 'Token';
		lmsCustomSettings8.Value__c = '4zCipR7qNNtCzLeBa7aJRQ==S/FmYArMKLFW2SjqkAhvaN6NLt3KxiSwblr1Ndc2Tp4OFp+Q+PPcpWM1sfIFFKNvCUJunWzZS+l2IZIfU6Z7w0vt9/jBX6XD5ZFwYRBv1U7C4sZAy9xT0gtv0lnSuUCigmsC/6fs0Ej0UnAhWYzIhg==';
		lmsSettingsList.add(lmsCustomSettings8);
		insert lmsSettingsList;
		//Insert Test Accounts
		Account genericAccount = new Account();
		genericAccount.Name = 'Generic consumer Account';
		genericAccount.shippingcountry = 'Japan';
		genericAccount.shippingstreet = 'Test Street';
		genericAccount.shippingcity = 'Test city';
		genericAccount.shippingpostalcode = 'TEST2007017';
		genericAccount.shippingstate = 'AICHI KEN';
		genericAccount.OutletNumber__c = '125416584';
		insert genericAccount;
		Account account = new Account();
		account.Name = 'Testing Account';
		account.shippingcountry = 'Japan';
		account.shippingstreet = 'Test Street';
		account.shippingcity = 'Test city';
		account.shippingpostalcode = 'TEST222217';
		account.shippingstate = 'AICHI KEN';
		account.OutletNumber__c = '125412652';
		insert account;
		//Insert Portal settings to store Generic Account ID
		Portal_Settings__c portalCustomSettings = new Portal_Settings__c();
		portalCustomSettings.Name = 'GenericAccount Id';
		portalCustomSettings.Value__c = genericAccount.Id;
		insert portalCustomSettings;
	}

	@istest static void Testinsertuser_withcorrectdata()
	{
		inserttestdata();
		Test.Starttest();
		RestRequest req1 = new RestRequest();
		RestResponse res1 = new RestResponse();
		String jsonRequest1 = '{"UsersDTO" : [ {"Salutation":"Mr.","FirstName":"Test First","LastName":"Test Last","EmailAddress":"Testpurposelistuser+1@gmail.com","Username":"Testpurposelistuser+1@gmail.com","Role":"Store Manager","SecretQuestion":"whats your firstname","SecretAnswer":"mytest","DateofBirth":"03/02/1990","SchoolName":"Test school","GraduationYear":"2013","Degree":"B.Tech","UserLanguage":"ja","UserLocale":"ja_JP","Timezone":"Asia/Tokyo","SAPAccountNumber":"125412652","PostalCode":"TEST222217","IsTestRecord":true,"EnableLMS":true}]}';
		req1.requestURI = 'b2b/services/apexrest/apex/myaccount/v1/MyAccountInvitation/';
		req1.httpMethod = 'POST';
		req1.requestbody = blob.valueof(jsonRequest1);
		RestContext.request = req1;
		RestContext.response = res1;
		BaseRestResponse response = MyAccountInvitation.uploadusers();
		system.assertNotEquals(response, null);
		Test.Stoptest();
	}

	@isTest static void Testexceptions_withincorrectdata()
	{
		inserttestdata();
		Test.Starttest();
		//For LastName Validation
		RestRequest req2 = new RestRequest();
		RestResponse res2 = new RestResponse();
		String jsonRequest2 = '{"UsersDTO" : [ {"Salutation":"Mr.","FirstName":"Test First","LastName":"","EmailAddress":"Testpurposelistuser+1@gmail.com","Username":"Testpurposelistuser+1@gmail.com","Role":"Store Manager","SecretQuestion":"whats your firstname","SecretAnswer":"mytest","DateofBirth":"03/02/1990","SchoolName":"Test school","GraduationYear":"2013","Degree":"B.Tech","UserLanguage":"ja","UserLocale":"ja_JP","Timezone":"Asia/Tokyo","SAPAccountNumber":"125412652","PostalCode":"TEST222217","IsTestRecord":true,"EnableLMS":false}]}';
		req2.requestURI = 'b2b/services/apexrest/apex/myaccount/v1/MyAccountInvitation/';
		req2.httpMethod = 'POST';
		req2.requestbody = blob.valueof(jsonRequest2);
		RestContext.request = req2;
		RestContext.response = res2;
		BaseRestResponse response = MyAccountInvitation.uploadusers();
		system.assertNotEquals(response, null);

		//For Username Empty validation
		RestRequest req3 = new RestRequest();
		RestResponse res3 = new RestResponse();
		String jsonRequest3 = '{"UsersDTO" : [ {"Salutation":"Mr.","FirstName":"Test First","LastName":"Test Last","EmailAddress":"","Username":"","Role":"Store Manager","SecretQuestion":"whats your firstname","SecretAnswer":"mytest","DateofBirth":"03/02/1990","SchoolName":"Test school","GraduationYear":"2013","Degree":"B.Tech","UserLanguage":"ja","UserLocale":"ja_JP","Timezone":"Asia/Tokyo","SAPAccountNumber":"125412652","PostalCode":"TEST222217","IsTestRecord":true,"EnableLMS":false}]}';
		req3.requestURI = 'b2b/services/apexrest/apex/myaccount/v1/MyAccountInvitation/';
		req3.httpMethod = 'POST';
		req3.requestbody = blob.valueof(jsonRequest3);
		RestContext.request = req3;
		RestContext.response = res3;
		BaseRestResponse response1 = MyAccountInvitation.uploadusers();
		system.assertNotEquals(response1, null);

		//For Existed user validation
		RestRequest req4 = new RestRequest();
		RestResponse res4 = new RestResponse();
		String jsonRequest4 = '{"UsersDTO" : [ {"Salutation":"Mr.","FirstName":"Test First","LastName":"Test Last","EmailAddress":"mahesh.bics+massupload+247@gmail.com","Username":"mahesh.bics+massupload+247@gmail.com","Role":"Store Manager","SecretQuestion":"whats your firstname","SecretAnswer":"mytest","DateofBirth":"03/02/1990","SchoolName":"Test school","GraduationYear":"2013","Degree":"B.Tech","UserLanguage":"ja","UserLocale":"ja_JP","Timezone":"Asia/Tokyo","SAPAccountNumber":"125412652","PostalCode":"TEST222217","IsTestRecord":true,"EnableLMS":false}]}';
		req4.requestURI = 'b2b/services/apexrest/apex/myaccount/v1/MyAccountInvitation/';
		req4.httpMethod = 'POST';
		req4.requestbody = blob.valueof(jsonRequest4);
		RestContext.request = req4;
		RestContext.response = res4;
		BaseRestResponse response2 = MyAccountInvitation.uploadusers();
		system.assertNotEquals(response2, null);

		//SAP&Postal code validation
		RestRequest req5 = new RestRequest();
		RestResponse res5 = new RestResponse();
		String jsonRequest5 = '{"UsersDTO" : [ {"Salutation":"Mr.","FirstName":"Test First","LastName":"Test Last","EmailAddress":"Testpurposelistuser+1@gmail.com","Username":"Testpurposelistuser+1@gmail.com","Role":"Store Manager","SecretQuestion":"whats your firstname","SecretAnswer":"mytest","DateofBirth":"03/02/1990","SchoolName":"Test school","GraduationYear":"2013","Degree":"B.Tech","UserLanguage":"ja","UserLocale":"ja_JP","Timezone":"Asia/Tokyo","SAPAccountNumber":"","PostalCode":"","IsTestRecord":true,"EnableLMS":false}]}';
		req5.requestURI = 'b2b/services/apexrest/apex/myaccount/v1/MyAccountInvitation/';
		req5.httpMethod = 'POST';
		req5.requestbody = blob.valueof(jsonRequest5);
		RestContext.request = req5;
		RestContext.response = res5;
		BaseRestResponse response3 = MyAccountInvitation.uploadusers();
		system.assertNotEquals(response3, null);

		//Role Validation
		RestRequest req6 = new RestRequest();
		RestResponse res6 = new RestResponse();
		String jsonRequest6 = '{"UsersDTO" : [ {"Salutation":"Mr.","FirstName":"Test First","LastName":"Test Last","EmailAddress":"Testpurposelistuser+1@gmail.com","Username":"Testpurposelistuser+1@gmail.com","Role":"Opticn","SecretQuestion":"whats your firstname","SecretAnswer":"mytest","DateofBirth":"03/02/1990","SchoolName":"Test school","GraduationYear":"2013","Degree":"B.Tech","UserLanguage":"ja","UserLocale":"ja_JP","Timezone":"Asia/Tokyo","SAPAccountNumber":"125412652","PostalCode":"TEST222217","IsTestRecord":true,"EnableLMS":false}]}';
		req6.requestURI = 'b2b/services/apexrest/apex/myaccount/v1/MyAccountInvitation/';
		req6.httpMethod = 'POST';
		req6.requestbody = blob.valueof(jsonRequest6);
		RestContext.request = req6;
		RestContext.response = res6;
		BaseRestResponse response4 = MyAccountInvitation.uploadusers();
		system.assertNotEquals(response4, null);
		Test.Stoptest();
	}

	@isTest static void Test_resendinvitationmethod()
	{
		Email_Triggersends__c emailTriggerSend = new Email_Triggersends__c();
		emailTriggerSend.Name = userinfo.getlocale();
		emailTriggerSend.Account_Invitation_Resend__c = 'Test resend invitation';
		insert emailTriggerSend;

		Test.Starttest();
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		String jsonRequest = '{"userID":[{"ID":"' + userinfo.getuserid() + '"}]}';
		req.httpMethod = 'PUT';
		req.requestbody = blob.valueof(jsonRequest);
		RestContext.request = req;
		RestContext.response = res;
		string response = MyAccountInvitation.ResendInvitation();
		system.assertNotEquals(response, null);

		RestRequest req1 = new RestRequest();
		RestResponse res1 = new RestResponse();
		String jsonRequest1 = '{"userID":[{"ID":"123456879"}]}';
		req1.httpMethod = 'PUT';
		req1.requestbody = blob.valueof(jsonRequest1);
		RestContext.request = req1;
		RestContext.response = res1;
		string response1 = MyAccountInvitation.ResendInvitation();
		system.assertNotEquals(response1, null);
		Test.Stoptest();
	}
}