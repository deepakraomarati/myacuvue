/**
* File Name: MyAccountLearningService
* Author : Sathishkumar | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : This class manages the user's online learning. Retrieves the user learning progress from absorb and update it in salesoforc
               and transfer the information to drupal on request basis.
* Copyright (c) $2018 Johnson & Johnson
*/
@RestResource(urlMapping='/apex/myaccount/v1/LearningService/*')
global without sharing class MyAccountLearningService
{
	global static final String EMAIL_ERROR = 'PC0011';
	global static final String ERROR_TYPE = 'LearningService' ;
	global static final String STATUS_ERROR = 'PC0013';
	global static final String PARAMETER_ERROR = 'PC0014';
	global static final String GENERIC_ERROR = 'GEN0001';
	global static final String GENERIC_ERROR_TYPE = 'Generic';
	global static final String USERID_ERROR = 'PC0019';

	/**
	* Description : When this method called, System will call the absorb API to get the logged in user learning progress from the absorb. 
	* And create the learning progress in salesforce for new user. If user is already launched the course then create/update exsiting prgress
	*/
	@HttpGet
	global static String doGet()
	{
		RestRequest req = RestContext.request;
		try
		{
			Set<String> setForStatusCheck = new Set<String>();
			setForStatusCheck.add('all');
			setForStatusCheck.add('completion');
			setForStatusCheck.add('inprogress');
			String status;
			List<String> listURI = req.requestURI.split('/');
			if (listURI.size() != 7)
			{
				throw new CustomException(LmsUtils.getException(PARAMETER_ERROR, ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0014').Error_Description__c)));
			}
			String userId = listURI[5];

			if (userId == '' || userId == null)
			{
				throw new CustomException(LmsUtils.getException(PARAMETER_ERROR, ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0014').Error_Description__c)));
			}
			else if (setForStatusCheck.contains(listURI[6]))
			{
				status = LmsUtils.getEnrollments(userId, listURI[6]);
			}
			else
			{
				throw new CustomException(LmsUtils.getException(STATUS_ERROR, ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0013').Error_Description__c)));
			}

			return status ;
		}
		catch (Exception e)
		{
			return e.getMessage();
		}
	}
	/**
	* Description : When this method called, system will create an user in absorb system by calling absorb CreateUser API. 
	* 
	*/
	@HttpPost
	global static String RegLMSUser()
	{
		RestRequest req = RestContext.request;
		try
		{
			Id UserId = req.requestURI.substring(req.requestURI.lastIndexOf('/') + 1);
			User holdsUser = [SELECT Id, FirstName, LastName, Username, Email, ContactId, Contact.User_Role__r.Name,LocaleSidKey FROM user WHERE Id = :UserId];
			String DeptId = LMS_Settings__c.getValues('DEPT_ID_' + holdsUser.LocaleSidKey).Value__c;
			String Lang = '1';
			if (LMS_Settings__c.getValues(holdsUser.LocaleSidKey).Value__c != null)
			{
				Lang = LMS_Settings__c.getValues(holdsUser.LocaleSidKey).Value__c;
			}
			B2B_Utils.NewUser newUser = new B2B_Utils.NewUser(holdsUser.FirstName, holdsUser.LastName, holdsUser.Username, holdsUser.Id, holdsUser.Email, DeptId, UserId, holdsUser.Contact.User_Role__r.Name, Lang);
			String LMS_UserId = B2B_Utils.createLMSUser(JSON.serialize(newUser), true);
			if (LMS_UserId == null)
			{
				throw new CustomException(B2B_Utils.getException(GENERIC_ERROR, ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('GEN0001').Error_Description__c)));
			}
			else
			{
				Userresult responseForUser = new Userresult('Success', LMS_UserId);
				return json.serialize(responseForUser);
			}

		}
		catch (Exception e)
		{
			if (e.getMessage().contains('Invalid id:'))
			{
				return B2B_Utils.getException(USERID_ERROR, ERROR_TYPE, B2B_Custom_Exceptions__c.getValues('PC0019').Error_Description__c);
			}
			return e.getMessage();
		}
	}
	public class Userresult
	{
		public string Status { get; set; }
		public String LMSUserId { get; set; }
		public Userresult(string status, String LMSUserId)
		{
			this.Status = status;
			this.LMSUserId = LMSUserId;
		}
	}
}