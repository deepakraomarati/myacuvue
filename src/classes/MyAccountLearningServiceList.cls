/**
* File Name: MyAccountLearningServiceList
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : Get course information and related data based on input language
* Copyright (c) $2018 Johnson & Johnson
*/
@RestResource(urlMapping='/apex/myaccount/v1/CourseServiceList/*')
global without sharing class MyAccountLearningServiceList
{
	global static final String ERROR_TYPE = 'LearningService' ;
	global static final String REQUIRED = 'PC0014';
	global static final String GENERIC_ERROR = 'GEN0001';
	global static final String GENERIC_ERROR_TYPE = 'Generic';
	global static final String PARAMETER_ERROR_TYPE = 'LearningService';
	/**
	* Description : When this method called, System will get the Course,Tags & images information for the given language. 
	*/
	@HttpGet
	global static string doGet()
	{
		List<DTOForListvalues> ListforCourseValues = new List<DTOForListvalues >();
		RestRequest holdsrequest = RestContext.request;
		try
		{
			String language = holdsrequest.requestURI.substring(holdsrequest.requestURI.lastIndexOf('/') + 1);
			if (language == null || language == '')
			{
				throw new CustomException(B2B_Utils.getException(REQUIRED, ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0014').Error_Description__c)));
			}
			else
			{
				Map<String, Course__c> mapForCourse = new Map<String, Course__c>();
				List<Course__c> listForCourseMapping = [SELECT Id,Name, LMS_ID__c,Description__c,Duration__c,Main_Course_Image__c,Related_Content_Image__c,Landing_page_Image__c,Online_CoursePage_Image__c,Type__c,CategoryId__c,Language__c, (SELECT id,courseTagAssignedId__c,CustomCourse__c,CustomCurriculum__c,CustomTag__c FROM CourseTagAssignments__r) FROM Course__c WHERE Language__c = :language AND ActiveStatus__c = 'Active'];
				for (Course__c holdsCourse : listForCourseMapping)
				{
					mapForCourse.put(holdsCourse.LMS_ID__c, holdsCourse);
				}
				Map<String, String> mapFrCategory = new Map<String, String>();
				Map<String, tag > mapFrTags = new Map<String, tag >();
				List<Categories_Tag__c> lstFrCategory = [SELECT LMS_ID__c, Name FROM Categories_Tag__c WHERE Type__c = 'Category'];
				List<Categories_Tag__c> lstFrTag = [SELECT Id,LMS_ID__c, Name FROM Categories_Tag__c WHERE Type__c = 'Tag'];
				for (Categories_Tag__c lstFrCategoryObj : lstFrCategory)
				{
					mapFrCategory.put(lstFrCategoryObj.LMS_ID__c, lstFrCategoryObj.Name);
				}
				for (Categories_Tag__c lstFrTagObj : lstFrTag)
				{
					tag holdsTag = new tag();
					holdsTag.Id = lstFrTagObj.LMS_ID__c;
					holdsTag.Name = lstFrTagObj.Name;
					mapFrTags.put(lstFrTagObj.Id, holdsTag);
				}
				for (Course__c CourseDTOObjj : listForCourseMapping)
				{
					List<image> images = new List<image>();
					List<Category> CategoryWithName = new List<Category>();
					List<tag> TagWithName = new List<tag>();
					if (mapForCourse.get(CourseDTOObjj.LMS_ID__c) != null)
					{
						if (mapForCourse.get(CourseDTOObjj.LMS_ID__c).Main_Course_Image__c != null)
						{
							image img1 = new image();
							img1.Name = 'Main_Course_Image';
							img1.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCourse.get(CourseDTOObjj.LMS_ID__c).Main_Course_Image__c;
							images.add(img1);
						}
						if (mapForCourse.get(CourseDTOObjj.LMS_ID__c).Related_Content_Image__c != null)
						{
							image img2 = new image();
							img2.Name = 'Related_Content_Image';
							img2.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCourse.get(CourseDTOObjj.LMS_ID__c).Related_Content_Image__c;
							images.add(img2);
						}
						if (mapForCourse.get(CourseDTOObjj.LMS_ID__c).Landing_page_Image__c != null)
						{
							image img3 = new image();
							img3.Name = 'Landing_page_Image';
							img3.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCourse.get(CourseDTOObjj.LMS_ID__c).Landing_page_Image__c;
							images.add(img3);
						}
						if (mapForCourse.get(CourseDTOObjj.LMS_ID__c).Online_CoursePage_Image__c != null)
						{
							image img4 = new image();
							img4.Name = 'Online_CoursePage_Image';
							img4.URL = LMS_Settings__c.getValues('ImageURL').Value__c + mapForCourse.get(CourseDTOObjj.LMS_ID__c).Online_CoursePage_Image__c;
							images.add(img4);
						}
					}
					if (mapForCourse.get(CourseDTOObjj.LMS_ID__c).CategoryId__c != null)
					{
						Category holdsCategory = new Category();
						holdsCategory.Id = mapForCourse.get(CourseDTOObjj.LMS_ID__c).CategoryId__c;
						holdsCategory.Name = mapFrCategory.get(mapForCourse.get(CourseDTOObjj.LMS_ID__c).CategoryId__c);
						CategoryWithName.add(holdsCategory);
					}
					if (!CourseDTOObjj.CourseTagAssignments__r.isEmpty() && CourseDTOObjj.CourseTagAssignments__r.size() > 0)
					{
						for (CourseTagAssignment__c holdsCourseTagAssignment : CourseDTOObjj.CourseTagAssignments__r)
						{
							Tag holdsTag = mapFrTags.get(holdsCourseTagAssignment.CustomTag__c);
							TagWithName.add(holdsTag);
						}
					}
					DTOForListvalues DTOForListvalue = new DTOForListvalues();
					DTOForListvalue.Name = CourseDTOObjj.Name;
					DTOForListvalue.LMSID = CourseDTOObjj.LMS_ID__c;
					DTOForListvalue.Description = CourseDTOObjj.Description__c;
					DTOForListvalue.Images = images;
					DTOForListvalue.Duration = CourseDTOObjj.Duration__c;
					DTOForListvalue.CourseId = CourseDTOObjj.Id;
					DTOForListvalue.Type = CourseDTOObjj.Type__c;
					DTOForListvalue.Language = CourseDTOObjj.Language__c;
					DTOForListvalue.Category = CategoryWithName;
					DTOForListvalue.Tags = TagWithName;
					ListforCourseValues.add(DTOForListvalue);
				}
			}
			return JSON.serializePretty(ListforCourseValues);
		}
		catch (Exception e)
		{
			return e.getmessage();
		}
	}
	public class DTOForListvalues
	{
		public String Type { get; set; }
		public List<tag> Tags { get; set; }
		public String Name { get; set; }
		public String LMSID { get; set; }
		public String Language { get; set; }
		public List<image> Images { get; set; }
		public String Duration { get; set; }
		public String Description { get; set; }
		public String CourseId { get; set; }
		public List<Category> Category { get; set; }
	}
	public class tag
	{
		public String Id { get; set; }
		public String Name { get; set; }
	}
	public class image
	{
		public String Name { get; set; }
		public String URL { get; set; }
	}
	public class Category
	{
		public String Id { get; set; }
		public String Name { get; set; }
	}
}