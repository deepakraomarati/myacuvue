/**
* File Name: MyAccountLearningServiceListTest
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : Test class for MyAccountLearningServiceList class
* Copyright (c) $2018 Johnson & Johnson
*/
@isTest(SeeAlldata=false)
public class MyAccountLearningServiceListTest
{
	static testmethod void Test_Getcoursesbylocale()
	{
		B2B_Custom_Exceptions__c BCE1 = new B2B_Custom_Exceptions__c();
		BCE1.Name = 'PC0014';
		BCE1.Error_Description__c = 'Required parameters missing.';
		insert BCE1;

		LMS_Settings__c LmsCustomSettings1 = new LMS_Settings__c();
		LmsCustomSettings1.Name = 'ImageURL';
		LmsCustomSettings1.Value__c = 'https://jjvus.sandbox.myabsorb.com/Files/';
		insert LmsCustomSettings1;

		Categories_Tag__c tag = new Categories_Tag__c();
		tag.Description__c = 'Test Tag5';
		tag.LMS_ID__c = '7a934842-c88a-4dfc-aa73-3c8d55074de3';
		tag.Parent_Id__c = 'ty642f42-e49d-r78t-8w2a-4r85a7t78se8';
		tag.Type__c = 'Tag';
		insert tag;

		Categories_Tag__c tag1 = new Categories_Tag__c();
		tag1.Description__c = 'Test Tag' ;
		tag1.LMS_ID__c = '7a934842-c88a-4dfc-aa73-3c8d74174de4';
		tag1.Parent_Id__c = 'ty642f24-e74d-r78t-8w2a-4r85a7t78se8';
		tag1.Type__c = 'Tag';
		insert tag1;

		Categories_Tag__c holdscategory = new Categories_Tag__c();
		holdscategory.Description__c = 'Test cat' ;
		holdscategory.LMS_ID__c = '7a934842-c99a-4dfc-aa73-3c8d74174de4';
		holdscategory.Parent_Id__c = 'ty642f24-e74d-r78t-8w2a-4r85a7t78se8';
		holdscategory.Type__c = 'Category';
		insert holdscategory;

		Course__c course = new Course__c();
		course.Name = 'Test Course1';
		course.ActiveStatus__c = 'Active';
		course.CategoryId__c = '15654144-afc0-48eb-8838';
		course.Main_Course_Image__c = 'dgdddgd';
		course.Landing_page_Image__c = 'csdfsfffg';
		course.Online_CoursePage_Image__c = 'sddfffg';
		course.Related_Content_Image__c = 'df5CdffWE';
		course.Description__c = 'some test data';
		course.ExpireDuration__c = 'test ExpireDuration';
		course.LMS_ID__c = '7a934842-c88a-4dfc-aa33-3c8d95074de3';
		course.Language__c = 'ja_JP';
		course.Type__c = 'OnlineCourse';
		course.Notes__C = 'tets note';
		insert course;

		CourseTagAssignment__c coursetag = new CourseTagAssignment__c();
		coursetag.Name = 'Test1';
		coursetag.CustomTag__c = tag1.id;
		coursetag.CustomCourse__c = course.id;
		coursetag.courseTagAssignedId__c = 'Test';
		insert coursetag;

		MyAccountLearningServiceList.DTOForListvalues holdsDtoValues = new MyAccountLearningServiceList.DTOForListvalues();
		holdsDtoValues.Type = 'OnlineCourse';
		holdsDtoValues.Name = 'Test';
		holdsDtoValues.LMSID = '7a934842-c99a-4dfc-aa73-3c8d74174de4';
		holdsDtoValues.Language = 'en_US';
		holdsDtoValues.Duration = '10.00';
		holdsDtoValues.Description = 'test';
		holdsDtoValues.CourseId = course.id;

		MyAccountLearningServiceList.image im = new MyAccountLearningServiceList.image();
		im.Name = 'test';
		im.URL = 'hjdwhffrhkjf';

		RestRequest req1 = new RestRequest();
		RestResponse res1 = new RestResponse();
		req1.requestURI = '/apex/myaccount/v1/CourseServiceList/' + 'ja_JP';
		req1.httpMethod = 'GET';
		RestContext.request = req1;
		RestContext.response = res1;
		string response = MyAccountLearningServiceList.doGet();
		system.assertNotEquals(response, null);

		RestRequest req2 = new RestRequest();
		RestResponse res2 = new RestResponse();
		req2.requestURI = '/apex/myaccount/v1/CourseServiceList/';
		req2.httpMethod = 'GET';
		RestContext.request = req2;
		RestContext.response = res2;
		string errorresponse = MyAccountLearningServiceList.doGet();
		system.assertNotEquals(errorresponse, null);
	}
}