/**
* File Name: MyAccountLearningServiceTest
* Author : Sathishkumar | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : Test class for MyAccountLearningService class
* Copyright (c) $2018 Johnson & Johnson
*/
@isTest(SeeAlldata=false)
private class MyAccountLearningServiceTest
{
	static testMethod void Test_Getcoursesbystatus()
	{
		User u = insertSettings();

		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/apex/myaccount/v1/LearningService/' + u.id + '/all';
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;

		string responseGet = MyAccountLearningService.doGet();
		system.assertNotEquals(responseGet, null);

		RestRequest req_progress = new RestRequest();
		RestResponse res_progress = new RestResponse();
		req_progress.requestURI = '/apex/myaccount/v1/LearningService/' + u.id + '/inprogress';
		req_progress.httpMethod = 'GET';
		RestContext.request = req_progress;
		RestContext.response = res_progress;
		System.runAs(u)
		{
			string response = MyAccountLearningService.doGet();
			system.assertNotEquals(response, null);
		}
		RestRequest req_compl = new RestRequest();
		RestResponse res_compl = new RestResponse();
		req_compl.requestURI = '/apex/myaccount/v1/LearningService/' + u.id + '/completion';
		req_compl.httpMethod = 'GET';
		RestContext.request = req_compl;
		RestContext.response = res_compl;
		System.runAs(u)
		{
			string response = MyAccountLearningService.doGet();
			system.assertNotEquals(response, null);
		}
		RestRequest req_catchScenario_1 = new RestRequest();
		RestResponse res_catchScenario_1 = new RestResponse();
		req_catchScenario_1.requestURI = '/apex/myaccount/v1/LearningService/' + u.id;
		req_catchScenario_1.httpMethod = 'GET';
		RestContext.request = req_catchScenario_1;
		RestContext.response = res_catchScenario_1;
		System.runAs(u)
		{
			string response = MyAccountLearningService.doGet();
			system.assertNotEquals(response, null);
		}
		RestRequest req_catchScenario_2 = new RestRequest();
		RestResponse res_catchScenario_2 = new RestResponse();
		req_catchScenario_2.requestURI = '/apex/myaccount/v1/LearningService//completion';
		req_catchScenario_2.httpMethod = 'GET';
		RestContext.request = req_catchScenario_2;
		RestContext.response = res_catchScenario_2;
		System.runAs(u)
		{
			string response = MyAccountLearningService.doGet();
			system.assertNotEquals(response, null);
		}
		RestRequest req_catchScenario_3 = new RestRequest();
		RestResponse res_catchScenario_3 = new RestResponse();
		req_catchScenario_3.requestURI = '/apex/myaccount/v1/LearningService/' + u.id + '/DummyStatus';
		req_catchScenario_3.httpMethod = 'GET';
		RestContext.request = req_catchScenario_3;
		RestContext.response = res_catchScenario_3;
		System.runAs(u)
		{
			string response = MyAccountLearningService.doGet();
			system.assertNotEquals(response, null);
		}
	}

	static testMethod void Test_RegisterLMSUser()
	{
		User u = insertSettings();

		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = 'services/apexrest/apex/myaccount/v1/LearningService/' + u.id;
		req.httpMethod = 'POST';
		RestContext.request = req;
		RestContext.response = res;
		System.runAs (u)
		{
			string response = MyAccountLearningService.RegLMSUser();
			system.assertNotEquals(response, null);
		}
		RestRequest req_catchSce = new RestRequest();
		RestResponse res_catchSce = new RestResponse();
		req_catchSce.requestURI = '';
		req_catchSce.httpMethod = 'POST';
		RestContext.request = req_catchSce;
		RestContext.response = res_catchSce;
		MyAccountLearningService.RegLMSUser();
	}

	static testMethod void Test_courseAutoEnrollments()
	{
		User u = insertSettings();
		System.debug('user::' + u.id);
		string ReqBody = '{"UserId":"' + u.id + '","Courseid":"123456"}';
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = 'apexrest/apex/myaccount/v1/Enrollments';
		req.httpMethod = 'POST';
		req.requestbody = blob.valueof(ReqBody);
		RestContext.request = req;
		RestContext.response = res;
		System.runAs (u)
		{
			string response = LmsEnrollments.RegEnrollments();
			system.assertNotEquals(response, null);
		}
		ReqBody = '{"UserId":"","Courseid":"123456"}';
		req.requestURI = 'apexrest/apex/myaccount/v1/Enrollments';
		req.httpMethod = 'POST';
		req.requestbody = blob.valueof(ReqBody);
		RestContext.request = req;
		RestContext.response = res;
		System.runAs (u)
		{
			string response = LmsEnrollments.RegEnrollments();
			system.assertNotEquals(response, null);
		}
	}

	static testMethod void Test_courseMassEnrollments()
	{
		User u = insertSettings();
		System.debug('user::' + u.id);
		string ReqBody = '{"CourseList": [{"ContactId":"' + u.ContactId + '","Courseid": "123456"}]}';
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = 'apexrest/apex/myaccount/v1/Enrollments';
		req.httpMethod = 'PUT';
		req.requestbody = blob.valueof(ReqBody);
		RestContext.request = req;
		RestContext.response = res;
		System.runAs (u)
		{
			string response = LmsEnrollments.RegMassEnrollments();
			system.assertNotEquals(response, null);
		}
		User U2 = [Select id,contactid from user where UserName = 'protestuser+acuvue_Learning1@gmail.com'];

		ReqBody = '{"CourseList": [{"ContactId":"' + u2.ContactId + '","Courseid": "123456"}]}';
		req.requestURI = 'apexrest/apex/myaccount/v1/Enrollments';
		req.httpMethod = 'PUT';
		req.requestbody = blob.valueof(ReqBody);
		RestContext.request = req;
		RestContext.response = res;
		System.runAs (u)
		{
			string response = LmsEnrollments.RegMassEnrollments();
			system.assertNotEquals(response, null);
		}
	}

	static User insertSettings()
	{
		List<sObject> B2BCE = Test.loadData(B2B_Custom_Exceptions__c.sObjectType, 'B2BCustomexceptions');
		List<sObject> PLS = Test.loadData(Portal_Settings__c.sObjectType, 'portalsettings');

		Role__c RL = new Role__c();
		RL.Name = '医師';
		RL.Approval_Required__c = true;
		RL.Country__c = 'Japan';
		RL.Type__c = 'Staff';
		insert RL;

		Categories_Tag__c cat = new Categories_Tag__c();
		cat.Description__c = 'Test description' ;
		cat.LMS_ID__c = '7f685e32-f1bd-42b3-932e-41ea40fd7b7b';
		cat.Parent_Id__c = '85522a4o-31rt-45d5-8w2a-41ea40fd7c7d';
		cat.Type__c = 'Category';
		insert cat;

		Categories_Tag__c tag = new Categories_Tag__c();
		tag.Description__c = 'Test Tag' ;
		tag.LMS_ID__c = '7a934842-c88a-4dfc-aa73-3c8d95074de3';
		tag.Parent_Id__c = 'ty642f42-e74d-r78t-8w2a-4r85a7t78se8';
		tag.Type__c = 'Tag';
		insert tag;

		LMS_Settings__c LmsCustomSettings = new LMS_Settings__c();
		LmsCustomSettings.Name = 'EndPoint';
		LmsCustomSettings.Value__c = 'https://jjvus.sandbox.myabsorb.com/api/Rest/';
		insert LmsCustomSettings;

		LMS_Settings__c LmsCustomSettings1 = new LMS_Settings__c();
		LmsCustomSettings1.Name = 'ImageURL';
		LmsCustomSettings1.Value__c = 'https://jjvci.sandbox.myabsorb.com/Files/';
		insert LmsCustomSettings1;

		LMS_Settings__c LmsCustomSettings2 = new LMS_Settings__c();
		LmsCustomSettings2.Name = 'Password';
		LmsCustomSettings2.Value__c = 'Test.123';
		insert LmsCustomSettings2;

		LMS_Settings__c LmsCustomSettings3 = new LMS_Settings__c();
		LmsCustomSettings3.Name = 'PrivateKey';
		LmsCustomSettings3.Value__c = '58091a9c-7077-40ee-927b-143ea1af810a';
		insert LmsCustomSettings3;

		LMS_Settings__c LmsCustomSettings4 = new LMS_Settings__c();
		LmsCustomSettings4.Name = 'Username';
		LmsCustomSettings4.Value__c = 'prorestadmin';
		insert LmsCustomSettings4;

		LMS_Settings__c LmsCustomSettings5 = new LMS_Settings__c();
		LmsCustomSettings5.Name = 'DEPT_ID_ja_JP';
		LmsCustomSettings5.Value__c = '20bf217e-41a7-44b3-a1c7-2e084a44d936';
		insert LmsCustomSettings5;

		LMS_Settings__c LmsCustomSettings6 = new LMS_Settings__c();
		LmsCustomSettings6.Name = 'ja_JP-' + RL.id;
		LmsCustomSettings6.Value__c = '154378c7-49da-4ca8-850f-471d59e64b96';
		insert LmsCustomSettings6;

		LMS_Settings__c LmsCustomSettings7 = new LMS_Settings__c();
		LmsCustomSettings7.Name = 'ja_JP';
		LmsCustomSettings7.Value__c = '1';
		insert LmsCustomSettings7;

		LMS_Settings__c LmsCustomSettings8 = new LMS_Settings__c();
		LmsCustomSettings8.Name = 'Token';
		LmsCustomSettings8.Value__c = 'TEST TOKEN';
		insert LmsCustomSettings8;

		B2B_Custom_Exceptions__c BCE1 = new B2B_Custom_Exceptions__c();
		BCE1.Name = 'PC0013';
		BCE1.Error_Description__c = 'Invalid filter parameter.';
		insert BCE1;

		B2B_Custom_Exceptions__c BCE2 = new B2B_Custom_Exceptions__c();
		BCE2.Name = 'PC0014';
		BCE2.Error_Description__c = 'Required parameters missing.';
		insert BCE2;

		B2B_Custom_Exceptions__c BCE3 = new B2B_Custom_Exceptions__c();
		BCE3.Name = 'PC0011';
		BCE3.Error_Description__c = 'Required field UserId missing.';
		insert BCE3;

		B2B_Custom_Exceptions__c BCE4 = new B2B_Custom_Exceptions__c();
		BCE4.Name = 'PC0019';
		BCE4.Error_Description__c = 'The given UserId is invalid';
		insert BCE4;

		B2B_Custom_Exceptions__c BCE5 = new B2B_Custom_Exceptions__c();
		BCE5.Name = 'GEN0001';
		BCE5.Error_Description__c = 'An unexpected error has occurred. Please contact your System Administrator.';
		insert BCE5;

		LmsUtils.runClassFrCategoryandTag();
		LmsUtils.runBatchClassForCourseUpdate();

		Account A = new Account();
		A.Name = 'Generic consumer Account';
		A.shippingcountry = 'United States';
		A.shippingstreet = 'Test Street';
		A.shippingcity = 'Test city';
		A.shippingpostalcode = 'TEST20017';
		insert A;

		Portal_Settings__c GA = new Portal_Settings__c();
		GA.Name = 'GenericAccount Id';
		GA.Value__c = A.Id;
		insert GA;

		JJVC_Contact_Recordtype__mdt rectype = [select MasterLabel from JJVC_Contact_Recordtype__mdt where DeveloperName = 'HCP_Contacts_RecordtypeId'];
		Id HCPRecordTypeId = rectype.MasterLabel;

		/* wrapper class covering*/
		LmsUtils.CourseDTO lms = new LmsUtils.CourseDTO();
		lms.Id = '15654184-afc0-48eb-8838-12c14bf51a56';
		lms.CourseId = 'fddgjj2c14bf51a56';
		lms.CourseName = 'Test name';
		lms.Progress = 5;
		lms.Score = 5;
		lms.Status = 'Active';
		lms.DateCompleted = 'er78ee4';
		lms.DateStarted = 'fg';

		LmsUtils.CourseDTOFrUpdate lmsfr = new LmsUtils.CourseDTOFrUpdate();
		lmsfr.id = lms.Id;

		LmsUtils.Category lmsc = new LmsUtils.Category();
		lmsc.id = lms.CourseId;
		lmsc.Name = 'test cname';

		LmsUtils.Tag lmst = new LmsUtils.Tag();
		lmst.id = lms.CourseId;
		lmst.Name = 'test cname';

		LmsUtils.image img = new LmsUtils.image();
		img.Name = 'sfgsh';
		img.URL = 'ffrrt4';

		/* course insertion */
		Course__c course = new Course__c();
		course.ActiveStatus__c = 'Active';
		course.CategoryId__c = '15654184-afc0-48eb-8838';
		course.Main_Course_Image__c = 'dggd';
		course.Landing_page_Image__c = 'csdfsg';
		course.Online_CoursePage_Image__c = 'sdfg';
		course.Related_Content_Image__c = 'df5CWE';
		course.Description__c = 'some test data';
		course.ExpireDuration__c = 'test ExpireDuration';
		course.LMS_ID__c = '15654184-afc0-48eb-8838-12c14bf51a56';
		course.Type__c = 'OnlineCourse';
		course.Notes__C = 'tets note';
		insert course;

		LmsUtils.DTOForGetEnrollmentsSts lmsen = new LmsUtils.DTOForGetEnrollmentsSts();
		lmsen.StartDate = 'sdaf4';
		lmsen.CompletedDate = 'sd4f';
		lmsen.Name = 'tets name';
		lmsen.CourseId = course.id;
		lmsen.Description = 'test data';
		lmsen.Status = 'active';
		lmsen.LMSID = 'fd4ger4gte4g';
		lmsen.Type = 'online course';
		lmsen.Language = 'ja_JP';

		LmsUtils.ResourcesDTOFrUpdate lmsr = new LmsUtils.ResourcesDTOFrUpdate();
		lmsr.ModuleId = 'c5dsfg';
		lmsr.Description = 'test';
		lmsr.Id = 'c4wf4r';

		Contact cc = new Contact();
		cc.FirstName = 'Acuvue';
		cc.LastName = 'Pro user';
		cc.Salutation = 'Mr.';
		cc.Email = 'Protestuser+acuvue_Learning@gmail.com';
		cc.School_Name__c = 'Test';
		cc.Graduation_Year__c = '2014';
		cc.Degree__c = 'Test';
		cc.BirthDate = date.today();
		cc.AccountId = A.iD;
		cc.RecordTypeId = HCPRecordTypeId;
		cc.user_Role__c = RL.Id;
		insert cc;

		Contact cc1 = new Contact();
		cc1.FirstName = 'Acuvue';
		cc1.LastName = 'Pro user1';
		cc1.Salutation = 'Mr.';
		cc1.Email = 'Protestuser+acuvue_Learning1@gmail.com';
		cc1.School_Name__c = 'Test';
		cc1.Graduation_Year__c = '2014';
		cc1.Degree__c = 'Test';
		cc1.BirthDate = date.today();
		cc1.AccountId = A.iD;
		cc1.RecordTypeId = HCPRecordTypeId;
		cc1.user_Role__c = RL.Id;
		insert cc1;

		User u = new User();
		u.Title = 'Mr.';
		u.FirstName = 'Acuvue';
		u.LastName = 'Pro user';
		u.Email = 'Protestuser+acuvue_Learning@gmail.com';
		u.UserName = 'Protestuser+acuvue_Learning@gmail.com';
		u.Occupation__c = RL.Name;
		u.Secret_Question__c = 'birth_month';
		u.SecretAnswerSalt__c = RegistrationService.generateSalt();
		Blob hash = Crypto.generateDigest('SHA-512', Blob.valueOf('09/09/1992' + u.SecretAnswerSalt__c));
		u.Secret_Answer__c = EncodingUtil.base64Encode(hash);
		u.Communication_Agreement__c = true;
		u.Localesidkey = 'ja_JP';
		u.Languagelocalekey = 'ja';
		u.EmailEncodingKey = 'ISO-8859-1';
		u.Alias = (u.LastName.length() > 7) ? u.LastName.substring(0, 7) : u.LastName;
		u.TimeZoneSidKey = 'America/New_York';
		u.CommunityNickname = B2B_Utils.generateGUID();
		u.NPINumber__c = 'TESTPRO01235';
		u.ContactId = cc.Id;
		u.ProfileId = Portal_Settings__c.getValues('ProfileId').Value__c;
		u.LMS_ID__c = '0cbd99f0-ed03-4d68-8c48-3a647de66e4b';
		u.Unique_User_Id__c = cc.id;
		insert u;

		User u1 = new User();
		u1.Title = 'Mr.';
		u1.FirstName = 'Acuvue';
		u1.LastName = 'Pro user1';
		u1.Email = 'Protestuser+acuvue_Learning1@gmail.com';
		u1.UserName = 'Protestuser+acuvue_Learning1@gmail.com';
		u1.Occupation__c = RL.Name;
		u1.Secret_Question__c = 'birth_month';
		u1.SecretAnswerSalt__c = RegistrationService.generateSalt();
		Blob hash1 = Crypto.generateDigest('SHA-512', Blob.valueOf('09/09/1992' + u.SecretAnswerSalt__c));
		u1.Secret_Answer__c = EncodingUtil.base64Encode(hash);
		u1.Communication_Agreement__c = true;
		u1.Localesidkey = 'ja_JP';
		u1.Languagelocalekey = 'ja';
		u1.EmailEncodingKey = 'ISO-8859-1';
		u1.Alias = (u.LastName.length() > 7) ? u.LastName.substring(0, 7) : u.LastName;
		u1.TimeZoneSidKey = 'America/New_York';
		u1.CommunityNickname = B2B_Utils.generateGUID();
		u1.NPINumber__c = 'TESTPRO012356';
		u1.ContactId = cc1.Id;
		u1.ProfileId = Portal_Settings__c.getValues('ProfileId').Value__c;
		u.LMS_ID__c = '';
		u1.Unique_User_Id__c = cc1.id;
		Insert u1;
		return u;
	}
	static testmethod void unittest1()
	{
		insertSettings();
		LmsUtils.updateLMSProfiledetails('ExternalId', 'Firstname', 'Lastname', 'Role', 'Email', 'Uname', 'ja_JP');
		string response = string.valueof(LmsUtils.getResourcesforCourses('fec68548-1047-472d-b427-b54b2facd2ba'));
		system.assertNotEquals(response,null);
		LmsUtils.getLessonsForCourse('0d1c1f3f-3d8f-4cff-9429-4c887f0fb255');
	}
}