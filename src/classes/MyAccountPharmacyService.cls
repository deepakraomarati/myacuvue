/**
* File Name: MyAccountPharmacyService
* Author : SathishKumar | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : Get reporting information for sales rep from practices on date basis.
			   Send promotional emails to practice users
			   Track users Marketing Material information 
* Copyright (c) $2018 Johnson & Johnson
*/
@RestResource(URLMapping='/apex/myaccount/v1/MyAccountPracticeService/*')
global without sharing class MyAccountPharmacyService extends BaseRestResponse
{
	@HttpGet
	global static BaseRestResponse doGet()
	{
		RestRequest req = RestContext.request;
		String url = req.requestURI;
		List<String> ls = url.split('/');
		String action = ls[5];
		if (action == 'GetPracticesBySalesrep')
		{
			ResultsPracticeDetails red = new ResultsPracticeDetails();
			String SalesRepId = RestContext.request.params.get('Userid');
			red.Practices = getPharmacyDetailsofSalesrep(SalesRepId);
			return red;
		}
		if (action == 'GetPracticeDetailsById')
		{
			String PracticeId = RestContext.request.params.get('PracticeId');
			String FDate = RestContext.request.params.get('FromDate');
			String TDate = RestContext.request.params.get('ToDate');
			Date fd = null;
			Date td = null;
			if (FDate != null)
			{
				fd = Date.parse(FDate);
			}
			if (TDate != null)
			{
				td = Date.parse(TDate).addDays(1);
			}
			return getPracticeDetailedResponse(PracticeId, fd, td);
		}
		return null;
	}

	public static List<PracticeDetails> getPharmacyDetailsofSalesrep(String UserId)
	{
		List<PracticeDetails> results = new List<PracticeDetails>();
		JJVC_Contact_Recordtype__mdt rectype = [SELECT MasterLabel FROM JJVC_Contact_Recordtype__mdt WHERE DeveloperName = 'HCP_Contacts_RecordtypeId'];
		Id HCPRecordTypeId = rectype.MasterLabel;
		Map<Id, Account> accountMap = new Map<Id, Account>([
				SELECT Id, Name, Email__c, Opt_Out__c,
						OutletNumber__c,
						ShippingStreet,
						ShippingCity,
						ShippingPostalCode,
						ShippingState,
						ShippingCountry,
				(
						SELECT Id,AccountId
						FROM Contacts
						WHERE Id in
						(
								SELECT Contact__c
								FROM AccountContactRole__c
								WHERE Account_Ownership_Flag__c = true AND
								Approval_Status__c = 'Accepted' AND
								Contact__R.RecordTypeId = :HCPRecordTypeId
						)
				)
				FROM Account
				WHERE ownerid = :Userid
		]);
		Map<Id, Id> contactMap = new Map<Id, Id>();
		Set<Id> ConId = new Set<id>();
		for (Account a : accountMap.values())
		{
			if (a.Contacts != null && a.Contacts.size() > 0)
			{
				contactMap.put(a.Contacts[0].AccountId, a.Contacts[0].Id);
				ConId.add(a.Contacts[0].Id);
			}
		}
		Map<Id, User> userMap = new Map<Id, User>([
				SELECT Id,Name, Email, AccountId, ContactId
				FROM User
				WHERE isActive = true AND
				AccountId In:accountMap.keyset() AND
				ContactId in
				(
						SELECT Contact__c
						FROM AccountContactRole__c
						WHERE Contact__c in:ConId
						AND Account_Ownership_Flag__c = true
						AND Approval_Status__c = 'Accepted'
				)
		]);
		Map<Id, User> contactUserMap = new Map<Id, User>();
		for (User a : userMap.values())
		{
			contactUserMap.put(a.ContactId, a);
		}
		for (Account a : accountMap.values())
		{
			if (contactMap.ContainsKey(a.Id) && contactUserMap.ContainsKey(contactMap.get(a.Id)))
			{
				Id ConsId = contactMap.get(a.Id);
				User u = contactUserMap.get(ConsId);
				PracticeDetails p = new PracticeDetails(a);
				p.OwnerId = u.Id;
				p.OwnerName = u.Name;
				results.add(p);
			}
			else
			{
				PracticeDetails p = new PracticeDetails(a);
				results.add(p);
			}
		}
		return results;
	}

	public static PracticeDetailedResponse getPracticeDetailedResponse(Id PracticeId, Date FDate, Date TDate)
	{
		Map<Id, User> userMap = new Map<Id, User>();
		List<User> practiceUser = new List<User>();
		Set<ID> conIds = new Set<ID>();
		List<User> userList = new List<User>();
		List<MMHistory> marketingMeterialHistroyList = new List<MMHistory>();
		Account Acc = [SELECT Id, Name,Email__c FROM Account WHERE Id = :PracticeId];
		List<AggregateResult> reportCenterCount;
		List<AggregateResult> Top3Resource;
		List<AggregateResult> Top3Page;
		List<AggregateResult> Top3MMDownLoad;
		JJVC_Contact_Recordtype__mdt rectype = [SELECT MasterLabel FROM JJVC_Contact_Recordtype__mdt WHERE DeveloperName = 'HCP_Contacts_RecordtypeId'];
		Id HCPRecordTypeId = rectype.MasterLabel;
		if (FDate == null || TDate == null)
		{
			reportCenterCount =
			[
					SELECT Type__c, Count(Id)
					FROM Report_Center_Data__c
					WHERE Account__c = :PracticeId
					Group by type__c
			];
			Top3Resource =
			[
					SELECT Resource_Product__c, Count(Id)
					FROM Report_Center_Data__c
					WHERE Account__c = :PracticeId AND (Type__c = 'Resources Emailed' OR Type__c = 'Resources Downloaded')
					group by Resource_Product__c
					order by Count(ID) Desc
					limit 3
			];
			Top3Page =
			[
					SELECT Resource_Product__c, Count(Id)
					FROM Report_Center_Data__c
					WHERE Account__c = :PracticeId AND
					Type__c = 'Product Page Views'
					group by Resource_Product__c
					order by Count(ID) Desc
					limit 3
			];
			Top3MMDownLoad =
			[
					SELECT Resource_Product__c, Count(Id)
					FROM Report_Center_Data__c
					WHERE Account__c = :PracticeId AND
					Type__c = 'Marketing Material Downloaded'
					group by Resource_Product__c
					order by Count(ID) Desc
					limit 3
			];

		}
		if (FDate != null && TDate != null)
		{
			reportCenterCount =
			[
					SELECT Type__c, Count(Id)
					FROM Report_Center_Data__c
					WHERE Account__c = :PracticeId AND CreatedDate >= :FDate AND CreatedDate <= :TDate
					group by type__c
			];
			Top3Resource =
			[
					SELECT Resource_Product__c, Count(Id)
					FROM Report_Center_Data__c
					WHERE Account__c = :PracticeId AND (Type__c = 'Resources Emailed' OR Type__c = 'Resources Downloaded')
					AND CreatedDate >= :FDate and CreatedDate <= :TDate
					group by Resource_Product__c
					order by Count(ID) Desc
					limit 3
			];
			Top3Page =
			[
					SELECT Resource_Product__c, Count(Id)
					FROM Report_Center_Data__c
					WHERE Account__c = :PracticeId and Type__c = 'Product Page Views' AND CreatedDate >= :FDate AND CreatedDate <= :TDate
					group by Resource_Product__c
					order by Count(ID) Desc
					limit 3
			];
			Top3MMDownLoad =
			[
					SELECT Resource_Product__c, Count(Id)
					FROM Report_Center_Data__c
					WHERE Account__c = :PracticeId AND Type__c = 'Marketing Material Downloaded'
					AND CreatedDate >= :FDate AND CreatedDate <= :TDate
					group by Resource_Product__c
					order by Count(ID) Desc
					limit 3
			];
		}
		Map<Id, AccountContactRole__c> mpACR = new Map<Id, AccountContactRole__c>([
				SELECT Id,
						Account__c,
						Contact__c
				FROM AccountContactRole__c
				WHERE Account__c = :PracticeId AND
				Approval_status__c = 'Accepted' AND
				Contact__r.RecordTypeId = :HCPRecordTypeId
		]);
		userList =
		[
				SELECT Id, Name,
						FirstName,
						LastName,
						ContactId,
						Email,
						Occupation__c,
						Account.Name,
						Account.Id
				FROM User
				WHERE isActive = true AND
				ContactId in
				(
						SELECT Contact__c
						FROM AccountContactRole__c
						WHERE Id in:mpACR.keyset()
				)
		];

		for (User emuser : userList)
		{
			conIds.add(emuser.contactid);
		}
		if (FDate != null && TDate != null)
		{
			userList =
			[
					SELECT Id, Name,
							FirstName,
							LastName,
							ContactId,
							Email,
							Occupation__c,
							Account.Name,
							Account.Id
					FROM User
					WHERE isActive = true and
					CreatedDate >= :FDate and CreatedDate <= :TDate and
					ContactId in (SELECT Contact__c FROM AccountContactRole__c WHERE id in:mpACR.keyset())
			];
		}
		for (user u : userList)
		{
			userMap.put(u.ContactId, u);
			practiceUser.add(u);
		}
		AggregateResult[] arStarted =
		[
				SELECT Count(Course__c)
				FROM CourseAssignment__c
				WHERE Contact__r.AccountId = :PracticeId and
				(Status__c = 'In Progress' OR Status__c = 'Completed') and
				Contact__c in
				(
						SELECT Contact__c
						FROM AccountContactRole__c
						WHERE Approval_Status__c = 'Accepted'and
						Contact__r.RecordTypeId = :HCPRecordTypeId
				)
				Group By Id
		];

		AggregateResult[] arCompleted =
		[
				SELECT Count(Course__c)
				FROM CourseAssignment__c
				WHERE Contact__r.AccountId = :PracticeId and
				Status__c = 'Completed' and
				Contact__c in
				(
						SELECT Contact__c
						FROM AccountContactRole__c
						WHERE Approval_Status__c = 'Accepted' and
						Contact__r.RecordTypeId = :HCPRecordTypeId
				)
				Group By Id
		];
		AggregateResult[] TopCours ;
		TopCours =
		[
				SELECT Course__r.Name Cname,Course__c cId, Count(ID)
				FROM CourseAssignment__c
				WHERE Contact__r.AccountId = :PracticeId and
				Contact__r.RecordTypeId = :HCPRecordTypeId and
				Status__c = 'Completed'
				group by Course__r.Name, Course__c
				Order By Count(ID) desc
				limit 3
		];
		if (FDate != null && TDate != null)
		{
			arStarted =
			[
					SELECT Count(Course__c)
					FROM CourseAssignment__c
					WHERE Contact__r.AccountId = :PracticeId and
					(Status__c = 'In Progress' or Status__c = 'Completed') and
					Contact__c in
					(
							SELECT Contact__c
							FROM AccountContactRole__c
							WHERE Approval_Status__c = 'Accepted' and
							Contact__r.RecordTypeId = :HCPRecordTypeId
					) and
					(CreatedDate >= :FDate and CreatedDate <= :TDate)
					Group By Id
			];

			arCompleted =
			[
					SELECT Count(Course__c)
					FROM CourseAssignment__c
					WHERE Contact__r.AccountId = :PracticeId and
					Status__c = 'Completed' and
					Contact__c in
					(
							SELECT Contact__c
							FROM AccountContactRole__c
							WHERE Approval_Status__c = 'Accepted' and
							Contact__r.RecordTypeId = :HCPRecordTypeId
					) and
					(CompletionDate__c >= :FDate and CompletionDate__c <= :TDate)
					Group By Id
			];
			TopCours =
			[
					SELECT Course__r.Name Cname,Course__c cId, Count(ID)
					FROM CourseAssignment__c
					WHERE Contact__r.AccountId = :PracticeId and
					Contact__r.RecordTypeId = :HCPRecordTypeId and
					Status__c = 'Completed' and (CompletionDate__c >= :FDate and
					CompletionDate__c <= :TDate)
					group by Course__r.Name, Course__c
					Order By Count(ID) desc
					limit 3
			];
		}
		List<TopCourse> topCourseList = new List<TopCourse>();
		if (TopCours != null)
		{
			for (AggregateResult ars : TopCours)
			{
				TopCourse t = new TopCourse();
				t.Name = (String) ars.get('Cname');
				t.CourseId = (String) ars.get('CId');
				topCourseList.add(t);
			}
		}
		PracticeDetailedResponse result = new PracticeDetailedResponse();
		if (practiceUser.size() > 0)
		{
			result.PracticeName = practiceUser[0].Account.Name;
			result.PracticeId = practiceUser[0].AccountId;
			result.ActiveUsers = practiceUser.size();

			List<UserDetails> usdet = new List<UserDetails>();
			for (user us : practiceUser)
			{
				UserDetails u = new UserDetails(us);
				usdet.add(u);
			}
			result.UserDetails = usdet;
		}
		else
		{
			result.PracticeName = Acc.Name;
			result.PracticeId = Acc.Id;
			result.ActiveUsers = 0;
			result.CoursesStarted = 0;
			result.CourseCompleted = 0;
			result.ResourcesEmailed = 0;
			result.ResourcesDownloaded = 0;
			result.ProductPageViews = 0;
			result.UserDetails = null;
			result.TopCourses = null;
		}
		if (topCourseList.size() > 0 && topCourseList != null)
		{
			result.TopCourses = topCourseList;
		}
		if (arStarted.size() > 0 && arStarted != null)
		{
			result.CoursesStarted = arStarted.size();
		}
		if (arCompleted.size() > 0 && arCompleted != null)
		{
			result.CourseCompleted = arCompleted.size();
		}
		if (reportCenterCount.size() > 0)
		{
			string typo = 'Type__c';
			for (AggregateResult ars : reportCenterCount)
			{
				if (ars.get(typo) == 'Resources Emailed') result.ResourcesEmailed = (Integer) ars.get('expr0');
				if (ars.get(typo) == 'Resources Downloaded') result.ResourcesDownloaded = (Integer) ars.get('expr0');
				if (ars.get(typo) == 'Product Page Views') result.ProductPageViews = (Integer) ars.get('expr0');
				if (ars.get(typo) == 'Marketing Material Downloaded') result.mmDownloads = (Integer) ars.get('expr0');
			}
		}
		if (Top3Resource.size() > 0)
		{
			List<TopResources> topResourcesList = new List<TopResources>();
			for (AggregateResult arRes : Top3Resource)
			{
				TopResources topResources = new TopResources();
				topResources.Name = (String) arRes.get('Resource_Product__c');
				topResourcesList.add(topResources);
			}
			result.TopResources = topResourcesList;
		}
		if (Top3Page.size() > 0)
		{
			List<TopVisitedPages> topVisitedPages = new List<TopVisitedPages>();
			for (AggregateResult arpg : Top3Page)
			{
				TopVisitedPages tpg = new TopVisitedPages();
				tpg.Name = (String) arpg.get('Resource_Product__c');
				topVisitedPages.add(tpg);
			}
			result.TopVisitedPages = topVisitedPages;
		}
		if (Top3MMDownLoad.size() > 0)
		{
			List<TopMMDownloads> topDownloads = new List<TopMMDownloads>();
			for (AggregateResult arpg : Top3MMDownLoad)
			{
				TopMMDownloads tpmm = new TopMMDownloads();
				tpmm.Name = (String) arpg.get('Resource_Product__c');
				topDownloads.add(tpmm);
			}
			result.TopMMDownloads = topDownloads;
		}
		List<Report_Center_Data__c> reportCenterDataList = new List<Report_Center_Data__c>();
		if (FDate == null || TDate == null)
		{
			reportCenterDataList =
			[
					SELECT Id,
							name,
							Resource_Product__c,
							Contact__r.Name,
							CreatedDate,
							Account__c,
							Type__c
					FROM Report_Center_Data__c
					WHERE Account__c = :Acc.id and
					Type__c = 'Marketing Material Downloaded'
					ORDER BY Createddate ASC
			];
		}
		else
		{
			reportCenterDataList =
			[
					SELECT Id,
							name,
							Resource_Product__c,
							Contact__r.Name,
							CreatedDate,
							Account__c,
							Type__c
					FROM Report_Center_Data__c
					WHERE Account__c = :Acc.id and
					Type__c = 'Marketing Material Downloaded' and
					(CreatedDate >= :FDate and CreatedDate <= :TDate)
					ORDER BY Createddate ASC
			];
		}
		for (Report_Center_Data__c reportCenterData : reportCenterDataList)
		{
			String dateOutput = reportCenterData.CreatedDate.format('MM/dd/yyyy');
			MMHistory marketingMeterialHistory = new MMHistory();
			marketingMeterialHistory.ResourceProduct = reportCenterData.Resource_Product__c;
			marketingMeterialHistory.ContactName = reportCenterData.Contact__r.Name;
			marketingMeterialHistory.DownloadedDate = dateOutput;
			marketingMeterialHistroyList.add(marketingMeterialHistory);
		}
		result.MMHistoryResult = marketingMeterialHistroyList;
		result.EmailResults = getSentEmailTracking(conids, FDate, TDate);
		return result;
	}

	private static List<EmailResult> getSentEmailTracking(Set<ID> conids, Date FDate, Date TDate)
	{
		Map<ID, List<et4ae5__IndividualEmailResult__c>> individualEmailResultMap = new Map<ID, List<et4ae5__IndividualEmailResult__c>>();
		List<et4ae5__IndividualEmailResult__c> individualEmailResultList = new List<et4ae5__IndividualEmailResult__c>();
		List<EmailResult> emailResultList = new List<EmailResult>();
		List<Campaign> campaignList =
		[
				SELECT Id,
						Name,
						Type,
				(
						SELECT Id,
								ContactId,
								SalesRep_Email__c,
								SalesRep_Name__c
						FROM CampaignMembers
						WHERE ContactId IN :conids
				)
				FROM Campaign
				WHERE Type = 'SALESREP'
		];
		Set<ID> campaignMemberId = new Set<ID>();
		for (Campaign cmp : campaignList)
		{
			for (CampaignMember member : cmp.CampaignMembers)
			{
				campaignMemberId.add(member.id);
			}
		}
		if (FDate == null || TDate == null)
		{
			individualEmailResultList =
			[
					SELECT Id,
							et4ae5__CampaignMemberId__c,
							et4ae5__Contact__c,
							et4ae5__Contact__r.Firstname,
							et4ae5__Contact__r.lastname,
							et4ae5__Contact__r.email,
							et4ae5__Contact_ID__c,
							et4ae5__DateOpened__c,
							et4ae5__DateSent__c,
							et4ae5__Opened__c,
							et4ae5__TriggeredSendDefinition__c,
							et4ae5__TriggeredSendDefinitionName__c
					FROM et4ae5__IndividualEmailResult__c
					WHERE et4ae5__CampaignMemberId__c = :campaignMemberId
			];			
		}
		else
		{
			individualEmailResultList =
			[
					SELECT Id,
							et4ae5__CampaignMemberId__c,
							et4ae5__Contact__c,
							et4ae5__Contact__r.Firstname,
							et4ae5__Contact__r.lastname,
							et4ae5__Contact__r.email,
							et4ae5__Contact_ID__c,
							et4ae5__DateOpened__c,
							et4ae5__DateSent__c,
							et4ae5__Opened__c,
							et4ae5__TriggeredSendDefinition__c,
							et4ae5__TriggeredSendDefinitionName__c
					FROM et4ae5__IndividualEmailResult__c
					WHERE et4ae5__CampaignMemberId__c = :campaignMemberId AND
					(et4ae5__DateSent__c >= :FDate AND et4ae5__DateSent__c <= :TDate)
			];
		}
		for (et4ae5__IndividualEmailResult__c track : individualEmailResultList)
		{
			if (individualEmailResultMap.containskey(track.et4ae5__CampaignMemberId__c))
			{
				individualEmailResultMap.get(track.et4ae5__CampaignMemberId__c).add(track);
			}
			else
			{
				List<et4ae5__IndividualEmailResult__c> trackIndividualEmailResultList = new List<et4ae5__IndividualEmailResult__c>();
				trackIndividualEmailResultList.add(track);
				individualEmailResultMap.put(track.et4ae5__CampaignMemberId__c, trackIndividualEmailResultList);
			}
		}
		for (campaign trresult : campaignList)
		{
			for (CampaignMember cmbrs : trresult.CampaignMembers)
			{
				if (individualEmailResultMap.containskey(cmbrs.id))
				{
					for (et4ae5__IndividualEmailResult__c emresults : individualEmailResultMap.get(cmbrs.id))
					{
						EmailResult er = new EmailResult();
						er.EmailTriggerId = trresult.Name;
						er.FirstName = emresults.et4ae5__Contact__r.Firstname;
						er.LastName = emresults.et4ae5__Contact__r.lastname;
						er.Email = emresults.et4ae5__Contact__r.email;
						er.ContactID = emresults.et4ae5__Contact_ID__c;
						er.SentDate = string.valueOf(emresults.et4ae5__DateSent__c);
						er.OpenDate = string.valueOf(emresults.et4ae5__DateOpened__c);
						emailResultList.add(er);
					}
				}
			}
		}
		return emailResultList;
	}

	@HTTPPOST
	global static String doPOST()
	{
		RestRequest req = RestContext.request;
		Blob body = req.requestBody;
		ResourceDetails rDetails = (ResourceDetails) System.JSON.deserialize(body.toString(), ResourceDetails.class);
		if (rDetails.PracticeId != null && rDetails.PracticeId != '')
		{
			User u = [SELECT Id, ContactId FROM User WHERE Id = :rDetails.UserId];
			Report_Center_Data__c rcData = new Report_Center_Data__c();
			rcData.Account__c = rDetails.PracticeId;
			rcData.Type__c = rDetails.Type;
			rcData.Resource_Product__c = rDetails.ResourceProductId;
			rcData.Contact__c = u.ContactId;
			insert rcData;
			return '{"Status": "Success"}';
		}
		return null;
	}

	@HTTPPUT
	global static String doPUT()
	{
		RestRequest req = RestContext.request;
		Blob body = req.requestBody;
		EmailSendData EmailDetails = (EmailSendData) System.JSON.deserialize(body.toString(), EmailSendData.class);

		if (EmailDetails.EmailTriggerId != null && EmailDetails.EmailTriggerId != '')
		{
			List<Campaign> campaignList =
			[
					SELECT Id,
							Name,
							Type,
					(
							SELECT Id,
									ContactId,
									SalesRep_Email__c,
									SalesRep_Name__c
							FROM CampaignMembers
					)
					FROM Campaign
					WHERE Name = :EmailDetails.EmailTriggerId
			];
			if (campaignList.size() > 0)
			{
				Map<ID, CampaignMember> campaignMemberMap = new Map<ID, CampaignMember>();
				for (CampaignMember cm : campaignList[0].CampaignMembers)
				{
					campaignMemberMap.put(cm.contactId, cm);
				}
				List<CampaignMember> member = new List<CampaignMember>();
				User salesRep = [SELECT ID,Name,Email FROM User WHERE ID = :EmailDetails.userID];
				String repName = salesRep.Name;
				String repEmail = salesRep.Email;
				for (EmailUser emusers : EmailDetails.Users)
				{
					if (campaignMemberMap.containskey(emusers.contactID))
					{
						CampaignMember cmb = campaignMemberMap.get(emusers.contactID);
						cmb.SalesRep_Email__c = repEmail;
						cmb.SalesRep_Name__c = repName;
						member.add(cmb);
					}
					else
					{
						CampaignMember cmb = new CampaignMember();
						cmb.ContactId = emusers.contactId;
						cmb.CampaignID = campaignList[0].id;
						cmb.Status = 'Send';
						cmb.SalesRep_Email__c = repEmail;
						cmb.SalesRep_Name__c = repName;
						member.add(cmb);
					}
				}
				if (member.size() > 0)
				{
					upsert member;
				}
			}
		}
		return '{"Status": "Success"}';
	}

	public override void restResponse()
	{
		/*override method*/
	}
	public class ResultsPracticeDetails extends BaseRestResponse
	{
		List<PracticeDetails> Practices { get; set; }
		public override void restResponse()
		{/*override method*/
		}
	}

	public class PracticeDetailedResponse extends BaseRestResponse
	{
		String PracticeName { get; set; }
		String PracticeId { get; set; }
		Integer ActiveUsers { get; set; }
		Integer CoursesStarted { get; set; }
		Integer CourseCompleted { get; set; }
		integer ResourcesEmailed { get; set; }
		integer ResourcesDownloaded { get; set; }
		integer ProductPageViews { get; set; }
		integer MMDownloads { get; set; }
		UserDetails[] UserDetails { get; set; }
		TopCourse[] TopCourses { get; set; }
		TopResources[] TopResources { get; set; }
		TopMMDownloads[] TopMMDownloads { get; set; }
		TopVisitedPages[] TopVisitedPages { get; set; }
		EmailResult[] EmailResults { get; set; }
		MMHistory[] MMHistoryResult { get; set; }
		public override void restResponse()
		{/*override method*/
		}
	}

	public class TopResources extends BaseRestResponse
	{
		String Name { get; set; }
		public override void restResponse()
		{/*override method*/
		}
	}

	public class TopMMDownloads extends BaseRestResponse
	{
		String Name { get; set; }
		public override void restResponse()
		{/*override method*/
		}
	}

	public class TopVisitedPages extends BaseRestResponse
	{
		String Name { get; set; }
		public override void restResponse()
		{/*override method*/
		}
	}

	public class EmailResult extends BaseRestResponse
	{
		String EmailTriggerId { get; set; }
		String SentDate { get; set; }
		String OpenDate { get; set; }
		String FirstName { get; set; }
		String LastName { get; set; }
		String Email { get; set; }
		String ContactId { get; set; }
		public override void restResponse()
		{/*override method*/
		}
	}

	public class MMHistory extends BaseRestResponse
	{
		String ResourceProduct { get; set; }
		string ContactName { get; set; }
		string DownloadedDate { get; set; }
		public override void restResponse()
		{/*override method*/
		}
	}

	public class PracticeDetails extends BaseRestResponse
	{
		String Name { get; set; }
		String Id { get; set; }
		String SAPAccountNumber { get; set; }
		String EmailAddress { get; set; }
		String Street { get; set; }
		String City { get; set; }
		String State { get; set; }
		String PostalCode { get; set; }
		String Country { get; set; }
		String OwnerId { get; set; }
		String OwnerName { get; set; }
		boolean isEmailOptout { get; set; }
		public override void restResponse()
		{/*override method*/
		}

		public PracticeDetails(Account acc)
		{
			Name = acc.Name;
			Id = acc.Id;
			SAPAccountNumber = acc.OutletNumber__c;
			EmailAddress = acc.Email__c;
			Street = acc.shippingStreet;
			City = acc.shippingcity;
			State = acc.shippingstate;
			PostalCode = acc.ShippingPostalCode;
			Country = acc.shippingcountry;
			isEmailOptout = acc.Opt_Out__c;
			OwnerId = null;
			OwnerName = null;
		}
	}

	public class UserDetails extends BaseRestResponse
	{
		String FirstName { get; set; }
		String LastName { get; set; }
		String EmailAddress { get; set; }
		String Role { get; set; }
		Id UserId { get; set; }
		public override void restResponse()
		{/*override method*/
		}

		public UserDetails(User U)
		{
			FirstName = u.FirstName;
			LastName = u.LastName;
			EmailAddress = u.Email;
			Role = u.Occupation__c;
			UserId = u.Id;
		}
	}

	public class TopCourse extends BaseRestResponse
	{
		String Name { get; set; }
		Id CourseId { get; set; }
		public override void restResponse()
		{/*override method*/
		}
	}

	public class ResourceDetails extends BaseRestResponse
	{
		String PracticeId { get; set; }
		String Type { get; set; }
		String ResourceProductId { get; set; }
		Id UserId { get; set; }
		public override void restResponse()
		{/*override method*/
		}
	}

	public class EmailSendData
	{
		String EmailTriggerId { get; set; }
		Id userID { get; set; }
		List<EmailUser> Users;
	}

	public class EmailUser
	{
		String ContactID;
		String PracticeID;
	}
}