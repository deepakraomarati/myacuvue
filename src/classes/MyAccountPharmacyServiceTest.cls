/**
* File Name: MyAccountPharmacyServiceTest
* Author: Sathishkumar | BICSGLBOAL
* Date Last Modified:  25-Oct-2018
* Description : Test class for MyAccountPharmacyService class
* Copyright (c) $2018 Johnson & Johnson
*/
@isTest(SeeAlldata=false)
public class MyAccountPharmacyServiceTest
{
	@istest static void Test_Createreportcenterdata()
	{
		User TestUser = MyAccountProfileServiceTest.insertuser();
		System.debug('####TestUser>>>>' + TestUser.accountid);
		User u = [Select id,AccountId from User where Id = :testuser.Id];
		RestRequest req1 = new RestRequest();
		RestResponse res1 = new RestResponse();
		string jsonBody = '{';
		jsonBody += ' "UserId": "' + u.id + '",';
		jsonBody += ' "PracticeId": "' + u.AccountId + '",';
		jsonBody += ' "Type": "Resources Emailed",';
		jsonBody += ' "ResourceProductId": "021453"';
		jsonBody += ' }';
		system.debug('Update profile body####' + jsonBody);
		req1.requestURI = '/apex/myaccount/v1/MyAccountPracticeService/';
		req1.httpMethod = 'POST';
		req1.requestbody = blob.valueof(jsonBody);
		RestContext.request = req1;
		RestContext.response = res1;
		String response = MyAccountPharmacyService.doPost();
		system.assertNotEquals(response, null);
	}

	@istest static void Test_sendemailtouser()
	{
		User TestUser = MyAccountProfileServiceTest.insertuser();

		Campaign cmp = new Campaign();
		cmp.Name = 'Test_Salesrep_campaign';
		cmp.Type = 'SALESREP';
		insert cmp;

		User u = [Select id,AccountId,contactid from User where Id = :testuser.Id];
		RestRequest req2 = new RestRequest();
		RestResponse res2 = new RestResponse();
		string jsonBody = '{';
		jsonBody += ' "UserId": "' + u.Id + '",';
		jsonBody += ' "EmailTriggerId": "Test_Salesrep_campaign",';
		jsonBody += ' "Users": [';
		jsonBody += ' {"ContactID": "' + u.contactid + '",';
		jsonBody += ' "PracticeID": "' + u.accountId + '"';
		jsonBody += ' }';
		jsonBody += ' ]';
		jsonBody += ' }';
		system.debug('Update profile body####' + jsonBody);
		req2.requestURI = '/apex/myaccount/v1/MyAccountPracticeService/';
		req2.httpMethod = 'PUT';
		req2.requestbody = blob.valueof(jsonBody);
		RestContext.request = req2;
		RestContext.response = res2;
		String response = MyAccountPharmacyService.doPut();
		system.assertNotEquals(response, null);

		RestRequest req3 = new RestRequest();
		RestResponse res3 = new RestResponse();
		req3.requestURI = '/apex/myaccount/v1/MyAccountPracticeService/';
		req3.httpMethod = 'PUT';
		req3.requestbody = blob.valueof(jsonBody);
		RestContext.request = req3;
		RestContext.response = res3;
		String response1 = MyAccountPharmacyService.doPut();
		system.assertNotEquals(response1, null);
	}

	@istest static void Test_getpraticedetails()
	{
		User TestUser = insertnewuser();

		Account A1 = new Account();
		A1.Name = 'Empty Test Account';
		A1.OutletNumber__c = '000235698';
		A1.shippingcountry = 'Japan';
		A1.shippingstreet = 'Test Street';
		A1.shippingcity = 'Test city';
		A1.shippingpostalcode = 'TEST20017';
		insert A1;

		User u = [Select id,FirstName, LastName, Email, Occupation__c, AccountId,Account.ownerid from User where Id = :testuser.Id];

		Report_Center_Data__c rc1 = new Report_Center_Data__c();
		rc1.Account__c = u.AccountId;
		rc1.Resource_Product__c = '12345';
		rc1.Type__c = 'Resources Emailed';
		insert rc1;

		Report_Center_Data__c rc2 = new Report_Center_Data__c();
		rc2.Account__c = u.AccountId;
		rc2.Resource_Product__c = '12345';
		rc2.Type__c = 'Resources Downloaded';
		insert rc2;

		Report_Center_Data__c rc3 = new Report_Center_Data__c();
		rc3.Account__c = u.AccountId;
		rc3.Resource_Product__c = '12345';
		rc3.Type__c = 'Product Page Views';
		insert rc3;

		Report_Center_Data__c rc4 = new Report_Center_Data__c();
		rc4.Account__c = u.AccountId;
		rc4.Resource_Product__c = '12345';
		rc4.Type__c = 'Resources Emailed';
		insert rc4;

		Report_Center_Data__c rc5 = new Report_Center_Data__c();
		rc5.Account__c = u.AccountId;
		rc5.Resource_Product__c = '12345';
		rc5.Type__c = 'Marketing Material Downloaded';
		insert rc5;

		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();

		req.requestURI = '/apex/myaccount/v1/MyAccountPracticeService/GetPracticesBySalesrep';
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		RestContext.request.params.put('Userid', u.Account.ownerid);
		BaseRestResponse response = MyAccountPharmacyService.doget();
		system.assertNotEquals(response, null);

		RestRequest req1 = new RestRequest();
		RestResponse res1 = new RestResponse();
		req1.requestURI = '/apex/myaccount/v1/MyAccountPracticeService/GetPracticeDetailsById';
		req1.httpMethod = 'GET';
		RestContext.request = req1;
		RestContext.response = res1;
		Date d = Date.today();
		RestContext.request.params.put('PracticeId', u.Accountid);
		RestContext.request.params.put('FromDate', d.month() + '/' + d.day() + '/' + d.year());
		RestContext.request.params.put('ToDate', d.month() + '/' + d.day() + '/' + d.year());
		BaseRestResponse response1 = MyAccountPharmacyService.doget();
		system.assertNotEquals(response1, null);

		RestRequest req2 = new RestRequest();
		RestResponse res2 = new RestResponse();
		req2.requestURI = '/apex/myaccount/v1/MyAccountPracticeService/GetPracticeDetailsById';
		req2.httpMethod = 'GET';
		RestContext.request = req2;
		RestContext.response = res2;
		RestContext.request.params.put('PracticeId', u.Accountid);
		MyAccountPharmacyService.doget();

		RestRequest req3 = new RestRequest();
		RestResponse res3 = new RestResponse();
		req3.requestURI = '/apex/myaccount/v1/MyAccountPracticeService/GetPracticeDetailsById';
		req3.httpMethod = 'GET';
		RestContext.request = req3;
		RestContext.response = res3;
		RestContext.request.params.put('PracticeId', A1.Id);
		MyAccountPharmacyService.doget();
	}

	public static user insertnewuser()
	{
		List<sObject> B2BCE = Test.loadData(B2B_Custom_Exceptions__c.sObjectType, 'B2BCustomexceptions');
		List<sObject> PLS = Test.loadData(Portal_Settings__c.sObjectType, 'portalsettings');
		List<sObject> ETS = Test.loadData(Email_Triggersends__c.sObjectType, 'ET_Triggersends');

		Account A1 = new Account();
		A1.Name = 'Generic Consumer Account';
		A1.shippingcountry = 'Japan';
		A1.shippingstreet = 'Test Street';
		A1.shippingcity = 'Test city';
		A1.shippingpostalcode = 'TEST20017';
		insert A1;

		Account A = new Account();
		A.Name = 'NextGen SSO 2';
		A.OutletNumber__c = '000657812';
		A.shippingcountry = 'Japan';
		A.shippingstreet = 'Test Street';
		A.shippingcity = 'Test city';
		A.shippingpostalcode = 'TEST20017';
		A.Email__c = 'a@b.c';
		A.ownerid = userinfo.getuserid();
		insert A;

		Portal_Settings__c GA = new Portal_Settings__c();
		GA.Name = 'GenericAccount Id';
		GA.Value__c = A1.Id;
		insert GA;

		Role__c RL = new Role__c();
		RL.Name = '医療機器販売責任者';
		RL.Approval_Required__c = false;
		RL.Country__c = 'Japan';
		RL.Type__c = 'Manager';
		insert RL;

		JJVC_Contact_Recordtype__mdt rectype = [select MasterLabel from JJVC_Contact_Recordtype__mdt where DeveloperName = 'HCP_Contacts_RecordtypeId'];
		Id HCPRecordTypeId = rectype.MasterLabel;

		Contact cc = new Contact();
		cc.FirstName = 'Acuvue';
		cc.LastName = 'Pro user';
		cc.Salutation = 'Mr.';
		cc.Email = 'Protestuser+acuvue@gmail.com';
		cc.School_Name__c = 'Test';
		cc.Graduation_Year__c = '2014';
		cc.Degree__c = 'Test';
		cc.BirthDate = date.today();
		cc.AccountId = A.iD;
		cc.RecordTypeId = HCPRecordTypeId;
		cc.User_Role__c = RL.Id;
		insert cc;

		User u = new User();
		u.Title = 'Mr.';
		u.FirstName = 'Acuvue';
		u.LastName = 'Pro user';
		u.Email = 'Protestuser+acuvue1@gmail.com';
		u.UserName = 'Protestuser+acuvue1@gmail.com';
		u.Occupation__c = RL.Name;
		u.Secret_Question__c = 'birth_month';
		u.SecretAnswerSalt__c = RegistrationService.generateSalt();
		Blob hash = Crypto.generateDigest('SHA-512', Blob.valueOf('09/09/1992' + u.SecretAnswerSalt__c));
		u.Secret_Answer__c = EncodingUtil.base64Encode(hash);
		u.Communication_Agreement__c = true;
		u.Localesidkey = 'ja_JP';
		u.Languagelocalekey = 'ja';
		u.EmailEncodingKey = 'ISO-8859-1';
		u.Alias = (u.LastName.length() > 7) ? u.LastName.substring(0, 7) : u.LastName;
		u.TimeZoneSidKey = 'America/New_York';
		u.CommunityNickname = B2B_Utils.generateGUID();
		u.NPINumber__c = 'TESTPRO01235';
		u.ContactId = cc.Id;
		u.ProfileId = Portal_Settings__c.getValues('ProfileId').Value__c;
		u.Unique_User_Id__c = 'Test1';
		insert u;

		AccountContactRole__c acr = [SELECT ID,Approval_Status__c FROM AccountContactRole__c WHERE contact__c = :cc.id];
		acr.Approval_Status__c = 'Accepted';
		update acr;

		return u;
	}
}