@RestResource(urlmapping='/apex/Profile/*')
global without sharing class MyAccountProfile {

 global static final String DATA_NOTAVAILABLE ='ACE0001';
 global static final String REQURIED_FIELDS   ='ACE0002';
 global static final String ERROR_TYPE        = 'updateProfile';
    
 @HTTPPOST
 global static String doPost()
 {
    RestRequest req  = RestContext.request;
    RestResponse res = RestContext.response;
    Blob body=req.requestBody;
    Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(body.toString());
    
    String Email = String.ValueOf(m.get('Email'));
    String Country = String.ValueOf(m.get('Country'));
    String EtrailGender =String.ValueOf(m.get('EtrailGender'));
    String EtrialFirstName =String.ValueOf(m.get('EtrialFirstName'));
    String EtrialLastName =String.ValueOf(m.get('EtrialLastName'));
    String EtrialPhone =String.ValueOf(m.get('EtrialPhone'));
    String AcuAddress =String.ValueOf(m.get('AcuAddress'));
    String AcuBirthDate = String.ValueOf(m.get('AcuBirthDate'));
    String AcuEnewsletter =String.ValueOf(m.get('AcuEnewsletter'));
    String AcuQuestion =String.ValueOf(m.get('AcuQuestion'));
    
    return updateProfile(Email,Country,EtrailGender,EtrialFirstName,EtrialLastName,EtrialPhone,AcuAddress,AcuBirthDate,AcuEnewsletter,AcuQuestion);
 }
 
 global static String updateProfile(String Email,String Country,String EtrialGender,String EtrialFirstName,String EtrialLastName,String EtrialPhone,String AcuAddress,String AcuBirthDate,String AcuEnewsletter,String AcuQuestion)
 {
    try{
        
        if(Email =='' || Country =='')
        {
            throw new CustomException(CustomException.getException(REQURIED_FIELDS,ERROR_TYPE));
        }
        List<Etrial__c> etlist = [SELECT Id,
                                        Account__c,
                                        etrial_email__c,
                                        etrial_first_name__c,
                                        etrial_gender__c,
                                        etrial_last_name__c,
                                        Country__c,
                                        Booking_date__c,
                                        acu_address__c,
                                        acu_birthdate__c,
                                        acu_enewsletter__c,
                                        acu_question__c FROM Etrial__c WHERE etrial_email__c=:Email AND Country__c=:Country];
        
        List<Etrial__c> etlistUpdate=new List<Etrial__c>();
        
        if(etlist.size()>0 && !etlist.isEmpty())
        {
          for(Etrial__c et:etlist)
          {
            if(EtrialGender!=null && EtrialGender!=''){
                  et.etrial_gender__c =EtrialGender;
              }
              if(EtrialFirstName!=null && EtrialFirstName!=''){
                  et.etrial_first_name__c =EtrialFirstName;
              }
              if(EtrialLastName!=null && EtrialLastName!=''){
                  et.etrial_last_name__c =EtrialLastName;
              }
              if(EtrialPhone!=null && EtrialPhone!=''){
                  et.etrial_phone__c =EtrialPhone;
              }
              if(AcuAddress!=null && AcuAddress!=''){
                  et.acu_address__c =AcuAddress;
              }
              if(AcuBirthDate!=null && AcuBirthDate!=''){
                System.debug('Splittddate###'+etlist[0].acu_birthdate__c);
                String[] strDate1 = AcuBirthDate.split('/');
                System.debug('Formatted:'+strDate1[2]+'-'+strDate1[1]+'-'+strDate1[0]);
                String stdate1  = strDate1[2]+'-'+strDate1[1]+'-'+strDate1[0];
                System.debug('Splittddate###'+stdate1);
                Date dtime =Date.valueOf(stdate1);
                System.debug('Split@@@@'+dtime);
                et.acu_birthdate__c = dtime ;
              }
              if(AcuAddress!=null){
                  et.acu_enewsletter__c =Boolean.ValueOf(AcuEnewsletter);
              }
              if(AcuQuestion!=null && AcuQuestion!=''){
                  et.acu_question__c =AcuQuestion;
              }
            etlistUpdate.add(et);
          }
            update etlistUpdate;   
            
            List<EtrailDTO> lstEtrailDTO=new List<EtrailDTO>();
            for(Etrial__c et:etlistUpdate){
              EtrailDTO etobj=new EtrailDTO(et.etrial_email__c,et.Country__c,et.etrial_gender__c,et.etrial_first_name__c,et.etrial_last_name__c,et.etrial_phone__c,et.acu_address__c,String.valueOf(et.acu_birthdate__c),et.acu_enewsletter__c,et.acu_question__c);
              lstEtrailDTO.add(etobj);
            }
            
            EtrailListDTO Etrailobj=new EtrailListDTO('Success',lstEtrailDTO);
            return json.serialize(Etrailobj);
        }else{
            throw new CustomException(CustomException.getException(DATA_NOTAVAILABLE,ERROR_TYPE));
        }
        
    }catch(Exception e)
    {
        return e.getMessage();
    }
 }
 
 
 public class EtrailDTO
 {
    public String Email {get;set;}
    public String Country {get;set;}
    public String EtrialGender{get;set;}
    public String EtrialFirstName{get;set;}
    public String EtrialLastName{get;set;}
    public String EtrialPhone{get;set;}
    public String AcuAddress{get;set;}
    public String AcuBirthDate {get;set;}
    public Boolean AcuEnewsletter {get;set;}
    public String AcuQuestion {get;set;}
    
    public EtrailDTO(String Email,String Country,String EtrialGender,String EtrialFirstName,String EtrialLastName,String EtrialPhone,String AcuAddress,String AcuBirthDate,Boolean AcuEnewsletter,String AcuQuestion){
        
        this.Email= Email;
        this.Country= Country;
        this.EtrialGender= EtrialGender;
        this.EtrialFirstName= EtrialFirstName;
        this.EtrialLastName= EtrialLastName;
        this.EtrialPhone= EtrialPhone;
        this.AcuAddress= AcuAddress;
        this.AcuBirthDate= AcuBirthDate;
        this.AcuEnewsletter= AcuEnewsletter;
        this.AcuQuestion= AcuQuestion;
        

    }
 }
 
 public class EtrailListDTO
 {
   public String Status {get;set;}
   public List<EtrailDTO> Etrials {get;set;}
   public EtrailListDTO(String Status,List<EtrailDTO> Etrials){
     this.Status= Status;
     this.Etrials= Etrials;
   }
 }
}