/**
* File Name: MyAccountProfileService
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  18-Oct-2018
* Description : Retrieve & update JJVPRO user information of Personal details and Practice information. 
               last name and password and practice like adding & removing.
* Copyright (c) $2018 Johnson & Johnson
*/
@RestResource(urlMapping='/apex/myaccount/v1/BusinessProfileService/*')
global without sharing class MyAccountProfileService
{
	global static final String ERROR_TYPE = 'UpdateProfile';
	global static final String GENERIC_ERROR = 'GEN0001';
	global static final String GENERIC_ERROR_TYPE = 'Generic';
	/**
    * @description  Retrieve logged in user information and return in JSON format
    */
	@HttpGet
	global static String doGet()
	{
		RestRequest req = RestContext.request;
		try
		{
			String UserId = req.requestURI.substring(req.requestURI.lastIndexOf('/') + 1);
			return getBusinessProfile(UserId);
		}
		catch (Exception e)
		{
			return B2B_Utils.getException(GENERIC_ERROR, GENERIC_ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('GEN0001').Error_Description__c));
		}
	}
	/**
	* @description update user personal & practice information
	*/
	@HttpPost
	global static String doPost()
	{
		RestRequest req = RestContext.request;
		Blob body = req.requestBody;
		UpdateProfile profiles = (UpdateProfile) System.JSON.deserialize(body.toString(), UpdateProfile.class);
		return updateBusinessProfile(profiles);
	}
	/**
	* @description update user password
	*/
	@HttpPut
	global static String manageUser()
	{
		RestRequest req = RestContext.request;
		Blob body = req.requestBody;
		manageUser userDetails = (manageUser) System.JSON.deserialize(body.toString(), manageUser.class);
		if (userDetails.action == 'setPassword')
		{
			return setorresetpassword(userDetails);
		}
		else if (userDetails.action == 'resetPassword')
		{
			return setorresetpassword(userDetails);
		}
		return null;
	}
	/**
	* @description  Helper method to retreive personal information
	*/
	global static String getBusinessProfile(String UserId)
	{
		try
		{
			List<Practices> pharmacy = new List<Practices>();
			Map<id, AccountContactRole__c> accountContactRoleMap = new Map<id, AccountContactRole__c>();
			Map<Id, Boolean> webLocationVisibleMap = new Map<Id, Boolean>();
			List<User> users =
			[
					SELECT Id,
							Title,
							FirstName,
							LastName,
							Email,
							Username,
							Country,
							MobilePhone,
							Fax,
							Phone,
							ContactID,
							AccountID,
							Secret_Answer__c,
							Secret_Question__c,
							SecretAnswerSalt__c,
							LanguageLocaleKey,
							LocaleSidKey,
							IsActive,
							Occupation__c,
							NPINumber__c,
							Communication_Agreement__c,
							LMS_Id__c,
							Migration_Completed__c,
							Registration_Type__c
					FROM User
					WHERE Id = :UserId
			];
			if (!users.isEmpty())
			{
				User user = users[0];
				Contact con =
				[
						SELECT Id,
								Salutation,
								FirstName,
								LastName,
								Email,
								AccountId,
								BirthDate,
								School_Name__c,
								Graduation_Year__c,
								Degree__c,
								Account.Name,
						(
								SELECT Account__c,
										Contact__c,
										Account_Ownership_Flag__c,
										Functions__c,
										Approval_Status__c,
										Account__r.Name,
										Account__r.Ownerid,
										Account__r.Owner.Name,
										Account__r.Owner.Email,
										Account__r.Phone,
										Account__r.OutletNumber__c,
										Account__r.BillingPostalCode,
										Account__r.BillingStreet,
										Account__r.BillingCity,
										Account__r.BillingState,
										Account__r.BillingCountry,
										Primary__c,
										Account__r.ShippingStreet,
										Account__r.ShippingCity,
										Account__r.ShippingState,
										Account__r.ShippingPostalcode,
										Account__r.ShippingCountry,
										Account__r.owner.Userrole.name,
										Account__r.owner.Phone,
										Account__r.owner.mobilephone,
										Account__r.owner.FullPhotoUrl
								FROM AccountContactRoles__r
						)
						FROM Contact
						WHERE Id = :user.ContactId
				];

				if (con.AccountContactRoles__r.size() > 0)
				{
					for (AccountContactRole__c accountContactRole : con.AccountContactRoles__r)
					{
						accountContactRoleMap.put(accountContactRole.Account__c, accountContactRole);
					}
					List<Web_Location__c> ecpLocation =
					[
							SELECT Id,
									Account__c,
									Web_Locator_Visible__c
							FROM Web_Location__c
							WHERE Account__c IN:accountContactRoleMap.Keyset()
					];
					for (Web_Location__c webloc : ecpLocation)
					{
						webLocationVisibleMap.put(webloc.Account__c, webloc.Web_Locator_Visible__c);
					}
					for (AccountContactRole__c accountContactRole : con.AccountContactRoles__r)
					{
						pharmacy.add(prepPracticeDTO(accountContactRole, webLocationVisibleMap, accountContactRoleMap));
					}
					return json.serialize(prepProfileDTO(user, con, pharmacy));
				}
				else if (con.AccountId == Portal_Settings__c.getValues('GenericAccount Id').Value__c)
				{
					Practices pharma = new Practices();
					pharma.Name = con.Account.Name;
					pharma.Id = con.AccountId;
					pharma.SAPAccountNumber = '';
					pharma.Phone = '';
					pharma.ApprovalStatus = '';
					pharmacy.add(pharma);
					return json.serialize(prepProfileDTO(user, con, pharmacy));
				}
				else
				{
					throw new CustomException(B2B_Utils.getException(GENERIC_ERROR, GENERIC_ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('GEN0001').Error_Description__c)));
				}
			}
		}
		catch (Exception e)
		{
			return e.getMessage();
		}
		return null;
	}
	/**
	* @description  Helper method to prepare profile DTO class with given inputs
	*/
	private static ProfileDetails prepProfileDTO(User user, Contact con, List<Practices> pharmacy)
	{
		ProfileDetails prDetails = new ProfileDetails();
		prdetails.Id = user.Id;
		prdetails.LMSId = user.LMS_Id__c;
		prdetails.Salutation = user.Title;
		prdetails.FirstName = user.FirstName;
		prdetails.LastName = user.LastName;
		prdetails.EmailAddress = user.Email;
		prdetails.Username = user.UserName;
		prdetails.Role = user.Occupation__c;
		prdetails.CommunicationAggrement = user.Communication_Agreement__c;
		if (con.BirthDate != null)
		{
			prdetails.DateofBirth = con.BirthDate.format();
		}
		prdetails.PrimaryPracticeID = con.AccountId;
		prdetails.SecretQuestion = user.Secret_Question__c;
		prdetails.SecretAnswer = user.Secret_Answer__c;
		prdetails.SecretAnswerSalt = user.SecretAnswerSalt__c;
		prdetails.UserLocale = user.LocaleSidkey;
		prdetails.UserLanguage = user.LanguageLocalekey;
		prdetails.NPINumber = user.NPINumber__c;
		prdetails.Practices = pharmacy;
		prdetails.SchoolName = con.School_Name__c;
		prdetails.GraduationYear = con.Graduation_Year__c;
		prdetails.Degree = con.Degree__c;
		prdetails.MigrationCompleted = user.Migration_Completed__c;
		prdetails.RegType = user.Registration_Type__c;
		return prdetails;
	}
	/**
	* @description  Helper method to prepare Practice DTO class with given inputs
	*/
	private static Practices prepPracticeDTO(AccountContactRole__c acr, Map<Id, Boolean> webLocationVisibleMap, Map<id, AccountContactRole__c> accountContactRoleMap)
	{
		Practices pharma = new Practices();
		pharma.Name = acr.Account__r.Name;
		pharma.Id = String.ValueOf(acr.Account__c);
		pharma.SAPAccountNumber = acr.Account__r.OutletNumber__c;
		pharma.Phone = acr.Account__r.Phone;
		pharma.isPrimary = acr.Primary__c;
		pharma.AccountOwnership = acr.Account_Ownership_Flag__c;
		pharma.ApprovalStatus = acr.Approval_Status__c;
		pharma.Address = PrepAddressDTO(acr);
		pharma.Functions = prepFuctionsDTO(accountContactRoleMap.get(acr.Account__c).Functions__c);
		pharma.SalesRepDetails = prepSalesRepDTO(acr);
		if (webLocationVisibleMap.get(acr.Account__c) == null)
		{
			pharma.EnrollFindAnEyeDoctor = false;
		}
		else
		{
			pharma.EnrollFindAnEyeDoctor = webLocationVisibleMap.get(acr.Account__c);
		}
		return pharma;
	}
	/**
	* @description  Helper method to prepare salesrep DTO as return of list with given inputs
	*/
	private static List<SalesRepDetails> prepSalesRepDTO(AccountContactRole__c acr)
	{
		List<SalesRepDetails> srepdetails = new List<SalesRepDetails>();
		SalesRepDetails sr = new SalesRepDetails();
		sr.Name = acr.Account__r.owner.name;
		sr.EmailAddress = acr.Account__r.owner.email;
		sr.RoleName = acr.Account__r.owner.Userrole.name;
		sr.Phone = acr.Account__r.owner.Phone;
		sr.Mobile = acr.Account__r.owner.mobilephone;
		if (Test.IsrunningTest())
		{
			sr.ProfilePhoto = 'Test';
		}
		else
		{
			sr.ProfilePhoto = getPracticeOwnerPhoto(acr.Account__r.ownerid);
		}
		srepdetails.add(sr);
		return srepdetails;
	}
	/**
	* @description  Helper method to prepare Practice address DTO class with given inputs
	*/
	private static List<Address> PrepAddressDTO(AccountContactRole__c acr)
	{
		List<Address> PhAddresses = new List<Address>();
		Address PhBillingAddress = new Address();
		PhBillingAddress.Street = acr.Account__r.BillingStreet;
		PhBillingAddress.City = acr.Account__r.BillingCity;
		PhBillingAddress.State = acr.Account__r.BillingState;
		PhBillingAddress.PostalCode = acr.Account__r.BillingPostalCode;
		PhBillingAddress.Country = acr.Account__r.BillingCountry;
		PhBillingAddress.Type = 'Billing';
		PhAddresses.add(PhBillingAddress);

		Address PhShippingAddress = new Address();
		PhShippingAddress.Street = acr.Account__r.ShippingStreet;
		PhShippingAddress.City = acr.Account__r.ShippingCity;
		PhShippingAddress.State = acr.Account__r.ShippingState;
		PhShippingAddress.PostalCode = acr.Account__r.ShippingPostalCode;
		PhShippingAddress.Country = acr.Account__r.ShippingCountry;
		PhShippingAddress.Type = 'Shipping';
		PhAddresses.add(PhShippingAddress);
		return PhAddresses;
	}
	/**
	* @description  Helper method to prepare fuctions DTO for all practice input provided
	*/
	private static List<Functions> prepFuctionsDTO(String functions)
	{
		List<Functions> acrfunc = new List<Functions>();
		if (functions != null)
		{
			String[] flist = functions.split(';');
			for (String fn : flist)
			{
				Functions fns = new Functions();
				fns.Name = fn;
				acrfunc.add(fns);
			}
		}
		if (acrfunc != null)
		{
			return acrfunc;
		}
		return acrfunc;
	}
	/**
	* @description  Helper method to update user information
	*/
	public static String updateBusinessProfile(UpdateProfile profiles)
	{
		savepoint changes = Database.setSavepoint();
		try
		{
			List<AccountContactRole__c> accountContactRoleInserList = new List<AccountContactRole__c>();
			List<AccountContactRole__c> accountContactRoleDeleteList = new List<AccountContactRole__c>();
			List<AccountContactRole__c> accountContactRoleUpdateList = new List<AccountContactRole__c>();
			List<AccountContactRole__c> accountOwnerList = new List<AccountContactRole__c>();
			List<AccountContactRole__c> accountContactRolewithPendingStatusList = new List<AccountContactRole__c>();
			String userFirstName;
			String userLastName;
			String userOccupation;
			string userEmail;
			String userLanguage;
			Set <Id> accountIdSet = new Set<Id>();
			Map<Id, Id> accountContactRoleAccountMap = new Map<Id, Id>();
			Map<ID, ID> existingManagerIdMap = new Map<ID, ID>();
			Map<String, AccountContactRole__c> accountContactRoleContactMap = new Map<String, AccountContactRole__c>();
			Map<ID, ID> ownerContactIdMap = new Map<ID, ID>();
			Map<ID, User> ownerUserIdMap = new Map<ID, User>();
			JJVC_Contact_Recordtype__mdt rectype = [SELECT MasterLabel FROM JJVC_Contact_Recordtype__mdt WHERE DeveloperName = 'HCP_Contacts_RecordtypeId'];
			ID HCPRecordTypeId = rectype.MasterLabel;
			List<User> users =
			[
					SELECT id,
							Title,
							FirstName,
							LastName,
							Email,
							Username,
							Country,
							MobilePhone,
							Fax,
							Phone,
							ContactID,
							AccountID,
							Secret_Answer__c,
							Secret_Question__c,
							LanguageLocaleKey,
							LocaleSidKey,
							IsActive,
							Occupation__c,
							NPINumber__c,
							Communication_Agreement__c,
							LMS_Id__c,
							Migration_Completed__c
					FROM User
					WHERE Id = :profiles.UserId
			];

			if (!users.isEmpty())
			{
				userFirstName = users[0].FirstName;
				userLastName = users[0].LastName;
				userOccupation = users[0].Occupation__c;
				userEmail = users[0].Email;
				userLanguage = users[0].LanguageLocaleKey;
				User user = users[0];

				Contact con =
				[
						SELECT Id,
								Salutation,
								FirstName,
								LastName,
								Email,
								AccountId,
								Account.Name,
								BirthDate,
								is_Test_User__c,
								user_Role__c,
								user_Role__r.Type__c,
								user_Role__r.name,
						(
								SELECT Id,
										Functions__c,
										Account__c,
										Contact__c,
										Account_Ownership_Flag__c,
										Approval_Status__c,
										Account__r.Name,
										Account__r.Ownerid,
										Account__r.Owner.Name,
										Account__r.Owner.Email,
										Account__r.Phone,
										Account__r.BillingPostalCode,
										Account__r.BillingStreet,
										Account__r.BillingCity,
										Account__r.BillingState,
										Account__r.BillingCountry,
										Primary__c
								FROM AccountContactRoles__r
						)
						FROM Contact
						WHERE Id = :user.ContactId
				];
				validateUserDetails(profiles);
				if (profiles.FirstName != '' && profiles.FirstName != null)
				{
					user.FirstName = profiles.FirstName;
					con.FirstName = profiles.FirstName;
				}
				if (profiles.LastName != '' && profiles.LastName != null)
				{
					user.LastName = profiles.LastName;
					con.LastName = profiles.LastName;
				}
				if ((profiles.Salutation == '' || profiles.Salutation == ' ') && profiles.Salutation != null)
				{
					user.Title = '';
					con.Salutation = '';
				}
				else if (profiles.Salutation != '' && profiles.Salutation != null)
				{
					user.Title = profiles.Salutation;
					con.Salutation = profiles.Salutation;
				}

				for (AccountContactRole__c acr : con.AccountContactRoles__r)
				{
					accountContactRoleContactMap.put(String.ValueOf(acr.Account__c) + con.Id, acr);
					accountContactRoleAccountMap.put(acr.Account__c, acr.Account__c);
					if (acr.Approval_status__c == 'Pending Invitation')
					{
						accountContactRolewithPendingStatusList.add(acr);
					}
				}
				// update email and username when user changing the emailaddress
				if ((user.Username != profiles.Username) && ((profiles.Username != '' && profiles.Username != null) || (profiles.EmailAddress != '' && profiles.EmailAddress != null)))
				{
					List<User> userList = [SELECT Id, Username, Email FROM User WHERE Username = :profiles.Username];
					if (userList.Isempty())
					{
						con.Email = profiles.Username;
						B2B_Utils.updateUserDetails(profiles.Username, Id.ValueOf(profiles.UserId));
					}
					else
					{
						throw new customexception(B2B_Utils.getException('PC0001', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0001').Error_Description__c)));
					}
				}
				List<Role__c> contactRole = new List<Role__c>();
				if (profiles.Role != '' && profiles.Role != null && con.user_Role__r.name != profiles.Role)
				{
					contactRole = [SELECT Id, Name, Type__c FROM Role__c WHERE Name = :profiles.Role];
					if (contactRole.isempty())
					{
						throw new customexception(B2B_Utils.getException('PC0016', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0016').Error_Description__c)));
					}
					else
					{
						if (contactRole[0].type__c == 'Manager')
						{
							Map<ID, ID> managerAccountContactRoleMap = new Map<ID, ID>();
							List<AccountContactRole__c> managerAccountContactList = [SELECT Id, Account__c, Contact__c, Account_Ownership_Flag__c FROM AccountContactRole__c WHERE Account__c = :accountContactRoleAccountMap.keyset() AND Account_Ownership_Flag__c = true];
							for (AccountContactRole__c managerAccountContact : managerAccountContactList)
							{
								managerAccountContactRoleMap.put(managerAccountContact.Account__c, managerAccountContact.Account__c);
							}
							for (AccountContactRole__c managerAccountContact : con.AccountContactRoles__r)
							{
								if (managerAccountContactRoleMap.containskey(managerAccountContact.Account__c))
								{
									throw new customexception(B2B_Utils.getException('PC0029', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0029').Error_Description__c)));
								}
								else
								{
									managerAccountContact.Functions__c = 'Ordering;Rewards;Administrator;ECPLocator';
									accountOwnerList.add(managerAccountContact);
								}
							}
						}
					}
					con.user_Role__c = contactRole[0].Id;
					user.Occupation__c = contactRole[0].Name;
				}
				if (profiles.PrimaryPracticeID != '' && profiles.PrimaryPracticeID != null)
				{
					con.AccountId = Id.Valueof(profiles.PrimaryPracticeID);
				}
				if (profiles.DateofBirth != '' && profiles.DateofBirth != null)
				{
					con.BirthDate = Date.Parse(profiles.DateofBirth);
				}
				if (profiles.IsTestRecord != null)
				{
					con.is_Test_User__c = profiles.IsTestRecord;
				}
				if (profiles.SecretQuestion != '' && profiles.SecretQuestion != null)
				{
					user.Secret_Question__c = profiles.SecretQuestion;
				}
				if (profiles.SecretAnswer != '' && profiles.SecretAnswer != null)
				{
					user.SecretAnswerSalt__c = MyAccountProfileService.generateSalt();
					Blob hash = Crypto.generateDigest('SHA-512', Blob.valueOf(profiles.SecretAnswer + user.SecretAnswerSalt__c));
					user.Secret_Answer__c = EncodingUtil.base64Encode(hash);
				}
				if (profiles.UserLanguage != '' && profiles.UserLanguage != null)
				{
					user.LanguageLocaleKey = profiles.UserLanguage;
				}
				if (profiles.UserLocale != '' && profiles.UserLocale != null)
				{
					user.LocaleSidKey = profiles.UserLocale;
				}
				if (profiles.CommunicationAggrement != null)
				{
					user.Communication_Agreement__c = profiles.CommunicationAggrement;
				}
				if (profiles.NPINumber != '' && profiles.NPINumber != null)
				{
					user.NPINumber__c = profiles.NPINumber;
				}
				if (profiles.SchoolName != '' && profiles.SchoolName != null)
				{
					con.School_Name__c = profiles.SchoolName;
				}
				if (profiles.GraduationYear != '' && profiles.GraduationYear != null)
				{
					con.Graduation_Year__c = profiles.GraduationYear;
				}
				if (profiles.Degree != '' && profiles.Degree != null)
				{
					con.Degree__c = profiles.Degree;
				}
				if (profiles.MigrationCompleted != null)
				{
					user.Migration_Completed__c = profiles.MigrationCompleted;
				}
				for (Practices pc : profiles.Practices)
				{
					Id ids = pc.Id;
					accountIdSet.add(ids);
				}
				if (profiles.MigrationCompleted != null && profiles.MigrationCompleted)
				{
					for (AccountContactRole__c pendingAccountContact : accountContactRolewithPendingStatusList)
					{
						pendingAccountContact.Approval_status__c = 'Accepted';
						accountContactRoleUpdateList.add(pendingAccountContact);
					}
				}
				Map<Id, Account> mpAccounts = New Map<Id, Account>([
						SELECT Id,
								Name,
								Email__c,
						(
								SELECT Id,
										Contact__c,
										Primary__c,
										Account_Ownership_Flag__c,
										Approval_Status__c,
										Functions__c
								FROM AccountContactRoles__r
								WHERE Contact__r.RecordtypeID = :HCPRecordTypeId
								order by createddate asc
						)
						FROM Account
						WHERE Id IN:accountIdSet
				]);
				List<AccountContactRole__c> managerAccountContactRole =
				[
						SELECT Id,
								Account__c,
								Contact__c,
								Account_Ownership_Flag__c
						FROM AccountContactRole__c
						WHERE Account__c = :accountIdSet AND
						Account_Ownership_Flag__c = true
				];

				for (AccountContactRole__c accountContactRole : managerAccountContactRole)
				{
					existingManagerIdMap.put(accountContactRole.Account__c, accountContactRole.Account__c);
				}
				for (Practices pc : profiles.Practices)
				{
					if (pc.action == 'add')
					{
						if (!accountContactRoleContactMap.containsKey(pc.Id + con.Id))
						{
							// Check if contact is already associated to this practice. if yes, no further operation performed else add new account contact role record.
							if (con.user_Role__r.type__c == 'Manager')
							{
								if (existingManagerIdMap.containskey(pc.id))
								{
									throw new customexception(B2B_Utils.getException('PC0020', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0020').Error_Description__c)));
								}
								else
								{
									AccountContactRole__c SecondaryAccount = new AccountContactRole__c();
									SecondaryAccount.Account__c = pc.Id;
									SecondaryAccount.Contact__c = Con.id;
									SecondaryAccount.Approval_Status__c = 'Accepted';
									SecondaryAccount.Functions__c = 'Ordering;Rewards;Administrator;ECPLocator';
									accountContactRoleInserList.add(SecondaryAccount);
								}
							}
							else
							{
								if (existingManagerIdMap.containskey(pc.id))
								{
									AccountContactRole__c SecondaryAccount = new AccountContactRole__c();
									SecondaryAccount.Account__c = pc.Id;
									SecondaryAccount.Contact__c = Con.id;
									SecondaryAccount.Approval_Status__c = 'Pending Approval';
									accountContactRoleInserList.add(SecondaryAccount);
								}
								else
								{
									throw new customexception(B2B_Utils.getException('PC0012', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0012').Error_Description__c)));
								}
							}
						}
						else if (accountContactRoleContactMap.containsKey(pc.Id + con.Id))
						{
							AccountContactRole__c accountContactRole = accountContactRoleContactMap.get(pc.Id + con.Id);
							if (accountContactRole.Approval_Status__c == 'Rejected')
							{
								if (existingManagerIdMap.containskey(accountContactRole.Account__c))
								{
									accountContactRole.Approval_Status__c = 'Pending Approval';
									accountContactRoleUpdateList.add(accountContactRole);
								}
								else
								{
									throw new customexception(B2B_Utils.getException('PC0012', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0012').Error_Description__c)));
								}
							}
							for (AccountContactRole__c approver : managerAccountContactRole)
							{
								ownerContactIdMap.put(approver.Account__c, approver.Contact__c);
							}
						}
					}
					else if (pc.Action == 'remove' && accountContactRoleContactMap.containsKey(pc.Id + con.Id))
					{
						accountContactRoleDeleteList.add(accountContactRoleContactMap.get(pc.Id + con.Id));
					}
				}
				List<User> approvalUser = [SELECT id, firstname, contactid, email, localesidkey FROM User WHERE contactid IN :ownerContactIdMap.values() AND IsActive = true];
				for (User us : approvalUser)
				{
					ownerUserIdMap.put(us.contactid, us);
				}
				if (con.AccountId != Portal_Settings__c.getValues('GenericAccount Id').Value__c)
				{
					Account accaddress = [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry FROM Account WHERE Id = :con.AccountId];
					if (accaddress.ShippingStreet != null && accaddress.ShippingStreet != '')
					{
						user.Street = accaddress.ShippingStreet;
					}
					if (accaddress.ShippingCity != null && accaddress.ShippingCity != '')
					{
						user.City = accaddress.ShippingCity;
					}
					if (accaddress.ShippingState != null && accaddress.ShippingState != '')
					{
						user.State = accaddress.ShippingState;
					}
					if (accaddress.ShippingPostalCode != null && accaddress.ShippingPostalCode != '')
					{
						user.Postalcode = accaddress.ShippingPostalCode;
					}
					if (accaddress.ShippingCountry != null && accaddress.ShippingCountry != '')
					{
						user.Country = accaddress.ShippingCountry;
					}
				}
				if ((profiles.FirstName != null && profiles.FirstName != '') || (profiles.LastName != null && profiles.LastName != '') || (profiles.Role != null && profiles.Role != '') || (profiles.EmailAddress != null && profiles.EmailAddress != '') || (profiles.UserLanguage != ''))
				{

					if ((user.LMS_Id__c != null) && ((userFirstName != profiles.FirstName) || (userLastName != profiles.LastName) || (userOccupation != profiles.Role) || (userEmail != Profiles.EmailAddress) || (userLanguage != Profiles.UserLanguage)))
					{
						LmsUtils.updateLMSProfiledetails(user.LMS_Id__c, profiles.FirstName, profiles.LastName, profiles.Role, Profiles.EmailAddress, Profiles.EmailAddress, user.localesidkey);
					}
				}
				if (!accountContactRoleInserList.isempty())
				{
					insert accountContactRoleInserList;
				}
				if (!accountContactRoleDeleteList.isempty())
				{
					delete accountContactRoleDeleteList;
				}
				if (!accountContactRoleDeleteList.isempty())
				{
					Email_Triggersends__c updateprofilekey = Email_Triggersends__c.getvalues(user.LocaleSidKey);
					for (AccountContactRole__c delacr : accountContactRoleDeleteList)
					{
						ET_EmailAdministration.sendemail(user.email, updateprofilekey.secondary_practice_removed__c, user.Firstname, delacr.Account__r.Name, '');
					}
				}
				if (!accountContactRoleUpdateList.isempty())
				{
					update accountContactRoleUpdateList;
					for (AccountContactRole__c appacr : accountContactRoleUpdateList)
					{
						if (appacr.Approval_Status__c == 'Pending Approval')
						{
							User userId = ownerUserIdMap.get(ownerContactIdMap.get(appacr.Account__c));
							Email_Triggersends__c approvalEmailKey = Email_Triggersends__c.getvalues(userId.LocaleSidKey);
							MyAccountProfileService.UpdateApprovalInstance(appacr.Id, appacr.Contact__c, userId.ID);
							ET_EmailAdministration.sendemail(userId.Email, approvalEmailKey.Practice_link_approval_notification__c, profiles.firstname, '', userId.firstname);       // Send approval notification to owner or firstuser
						}
					}
				}
				if (!accountOwnerList.isempty())
				{
					update accountOwnerList;
				}
				update con;
				Map<ID, AccountContactRole__c> ownerflag = new Map<ID, AccountContactRole__c>([
						SELECT ID,
								Account_Ownership_Flag__c,
								Account__c,
								contact__c,
								primary__c
						FROM AccountContactRole__c
						WHERE ID in:accountContactRoleInserList
				]);
				Email_Triggersends__c updateprofilekey = Email_Triggersends__c.getvalues(user.LocaleSidKey);
				for (AccountContactRole__c accountContactRole : accountContactRoleInserList)
				{
					if (accountContactRole.Approval_Status__c == 'Accepted')
					{
						if (accountContactRole.primary__c)
						{
							if (ownerflag.get(accountContactRole.id).Account_Ownership_Flag__c)
							{
								ET_EmailAdministration.sendemail(user.Email, updateprofilekey.Reg_welcome_practice_linked__c, user.firstname, mpAccounts.get(accountContactRole.Account__c).Name, '');
							}
							else
							{
								ET_EmailAdministration.sendemail(user.Email, updateprofilekey.Reg_welcome_practice_linked__c, user.firstname, mpAccounts.get(accountContactRole.Account__c).Name, '');
								ET_EmailAdministration.sendemail(mpAccounts.get(accountContactRole.Account__c).Email__c, updateprofilekey.First_user_reg_notify_owner__c, user.firstname, '', '');
							}
						}
						else
						{
							if (ownerflag.get(accountContactRole.id).Account_Ownership_Flag__c)
							{
								ET_EmailAdministration.sendemail(user.Email, updateprofilekey.Secondary_practice_welcome_email__c, user.firstname, mpAccounts.get(accountContactRole.Account__c).Name, '');
							}
							else
							{
								ET_EmailAdministration.sendemail(user.Email, updateprofilekey.Secondary_practice_welcome_email__c, user.firstname, mpAccounts.get(accountContactRole.Account__c).Name, '');
								ET_EmailAdministration.sendemail(mpAccounts.get(accountContactRole.Account__c).Email__c, updateprofilekey.First_user_reg_notify_owner__c, user.firstname, '', '');
							}
						}
					}
				}
				update user;
				if (profiles.Username != user.username)
				{
					ET_EmailAdministration.sendemail(con.email, updateprofilekey.Email_update__c, user.Firstname, '', '');
				}
				JSONGenerator gen = JSON.createGenerator(true);
				gen.writeStartObject();
				gen.writeObjectField('Status', 'Success');
				gen.writeEndObject();
				return gen.getAsString();
			}

		}
		catch (Exception e)
		{
			Database.rollback(changes);
			if (e.getMessage().contains('Invalid id:'))
			{
				return B2B_Utils.getException('PC0017', ERROR_TYPE, B2B_Custom_Exceptions__c.getValues('PC0017').Error_Description__c);
			}
			else
			{
				return e.getMessage();
			}
		}
		return null;
	}

	/**
	* @description  Helper method to validate firstname,lastname and email
	*/
	private static void validateUserDetails(UpdateProfile profiles)
	{
		if (profiles.FirstName == null || profiles.FirstName == '')
		{
			throw new CustomException(B2B_Utils.getException('PC0005', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0005').Error_Description__c)));
		}
		if (profiles.LastName == null || profiles.LastName == '')
		{
			throw new CustomException(B2B_Utils.getException('PC0006', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0006').Error_Description__c)));
		}
		if (profiles.EmailAddress == null || profiles.EmailAddress == '')
		{
			throw new CustomException(B2B_Utils.getException('PC0007', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0007').Error_Description__c)));
		}
	}
	/**
	* @description  Helper method to to initiate approval process
	*/
	public static void UpdateApprovalInstance(Id acrid, Id conid, Id suserid)
	{
		List<processinstance> pis = [SELECT id FROM processinstance WHERE targetobjectId = :acrid AND status = 'Pending'];
		if (pis.isEmpty())
		{
			Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
			req.setComments('Re-Submitted for approval. Please approve.');
			req.setObjectId(acrid);
			req.setNextApproverIds(new Id[] {Portal_Settings__c.getValues('ApproverID').Value__c});
			Approval.process(req);
		}
	}
	/**
	* @description  Helper method to generate a random text
	*/
	public static String generateSalt()
	{
		Blob b = Crypto.GenerateAESKey(128);
		String h = EncodingUtil.ConvertTohex(b);
		return h.SubString(0, 30);
	}
	/**
	* @description  Helper method to setpassword
	*/
	private static string setorResetPassword(manageUser mu)
	{
		Map<String, Email_Triggersends__c> emailTriggerSendKeyMap = Email_Triggersends__c.getAll();
		try
		{
			String userId = mu.userId;
			String code = mu.newPassword;
			User user = [SELECT Id, Username, Firstname, Lastname, email, localesidkey FROM user WHERE Id = :userId];
			Email_Triggersends__c tid = emailTriggerSendKeyMap.get(user.LocaleSidKey);
			validatePassword(code, user);
			try
			{
				system.setPassword(userid, code);
				ET_EmailAdministration.sendemail(user.Email, tid.Password_Updated__c, user.firstname, '', '');
			}
			catch (Exception e)
			{
				if (e.getMessage().contains('invalid repeated password'))
				{
					return B2B_Utils.getException('PC0004', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0004').Error_Description__c));
				}
				if (e.getMessage().contains('INVALID_NEW_PASSWORD'))
				{
					return B2B_Utils.getException('PC0003', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0003').Error_Description__c));
				}
			}
			JSONGenerator gen = JSON.createGenerator(true);
			gen.writeStartObject();
			gen.writeObjectField('Status', 'Success');
			gen.writeEndObject();
			return gen.getAsString();
		}
		catch (exception e)
		{
			return e.getMessage();
		}
	}
	/**
	* @description  Helper method to validate given new password
	*/
	private static void validatePassword(String Code, User user)
	{
		if (Code == '' || Code == null)
		{
			throw new CustomException(B2B_Utils.getException(GENERIC_ERROR, GENERIC_ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('GEN0001').Error_Description__c)));
		}
		if (B2B_Utils.IsValidPassword(Code))
		{
			throw new CustomException(B2B_Utils.getException('PC0003', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0003').Error_Description__c)));
		}
		if ((user.Username != '' && user.Username != null) || (user.LastName != '' && user.LastName != null) || (user.FirstName != '' && user.FirstName != null))
		{
			String pwuser = user.Username.split('@')[0];
			if (Code.containsIgnoreCase(user.LastName) || Code.containsIgnoreCase(user.FirstName) || Code.containsIgnoreCase(pwuser))
			{
				throw new customexception(B2B_Utils.getException('PC0002', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0002').Error_Description__c)));
			}
		}
	}

	private static String getPracticeOwnerPhoto(string ownerid)
	{
		ConnectApi.Photo ph = ConnectApi.UserProfiles.getPhoto(null, ownerid);
		HttpRequest req = new HttpRequest();
		req.setEndpoint(ph.fullEmailPhotoUrl);
		req.setMethod('GET');
		req.setHeader('Content-Type', 'application/json');
		Http binding = new Http();
		HttpResponse res = binding.send(req);
		return EncodingUtil.base64Encode(res.getBodyAsBlob());
	}

	public class manageUser
	{
		public String userId { get; set; }
		public String action { get; set; }
		public String newPassword { get; set; }
	}

	public class ProfileDetails
	{
		public String Id { get; set; }
		public String LMSId { get; set; }
		public String Salutation { get; set; }
		public String FirstName { get; set; }
		public String LastName { get; set; }
		public String EmailAddress { get; set; }
		public String Username { get; set; }
		public String Role { get; set; }
		public Boolean CommunicationAggrement { get; set; }
		public String PrimaryPracticeID { get; set; }
		public String DateofBirth { get; set; }
		public String SecretQuestion { get; set; }
		public String SecretAnswer { get; set; }
		public String SecretAnswerSalt { get; set; }
		public String UserLocale { get; set; }
		public String UserLanguage { get; set; }
		public String NPINumber { get; set; }
		public List<Practices> Practices { get; set; }
		public String SchoolName { get; set; }
		public String GraduationYear { get; set; }
		public String Degree { get; set; }
		public Boolean MigrationCompleted { get; set; }
		public String RegType { get; set; }
	}
	public class Practices
	{
		public String Name { get; set; }
		public String Id { get; set; }
		public Boolean AccountOwnership { get; set; }
		public String SAPAccountNumber { get; set; }
		public Boolean EnrollFindAnEyeDoctor { get; set; }
		public List<Address> Address { get; set; }
		public String Phone { get; set; }
		public Boolean isPrimary { get; set; }
		public String ApprovalStatus { get; set; }
		public String Action { get; set; }
		public List<SalesRepDetails> SalesRepDetails { get; set; }
		public List<Functions> Functions { get; set; }
	}

	public class Functions
	{
		public String Name { get; set; }
	}

	public class Address
	{
		public String Street { get; set; }
		public String City { get; set; }
		public String State { get; set; }
		public String PostalCode { get; set; }
		public String Country { get; set; }
		public String Type { get; set; }
	}
	public class SalesRepDetails
	{
		public String Name { get; set; }
		public String EmailAddress { get; set; }
		public String RoleName { get; set; }
		public String Phone { get; set; }
		public String Mobile { get; set; }
		public String ProfilePhoto { get; set; }
	}
	public class UpdateProfile
	{
		public String UserId { get; set; }
		public String Salutation { get; set; }
		public String FirstName { get; set; }
		public String LastName { get; set; }
		public String EmailAddress { get; set; }
		public String Username { get; set; }
		public String Role { get; set; }
		public Boolean CommunicationAggrement { get; set; }
		public String PrimaryPracticeID { get; set; }
		public String DateofBirth { get; set; }
		public String SecretQuestion { get; set; }
		public String SecretAnswer { get; set; }
		public String UserLocale { get; set; }
		public String UserLanguage { get; set; }
		public String NPINumber { get; set; }
		public List<Practices> Practices { get; set; }
		public String SchoolName { get; set; }
		public String GraduationYear { get; set; }
		public String Degree { get; set; }
		public Boolean MigrationCompleted { get; set; }
		public Boolean IsTestRecord { get; set; }
	}
}