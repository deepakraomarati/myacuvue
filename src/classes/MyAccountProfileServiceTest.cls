/**
* File Name: MyAccountProfileServiceTest 
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : Test class for MyAccountProfileService class
* Copyright (c) $2018 Johnson & Johnson
*/
@isTest(SeeAlldata=false)
public class MyAccountProfileServiceTest
{
	public static user insertuser()
	{

		List<sObject> B2BCE = Test.loadData(B2B_Custom_Exceptions__c.sObjectType, 'B2BCustomexceptions');
		List<sObject> PLS = Test.loadData(Portal_Settings__c.sObjectType, 'portalsettings');
		List<sObject> ETS = Test.loadData(Email_Triggersends__c.sObjectType, 'ET_Triggersends');

		Account A = new Account();
		A.Name = 'Generic Consumer Account';
		A.shippingcountry = 'Japan';
		A.shippingstreet = 'Test Street';
		A.shippingcity = 'Test city';
		A.shippingpostalcode = 'TEST20017';
		A.shippingstate = 'AICHI KEN';
		insert A;

		Portal_Settings__c GA = new Portal_Settings__c();
		GA.Name = 'GenericAccount Id';
		GA.Value__c = A.Id;
		insert GA;

		Role__c RL = new Role__c();
		RL.Name = 'Technician';
		RL.Approval_Required__c = true;
		RL.Country__c = 'Japan';
		RL.Type__c = 'Manager';
		insert RL;

		JJVC_Contact_Recordtype__mdt rectype = [select MasterLabel from JJVC_Contact_Recordtype__mdt where DeveloperName = 'HCP_Contacts_RecordtypeId'];
		Id HCPRecordTypeId = rectype.MasterLabel;

		Contact cc = new Contact();
		cc.FirstName = 'Acuvue';
		cc.LastName = 'Pro user';
		cc.Salutation = 'Mr.';
		cc.Email = 'Protestuser+acuvue@gmail.com';
		cc.School_Name__c = 'Test';
		cc.Graduation_Year__c = '2014';
		cc.Degree__c = 'Test';
		cc.BirthDate = date.today();
		cc.AccountId = A.iD;
		cc.RecordTypeId = HCPRecordTypeId;
		cc.User_Role__c = RL.Id;
		insert cc;

		User u = new User();
		u.Title = 'Mr.';
		u.FirstName = 'Acuvue';
		u.LastName = 'Pro user';
		u.Email = 'Protestuser+acuvue@gmail.com';
		u.UserName = 'Protestuser+acuvue@gmail.com';
		u.Occupation__c = RL.Name;
		u.Secret_Question__c = 'birth_month';
		u.SecretAnswerSalt__c = RegistrationService.generateSalt();
		Blob hash = Crypto.generateDigest('SHA-512', Blob.valueOf('09/09/1992' + u.SecretAnswerSalt__c));
		u.Secret_Answer__c = EncodingUtil.base64Encode(hash);
		u.Communication_Agreement__c = true;
		u.Localesidkey = 'ja_JP';
		u.Languagelocalekey = 'ja';
		u.EmailEncodingKey = 'ISO-8859-1';
		u.Alias = (u.LastName.length() > 7) ? u.LastName.substring(0, 7) : u.LastName;
		u.TimeZoneSidKey = 'Asia/Tokyo';
		u.CommunityNickname = B2B_Utils.generateGUID();
		u.NPINumber__c = 'TESTPRO01235';
		u.ContactId = cc.Id;
		u.ProfileId = Portal_Settings__c.getValues('ProfileId').Value__c;
		u.Unique_User_Id__c = cc.id;
		insert u;

		return u;
	}

	static testmethod void Testdogetprofile()
	{

		user u = insertuser();
		date dt = date.newInstance(1991, 2, 21);
		Account A2 = new Account();
		A2.Name = 'Acuvue PRO test Acccount';
		A2.shippingcountry = 'Japan';
		A2.shippingstreet = 'Test Street';
		A2.shippingcity = 'Test city';
		A2.shippingpostalcode = 'TEST20017';
		A2.OutletNumber__c = '0003696895';
		A2.shippingstate = 'AICHI KEN';
		A2.Phone = '9854845545';
		A2.Email__C = 'Protestuser+acuvue@gmail.com';
		insert A2;

		Account A3 = new Account();
		A3.Name = 'Acuvue PRO test Acccount-1';
		A3.shippingcountry = 'Japan';
		A3.shippingstreet = 'Test Street';
		A3.shippingcity = 'Test city';
		A3.shippingpostalcode = 'TEST200117';
		A3.OutletNumber__c = '0003696893';
		A3.shippingstate = 'AICHI KEN';
		A3.Phone = '9854845545';
		A3.Email__C = 'Protestuser+acuvue@gmail.com';
		insert A3;

		RestRequest req4 = new RestRequest();
		RestResponse res4 = new RestResponse();
		req4.requestURI = '/apex/myaccount/v1/BusinessProfileService/' + u.id;
		req4.httpMethod = 'GET';
		RestContext.request = req4;
		RestContext.response = res4;
		MyAccountProfileService.doget();

		AccountContactRole__c acr = new AccountContactRole__c();
		acr.Account__c = a3.id;
		acr.Approval_Status__c = 'Rejected';
		acr.Contact__c = u.contactid;

		insert acr;
		test.starttest();
		system.runas(u)
		{
			String dayString = dt.format();
			RestRequest req = new RestRequest();
			RestResponse res = new RestResponse();
			req.requestURI = '/apex/myaccount/v1/BusinessProfileService/' + u.id;
			req.httpMethod = 'GET';
			RestContext.request = req;
			RestContext.response = res;
			string response = MyAccountProfileService.doget();
			system.assertNotEquals(response, null);

			RestRequest req1 = new RestRequest();
			RestResponse res1 = new RestResponse();
			string updatebody = '{"UserId":"' + u.id + '",';
			updatebody = updatebody + '"Salutation":"' + U.Title + '",';
			updatebody = updatebody + '"FirstName":"' + U.firstname + '",';
			updatebody = updatebody + '"LastName":"' + U.LastName + '",';
			updatebody = updatebody + '"EmailAddress":"Protestuser+acuvue1@gmail.com",';
			updatebody = updatebody + '"Username":"Protestuser+acuvue1@gmail.com",';
			updatebody = updatebody + '"Role":"' + U.Occupation__c + '",';
			updatebody = updatebody + '"CommunicationAggrement":' + U.Communication_Agreement__c + ',';
			updatebody = updatebody + '"PrimaryPracticeID":"' + A2.id + '",';
			updatebody = updatebody + '"DateofBirth":"' + dayString + '",';
			updatebody = updatebody + '"SecretQuestion":"' + U.Secret_Question__c + '",';
			updatebody = updatebody + '"SecretAnswer":"test",';
			updatebody = updatebody + '"UserLocale":"' + U.localesidkey + '",';
			updatebody = updatebody + '"UserLanguage":"' + U.Languagelocalekey + '",';
			updatebody = updatebody + '"NPINumber":"' + U.NPINumber__c + '",';
			updatebody = updatebody + '"SchoolName":"Test",';
			updatebody = updatebody + '"GraduationYear":"2014",';
			updatebody = updatebody + '"Degree":"Test",';
			updatebody = updatebody + '"MigrationCompleted":true,';
			updatebody = updatebody + '"IsAlreadyhaveNG":false,';
			updatebody = updatebody + '"Practices":[ {"Id":"' + A2.id + '","Action":"add","Name":"Test","AccountOwnership":true,"SAPAccountNumber":"0002356897","EnrollFindAnEyeDoctor":true,"Phone":"5689784512","isPrimary":true,"ApprovalStatus":"Accepted","Functions":[{"Name":"Ordering"}],"Address":[{"Street":"test","City":"test","State":"test","PostalCode":"test","Country":"United States","Type":"Shipping"}]}],"SalesRepDetails":[]}';

			system.debug('Update profile body####' + updatebody);
			req1.requestURI = '/apex/myaccount/v1/BusinessProfileService/';
			req1.httpMethod = 'POST';
			req1.requestbody = blob.valueof(updatebody);
			RestContext.request = req1;
			RestContext.response = res1;
			MyAccountProfileService.doPost();

			RestRequest req2 = new RestRequest();
			RestResponse res2 = new RestResponse();
			req2.requestURI = '/apex/myaccount/v1/BusinessProfileService/' + u.id;
			req2.httpMethod = 'GET';
			RestContext.request = req2;
			RestContext.response = res2;
			MyAccountProfileService.doget();

			RestRequest req3 = new RestRequest();
			RestResponse res3 = new RestResponse();
			string updatebody1 = '{"UserId":"' + u.id + '",';
			updatebody1 = updatebody1 + '"Salutation":"' + U.Title + '",';
			updatebody1 = updatebody1 + '"FirstName":"' + U.firstname + '",';
			updatebody1 = updatebody1 + '"LastName":"' + U.LastName + '",';
			updatebody1 = updatebody1 + '"EmailAddress":"Protestuser+acuvue1@gmail.com",';
			updatebody1 = updatebody1 + '"Username":"Protestuser+acuvue1@gmail.com",';
			updatebody1 = updatebody1 + '"Role":"' + U.Occupation__c + '",';
			updatebody1 = updatebody1 + '"IsAlreadyhaveNG":false,';
			updatebody1 = updatebody1 + '"Practices":[ {"Id":"' + A3.id + '","Action":"add","Name":"Test","AccountOwnership":true,"SAPAccountNumber":"0002356897","EnrollFindAnEyeDoctor":true,"Phone":"5689784512","isPrimary":true,"ApprovalStatus":"Accepted","Functions":[{"Name":"Ordering"}],"Address":[{"Street":"test","City":"test","State":"test","PostalCode":"test","Country":"United States","Type":"Shipping"}]}],"SalesRepDetails":[]}';
			system.debug('Update profile body####' + updatebody1);
			req3.requestURI = '/apex/myaccount/v1/BusinessProfileService/';
			req3.httpMethod = 'POST';
			req3.requestbody = blob.valueof(updatebody1);
			RestContext.request = req3;
			RestContext.response = res3;
			MyAccountProfileService.doPost();

			MyAccountProfileService.SalesRepDetails map2 = new MyAccountProfileService.SalesRepDetails();
			MyAccountProfileService.UpdateProfile map3 = new MyAccountProfileService.UpdateProfile();
		}
		test.stoptest();
	}

	static testmethod void Testdoupdateprofile()
	{
		user u = insertuser();
		test.starttest();
		system.runas(u)
		{
			RestRequest req3 = new RestRequest();
			RestResponse res3 = new RestResponse();
			string updatebody1 = '{"UserId":"' + u.id + '",';
			updatebody1 = updatebody1 + '"Salutation":"' + U.Title + '",';
			updatebody1 = updatebody1 + '"FirstName":"' + U.firstname + 'test",';
			updatebody1 = updatebody1 + '"LastName":"' + U.LastName + '",';
			updatebody1 = updatebody1 + '"EmailAddress":"Protestuser+acuvue1@gmail.com",';
			updatebody1 = updatebody1 + '"Username":"Protestuser+acuvue1@gmail.com",';
			updatebody1 = updatebody1 + '"IsAlreadyhaveNG":false,';
			updatebody1 = updatebody1 + '"Role":"' + U.Occupation__c + '"}';
			req3.requestURI = '/apex/myaccount/v1/BusinessProfileService/';
			req3.httpMethod = 'POST';
			req3.requestbody = blob.valueof(updatebody1);
			RestContext.request = req3;
			RestContext.response = res3;
			string response = MyAccountProfileService.doPost();
			system.assertNotEquals(response, null);

			RestRequest req5 = new RestRequest();
			RestResponse res5 = new RestResponse();
			string errorbody = '{"UserId":"' + u.id + '","FirstName":""}';
			req5.requestURI = '/apex/myaccount/v1/BusinessProfileService/';
			req5.httpMethod = 'POST';
			req5.requestbody = blob.valueof(errorbody);
			RestContext.request = req5;
			RestContext.response = res5;
			MyAccountProfileService.doPost();

			RestRequest req6 = new RestRequest();
			RestResponse res6 = new RestResponse();
			string errorbody1 = '{"UserId":"' + u.id + '","FirstName":"' + U.firstname + '","LastName":""}';
			req6.requestURI = '/apex/myaccount/v1/BusinessProfileService/';
			req6.httpMethod = 'POST';
			req6.requestbody = blob.valueof(errorbody1);
			RestContext.request = req6;
			RestContext.response = res6;
			MyAccountProfileService.doPost();

			RestRequest req7 = new RestRequest();
			RestResponse res7 = new RestResponse();
			string errorbody2 = '{"UserId":"' + u.id + '","FirstName":"' + U.firstname + '","LastName":"' + u.lastname + '","EmailAddress":""}';
			req7.requestURI = '/apex/myaccount/v1/BusinessProfileService/';
			req7.httpMethod = 'POST';
			req7.requestbody = blob.valueof(errorbody2);
			RestContext.request = req7;
			RestContext.response = res7;
			MyAccountProfileService.doPost();

			RestRequest req8 = new RestRequest();
			RestResponse res8 = new RestResponse();
			string errorbody3 = '{"UserId":"' + u.id + '","FirstName":"' + U.firstname + '","LastName":"' + u.lastname + '","EmailAddress":"' + u.email + '","Salutation":"","Username":"' + u.email + '"}';
			req8.requestURI = '/apex/myaccount/v1/BusinessProfileService/';
			req8.httpMethod = 'POST';
			req8.requestbody = blob.valueof(errorbody3);
			RestContext.request = req8;
			RestContext.response = res8;
			MyAccountProfileService.doPost();
		}
		test.stoptest();
	}

	static testmethod void TestPutpassword()
	{
		user u = insertuser();
		test.starttest();
		system.runas(u)
		{
			RestRequest req1 = new RestRequest();
			RestResponse res1 = new RestResponse();
			string setbody1 = '{"userId":"' + u.id + '","newPassword":"Welcome@2018JNJ","action":"setPassword"}';
			req1.requestURI = '/apex/myaccount/v1/BusinessProfileService/' + u.id;
			req1.httpMethod = 'PUT';
			req1.requestbody = blob.valueof(setbody1);
			RestContext.request = req1;
			RestContext.response = res1;
			String response = MyAccountProfileService.manageUser();
			system.assertNotEquals(response, null);

			RestRequest req2 = new RestRequest();
			RestResponse res2 = new RestResponse();
			string setbody2 = '{"userId":"' + u.id + '","newPassword":"Welcome@2018JNJ","action":"resetPassword"}';
			req2.requestURI = '/apex/myaccount/v1/BusinessProfileService/' + u.id;
			req2.httpMethod = 'PUT';
			req2.requestbody = blob.valueof(setbody2);
			RestContext.request = req2;
			RestContext.response = res2;
			MyAccountProfileService.manageUser();

			RestRequest req3 = new RestRequest();
			RestResponse res3 = new RestResponse();
			string setbody3 = '{"userId":"' + u.id + '","newPassword":"Rest12","action":"resetPassword"}';
			req3.requestURI = '/apex/myaccount/v1/BusinessProfileService/' + u.id;
			req3.httpMethod = 'PUT';
			req3.requestbody = blob.valueof(setbody3);
			RestContext.request = req3;
			RestContext.response = res3;
			MyAccountProfileService.manageUser();

			RestRequest req4 = new RestRequest();
			RestResponse res4 = new RestResponse();
			string setbody4 = '{"userId":"' + u.id + '","newPassword":"","action":"resetPassword"}';
			req4.requestURI = '/apex/myaccount/v1/BusinessProfileService/' + u.id;
			req4.httpMethod = 'PUT';
			req4.requestbody = blob.valueof(setbody4);
			RestContext.request = req4;
			RestContext.response = res4;
			MyAccountProfileService.manageUser();
		}
		test.stoptest();
	}
}