@RestResource(urlMapping='/apex/Promotions/*')
global without sharing class MyAccountPromotion {
    /* --------------------------------------------
     * Author      : 
     * Company     : BICS
     * Description : a)Retrieve the promotions either available to the user or the User has enrolled in.         
     * ---------------------------------------------*/
    global static final String  Receiptnumber_REQUIRED               = 'ACE0003';
    global static final String  INVALID_PROMOTIONID                  = 'ACE0004';
    global static final String  Name_Phone_EMAIL_REQUIRED            = 'ACE0005';
    global static final String  CMEMBER_EXIST                        = 'ACE0006';
    global static final String  INVALID_EMAIL                        = 'ACE0007';
    global static final String  Not_Eligible                         = 'ACE00016';
    global static final String  POSTERRORTYPE                        = 'subscribeToPromotion';


    @HttpPost
    global static String dopost()
    {
        RestRequest req  = RestContext.request;
        RestResponse res = RestContext.response;
        Blob body = req.requestBody;
        String contactID; 
        list<Account> lstAcc = new list<Account>();  

        Map<String, Object> m1 = (Map<String, Object>)JSON.deserializeUntyped(body.toString());
        System.debug('m1:' + m1.get('UserId'));
        System.debug('m1###:' + m1);
                
        Map<String,ExactTarget_Integration_Settings__c> MapcampId = ExactTarget_Integration_Settings__c.getAll();
        string Rtyid = MapcampId.get('RecordType').Value__c;
        string SgCampaign1  = MapcampId.get('SGCampaign').Value__c;
        string SgCampaign2  = MapcampId.get('SGCampaignII').Value__c;
        
        
        
        String  PromotionID              = String.valueOf(m1.get('PromotionId'));
        String  FirstName                = String.valueOf(m1.get('FirstName'));
        String  LastName                 = String.valueOf(m1.get('Surname'));
        String  Email                    = String.valueOf(m1.get('Email'));
        String  FriendName               = String.valueOf(m1.get('FriendName'));
        String  purchasestore            = String.valueOf(m1.get('purchasestore'));
        String  Receiptnumber            = String.valueOf(m1.get('Receiptnumber'));
        String  AcuvueProduct            = String.valueOf(m1.get('AcuvueProduct'));
        Boolean ContactEmail             = Boolean.valueOf(m1.get('ContactEmail'));
        Boolean Contactsms               = Boolean.valueOf(m1.get('Contactsms'));
        Boolean AgreeTC                  = Boolean.valueOf(m1.get('AgreeTC'));
        Boolean AgreePP                  = Boolean.valueOf(m1.get('AgreePP'));
        String  ContactNumber            = String.valueOf(m1.get('ContactNumber'));
        String  Wheredidyouhear          = String.valueOf(m1.get('Wheredidyouhear'));
        String  FriendEmail              = String.valueOf(m1.get('FriendEmail'));
        String  Address                  = String.valueOf(m1.get('Address'));
        String  Suburb                   = String.valueOf(m1.get('Suburb'));
        String  City                     = String.valueOf(m1.get('City'));
        String  State                    = String.valueOf(m1.get('State'));
        String  IPAddress                = String.valueOf(m1.get('IPAddress'));
        string  SourceURL                = String.valueOf(m1.get('SourceURL'));
        string  REDEMPTIONWEBSITE        = String.valueOf(m1.get('REDEMPTIONWEBSITE'));
        string  NRIC                     = String.valueOf(m1.get('NRIC'));
        Boolean CommunicationAgreement   = Boolean.valueOf(m1.get('CommunicationAgreement'));
        String  UtmSource                = String.valueOf(m1.get('UtmSource'));



        Savepoint sp = Database.setSavepoint();
        Map<sObject,String> cMapExist ;
        string Newcamp;

        try{

            if(PromotionID == '' || PromotionID == null){
                throw new CustomException(CustomException.getException(INVALID_PROMOTIONID,POSTERRORTYPE));
            }

            list<Campaign> lstCamp = [SELECT Id,Name,Country__c,Type,SG_Campaign__c FROM Campaign WHERE Id=:PromotionID and IsActive =: true];
            String Country = lstCamp[0].Country__c;
            system.debug('SGCampaign  #####'+lstCamp[0].SG_Campaign__c);
            
            if(lstCamp.isEmpty() && lstCamp.size() == 0)
            {
                throw new CustomException(CustomException.getException(INVALID_PROMOTIONID,POSTERRORTYPE));
            }


            if(ContactNumber == ''  || ContactNumber == null || Email == '' || Email == null || LastName== '' || LastName== null )
            {
                throw new CustomException(CustomException.getException(Name_Phone_EMAIL_REQUIRED,POSTERRORTYPE));

            }

            if(Receiptnumber== ''  || Receiptnumber== null)
            {
                throw new CustomException(CustomException.getException(Receiptnumber_REQUIRED,POSTERRORTYPE));

            }
            if(lstCamp[0].SG_Campaign__c){
                
                ContactMatch cm=new ContactMatch(Email,ContactNumber,LastName,NRIC,lstCamp[0].SG_Campaign__c);
                cMapExist = cm.findcontact();
                
            }else{

                ContactMatch cm=new ContactMatch(Email,ContactNumber,'','',lstCamp[0].SG_Campaign__c);
                cMapExist = cm.findcontact();
            }
             
            if(cMapExist.size()>0 && cMapExist.values()[0]=='Lead')
            {

                System.debug('inputvalues:');
                Database.LeadConvert[] leadsToConvert =new Database.LeadConvert[0];
                for(sObject lead : CMapExist.KeySet())
                {
                    if(lead.get('Status') =='Open')
                    {
                        Database.LeadConvert lc = new Database.LeadConvert();
                        lc.setLeadId(lead.Id);
                        lc.setConvertedStatus('Qualified');
                        lc.setDoNotCreateOpportunity(true);
                        leadsToConvert.add(lc);
                    }
                }

                Database.LeadConvertResult[] lcresults = Database.convertLead(leadsToConvert,true);
                for(Database.LeadConvertResult lcr : lcresults )
                {
                    if(lcr.isSuccess())
                    {
                        contactID = lcr.getContactId();
                        System.debug('contactID:'+contactID);
                        
                        updatecontact(FirstName,LastName,Email,ContactNumber,ContactEmail,Contactsms,AgreeTC,AgreePP,lstCamp[0].Country__c,Rtyid,IPAddress,contactID,NRIC,CommunicationAgreement);
                    }                       
                }

            }else if(cMapExist.size()>0 && cMapExist.values()[0]=='Contact'){

                for(sobject sobj:cMapExist.keySet())
                {
                    contactID = sobj.Id;
                    System.debug('CONTACTID ###'+contactID);
                }

                updatecontact(FirstName,LastName,Email,ContactNumber,ContactEmail,Contactsms,AgreeTC,AgreePP,lstCamp[0].Country__c,Rtyid,IPAddress,contactID,NRIC,CommunicationAgreement);

            }else if(cMapExist.size() ==0 && cMapExist.isEmpty())
            {

                Contact c                 = new contact();
                c.Firstname               = FirstName;
                c.Lastname                = LastName;
                c.Email                   = Email;
                c.contact_method_email__c = ContactEmail;
                c.contact_method_sms__c   = Contactsms;
                c.agree_tc__c             = AgreeTC;
                c.agree_pp__c             = AgreePP;
                c.Phone                   = ContactNumber;
                c.MailingCountry          = lstCamp[0].Country__c;
                c.RecordTypeId            = Rtyid;
                c.IP_Address__c           = IPAddress;
                c.NRIC__c                 = NRIC;
                c.Communication_Agreement__c = CommunicationAgreement;
                insert c;
                system.debug('c ######'+c);
                contactID= c.id;
            }
            
            /* ACUVUE® E-Voucher $30 Journey Revamp*/
            
            if(PromotionID == SgCampaign2){
                
                list<CampaignMember> lstCampMember =[SELECT Id,
                                                              Voucher_Status__c,
                                                              Account__c,
                                                              New_Store__c,
                                                              receipt_number__c,
                                                              Expiry_Date__c,
                                                              Communication_Agreement__c  
                                                              FROM CampaignMember 
                                                              WHERE CampaignId=:PromotionID and ContactId=:contactID];
                System.debug('lstCampMember:'+lstCampMember);
                
                if (!lstCampMember.isEmpty() && lstCampMember.size() > 0)
                {
                    if(lstCampMember[0].Voucher_Status__c != 'Redeemed' && lstCampMember[0].Expiry_Date__c < Date.Today()){
                    
                        if(purchasestore !=null && purchasestore !='')
                        {
                            lstAcc    = [SELECT Id,Name FROM Account WHERE Id=:purchasestore limit 1];
                            system.debug('lstAcc ::::'+lstAcc);
                        }
                        if(!lstAcc.isempty() && lstAcc.size()>0){
                            string AccId = lstAcc[0].Id;
                
                            lstCampMember[0].Account__c      = AccId;    
                        }
                        lstCampMember[0].Voucher_Status__c   =  'Not Used';
                        lstCampMember[0].receipt_number__c   = lstCampMember[0].receipt_number__c +';'+ Receiptnumber;
                        lstCampMember[0].Expiry_Date__c      = Date.Today().addDays(29);
                        lstCampMember[0].Communication_Agreement__c = CommunicationAgreement;
                        
                        update lstCampMember[0];
                        
                        JSONGenerator gen = JSON.createGenerator(true);
                        gen.writeStartObject();
                        gen.writeObjectField('Status','Success');
                        gen.writeObjectField('ContactId',contactID);
                        gen.writeEndObject(); 
                        String pretty = gen.getAsString();
                        return pretty; 
                        
                    }else{
                        
                        throw new CustomException(CustomException.getException(CMEMBER_EXIST,POSTERRORTYPE));
                    }
                }else{
                    
                    list<CampaignMember> oldCampMember =[SELECT Id,Voucher_Status__c FROM CampaignMember WHERE CampaignId=:SgCampaign1 and ContactId=:contactID and Voucher_Status__c ='Redeemed' AND LastModifieddate <2016-02-19T00:00:00Z];
                    System.debug('lstCampMember:'+lstCampMember);
                    
                    if (!oldCampMember.isEmpty() && oldCampMember.size() > 0)
                    {
                        throw new CustomException(CustomException.getException(Not_Eligible,POSTERRORTYPE));
                    }else{
                        return subscribeToPromotion(contactID,Receiptnumber,purchasestore,AcuvueProduct,PromotionId,FriendName,Wheredidyouhear,FriendEmail,SourceURL,REDEMPTIONWEBSITE,Address,Suburb,City,State,Country,CommunicationAgreement,UtmSource);
                    }
                }
                
            }else{
                
                /*Basic OR Basic+ECP Campaigns*/
                
                if(lstCamp[0].Type == 'Basic' || lstCamp[0].Type == 'Basic+ECP')
                {
                    System.debug('Basic:'+ lstCamp[0].Type);
                    list<CampaignMember> lstCampMember =[SELECT Id FROM CampaignMember WHERE CampaignId=:PromotionID and ContactId=:contactID];
                    System.debug('lstCampMember:'+lstCampMember);
    
                    if (!lstCampMember.isEmpty() && lstCampMember.size() > 0)
                    {
                        throw new CustomException(CustomException.getException(CMEMBER_EXIST,POSTERRORTYPE));
                    }else{
                        return subscribeToPromotion(contactID,Receiptnumber,purchasestore,AcuvueProduct,PromotionId,FriendName,Wheredidyouhear,FriendEmail,SourceURL,REDEMPTIONWEBSITE,Address,Suburb,City,State,Country,CommunicationAgreement,UtmSource);
                    }
                }
            }   
        }catch(Exception e){

            if(e.getMessage().contains('INVALID_EMAIL_ADDRESS'))
            {
                return String.valueOf(CustomException.getException(INVALID_EMAIL,POSTERRORTYPE));
            }
            if(e.getMessage().contains('REQUIRED_FIELD_MISSING'))
            {
                return String.valueOf(CustomException.getException(Name_Phone_EMAIL_REQUIRED,POSTERRORTYPE));
            }
            if(e.getMessage().contains('List index out of bounds'))
            {
                return String.valueof(CustomException.getException(INVALID_PROMOTIONID,POSTERRORTYPE));
            }

            Database.rollback(sp);
            return e.getMessage();
        }
        return null;
    }


    global static String updatecontact(String FirstName,String LastName,String Email,String ContactNumber,Boolean ContactEmail,Boolean Contactsms,Boolean AgreeTC,Boolean AgreePP, String Country,String Rtyid,String IPAddress,String ContactID,string NRIC,Boolean CommunicationAgreement)
    {
        Contact cont = [SELECT Id,
                        contact_method_email__c,
                        contact_method_sms__c,
                        agree_tc__c,
                        agree_pp__c,
                        Phone,
                        Name,
                        Email,Communication_Agreement__c 
                        FROM Contact 
                        WHERE Id=:contactID];

        cont.Firstname                  = FirstName;
        cont.Lastname                   = LastName;
        cont.Email                      = Email;
        cont.Phone                      = ContactNumber;
        cont.contact_method_email__c    = ContactEmail;
        cont.contact_method_sms__c      = Contactsms;
        cont.agree_tc__c                = AgreeTC;
        cont.agree_pp__c                = AgreePP;
        cont.MailingCountry             = Country;
        cont.RecordTypeId               = Rtyid;
        cont.IP_Address__c              = IPAddress;
        cont.NRIC__c                    = NRIC;
        cont.Communication_Agreement__c = CommunicationAgreement;
        update cont;

        return null;    
    } 

    global static String SubscribeToPromotion(string contactID,string Receiptnumber,string purchasestore,string AcuvueProduct,string PromotionId,string FriendName,string Wheredidyouhear,string FriendEmail,string SourceURL,string REDEMPTIONWEBSITE,String Address,String Suburb,string City,string State,string Country,Boolean CommunicationAgreement,String UtmSource)
    {
        list<Account> lstAcc = new list<Account>();
        
        Map<String,ExactTarget_Integration_Settings__c> MapcampId = ExactTarget_Integration_Settings__c.getAll();
        string SgCampaign2  = MapcampId.get('SGCampaignII').Value__c;
        
        if(purchasestore !=null && purchasestore !='')
        {
            lstAcc    = [SELECT Id,Name,PublicETrialStoreId__c FROM Account WHERE Id=:purchasestore limit 1];
            system.debug('lstAcc ::::'+lstAcc);
        }

        CampaignMember campmem      =  new CampaignMember();
        campmem.CampaignId          =  PromotionId;
        campmem.ContactId           =  contactID;
        campmem.Status              =  'Subscribed';
        campmem.receipt_number__c   =  Receiptnumber;

        if(!lstAcc.isempty() && lstAcc.size()>0){
            string AccId = lstAcc[0].Id;

            campmem.Account__c      = AccId;    
        }else{
            campmem.New_Store__c    = purchasestore;
        }

        campmem.Product__c          = AcuvueProduct;
        campmem.Friend_s_Name__c    = FriendName;
        campmem.Approval_by_Campaign_Admin__c = false;
        campmem.FriendEmail__c      = FriendEmail;
        campmem.Where_did_you_hear_about_this_promotion__c = Wheredidyouhear;
        //campmem.Source_URL__c       = SourceURL;
        campmem.REDEMPTION_WEBSITE__c = REDEMPTIONWEBSITE;
        campmem.Practice_Street__c    = Address;
        campmem.suburb__c             = Suburb;
        campmem.Practice_City__c      = City;
        campmem.Practice_State__c     = State;
        campmem.Practice_Country__c   = Country;
        campmem.Communication_Agreement__c = CommunicationAgreement;
        campmem.Source__c   =UtmSource; 
        if(PromotionId == SgCampaign2){
            
            campmem.Expiry_Date__c      = Date.Today().addDays(29);
        }
        insert campmem;  

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeObjectField('Status','Success');
        gen.writeObjectField('ContactId',contactID);
        gen.writeEndObject(); 
        String pretty = gen.getAsString();
        return pretty;            

    }

}