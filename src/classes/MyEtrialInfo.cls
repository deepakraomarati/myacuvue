@RestResource(urlmapping='/apex/etrialinfo/*')
global without sharing class MyEtrialInfo{

 global static final String REQURIED_FIELDS   = 'ACE0002';
 global static final String INVALID_EMAIL     = 'ACE0007';
 global static final String EMAIL_FIELDS      = 'ACE0008';
 global static final String ETRIAL_EXIT       = 'ACE0009';
 global static final String INVALID_PRODCTID = 'ACE0012';
 global static final String INVALID_ACCOUNTID = 'ACE0011';
 global static final String POST_ERROR_TYPE   = 'SubscribeToEtrial';
 global static final String GET_ERROR_TYPE    = 'GetEtrial';
 global static final String ACCOUNT_REQUIRED  = 'ACE00013';
 global static final String PRODUCT_REQUIRED  = 'ACE00014';
 global static final String INVALID_DATE      = 'ACE00015';
 
 public static List<EtrailDTO> lstEtrailDTO=new List<EtrailDTO>();
 
 @HttpGet
 global static String DoEtrial() 
 {
    RestRequest req  = RestContext.request;
    RestResponse res = RestContext.response;
    String Email = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    
    return GetEtrial(Email);    
  }
  
  
 @HTTPPOST
 global static String DoPost()
 {
    RestRequest req  = RestContext.request;
    RestResponse res = RestContext.response;
    Blob body=req.requestBody;
    Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(body.toString());
    
    String Email = String.ValueOf(m.get('Email'));
    String Country = String.ValueOf(m.get('Country'));
    String EtrialGender=String.ValueOf(m.get('EtrialGender'));
    String EtrialFirstName =String.ValueOf(m.get('EtrialFirstName'));
    String EtrialLastName =String.ValueOf(m.get('EtrialLastName'));
    String EtrialPhone =String.ValueOf(m.get('EtrialPhone'));
    String AcuAddress =String.ValueOf(m.get('AcuAddress'));
    String AcuBirthDate = String.ValueOf(m.get('AcuBirthDate'));
    Boolean AcuEnewsletter =Boolean.ValueOf(m.get('AcuEnewsletter'));
    String AcuQuestion =String.ValueOf(m.get('AcuQuestion'));
    Boolean EtrialCheckbox1 =Boolean.ValueOf(m.get('EtrialCheckbox1'));
    Boolean EtrialCheckbox2 =Boolean.ValueOf(m.get('EtrialCheckbox2'));
    Boolean EtrialCheckbox3 =Boolean.ValueOf(m.get('EtrialCheckbox3'));
    Boolean EtrialCheckbox4 =Boolean.ValueOf(m.get('EtrialCheckbox4'));
    Boolean Etrialtnc=Boolean.ValueOf(m.get('Etrialtnc'));
    String AccountId =String.ValueOf(m.get('AccountId'));
    String ProductId = String.ValueOf(m.get('ProductId'));
    
    return SubscribeToEtrial(Email,Country,EtrialGender,EtrialFirstName,EtrialLastName,EtrialPhone,AcuAddress,AcuBirthDate,AcuEnewsletter,AcuQuestion,EtrialCheckbox1,EtrialCheckbox2,EtrialCheckbox3,EtrialCheckbox4,Etrialtnc,AccountId,ProductId);
 }
 
 global static String GetEtrial(String Email)
 {
    try
    {
        list<Etrial__c> etobj   = [SELECT Id,
                                            etrial_email__c,
                                            Country__c,
                                            etrial_gender__c,
                                            etrial_first_name__c,
                                            etrial_last_name__c,
                                            etrial_phone__c,
                                            acu_address__c,
                                            acu_birthdate__c,
                                            acu_enewsletter__c,
                                            acu_question__c,
                                            etrial_registration_check_1__c,
                                            etrial_registration_check_2__c,
                                            etrial_registration_check_3__c,
                                            etrial_registration_check_4__c,
                                            etrial_product__c,
                                            Account__c,
                                            etrial_tnc__c,
                                            RedemptionCode__c FROM Etrial__c WHERE etrial_email__c=:Email];
    
    if(etobj.size()>0 && !etobj.isEmpty())
    {
        for(Etrial__c etr:etobj)
        {
            String myDatetimeStr;
            
            if(etr.acu_birthdate__c !=null){
                Datetime myDatetime;
                myDatetime = etr.acu_birthdate__c;
                Date ddate=myDatetime.dateGmt();
                DateTime dfdate=ddate;
                myDatetimeStr = dfdate.formatGmt('d/MM/yyyy');
            }
            
           String pid;
           if(etr.etrial_product__c !=null) 
           {  
             pid = String.valueOf(etr.etrial_product__c).subString(0,15); 
           }  
            
            EtrailDTO etobj1 =new EtrailDTO(etr.id,etr.etrial_email__c,etr.Country__c,etr.etrial_gender__c,etr.etrial_first_name__c,etr.etrial_last_name__c,etr.etrial_phone__c,etr.acu_address__c,myDatetimeStr,etr.acu_enewsletter__c,etr.acu_question__c,etr.etrial_registration_check_1__c,etr.etrial_registration_check_2__c,etr.etrial_registration_check_3__c,etr.RedemptionCode__c,etr.etrial_registration_check_4__c,etr.etrial_tnc__c,etr.Account__c,pid);
            lstEtrailDTO.add(etobj1);
         }
         
         
         return json.serialize(lstEtrailDTO);
     }
     else
     {
        throw new CustomException(CustomException.getException(EMAIL_FIELDS,GET_ERROR_TYPE));
     } 
    }
    catch(Exception e)
    {
        return e.getMessage();
        }
        
        return null;
    }
 
  
 
 global static String SubscribeToEtrial(String Email,String Country,String EtrialGender,String EtrialFirstName,String EtrialLastName,String EtrialPhone,String AcuAddress,String AcuBirthDate,Boolean AcuEnewsletter,String AcuQuestion,Boolean EtrialCheckbox1,Boolean EtrialCheckbox2,Boolean EtrialCheckbox3,Boolean EtrialCheckbox4,Boolean Etrialtnc,String AccountId,String ProductId)
 {
    try{
    
        list<Account> lstAcc;
        list<Product2> lstPro;
        
        
        if(Email =='' || Country =='')
        {
            throw new CustomException(CustomException.getException(REQURIED_FIELDS,POST_ERROR_TYPE));
        }
        
     
        if(AccountId == '' || AccountId == null){
            throw new CustomException(CustomException.getException(ACCOUNT_REQUIRED,POST_ERROR_TYPE));
        }
        
        
        if(ProductId== '' || ProductId== null){
            throw new CustomException(CustomException.getException(PRODUCT_REQUIRED,POST_ERROR_TYPE));
        }
        
       
        
        lstAcc = [SELECT Id, Name FROM ACCOUNT  WHERE Id=:AccountId];
       
        if(lstacc.size() ==0 && lstacc.isEmpty())
        {
          throw new CustomException(CustomException.getException(INVALID_ACCOUNTID,POST_ERROR_TYPE));
        }
        
        
          
       lstPro = [SELECT Id,Name FROM Product2 WHERE Id=:ProductId];
                                        
         if(lstPro .isEmpty() && lstPro .size() == 0)
          {
             throw new CustomException(CustomException.getException(INVALID_PRODCTID,POST_ERROR_TYPE));
          }
       
        
          list<Etrial__c> etlist  = [SELECT Id,
                                            etrial_email__c,
                                            Country__c,
                                            etrial_gender__c,
                                            etrial_first_name__c,
                                            etrial_last_name__c,
                                            etrial_phone__c,
                                            acu_address__c,
                                            acu_birthdate__c,
                                            acu_enewsletter__c,
                                            acu_question__c,
                                            etrial_registration_check_1__c,
                                            etrial_registration_check_2__c,
                                            etrial_registration_check_3__c,
                                            etrial_registration_check_4__c,
                                            etrial_tnc__c,
                                            RedemptionCode__c FROM Etrial__c WHERE etrial_email__c=:Email AND Country__c=:Country and createddate >= LAST_N_DAYS:365];
    
        string RedemptionCode;
        string ID;
        
        if(etlist.size()>0 && !etlist.isEmpty())
        {
            throw new CustomException(CustomException.getException(ETRIAL_EXIT,POST_ERROR_TYPE));        
        }
        
        else{
            Date dtime;
            if(AcuBirthDate !='' )
            {
            
            String[] strDate1 = AcuBirthDate.split('/');
            System.debug('Formatted:'+strDate1[2]+'-'+strDate1[1]+'-'+strDate1[0]);
            String stdate1  = strDate1[2]+'-'+strDate1[1]+'-'+strDate1[0];
            System.debug('Splittddate###'+stdate1);
            dtime =Date.valueOf(stdate1);
            System.debug('Split@@@@'+dtime);
            
            } 
            
            Etrial__c  et = new Etrial__c();
            et.etrial_email__c          =Email;
            et.Country__c               =Country;
            et.etrial_gender__c         =EtrialGender;
            et.etrial_first_name__c     =EtrialFirstName; 
            et.etrial_last_name__c      =EtrialLastName;
            et.etrial_phone__c          =EtrialPhone;
            et.acu_address__c           =AcuAddress;
            et.acu_birthdate__c         =dtime;
            et.acu_enewsletter__c       =AcuEnewsletter; 
            et.acu_question__c          =AcuQuestion;
            et.etrial_registration_check_1__c =EtrialCheckbox1;
            et.etrial_registration_check_2__c =EtrialCheckbox2;
            et.etrial_registration_check_3__c =EtrialCheckbox3;
            et.etrial_registration_check_4__c =EtrialCheckbox4;
            et.etrial_tnc__c                 =etrialtnc;
            et.Account__c                  =AccountId;
            et.etrial_product__c           =ProductId;
            
            insert et;
            
            Etrial__c  etrl =[SELECT Id,RedemptionCode__c FROM Etrial__c  WHERE Id=:et.Id];
        
            RedemptionCode = etrl.RedemptionCode__c;
            ID=etrl.id;
        
       }
       
       system.debug('RedemptionCode  ####'+RedemptionCode );
       EtrailDTO etobj=new EtrailDTO(ID,Email,Country,EtrialGender,EtrialFirstName,EtrialLastName,EtrialPhone,AcuAddress,AcuBirthDate,AcuEnewsletter,AcuQuestion,EtrialCheckbox1,EtrialCheckbox2,EtrialCheckbox3,RedemptionCode,EtrialCheckbox4,Etrialtnc,AccountId,ProductId);
       lstEtrailDTO.add(etobj);
       
       EtrailListDTO Etrailobj=new EtrailListDTO('Success',lstEtrailDTO);
       return json.serialize(Etrailobj);
       
       }
       catch(Exception e)
       {
        if(e.getMessage().contains('INVALID_EMAIL_ADDRESS'))
        {
            return String.valueOf(CustomException.getException(INVALID_EMAIL,POST_ERROR_TYPE));
        }
        
        if(e.getMessage().contains('Invalid date'))
        {
            return String.valueOf(CustomException.getException(INVALID_DATE ,POST_ERROR_TYPE));
        }
        
        
        
        return e.getMessage();
      }
   }
 
 
 public class EtrailDTO
 {  public String ID{get;set;}
    public String Email {get;set;}
    public String Country {get;set;}
    public String EtrialGender{get;set;}
    public String EtrialFirstName{get;set;}
    public String EtrialLastName{get;set;}
    public String EtrialPhone{get;set;}
    public String AcuAddress{get;set;}
    public String AcuBirthDate {get;set;}
    public Boolean AcuEnewsletter {get;set;}
    public String AcuQuestion {get;set;}
    public Boolean EtrialCheckbox1{get;set;}
    public Boolean EtrialCheckbox2{get;set;}
    public Boolean EtrialCheckbox3{get;set;}
    public String  RedemptionCode{get;set;}
    public Boolean EtrialCheckbox4{get;set;}
    public Boolean Etrialtnc{get;set;}
    public String AccountId{get;set;}
    public String ProductId{get;set;}
    
    public EtrailDTO(String ID,String Email,String Country,String EtrialGender,String EtrialFirstName,String EtrialLastName,String EtrialPhone,String AcuAddress,String AcuBirthDate,Boolean AcuEnewsletter,String AcuQuestion,Boolean EtrialCheckbox1,Boolean EtrialCheckbox2,Boolean EtrialCheckbox3,String RedemptionCode,Boolean EtrialCheckbox4,Boolean Etrialtnc,String AccountId,String ProductId){
        
        this.ID= ID;
        this.Email= Email;
        this.Country= Country;
        this.EtrialGender= EtrialGender;
        this.EtrialFirstName= EtrialFirstName;
        this.EtrialLastName= EtrialLastName;
        this.EtrialPhone= EtrialPhone;
        this.AcuAddress= AcuAddress;
        this.AcuBirthDate= AcuBirthDate;
        this.AcuEnewsletter= AcuEnewsletter;
        this.AcuQuestion= AcuQuestion;
        this.EtrialCheckbox1= EtrialCheckbox1;
        this.EtrialCheckbox2= EtrialCheckbox2;
        this.EtrialCheckbox3= EtrialCheckbox3;
        this.RedemptionCode= RedemptionCode;
        this.EtrialCheckbox4= EtrialCheckbox4;
        this.Etrialtnc= Etrialtnc;
        this.AccountId= AccountId;
        this.ProductId= ProductId;
        

    }
  }
 
 public class EtrailListDTO
 {
   public String Status {get;set;}
   public List<EtrailDTO> Etrials {get;set;}
   public EtrailListDTO(String Status,List<EtrailDTO> Etrials){
     this.Status= Status;
     this.Etrials= Etrials;
   }
 }
}