/**
* File Name: MyTeamServiceTest
* Author : Venkta Mahesh | BICSGLOBAL
* Date Last Modified:  25-Oct-2018
* Description : Test class for MyTeamService class
* Copyright (c) $2018 Johnson & Johnson
*/
@isTest(SeeAlldata=false)
public class MyTeamServiceTest
{

	@istest static void TestdoGet()
	{
		insertData();
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		User TestUser = [SELECT id,AccountId, ContactId FROM user WHERE UserName = 'protestuser+acuvue@gmail.com' limit 1];
		system.runas(TestUser)
		{
			req.requestURI = '/apex/myaccount/v1/MyTeamService/' + TestUser.id;
			req.httpMethod = 'GET';
			RestContext.request = req;
			RestContext.response = res;
			string response = MyTeamService.doGet();
			system.assertNotEquals(response, null);

			req.requestURI = '/apex/myaccount/v1/MyTeamService/' + Testuser.id + '/' + TestUser.AccountId;
			req.httpMethod = 'GET';
			RestContext.request = req;
			RestContext.response = res;
			MyTeamService.doGet();

			req.requestURI = '/apex/myaccount/v1/MyTeamService//' + TestUser.AccountId;
			req.httpMethod = 'GET';
			RestContext.request = req;
			RestContext.response = res;
			MyTeamService.doGet();
		}
		req.requestURI = '/apex/myaccount/v1/MyTeamService/' + userinfo.getuserid() + '/' + TestUser.AccountId;
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		MyTeamService.doGet();

		req.requestURI = '/apex/myaccount/v1/MyTeamService/' + userinfo.getuserid();
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		MyTeamService.doGet();
	}
	@istest static void TestdoPost()
	{
		insertData();
		User TestUser1 = [SELECT id,AccountId, ContactId FROM user WHERE UserName = 'protestuser+acuvue@gmail.com' limit 1];
		system.runas(TestUser1)
		{
			RestRequest req1 = new RestRequest();
			RestResponse res1 = new RestResponse();
			req1.requestURI = '/apex/myaccount/v1/MyTeamService/' + Testuser1.id;
			String jsoninput = '{"UserId":"' + Testuser1.id + '","ContactId":"' + Testuser1.ContactId + '","Practices": [{"PracticeId":"' + Testuser1.AccountId + '","Functions":[{"Name": "Administrator","Action": "add"}]}]}';
			req1.httpMethod = 'POST';
			req1.requestbody = blob.valueof(jsoninput);
			RestContext.request = req1;
			RestContext.response = res1;
			string response = MyTeamService.updateAffiliations();
			system.assertNotEquals(response, null);

			req1.requestURI = '/apex/myaccount/v1/MyTeamService/' + Testuser1.id;
			String jsoninput1 = '{"UserId":"' + Testuser1.id + '","ContactId":"' + Testuser1.ContactId + '","Practices": [{"PracticeId":"' + Testuser1.AccountId + '","Functions":[{"Name": "Administrator","Action": "add"},{"Name": "Ordering","Action": "add"}]}]}';
			req1.httpMethod = 'POST';
			req1.requestbody = blob.valueof(jsoninput1);
			RestContext.request = req1;
			RestContext.response = res1;
			MyTeamService.updateAffiliations();

			req1.requestURI = '/apex/myaccount/v1/MyTeamService/' + Testuser1.id;
			String jsoninput2 = '{"UserId":"' + Testuser1.id + '","ContactId":"' + Testuser1.ContactId + '","Practices": [{"PracticeId":"' + Testuser1.AccountId + '","Functions":[{"Name": "Administrator","Action": "add"},{"Name": "Ordering","Action": "add"}]}]}';
			req1.httpMethod = 'POST';
			req1.requestbody = blob.valueof(jsoninput2);
			RestContext.request = req1;
			RestContext.response = res1;
			MyTeamService.updateAffiliations();

			req1.requestURI = '/apex/myaccount/v1/MyTeamService/' + Testuser1.id;
			String jsoninput3 = '{"UserId":"' + Testuser1.id + '","ContactId":"' + Testuser1.ContactId + '","Practices": [{"PracticeId":"' + Testuser1.AccountId + '","Functions":[{"Name": "Administrator","Action": "remove"}]}]}';
			req1.httpMethod = 'POST';
			req1.requestbody = blob.valueof(jsoninput3);
			RestContext.request = req1;
			RestContext.response = res1;
			MyTeamService.updateAffiliations();
		}
	}
	@istest static void TestdoPut()
	{
		insertData();
		User ownuser = [SELECT id,AccountId, ContactId FROM user WHERE UserName = 'protestuser+acuvue@gmail.com' limit 1];
		RestRequest req2 = new RestRequest();
		RestResponse res2 = new RestResponse();
		req2.requestURI = '/apex/myaccount/v1/MyTeamService/' + ownuser.id;
		String jsoninput1 = '{"userId":"' + ownuser.id + '","Action": "add","People":[{"FirstName": "Acuvue","LastName": "Pro user1","Role": "Technician","EmailAddress":"protestuser+acuvue1@gmail.com","PracticeId": "' + ownuser.AccountId + '"}]}';
		req2.httpMethod = 'PUT';
		req2.requestbody = blob.valueof(jsoninput1);
		RestContext.request = req2;
		RestContext.response = res2;
		string response = MyTeamService.doPut();
		system.assertNotEquals(response, null);

		req2.requestURI = '/apex/myaccount/v1/MyTeamService/' + ownuser.id;
		String jsoninput2 = '{"userId":"' + ownuser.id + '","Action":"add","People":[{"FirstName": "Acuvue","LastName": "Pro user1","Role": "Technician","EmailAddress": "protestuser+acuvue2#gmail.com","PracticeId": "' + ownuser.AccountId + '"}]}';
		req2.httpMethod = 'PUT';
		req2.requestbody = blob.valueof(jsoninput2);
		RestContext.request = req2;
		RestContext.response = res2;
		MyTeamService.doPut();

		req2.requestURI = '/apex/myaccount/v1/MyTeamService/' + ownuser.id;
		String jsoninput3 = '{"userId":"' + ownuser.id + '","Action":"add","People":[{"FirstName": "Acuvue","LastName": "Pro user","Role": "Technician","EmailAddress": "protestuser+acuvue@gmail.com","PracticeId": "' + ownuser.AccountId + '"}]}';
		req2.httpMethod = 'PUT';
		req2.requestbody = blob.valueof(jsoninput3);
		RestContext.request = req2;
		RestContext.response = res2;
		MyTeamService.doPut();

	}
	@istest static void TestdoPatch()
	{
		insertData();
		User TestUser1 = [SELECT id,AccountId, ContactId FROM user WHERE UserName = 'protestuser+acuvue@gmail.com' limit 1];
		RestRequest req3 = new RestRequest();
		RestResponse res3 = new RestResponse();

		req3.requestURI = '/apex/myaccount/v1/MyTeamService/';
		String jsoninput2 = '{"UserID":"'+userinfo.getuserid()+'","action": "Reject","PracticeId":"' + TestUser1.Accountid + '","ContactId":"' + TestUser1.Contactid + '"}';
		req3.httpMethod = 'PATCH';
		req3.requestbody = blob.valueof(jsoninput2);
		RestContext.request = req3;
		RestContext.response = res3;
		string response = MyTeamService.manageMyTeamApprovals();
		system.assertNotEquals(response, null);

		req3.requestURI = '/apex/myaccount/v1/MyTeamService/';
		String jsoninput3 = '{"UserID":"'+userinfo.getuserid()+'","action": "approve","PracticeId":"' + TestUser1.Accountid + '","ContactId":"' + TestUser1.Contactid + '"}';
		req3.httpMethod = 'PATCH';
		req3.requestbody = blob.valueof(jsoninput3);
		RestContext.request = req3;
		RestContext.response = res3;
		MyTeamService.manageMyTeamApprovals();
	}

	@istest static void TestdoDelete()
	{
		insertData();
		User staff = [SELECT id,AccountId, ContactId FROM user WHERE UserName = 'Protestuser+acuvue1@gmail.com' limit 1];
		User owner = [SELECT id,AccountId, ContactId FROM user WHERE UserName = 'Protestuser+acuvue@gmail.com' limit 1];
		RestRequest req2 = new RestRequest();
		RestResponse res2 = new RestResponse();

		req2.requestURI = '/apex/myaccount/v1/MyTeamService/';
		String jsoninput1 = '{"userId": "' + owner.id + '","PracticeId":"' + staff.accountid + '","ContactId":"' + staff.ContactId + '"}';
		req2.httpMethod = 'PATCH';
		req2.requestbody = blob.valueof(jsoninput1);
		RestContext.request = req2;
		RestContext.response = res2;
		string response = MyTeamService.doDelete();
		system.assertNotEquals(response, null);
	}

	public static void insertData()
	{
		List<sObject> B2BCE = Test.loadData(B2B_Custom_Exceptions__c.sObjectType, 'B2BCustomexceptions');
		List<sObject> PLS = Test.loadData(Portal_Settings__c.sObjectType, 'portalsettings');
		List<sObject> ETS = Test.loadData(Email_Triggersends__c.sObjectType, 'ET_Triggersends');
		List<User> Newusers = New list<User>();

		Account A = new Account();
		A.Name = 'Generic Consumer Account';
		A.shippingcountry = 'United States';
		A.shippingstreet = 'Test Street';
		A.shippingcity = 'Test city';
		A.shippingpostalcode = 'TEST20017';
		insert A;

		Portal_Settings__c GA = new Portal_Settings__c();
		GA.Name = 'GenericAccount Id';
		GA.Value__c = A.Id;
		insert GA;

		Account Acc = new Account();
		Acc.Name = 'Test Account';
		Acc.shippingcountry = 'United States';
		Acc.shippingstreet = 'Test Street';
		Acc.shippingcity = 'Test city';
		Acc.shippingpostalcode = 'TEST20017';
		Acc.OutletNumber__c = '12345678';
		Acc.Email__c = 'protestuser+acuvue@gmail.com';
		insert Acc;

		Role__c RL = new Role__c();
		RL.Name = 'Store Manger';
		RL.Approval_Required__c = true;
		RL.Country__c = 'Japan';
		RL.Type__c = 'Manager';
		insert RL;

		JJVC_Contact_Recordtype__mdt rectype = [SELECT MasterLabel FROM JJVC_Contact_Recordtype__mdt WHERE DeveloperName = 'HCP_Contacts_RecordtypeId'];
		Id HCPRecordTypeId = rectype.MasterLabel;

		Contact cc = new Contact();
		cc.FirstName = 'Acuvue';
		cc.LastName = 'Pro user';
		cc.Salutation = 'Mr.';
		cc.Email = 'protestuser+acuvue@gmail.com';
		cc.School_Name__c = 'Test';
		cc.Graduation_Year__c = '2014';
		cc.Degree__c = 'Test';
		cc.BirthDate = date.today();
		cc.AccountId = Acc.iD;
		cc.RecordTypeId = HCPRecordTypeId;
		cc.user_Role__c = RL.Id;
		insert cc;

		AccountContactRole__c acr =
		[
				SELECT ID, Primary__c, Account__c, Contact__c,Functions__c,Approval_Status__c
				FROM AccountContactRole__c
				WHERE Contact__c = :cc.id
		];
		acr.Approval_Status__c = 'Accepted';
		acr.Functions__c = 'Administrator';
		update acr;

		User Testuser = new User();
		Testuser.Title = 'Mr.';
		Testuser.FirstName = 'Acuvue';
		Testuser.LastName = 'Pro user';
		Testuser.Email = 'protestuser+acuvue@gmail.com';
		Testuser.UserName = 'protestuser+acuvue@gmail.com';
		Testuser.Occupation__c = RL.Name;
		Testuser.Secret_Question__c = 'birth_month';
		Testuser.SecretAnswerSalt__c = RegistrationService.generateSalt();
		Blob hash = Crypto.generateDigest('SHA-512', Blob.valueOf('09/09/1992' + Testuser.SecretAnswerSalt__c));
		Testuser.Secret_Answer__c = EncodingUtil.base64Encode(hash);
		Testuser.Communication_Agreement__c = true;
		Testuser.Localesidkey = 'ja_JP';
		Testuser.Languagelocalekey = 'ja';
		Testuser.EmailEncodingKey = 'ISO-8859-1';
		Testuser.Alias = (Testuser.LastName.length() > 7) ? Testuser.LastName.substring(0, 7) : Testuser.LastName;
		Testuser.TimeZoneSidKey = 'America/New_York';
		Testuser.CommunityNickname = B2B_Utils.generateGUID();
		Testuser.NPINumber__c = 'TESTPRO01235';
		Testuser.ContactId = cc.Id;
		Testuser.Unique_User_Id__c = cc.Id;
		Testuser.ProfileId = Portal_Settings__c.getValues('ProfileId').Value__c;
		Newusers.add(Testuser);

		//***Generic Account User***//

		Role__c RL1 = new Role__c();
		RL1.Name = 'Technician';
		RL1.Approval_Required__c = true;
		RL1.Country__c = 'Japan';
		RL1.Type__c = 'Staff';
		insert RL1;

		Contact cc1 = new Contact();
		cc1.FirstName = 'Acuvue';
		cc1.LastName = 'Pro user1';
		cc1.Salutation = 'Mr.';
		cc1.Email = 'protestuser+acuvue1@gmail.com';
		cc1.School_Name__c = 'Test';
		cc1.Graduation_Year__c = '2014';
		cc1.Degree__c = 'Test';
		cc.BirthDate = date.today();
		cc1.AccountId = Acc.iD;
		cc1.RecordTypeId = HCPRecordTypeId;
		cc1.user_Role__c = RL1.Id;
		insert cc1;

		User Generaluser = new User();
		Generaluser.Title = 'Mr.';
		Generaluser.FirstName = 'Acuvue';
		Generaluser.LastName = 'Pro user1';
		Generaluser.Email = 'protestuser+acuvue1@gmail.com';
		Generaluser.UserName = 'protestuser+acuvue1@gmail.com';
		Generaluser.Occupation__c = RL1.Name;
		Generaluser.Secret_Question__c = 'birth_month';
		Blob hash1 = Crypto.generateDigest('SHA-512', Blob.valueOf('09/09/1992' + Generaluser.SecretAnswerSalt__c));
		Generaluser.Secret_Answer__c = EncodingUtil.base64Encode(hash1);
		Generaluser.Communication_Agreement__c = true;
		Generaluser.Localesidkey = 'ja_JP';
		Generaluser.Languagelocalekey = 'ja';
		Generaluser.EmailEncodingKey = 'ISO-8859-1';
		Generaluser.Alias = (Generaluser.LastName.length() > 7) ? Generaluser.LastName.substring(0, 7) : Generaluser.LastName;
		Generaluser.TimeZoneSidKey = 'America/New_York';
		Generaluser.CommunityNickname = B2B_Utils.generateGUID();
		Generaluser.NPINumber__c = 'TESTPRO012356';
		Generaluser.ContactId = cc1.Id;
		Generaluser.ProfileId = Portal_Settings__c.getValues('ProfileId').Value__c;
		Generaluser.Unique_User_Id__c = cc1.Id;
		Newusers.add(Generaluser);
		insert Newusers;
	}
}