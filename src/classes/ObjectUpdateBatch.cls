global class ObjectUpdateBatch implements Database.Batchable<sObject>,Database.Stateful{
    string query;
    Integer ObjectsTag = 0;
    Integer TotalObjectsTag=0;
    string field='TotalCount__c';
    
    
     global  ObjectUpdateBatch(String q,Integer C)
      {
         query = q;
         ObjectsTag= C;
      }
      
    global Database.QueryLocator start(Database.BatchableContext BC){
        //String query = 'SELECT Id,Name FROM Account';        
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        //ObjectsTag = ObjectsTag+scope.size();
        TotalObjectsTag= TotalObjectsTag+scope.size();
        System.debug('ObjectsTag:'+TotalObjectsTag);
    }   
    global void finish(Database.BatchableContext BC){
       if(ObjectsTag==3)
        {
             List<CountObject__c> t = [Select ID, Count__C from CountObject__c where Name = 'BatchDetails'];
             t[0].Count__c = TotalObjectsTag;
             update t;
             System.debug('ObjectsTag'+TotalObjectsTag);
        }
        
        if(ObjectsTag==1)
        {             
             List<CountObject__c> t = [Select ID, Count__C from CountObject__c where Name = 'Account'];
             t[0].Count__c = TotalObjectsTag;
             update t;
             System.debug('ObjectsTag'+TotalObjectsTag);
        }
        
        if(ObjectsTag==2)
        {
             List<CountObject__c> t = [Select ID, Count__C from CountObject__c where Name = 'Contact'];
             t[0].Count__c = TotalObjectsTag;
             update t;
             System.debug('ObjectsTag'+TotalObjectsTag);
        }
        
        if(ObjectsTag==4)
        {
             List<CountObject__c> t = [Select ID, Count__C from CountObject__c where Name = 'Product'];
             t[0].Count__c = TotalObjectsTag;
             update t;
             System.debug('ObjectsTag'+TotalObjectsTag);
        }
        
        if(ObjectsTag==5)
        {
             List<CountObject__c> t = [Select ID, Count__C from CountObject__c where Name = 'Campaign'];
             t[0].Count__c = TotalObjectsTag;
             update t;
             System.debug('ObjectsTag'+TotalObjectsTag);
        }
        
         if(ObjectsTag==6)
        {
             List<CountObject__c> t = [Select ID, Count__C from CountObject__c where Name = 'CampaignMember'];
             t[0].Count__c = TotalObjectsTag;
             update t;
             System.debug('ObjectsTag'+TotalObjectsTag);
        }
        
        if(ObjectsTag==7)
        {
             List<CountObject__c> t = [Select ID, Count__C from CountObject__c where Name = 'PointHistory'];
             t[0].Count__c = TotalObjectsTag;
             update t;
             System.debug('ObjectsTag'+TotalObjectsTag);
        }
        
        if(ObjectsTag==8)
        {
            
            List<CountObject__c> t = [Select ID, Count__C from CountObject__c where Name = 'TransactionTds'];
             t[0].Count__c = TotalObjectsTag;
             update t;
             System.debug('ObjectsTag'+TotalObjectsTag);
        }





    
    }
}