@isTest
public class POAObjectiveTest {
    static testMethod void validatePOAObjective(){
        Event newEvent = new Event();
        RecordType rt = [Select id from RecordType where sobjecttype='Event' and Name='Customer Development Manager Call KR'];
        newEvent.RecordTypeId=rt.id;
        newEvent.Subject = 'Testing trigger';
        newEvent.StartDateTime=date.newinstance(2016, 1, 20);
        newEvent.EndDateTime=date.newinstance(2016, 1, 20);
        newEvent.Objective1__c='Price Related';
        newEvent.Objective2__c='Price Related';
        insert newEvent;
        system.assertEquals(newEvent.Objective1__c,'Price Related','success');        
    }
}