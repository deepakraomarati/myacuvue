/*
* File Name: MA2_BatchSingaporAssignBirthCouponTest
* Description : Test class for Batch Job of Coupon Expired 
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |8-Sep-2016  |hsingh53@its.jnj.com  |New Class created
* ================================================================
*    Ver  |Date         |Author                |Modification
*    2.0  |30-Aug-2017  |lbhutia@its.jnj.com   |Class Modified
*
*/
@isTest
public class PerformanceClassTest{
    
    /* Method for excuting the test class */
    static Testmethod void runBatchJobMethod(){
        
        final string Name = 'SGP';
        final string CountryCurrency = 'SGD';
        final string AccountId = '123456';
        final TriggerHandler__c cred = new TriggerHandler__c(Name = 'HandleTriggers' , 
                                                             GetTransactionTd__c = true, MA2_createUpdateTransaction__c = true, CouponContact__c= true, MA2_CouponInactiveTrigger__c = true,
                                                             MA2_CouponToApigeeTrigger__c= true, MA2_CouponWalletForApigee__c= true, MA2_createUpdateTransactionProd__c= true );
        insert cred;
        
        
        MA2_CountryCode__c CountryCode = new MA2_CountryCode__c(Name= Name, MA2_CountryCodeValue__c = Name);
        insert CountryCode;
        
        
        final MA2_Country_Currency_Map__c CCM = new MA2_Country_Currency_Map__c(Name = Name, Currency__c = Name);
        insert CCM;
        
        final Id Bday1xId = [select Id from RecordType where Name = 'Bonus Multiplier' and sObjectType = 'Coupon__c'].Id;        
        
        final Coupon__c coupon2 = new Coupon__c(MA2_CouponName__c = '1.5x Birthday Points',RecordTypeID = Bday1xId ,MA2_CountryCode__c =Name
                                                ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Campaign');  
        insert coupon2;
        
        final Coupon__c coupon3 = new Coupon__c(MA2_CouponName__c = '2x Birthday Points',RecordTypeID = Bday1xId ,MA2_CountryCode__c =Name
                                                ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Campaign');  
        insert coupon3;
        system.assertEquals(coupon3.MA2_CouponName__c, '2x Birthday Points');                           
        
        
        final Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id;
        
        final Account acc = new Account(Name = 'Test' , MA2_AccountId__c = AccountId ,AccountNumber = '54321',OutletNumber__c = '54321',ActiveYN__c = true,My_Acuvue__c = 'Yes',CountryCode__c = Name);
        insert acc;
        system.assertEquals(acc.Name, 'Test','Success');                           
        
        final Contact con2 = new Contact(LastName = 'test' ,MA2_AccountId__c = AccountId,MA2_ContactId__c = '23456',
                                         RecordTypeId  = consumerRecTypeId  ,
                                         AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = CountryCurrency, 
                                         MembershipNo__c = 'SGP123', MA2_Country_Code__c = Name, MA2_Grade__c='Base', DOB__c = date.newinstance(2012,06,12),
                                         MA2_PreAssessment__c = true);
        insert con2;
        
        final Contact con3 = new Contact(LastName = 'test' ,MA2_AccountId__c = AccountId,MA2_ContactId__c = '23456',
                                         RecordTypeId  = consumerRecTypeId  ,
                                         AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = CountryCurrency, 
                                         MembershipNo__c = 'SGP123', MA2_Country_Code__c = Name, MA2_Grade__c='VIP', DOB__c = date.newinstance(2012,06,12),
                                         MA2_PreAssessment__c = true);
        insert con3;
        
        final Contact con4 = new Contact(LastName = 'test' ,MA2_AccountId__c = AccountId,MA2_ContactId__c = '23456',
                                         RecordTypeId  = consumerRecTypeId  ,
                                         AccountId = acc.Id, email = 'test@gmail.com', phone = '123456789' ,CurrencyIsoCode = CountryCurrency, 
                                         MembershipNo__c = 'SGP123', MA2_Country_Code__c = Name, MA2_Grade__c='Base', DOB__c = date.newinstance(2012,07,12),
                                         MA2_PreAssessment__c = true);
        insert con4;
        
        con4.MA2_Grade__c='VIP';
        update con4;
        
        Test.startTest();
        
        Database.executeBatch(new TestPerformance(),10);
        
        Test.stopTest();
    }
}