@IsTest (SeeAllData=true)
public class PreventEventDeletion_TEST 
{
	static testMethod void PreventEventDeletion_TEST()
	{
		Profile p1 = [SELECT Id FROM Profile WHERE Name='ASPAC ANZ Country Champion'];
		User u1 = new User(Alias = 'test2', Email='standardtest2@testorg.com', EmailEncodingKey='UTF-8', LastName='Prasad', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p1.Id,	TimeZoneSidKey='America/Los_Angeles', UserName='test1@test.com.staging',Unique_User_Id__c = 'test2');
		insert u1;

		system.runas(u1)
		{
			Account acc = New Account(Name ='Test Account11',PublicAddress__c='Bangalore', SalesRep__c = 'test2', AccountNumber = '566622', OutletNumber__c = '693214', CountryCode__c = 'AUS',Subscribe__c=true);
			insert acc;

			event e= new event();
			e.StartDateTime = date.today();
			e.EndDateTime = date.today().addDays(2);
			e.Subject = 'Test';
			e.WhatId = acc.id ;
			insert e;

			//updating event
			e.CallOutcomeCallTargetComments__c='test';
			e.CallObjectiveAchieved__c='Fully Achieved';
			e.ActivityStatus__c='Completed';
			update e;

			//deleting event
			boolean checkError;

			Test.startTest();
			try 
			{
				delete e;
				checkError = false;
			}
			catch(Exception ex) 
			{
				checkError = true;
			}
			Test.stopTest();

			System.assert(true, 'Deletion Failed Appropriately' );
		}
	}
	
	static testMethod void PreventEventDeletion_TEST1()
	{
		Profile p2 = [SELECT Id FROM Profile WHERE Name='ASPAC ANZ CDM/KAM'];
		User u2 = new User(Alias = 'test2', Email='standardtest2@testorg.com', EmailEncodingKey='UTF-8', LastName='Prasad', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p2.Id,	TimeZoneSidKey='America/Los_Angeles', UserName='test1@test.com.staging',Unique_User_Id__c = 'test2');
		insert u2;

		system.runas(u2)
		{
			Account acc = New Account(Name ='Test Account11',PublicAddress__c='Bangalore', SalesRep__c = 'test2', AccountNumber = '566622', OutletNumber__c = '693214', CountryCode__c = 'AUS',Subscribe__c=true);
			insert acc;

			event e2= new event();
			e2.StartDateTime = date.today();
			e2.EndDateTime = date.today().addDays(2);
			e2.Subject = 'Test';
			e2.WhatId = acc.id ;
			insert e2;

			//updating event
			e2.CallOutcomeCallTargetComments__c='test';
			e2.CallObjectiveAchieved__c='Fully Achieved';
			e2.ActivityStatus__c='Completed';
			update e2;

			//deleting event
			boolean checkError;

			Test.startTest();
			try 
			{
				delete e2;
				checkError = false;
			}
			catch(Exception ex) 
			{
				checkError = true;
			}
			Test.stopTest();

			System.assert(true, 'Deletion Failed Appropriately' );
		}
	}
}