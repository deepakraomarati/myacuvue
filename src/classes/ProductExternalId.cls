public class ProductExternalId{
    public void insertproductexternalId(List<TradeHistory__c> tradeList){
        Map<TradeHistory__c,Set<String>> tradeHistoryProd = new Map<TradeHistory__c,Set<String>>();
        Set<String> prodNameList = new Set<String>();
        Set<String> prodNameUPCList = null;
        for(TradeHistory__c trans : tradeList){
                prodNameUPCList = new Set<String>();
                if(trans.ProductList__c != null){
                    if(trans.ProductList__c.contains(',')){
                        String[] arrayValue = trans.ProductList__c.split(',');
                        System.Debug('arrayValue ---'+arrayValue );
                        for(String rec : arrayValue){
                            prodNameList.add(rec);
                            prodNameUPCList.add(rec);  
                        } 
                    }else{
                        prodNameList.add(trans.ProductList__c);
                        prodNameUPCList.add(trans.ProductList__c);   
                    }    
                    tradeHistoryProd.put(trans,prodNameUPCList); 
                }
        }
        System.Debug('tradeHistoryProd--'+tradeHistoryProd);
        Map<String,Product2> prodMapKey = new map<String,Product2>();
            List<Product2> productList = [select Name,Id,ExternalId__c,UPC_Code__c,Price__c from Product2 where UPC_Code__c in: prodNameList
                                          and MA2_CountryCode__c = 'KOR'];
            System.Debug('productList--'+productList);
            for(Product2 prod : productList){
                    prodMapKey.put(prod.UPC_Code__c,prod); 
            }
            System.Debug('prodMapKey--'+prodMapKey);
            if(!prodMapKey.isEmpty()){
                for(TradeHistory__c trans : tradeList){
                    
                    /*for(String productCode :  prodMapKey.keySet()){
                        
                        if(productCode  != null || productCode  != 'null'){
                            System.Debug('productCode--'+productCode);
                            if(trans.ProductList__c.contains(productCode)){
                                System.Debug('prodMapKey.get(productCode)--'+prodMapKey.get(productCode));
                                List<Product2> prodList = prodMapKey.get(productCode);
                                if(prodList.size() > 0){
                                    for(Product2 prod : prodList){
                                        System.Debug('prod.ExternalId__c--'+prod.ExternalId__c);
                                        externalIdList = externalIdList  +   prod.ExternalId__c + ',';  
                                    }
                                    System.Debug('externalIdList --'+externalIdList);
                                    System.Debug('trans.MA2_ProductExternalId__c--'+trans.MA2_ProductExternalId__c);
                                }
                            }
                        }    
                    } */
                    if(tradeHistoryProd.containsKey(trans)){
                        Set<String> prodUPCList = new Set<String>();
                        prodUPCList.addAll(tradeHistoryProd.get(trans));
                        System.Debug('prodUPCList--'+prodUPCList);
                        String externalIdList = '';
                        if(prodUPCList.size() > 0){
                            for(String prod : prodUPCList){
                                if(prodMapKey.containsKey(prod)){
                                    externalIdList = externalIdList  +   prodMapKey.get(prod).ExternalId__c + ',';         
                                }    
                            } 
                            System.Debug('externalIdList--'+externalIdList); 
                            if(externalIdList != ''){
                                externalIdList = externalIdList.replaceAll('null,','');
                                externalIdList  = externalIdList.subString(0,externalIdList.length()-1);
                                trans.MA2_ProductExternalId__c = externalIdList;
                            }    
                        }
                    }
                }
            }   
            System.Debug('tradeList--'+tradeList); 
            
    }
}