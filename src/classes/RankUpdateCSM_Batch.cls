public class RankUpdateCSM_Batch implements Database.Batchable<sObject>
{
  //  String recId= 'a0k6F00000n3BCO';
   	String recId;
    //String query='select id from Customer_Segmentation_Master__c where ID=:recID';
    String query='select id from Customer_Segmentation_Master__c';
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        SET<ID> masterRecID= NEW SET<ID>();
        List<Customer_Segmentation_Master__c> masterRecords= new List<Customer_Segmentation_Master__c>();
        for(sobject rec: scope)
        {
            masterRecID.add(rec.ID);
        }
        RankUpdateCSM_handler.rankUpdateMethod(masterRecID);
    }
    
    public void finish(Database.BatchableContext BC)
    {
        
    }
}