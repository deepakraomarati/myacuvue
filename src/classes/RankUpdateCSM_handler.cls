public class RankUpdateCSM_handler {
   
    public static void rankUpdateMethod(SET<ID> masterRecID)
    {
        Map<ID,List<Customer_Segmentation_Matrix__c>> masterParentMap= new Map<ID,List<Customer_Segmentation_Matrix__c>>();
        LisT<Customer_Segmentation_Matrix__c> allChildRecords=[select id,New_Wearers_of_Beauty__c,New_Wearers_of_Daily__c,New_Wearers_of_Multifocal__c,New_wearers_of_Reusable__c,
                                                               New_Wearers_of_Toric__c,Ranking_for_Beauty__c,Ranking_for_Daily__c,Ranking_for_Multifocal__c,Ranking_for_Reusable__c,
                                                              Ranking_for_Toric__c,Customer_Segmentation_Master__c from Customer_Segmentation_Matrix__c where Customer_Segmentation_Master__c=: masterRecID 
                                                                ];
        system.debug('allChildRecords : '+allChildRecords);
        
        for(Customer_Segmentation_Matrix__c childRec: allChildRecords)
        {
            if(masterParentMap.containsKey(childRec.Customer_Segmentation_Master__c))
            {
                masterParentMap.get(childRec.Customer_Segmentation_Master__c).add(childRec);
            }
            else
            {
                List<Customer_Segmentation_Matrix__c> newChildRec =new List<Customer_Segmentation_Matrix__c>();
                newChildRec.add(childRec);
                masterParentMap.put(childRec.Customer_Segmentation_Master__c, newChildRec);
            }
        }        
        system.debug('map--> '+masterParentMap);
                
           for(ID childRecordID: masterParentMap.keySet() )
        {
            Integer countOfRank= masterParentMap.get(ChildRecordID).size();
            LIST<Decimal> rankCalList= new LIST<Decimal>();
            SET<Decimal> rankCalSet= new SET<Decimal>();
            system.debug('rankCalList-->'+rankCalList);
            
            for(Customer_Segmentation_Matrix__c child: masterParentMap.get(ChildRecordID))
            {
              if(child.New_Wearers_of_Beauty__c!= null || string.valueof(child.New_Wearers_of_Beauty__c)!='')
                {	
                       rankCalSet.add(child.New_Wearers_of_Beauty__c);
                }
            }
            rankCalList.addall(rankCalSet);
            rankCalList.sort();
            system.debug('rankCalList-->'+rankCalList);
            for(Decimal decVal: rankCalList)
            {
                for(Customer_Segmentation_Matrix__c child: masterParentMap.get(ChildRecordID))
                {
                    if(child.New_Wearers_of_Beauty__c== decVal)
                    {
                        system.debug('decVal-->'+decVal+'countOfRank -->'+countOfRank);
                        child.Ranking_for_Beauty__c=countOfRank;
                        countOfRank=countOfRank-1;
                        
                    }
                    
                }
            } 
        }
           for(ID childRecordID: masterParentMap.keySet() )
        {
            Integer countOfRank= masterParentMap.get(ChildRecordID).size();
            LIST<Decimal> rankCalList= new LIST<Decimal>();
            SET<Decimal> rankCalSet= new SET<Decimal>();
            system.debug('rankCalList-->'+rankCalList);
            
            for(Customer_Segmentation_Matrix__c child: masterParentMap.get(ChildRecordID))
            {
                 if(child.New_Wearers_of_Daily__c!= null || string.valueof(child.New_Wearers_of_Daily__c)!='')
                {	
                     rankCalSet.add(child.New_Wearers_of_Daily__c);
                }
            }
            rankCalList.addall(rankCalSet);
            rankCalList.sort();
            system.debug('rankCalList-->'+rankCalList);
            for(Decimal decVal: rankCalList)
            {
                for(Customer_Segmentation_Matrix__c child: masterParentMap.get(ChildRecordID))
                {
                    if(child.New_Wearers_of_Daily__c== decVal)
                    {
                        system.debug('decVal-->'+decVal+'countOfRank -->'+countOfRank);
                        child.Ranking_for_Daily__c=countOfRank;
                        countOfRank=countOfRank-1;
                        
                    }
                    
                }
            } 
        }
      
          for(ID childRecordID: masterParentMap.keySet() )
        {
            Integer countOfRank= masterParentMap.get(ChildRecordID).size();
            LIST<Decimal> rankCalList= new LIST<Decimal>();
            SET<Decimal> rankCalSet= new SET<Decimal>();
            system.debug('rankCalList-->'+rankCalList);
            
            for(Customer_Segmentation_Matrix__c child: masterParentMap.get(ChildRecordID))
            {
               if(child.New_Wearers_of_Multifocal__c!= null || string.valueof(child.New_Wearers_of_Multifocal__c)!='')
                {	
                   rankCalSet.add(child.New_Wearers_of_Multifocal__c);
                }
            }
            rankCalList.addall(rankCalSet);
            rankCalList.sort();
            system.debug('rankCalList-->'+rankCalList);
            for(Decimal decVal: rankCalList)
            {
                for(Customer_Segmentation_Matrix__c child: masterParentMap.get(ChildRecordID))
                {
                    if(child.New_Wearers_of_Multifocal__c== decVal)
                    {
                        system.debug('decVal-->'+decVal+'countOfRank -->'+countOfRank);
                        child.Ranking_for_Multifocal__c=countOfRank;
                        countOfRank=countOfRank-1;
                        
                    }
                    
                }
            } 
        }
       
                for(ID childRecordID: masterParentMap.keySet() )
        {
            Integer countOfRank= masterParentMap.get(ChildRecordID).size();
            LIST<Decimal> rankCalList= new LIST<Decimal>();
            SET<Decimal> rankCalSet= new SET<Decimal>();
            system.debug('rankCalList-->'+rankCalList);
            
            for(Customer_Segmentation_Matrix__c child: masterParentMap.get(ChildRecordID))
            {
                 if(child.New_wearers_of_Reusable__c!= null ||string.valueof(child.New_wearers_of_Reusable__c)!='')
                {	
                    rankCalSet.add(child.New_wearers_of_Reusable__c);
                }
               
            }
            rankCalList.addall(rankCalSet);
            rankCalList.sort();
            system.debug('rankCalList-->'+rankCalList);
            for(Decimal decVal: rankCalList)
            {
                for(Customer_Segmentation_Matrix__c child: masterParentMap.get(ChildRecordID))
                {
                    if(child.New_wearers_of_Reusable__c== decVal)
                    {
                        system.debug('decVal-->'+decVal+'countOfRank -->'+countOfRank);
                        child.Ranking_for_Reusable__c=countOfRank;
                        countOfRank=countOfRank-1;
                        
                    }
                    
                }
            } 
        }
        for(ID childRecordID: masterParentMap.keySet() )
        {
            Integer countOfRank= masterParentMap.get(ChildRecordID).size();
            LIST<Decimal> rankCalList= new LIST<Decimal>();
            SET<Decimal> rankCalSet= new SET<Decimal>();
            system.debug('rankCalList-->'+rankCalList);
            
            for(Customer_Segmentation_Matrix__c child: masterParentMap.get(ChildRecordID))
            {
                if(child.New_Wearers_of_Toric__c!= null ||string.valueof(child.New_Wearers_of_Toric__c)!='')
                {	
                    rankCalSet.add(child.New_Wearers_of_Toric__c);
                }
            }
            rankCalList.addall(rankCalSet);
            rankCalList.sort();
            system.debug('rankCalList-->'+rankCalList);
            for(Decimal decVal: rankCalList)
            {
                for(Customer_Segmentation_Matrix__c child: masterParentMap.get(ChildRecordID))
                {
                    if(child.New_Wearers_of_Toric__c== decVal)
                    {
                        system.debug('decVal-->'+decVal+'countOfRank -->'+countOfRank);
                        child.Ranking_for_Toric__c=countOfRank;
                        countOfRank=countOfRank-1;
                        
                    }
                    
                }
            } 
        }
        
        List<Customer_Segmentation_Matrix__c> updatedRecords = new List<Customer_Segmentation_Matrix__c>();
        
        for(ID childRecordID: masterParentMap.keySet())
        {
            for(Customer_Segmentation_Matrix__c child: masterParentMap.get(ChildRecordID))
            {
                child.Last_Refreshed_Date__c=system.now();
                updatedRecords.add(child);
            }
        }
        
        update updatedRecords;
    }

}