@isTest
public class RankUpdateCSM_handler_test {
    static testMethod void Rankupdate() {
        Account acc= new Account(name='Account new',AccountNumber='32434');
        insert acc;
        
        Customer_Segmentation_Master__c cm = new Customer_Segmentation_Master__c();
        cm.Name = 'testMaster';
        insert cm;
        Customer_Segmentation_Matrix__c csm = new Customer_Segmentation_Matrix__c(Customer_Segmentation_Master__c=cm.id, What_is_the_exact_number__c=100,AccountName__c=acc.ID,of_total_fits_Toric__c='20%',What_of_total_fits_Reusable__c='30%',What_of_total_fits_Multifocal__c='60%',What_of_total_fits_Daily__c='70%',What_of_total_fits_Beauty__c='90%' );
        insert csm;
        system.assertEquals(csm.What_is_the_exact_number__c,100,'success');
        Database.ExecuteBatch(new RankUpdateCSM_Batch());
    }
}