/**
 * File Name: RegistrationService
 * Author: Sathishkumar | BICSGLBOAL
 * Date Last Modified:  26-Oct-2018
 * Description: Allow JJVPRO user to self register. when this class is invoked system  will
 *              create a new Community User and set permissions
 * Copyright (c) $2018 Johnson & Johnson
 */
@RestResource(URLMapping='/apex/businessRegistration/*')
global without sharing class RegistrationService
{
	global static final String GENERIC_ERROR = 'GEN0001';
	global static final String ERROR_TYPE = 'RegisterUser';

	/**
	* @description  register new user with the given inputs
	*/
	@HTTPPost
	global static String registerNewUser()
	{
		Savepoint changes = Database.setSavepoint();
		try
		{
			RestRequest req = RestContext.request;
			RegDTO regdata = parse(req.requestBody.tostring());
			Boolean isRegFirstUser = false;
			Boolean IsAccountOwner = false;
			List<Account> accountList = new List<Account>();
			validateUserDetails(regdata);
			String AccountId = setPractice(regdata);
			List<AccountContactRole__c> accountContactRoleList = new List<AccountContactRole__c>();
			JJVC_Contact_Recordtype__mdt rectype = [SELECT MasterLabel FROM JJVC_Contact_Recordtype__mdt WHERE DeveloperName = 'HCP_Contacts_RecordtypeId'];
			ID HCPRecordTypeId = rectype.MasterLabel;
			if (AccountId == '' || AccountId == null)
			{
				AccountId = Portal_Settings__c.getValues('GenericAccount Id').Value__c;
			}
			else
			{
				accountList =
				[
						SELECT id,
								Name,
								Email__c,
								OutletNumber__c,
								BillingStreet,
								BillingCity,
								BillingState,
								BillingPostalCode,
								BillingCountry,
								ShippingStreet,
								ShippingCity,
								ShippingState,
								ShippingPostalCode,
								ShippingCountry
						FROM Account
						WHERE ID = :AccountId
				];
				if (accountList.isEmpty())
				{
					throw new customexception(B2B_Utils.getException('PC0017', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0017').Error_Description__c)));
				}
				accountContactRoleList =
				[
						SELECT Contact__c
						FROM AccountContactRole__c
						WHERE Account__c = :AccountId and
						Account_Ownership_Flag__c = true and
						Contact__r.Recordtypeid = :HCPRecordTypeId
				];
				// If Contact Email matches with Account email address then user set as account owner
				if (accountContactRoleList.size() > 0)
				{
					IsAccountOwner = (accountContactRoleList[0].Account_Ownership_Flag__c ? true : false);
				}
				AccountId = accountList[0].Id;
			}
			List<Role__c> roleList = [SELECT Id, Name, Country__c, Type__c FROM Role__c WHERE Name = :regdata.Role];
			Map<String, Email_Triggersends__c> emailTriggerSendKeyMap = Email_Triggersends__c.getAll();
			Email_Triggersends__c regWelcomeEmailKey = emailTriggerSendKeyMap.get(regdata.UserLocale);
			checkRoleAndManagerExist(roleList, accountContactRoleList, AccountId);
			Contact contact = insertContact(regdata, AccountId, HCPRecordTypeId, roleList[0].Id);
			User user = insertUser(regdata, accountList[0], contact, roleList[0]);
			if (user == null)
			{
				throw new CustomException(B2B_Utils.getException(GENERIC_ERROR, ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('GEN0001').Error_Description__c)));
			}
			if (AccountId == null || contact.AccountId == Portal_Settings__c.getValues('GenericAccount Id').Value__c)
			{
				if (roleList[0].Type__c == 'Student')
				{
					ET_EmailAdministration.sendEmail(user.Email, regWelcomeEmailKey.Reg_welcome_student__c, user.firstname, '', '');
				}
				else
				{
					ET_EmailAdministration.sendEmail(user.Email, regWelcomeEmailKey.Reg_welcome_email_no_practice__c, user.firstname, '', '');
				}
			}
			else
			{
				List<AccountContactRole__c> AccountContactRole =
				[
						SELECT Account__c,
								Contact__c,
								Account_Ownership_Flag__c,
								Account__r.Name,
								Account__r.Ownerid,
								Account__r.Owner.Name,
								Account__r.Owner.Email,
								Account__r.Phone,
								Account__r.BillingPostalCode,
								Account__r.BillingStreet,
								Account__r.BillingCity,
								Account__r.BillingState,
								Account__r.BillingCountry,
								Primary__c
						FROM AccountContactRole__c
						WHERE Contact__c = :contact.Id and Account__c = :AccountId
				];
				if (AccountContactRole[0].Account_Ownership_Flag__c)
				{
					IsAccountOwner = true;
					AccountContactRole[0].Functions__c = 'Ordering;Rewards;Administrator;ECPLocator';
					AccountContactRole[0].Approval_Status__c = 'Accepted';
					isregfirstuser = true;
				}
				else
				{
					AccountContactRole[0].Approval_Status__c = 'Pending Approval';
				}
				update AccountContactRole[0];
				if (IsAccountOwner == true)
				{
					ET_EmailAdministration.sendEmail(user.Email, regWelcomeEmailKey.Reg_welcome_practice_linked__c, user.firstname, accountList[0].Name, '');
				}
				else
				{
					if (isregfirstuser)
					{
						ET_EmailAdministration.sendEmail(user.Email, regWelcomeEmailKey.Reg_welcome_practice_linked__c, user.firstname, accountList[0].Name, '');
						ET_EmailAdministration.sendEmail(accountList[0].Email__c, regWelcomeEmailKey.First_user_reg_notify_owner__c, user.firstname, accountList[0].Name, '');
					}
				}
			}
			if (user.Id != null)
			{
				Userresult usr = new Userresult('Success', user.Id);
				return json.serialize(usr);
			}
			return null;
		}
		catch (Exception e)
		{
			Database.rollback(changes);
			return e.getMessage();
		}
	}

	/**
	 * @description: This method validates the given parameters are fine to create an user or contact.
	 * @throw: Throw an specific error based on parameters values provided by user.
	 */
	private static void validateUserDetails(RegDTO reg)
	{
		if (reg.LastName == '' || reg.LastName == null)
		{
			throw new customexception(B2B_Utils.getException('PC0006', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0006').Error_Description__c)));
		}
		if ((reg.Username == '' || reg.Username == null) || (reg.EmailAddress == '' || reg.EmailAddress == null))
		{
			throw new customexception(B2B_Utils.getException('PC0015', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0015').Error_Description__c)));
		}
		if (reg.EmailAddress != null && reg.EmailAddress != '')
		{
			List<User> userExist = new List<User>();
			userExist = [SELECT Id,Username,Email FROM User WHERE Email = :reg.EmailAddress];
			if (userExist.size() > 0)
			{
				throw new customexception(B2B_Utils.getException('PC0001', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0001').Error_Description__c)));
			}
		}
		if (B2B_Utils.IsValidPassword(reg.Password))
		{
			throw new CustomException(B2B_Utils.getException('PC0003', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0003').Error_Description__c)));
		}
		if ((reg.Username != '' && reg.Username != null) || (reg.LastName != '' && reg.LastName != null) || (reg.FirstName != '' && reg.FirstName != null))
		{
			String pwuser = reg.Username.split('@')[0];
			if (reg.Password.containsIgnoreCase(reg.LastName) || reg.Password.containsIgnoreCase(reg.FirstName) || reg.Password.containsIgnoreCase(pwuser))
			{
				throw new customexception(B2B_Utils.getException('PC0002', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0002').Error_Description__c)));
			}
		}
	}

	/**
	 * @description: This method validates the given role is available in salesforce and manager already exist for the given practice id.
	 * @throw: Throw an specific error if role is not exist or manager is already registered on the practice.
	 */
	private static void checkRoleAndManagerExist(List<Role__c> sRole, List<AccountContactRole__c> ac, Id AccountId)
	{
		if (sRole.Isempty())
		{
			throw new customexception(B2B_Utils.getException('PC0016', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0016').Error_Description__c)));
		}
		if (sRole[0].Type__c == 'Manager' && ac.size() > 0)
		{
			throw new customexception(B2B_Utils.getException('PC0020', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0020').Error_Description__c)));
		}
		if (sRole[0].Type__c != 'Manager' && AccountId != Portal_Settings__c.getValues('GenericAccount Id').Value__c && ac.size() <= 0)
		{
			throw new customexception(B2B_Utils.getException('PC0012', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0012').Error_Description__c)));
		}
	}

	private static String setPractice(RegDTO reg)
	{
		String PracticeId;
		if (reg.Practices != null)
		{
			try
			{
				if (reg.Practices.isempty())
				{
					PracticeId = Portal_Settings__c.getValues('GenericAccount Id').Value__c;
				}
				else
				{
					if (reg.Practices[0].id == null || reg.Practices[0].id == '')
					{
						PracticeId = Portal_Settings__c.getValues('GenericAccount Id').Value__c;
					}
					else
					{
						PracticeId = reg.Practices[0].Id;

					}
				}
				return PracticeId;
			}
			catch (Exception e)
			{
				if (e.getMessage().contains('Invalid id:'))
				{
					return B2B_Utils.getException('PC0017', ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('PC0017').Error_Description__c));
				}
			}
		}
		return null;
	}

	/**
	 * @description: This method will create a contact based on given parameters.
	 * @return: This method retrun the newly created contact details
	 */
	private static Contact insertContact(RegDTO reg, Id AccountId, Id RecordTypeId, ID RoleId)
	{
		Contact cc = new Contact();
		cc.FirstName = reg.FirstName;
		cc.LastName = reg.LastName;
		cc.Salutation = reg.Salutation;
		cc.Email = reg.EmailAddress;
		cc.School_Name__c = reg.SchoolName;
		cc.Graduation_Year__c = reg.GraduationYear;
		cc.Degree__c = reg.Degree;
		// Convert String to Date format
		if (reg.DateofBirth != null && reg.DateofBirth != '')
		{
			List<String> dtStr = reg.DateofBirth.split('/');
			String dt = dtStr[2] + '-' + dtStr[0] + '-' + dtStr[1];
			cc.BirthDate = date.Valueof(dt);
		}
		cc.AccountId = AccountId;
		cc.RecordTypeId = RecordTypeId;
		if (reg.IsTestRecord != null)
		{
			cc.is_Test_User__c = reg.IsTestRecord;
		}
		if (cc.AccountId != Portal_Settings__c.getValues('GenericAccount Id').Value__c)
		{
			cc.MailingStreet = reg.Practices[0].Street;
			cc.MailingCity = reg.Practices[0].City;
			cc.MailingPostalCode = reg.Practices[0].PostalCode;
			cc.MailingState = reg.Practices[0].State;
			cc.MailingCountry = reg.Practices[0].Country;
		}
		cc.user_Role__c = RoleId;
		insert cc;
		return cc;
	}

	/**
	 * @description: This method will create a community user based on given parameters.
	 * @return: This method retrun the newly created user record
	 * @throw: Throw an generic error in case of any issues.
	 */
	private static User insertUser(RegDTO reg, Account account, Contact contact, Role__c role)
	{
		User u = new User();
		u.Title = reg.Salutation;
		u.FirstName = reg.FirstName;
		u.LastName = reg.LastName;
		u.Email = reg.EmailAddress;
		u.UserName = reg.Username;
		if (Role__c.Name != null)
		{
			u.Occupation__c = role.Name;
		}
		u.Secret_Question__c = reg.SecretQuestion;
		u.SecretAnswerSalt__c = RegistrationService.generateSalt();
		Blob hash = Crypto.generateDigest('SHA-512', Blob.valueOf(reg.SecretAnswer + u.SecretAnswerSalt__c));
		u.Secret_Answer__c = EncodingUtil.base64Encode(hash);
		if (reg.CommunicationAggrement != null)
		{
			u.Communication_Agreement__c = reg.CommunicationAggrement;
		}
		u.Localesidkey = reg.UserLocale;
		u.Languagelocalekey = reg.UserLanguage;
		u.EmailEncodingKey = 'ISO-8859-1';
		u.Alias = (reg.LastName.length() > 7) ? reg.LastName.substring(0, 7) : reg.LastName;
		u.TimeZoneSidKey = reg.Timezone;
		u.CommunityNickname = B2B_Utils.generateGUID();
		u.NPINumber__c = reg.NPINumber;
		u.ContactId = contact.Id;
		u.Unique_User_Id__c = contact.Id;
		u.ProfileId = Portal_Settings__c.getValues('ProfileId').Value__c;
		if (contact.AccountId != Portal_Settings__c.getValues('GenericAccount Id').Value__c)
		{
			if (account.ShippingStreet != null && account.ShippingStreet != '')
			{
				u.Street = account.ShippingStreet;
			}
			if (account.ShippingCity != null && account.ShippingCity != '')
			{
				u.City = account.ShippingCity;
			}
			if (account.ShippingState != null && account.ShippingState != '')
			{
				u.State = account.ShippingState;
			}
			if (account.ShippingPostalCode != null && account.ShippingPostalCode != '')
			{
				u.Postalcode = account.ShippingPostalCode;
			}
			if (account.ShippingCountry != null && account.ShippingCountry != '')
			{
				u.Country = account.ShippingCountry;
			}
		}
		try
		{
			insert u;
			system.setpassword(u.Id, reg.password);
			return u;
		}
		catch (exception e)
		{
			if (e.getMessage().contains('Your request cannot be processed'))
			{
				//throw new CustomException(B2B_Utils.getException(GENERIC_ERROR, ERROR_TYPE, (B2B_Custom_Exceptions__c.getValues('GEN0001').Error_Description__c)));
				return null;
			}
		}
		return null;
	}

	public static String generateSalt()
	{
		Blob b = Crypto.GenerateAESKey(128);
		String h = EncodingUtil.ConvertTohex(b);
		return h.SubString(0, 30);
	}
	public class Userresult
	{
		public string Status { get; set; }
		public id UserId { get; set; }
		public Userresult(string status, id userid)
		{
			this.Status = status;
			this.UserId = userid;
		}
	}
	public static RegDTO parse(string reg)
	{
		return(RegDTO) System.json.deserialize(reg, RegDTO.class);
	}
	public class RegDTO
	{
		String Salutation { get; set; }
		String FirstName { get; set; }
		String LastName { get; set; }
		String EmailAddress { get; set; }
		String Username { get; set; }
		String Password { get; set; }
		String Role { get; set; }
		Boolean CommunicationAggrement { get; set; }
		String SecretQuestion { get; set; }
		String SecretAnswer { get; set; }
		String NPINumber { get; set; }
		String DateofBirth { get; set; }
		List <Practices> Practices { get; set; }
		String UserLocale { get; set; }
		String UserLanguage { get; set; }
		String Timezone { get; set; }
		String SchoolName { get; set; }
		String GraduationYear { get; set; }
		String Degree { get; set; }
		Boolean IsTestRecord { get; set; }
	}
	public class Practices
	{
		String Id { get; set; }
		String Name { get; set; }
		String SAPAccountNumber { get; set; }
		Boolean AccountOwnerShip { get; set; }
		String Street { get; set; }
		String City { get; set; }
		String PostalCode { get; set; }
		String Country { get; set; }
		String State { get; set; }
		String Phone { get; set; }
		public Practices(string Id, string Name, string SAPAccountNumber, Boolean AccountOwnerShip, string Street, string City, string PostalCode, string Country, String State, String Phone)
		{
			this.Id = Id;
			this.Name = Name;
			this.SAPAccountNumber = SAPAccountNumber;
			this.AccountOwnerShip = AccountOwnerShip;
			this.Street = Street;
			this.City = City;
			this.PostalCode = PostalCode;
			this.Country = Country;
			this.State = State;
			this.Phone = Phone;
		}
	}
}