/**
* File Name: TestRegistrationService
* Author: Sathishkumar | BICSGLBOAL
* Date Last Modified:  18-Oct-2018
* Description : Test class for RegistrationService class
* Copyright (c) $2018 Johnson & Johnson
*/
@isTest(SeeAlldata=false)
public class RegistrationServiceTest
{
	static testmethod void registerUserWithoutPractice()
	{
		List<sObject> b2bCustomExceptions = Test.loadData(B2B_Custom_Exceptions__c.sObjectType, 'B2BCustomexceptions');
		List<sObject> portalSettings = Test.loadData(Portal_Settings__c.sObjectType, 'portalsettings');
		List<sObject> emailTriggerSends = Test.loadData(Email_Triggersends__c.sObjectType, 'ET_Triggersends');

		Account genericAccount = new Account();
		genericAccount.Name = 'Generic consumer Account';
		genericAccount.shippingcountry = 'JAPAN';
		genericAccount.shippingstreet = 'Test Street';
		genericAccount.shippingcity = 'Test city';
		genericAccount.shippingstate = 'AICHI KEN';
		genericAccount.shippingpostalcode = 'TEST20017';
		insert genericAccount;

		Portal_Settings__c portalCustomSettings = new Portal_Settings__c();
		portalCustomSettings.Name = 'GenericAccount Id';
		portalCustomSettings.Value__c = genericAccount.Id;
		insert portalCustomSettings;

		Role__c role = new Role__c();
		role.Name = 'Store Manager';
		role.Approval_Required__c = true;
		role.Country__c = 'Japan';
		role.Type__c = 'Manager';
		insert role;

		User user = [SELECT ID,Username,email,name FROM User WHERE name = 'B2B Site Guest User'];
		system.runas(user)
		{
			/*All data correct*/
			RestRequest req = new RestRequest();
			RestResponse res = new RestResponse();
			String jsonRequest = '{"Salutation":"Mr.","FirstName":"Acuvue","LastName":"Pro test user","EmailAddress":"testregprouser+1@gmail.com","Username":"testregprouser+1@gmail.com","Password":"DummyPassword1","Role":"Store Manager","CommunicationAggrement":true,"GraduationYear":"2018","Degree":"Test Degree","Practices":[],"UserLocale":"ja_JP","UserLanguage":"ja","DateofBirth":"09/09/1992","SecretQuestion":"favourite_car?","SecretAnswer":"swift","Timezone":"America/New_York","SchoolName":"Test School","NPINumber":"TESTPRO2018"}';
			req.requestURI = 'b2b/services/apexrest/apex/businessRegistration/';
			req.httpMethod = 'POST';
			req.requestbody = blob.valueof(jsonRequest);
			RestContext.request = req;
			RestContext.response = res;
			string response = RegistrationService.registerNewUser().remove('[');
			except exception0 = parse(response);
			system.assertequals(exception0.Status, 'Success');

			/*User already exist validation*/
			RestRequest req1 = new RestRequest();
			RestResponse res1 = new RestResponse();
			String jsonRequest1 = '{"Salutation":"Mr.","FirstName":"Acuvue","LastName":"Pro test user","EmailAddress":"testregprouser+1@gmail.com","Username":"testregprouser+1@gmail.com","Password":"DummyPassword1","Role":"Store Manager","CommunicationAggrement":true,"GraduationYear":"2018","Degree":"Test Degree","Practices":[],"UserLocale":"ja_JP","UserLanguage":"ja","DateofBirth":"09/09/1992","SecretQuestion":"favourite_car?","SecretAnswer":"swift","Timezone":"America/New_York","SchoolName":"Test School","NPINumber":"TESTPRO2018"}';
			req1.requestURI = 'b2b/services/apexrest/apex/businessRegistration/';
			req1.httpMethod = 'POST';
			req1.requestbody = blob.valueof(jsonRequest1);
			RestContext.request = req1;
			RestContext.response = res1;
			string response1 = RegistrationService.registerNewUser().remove('[');
			except exception1 = parse(response1.remove(']'));
			system.assertequals(exception1.errorCode, 'PC0001');
			/*Ends here*/

			/*Last name validation*/
			RestRequest req2 = new RestRequest();
			RestResponse res2 = new RestResponse();
			String jsonRequest2 = '{"Salutation":"Mr.","FirstName":"Acuvue","LastName":"","EmailAddress":"testregprouser+2@gmail.com","Username":"testregprouser+2@gmail.com","Password":"DummyPassword1","Role":"Store Manager","CommunicationAggrement":true,"GraduationYear":"2018","Degree":"Test Degree","Practices":[],"UserLocale":"ja_JP","UserLanguage":"ja","DateofBirth":"09/09/1992","SecretQuestion":"favourite_car?","SecretAnswer":"swift","Timezone":"America/New_York","SchoolName":"Test School","NPINumber":"TESTPRO2018"}';
			req2.requestURI = 'b2b/services/apexrest/apex/businessRegistration/';
			req2.httpMethod = 'POST';
			req2.requestbody = blob.valueof(jsonRequest2);
			RestContext.request = req2;
			RestContext.response = res2;
			string response2 = RegistrationService.registerNewUser().remove('[');
			except exception2 = parse(response2.remove(']'));
			system.assertequals(exception2.errorCode, 'PC0006');
			/*Ends here*/

			/*Email or username validation*/
			RestRequest req3 = new RestRequest();
			RestResponse res3 = new RestResponse();
			String jsonRequest3 = '{"Salutation":"Mr.","FirstName":"Acuvue","LastName":"Pro test user","EmailAddress":"testregprouser+2@gmail.com","Username":"","Password":"DummyPassword1","Role":"Store Manager","CommunicationAggrement":true,"GraduationYear":"2018","Degree":"Test Degree","Practices":[],"UserLocale":"ja_JP","UserLanguage":"ja","DateofBirth":"09/09/1992","SecretQuestion":"favourite_car?","SecretAnswer":"swift","Timezone":"America/New_York","SchoolName":"Test School","NPINumber":"TESTPRO2018"}';
			req3.requestURI = 'b2b/services/apexrest/apex/businessRegistration/';
			req3.httpMethod = 'POST';
			req3.requestbody = blob.valueof(jsonRequest3);
			RestContext.request = req3;
			RestContext.response = res3;
			string response3 = RegistrationService.registerNewUser().remove('[');
			except exception3 = parse(response3.remove(']'));
			system.assertequals(exception3.errorCode, 'PC0015');
			/*Ends here*/

			/*Password validation*/
			RestRequest req4 = new RestRequest();
			RestResponse res4 = new RestResponse();
			String jsonRequest4 = '{"Salutation":"Mr.","FirstName":"Acuvue","LastName":"Pro test user","EmailAddress":"testregprouser+2@gmail.com","Username":"testregprouser+2@gmail.com","Password":"Dumm","Role":"Store Manager","CommunicationAggrement":true,"GraduationYear":"2018","Degree":"Test Degree","Practices":[],"UserLocale":"ja_JP","UserLanguage":"ja","DateofBirth":"09/09/1992","SecretQuestion":"favourite_car?","SecretAnswer":"swift","Timezone":"America/New_York","SchoolName":"Test School","NPINumber":"TESTPRO2018"}';
			req4.requestURI = 'b2b/services/apexrest/apex/businessRegistration/';
			req4.httpMethod = 'POST';
			req4.requestbody = blob.valueof(jsonRequest4);
			RestContext.request = req4;
			RestContext.response = res4;
			string response4 = RegistrationService.registerNewUser().remove('[');
			except exception4 = parse(response4.remove(']'));
			system.assertequals(exception4.errorCode, 'PC0003');

			RestRequest req5 = new RestRequest();
			RestResponse res5 = new RestResponse();
			String jsonRequest5 = '{"Salutation":"Mr.","FirstName":"Acuvue1","LastName":"Pro test user","EmailAddress":"testregprouser+2@gmail.com","Username":"testregprouser+2@gmail.com","Password":"Acuvue123","Role":"Store Manager","CommunicationAggrement":true,"GraduationYear":"2018","Degree":"Test Degree","Practices":[],"UserLocale":"ja_JP","UserLanguage":"ja","DateofBirth":"09/09/1992","SecretQuestion":"favourite_car?","SecretAnswer":"swift","Timezone":"America/New_York","SchoolName":"Test School","NPINumber":"TESTPRO2018"}';
			req5.requestURI = 'b2b/services/apexrest/apex/businessRegistration/';
			req5.httpMethod = 'POST';
			req5.requestbody = blob.valueof(jsonRequest5);
			RestContext.request = req5;
			RestContext.response = res5;
			string response5 = RegistrationService.registerNewUser().remove('[');
			except exception5 = parse(response5.remove(']'));
			system.assertequals(exception5.errorCode, 'PC0002');
			/*Ends here*/

			/*Invalid Role validation*/
			RestRequest req6 = new RestRequest();
			RestResponse res6 = new RestResponse();
			String jsonRequest6 = '{"Salutation":"Mr.","FirstName":"Acuvue","LastName":"Pro test user","EmailAddress":"testregprouser+2@gmail.com","Username":"testregprouser+2@gmail.com","Password":"DummyPassword123","Role":"Technic","CommunicationAggrement":true,"GraduationYear":"2018","Degree":"Test Degree","Practices":[],"UserLocale":"ja_JP","UserLanguage":"ja","DateofBirth":"09/09/1992","SecretQuestion":"favourite_car?","SecretAnswer":"swift","Timezone":"America/New_York","SchoolName":"Test School","NPINumber":"TESTPRO2018"}';
			req6.requestURI = 'b2b/services/apexrest/apex/businessRegistration/';
			req6.httpMethod = 'POST';
			req6.requestbody = blob.valueof(jsonRequest6);
			RestContext.request = req6;
			RestContext.response = res6;
			string response6 = RegistrationService.registerNewUser().remove('[');
			except exception6 = parse(response6.remove(']'));
			system.assertequals(exception6.errorCode, 'PC0016');
			/*Ends here*/
		}
	}

	static testmethod void registerUserWithPractice()
	{
		List<sObject> b2bCustomExceptions = Test.loadData(B2B_Custom_Exceptions__c.sObjectType, 'B2BCustomexceptions');
		List<sObject> portalSettings = Test.loadData(Portal_Settings__c.sObjectType, 'portalsettings');
		List<sObject> emailTriggerSends = Test.loadData(Email_Triggersends__c.sObjectType, 'ET_Triggersends');

		Account genericAccount = new Account();
		genericAccount.Name = 'Generic consumer Account';
		genericAccount.shippingcountry = 'JAPAN';
		genericAccount.shippingstreet = 'Test Street';
		genericAccount.shippingcity = 'Test city';
		genericAccount.Shippingstate = 'AICHI KEN';
		genericAccount.shippingpostalcode = 'TEST20017';
		insert genericAccount;

		Account account = new Account();
		account.Name = 'Acuvue PRO test Acccount';
		account.shippingcountry = 'JAPAN';
		account.shippingstreet = 'Test Street';
		account.shippingcity = 'Test city';
		account.shippingpostalcode = 'TEST20017';
		account.OutletNumber__c = '0003696895';
		account.shippingstate = 'AICHI KEN';
		account.Phone = '9854845545';
		insert account;

		Portal_Settings__c portalCustomSettings = new Portal_Settings__c();
		portalCustomSettings.Name = 'GenericAccount Id';
		portalCustomSettings.Value__c = genericAccount.Id;
		insert portalCustomSettings;

		Role__c role = new Role__c();
		role.Name = 'Store Manager';
		role.Approval_Required__c = true;
		role.Country__c = 'Japan';
		role.Type__c = 'Manager';
		insert role;

		User user = [SELECT ID, Username, email, name FROM User WHERE name = 'B2B Site Guest User'];
		system.runas(user)
		{
			RegistrationService.Practices practiceData = new RegistrationService.Practices('ID', 'Name', '0003698741', true, 'street', 'city', 'postalcode', 'country', 'state', 'phone');
			RestRequest req1 = new RestRequest();
			RestResponse res1 = new RestResponse();
			string practice = '{"Id":"' + account.id + '","Name":"' + account.Name + '","SAPAccountNumber":"' + account.OutletNumber__c + '","AccountOwnerShip":true,"Street":"' + account.shippingstreet + '","City":"' + account.shippingcity + '","PostalCode":"' + account.Shippingpostalcode + '","Country":"' + account.shippingcountry + '","State":"' + account.shippingstate + '","Phone":"' + account.phone + '"}';
			String jsonRequest1 = '{"Salutation":"Mr.","FirstName":"Acuvue","LastName":"Pro test user","EmailAddress":"testregprouser+3@gmail.com","Username":"testregprouser+1@gmail.com","Password":"DummyPassword1","Role":"Store Manager","CommunicationAggrement":true,"GraduationYear":"2018","Degree":"Test Degree","Practices":[' + practice + '],"UserLocale":"ja_JP","UserLanguage":"ja","DateofBirth":"09/09/1992","SecretQuestion":"favourite_car?","SecretAnswer":"swift","Timezone":"America/New_York","SchoolName":"Test School","NPINumber":"TESTPRO2018"}';
			req1.requestURI = 'b2b/services/apexrest/apex/businessRegistration/';
			req1.httpMethod = 'POST';
			req1.requestbody = blob.valueof(jsonRequest1);
			RestContext.request = req1;
			RestContext.response = res1;
			String response1 = RegistrationService.registerNewUser();
			except exception1 = parse(response1);
			system.assertequals(exception1.Status, 'Success');
		}
	}
	public class except
	{
		String errorMessage;
		String errorCode;
		String errorType;
		String status;
		ID UserId;
	}

	public static except parse(String reg)
	{
		return(except) System.json.deserialize(reg, except.class);
	}
}