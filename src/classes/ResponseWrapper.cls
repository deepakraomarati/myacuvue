/**
 * File Name: ResponseWrapper 
 * Description : Create custom JSON response
 * Copyright : Johnson & Johnson
 * @author : Shobana K G | skg6@its.jnj.com | Shobana.KG@cognizant.com
 * 
 * Modification Log 
 * =============================================================== 
 *  Ver  |Date         |Author                |Modification
 *  1.0  |22-May-2018  |skg6@its.jnj.com      |New Class created
 *  1.1  |13-Jul-2018  |skg6@its.jnj.com      |Include separate logic for constructing error and success response
 */
public with sharing class ResponseWrapper {
    
    // Get custom setting - CustomException__c records
    public static Map<String,CustomException__c> ExpMap = new Map<String,CustomException__c> (CustomException__c.getAll());
    
    // Inner wrapper class for constructing error response
    class ErrorResponseWrapper {
    	// Properties
        public String message{get;set;}
        public String errorCode{get;set;}
        public String errorType{get;set;}
        
        // Constructor
		public ErrorResponseWrapper(String fields, String errorCode, String errorType) {
        	this.message = ExpMap.get(errorCode).Error_Description__c + ((fields != null) ? (': ' + fields) : '');
            this.errorCode = errorCode;
            this.errorType = errorType;
    	}           
    }
    
    // Inner wrapper class for constructing success response
    class SuccessResponseWrapper {
    	// Properties
    	public String statusCode{get;set;}
        public String status{get;set;}
        public String contactId{get;set;}
        public String campaignMemberId{get;set;}
        
        // Constructor
    	public SuccessResponseWrapper(String statusCode, String status, String contactId, String campaignMemberId) {
        	this.statusCode = statusCode;
            this.status = status;
            this.contactId = contactId;
            this.campaignMemberId = campaignMemberId;
    	}        
    }
    
    /**
	 * Description - Create Error JSON Response
	 * Input - res - Response
	 *  	   fields - List of fields to include in response for specific error message
	 *  	   errorCode, errorType - Code and type of error to include in response 
	 * Output - JSON response
	 */
    public static void createErrorResponse(RestResponse res, String fields, String errorCode, String errorType) {
        RestResponse resp = res;
        resp.addHeader('Content-Type', 'application/json');
		resp.responseBody = Blob.valueOf(JSON.serialize(new ErrorResponseWrapper(fields, errorCode, errorType)));
    }
    
    /**
	 * Description - Create Success JSON Response
	 * Input - res - Response
	 *  	   statusCode, status - code and status to include in response
	 *         contactId, campaignMemberId - Id of Contact and CampaignMember created
	 * Output - JSON response
	 */
    public static void createSuccessResponse(RestResponse res, String statusCode, String status, String contactId, String campaignMemberId) {
    	RestResponse resp = res;
        resp.addHeader('Content-Type', 'application/json');
		resp.responseBody = Blob.valueOf(JSON.serialize(new SuccessResponseWrapper(statusCode, status, contactId, campaignMemberId)));
    }

}