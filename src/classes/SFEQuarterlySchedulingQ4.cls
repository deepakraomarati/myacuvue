global class SFEQuarterlySchedulingQ4 implements schedulable
{
    global void execute(SchedulableContext sc)
    {
      //1. Schedule batch for each country
        set<String> strCountries = new set<String>{'ANZ%'};
        
        //ANZ
        SFE_ReachCallFrequencyBatchClass_Q4 b = new SFE_ReachCallFrequencyBatchClass_Q4(strCountries); 
        database.executebatch(b,100);
        
        strCountries.clear();
        strCountries.add('IN%');
        //IN
        b = new SFE_ReachCallFrequencyBatchClass_Q4(strCountries); 
        database.executebatch(b,100);
        
        strCountries.clear();
        strCountries.add('HK%');
        //HK
        b = new SFE_ReachCallFrequencyBatchClass_Q4(strCountries); 
        database.executebatch(b,100);
        
        strCountries.clear();
        strCountries.add('CH%');
        //CH
        b = new SFE_ReachCallFrequencyBatchClass_Q4(strCountries); 
        database.executebatch(b,100);
        
        strCountries.clear();
        strCountries.add('TW%');
        //TW
        b = new SFE_ReachCallFrequencyBatchClass_Q4(strCountries); 
        database.executebatch(b,100);
        
        strCountries.clear();
        strCountries.add('KR%');
        //KR
        b = new SFE_ReachCallFrequencyBatchClass_Q4(strCountries); 
        database.executebatch(b,100);
        
        strCountries.clear();
        strCountries.add('KOR%');
        //KOR
        b = new SFE_ReachCallFrequencyBatchClass_Q4(strCountries); 
        database.executebatch(b,100);    
    }
}