@isTest 
private class SFE_MonthlyQuarterlySchedulerTest{
    @isTest static void validateMonthlyScheduler() {
        Test.startTest();
        Reach_CallFrequency__c ReachFrequency_Lead = new Reach_CallFrequency__c(Name='Lead',Frequency_Goal__c=6,Overall_Metrics__c='Lead',Reach_Goal__c=100);
        insert ReachFrequency_Lead;
        Reach_CallFrequency__c ReachFrequency_Compete = new Reach_CallFrequency__c(Name='Compete',Frequency_Goal__c=3,Overall_Metrics__c='Compete',Reach_Goal__c=80);
        insert ReachFrequency_Compete;
        Reach_CallFrequency__c ReachFrequency_Develop = new Reach_CallFrequency__c(Name='Develop',Frequency_Goal__c=3,Overall_Metrics__c='Develop',Reach_Goal__c=80);
        insert ReachFrequency_Develop;
        Reach_CallFrequency__c ReachFrequency_Maintain = new Reach_CallFrequency__c(Name='Maintain',Frequency_Goal__c=1,Overall_Metrics__c='Maintain',Reach_Goal__c=40);
        insert ReachFrequency_Maintain;
        SFEMonthlyScheduling monthlybatch = new SFEMonthlyScheduling();      
        String sch = '0  00 1 3 * ?';
        system.schedule('SFEMonthlyScheduling', sch, monthlybatch );
        system.assertEquals(sch,'0  00 1 3 * ?','success');
        Test.stopTest();
    }
    @isTest static void validateQuarterlyScheduler() {
        Test.startTest();
        Reach_CallFrequency__c ReachFrequency_Lead = new Reach_CallFrequency__c(Name='Lead',Frequency_Goal__c=6,Overall_Metrics__c='Lead',Reach_Goal__c=100);
        insert ReachFrequency_Lead;
        Reach_CallFrequency__c ReachFrequency_Compete = new Reach_CallFrequency__c(Name='Compete',Frequency_Goal__c=3,Overall_Metrics__c='Compete',Reach_Goal__c=80);
        insert ReachFrequency_Compete;
        Reach_CallFrequency__c ReachFrequency_Develop = new Reach_CallFrequency__c(Name='Develop',Frequency_Goal__c=3,Overall_Metrics__c='Develop',Reach_Goal__c=80);
        insert ReachFrequency_Develop;
        Reach_CallFrequency__c ReachFrequency_Maintain = new Reach_CallFrequency__c(Name='Maintain',Frequency_Goal__c=1,Overall_Metrics__c='Maintain',Reach_Goal__c=40);
        insert ReachFrequency_Maintain;
        SFEQuarterlySchedulingQ1 quarterlybatchQ1 = new SFEQuarterlySchedulingQ1();      
        String sch1 = '0  00 1 3 * ?';
        system.schedule('SFEQuarterlySchedulingQ1', sch1, quarterlybatchQ1 );
        
        SFEQuarterlySchedulingQ2 quarterlybatchQ2 = new SFEQuarterlySchedulingQ2();      
        String sch2 = '0  00 1 3 * ?';
        system.schedule('SFEQuarterlySchedulingQ2', sch2, quarterlybatchQ2);
        
        SFEQuarterlySchedulingQ3 quarterlybatchQ3 = new SFEQuarterlySchedulingQ3();      
        String sch3 = '0  00 1 3 * ?';
        system.schedule('SFEQuarterlySchedulingQ3', sch3, quarterlybatchQ3 );
        
        SFEQuarterlySchedulingQ4 quarterlybatchQ4 = new SFEQuarterlySchedulingQ4();      
        String sch4 = '0  00 1 3 * ?';
        system.schedule('SFEQuarterlySchedulingQ4', sch4, quarterlybatchQ4 );
        system.assertEquals(sch4, '0  00 1 3 * ?','success');
        Test.stopTest();
    }
}