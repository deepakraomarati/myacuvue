@isTest
private class SFE_ReachCallFrequencyBatchClassTest {
    static testMethod void validatebatchclasslogic(){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'ASPAC ANZ CDM/KAM' LIMIT 1];
        UserRole usrrole = [SELECT Id from UserRole where Name= 'ANZ - Customer Development Manager/ Key Account Manager' limit 1];
        User usr = new User(
            ProfileId = profileId .Id,
            LastName = 'test local user',
            Email = 'testlocaluser@sfdc.com',
            Username = 'testlocaluser@sfdc.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = usrrole.Id,
            Unique_User_Id__c= 'testu',
            Target_Calls_a_Day_del__c = 6
        );   
        insert usr;
        system.runas(usr){
            system.debug('User Record' + usr);
            Test.startTest();
            List<Account> lstAccount= new List<Account>();
            Reach_CallFrequency__c ReachFrequency_Lead = new Reach_CallFrequency__c(Name='Lead',Frequency_Goal__c=6,Overall_Metrics__c='Lead',Reach_Goal__c=100);
            insert ReachFrequency_Lead;
            Reach_CallFrequency__c ReachFrequency_Compete = new Reach_CallFrequency__c(Name='Compete',Frequency_Goal__c=3,Overall_Metrics__c='Compete',Reach_Goal__c=80);
            insert ReachFrequency_Compete;
            Reach_CallFrequency__c ReachFrequency_Develop = new Reach_CallFrequency__c(Name='Develop',Frequency_Goal__c=3,Overall_Metrics__c='Develop',Reach_Goal__c=80);
            insert ReachFrequency_Develop;
            Reach_CallFrequency__c ReachFrequency_Maintain = new Reach_CallFrequency__c(Name='Maintain',Frequency_Goal__c=1,Overall_Metrics__c='Maintain',Reach_Goal__c=40);
            insert ReachFrequency_Maintain;
            
            Account acc1 = new Account();
            acc1.Name ='Test Account 1';
            acc1.Call_Frequency_Cycle__c=2.0;
            acc1.Call_Frequency_Quarter__c=2.0;
            acc1.Overall_Metrics__c='Lead';
            acc1.OwnerId = usr.Id;
            lstAccount.add(acc1);
            
            Account acc2 = new Account();
            acc2.Name ='Test Account 2';
            acc2.Call_Frequency_Cycle__c=2.0;
            acc2.Call_Frequency_Quarter__c=2.0;
            acc2.Overall_Metrics__c='Maintain';
            acc2.OwnerId = usr.Id;
            lstAccount.add(acc2);
            
            Account acc3 = new Account();
            acc3.Name ='Test Account 3';
            acc3.Call_Frequency_Cycle__c=2.0;
            acc3.Call_Frequency_Quarter__c=2.0;
            acc3.Overall_Metrics__c='Develop';
            acc3.OwnerId = usr.Id;
            lstAccount.add(acc3);
            
            Account acc4 = new Account();
            acc4.Name ='Test Account 4';
            acc4.Call_Frequency_Cycle__c=2.0;
            acc4.Call_Frequency_Quarter__c=2.0;
            acc4.Overall_Metrics__c='Compete';
            acc4.OwnerId = usr.Id;
            lstAccount.add(acc4);
            
            Account acc5 = new Account();
            acc5.Name ='Test Account 5';
            acc5.Call_Frequency_Cycle__c=1.0;
            acc5.Call_Frequency_Quarter__c=1.0;
            acc5.Overall_Metrics__c='Lead';
            acc5.OwnerId = usr.Id;
            lstAccount.add(acc5);
            
            Account acc6 = new Account();
            acc6.Name ='Test Account 6';
            acc6.Call_Frequency_Cycle__c=1.0;
            acc6.Call_Frequency_Quarter__c=1.0;
            acc6.Overall_Metrics__c='Maintain';
            acc6.OwnerId = usr.Id;
            lstAccount.add(acc6);
            
            Account acc7 = new Account();
            acc7.Name ='Test Account 7';
            acc7.Call_Frequency_Cycle__c=1.0;
            acc7.Call_Frequency_Quarter__c=1.0;
            acc7.Overall_Metrics__c='Develop';
            acc7.OwnerId = usr.Id;
            lstAccount.add(acc7);
            
            Account acc8 = new Account();
            acc8.Name ='Test Account 8';
            acc8.Call_Frequency_Cycle__c=1.0;
            acc8.Call_Frequency_Quarter__c=1.0;
            acc8.Overall_Metrics__c='Compete';
            acc8.OwnerId = usr.Id;
            lstAccount.add(acc8);
            
            insert lstAccount;
            List<Event> lstEvent=new List<Event>();
            
            for(integer i=0;i<lstaccount.size();i++){
                Event insertEvent1=new Event();
                insertEvent1.whatid=lstaccount[i].Id;
                insertEvent1.StartDateTime=DateTime.newInstance(2018, 1, 15, 7, 8, 16);
                insertEvent1.EndDateTime=DateTime.newInstance(2018, 1, 16, 7, 8, 16);
                insertEvent1.ActivityStatus__c='Completed';
                insertEvent1.ActivityType__c='Customer visit (1 call)';
                insertEvent1.CallOutcomeCallTargetComments__c ='this is it';
                insertEvent1.OwnerId = usr.Id;
                insertEvent1.Brand_Name__c = 'Other';
                insertEvent1.Objective1__c = 'Others';
                insertEvent1.TargetAudience__c = 'Others';
                lstEvent.add(insertEvent1);
                
                Event insertEvent2=new Event();
                insertEvent2.whatid=lstaccount[i].Id;
                insertEvent2.StartDateTime=DateTime.newInstance(2018, 1, 15, 7, 8, 16);
                insertEvent2.EndDateTime=DateTime.newInstance(2018, 1, 16, 7, 8, 16);
                insertEvent2.ActivityStatus__c='Completed';
                insertEvent2.ActivityType__c='Working Day';
                insertEvent2.CallOutcomeCallTargetComments__c ='this is it';
                insertEvent2.OwnerId = usr.Id;
                insertEvent2.Brand_Name__c = 'Other';
                insertEvent2.Objective1__c = 'Others';
                insertEvent2.TargetAudience__c = 'Others';
                lstEvent.add(insertEvent2);
                
                Event insertEvent3=new Event();
                insertEvent3.whatid=lstaccount[i].Id;
                insertEvent3.StartDateTime=DateTime.newInstance(2018, 2, 15, 7, 8, 16);
                insertEvent3.EndDateTime=DateTime.newInstance(2018, 2, 16, 7, 8, 16);
                insertEvent3.ActivityStatus__c='Completed';
                insertEvent3.ActivityType__c='Customer visit (1 call)';
                insertEvent3.CallOutcomeCallTargetComments__c ='this is it';
                insertEvent3.OwnerId = usr.Id;
                insertEvent3.Brand_Name__c = 'Other';
                insertEvent3.Objective1__c = 'Others';
                insertEvent3.TargetAudience__c = 'Others';
                lstEvent.add(insertEvent3);
                
                Event insertEvent4=new Event();
                insertEvent4.whatid=lstaccount[i].Id;
                insertEvent4.StartDateTime=DateTime.newInstance(2018, 2, 15, 7, 8, 16);
                insertEvent4.EndDateTime=DateTime.newInstance(2018, 2, 16, 7, 8, 16);
                insertEvent4.ActivityStatus__c='Completed';
                insertEvent4.ActivityType__c='Working Day';
                insertEvent4.CallOutcomeCallTargetComments__c ='this is it';
                insertEvent4.OwnerId = usr.Id;
                insertEvent4.Brand_Name__c = 'Other';
                insertEvent4.Objective1__c = 'Others';
                insertEvent4.TargetAudience__c = 'Others';
                lstEvent.add(insertEvent4);
                
                Event insertEvent5=new Event();
                insertEvent5.whatid=lstaccount[i].Id;
                insertEvent5.StartDateTime=DateTime.newInstance(2018, 3, 15, 7, 8, 16);
                insertEvent5.EndDateTime=DateTime.newInstance(2018, 3, 16, 7, 8, 16);
                insertEvent5.ActivityStatus__c='Completed';
                insertEvent5.ActivityType__c='Customer visit (1 call)';
                insertEvent5.CallOutcomeCallTargetComments__c ='this is it';
                insertEvent5.OwnerId = usr.Id;
                insertEvent5.Brand_Name__c = 'Other';
                insertEvent5.Objective1__c = 'Others';
                insertEvent5.TargetAudience__c = 'Others';
                lstEvent.add(insertEvent5);
                
                Event insertEvent6=new Event();
                insertEvent6.whatid=lstaccount[i].Id;
                insertEvent6.StartDateTime=DateTime.newInstance(2018, 3, 15, 7, 8, 16);
                insertEvent6.EndDateTime=DateTime.newInstance(2018, 3, 16, 7, 8, 16);
                insertEvent6.ActivityStatus__c='Completed';
                insertEvent6.ActivityType__c='Working Day';
                insertEvent6.CallOutcomeCallTargetComments__c ='this is it';
                insertEvent6.OwnerId = usr.Id;
                insertEvent6.Brand_Name__c = 'Other';
                insertEvent6.Objective1__c = 'Others';
                insertEvent6.TargetAudience__c = 'Others';
                lstEvent.add(insertEvent6);
                
                Event insertEvent7=new Event();
                insertEvent7.whatid=lstaccount[i].Id;
                insertEvent7.StartDateTime=DateTime.newInstance(2018, 4, 15, 7, 8, 16);
                insertEvent7.EndDateTime=DateTime.newInstance(2018, 4, 16, 7, 8, 16);
                insertEvent7.ActivityStatus__c='Completed';
                insertEvent7.ActivityType__c='Customer visit (1 call)';
                insertEvent7.CallOutcomeCallTargetComments__c ='this is it';
                insertEvent7.OwnerId = usr.Id;
                insertEvent7.Brand_Name__c = 'Other';
                insertEvent7.Objective1__c = 'Others';
                insertEvent7.TargetAudience__c = 'Others';
                lstEvent.add(insertEvent7);
                
                Event insertEvent8=new Event();
                insertEvent8.whatid=lstaccount[i].Id;
                insertEvent8.StartDateTime=DateTime.newInstance(2018, 4, 15, 7, 8, 16);
                insertEvent8.EndDateTime=DateTime.newInstance(2018, 4, 16, 7, 8, 16);
                insertEvent8.ActivityStatus__c='Completed';
                insertEvent8.ActivityType__c='Working Day';
                insertEvent8.CallOutcomeCallTargetComments__c ='this is it';
                insertEvent8.OwnerId = usr.Id;
                insertEvent8.Brand_Name__c = 'Other';
                insertEvent8.Objective1__c = 'Others';
                insertEvent8.TargetAudience__c = 'Others';
                lstEvent.add(insertEvent8);
                
                Event insertEvent9=new Event();
                insertEvent9.whatid=lstaccount[i].Id;
                insertEvent9.StartDateTime=DateTime.newInstance(2018, 7, 15, 7, 8, 16);
                insertEvent9.EndDateTime=DateTime.newInstance(2018, 7, 16, 7, 8, 16);
                insertEvent9.ActivityStatus__c='Completed';
                insertEvent9.ActivityType__c='Customer visit (1 call)';
                insertEvent9.CallOutcomeCallTargetComments__c ='this is it';
                insertEvent9.OwnerId = usr.Id;
                insertEvent9.Brand_Name__c = 'Other';
                insertEvent9.Objective1__c = 'Others';
                insertEvent9.TargetAudience__c = 'Others';
                lstEvent.add(insertEvent9);
                
                Event insertEvent10=new Event();
                insertEvent10.whatid=lstaccount[i].Id;
                insertEvent10.StartDateTime=DateTime.newInstance(2018, 7, 15, 7, 8, 16);
                insertEvent10.EndDateTime=DateTime.newInstance(2018, 7, 16, 7, 8, 16);
                insertEvent10.ActivityStatus__c='Completed';
                insertEvent10.ActivityType__c='Working Day';
                insertEvent10.CallOutcomeCallTargetComments__c ='this is it';
                insertEvent10.OwnerId = usr.Id;
                insertEvent10.Brand_Name__c = 'Other';
                insertEvent10.Objective1__c = 'Others';
                insertEvent10.TargetAudience__c = 'Others';
                lstEvent.add(insertEvent10);
                
                Event insertEvent11=new Event();
                insertEvent11.whatid=lstaccount[i].Id;
                insertEvent11.StartDateTime=DateTime.newInstance(2018, 10, 15, 7, 8, 16);
                insertEvent11.EndDateTime=DateTime.newInstance(2018, 10, 16, 7, 8, 16);
                insertEvent11.ActivityStatus__c='Completed';
                insertEvent11.ActivityType__c='Customer visit (1 call)';
                insertEvent11.CallOutcomeCallTargetComments__c ='this is it';
                insertEvent11.OwnerId = usr.Id;
                insertEvent11.Brand_Name__c = 'Other';
                insertEvent11.Objective1__c = 'Others';
                insertEvent11.TargetAudience__c = 'Others';
                lstEvent.add(insertEvent11);
                
                Event insertEvent12=new Event();
                insertEvent12.whatid=lstaccount[i].Id;
                insertEvent12.StartDateTime=DateTime.newInstance(2018, 10, 15, 7, 8, 16);
                insertEvent12.EndDateTime=DateTime.newInstance(2018, 10, 16, 7, 8, 16);
                insertEvent12.ActivityStatus__c='Completed';
                insertEvent12.ActivityType__c='Working Day';
                insertEvent12.CallOutcomeCallTargetComments__c ='this is it';
                insertEvent12.OwnerId = usr.Id;
                insertEvent12.Brand_Name__c = 'Other';
                insertEvent12.Objective1__c = 'Others';
                insertEvent12.TargetAudience__c = 'Others';
                lstEvent.add(insertEvent12);                
            }
            
            insert lstEvent;
            SFE_ReachCallFrequencyBatchClass_JAN obj_jan = new SFE_ReachCallFrequencyBatchClass_JAN();
            DataBase.executeBatch(obj_jan,200);
            
            SFE_ReachCallFrequencyBatchClass_FEB obj_feb = new SFE_ReachCallFrequencyBatchClass_FEB();
            DataBase.executeBatch(obj_feb,200);
            
            SFE_ReachCallFrequencyBatchClass_MAR obj_mar = new SFE_ReachCallFrequencyBatchClass_MAR();
            DataBase.executeBatch(obj_mar,200);      
            Set<String> countryrole = new Set<String>();
            countryrole.add('ANZ%');
            
            SFE_ReachCallFrequencyBatchClass obj = new SFE_ReachCallFrequencyBatchClass(countryrole);
            DataBase.executeBatch(obj,200);
            
            //20180606 test for monthly manual
            SFE_ReachCallFrequencyBatchClass_MManual obj_mm = new SFE_ReachCallFrequencyBatchClass_MManual(countryrole,2018,1);
            DataBase.executeBatch(obj_mm,200);
            
            //end monthly manual
            
            SFE_ReachCallFrequencyBatchClass_Q1 obj_q1 = new SFE_ReachCallFrequencyBatchClass_Q1(countryrole);
            DataBase.executeBatch(obj_q1,200);
            
            SFE_ReachCallFrequencyBatchClass_Q2 obj_q2 = new SFE_ReachCallFrequencyBatchClass_Q2(countryrole);
            
            DataBase.executeBatch(obj_q2,200);
            
            SFE_ReachCallFrequencyBatchClass_Q3 obj_q3 = new SFE_ReachCallFrequencyBatchClass_Q3(countryrole);
            DataBase.executeBatch(obj_q3,200);       
            
            SFE_ReachCallFrequencyBatchClass_Q4 obj_q4 = new SFE_ReachCallFrequencyBatchClass_Q4(countryrole);
            DataBase.executeBatch(obj_q4,200);
            system.assertequals(ReachFrequency_Maintain.Name,'Maintain','success');
            Test.stopTest();
        }
    }   
}