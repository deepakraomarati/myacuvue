public without sharing class SGCampaignController {

    public String SAPID { get; set; }
   
    public String VoucherCode { get; set; }

    public String fileName {get;set;}
    
    public Transient Blob fileBody {get;set;}
    
    public String status { get; set; }

    public boolean showpanel { get; set; }
    
    public String fontcolor { get; set; }
    
    Public list<CampaignMember> lstCampMember;
    
    Public list<CampaignMember> lstCampMemberSAPID;
    
    Public list<Attachment> contactAttachments;
    
    Public Id conId;
    
    Public string SGCampaign;
    
    Public PageReference resultpage;

    /********  Constructor  *************/
    public SGCampaignController() {
        fontcolor = 'Red';
        status = '';
        showpanel =false;
        lstCampMember = new list<CampaignMember>();
        lstCampMemberSAPID = new list<CampaignMember>();
        contactAttachments =new list<Attachment>();
    }

    /********  Get my e-Vocher  *************/
    public PageReference ValidateVoucher() {
        
        showpanel =false;
        status ='';
        fontcolor = ''; 
        
        try { 
                Map<String,ExactTarget_Integration_Settings__c> MapcampId = ExactTarget_Integration_Settings__c.getAll();
                SGCampaign = MapcampId.get('SGCampaign').Value__c;
                system.debug('SGCampaign  #####'+SGCampaign);
                
                String  MemberqryString = 'SELECT Id,Name,SAP_ID__c,Receipt_number__c,contactId,AttachmentId__c,CampaignId,Voucher_Status__c FROM CampaignMember WHERE CampaignId=:SGCampaign AND Receipt_number__c=:VoucherCode LIMIT 1';
                System.debug('Campaign Member qryStr:'+MemberqryString);
                lstCampMember=  Database.Query(MemberqryString);
                System.debug('Member list:'+lstCampMember);
                
                if((!lstCampMember.isEmpty()&& lstCampMember.size()>0)&&(fileBody == null)){
                    if(lstCampMember[0].Voucher_Status__c =='Not Used'){
                        lstCampMember[0].SAP_ID__c= SAPID;
                        lstCampMember[0].Voucher_Status__c='Redeemed';    
                        update lstCampMember;
                        return ResultMessage(System.Label.Voucher_Code_Reserved,'Green');  
                    }
                    else{
                        conId = lstCampMember[0].ContactId;
                        contactAttachments = [SELECT Id, Name FROM Attachment Where parentId = :conId AND Description= 'SG_Voucher_Receipt'];
                        System.debug('Files attached to contact:'+contactAttachments);
                        if(!contactAttachments.isEmpty()&& contactAttachments.size()>0){
                             return ResultMessage(System.Label.Voucher_Code_Redeemed,'Red');  
                        }else{
                             return ResultMessage(System.Label.Voucher_Code_Used,'Red');    
                        }
                          
                    }
                }else if((!lstCampMember.isEmpty()&& lstCampMember.size()>0)&&(fileBody != null)){
                            
                    String MemberqryString1 = 'SELECT Id,Name,SAP_ID__c,Receipt_number__c,contactId,AttachmentId__c,CampaignId,Voucher_Status__c FROM CampaignMember WHERE CampaignId=:SGCampaign AND Receipt_number__c=:VoucherCode AND SAP_ID__c =:SAPID LIMIT 1';
                    lstCampMemberSAPID =  Database.Query(MemberqryString1);
                    System.debug('Member list SAPID:'+lstCampMemberSAPID);
                   
                    if((!lstCampMemberSAPID.isEmpty()&& lstCampMemberSAPID.size()>0) &&(lstCampMemberSAPID[0].Voucher_Status__c =='Redeemed')){
                               
                        conId = lstCampMemberSAPID[0].ContactId;
                        contactAttachments = [SELECT Id, Name FROM Attachment Where parentId = :conId AND Description= 'SG_Voucher_Receipt'];
                        System.debug('Files attached to contact:'+contactAttachments);
                        if(!contactAttachments.isEmpty()&& contactAttachments.size()>0){
                             return ResultMessage(System.Label.Voucher_Code_Redeemed,'Red');     
                             //return ResultMessage('You already uploaded the image of the receipt','Red');          
                        }
                        else{
                                Database.SaveResult attachmentResult = saveStandardAttachment(conId);
                                System.debug('Standard Attachment Result::'+attachmentResult);
            
                                if (attachmentResult == null || !attachmentResult.isSuccess()) {
                                    // ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Could not save attachment.')); 
                                    return ResultMessage(System.Label.Image_Upload_Error,'Red');  
                                   } else {
                                    lstCampMemberSAPID[0].AttachmentId__c = attachmentResult.Id;
                                    update lstCampMemberSAPID;
                                    resultpage = Page.Thank_You_Page;
                                    return resultpage;
                                }
                        }
                    }
                    else{
                        return ResultMessage(System.Label.SAPId_Voucher_Code_Does_not_Match,'Red'); 
                    }
                    
                    
                    
                }else{
                    return ResultMessage(System.Label.Invalid_Voucher_Code,'Red');
                } 
                System.debug('Updated member list:'+lstCampMember);
        }catch (Exception e) {
             return ResultMessage(e.getMessage(),'Red');
        }
                        
                  
        return null;
    }
        
    /********  Reset the form  *************/
    Public void  resetform(){
        SAPID = '';
        VoucherCode = '';
        fileName = '';
        fileBody = null;
    }
    
    /********  Return Result  *************/
    Public PageReference ResultMessage(String MessageDescription,String textcolor){
        status = MessageDescription;
        fontcolor = textcolor;
        showpanel = true;
        resetform();
        return null;
    }
    
    private Database.SaveResult saveStandardAttachment(Id parentId) {
        Database.SaveResult result;
        
        Attachment attachment = new Attachment();
        attachment.body = this.fileBody;
        attachment.name = this.fileName;
        attachment.parentId = parentId;
        attachment.Description= 'SG_Voucher_Receipt';
        result = Database.insert(attachment);
        // reset the file for the view state
        fileBody = Blob.valueOf(' ');
        return result;
    }
    
    
}