public without sharing class SW_ANZ_VoucherRedemptionFormCntrl_LEX
{
    public static String SelectedBrand1;
    public static String SelectedBrand2;
    Public static string ANZCampaign;
    public static String ReceiptNo;
    public static  String fileName;
    public static Transient Blob fileBody;
    Public static  Id conId;
    public static  String SAPID;
    public static  String Phone;
    public  static String eVoucher;
    Public static  list<CampaignMember> lstCampMember;
    @AuraEnabled
    public static String Onload(String ECPId)
    {
        List<Account> loggedinECP = [SELECT Id,Name,Phone,OutletNumber__c FROM Account WHERE Id=:ECPId LIMIT 1];
       return loggedinECP[0].Name;
    }
    @AuraEnabled
    public static resultWrapper RedeemVoucher(String fileName, String base64Data, String contentType, String fileId,String eVoucher ,String AccountId,String ReceiptNo ,String Box1 ,String Box2)
	{
        SelectedBrand1='TSP';
        SelectedBrand2='TAS';
        try{
            List<CampaignMember> lstcontact = new list<CampaignMember>();
            Map<String,ExactTarget_Integration_Settings__c> MapcampId = ExactTarget_Integration_Settings__c.getAll();
            ANZCampaign = MapcampId.get('ANZCampaign').Value__c;
           
            lstcontact = [SELECT Id,ContactID, Contact.Name,Contact.FirstName,Contact.LastName,Contact.NRIC__c FROM CampaignMember WHERE CampaignId=:ANZCampaign AND User_eVoucher_Code__c=:eVoucher LIMIT 1];
           
            String  MemberqryString;
            if(lstcontact.size() > 0)
            {
                conId=lstcontact[0].ContactID;
            }
            else
            {
               resultWrapper result = new resultWrapper('Error' ,System.Label.Invalid_Voucher_Code);
                return result;
            }
            if(ReceiptNo!=NULL && ReceiptNo!='')
            {
                MemberqryString = 'SELECT Id,Name,SAP_ID__c,Account__c,Receipt_number__c,contactId,AttachmentId__c,CampaignId,Voucher_Status__c,User_eVoucher_Code__c FROM CampaignMember WHERE CampaignId=:ANZCampaign AND User_eVoucher_Code__c=:eVoucher AND Account__c =: AccountId LIMIT 1';
            }else
            {
                MemberqryString = 'SELECT Id,Name,SAP_ID__c,Account__c,Receipt_number__c,contactId,AttachmentId__c,CampaignId,Voucher_Status__c,User_eVoucher_Code__c FROM CampaignMember WHERE CampaignId=:ANZCampaign AND User_eVoucher_Code__c=:eVoucher AND Account__c =: AccountId LIMIT 1';
            }
            lstCampMember=  Database.Query(MemberqryString);
            Map<ID, CampaignMember> mapCampMember = new Map<ID, CampaignMember>();
            String voucherCode;
            try
            {
                if(!lstCampMember.isEmpty() && lstCampMember.size() > 0)
                {
                    voucherCode = lstCampMember[0].User_eVoucher_Code__c;
                    if (voucherCode != eVoucher)
                    {
                        resultWrapper result = new resultWrapper('Error',System.Label.Invalid_Voucher_Code);
                        return result;
                    }
                }
            }
            catch(exception e)
            {
                resultWrapper result = new resultWrapper('Error',System.Label.Invalid_Voucher_Code);
                return result;
            }
            if(lstCampMember.isEmpty()&& lstCampMember.size()==0 && (ReceiptNo ==''|| ReceiptNo==NULL))
            {
                resultWrapper result = new resultWrapper('Error',System.Label.Unsubscribe);
                return result;
            }
            if(lstCampMember.isEmpty()&& lstCampMember.size()==0 && (ReceiptNo !='' || ReceiptNo != NULL))
            {
                resultWrapper result = new resultWrapper('Error',System.Label.Invalid_Voucher_Code);
                return result;
            }
            if((!lstCampMember.isEmpty()&& lstCampMember.size()>0)&&(lstCampMember[0].Account__c!=AccountId))
            {
                resultWrapper result = new resultWrapper('Error',System.Label.Not_Valid_Store);
                return result;
            }
            if(lstCampMember[0].Voucher_Status__c =='Redeemed' && lstCampMember[0].AttachmentId__c !='')
            {
                resultWrapper result = new resultWrapper('Error',System.Label.Voucher_Code_Redeemed);
                return result;
            }

            if(lstCampMember[0].Voucher_Status__c =='Not Used' && lstCampMember[0].AttachmentId__c ==Null && String.isNotBlank(base64Data))
            {
                String base64DataResult = EncodingUtil.urlDecode(base64Data, 'UTF-8');
                Database.SaveResult attachmentResult;
                Attachment attachment = new Attachment();
                attachment.body = EncodingUtil.base64Decode(base64DataResult);
                attachment.name = fileName;
                attachment.parentId = conId;
                attachment.Description= '$30 e-voucher';
                attachmentResult = Database.insert(attachment);
                if (attachmentResult == null || !attachmentResult.isSuccess())
                {
                     resultWrapper result = new resultWrapper('Error',System.Label.Image_Upload_Error);
                     return result;
                }
                else
                {
                    system.debug('Box1'+Box1);
                     system.debug('Box2'+Box2);
                    if(Box1!=null && Box1!='')
                    {
                        lstCampMember[0].Product1__c= SelectedBrand1;
                        lstCampMember[0].No_of_Boxes1__c= Integer.ValueOf(Box1);
                    }
                    if(Box2!=null && Box2!='')
                    {
                        lstCampMember[0].Product2__c= SelectedBrand2;
                        lstCampMember[0].No_of_Boxes2__c= Integer.ValueOf(Box2);
                    }
                    lstCampMember[0].AttachmentId__c = attachmentResult.Id;
                    lstCampMember[0].Voucher_Status__c='Redeemed';
                    lstCampMember[0].Redeemed_Date_Time__c = System.Now();
                    update lstCampMember;
                     resultWrapper result = new resultWrapper('Success',System.Label.Thank_You);
                     return result;
                }
            }
            else if(lstCampMember[0].Voucher_Status__c =='Not Used' && String.isBlank(base64Data) && ReceiptNo != null)
            {
                if(ReceiptNo!=NULL && ReceiptNo!='')
                {
                    lstCampMember[0].Receipt_number__c= ReceiptNo;
                }
                if(Box1!=null && Box1!='')
                {
                    lstCampMember[0].Product1__c= SelectedBrand1;
                    lstCampMember[0].No_of_Boxes1__c= Integer.ValueOf(Box1);
                }
                if(Box2!=null && Box2!='')
                {
                    lstCampMember[0].Product2__c= SelectedBrand2;
                    lstCampMember[0].No_of_Boxes2__c= Integer.ValueOf(Box2);
                }
                lstCampMember[0].Voucher_Status__c='Redeemed';
                lstCampMember[0].Redeemed_Date_Time__c = System.Now();
                update lstCampMember;
                                resultWrapper result = new resultWrapper('Success',System.Label.Thank_You);
                return result;
            }
            else if(lstCampMember[0].Voucher_Status__c =='Not Used' && lstCampMember[0].AttachmentId__c !=Null && String.isNotBlank(base64Data))
            {
               resultWrapper result = new resultWrapper('Error',System.Label.Invalid_Voucher_Code);
            return result; 
            }
        }
        catch (Exception e)
        {
            resultWrapper result = new resultWrapper('Error',e.getMessage());
            return result;
        }
        return null;
    }
    public class resultWrapper
    {
        @AuraEnabled public String type{get;set;}
        @AuraEnabled public String value{get;set;}
        public resultWrapper(String type, String value)
        {
            this.type = type;
            this.value = value;
        }
    }
}