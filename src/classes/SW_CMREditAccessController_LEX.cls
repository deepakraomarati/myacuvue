public with sharing  class SW_CMREditAccessController_LEX {
    public Static Account accListToEditOrDel;
    public  Account AccRec{get;set;}
    
    @AuraEnabled
    public static Boolean cmrAccess(ID recordId)
    {
     JJ_JPN_CustomerMasterRequest__c access   =  [select JJ_JPN_IsEditDeleteAcc__c from JJ_JPN_CustomerMasterRequest__c where id=:recordId];
        return access.JJ_JPN_IsEditDeleteAcc__c;
    }
 
    public class pickListValues{
        @AuraEnabled public String value{get;set;}
        @AuraEnabled public String label{get;set;}
            
        public pickListValues(String label, String value){
            this.label = label;
            this.value = value;
        }
    }
     @AuraEnabled 
    public static Map<String, Map<String, List<pickListValues>>>  getPicklistValues(String objpicklistFieldsMap)
    {
        Map<String, List<String>> objPickmap = (Map<String, List<String>>)JSON.deserialize(objpicklistFieldsMap, Map<String, List<String>>.class);
        system.debug('objpickmap ' + objPickmap);
        
        Map<String, Map<String, List<pickListValues>>> objFieldPicklistMap = new Map<String, Map<String, List<pickListValues>>>();
        List<String> sobjectslist = new List<String>(objPickmap.keySet());
        system.debug('sobjectslist ' + sobjectslist);
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(sobjectslist);
        system.debug('results ' + results);
        Map<String,List<pickListValues>> fieldOptionsMap;
        try{
            for(Schema.DescribeSObjectResult result : results){
                fieldOptionsMap = new Map<String,  List<pickListValues>>();
                
                Schema.sObjectType objType = result.getSObjectType();
                
                Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
                map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
                List<String> objFieldlist = objPickmap.get(result.getName());
                system.debug('objname ' + result.getName());
                system.debug('list of fields ' + objFieldlist);
                for(String fld : objFieldlist){
                   Boolean defaultVal = false;
                    system.debug('fields ' + fld);
                    List<String > allOpts = new list<String>();
                    //Map <String,String> mapValues =new Map<String,String>();
                    list<Schema.PicklistEntry> values =
                        fieldMap.get(fld).getDescribe().getPickListValues();
                    System.debug('values'+values);
                    List<pickListValues> lstFinalpickListValuesWrapper =new  List<pickListValues>();
                    List<pickListValues> lstpickListValuesWrapper =new  List<pickListValues>();
                    pickListValues pickListValuesWrapper = new  pickListValues(Label.None, '');
                    lstpickListValuesWrapper.add(pickListValuesWrapper);
                    for (Schema.PicklistEntry a : values){
                        if(a.isDefaultValue()){
                            defaultVal = true;
                            pickListValuesWrapper = new  pickListValues(a.getLabel(), a.getValue());
                            lstFinalpickListValuesWrapper.add(pickListValuesWrapper);
                        }
                        else{
                            pickListValuesWrapper = new  pickListValues(a.getLabel(), a.getValue());
                            lstpickListValuesWrapper.add(pickListValuesWrapper);
                        }
                        //mapValues.put(a.getValue(),a.getLabel());
                    }
                    if(defaultVal){
                        lstpickListValuesWrapper.remove(0);
                        if(fld != 'JJ_JPN_StatusCode__c'){
                            pickListValuesWrapper = new  pickListValues(Label.None, '');
                            lstpickListValuesWrapper.add(pickListValuesWrapper);
                        }
                    }
                    lstFinalpickListValuesWrapper.addAll(lstpickListValuesWrapper);
                    fieldOptionsMap.put(fld, lstFinalpickListValuesWrapper);
                }
                objFieldPicklistMap.put(result.getName(), fieldOptionsMap);
            }
        }catch(AuraHandledException e){
			throw e;
		}
        return objFieldPicklistMap;
    }
    
    @AuraEnabled 
    public static boolean accessDenied(){
        try{
            String profileName = [Select Name from Profile where Id =: UserInfo.getProfileId()].Name;
            return (profileName !='ASPAC JPN Sales Admin' && profileName !='System Administrator');
        }Catch(AuraHandledException e){
			throw e;
		}
    }
    @AuraEnabled 
    public static boolean accessDeniedEdit(){
        try{
            system.debug('Access Denied');
            String profileName = [Select Name from Profile where Id =: UserInfo.getProfileId()].Name;
            return (profileName =='System Administrator JPN' || profileName =='ASPAC JPN Sales Admin');
        }Catch(AuraHandledException e){
            System.debug('Inside catch');
			throw e;
		}
    }
    @AuraEnabled 
    public static CMRWrapper autoPopulateAccValues(String acc1, String objpicklistFieldsMap)
    {
        JJ_JPN_CustomerMasterRequest__c CMR = new JJ_JPN_CustomerMasterRequest__c();
        try{
            CMR= [SELECT id , Name,JJ_JPN_Description__c,JJ_JPN_StatusCodeRD__c,JJ_JPN_StatusCode__c,
                        JJ_JPN_SoldToCode__c,JJ_JPN_SoldToNameKana__c,JJ_JPN_SoldToNameKanji__c,JJ_JPN_SubCustomerGroup__c,
                        JJ_JPN_Person_Incharge_NameSAP__c,Account_Activation_Start_Date__c,JJ_JPN_CreditPersonInchargeName__c,
                        JJ_JPN_PayerCode__c,JJ_JPN_PayerNameKanji__c,JJ_JPN_BillToCode__c,JJ_JPN_OfficeName__c,JJ_JPN_BillToNameKanji__c,
                        JJ_JPN_SoldToPostalCode__c,JJ_JPN_SoldToState__c,JJ_JPN_SoldToTownshipCity__c,JJ_JPN_SoldToStreet__c,
                        JJ_JPN_SoldToOtherAddress__c,JJ_JPN_SoldToTelephone__c,JJ_JPN_SoldToFax__c,JJ_JPN_PaymentCondition__c,JJ_JPN_OrderBlock__c,
                        JJ_JPN_ReturnFAXNo__c,JJ_JPN_SalesItem__c,JJ_JPN_SAMDESC__c,JJ_JPN_ADSHandlingFlag__c,JJ_JPN_ShopName__c,JJ_JPN_PaymentMethod__c,
                        JJ_JPN_CustomerRTNFAXHowManyTimesToSend__c,JJ_JPN_DirectDelivery__c,JJ_JPN_BillToRTNFAX__c,JJ_JPN_SoldToNumberOfPagesReceived__c,
                        JJ_JPN_TransactionDetailSubmission__c,JJ_JPN_BillingSummaryTableSubmission__c,JJ_JPN_ItemizedBilling__c,JJ_JPN_BillSubmission__c,
                        JJ_JPN_LotNumberCommunicationDocument__c,JJ_JPN_DeliveryDocumentNote__c,JJ_JPN_EDIStoreCode__c,JJ_JPN_EDIFlag__c,JJ_JPN_FAXOrder__c,
                        JJ_JPN_CustomerOrderNoRequired__c,JJ_JPN_CustomerOrderNoNumberofDigits__c,JJ_JPN_DeadlineTime__c,JJ_JPN_ReturnedGoodsOrderNoInDigits__c,
                        JJ_JPN_NoDeliveryCharges__c,JJ_JPN_FestivalDelivery__c,JJ_JPN_IndividualDeliveryCommentsMonday__c,JJ_JPN_IndividualDeliveryCommentsTuesday__c,
                        JJ_JPN_IndividualDeliveryCommentsWed__c,JJ_JPN_IndividualDeliveryCommentsThurs__c,JJ_JPN_IndividualDeliveryCommentsFriday__c,
                        JJ_JPN_IndividualDeliveryCommentsSat__c,JJ_JPN_IndividualDeliveryCommentsSun__c,JJ_JPN_CautionMemoCustomerInformation__c,
                        JJ_JPN_IndividualDeliveryPossibleMonday__c,JJ_JPN_IndividualDeliveryPossibleTues__c,JJ_JPN_IndividualDeliveryPossibleDateWed__c,
                        JJ_JPN_PIndividualDeliveryPossibleThur__c,JJ_JPN_IndividualDeliveryPossibleFriday__c,JJ_JPN_IndividualDeliveryPossibleSat__c,
                        JJ_JPN_IndividualDeliveryPossibleDateSun__c  
                        FROM JJ_JPN_CustomerMasterRequest__c 
                        where id=:acc1];
          
        }Catch(AuraHandledException e){
			throw e;
		}
        system.debug('CMR:'+ CMR);
        //CMRWrapper resultWrapper = new CMRWrapper(CMR , getPicklistValues(objpicklistFieldsMap));
        return new CMRWrapper(CMR , getPicklistValues(objpicklistFieldsMap));
    }
    
    public class CMRWrapper{
        @AuraEnabled public JJ_JPN_CustomerMasterRequest__c CMR{get;set;}
        @AuraEnabled public Map<String, Map<String, List<pickListValues>>> objFieldPicklistMap{get;set;}
        public CMRWrapper(JJ_JPN_CustomerMasterRequest__c CMR, Map<String, Map<String, List<pickListValues>>>  objFieldPicklistMap){
            this.CMR = CMR;
            this.objFieldPicklistMap = objFieldPicklistMap;
        }
    }
    
    
     @AuraEnabled
    public static Map<String,String> CMRSave(JJ_JPN_CustomerMasterRequest__c toUpdCMRrec)
     {
         Map<String,String> resultMap = new Map<String,String>();
         try{
             system.debug('');
                 update toUpdCMRrec;
             system.debug('Local Language'+UserInfo.getLanguage());
             if(UserInfo.getLanguage()=='en_US')
             {
             	resultMap.put('status', 'success');
                resultMap.put('message', 'CMR Updated Successfully'); 
             }
             else
             {
                 resultMap.put('status', 'success');
                resultMap.put('message', 'CMRが正常に更新されました'); 
             }
           }   /*               
         catch( NullPointerException ne) {
             resultMap.put('status', 'error');
             resultMap.put('message',ne.getMessage());
             system.debug('=====NullPointerException======'+ne.getMessage());
         } */       
         catch (DmlException de) {           
             resultMap.put('status', 'error');
             resultMap.put('message',de.getDmlMessage(0));
             system.debug('=====DmlException======'+de.getMessage()); 
         }
         catch(Exception e){
             resultMap.put('status', 'error');
             resultMap.put('message',e.getMessage());
             system.debug('=====Exception======'+e.getMessage()); 
         }
          system.debug('=====resultMap======'+resultMap);      
          return resultMap;
     }
    
    public class errorWrapper{
        @AuraEnabled
        public String type{get;set;}
        @AuraEnabled
        public String value{get;set;}
        public errorWrapper(String type, String value){
            this.type = type;
            this.value = value;
        }
    }
    @AuraEnabled 
    public static String accessCheck(String recordId){
        
        //JPN Sales Rep + Draft = Standard
        //JPN Sales Rep + Other Status = 'Access Denied'
        //JPN Sales Admin/System Admin + true = Custom
        //JPN Sales Admin/System Admin + false = Standard
        String accessFlag = '';
        try{
            String profileName = [Select Name from Profile where Id =: UserInfo.getProfileId()].Name;
            List<JJ_JPN_CustomerMasterRequest__c > lstCMR =[Select Id,JJ_JPN_IsEditDeleteAcc__c,JJ_JPN_ApprovalStatus__c  from JJ_JPN_CustomerMasterRequest__c WHERE Id =:recordId];
            if(profileName =='ASPAC JPN Sales Rep'){
                if(!lstCMR.isEmpty() && lstCMR[0].JJ_JPN_ApprovalStatus__c != 'Draft')
                    accessFlag = 'Access Denied'; //access denied
                else
                    accessFlag = 'Standard'; //standard UI
            }
            if(profileName =='ASPAC JPN Sales Admin' || profileName =='System Administrator JPN' || profileName =='System Administrator'){
                if(!lstCMR.isEmpty() && lstCMR[0].JJ_JPN_IsEditDeleteAcc__c == true)
                    accessFlag = 'Custom'; //custom UI
                else
                    accessFlag = 'Standard'; //standard UI
            }
            return accessFlag;
        }Catch(AuraHandledException e){
			throw e;
		}
    }
}