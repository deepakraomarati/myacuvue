public without sharing class SW_CountryWiseAttachmentCntrl_LEX {
   
    public List<WrapperClass> wrapperList{get;set;}

    
    @AuraEnabled
    public static resultWrapper  uploadAttachment(String  couponId,String  fileName,String uploadFile,
                                                             String fileType){
        List<String> errList = new List<String>();       
        resultWrapper resultWrapperData=new resultWrapper(' ' , errList);
       
       
        if(uploadFile == null || uploadFile == ''){
        	errList.add('Please specify a file to upload. Type in the path to the file, or use the "Browse" button to locate it in your local filesystem');
		}
		system.debug('errList:: '+errList);
                                                                 
		if(!errList.isEmpty()){
			resultWrapperData = new resultWrapper('error' , errList);
		}
		else{
            if(fileName != null && fileName != '' && fileType != ''){
               
           string     uploadFileencode = EncodingUtil.urlDecode(uploadFile, 'UTF-8');
                if(couponId != null){
                    Attachment attachFile = new Attachment();
                    attachFile.Name = fileName;
                    attachFile.body = EncodingUtil.base64Decode(uploadFileencode);
                    attachFile.ParentId = couponId;
                    attachFile.contentType = fileType;
                    insert attachFile;
                    resultWrapperData = new resultWrapper('success' , null);
                }
            }
		}
                                                                 
        return resultWrapperData;
    }
   
    public class resultWrapper{
        @AuraEnabled public String type{get;set;}
        @AuraEnabled public List<String> value{get;set;}
        public resultWrapper(String type, List<String> value){
            this.type = type;
            this.value = value;
        }
    }
    
    
    @AuraEnabled 
    public static List<WrapperClass> getAllAttachment( Id cwcID) {
        List<WrapperClass> wrapperList = new List<WrapperClass>();
       
        List<Attachment> attachmentList = [select Id, Name , ParentId  ,createdDate , LastModifiedDate,createdBy.Name , CreatedById 
                                           from Attachment where ParentId =: cwcID];
  
        for(Attachment attach : attachmentList){
        	wrapperList.add(new WrapperClass(Label.SW_Attachment_LEX,attach.Id, attach.Name, '/servlet/servlet.FileDownload?file='+attach.Id,attach.LastModifiedDate.format(),attach.createdBy.Name));    
        }
        return wrapperList;
    }
    
    
    @AuraEnabled 
    public static void deleteRecord( Id cwcRowID) {
        system.debug('couponimagId>>>'+cwcRowID);
        Attachment attachDel = new Attachment();
        attachDel.Id = cwcRowID;
        delete attachDel;
    }
        
    //Inner class for setting and getting all the file info along with file Type
    public class WrapperClass{
        @AuraEnabled  public String ConType{get; set;}
        @AuraEnabled public String Id{get; set;}
        @AuraEnabled public String Title{get; set;}
        @AuraEnabled public String titleURL{get; set;}
        @AuraEnabled public String LastModifiedDate{get; set;}
        @AuraEnabled public String CreatedBy{get; set;}
      
        public WrapperClass(String ConType,String Id,String Title,String titleURL,String LastModifiedDate,String CreatedBy){
            this.ConType = ConType;
            this.Id = Id;
            this.Title = Title;
            this.titleURL = titleURL;
            this.LastModifiedDate = LastModifiedDate;
            this.CreatedBy = CreatedBy;
          
        }
    }
}