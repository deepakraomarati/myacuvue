@isTest
Private class SW_CountryWiseAttachmentCntrl_LEXTest 
{
    
    Public Static Testmethod void uploadAttachmentTest()
    {
        MA2_CountryWiseConfiguration__c	 CountWise=new MA2_CountryWiseConfiguration__c(Name='TestCoup');
        insert  CountWise;
        
        test.startTest();
        SW_CountryWiseAttachmentCntrl_LEX.resultWrapper resultWrapper = SW_CountryWiseAttachmentCntrl_LEX.uploadAttachment(CountWise.Id,'Testing',
                                                                                                                           'iVBORw0KGgoAAAANSUhEUgAAAi8AAAFtCAIAAABAzh3pAAAACXBIWXMAAA7EAAAOxA',
                                                                                                                           'image/jpeg');
        system.assertEquals(resultWrapper.type , 'success');
        test.stopTest();
    }     
    
    Public Static Testmethod void getAllAttachmentTest()
    {
        String uploadFile='iVBORw0KGgoAAAANSUhEUgAAAi8AAAFtCAIAAABAzh3pAAAACXBIWXMAAA7EAAAOxA';
        uploadFile = EncodingUtil.urlDecode(uploadFile, 'UTF-8');
        
        MA2_CountryWiseConfiguration__c	 CountWise=new MA2_CountryWiseConfiguration__c(Name='CWC');
        insert  CountWise;
        Attachment attach=new Attachment(Name='Test' , ParentId=CountWise.Id,body=EncodingUtil.base64Decode(uploadFile));
        insert  attach;
        test.startTest();
        List< SW_CountryWiseAttachmentCntrl_LEX.WrapperClass> wrapperList= SW_CountryWiseAttachmentCntrl_LEX.getAllAttachment(CountWise.Id);
        system.debug('wrapperList>>>'+wrapperList);
        
        system.assertEquals(wrapperList[0].Id,attach.Id);
        test.stopTest();
    }
    
    Public Static Testmethod void deleteRecordTest()
    {
        String uploadFile='iVBORw0KGgoAAAANSUhEUgAAAi8AAAFtCAIAAABAzh3pAAAACXBIWXMAAA7EAAAOxA';
        uploadFile = EncodingUtil.urlDecode(uploadFile, 'UTF-8');
        MA2_CountryWiseConfiguration__c	 CountWise=new MA2_CountryWiseConfiguration__c(Name='TestCoup');
        insert  CountWise;
        Attachment attach=new Attachment(Name='Test' , ParentId=CountWise.Id,body=EncodingUtil.base64Decode(uploadFile));
        insert  attach;
        
        test.startTest();
        SW_CountryWiseAttachmentCntrl_LEX.deleteRecord(attach.Id);
        Integer  Count = [SELECT count() from Attachment where id =:attach.id];
        system.debug('Count>>>'+Count);
      system.assertEquals(Count,0);
        test.stopTest();
    }     
}