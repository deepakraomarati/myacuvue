public with sharing class  SW_CreateFollowUpEventCtlr_LEX {

    public static Map<Id, String> recordtypemap {get;set;}
    
   @AuraEnabled        
    public static List<String> fetchRecordTypeValues(){
        List<Schema.RecordTypeInfo> recordtypes = Event.SObjectType.getDescribe().getRecordTypeInfos();    
        recordtypemap = new Map<Id, String>();
        
                
        for(RecordTypeInfo rt : recordtypes){
            if (rt.isAvailable() && !String.valueOf(rt.getRecordTypeId()).endsWith('AAA'))
                recordtypemap.put(rt.getRecordTypeId(), rt.getName());
        }        
        return recordtypemap.values();
    }
    
    @AuraEnabled
    public static id getRecTypeId(String recordTypeLabel){
        id recid = Schema.SObjectType.Event.getRecordTypeInfosByName().get(recordTypeLabel).getRecordTypeId();
        return recid;
    }
    @AuraEnabled
    public static Event getEventDetails(String recordId){
        Event event =[SELECT WhatId,WhoId,Subject FROM Event Where Id=:recordId Limit 1];
        return event;
    }

}