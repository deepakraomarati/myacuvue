public without sharing  class SW_CustomEditController_LEX {
   
    @AuraEnabled 
    public static Attachment autoPopulateAccValues(String recordID)
    {
        String size;
        List<Attachment> attach = [SELECT Id,IsPrivate,Description,Name,BodyLength FROM Attachment WHERE Id =: recordID];
        return attach[0];
    }
    
    @AuraEnabled
    public static void updateAttachment(Attachment attachObj){
        system.debug('attachObj : ' + attachObj);
        update attachObj;
    }
}