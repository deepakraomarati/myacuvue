@isTest
public class SW_CustomEditController_LEXTest {
    
    Public Static Testmethod void autoPopulateAccValuesTest()
    {
        String uploadFile='iVBORw0KGgoAAAANSUhEUgAAAi8AAAFtCAIAAABAzh3pAAAACXBIWXMAAA7EAAAOxA';
        uploadFile = EncodingUtil.urlDecode(uploadFile, 'UTF-8');
        Product2 Prod=new Product2(Name='TestCoup');
        insert  Prod;
        Attachment attach=new Attachment(Name='Test',ParentId=Prod.Id,body=EncodingUtil.base64Decode(uploadFile));
        insert  attach;
        test.startTest();
        Attachment attachReturn = SW_CustomEditController_LEX.autoPopulateAccValues(attach.Id);
        system.debug('attachReturn>>>'+attachReturn);
        system.assertEquals(Prod.Name,'TestCoup','success');
        test.stopTest();
    }
    
    Public Static Testmethod void updateAttachmentTest()
    {
        String uploadFile='iVBORw0KGgoAAAANSUhEUgAAAi8AAAFtCAIAAABAzh3pAAAACXBIWXMAAA7EAAAOxA';
        uploadFile = EncodingUtil.urlDecode(uploadFile, 'UTF-8');
        Product2 Prod=new Product2(Name='TestCoup');
        insert  Prod;
        Attachment attach=new Attachment(Name='Test',ParentId=Prod.Id,body=EncodingUtil.base64Decode(uploadFile));
        insert  attach;
        test.startTest();
        SW_CustomEditController_LEX.updateAttachment(attach);
        system.assertEquals(Prod.Name,'TestCoup','success');
        test.stopTest();
    }
    
}