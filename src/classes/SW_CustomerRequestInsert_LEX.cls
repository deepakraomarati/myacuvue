public without sharing  class SW_CustomerRequestInsert_LEX {
    @AuraEnabled
    Public static List<productinsertwrapper> prodlstwrap {get; set;}
    @AuraEnabled
    Public list<Product2> prodlst{get; set;}
    @AuraEnabled
    Public  integer totalRecs {get; set;}
    //This wrapper class is used while loading the data.
    Public class productinsertwrapper{
        @AuraEnabled
        public product2 prodwrap{get; set;}
        @AuraEnabled
        public boolean ischeckwrap{get; set;}
        @AuraEnabled
        public integer quantitywrap{get; set;}
        
        Public productinsertwrapper(){
        }
        Public productinsertwrapper(product2 prod,boolean ischeck,integer quantity){
            system.debug('quantity'+quantity);
            system.debug('quantity'+prod);
            prodwrap = prod;
            ischeckwrap = ischeck;
            quantitywrap = quantity;      
        }
    }
    //This method will be called to load the data in to the aura component.
    @AuraEnabled
    public static List<productinsertwrapper> loadingData()
    {
        list<Product2> prodlst= new list<Product2>();
        integer totalRecs = 0;
        integer LimitSize= 10000;
        integer OffsetSize = 0;
        prodlstwrap = New List<productinsertwrapper>();
        Map<id,integer>   quantitymap = New Map<id,integer>();
        if(prodlst != null && !prodlst.isEmpty()){
            prodlst.clear();
        }
        string country = 'JPN';
        String pType = 'JPN POP';
        String strQuery ='SELECT Id,Name,ProductCode,BrandCode__c,BaseCurve__c,JJ_JPN_Power__c,JJ_JPN_TotalQuantity__c,JJ_JPN_CountryCode__c,JJ_JPN_AlignmentOrderSequence__c FROM Product2 WHERE ProductType__c !=: pType AND JJ_JPN_CountryCode__c=:country';
        if(totalRecs !=null && totalRecs ==0){
            List<Product2> prodTemp = Database.query(strQuery);
            totalRecs = (prodTemp !=null &&prodTemp.size()>0)?prodTemp.size():0;
        }
        system.debug('totalRecs................'+totalRecs);
        //strQuery += 'ORDER BY Name,JJ_JPN_AlignmentOrderSequence__c  ASC,CreatedDate DESC LIMIT :LimitSize OFFSET :OffsetSize'; 
        strQuery += ' ORDER BY JJ_JPN_AlignmentOrderSequence__c  ASC LIMIT :LimitSize OFFSET :OffsetSize'; 
        prodlst  =Database.query(strQuery);
        system.debug('prodlst................'+prodlst.size());
        prodlstwrap = New List<productinsertwrapper> ();
        for(Product2 prod: prodlst){
            if(quantitymap.containskey(prod.id)){
                prodlstwrap.add(New productinsertwrapper(prod,false,quantitymap.get(prod.id)));
            }
            else{
                prodlstwrap.add(New productinsertwrapper(prod,false,0));
            }   
        }
        
        return prodlstwrap;
        
    }
    //This method will be called while saving the added products.
    @AuraEnabled
    Public static string savingProduct(string prodListValues,string customermasterreqid){
        
        string validCheck='';
        system.debug('saved products63'+prodListValues);
        system.debug('saved products64'+customermasterreqid);
        //prodlstwrap=Lightning_CustomerRequestInsert.loadingData();
        prodlstwrap = (List<productinsertwrapper>)JSON.deserialize(prodListValues,List<productinsertwrapper>.class);
        // prodlstwrap.add(prodlstwrap1);
        system.debug('saved products68'+prodlstwrap.size());
        Map<id,integer> quantitymap = New Map<id,integer> ();
        for(productinsertwrapper selectedprod : prodlstwrap){
            system.debug('saved products71'+selectedprod);
            if( selectedprod.quantitywrap!=0){
                system.debug('selected ids'+selectedprod.prodwrap.id);
                quantitymap.put(selectedprod.prodwrap.id,selectedprod.quantitywrap); 
                
            }
        }    
        List<JJ_JPN_InitialStock__c>  existingprodlst = New List<JJ_JPN_InitialStock__c>();
        Map<Id,JJ_JPN_InitialStock__c>   initialstockmap = New Map<Id,JJ_JPN_InitialStock__c> ();
        existingprodlst =[SELECT Id,JJ_JPN_Product__c,JJ_JPN_Customer_Master_Request__c,JJ_JPN_Quantity__c FROM JJ_JPN_InitialStock__c WHERE JJ_JPN_Customer_Master_Request__c =:customermasterreqid];
        for(JJ_JPN_InitialStock__c initialstk :existingprodlst){
            initialstockmap.put(initialstk.JJ_JPN_Product__c,initialstk);    
        }
        system.debug('outside..........'+initialstockmap.keySet());
        List<JJ_JPN_InitialStock__c> initialstockinsertlst = New List<JJ_JPN_InitialStock__c>();
        List<JJ_JPN_InitialStock__c> initialstockupdatelst = New List<JJ_JPN_InitialStock__c>();
        
        for(id newprodmap : quantitymap.keyset()){
            
            if(initialstockmap.containskey(newprodmap)){            
                JJ_JPN_InitialStock__c initialstockupdate = New JJ_JPN_InitialStock__c(id = initialstockmap.get(newprodmap).id);
                initialstockupdate.JJ_JPN_Quantity__c = initialstockmap.get(newprodmap).JJ_JPN_Quantity__c+quantitymap.get(newprodmap);
                
                initialstockupdatelst.add(initialstockupdate );
                system.debug('saving ids'+initialstockupdatelst);
            }
            else{
                JJ_JPN_InitialStock__c initialstockinsert = New JJ_JPN_InitialStock__c();
                initialstockinsert.JJ_JPN_Product__c = newprodmap;
                initialstockinsert.JJ_JPN_Customer_Master_Request__c = customermasterreqid;
                //system.debug('initialstock.JJ_JPN_Customer_Master_Request__c..........'+initialstock.JJ_JPN_Customer_Master_Request__c);
                initialstockinsert.JJ_JPN_Quantity__c = quantitymap.get(newprodmap);
                initialstockinsertlst.add(initialstockinsert );
            }                             
        }
        if(!initialstockinsertlst.IsEmpty()){
            database.Insert(initialstockinsertlst,false);
            
        }
        if(!initialstockupdatelst.IsEmpty()){
            system.debug('Updating');
            database.Update(initialstockupdatelst,false);
            
        }
        
        if(initialstockinsertlst.IsEmpty() && initialstockupdatelst.IsEmpty()){
            validCheck='Please enter quntity for atleast one product or click cancel to close';
        }
        
        return validCheck;
        
    }
    
}