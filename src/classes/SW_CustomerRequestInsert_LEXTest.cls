@isTest
public class SW_CustomerRequestInsert_LEXTest {
    
    static testMethod void loadingDataTest()
    {
        SW_CustomerRequestInsert_LEX.loadingData();
        
        JJ_JPN_CustomerMasterRequest__c cus=new JJ_JPN_CustomerMasterRequest__c();
        cus.JJ_JPN_PayerNameKana__c='カスタ';
        cus.JJ_JPN_PayerNameKanji__c='カスタ';
        insert cus;
        Product2 prod=new Product2();
        prod.ProductType__c='JPN POP';
        prod.ProductCode__c='345678';
        prod.Name='Testing';
        prod.JJ_JPN_CountryCode__c='JPN';
        insert prod;
        
        
        string st='[{"ischeckwrap":true,"prodwrap":{"Id":"01t6F000006VlV5QAK","Name":"カスタマーインフォメーションセット","ProductCode":"ZC306","BrandCode__c":"新規必須","JJ_JPN_TotalQuantity__c":"1","JJ_JPN_CountryCode__c":"JPN","JJ_JPN_AlignmentOrderSequence__c":1},"quantitywrap":"1"}]';      
        SW_CustomerRequestInsert_LEX.savingProduct(st,cus.Id);  
        SW_CustomerRequestInsert_LEX.productinsertwrapper wrapper=new  SW_CustomerRequestInsert_LEX.productinsertwrapper(prod,true,5);
        system.assertEquals('Testing',prod.Name);
    }
    
}