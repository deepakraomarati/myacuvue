public without sharing class SW_CustomerRequestNewRequest_LEX {
    
    
    public class ResultWrapper {
        public String objName {get;set;}
        public String text{get;set;}
        public String val{get;set;}
        public datetime crDate{get;set;}
        public ResultWrapper(){
            //default constructor
        }
    }
    //This method returns pick list values for Customer Type look up in aura component
    @AuraEnabled
    public  static List<String> getPickListValuesAccType(){
       List<String> pickListValuesList= new List<String>();
		Schema.DescribeFieldResult fieldResult = JJ_JPN_CustomerMasterRequest__c.JJ_JPN_AccountType__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
			pickListValuesList.add(pickListVal.getLabel());
		}     
		return pickListValuesList;
    }
       //This method returns Account values for Customer Type look up in aura component
    @AuraEnabled
    public static string payerUpdatedValues(string payerId)
    {  
        system.debug('paerrrid'+payerId);
        Account acc=[select Name,AccountNumber from Account where id=:payerId];
        string accName=acc.Name;        
        return accName;
    }
      //This method returns User values for Customer Type look up in aura component
    @AuraEnabled
    public static string uerValues(string userIdValue)
    {   user uss=[select Name,id from user where id=:userIdValue];
        string accName=uss.Name;
        return accName;
    }
}