@isTest(Seealldata=true)
public class SW_CustomerRequestNewRequest_LEXTest {
    
    
    static testMethod void getPickListValuesAccTypeTest()
    {  
        SW_CustomerRequestNewRequest_LEX.ResultWrapper error = new SW_CustomerRequestNewRequest_LEX.ResultWrapper();
        error.objName = 'abcd';
        error.text = 'abcd';
        error.val = 'abcd';
        Account acc=new Account();
        User use;
        
        SW_CustomerRequestNewRequest_LEX.getPickListValuesAccType();
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        UserRole r = new UserRole(DeveloperName = 'MyCustomRole5', Name = 'My Role');
        insert r;
        
        use = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last1',
            Email = 'puser000161@amamama.com',
            Username = 'puser001310@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST1',
            Title = 'title1',
            Alias = 'alias1',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = r.Id
        );   
        system.runAs(thisUser){
            Test.startTest();
            acc=new Account();
            acc.Name='Ryan';
            insert acc;
            system.assertEquals(acc.Name,'Ryan','success');
            SW_CustomerRequestNewRequest_LEX.payerUpdatedValues(acc.id);
            system.debug('userid'+use);
            SW_CustomerRequestNewRequest_LEX.uerValues(thisUser.Id);
            test.stopTest();
        }
    }
}