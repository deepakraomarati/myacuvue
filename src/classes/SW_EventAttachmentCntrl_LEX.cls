public without sharing class SW_EventAttachmentCntrl_LEX {
    
    @AuraEnabled
    public static String uploadAttachment(String base64Data, String fileName, String parentId)
    {
        String base64DataResult = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        Database.SaveResult attachmentResult;      
        Attachment attachment = new Attachment();
        attachment.body = EncodingUtil.base64Decode(base64DataResult);
        attachment.name = fileName;
        attachment.parentId = parentId;
        attachment.OwnerId = [SELECT OwnerId FROM Event WHERE id=: parentId].OwnerId;
        attachmentResult = Database.insert(attachment);
        return null;
    }
    
}