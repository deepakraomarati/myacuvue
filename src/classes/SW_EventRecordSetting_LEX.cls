public with sharing class SW_EventRecordSetting_LEX {
    
    @RemoteAction
    public  static void updateEventRTSettings(List<evtResultData> res){
        system.debug('recTypePicklistDetails>>>'+res);
        List<Event_RecordType_Setting__c> lstevSettings = new List<Event_RecordType_Setting__c>();
        for(evtResultData evData: res){
            if(!evData.PK.startsWithIgnoreCase('Rec')){
                Event_RecordType_Setting__c evSettings = new Event_RecordType_Setting__c(Composite_Key__c = evData.label, 
                                                                                         Field_Api__c=evData.PK, Record_Type__c=evData.RT, 
                                                                                         Values__c=evData.PKVal, 
                                                                                         Default_Value__c=evData.DefaultVal,
                                                                                         Label_Value_pairs__c = evData.labelValuePair, 
                                                                                         SW_Language__c = evData.language);
                lstevSettings.add(evSettings);
            }
        }
        upsert lstevSettings Composite_Key__c;
    }
    
    public class evtResultData{
        public String label{get;set;}
        public String RT{get;set;}
        public String PK{get;set;}
        public String PKVal{get;set;}
        public String DefaultVal{get;set;}
        public String labelValuePair{get;set;}
        public String language{get;set;}
        
    }
}