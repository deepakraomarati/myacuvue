public without sharing class SW_FollowTaskCntrl_LEX {
    @AuraEnabled
    public static List<Object> getRelatedTo(String recordID){
        List<Event> EventObj = [SELECT WhoId,WhatId,Subject FROM Event WHERE  id =: recordID];
        return EventObj;
    }
 }