@isTest
public class SW_FollowTaskCntrl_LEXTest {
    
    Public Static Testmethod void testMethodFollowTask()
    {
        
        Account acc = New Account(Name ='Test Account',PublicAddress__c='Bangalore', SalesRep__c = 'ABC', AccountNumber = '12345', OutletNumber__c = '12345', CountryCode__c = 'AUS',Subscribe__c=true);
        insert acc;
        event e= new event();
        e.StartDateTime = date.today();
        e.EndDateTime = date.today().addDays(2);
        e.Subject = 'Test';
        e.WhatId = acc.id ;
        insert e;
        test.startTest();
        List<Object> result= SW_FollowTaskCntrl_LEX.getRelatedTo(e.id);
        System.assertEquals(result, result);
        test.stopTest();
    }
}