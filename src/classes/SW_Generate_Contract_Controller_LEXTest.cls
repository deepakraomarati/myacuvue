@isTest(SeeallData=true)
public class SW_Generate_Contract_Controller_LEXTest {
    
    static testMethod void fetchSignatureTest()
    {
        Account acc=new Account();
        acc.Name='Ryan';
        insert acc;
        Id conId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('아큐브 멀티포컬 취급 약정서').getRecordTypeId();
        Id conId1 = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('마이아큐브 프로그램 제공 및 사용 약정서').getRecordTypeId();
        Id conId2 = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('아큐브 매출 및 지원 약정서').getRecordTypeId();
        Id conId3 = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('공급계약서').getRecordTypeId();
        Contract cont=new Contract();
        cont.RecordTypeId=conId;
        cont.AccountId=acc.id;
        
        insert cont;
        SW_Generate_Contract_Controller_LEX.accountDetails(cont.Id);
        Contract cont1=new Contract();
        cont1.RecordTypeId=conId1;
        cont1.AccountId=acc.id;
        insert cont1;
        SW_Generate_Contract_Controller_LEX.accountDetails(cont1.Id);
        Contract cont2=new Contract();
        cont2.RecordTypeId=conId2;
        cont2.AccountId=acc.id;
        cont2.Send_Email__c='Test12@gmail.com';
        insert cont2;
        //SW_Generate_Contract_Controller_LEX.accountDetails(cont2.Id);
        
        Contract cont3=new Contract();
        cont3.RecordTypeId=conId3;
        cont3.AccountId=acc.id;
        cont3.Send_Email__c='Test@gmail.com';
        insert cont3;
        Attachment attch=new Attachment();
        Blob bod = Blob.valueOf('Test Data');
        attch.ParentId=cont3.id;
        attch.Name='Signature Contract 4';
        attch.Body=EncodingUtil.base64Decode('Test');
        
        
        insert attch;
        Contract contr=  SW_Generate_Contract_Controller_LEX.accountDetails(cont3.Id);
        system.assertEquals(contr.Send_Email__c,'Test@gmail.com');
        
        Attachment attch1=new Attachment();
        
        attch1.ParentId=cont2.id;
        attch1.Name='Signature Contract 3';
        attch1.Body=EncodingUtil.base64Decode('Test12');
        insert attch1;
        SW_Generate_Contract_Controller_LEX.accountDetails(cont2.Id);
    }
    static testMethod void fetchSignatureTesting()
    {
        Account acc=new Account();
        acc.Name='Ryan';
        insert acc;
        
        Id conId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('아큐브 멀티포컬 취급 약정서').getRecordTypeId();
        Id conId1 = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('마이아큐브 프로그램 제공 및 사용 약정서').getRecordTypeId();
        
        Contract cont=new Contract();
        cont.RecordTypeId=conId;
        cont.AccountId=acc.id;
        cont.Send_Email__c='ryan@gmail.com';
        
        insert cont;
        Attachment attch1=new Attachment();
        
        attch1.ParentId=cont.id;
        attch1.Name='Signature Contract 1';
        attch1.Body=EncodingUtil.base64Decode('Test123');
        insert attch1;
        
        Contract contr=SW_Generate_Contract_Controller_LEX.accountDetails(cont.Id);
        system.assertEquals(contr.Send_Email__c,'ryan@gmail.com');
        Contract cont1=new Contract();
        cont1.RecordTypeId=conId1;
        cont1.AccountId=acc.id;
        cont1.Send_Email__c='Ryan1@gmail.com';
        insert cont1;
        Attachment attch=new Attachment();
        
        attch.ParentId=cont1.id;
        attch.Name='Signature Contract 2';
        attch.Body=EncodingUtil.base64Decode('Test1234');
        insert attch;
        
        SW_Generate_Contract_Controller_LEX.accountDetails(cont1.Id);
        Contract cont4=new Contract();
        cont4.RecordTypeId=conId1;
        cont4.AccountId=acc.id;
        
        insert cont4;
        Attachment attch3=new Attachment();
        
        attch3.ParentId=cont4.id;
        attch3.Name='Signature Contract 2';
        attch3.Body=EncodingUtil.base64Decode('Test12346');
        insert attch3;
        SW_Generate_Contract_Controller_LEX.accountDetails(cont4.Id);
        
        
    }
    
}