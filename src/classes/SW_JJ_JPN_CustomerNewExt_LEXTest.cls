@isTest(seealldata=true)
public class SW_JJ_JPN_CustomerNewExt_LEXTest {
    
    static testMethod void randomGenerator()
    {   Account acc=new Account();
     acc.AccountNumber='09871';
     acc.Name='Ryan';
     insert acc;
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator();
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator2();
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator3();
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.autoPopulateValues('show','NotShow','NotShow','NotShow','NotShow','NotShow','NotShow',acc.id,acc.id);
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.autoPopulateValues('Notshow','show','NotShow','NotShow','NotShow','NotShow','NotShow',acc.id,acc.id);
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.autoPopulateValues('Notshow','Notshow','show','NotShow','NotShow','NotShow','NotShow',acc.id,acc.id);   
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.autoPopulateValues('Notshow','notshow','NotShow','show','NotShow','NotShow','NotShow',acc.id,acc.id);
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.autoPopulateValues('Notshow','notshow','NotShow','NotShow','show','NotShow','NotShow',acc.id,acc.id);
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.autoPopulateValues('Notshow','notshow','NotShow','NotShow','NotShow','show','NotShow',acc.id,acc.id);
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.autoPopulateValues('Notshow','notshow','NotShow','NotShow','NotShow','NotShow','show',acc.id,acc.id);
     JJ_JPN_CustomerMasterRequest__c cus=new JJ_JPN_CustomerMasterRequest__c();
     cus.JJ_JPN_PayerNameKanji__c='あ い';
     cus.JJ_JPN_PayerNameKana__c='あ い';
     insert cus;
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.save(cus,acc.id,'radio1',acc.id,acc.id,acc.id,'Test');
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.save(cus,acc.id,'radio2',acc.id,acc.id,acc.id,'Test');
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.save(cus,acc.id,'radio3',acc.id,acc.id,acc.id,'Test');
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.save(cus,acc.id,'radio4',acc.id,acc.id,acc.id,'Test');
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.save(cus,acc.id,'radio5',acc.id,acc.id,acc.id,'Test');
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.save(cus,acc.id,'radio6',acc.id,acc.id,acc.id,'Test');
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.save(cus,acc.id,'radio7',acc.id,acc.id,acc.id,'Test');
     SW_JJ_JPN_CustomerNewRegistrationExt_LEX.autoPopulateAccValues(acc.id);
     Account AccNum=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.autoPopulateAccBillValues(acc.id);
     system.assertEquals(acc.name,AccNum.name);
     
    }
    
}