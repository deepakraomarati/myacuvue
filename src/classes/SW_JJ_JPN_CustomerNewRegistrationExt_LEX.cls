public without sharing class SW_JJ_JPN_CustomerNewRegistrationExt_LEX {
    
    public static List<wrapperPayee> lstw = new List<wrapperPayee>();
    //this wrapper class returns values of different accounts . 
    public class wrapperPayee{
        @AuraEnabled
        public  String payerCode{get;set;}
        @AuraEnabled
        public  String BillCode{get;set;}
        @AuraEnabled     
        public  String soldValue{get;set;}
        @AuraEnabled     
        public  String AccNumber{get;set;}
        public wrapperPayee(String payCode,String BilCodeVal,string soldValue){
            this.payerCode=payCode;
            this.BillCode=BilCodeVal;
            this.soldValue=soldValue;
        }
    }
    //This method Account out let number
    @AuraEnabled
    public static  string randomNumberGenerator() 
    {   Integer autoNum = Integer.valueOf(AutoNumberCMR__c.getValues('Account Outlet').JJ_JPN_AutoNumberOutlet__c);
     system.debug('From Test==>'+autoNum);
     String rndNumFiveDigit='';
     if(autoNum!=Null)
     {
         String autoIncrement = String.ValueOf(autoNum+1);
         rndNumFiveDigit  = autoIncrement.substring(0,5);
     }
     system.debug('returning values'+rndNumFiveDigit);
     return rndNumFiveDigit;
    }
    //This method Account out let number
    @AuraEnabled
    public  static string randomNumberGenerator2() 
    {   Integer autoNum = Integer.valueOf(AutoNumberCMR__c.getValues('Account Outlet').JJ_JPN_AutoNumberOutlet__c);
     String rndNumFiveDigit='';
     if(autoNum!=Null)
     {
         String autoIncrement = String.ValueOf(autoNum+2);
         rndNumFiveDigit  = autoIncrement.substring(0,5);
     }
     return rndNumFiveDigit;
    }
    //This method Account out let number
    @AuraEnabled
    public static string randomNumberGenerator3() 
    {  Integer autoNum = Integer.valueOf(AutoNumberCMR__c.getValues('Account Outlet').JJ_JPN_AutoNumberOutlet__c);
     String rndNumFiveDigit='';
     if(autoNum!=Null)
     {
         String autoIncrement = String.ValueOf(autoNum+3);
         rndNumFiveDigit  = autoIncrement.substring(0,5);
     }
     return rndNumFiveDigit;
    }
    //This method Account out let number for selected radio buttons
    @AuraEnabled
    public static List<wrapperPayee> autoPopulateValues(string radiobut1,string radiobut2,string radiobut3,string radiobut4,string radiobut5,string radiobut6,string radiobut7,string payId,string billToId)
    {
        List<String> AccValues=new List<String>();
        string payerCode;
        if(radiobut1=='show')
        {  string payeeName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator();
         lstw.add(new wrapperPayee(payeeName,null,null));
        } else if(radiobut2=='show'){
            System.debug('Inside radiobut2');
            //string payeeName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.payerUpdatedValues(payId);
            string payeeName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator();
            string billName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator2();
            string soldName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator2();
            lstw.add(new wrapperPayee(payeeName,billName,soldName));
        }
        else if(radiobut3=='show'){
            System.debug('Inside radio button 4');
            string payeeName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator();
            string billName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator2();
            string soldName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator3();
            lstw.add(new wrapperPayee(payeeName,billName,soldName));
        }
        
        else if(radiobut4=='show'){
            System.debug('Inside radio3');
            string payeeName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator();
            string billName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator2();
            string soldName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator3();
            lstw.add(new wrapperPayee(payeeName,billName,soldName));
        }
        else if(radiobut5=='show'){
            //string payeeName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator();
            string payeeName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.payerUpdatedValues(payId);
            string billName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.billUpdatedValues(billToId);//Edited by Sourabh lakhera for Bill to code isssue 
            string soldName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator();
            lstw.add(new wrapperPayee(payeeName,billName,soldName));//Edited by Sourabh lakhera for Bill to code isssue 
        }
        else if(radiobut6=='show'){
            string payeeName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator();
            string payeeNumber=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.payerUpdatedValues(payId);
            lstw.add(new wrapperPayee(payeeNumber,payeeName,null));
        }
        else if(radiobut7=='show'){
            string soldName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator();
            string payeeName=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator2();
            string payeeNumber=SW_JJ_JPN_CustomerNewRegistrationExt_LEX.payerUpdatedValues(payId);
            
            lstw.add(new wrapperPayee(payeeNumber,payeeName,soldName));
        }
        return lstw;
    }
    //This method will be called to save the new CMR record
    @AuraEnabled
    public static List<errorWrapper>  save(JJ_JPN_CustomerMasterRequest__c cusdata,string payerId,string radVal,string billId,string solId,string samName,string typeAcc)
    {
        system.debug('cusdata'+cusdata);
        system.debug('typeAcc'+typeAcc);
        system.debug('soldid'+solId);
        system.debug('radVal'+radVal);
        String resultString='';
        string errorMessage = ''; 
        List<errorWrapper> errorWrapList = new List<errorWrapper>();
        JJ_JPN_CustomerMasterRequest__c cust=new JJ_JPN_CustomerMasterRequest__c();
        List<JJ_JPN_CustomerMasterRequest__c> custList=new List<JJ_JPN_CustomerMasterRequest__c>(); 
        try{
            if(radVal=='radio1'){
                if( (cusdata.JJ_JPN_PayerNameKanji__c!=null && cusdata.JJ_JPN_PayerNameKanji__c!='' ) && ( cusdata.JJ_JPN_PayerNameKana__c!=null && cusdata.JJ_JPN_PayerNameKana__c!=''))
                {
                    cusdata.JJ_JPN_SamManager__c = samName;
                    cusdata.JJ_JPN_AccountType__c = typeAcc;
                    
                    cusdata.JJ_JPN_AccountCreationType__c = System.Label.PayerBillToSoldToSameCode;
                    cusdata.JJ_JPN_InterfaceCheck__c = 1;
                    cusdata.Name = cusdata.JJ_JPN_PayerNameKanji__c;
                    cusdata.JJ_JPN_PayerCode__c = payerId;
                    cusdata.JJ_JPN_BillToCode__c =payerId;
                    cusdata.JJ_JPN_BillToTownshipCity__c = cusdata.JJ_JPN_PayerTownshipCity__c;
                    cusdata.JJ_JPN_BillToNameKanji__c = cusdata.JJ_JPN_PayerNameKanji__c;
                    cusdata.JJ_JPN_BillToStreet__c = cusdata.JJ_JPN_PayerStreet__c;
                    cusdata.JJ_JPN_BillToNameKana__c = cusdata.JJ_JPN_PayerNameKana__c;
                    cusdata.JJ_JPN_BillToOtherAddress__c = cusdata.JJ_JPN_PayerOtherAddress__c;
                    cusdata.JJ_JPN_BillToPostalCode__c= cusdata.JJ_JPN_PayerPostalCode__c;
                    cusdata.JJ_JPN_BillToTelephone__c = cusdata.JJ_JPN_PayerTelephone__c;
                    cusdata.JJ_JPN_BillToState__c = cusdata.JJ_JPN_PayerState__c;
                    cusdata.JJ_JPN_BillToFax__c = cusdata.JJ_JPN_PAYERFax__c;
                    
                    cusdata.JJ_JPN_SoldToCode__c = payerId;
                    cusdata.JJ_JPN_SoldToTownshipCity__c = cusdata.JJ_JPN_PayerTownshipCity__c;
                    cusdata.JJ_JPN_SoldToNameKanji__c = cusdata.JJ_JPN_PayerNameKanji__c;
                    cusdata.JJ_JPN_SoldToStreet__c = cusdata.JJ_JPN_PayerStreet__c;
                    cusdata.JJ_JPN_SoldToNameKana__c = cusdata.JJ_JPN_PayerNameKana__c;
                    cusdata.JJ_JPN_SoldToOtherAddress__c = cusdata.JJ_JPN_PayerOtherAddress__c;
                    cusdata.JJ_JPN_SoldToPostalCode__c= cusdata.JJ_JPN_PayerPostalCode__c;
                    cusdata.JJ_JPN_SoldToTelephone__c = cusdata.JJ_JPN_PayerTelephone__c;
                    cusdata.JJ_JPN_SoldToState__c = cusdata.JJ_JPN_PayerState__c;
                    cusdata.JJ_JPN_SoldToFax__c = cusdata.JJ_JPN_PAYERFax__c;
                    //default values on CMR-START
                    cusdata.JJ_JPN_PaymentCondition__c= 'ZJ42';
                    cusdata.JJ_JPN_PaymentMethod__c= 'D';
                    cusdata.JJ_JPN_DeliveryDocumentNote__c= '1';
                    cusdata.JJ_JPN_DirectDelivery__c= '10';
                    cusdata.JJ_JPN_BillSubmission__c= 'ZYNR';
                    cusdata.JJ_JPN_FestivalDelivery__c= 'J4';
                    cusdata.JJ_JPN_SubCustomerGroup__c= '';
                    cusdata.JJ_JPN_ItemizedBilling__c= '1';
                    cusdata.JJ_JPN_SalesItem__c = 'blank';
                    custList.add(cusdata);
                    AutoNumberCMR__c autoCMR = AutoNumberCMR__c.getInstance('Account Outlet');
                    autoCMR.JJ_JPN_AutoNumberOutlet__c = Decimal.ValueOf(randomNumberGenerator());
                    Update autoCMR;            
                }
            }
            else if(radVal=='radio2'){
                
                if( (cusdata.JJ_JPN_PayerNameKanji__c!=null && cusdata.JJ_JPN_PayerNameKanji__c!='' ) && ( cusdata.JJ_JPN_PayerNameKana__c!=null && cusdata.JJ_JPN_PayerNameKana__c!=''))
                {
                    cusdata.JJ_JPN_SamManager__c = samName;
                    cusdata.JJ_JPN_AccountType__c = typeAcc;
                    cusdata.JJ_JPN_AccountCreationType__c = System.Label.PyrBillWithSameCodeSoldDiffCode;
                    cusdata.JJ_JPN_InterfaceCheck__c = 2;
                    cusdata.Name = cusdata.JJ_JPN_PayerNameKanji__c;
                    cusdata.JJ_JPN_PayerCode__c=payerId;
                    cusdata.JJ_JPN_SoldToCode__c=solId;
                    cusdata.JJ_JPN_PaymentCondition__c= 'ZJ42';
                    cusdata.JJ_JPN_PaymentMethod__c= 'D';
                    cusdata.JJ_JPN_DeliveryDocumentNote__c= '1';
                    cusdata.JJ_JPN_DirectDelivery__c= '10';
                    cusdata.JJ_JPN_BillSubmission__c= 'ZYNR';
                    cusdata.JJ_JPN_FestivalDelivery__c= 'J4';
                    cusdata.JJ_JPN_SubCustomerGroup__c= '';
                    cusdata.JJ_JPN_ItemizedBilling__c= '1';
                    cusdata.JJ_JPN_OrderBlock__c='X';
                    cusdata.JJ_JPN_SalesItem__c = 'blank';
                    cusdata.JJ_JPN_BillToCode__c = cusdata.JJ_JPN_PayerCode__c;
                    cusdata.JJ_JPN_BillToTownshipCity__c = cusdata.JJ_JPN_PayerTownshipCity__c;
                    cusdata.JJ_JPN_BillToNameKanji__c = cusdata.JJ_JPN_PayerNameKanji__c;
                    cusdata.JJ_JPN_BillToStreet__c = cusdata.JJ_JPN_PayerStreet__c;
                    cusdata.JJ_JPN_BillToNameKana__c = cusdata.JJ_JPN_PayerNameKana__c;
                    cusdata.JJ_JPN_BillToOtherAddress__c = cusdata.JJ_JPN_PayerOtherAddress__c;
                    cusdata.JJ_JPN_BillToPostalCode__c= cusdata.JJ_JPN_PayerPostalCode__c;
                    cusdata.JJ_JPN_BillToTelephone__c = cusdata.JJ_JPN_PayerTelephone__c;
                    cusdata.JJ_JPN_BillToState__c = cusdata.JJ_JPN_PayerState__c;
                    cusdata.JJ_JPN_BillToFax__c = cusdata.JJ_JPN_PAYERFax__c;
                    //default values on CMR-END 
                    AutoNumberCMR__c autoCMR = AutoNumberCMR__c.getInstance('Account Outlet');
                    autoCMR.JJ_JPN_AutoNumberOutlet__c = Decimal.ValueOf(randomNumberGenerator2());
                    Update autoCMR;
                    system.debug('custoooo'+cusdata);
                    custList.add(cusdata);
                }
            }
            
            else if(radVal=='radio3'){
                System.debug('Inside radio button 4');
                if( (cusdata.JJ_JPN_PayerNameKanji__c!=null && cusdata.JJ_JPN_PayerNameKanji__c!='' ) && ( cusdata.JJ_JPN_PayerNameKana__c!=null && cusdata.JJ_JPN_PayerNameKana__c!=''))
                {
                    cusdata.JJ_JPN_SamManager__c = samName;
                    cusdata.JJ_JPN_AccountType__c = typeAcc;
                    system.debug('Customer Name'+cusdata.JJ_JPN_PayerNameKanji__c);
                    cusdata.JJ_JPN_AccountCreationType__c = System.Label.BillSoldWithSameCodePyrDiffCode;
                    cusdata.JJ_JPN_InterfaceCheck__c = 4;
                    cusdata.Name = cusdata.JJ_JPN_PayerNameKanji__c;
                    cusdata.JJ_JPN_PayerCode__c = payerId;
                    cusdata.JJ_JPN_SoldToCode__c =solId;
                    cusdata.JJ_JPN_BillToCode__c = billId;
                    
                    /*cusdata.JJ_JPN_SoldToTownshipCity__c = cusdata.JJ_JPN_BillToTownshipCity__c;
cusdata.JJ_JPN_SoldToNameKanji__c = cusdata.JJ_JPN_BillToNameKanji__c;
cusdata.JJ_JPN_SoldToStreet__c = cusdata.JJ_JPN_BillToStreet__c;
cusdata.JJ_JPN_SoldToNameKana__c = cusdata.JJ_JPN_BillToNameKana__c;
cusdata.JJ_JPN_SoldToOtherAddress__c = cusdata.JJ_JPN_BillToOtherAddress__c;
cusdata.JJ_JPN_SoldToPostalCode__c= cusdata.JJ_JPN_BillToPostalCode__c;
cusdata.JJ_JPN_SoldToTelephone__c = cusdata.JJ_JPN_BillToTelephone__c;
cusdata.JJ_JPN_SoldToState__c = cusdata.JJ_JPN_BillToState__c;
cusdata.JJ_JPN_SoldToFax__c = cusdata.JJ_JPN_BillToFax__c;*/
                    
                    //default values on CMR-START
                    cusdata.JJ_JPN_PaymentCondition__c= 'ZJ42';
                    cusdata.JJ_JPN_PaymentMethod__c= 'D';
                    cusdata.JJ_JPN_DeliveryDocumentNote__c= '1';
                    cusdata.JJ_JPN_DirectDelivery__c= '10';
                    cusdata.JJ_JPN_BillSubmission__c= 'ZYNR';
                    cusdata.JJ_JPN_FestivalDelivery__c= 'J4';
                    cusdata.JJ_JPN_SubCustomerGroup__c= '';
                    cusdata.JJ_JPN_ItemizedBilling__c= '1';
                    cusdata.JJ_JPN_OrderBlock__c='X';
                    cusdata.JJ_JPN_SalesItem__c = 'blank';
                    //default values on CMR-END 
                    AutoNumberCMR__c autoCMR = AutoNumberCMR__c.getInstance('Account Outlet');
                    autoCMR.JJ_JPN_AutoNumberOutlet__c = Decimal.ValueOf(randomNumberGenerator2());
                    Update autoCMR;
                    custList.add(cusdata);
                    system.debug('Customer Name'+cust);
                }
            }
            else if(radVal=='radio4'){
                System.debug('Inside radio4');
                if( (cusdata.JJ_JPN_PayerNameKanji__c!=null && cusdata.JJ_JPN_PayerNameKanji__c!='' ) && ( cusdata.JJ_JPN_PayerNameKana__c!=null && cusdata.JJ_JPN_PayerNameKana__c!=''))
                {   cusdata.JJ_JPN_SamManager__c = samName;
                 cusdata.JJ_JPN_AccountType__c = typeAcc;
                 cust=new JJ_JPN_CustomerMasterRequest__c();
                 cusdata.JJ_JPN_AccountCreationType__c = System.Label.PyrBillSoldWithDiffCode;
                 cusdata.JJ_JPN_InterfaceCheck__c = 3;
                 cusdata.Name = cusdata.JJ_JPN_PayerNameKanji__c;
                 cusdata.JJ_JPN_PayerCode__c=payerId;
                 cusdata.JJ_JPN_BillToCode__c = billId;
                 cusdata.JJ_JPN_SoldToCode__c =billId;
                 cusdata.JJ_JPN_SoldToTownshipCity__c = cusdata.JJ_JPN_BillToTownshipCity__c;
                 cusdata.JJ_JPN_SoldToNameKanji__c = cusdata.JJ_JPN_BillToNameKanji__c;
                 cusdata.JJ_JPN_SoldToStreet__c = cusdata.JJ_JPN_BillToStreet__c;
                 cusdata.JJ_JPN_SoldToNameKana__c = cusdata.JJ_JPN_BillToNameKana__c;
                 cusdata.JJ_JPN_SoldToOtherAddress__c = cusdata.JJ_JPN_BillToOtherAddress__c;
                 cusdata.JJ_JPN_SoldToPostalCode__c= cusdata.JJ_JPN_BillToPostalCode__c;
                 cusdata.JJ_JPN_SoldToTelephone__c = cusdata.JJ_JPN_BillToTelephone__c;
                 cusdata.JJ_JPN_SoldToState__c = cusdata.JJ_JPN_BillToState__c;
                 cusdata.JJ_JPN_SoldToFax__c = cusdata.JJ_JPN_BillToFax__c;
                 //default values on CMR-START
                 cusdata.JJ_JPN_PaymentCondition__c= 'ZJ42';
                 cusdata.JJ_JPN_PaymentMethod__c= 'D';
                 cusdata.JJ_JPN_DeliveryDocumentNote__c= '1';
                 cusdata.JJ_JPN_DirectDelivery__c= '10';
                 cusdata.JJ_JPN_BillSubmission__c= 'ZYNR';
                 cusdata.JJ_JPN_FestivalDelivery__c= 'J4';
                 cusdata.JJ_JPN_SubCustomerGroup__c= '';
                 cusdata.JJ_JPN_ItemizedBilling__c= '1';
                 cusdata.JJ_JPN_OrderBlock__c='X';
                 cusdata.JJ_JPN_SalesItem__c = 'blank';
                 //default values on CMR-END 
                 AutoNumberCMR__c autoCMR = AutoNumberCMR__c.getInstance('Account Outlet');
                 autoCMR.JJ_JPN_AutoNumberOutlet__c = Decimal.ValueOf(SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator3());
                 Update autoCMR;
                 custList.add(cusdata);
                }    
            }
            else if(radVal=='radio5')
            {
                System.debug('radVal'+radVal);
                System.debug('Inside radVal'+cusdata);
                if( (cusdata.JJ_JPN_PayerNameKanji__c!=null && cusdata.JJ_JPN_PayerNameKanji__c!='' ) && ( cusdata.JJ_JPN_PayerNameKana__c!=null && cusdata.JJ_JPN_PayerNameKana__c!=''))
                {
                    cusdata.JJ_JPN_SamManager__c = samName;
                    cusdata.JJ_JPN_AccountType__c = typeAcc;
                    cusdata.JJ_JPN_AccountCreationType__c = System.Label.SoldToOnlyPyrBillToAlreadyRegist;
                    cusdata.JJ_JPN_InterfaceCheck__c = 5;
                    cusdata.JJ_JPN_SoldToCode__c =solId;
                    cusdata.JJ_JPN_PayerCode__c=payerId;
                    cusdata.JJ_JPN_BillToCode__c =billId;
                    cusdata.Name = cusdata.JJ_JPN_PayerNameKanji__c;
                    //cusdata.JJ_JPN_SubCustomerGroup__c= cusdata.JJ_JPN_SubCustomerGroup__c;
                    //default values on CMR-START
                    /*cusdata.JJ_JPN_PaymentCondition__c= '';
cusdata.JJ_JPN_PaymentMethod__c= 'W';
cusdata.JJ_JPN_DirectDelivery__c ='';
cusdata.JJ_JPN_BillToRTNFAX__c ='';
cusdata.JJ_JPN_TransactionDetailSubmission__c='';
cusdata.JJ_JPN_BillSubmission__c ='';*/
                    //cusdata.JJ_JPN_BillToRTNFAX__c= cusdata.JJ_JPN_BillToRTNFAX__c;
                    cusdata.JJ_JPN_TransactionDetailSubmission__c='';
                    /*cusdata.JJ_JPN_DeliveryDocumentNote__c= '1';
                    //cusdata.JJ_JPN_DirectDelivery__c= '1';
                    cusdata.JJ_JPN_BillSubmission__c= 'ZYNR';
                    cusdata.JJ_JPN_FestivalDelivery__c= 'J4';
                    cusdata.JJ_JPN_ItemizedBilling__c= '1';
                    cusdata.JJ_JPN_SalesItem__c = 'blank';
                    cusdata.JJ_JPN_TransactionDetailSubmission__c='';*/
                    //cusdata.JJ_JPN_EDIFlag__c ='7';
                    //cusdata.JJ_JPN_EDIStoreCode__c='8032000101';
                    //cusdata.JJ_JPN_FAXOrder__c ='2';
                    //default values on CMR-END 
                    /*cusdata.JJ_JPN_BillToTownshipCity__c = cusdata.JJ_JPN_PayerTownshipCity__c;
cusdata.JJ_JPN_BillToNameKanji__c = cusdata.JJ_JPN_PayerNameKanji__c;
cusdata.JJ_JPN_BillToStreet__c = cusdata.JJ_JPN_PayerStreet__c;
cusdata.JJ_JPN_BillToNameKana__c = cusdata.JJ_JPN_PayerNameKana__c;
cusdata.JJ_JPN_BillToOtherAddress__c = cusdata.JJ_JPN_PayerOtherAddress__c;
cusdata.JJ_JPN_BillToPostalCode__c= cusdata.JJ_JPN_BillToPostalCode__c;
cusdata.JJ_JPN_BillToTelephone__c = cusdata.JJ_JPN_PayerTelephone__c;
cusdata.JJ_JPN_BillToState__c = cusdata.JJ_JPN_PayerState__c;
cusdata.JJ_JPN_BillToFax__c = cusdata.JJ_JPN_PAYERFax__c;*/
                    AutoNumberCMR__c autoCMR = AutoNumberCMR__c.getInstance('Account Outlet');
                    autoCMR.JJ_JPN_AutoNumberOutlet__c = Decimal.ValueOf(SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator());
                    Update autoCMR;
                    system.debug(cusdata);
                    custList.add(cusdata);
                }
            }
            else if(radVal=='radio6')
            {
                if( (cusdata.JJ_JPN_PayerNameKanji__c!=null && cusdata.JJ_JPN_PayerNameKanji__c!='' ) && ( cusdata.JJ_JPN_PayerNameKana__c!=null && cusdata.JJ_JPN_PayerNameKana__c!=''))
                {
                    cusdata.JJ_JPN_SamManager__c = samName;
                    cusdata.JJ_JPN_AccountType__c = typeAcc;
                    cusdata.JJ_JPN_AccountCreationType__c = System.Label.BillToOnlyPyrAlrdyRegSoldToSameAsPyr;
                    cusdata.JJ_JPN_InterfaceCheck__c = 6;
                    cusdata.JJ_JPN_SalesItem__c = 'blank';
                    cusdata.JJ_JPN_PayerCode__c=payerId;
                    cusdata.Name = cusdata.JJ_JPN_PayerNameKanji__c;
                    cusdata.JJ_JPN_BillToCode__c = billId;
                    cusdata.JJ_JPN_SoldToCode__c = cusdata.JJ_JPN_BillToCode__c;
                    cusdata.JJ_JPN_SoldToTownshipCity__c = cusdata.JJ_JPN_BillToTownshipCity__c;
                    cusdata.JJ_JPN_SoldToNameKanji__c = cusdata.JJ_JPN_BillToNameKanji__c;
                    cusdata.JJ_JPN_SoldToStreet__c = cusdata.JJ_JPN_BillToStreet__c;
                    cusdata.JJ_JPN_SoldToNameKana__c = cusdata.JJ_JPN_BillToNameKana__c;
                    cusdata.JJ_JPN_SoldToOtherAddress__c = cusdata.JJ_JPN_BillToOtherAddress__c;
                    cusdata.JJ_JPN_SoldToPostalCode__c= cusdata.JJ_JPN_BillToPostalCode__c;
                    cusdata.JJ_JPN_SoldToTelephone__c = cusdata.JJ_JPN_BillToTelephone__c;
                    cusdata.JJ_JPN_SoldToState__c = cusdata.JJ_JPN_BillToState__c;
                    cusdata.JJ_JPN_SoldToFax__c = cusdata.JJ_JPN_BillToFax__c;
                    //cusdata.JJ_JPN_EDIFlag__c ='3';
                    //cusdata.JJ_JPN_FAXOrder__c ='8';
                    cusdata.JJ_JPN_DeliveryDocumentNote__c='1';
                    cusdata.JJ_JPN_BillToRTNFAX__c	='';
                    cusdata.JJ_JPN_TransactionDetailSubmission__c	='';
                    //cusdata.JJ_JPN_DirectDelivery__c	='';
                    /*cusdata.JJ_JPN_PaymentCondition__c ='';
cusdata.JJ_JPN_PaymentMethod__c ='';
cusdata.JJ_JPN_DirectDelivery__c='';
cusdata.JJ_JPN_FestivalDelivery__c='';
cusdata.JJ_JPN_BillToRTNFAX__c='';
cusdata.JJ_JPN_TransactionDetailSubmission__c='';
cusdata.JJ_JPN_BillSubmission__c='';*/
                    
                    AutoNumberCMR__c autoCMR = AutoNumberCMR__c.getInstance('Account Outlet');
                    autoCMR.JJ_JPN_AutoNumberOutlet__c = Decimal.ValueOf(SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator());
                    Update autoCMR;
                    custList.add(cusdata);
                }
            }
            else if(radVal=='radio7')
            {
                if( (cusdata.JJ_JPN_PayerNameKanji__c!=null && cusdata.JJ_JPN_PayerNameKanji__c!='' ) && ( cusdata.JJ_JPN_PayerNameKana__c!=null && cusdata.JJ_JPN_PayerNameKana__c!=''))
                {
                    cusdata.JJ_JPN_SamManager__c = samName;
                    cusdata.JJ_JPN_AccountType__c = typeAcc;
                    cusdata.JJ_JPN_AccountCreationType__c = System.Label.BillToSoldToPyrAlrdyReg;
                    cusdata.JJ_JPN_InterfaceCheck__c = 7;
                    cusdata.Name = cusdata.JJ_JPN_PayerNameKanji__c;
                    cusdata.JJ_JPN_PayerCode__c=payerId;
                    cusdata.JJ_JPN_SoldToCode__c =solId;
                    cusdata.JJ_JPN_BillToCode__c = billId;
                    //cusdata.JJ_JPN_EDIFlag__c ='3';
                    //cusdata.JJ_JPN_FAXOrder__c ='8';
                    cusdata.JJ_JPN_DeliveryDocumentNote__c='1';
                    cusdata.JJ_JPN_BillToRTNFAX__c	='';
                    cusdata.JJ_JPN_TransactionDetailSubmission__c	='';
                    //cusdata.JJ_JPN_DirectDelivery__c	='';
                    /*cusdata.JJ_JPN_PaymentCondition__c ='';
cusdata.JJ_JPN_PaymentMethod__c ='';
cusdata.JJ_JPN_DirectDelivery__c='';
cusdata.JJ_JPN_FestivalDelivery__c='';
cusdata.JJ_JPN_BillToRTNFAX__c='';
cusdata.JJ_JPN_TransactionDetailSubmission__c='';
cusdata.JJ_JPN_BillSubmission__c='';*/
                    AutoNumberCMR__c autoCMR = AutoNumberCMR__c.getInstance('Account Outlet');
                    autoCMR.JJ_JPN_AutoNumberOutlet__c = Decimal.ValueOf(SW_JJ_JPN_CustomerNewRegistrationExt_LEX.randomNumberGenerator2());
                    Update autoCMR;
                    
                    custList.add(cusdata);
                }
            }
            system.debug('custList'+custList);
            Database.SaveResult[] srList = Database.insert(custList, false);  
            Set<String> errStr = new Set<String>();
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) { System.debug('Successfully inserted account. Account ID: ' + sr.getId());
                                     errorWrapList.add(new errorWrapper('SUCCESS',sr.getId()));
                                    }else{
                                        
                                        for(Database.Error err : sr.getErrors()) {
                                            system.debug('field values'+err.getFields());
                                            if(!errStr.contains(err.getMessage())) {
                                                /*if(err.getFields()=='JJ_JPN_BillToNameKanji__c' || err.getFields()=='JJ_JPN_PayerNameKanji__c' || err.getFields()=='JJ_JPN_SoldToNameKanji__c')
{
errorWrapList.add(new errorWrapper('Error',err.getMessage()));  
}*/
                                                //string errorMessgaes=err.getMessage();
                                                //errorWrapList.add(new errorWrapper('Error',err.getMessage()));
                                                // errorWrapList.add(new errorWrapper('Error',err.getMessage())); 
                                                //errStr.add(err.getMessage());
                                                system.debug(err);
                                                system.debug(err.getFields());
                                                for(string st:err.getFields())
                                                {
                                                    if(!err.getMessage().contains('large')){
                                                        if(st=='JJ_JPN_BillToNameKanji__c' || st=='JJ_JPN_PayerNameKanji__c' || st=='JJ_JPN_SoldToNameKanji__c')
                                                        {
                                                            errorWrapList.add(new errorWrapper('Error','Name (Kanji) :'+err.getMessage())); 
                                                            errStr.add(err.getMessage());
                                                        }  
                                                        if(st.contains('JJ_JPN_AccountType__c'))
                                                        {
                                                            errorWrapList.add(new errorWrapper('Error','得意先種類: SAM Managers are not allowed to create FS Type Account')); 
                                                            errStr.add(err.getMessage());
                                                        }
                                                        if(st.contains('Telephone__c'))
                                                        {
                                                            errorWrapList.add(new errorWrapper('Error','Telephone Code :'+err.getMessage())); 
                                                            errStr.add(err.getMessage());
                                                        }
                                                        if(st.contains('PostalCode__c') )
                                                        {
                                                            errorWrapList.add(new errorWrapper('Error','Postal Code :'+err.getMessage())); 
                                                            errStr.add(err.getMessage());
                                                        }
                                                        if(st.contains('Fax__c'))
                                                        {
                                                            errorWrapList.add(new errorWrapper('Error','Fax Code :'+err.getMessage())); 
                                                            errStr.add(err.getMessage());
                                                        }
                                                        else{
                                                            errorWrapList.add(new errorWrapper('Error',err.getMessage())); 
                                                            errStr.add(err.getMessage());
                                                        }
                                                    }else{
                                                        errorWrapList.add(new errorWrapper('Error',err.getMessage())); 
                                                        errStr.add(err.getMessage());
                                                    }
                                                    
                                                }
                                                
                                            }   
                                        }}
            }
        }
        catch(Exception e)
        {
            System.debug('e.getMessage()'+e);
            System.debug('e.getMessage()'+e.getMessage());
            errorWrapList.add(new errorWrapper('Error',e.getMessage()));
        }
        system.debug('Error message'+resultString);
        return errorWrapList;
    }
    //this wrapper class returns the error values
    public class errorWrapper{
        @AuraEnabled
        public String type{get;set;}
        @AuraEnabled
        public String value{get;set;}
        public errorWrapper(String type, String value){
            this.type = type;
            this.value = value;
        }
    }
    //This function will be called to get the Account values based on the selected record from look up
    @AuraEnabled
    public static string payerUpdatedValues(string payerId)
    {   Account acc=[select Name,AccountNumber from Account where id=:payerId];
     string accName=acc.AccountNumber;
     return accName;
    }
    
    //Added by Sourabh lakhera for Bill to code isssue 
    @AuraEnabled
    public static string billUpdatedValues(string billToAccID)
    {  
        Account acc=[SELECT id , JJ_JPN_BillToCode__c FROM Account where id=:billToAccID ];
        string accName=acc.JJ_JPN_BillToCode__c ;
        return accName;
    }
    //This function will be called to get the Account values based on the selected record from look up
    @AuraEnabled
    public static Account autoPopulateAccValues(string accid)
    { 
        return [SELECT id ,name,payment_terms__c,JJ_JPN_SubCustomerGroup__c,JJ_JPN_CustomerOrderNoNumberofDigits__c,JJ_JPN_PaymentMethod__c,JJ_JPN_DeliveryDocumentNote__c,JJ_JPN_BillToRTNFAXHowmanytimestosend__c,JJ_JPN_EDI_Flag__c,JJ_JPN_FAXOrder__c,JJ_JPN_PurchaseOrderNumberRequired__c,Call_Frequency_Cycle__c,JJ_JPN_DSO__c,JJ_JPN_DirectDeliveryReturned__c,JJ_JPN_TransactionDetailSubmission__c,JJ_JPN_ADSHandlingFlag__c,Sub_Customer_Group__c,JJ_JPN_FestivalDelivery__c,JJ_JPN_EDIStoreCode__c,JJ_JPN_BillSubmission__c,JJ_JPN_ItemizedBilling__c,JJ_JPN_CreditPersonInchargeName__c, AccountNumber, Phone, Fax, JJ_JPN_CustomerNameKana__c, ShippingPostalCode,Address2__c, Address3__c, ShippingCountry, JJ_JPN_OtherAddress__c,Address1__c,JJ_JPN_isPayerAcc__c  FROM Account where id=:accid ];
    }
    //This function will be called to get the Account values based on the selected record from look up
    @AuraEnabled
    public static Account autoPopulateAccBillValues(string accid)
    { 
        return [SELECT id , name, Phone, Fax, JJ_JPN_BillToCode__c,JJ_JPN_CustomerNameKana__c,ShippingPostalCode,Address2__c, Address3__c, ShippingCountry, Address1__c,JJ_JPN_OtherAddress__c,JJ_JPN_BillToNameKanji__c FROM Account where id=:accid ];
    }
}