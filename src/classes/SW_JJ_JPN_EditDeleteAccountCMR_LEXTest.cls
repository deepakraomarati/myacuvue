@isTest
Private class SW_JJ_JPN_EditDeleteAccountCMR_LEXTest 
{
  static testMethod void TestautoPopulateAccValues()
    {
        Account acc=new Account(Name='Test1');
        insert  acc;
        List<String> val = new List<String>{'JJ_JPN_PaymentCondition__c','JJ_JPN_PaymentCondition__c','JJ_JPN_SubCustomerGroup__c'};
        Map<String, List<String>> pickListVal = new Map<String, List<String>>();
        pickListVal.put('JJ_JPN_CustomerMasterRequest__c' , val);
        test.startTest();
        
        SW_JJ_JPN_EditDeleteAccountFromCMR_LEX.CMRWrapper CMRWrapper= SW_JJ_JPN_EditDeleteAccountFromCMR_LEX.autoPopulateAccValues(acc.ID,JSON.serialize(pickListVal));
        system.assertEquals(CMRWrapper.objFieldPicklistMap.keyset() , pickListVal.keyset());
        test.stopTest();
    }
    
 Public Static Testmethod void TestdoSave()
    {
       Account acc=new Account(Name='Test2');
        insert  acc;
        
        JJ_JPN_CustomerMasterRequest__c cmr=new JJ_JPN_CustomerMasterRequest__c(Name='Test CMR1');
        insert  cmr;
        
        test.startTest();
        SW_JJ_JPN_EditDeleteAccountFromCMR_LEX.errorWrapper error = SW_JJ_JPN_EditDeleteAccountFromCMR_LEX.doSave(cmr, acc.Id);
        system.assertEquals(error.value , 'Please Select Status Code and Add Activation Date');
        test.stopTest();
    }

 Public Static Testmethod void TestdoSavePostalNull()
    {
         Account acc=new Account(Name='Test3');
        insert  acc;
        
        JJ_JPN_CustomerMasterRequest__c cmr=new JJ_JPN_CustomerMasterRequest__c(Name='Test CMR2', JJ_JPN_StatusCode__c='A', Account_Activation_Start_Date__c=system.today());
        insert  cmr;
        
        test.startTest();
        SW_JJ_JPN_EditDeleteAccountFromCMR_LEX.errorWrapper error = SW_JJ_JPN_EditDeleteAccountFromCMR_LEX.doSave(cmr, acc.Id);
        system.assertEquals(error.value , 'Postal Code Sold To: Validation Error: Value is required');
        test.stopTest();
    }


Public Static Testmethod void TestdoSaveException()
    {
        Account acc=new Account(Name='Test4');
        insert  acc;
        
        JJ_JPN_CustomerMasterRequest__c cmr=new JJ_JPN_CustomerMasterRequest__c(Name='Test CMR3');
        insert  cmr;
        
        test.startTest();
        SW_JJ_JPN_EditDeleteAccountFromCMR_LEX.errorWrapper error = SW_JJ_JPN_EditDeleteAccountFromCMR_LEX.doSave(cmr, acc.Id);
        system.assertEquals(error.type , 'Error');
        test.stopTest();
    }

Public Static Testmethod void TestdoSaveInsert()
      {
        Account acc=new Account(Name='Test5');
        insert  acc;
        
        JJ_JPN_CustomerMasterRequest__c cmr=new JJ_JPN_CustomerMasterRequest__c(Name='Test CMR4', JJ_JPN_StatusCode__c='A', Account_Activation_Start_Date__c=system.today(),JJ_JPN_SoldToPostalCode__c='7894521');
        insert  cmr;
        
        test.startTest();
        SW_JJ_JPN_EditDeleteAccountFromCMR_LEX.errorWrapper error = SW_JJ_JPN_EditDeleteAccountFromCMR_LEX.doSave(cmr, acc.Id);
        system.assertEquals(error.type , 'Error');
        test.stopTest();
    }
    
    Public Static Testmethod void TestaccessDenied()
    {
        test.startTest();
        Boolean isNotAdmin = SW_JJ_JPN_EditDeleteAccountFromCMR_LEX.accessDenied();
        system.assertEquals(isNotAdmin,true);
        test.stopTest();
    }
 
}