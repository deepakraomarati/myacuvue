public with sharing class SW_JJ_JPN_EditDeleteAccountFromCMR_LEX {
    
    public Static Account accListToEditOrDel;
    Public  Account AccRec{get;set;}
    
    
    public class pickListValues{
        @AuraEnabled public String value{get;set;}
        @AuraEnabled public String label{get;set;}
        
        public pickListValues(String label, String value){
            this.label = label;
            this.value = value;
        }
    }
    @AuraEnabled 
    public static Map<String, Map<String, List<pickListValues>>>  getPicklistValues(String objpicklistFieldsMap)
    {
        Map<String, List<String>> objPickmap = (Map<String, List<String>>)JSON.deserialize(objpicklistFieldsMap, Map<String, List<String>>.class);
        system.debug('objpickmap ' + objPickmap);
        
        Map<String, Map<String, List<pickListValues>>> objFieldPicklistMap = new Map<String, Map<String, List<pickListValues>>>();
        List<String> sobjectslist = new List<String>(objPickmap.keySet());
        system.debug('sobjectslist ' + sobjectslist);
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(sobjectslist);
        system.debug('results ' + results);
        Map<String,List<pickListValues>> fieldOptionsMap;
        try{
            for(Schema.DescribeSObjectResult result : results){
                fieldOptionsMap = new Map<String,  List<pickListValues>>();
                
                Schema.sObjectType objType = result.getSObjectType();
                
                Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
                map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
                List<String> objFieldlist = objPickmap.get(result.getName());
                system.debug('objname ' + result.getName());
                system.debug('list of fields ' + objFieldlist);
                for(String fld : objFieldlist){
                    Boolean defaultVal = false;
                    system.debug('fields ' + fld);
                    List<String > allOpts = new list<String>();
                    //Map <String,String> mapValues =new Map<String,String>();
                    list<Schema.PicklistEntry> values =
                        fieldMap.get(fld).getDescribe().getPickListValues();
                    System.debug('values'+values);
                    List<pickListValues> lstFinalpickListValuesWrapper =new  List<pickListValues>();
                    List<pickListValues> lstpickListValuesWrapper =new  List<pickListValues>();
                    pickListValues pickListValuesWrapper = new  pickListValues(Label.None, '');
                    lstpickListValuesWrapper.add(pickListValuesWrapper);
                    for (Schema.PicklistEntry a : values){
                        if(a.isDefaultValue()){
                            defaultVal = true;
                            pickListValuesWrapper = new  pickListValues(a.getLabel(), a.getValue());
                            lstFinalpickListValuesWrapper.add(pickListValuesWrapper);
                        }
                        else{
                            pickListValuesWrapper = new  pickListValues(a.getLabel(), a.getValue());
                            lstpickListValuesWrapper.add(pickListValuesWrapper);
                        }
                        //mapValues.put(a.getValue(),a.getLabel());
                    }
                    if(defaultVal){
                        lstpickListValuesWrapper.remove(0);
                        pickListValuesWrapper = new  pickListValues(Label.None, '');
                        lstpickListValuesWrapper.add(pickListValuesWrapper);
                    }
                    lstFinalpickListValuesWrapper.addAll(lstpickListValuesWrapper);
                    fieldOptionsMap.put(fld, lstFinalpickListValuesWrapper);
                }
                objFieldPicklistMap.put(result.getName(), fieldOptionsMap);
            }
        }catch(AuraHandledException e){
            throw e;
        }
        return objFieldPicklistMap;
    }
    @AuraEnabled 
    public static boolean accessDenied(){
        try{
            String profileName = [Select Name from Profile where Id =: UserInfo.getProfileId()].Name;
            return (profileName =='ASPAC JPN Sales Admin' || profileName =='System Administrator');
        }Catch(AuraHandledException e){
            throw e;
        }
    }
    
    @AuraEnabled 
    public static CMRWrapper autoPopulateAccValues(String acc1, String objpicklistFieldsMap)
    {
        System.debug('acc1'+acc1);
        System.debug('objpicklistFieldsMap'+objpicklistFieldsMap);
        JJ_JPN_CustomerMasterRequest__c CMR = new JJ_JPN_CustomerMasterRequest__c();
        try{
            accListToEditOrDel= [SELECT id , name, AccountNumber, Description, OutletNumber__c,Territory1__c,subCustGroup__c, Mobile_Number__c,Phone, Fax, JJ_JPN_CustomerNameKana__c, ShippingPostalCode,Address2__c, Address3__c, ShippingCountry, JJ_JPN_OtherAddress__c,Address1__c,JJ_JPN_isPayerAcc__c,Status__c,JJ_JPN_BillToCode__c,JJ_JPN_BillToNameKanji__c,JJ_JPN_SalesItemDxDemoItemRForSale__c,JJ_JPN_Rank__c,JJ_JPN_SubTradeChannel__c,Sub_Customer_Group__c,JJ_JPN_SAMDESC__c,JJ_JPN_FieldSalesDistrictID__c,JJ_JPN_OfficeName__c,JJ_JPN_ADSHandlingFlag__c,JJ_JPN_ShopName__c,JJ_JPN_BillingCloseDate__c,JJ_JPN_PaymentMethod__c,JJ_JPN_CustomerRTNFAXHowmanytimestosend__c,JJ_JPN_BillToRTNFAXHowmanytimestosend__c,JJ_JPN_SoldToNumberOfPagesReceived__c,JJ_JPN_TransactionDetailSubmission__c,JJ_JPN_BillingSummaryTableSubmission__c,JJ_JPN_ItemizedBilling__c,JJ_JPN_BillSubmission__c,JJ_JPN_LotNumberCommunicationDocument__c,JJ_JPN_DeliveryDocumentNote__c,JJ_JPN_EDIStoreCode__c,JJ_JPN_EDI_Flag__c,JJ_JPN_FAXOrder__c,JJ_JPN_PurchaseOrderNumberRequired__c,JJ_JPN_DeadlineTime__c,JJ_JPN_NoDeliveryCharges__c,JJ_JPN_Solution_Flag__c,JJ_JPN_FestivalDelivery__c,JJ_JPN_IndividualDeliveryCommentsMon__c,IndividualDeliveryCommentsTues__c,IndividualDeliveryCommentsWed__c,JJ_JPN_IndividualDeliveryCommentsThu__c,JJ_JPN_IndividualDeliveryCommentsFri__c,JJ_JPN_IndividualDeliveryCommentsSat__c,JJ_JPN_IndividualDeliveryCommentsSun__c,Customer_Location__c,JJ_JPN_IndividualDeliveryPossible__c,JJ_JPN_IndividualDeliveryPossibleTues__c,JJ_JPN_IndividualDeliveryPossibleWed__c,JJ_JPN_IndividualDeliveryPossibleThurs__c,JJ_JPN_IndividualDeliveryPossibleDa__c,JJ_JPN_IndividualDeliveryPossibleDaSat__c,JJ_JPN_IndividualDeliveryPossibleSun__c,Last_update_date__c,CreditLimit__c,payment_terms__c,Customer_type_description__c,JJ_JPN_Website__c,JJ_JPN_Email__c,SalesRep__c,JJ_JPN_BusinessLicenceOwnerName__c,JJ_JPN_LicenseNumber__c,JJ_JPN_RepresentativeNameKanji__c,JJ_JPN_RepresentativeNameKana__c,JJ_JPN_FacilityInformation__c,JJ_JPN_PersonInchargeName__c,JJ_JPN_SubCustomerGroup__c,JJ_JPN_CreditPersonInchargeName__c,JJ_JPN_OrderBlock__c,JJ_JPN_DirectDeliveryReturned__c,JJ_JPN_CustomerOrderNoNumberofDigits__c,JJ_JPN_DSO__c,JJ_JPN_Validity__c,JJ_JPN_CLIssue__c,JJ_JPN_ContractDate__c,JJ_JPN_ContractType__c,JJ_JPN_CompanyName__c,JJ_JPN_RepresentativeNameFranchise__c,JJ_JPN_POSDevision__c,JJ_JPN_GroupName__c,JJ_JPN_ReturnedGoodMethod__c,JJ_JPN_DirectDeliveryLeafIncludeNG__c,JJ_JPN_OutboundNG__c,JJ_JPN_ReservedCampaignFlag1__c,JJ_JPN_ThoughtOnNetBusiness__c,ORT_Qualified_Person__c,JJ_JPN_RelationshipwithDoctor__c,JJ_JPN_InterviewSpecifiedTime__c,JJ_JPN_KeyPerson__c,Nearest_Station__c,JJ_JPN_TypeOfPublishedAds__c,JJ_JPN_VisitBan__c,JJ_JPN_FocusCategory__c,JJ_JPN_Parking__c,JJ_JPN_PowerRelationshipwithCompeting__c,JJ_JPN_ManagementPolicy__c,IntroductionPriority__c,JJ_JPN_PartnershopFlag__c,JJ_JPN_FacilityInformationTelephone__c,JJ_JPN_PicturesPublishedFlag__c,JJ_JPN_InformationRemarks__c,JJ_JPN_CP1DOMonitorCampaign__c,JJ_JPN_ReservedCampaignFlag3__c,JJ_JPN_CP1DO90RefundSecurityGuarranty__c,JJ_JPN_CPWtAcampaign__c,JJ_JPN_CPNewDesignLaunchCampaign__c,JJ_JPN_EyecarePartner__c,JJ_JPN_Information1DOasys__c,JJ_JPN_FT1DOasys__c,JJ_JPN_Information1DOasys90P__c,JJ_JPN_InformationTruEye__c,JJ_JPN_FTTruEye__c,JJ_JPN_InformationTruEye90Pack__c,JJ_JPN_InformationMoist__c,JJ_JPN_FTMoist__c,JJ_JPN_Information1MAstigmatism__c,JJ_JPN_FT1MAstigmatism__c,JJ_JPN_Information1DMoistMultifocal__c,JJ_JPN_FT1DMoistMultifocal__c,JJ_JPN_Information1DDefineMoistRadiant__c,JJ_JPN_FT1DDefineMoistRadiant__c,JJ_JPN_Information1DDefineMoist__c,JJ_JPN_FT1DDefineMoist__c,JJ_JPN_Information1DDefineMoist10Pack__c,JJ_JPN_Information1DAV90Pack__c,JJ_JPN_Information1DAV__c,JJ_JPN_InformationOasys__c,JJ_JPN_FTOasys__c,JJ_JPN_InformationOAAstigmatism__c,JJ_JPN_FTOAAstigmatism__c,JJ_JPN_InformationAdvance__c,JJ_JPN_FTAdvance__c,JJ_JPN_Information2WAV__c,JJ_JPN_Information2WDefine__c,JJ_JPN_InformationMoist90Pack__c,JJ_JPN_FT2WDefine__c,custGroup__c  FROM Account where id=:acc1 ];
            System.debug('accListToEditOrDel.JJ_JPN_DeadlineTime__c'+accListToEditOrDel.JJ_JPN_DeadlineTime__c );
            CMR.JJ_JPN_SoldToCode__c = accListToEditOrDel.OutletNumber__c;
            CMR.JJ_JPN_PayerCode__c = accListToEditOrDel.AccountNumber;
            CMR.JJ_JPN_SoldToNameKanji__c = accListToEditOrDel.Name;
            CMR.JJ_JPN_SoldToNameKana__c = accListToEditOrDel.JJ_JPN_CustomerNameKana__c;
            CMR.JJ_JPN_SoldToPostalCode__c = accListToEditOrDel.ShippingPostalCode; 
            CMR.JJ_JPN_SoldToState__c = accListToEditOrDel.Address3__c; 
            CMR.JJ_JPN_SoldToTownshipCity__c = accListToEditOrDel.Address2__c;
            CMR.JJ_JPN_SoldToStreet__c = accListToEditOrDel.Address1__c;
            CMR.JJ_JPN_SoldToState__c= accListToEditOrDel.Address3__c;
            CMR.JJ_JPN_SoldToOtherAddress__c = accListToEditOrDel.JJ_JPN_OtherAddress__c;
            CMR.JJ_JPN_SoldToTelephone__c = accListToEditOrDel.Phone;
            CMR.JJ_JPN_SoldToFax__c = accListToEditOrDel.Fax;
            CMR.JJ_JPN_StatusCodeRD__c= accListToEditOrDel.Status__c;
            
            CMR.JJ_JPN_PayerNameKanji__c = accListToEditOrDel.subCustGroup__c;
            CMR.JJ_JPN_BillToCode__c = accListToEditOrDel.JJ_JPN_BillToCode__c;
            CMR.JJ_JPN_BillToNameKanji__c = accListToEditOrDel.JJ_JPN_BillToNameKanji__c;
            //CMR.JJ_JPN_Person_Incharge_NameSAP__c = accListToEditOrDel.custGroup__c;
            CMR.JJ_JPN_CreditPersonInchargeName__c = accListToEditOrDel.JJ_JPN_CreditPersonInchargeName__c;
            CMR.JJ_JPN_DirectDelivery__c = accListToEditOrDel.JJ_JPN_DirectDeliveryReturned__c;
            //PersonInchangeName = accListToEditOrDel.custGroup__c;JJ_JPN_PersonInchargeName__c
            CMR.JJ_JPN_PersonInchargeName__c = accListToEditOrDel.custGroup__c;
            
            CMR.JJ_JPN_PaymentCondition__c = accListToEditOrDel.payment_terms__c;
            CMR.JJ_JPN_FieldSalesRegionID__c = accListToEditOrDel.CreditLimit__c;
            CMR.JJ_JPN_TerritoriyName__c = accListToEditOrDel.Territory1__c;
            // CMR.JJ_JPN_SubCustomerGroup__c = accListToEditOrDel.subCustGroup__c;  // jpn fields need to check sub customer Group
            CMR.JJ_JPN_SAMDestrictRepID__c = accListToEditOrDel.Customer_type_description__c;
            CMR.JJ_JPN_PersonInchargeCode__c = accListToEditOrDel.SalesRep__c;
            CMR.JJ_JPN_OrderBlock__c = accListToEditOrDel.JJ_JPN_OrderBlock__c;
            
            // Customer extra mapping
            //CMR.JJ_JPN_StatusCode__c = accListToEditOrDel.Status__c;
            CMR.JJ_JPN_ReturnFAXNo__c = accListToEditOrDel.Mobile_Number__c;
            CMR.JJ_JPN_SalesItem__c = accListToEditOrDel.JJ_JPN_SalesItemDxDemoItemRForSale__c;
            CMR.JJ_JPN_Rank__c = accListToEditOrDel.JJ_JPN_Rank__c;
            CMR.JJ_JPN_SubTradeChannel__c = accListToEditOrDel.JJ_JPN_SubTradeChannel__c;
            CMR.JJ_JPN_SubCustomerGroup__c = accListToEditOrDel.JJ_JPN_SubCustomerGroup__c; //need to check sub customer Group
            CMR.JJ_JPN_SAMDESC__c = accListToEditOrDel.JJ_JPN_SAMDESC__c;
            CMR.JJ_JPN_FieldSalesDistrictID__c = accListToEditOrDel.JJ_JPN_FieldSalesDistrictID__c;
            CMR.JJ_JPN_OfficeName__c = accListToEditOrDel.JJ_JPN_OfficeName__c;
            CMR.JJ_JPN_ADSHandlingFlag__c = accListToEditOrDel.JJ_JPN_ADSHandlingFlag__c;
            CMR.JJ_JPN_ShopName__c = accListToEditOrDel.JJ_JPN_ShopName__c;
            CMR.JJ_JPN_BillingCloseDate__c = accListToEditOrDel.JJ_JPN_BillingCloseDate__c;
            CMR.JJ_JPN_PaymentMethod__c = accListToEditOrDel.JJ_JPN_PaymentMethod__c;
            CMR.JJ_JPN_CustomerRTNFAXHowManyTimesToSend__c = accListToEditOrDel.JJ_JPN_CustomerRTNFAXHowmanytimestosend__c;
            CMR.JJ_JPN_BillToRTNFAX__c = accListToEditOrDel.JJ_JPN_BillToRTNFAXHowmanytimestosend__c;
            CMR.JJ_JPN_SoldToNumberOfPagesReceived__c = accListToEditOrDel.JJ_JPN_SoldToNumberOfPagesReceived__c;
            CMR.JJ_JPN_TransactionDetailSubmission__c = accListToEditOrDel.JJ_JPN_TransactionDetailSubmission__c;
            CMR.JJ_JPN_BillingSummaryTableSubmission__c = accListToEditOrDel.JJ_JPN_BillingSummaryTableSubmission__c;
            CMR.JJ_JPN_ItemizedBilling__c = accListToEditOrDel.JJ_JPN_ItemizedBilling__c;
            CMR.JJ_JPN_BillSubmission__c = accListToEditOrDel.JJ_JPN_BillSubmission__c;
            CMR.JJ_JPN_LotNumberCommunicationDocument__c = accListToEditOrDel.JJ_JPN_LotNumberCommunicationDocument__c;
            CMR.JJ_JPN_DeliveryDocumentNote__c = accListToEditOrDel.JJ_JPN_DeliveryDocumentNote__c;
            CMR.JJ_JPN_EDIStoreCode__c = accListToEditOrDel.JJ_JPN_EDIStoreCode__c;
            CMR.JJ_JPN_EDIFlag__c = accListToEditOrDel.JJ_JPN_EDI_Flag__c;
            CMR.JJ_JPN_FAXOrder__c = accListToEditOrDel.JJ_JPN_FAXOrder__c;
            CMR.JJ_JPN_CustomerOrderNoRequired__c = accListToEditOrDel.JJ_JPN_PurchaseOrderNumberRequired__c;
            CMR.JJ_JPN_ReturnedGoodsOrderNoInDigits__c = accListToEditOrDel.JJ_JPN_DSO__c;
            CMR.JJ_JPN_CustomerOrderNoNumberofDigits__c = accListToEditOrDel.JJ_JPN_CustomerOrderNoNumberofDigits__c;
            CMR.JJ_JPN_DeadlineTime__c = accListToEditOrDel.JJ_JPN_DeadlineTime__c;  // Picklist value changing as per the account values in CMR values is A, B , C
            CMR.JJ_JPN_NoDeliveryCharges__c = accListToEditOrDel.JJ_JPN_NoDeliveryCharges__c;  // picklist value changed as per the account values CMR values is 14:00 is Default, 16:00
            CMR.JJ_JPN_SolutionFlag__c = accListToEditOrDel.JJ_JPN_Solution_Flag__c;
            
            CMR.JJ_JPN_FestivalDelivery__c = accListToEditOrDel.JJ_JPN_FestivalDelivery__c;
            CMR.JJ_JPN_IndividualDeliveryCommentsMonday__c = accListToEditOrDel.JJ_JPN_IndividualDeliveryCommentsMon__c;
            CMR.JJ_JPN_IndividualDeliveryCommentsTuesday__c = accListToEditOrDel.IndividualDeliveryCommentsTues__c;
            CMR.JJ_JPN_IndividualDeliveryCommentsWed__c = accListToEditOrDel.IndividualDeliveryCommentsWed__c;
            CMR.JJ_JPN_IndividualDeliveryCommentsThurs__c = accListToEditOrDel.JJ_JPN_IndividualDeliveryCommentsThu__c;
            CMR.JJ_JPN_IndividualDeliveryCommentsFriday__c = accListToEditOrDel.JJ_JPN_IndividualDeliveryCommentsFri__c;
            CMR.JJ_JPN_IndividualDeliveryCommentsSat__c = accListToEditOrDel.JJ_JPN_IndividualDeliveryCommentsSat__c;
            CMR.JJ_JPN_IndividualDeliveryCommentsSun__c = accListToEditOrDel.JJ_JPN_IndividualDeliveryCommentsSun__c;
            CMR.JJ_JPN_CautionMemoCustomerInformation__c = accListToEditOrDel.Customer_Location__c;
            CMR.JJ_JPN_IndividualDeliveryPossibleMonday__c = accListToEditOrDel.JJ_JPN_IndividualDeliveryPossible__c;
            CMR.JJ_JPN_IndividualDeliveryPossibleTues__c = accListToEditOrDel.JJ_JPN_IndividualDeliveryPossibleTues__c;
            CMR.JJ_JPN_IndividualDeliveryPossibleDateWed__c = accListToEditOrDel.JJ_JPN_IndividualDeliveryPossibleWed__c;
            CMR.JJ_JPN_PIndividualDeliveryPossibleThur__c = accListToEditOrDel.JJ_JPN_IndividualDeliveryPossibleThurs__c;
            CMR.JJ_JPN_IndividualDeliveryPossibleFriday__c = accListToEditOrDel.JJ_JPN_IndividualDeliveryPossibleDa__c;
            CMR.JJ_JPN_IndividualDeliveryPossibleSat__c = accListToEditOrDel.JJ_JPN_IndividualDeliveryPossibleDaSat__c;
            CMR.JJ_JPN_IndividualDeliveryPossibleDateSun__c = accListToEditOrDel.JJ_JPN_IndividualDeliveryPossibleSun__c;
            CMR.JJ_JPN_Description__c = accListToEditOrDel.Description;
            CMR.Name = accListToEditOrDel.Name;
            CMR.JJ_JPN_PayerCode__c=accListToEditOrDel.AccountNumber;
            CMR.JJ_JPN_BillToCode__c=accListToEditOrDel.JJ_JPN_BillToCode__c;
            CMR.JJ_JPN_PersonInchargeName__c=accListToEditOrDel.custGroup__c;
            CMR.JJ_JPN_CreditPersonInchargeName__c=accListToEditOrDel.JJ_JPN_CreditPersonInchargeName__c;
            CMR.JJ_JPN_SoldToCode__c=accListToEditOrDel.OutletNumber__c;
            CMR.JJ_JPN_PayerNameKanji__c=accListToEditOrDel.subCustGroup__c;
            CMR.JJ_JPN_OfficeName__c=accListToEditOrDel.JJ_JPN_OfficeName__c;
            CMR.JJ_JPN_BillToNameKanji__c=accListToEditOrDel.JJ_JPN_BillToNameKanji__c;
            CMR.JJ_JPN_StatusCodeRD__c=accListToEditOrDel.Status__c;
        }Catch(AuraHandledException e){
            throw e;
        }
        system.debug('CMR:'+ CMR);
        //CMRWrapper resultWrapper = new CMRWrapper(CMR , getPicklistValues(objpicklistFieldsMap));
        return new CMRWrapper(CMR , getPicklistValues(objpicklistFieldsMap));
    }
    
    public class CMRWrapper{
        @AuraEnabled public JJ_JPN_CustomerMasterRequest__c CMR{get;set;}
        @AuraEnabled public Map<String, Map<String, List<pickListValues>>> objFieldPicklistMap{get;set;}
        public CMRWrapper(JJ_JPN_CustomerMasterRequest__c CMR, Map<String, Map<String, List<pickListValues>>>  objFieldPicklistMap){
            this.CMR = CMR;
            this.objFieldPicklistMap = objFieldPicklistMap;
        }
    }
    
    @AuraEnabled 
    public static errorWrapper doSave(JJ_JPN_CustomerMasterRequest__c CMR, Id accId)
    {
        try
        {
            accListToEditOrDel= [SELECT id , name, AccountNumber, Description, OutletNumber__c,Territory1__c,subCustGroup__c, Mobile_Number__c,Phone, Fax, JJ_JPN_CustomerNameKana__c, ShippingPostalCode,Address2__c, Address3__c, ShippingCountry, JJ_JPN_OtherAddress__c,Address1__c,JJ_JPN_isPayerAcc__c,Status__c,JJ_JPN_BillToCode__c,JJ_JPN_BillToNameKanji__c,JJ_JPN_SalesItemDxDemoItemRForSale__c,JJ_JPN_Rank__c,JJ_JPN_SubTradeChannel__c,Sub_Customer_Group__c,JJ_JPN_SAMDESC__c,JJ_JPN_FieldSalesDistrictID__c,JJ_JPN_OfficeName__c,JJ_JPN_ADSHandlingFlag__c,JJ_JPN_ShopName__c,JJ_JPN_BillingCloseDate__c,JJ_JPN_PaymentMethod__c,JJ_JPN_CustomerRTNFAXHowmanytimestosend__c,JJ_JPN_BillToRTNFAXHowmanytimestosend__c,JJ_JPN_SoldToNumberOfPagesReceived__c,JJ_JPN_TransactionDetailSubmission__c,JJ_JPN_BillingSummaryTableSubmission__c,JJ_JPN_ItemizedBilling__c,JJ_JPN_BillSubmission__c,JJ_JPN_LotNumberCommunicationDocument__c,JJ_JPN_DeliveryDocumentNote__c,JJ_JPN_EDIStoreCode__c,JJ_JPN_EDI_Flag__c,JJ_JPN_FAXOrder__c,JJ_JPN_PurchaseOrderNumberRequired__c,JJ_JPN_DeadlineTime__c,JJ_JPN_NoDeliveryCharges__c,JJ_JPN_Solution_Flag__c,JJ_JPN_FestivalDelivery__c,JJ_JPN_IndividualDeliveryCommentsMon__c,IndividualDeliveryCommentsTues__c,IndividualDeliveryCommentsWed__c,JJ_JPN_IndividualDeliveryCommentsThu__c,JJ_JPN_IndividualDeliveryCommentsFri__c,JJ_JPN_IndividualDeliveryCommentsSat__c,JJ_JPN_IndividualDeliveryCommentsSun__c,Customer_Location__c,JJ_JPN_IndividualDeliveryPossible__c,JJ_JPN_IndividualDeliveryPossibleTues__c,JJ_JPN_IndividualDeliveryPossibleWed__c,JJ_JPN_IndividualDeliveryPossibleThurs__c,JJ_JPN_IndividualDeliveryPossibleDa__c,JJ_JPN_IndividualDeliveryPossibleDaSat__c,JJ_JPN_IndividualDeliveryPossibleSun__c,Last_update_date__c,CreditLimit__c,payment_terms__c,Customer_type_description__c,JJ_JPN_Website__c,JJ_JPN_Email__c,SalesRep__c,JJ_JPN_BusinessLicenceOwnerName__c,JJ_JPN_LicenseNumber__c,JJ_JPN_RepresentativeNameKanji__c,JJ_JPN_RepresentativeNameKana__c,JJ_JPN_FacilityInformation__c,JJ_JPN_PersonInchargeName__c,JJ_JPN_SubCustomerGroup__c,JJ_JPN_CreditPersonInchargeName__c,JJ_JPN_OrderBlock__c,JJ_JPN_DirectDeliveryReturned__c,JJ_JPN_CustomerOrderNoNumberofDigits__c,JJ_JPN_DSO__c,JJ_JPN_Validity__c,JJ_JPN_CLIssue__c,JJ_JPN_ContractDate__c,JJ_JPN_ContractType__c,JJ_JPN_CompanyName__c,JJ_JPN_RepresentativeNameFranchise__c,JJ_JPN_POSDevision__c,JJ_JPN_GroupName__c,JJ_JPN_ReturnedGoodMethod__c,JJ_JPN_DirectDeliveryLeafIncludeNG__c,JJ_JPN_OutboundNG__c,JJ_JPN_ReservedCampaignFlag1__c,JJ_JPN_ThoughtOnNetBusiness__c,ORT_Qualified_Person__c,JJ_JPN_RelationshipwithDoctor__c,JJ_JPN_InterviewSpecifiedTime__c,JJ_JPN_KeyPerson__c,Nearest_Station__c,JJ_JPN_TypeOfPublishedAds__c,JJ_JPN_VisitBan__c,JJ_JPN_FocusCategory__c,JJ_JPN_Parking__c,JJ_JPN_PowerRelationshipwithCompeting__c,JJ_JPN_ManagementPolicy__c,IntroductionPriority__c,JJ_JPN_PartnershopFlag__c,JJ_JPN_FacilityInformationTelephone__c,JJ_JPN_PicturesPublishedFlag__c,JJ_JPN_InformationRemarks__c,JJ_JPN_CP1DOMonitorCampaign__c,JJ_JPN_ReservedCampaignFlag3__c,JJ_JPN_CP1DO90RefundSecurityGuarranty__c,JJ_JPN_CPWtAcampaign__c,JJ_JPN_CPNewDesignLaunchCampaign__c,JJ_JPN_EyecarePartner__c,JJ_JPN_Information1DOasys__c,JJ_JPN_FT1DOasys__c,JJ_JPN_Information1DOasys90P__c,JJ_JPN_InformationTruEye__c,JJ_JPN_FTTruEye__c,JJ_JPN_InformationTruEye90Pack__c,JJ_JPN_InformationMoist__c,JJ_JPN_FTMoist__c,JJ_JPN_Information1MAstigmatism__c,JJ_JPN_FT1MAstigmatism__c,JJ_JPN_Information1DMoistMultifocal__c,JJ_JPN_FT1DMoistMultifocal__c,JJ_JPN_Information1DDefineMoistRadiant__c,JJ_JPN_FT1DDefineMoistRadiant__c,JJ_JPN_Information1DDefineMoist__c,JJ_JPN_FT1DDefineMoist__c,JJ_JPN_Information1DDefineMoist10Pack__c,JJ_JPN_Information1DAV90Pack__c,JJ_JPN_Information1DAV__c,JJ_JPN_InformationOasys__c,JJ_JPN_FTOasys__c,JJ_JPN_InformationOAAstigmatism__c,JJ_JPN_FTOAAstigmatism__c,JJ_JPN_InformationAdvance__c,JJ_JPN_FTAdvance__c,JJ_JPN_Information2WAV__c,JJ_JPN_Information2WDefine__c,JJ_JPN_InformationMoist90Pack__c,JJ_JPN_FT2WDefine__c,custGroup__c  FROM Account where id=:accId ];
            
            CMR.JJ_JPN_IsEditDeleteAcc__c = true;
            CMR.JJ_JPN_PayerCode__c = accListToEditOrDel.AccountNumber;
            // NON SAP Fields only to save data in CMR records. This All  Mandatory fields in CMR
            CMR.JJ_JPN_BusinessLicenceOwnerName__c = accListToEditOrDel.JJ_JPN_BusinessLicenceOwnerName__c;
            CMR.JJ_JPN_LicenseNumber__c = accListToEditOrDel.JJ_JPN_LicenseNumber__c;
            CMR.JJ_JPN_RepresentativeNameKanji__c = accListToEditOrDel.JJ_JPN_RepresentativeNameKanji__c;
            CMR.JJ_JPN_RepresentativeNameKana__c = accListToEditOrDel.JJ_JPN_RepresentativeNameKana__c;
            CMR.JJ_JPN_FacilityInformation__c = accListToEditOrDel.JJ_JPN_FacilityInformation__c;
            
            // CMR.JJ_JPN_Person_Incharge_NameSAP__c = accListToEditOrDel.JJ_JPN_PersonInchargeName__c;
            
            // Basic Information 2 as per Account Layout
            CMR.JJ_JPN_Validity__c = accListToEditOrDel.JJ_JPN_Validity__c;
            CMR.JJ_JPN_CLIssue__c = accListToEditOrDel.JJ_JPN_CLIssue__c;
            CMR.JJ_JPN_ContractDate__c = accListToEditOrDel.JJ_JPN_ContractDate__c;
            CMR.JJ_JPN_ContractType__c = accListToEditOrDel.JJ_JPN_ContractType__c;
            CMR.JJ_JPN_CompanyName__c = accListToEditOrDel.JJ_JPN_CompanyName__c;
            CMR.JJ_JPN_RepresentativeNameFranchise__c = accListToEditOrDel.JJ_JPN_RepresentativeNameFranchise__c;
            CMR.JJ_JPN_POSDevision__c = accListToEditOrDel.JJ_JPN_POSDevision__c;
            CMR.JJ_JPN_GroupName__c = accListToEditOrDel.JJ_JPN_GroupName__c;
            CMR.JJ_JPN_ReturnedGoodMethod__c = accListToEditOrDel.JJ_JPN_ReturnedGoodMethod__c;
            CMR.JJ_JPN_DirectDeliveryLeafIncludeNG__c = accListToEditOrDel.JJ_JPN_DirectDeliveryLeafIncludeNG__c;
            CMR.JJ_JPN_OutboundNG__c = accListToEditOrDel.JJ_JPN_OutboundNG__c;
            CMR.JJ_JPN_ReservedCampaignFlag1__c = accListToEditOrDel.JJ_JPN_ReservedCampaignFlag1__c;
            
            // Business Related Information
            CMR.JJ_JPN_ThoughtOnNetBusiness__c = accListToEditOrDel.JJ_JPN_ThoughtOnNetBusiness__c;
            CMR.JJ_JPN_ORTQualifiedPerson__c = accListToEditOrDel.ORT_Qualified_Person__c;
            CMR.JJ_JPN_RelationshipWithDoctor__c = accListToEditOrDel.JJ_JPN_RelationshipwithDoctor__c;
            CMR.JJ_JPN_InterviewSpecifiedTime__c = accListToEditOrDel.JJ_JPN_InterviewSpecifiedTime__c;
            CMR.JJ_JPN_KeyPerson__c = accListToEditOrDel.JJ_JPN_KeyPerson__c;
            CMR.JJ_JPN_NearestStation__c = accListToEditOrDel.Nearest_Station__c;
            CMR.JJ_JPN_TypeOfPublishedAds__c = accListToEditOrDel.JJ_JPN_TypeOfPublishedAds__c;
            CMR.JJ_JPN_VisitBan__c = accListToEditOrDel.JJ_JPN_VisitBan__c;
            CMR.JJ_JPN_FocusCategory__c = accListToEditOrDel.JJ_JPN_FocusCategory__c;
            CMR.JJ_JPN_Parking__c = accListToEditOrDel.JJ_JPN_Parking__c;
            CMR.JJ_JPN_PowerRelationShipWithCompetings__c = accListToEditOrDel.JJ_JPN_PowerRelationshipwithCompeting__c;
            CMR.JJ_JPN_ManagementPolicy__c = accListToEditOrDel.JJ_JPN_ManagementPolicy__c;
            CMR.JJ_JPN_Email__c = accListToEditOrDel.JJ_JPN_Email__c;
            
            // Facility Guidance Information
            CMR.JJ_JPN_IntroductionPriority__c =  accListToEditOrDel.IntroductionPriority__c;    
            CMR.JJ_JPN_PartnershopFlag__c = accListToEditOrDel.JJ_JPN_PartnershopFlag__c;
            CMR.JJ_JPN_FacilityInformationTelephone__c = accListToEditOrDel.JJ_JPN_FacilityInformationTelephone__c;
            CMR.JJ_JPN_PicturesPublishedFlag__c = accListToEditOrDel.JJ_JPN_PicturesPublishedFlag__c;
            CMR.JJ_JPN_Website__c = accListToEditOrDel.JJ_JPN_Website__c;
            CMR.JJ_JPN_EyecarePartner__c = accListToEditOrDel.JJ_JPN_EyecarePartner__c;
            CMR.JJ_JPN_InformationRemarks__c = accListToEditOrDel.JJ_JPN_InformationRemarks__c;
            
            //Information/FT Flag
            CMR.JJ_JPN_Information1DOasys__c = accListToEditOrDel.JJ_JPN_Information1DOasys__c;
            CMR.JJ_JPN_FT1DOasys__c = accListToEditOrDel.JJ_JPN_FT1DOasys__c;
            CMR.JJ_JPN_Information1DOasys90P__c  = accListToEditOrDel.JJ_JPN_Information1DOasys90P__c;
            CMR.JJ_JPN_InformationTruEye__c = accListToEditOrDel.JJ_JPN_InformationTruEye__c;
            CMR.JJ_JPN_FTTruEye__c = accListToEditOrDel.JJ_JPN_FTTruEye__c;
            CMR.JJ_JPN_InformationTruEye90Pack__c = accListToEditOrDel.JJ_JPN_InformationTruEye90Pack__c;
            CMR.JJ_JPN_InformationMoist__c = accListToEditOrDel.JJ_JPN_InformationMoist__c;
            CMR.JJ_JPN_FTMoist__c = accListToEditOrDel.JJ_JPN_FTMoist__c;
            CMR.JJ_JPN_InformationMoist90Pack__c = accListToEditOrDel.JJ_JPN_InformationMoist90Pack__c;
            CMR.JJ_JPN_Information1MAstigmatism__c = accListToEditOrDel.JJ_JPN_Information1MAstigmatism__c;
            CMR.JJ_JPN_FT1MAstigmatism__c = accListToEditOrDel.JJ_JPN_FT1MAstigmatism__c;
            CMR.JJ_JPN_Information1DMoistMultifocal__c = accListToEditOrDel.JJ_JPN_Information1DMoistMultifocal__c;
            CMR.JJ_JPN_FT1DMoistMultifocal__c = accListToEditOrDel.JJ_JPN_FT1DMoistMultifocal__c;
            CMR.JJ_JPN_Information1DDefineMoistRadiant__c = accListToEditOrDel.JJ_JPN_Information1DDefineMoistRadiant__c;
            CMR.JJ_JPN_FT1DDefineMoistRadiant__c = accListToEditOrDel.JJ_JPN_FT1DDefineMoistRadiant__c;
            CMR.JJ_JPN_Information1DDefineMoist__c = accListToEditOrDel.JJ_JPN_Information1DDefineMoist__c;
            CMR.JJ_JPN_FT1DDefineMoist__c = accListToEditOrDel.JJ_JPN_FT1DDefineMoist__c;
            CMR.JJ_JPN_Information1DDefineMoist10Pack__c = accListToEditOrDel.JJ_JPN_Information1DDefineMoist10Pack__c;
            CMR.JJ_JPN_Information1DAV__c = accListToEditOrDel.JJ_JPN_Information1DAV__c;
            CMR.JJ_JPN_Information1DAV90Pack__c = accListToEditOrDel.JJ_JPN_Information1DAV90Pack__c;
            CMR.JJ_JPN_InformationOasys__c = accListToEditOrDel.JJ_JPN_InformationOasys__c;
            CMR.JJ_JPN_FTOasys__c = accListToEditOrDel.JJ_JPN_FTOasys__c;
            CMR.JJ_JPN_InformationOAAstigmatism__c = accListToEditOrDel.JJ_JPN_InformationOAAstigmatism__c;
            CMR.JJ_JPN_FTOAAstigmatism__c = accListToEditOrDel.JJ_JPN_FTOAAstigmatism__c;
            CMR.JJ_JPN_InformationAdvance__c = accListToEditOrDel.JJ_JPN_InformationAdvance__c;
            CMR.JJ_JPN_FTAdvance__c = accListToEditOrDel.JJ_JPN_FTAdvance__c;
            CMR.JJ_JPN_Information2WAV__c = accListToEditOrDel.JJ_JPN_Information2WAV__c;
            CMR.JJ_JPN_Information2WDefine__c = accListToEditOrDel.JJ_JPN_Information2WDefine__c;
            CMR.JJ_JPN_FT2WDefine__c = accListToEditOrDel.JJ_JPN_FT2WDefine__c;
            
            
            //Campaign
            CMR.JJ_JPN_CP1DOMonitorCampaign__c = accListToEditOrDel.JJ_JPN_CP1DOMonitorCampaign__c;
            CMR.JJ_JPN_ReservedCampaignFlag3__c = accListToEditOrDel.JJ_JPN_ReservedCampaignFlag3__c;
            CMR.JJ_JPN_CP1DO90Refund__c = accListToEditOrDel.JJ_JPN_CP1DO90RefundSecurityGuarranty__c;
            CMR.JJ_JPN_CPWtAcampaign__c = accListToEditOrDel.JJ_JPN_CPWtAcampaign__c;
            CMR.JJ_JPN_CPNewDesignLaunchCampaign__c = accListToEditOrDel.JJ_JPN_CPNewDesignLaunchCampaign__c;
            
            system.debug('CMR:'+ CMR); 
            system.debug('JJ_JPN_StatusCode__c:'+ CMR.JJ_JPN_StatusCode__c); 
            system.debug('Account_Activation_Start_Date__c:'+ CMR.Account_Activation_Start_Date__c);
            if(CMR.JJ_JPN_StatusCode__c!=Null && CMR.Account_Activation_Start_Date__c!=Null)
            {
                CMR.Name = [select name from account where id=:accId].name;
                // CMR.Name = CMR.JJ_JPN_SoldToNameKanji__c;
            }
            else  
            {
                // errorWrapper error = new errorWrapper('Error' , 'Please Select Status Code and Add Activation Date');
                return new errorWrapper('Error' , 'Please Select Status Code and Add Activation Date');
            } 
            if (CMR.JJ_JPN_SoldToPostalCode__c==null)
            {
                //errorWrapper error = new errorWrapper('Error' , 'Postal Code Sold To: Validation Error: Value is required');
                return new errorWrapper('Error' , 'Postal Code Sold To: Validation Error: Value is required');
            }
            insert CMR;
        }
        catch(DMLException e)
        {
            System.debug('e.getMessage()'+e.getMessage());
            //System.debug('e.getMessage()'+e.initCause);
            System.debug('e.getMessage()'+e.getDmlMessage(0));
            System.debug('e.getMessage()'+e.getCause());
            
            //errorWrapper error = new errorWrapper('Error' , e.getMessage());
            
            return new errorWrapper('Error' , e.getDmlMessage(0));
        }  
        catch(Exception e)
        {
            return new errorWrapper('Error' , e.getMessage());
        }  
        //errorWrapper error = new errorWrapper('Success' , CMR.id);
        return new errorWrapper('Success' , CMR.id);
    }
    
    public class errorWrapper{
        @AuraEnabled
        public String type{get;set;}
        @AuraEnabled
        public String value{get;set;}
        public errorWrapper(String type, String value){
            this.type = type;
            this.value = value;
        }
    }
}