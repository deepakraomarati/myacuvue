public without sharing  class SW_JJ_JPN_approvalRecall_LEX {
    
    @AuraEnabled
    public static Boolean checkPermission()
    {
        Boolean isEnabled = FeatureManagement.checkPermission('Stop_user_from_Recallling');
        return isEnabled;
    }
    
    @AuraEnabled
    public static id permissionSetRecords()
    {
        permissionSet permissionSetRecords = new permissionSet();
        permissionSetRecords = [SELECT Id FROM PermissionSet where Name = 'JJ_JPN_Recall_approval_permset' limit 1];
       // List<permissionSet> records = permissionSetRecords.getArray('records');
        system.debug('permissionSetRecords.id>>>>'+permissionSetRecords.id);
        return permissionSetRecords.id;
    }
    
    @AuraEnabled
    public static boolean recallApprovalpermasgn(String permsetid, String userid)
    {
        PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = permsetid, AssigneeId = userid);
       // system.debug('11@@ ' + permsetid);
      //  system.debug('12@@ ' + userid);
        insert psa;
       // ID recId = recordId;
       // boolean recall = recallApproval(recId);
       // system.debug('recall : ' + recall);
       // recallApprovalpermdel(permsetid, userid);
       System.debug('PermissionSetAssignment'+psa);
       return true;
    }
    @AuraEnabled
    public static boolean recallApprovalpermdel(string permsetid, string userid)
    {
        string permsetids=permsetid;
        string userids=userid;
        PermissionSetAssignment psa = [select id from PermissionSetAssignment where PermissionSetId=:permsetids and  AssigneeId = :userids];
        delete psa;
        return true;
    }
    @AuraEnabled
    public static boolean recallApproval(Id recId)
    {  
        system.debug('recId : ' + recId);
        List<ProcessInstanceWorkitem> piwi = [SELECT Id, ProcessInstanceId, ProcessInstance.TargetObjectId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =: recId];
        system.debug('piwi : ' + piwi);
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setAction('Removed');
        if(!piwi.isEmpty()){
            req.setWorkitemId(piwi.get(0).Id);
            //Approval.process(req,false);
            Approval.ProcessResult pr = Approval.process(req);
            system.debug('ProcessResult :: ' + pr);
            return true;
        }else{
            return false;
        }
    }   
}