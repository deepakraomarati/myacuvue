public class SW_LightningSiteWrapperClass_LEX {
	public class LightningVoucherRedeemWrapper{
		@AuraEnabled public String HeaderLabel{get; set;}
		@AuraEnabled public List<String> HeaderColumn{get; set;}
		@AuraEnabled public List<DataWrapper> CellValue{get; set;}
	}
	public class DataWrapper{
		@AuraEnabled public String DataLabel{get; set;}
		@AuraEnabled public List<String> HeaderColumn{get; set;}
		@AuraEnabled public Decimal totalData{get; set;}
		@AuraEnabled public Decimal Data1{get; set;}
		@AuraEnabled public Decimal Data2{get; set;}
		@AuraEnabled public Decimal Data3{get; set;}
		@AuraEnabled public Decimal Data4{get; set;}
		@AuraEnabled public Decimal Data5{get; set;}
		@AuraEnabled public Decimal Data6{get; set;}
		@AuraEnabled public Decimal Data7{get; set;}
		@AuraEnabled public Decimal Data8{get; set;}
		@AuraEnabled public Decimal Data9{get; set;}
		@AuraEnabled public Decimal Data10{get; set;}
		@AuraEnabled public Decimal Data11{get; set;}
		@AuraEnabled public Decimal Data12{get; set;}
	}
	public class RedeemBoxWrapper{
		@AuraEnabled public String brand1{get; set;}
		@AuraEnabled public Integer noOfBox1{get; set;}
		@AuraEnabled public String brand2{get; set;}
		@AuraEnabled public Integer noOfBox2{get; set;}
		@AuraEnabled public String brand3{get; set;}
		@AuraEnabled public Integer noOfBox3{get; set;}
		@AuraEnabled public String brand4{get; set;}
		@AuraEnabled public Integer noOfBox4{get; set;}
	}
}