public without sharing class SW_LoginController_LEX{
    @AuraEnabled
    public static resultWrapper Login( String SAPID,String Phone)
    {
        if(String.isNotBlank(SAPID) && String.isNotBlank(Phone))
        {
            List<Account> lstaccount = [SELECT Id,Name,Phone,OutletNumber__c FROM Account WHERE OutletNumber__c =:SAPID AND Phone =:Phone LIMIT 1];
            try{
                if(!lstaccount.isEmpty() && lstaccount.size()>0)
                {
                    resultWrapper result = new resultWrapper('Success',lstaccount[0].Id);
                    return result;
                }else
                {
                    resultWrapper result = new resultWrapper('Error',System.Label.Invalid_ECP);
                    return result;
                }
            }
            catch (Exception e){
                resultWrapper result = new resultWrapper('Error',e.getMessage());
                return result;
            }
        }
        else{
            resultWrapper result = new resultWrapper('Error',System.Label.Invalid_ECP);
            return result;
        }
    }
    public class resultWrapper{
        @AuraEnabled public String type{get;set;}
        @AuraEnabled public String value{get;set;}
        public resultWrapper(String type, String value){
            this.type = type;
            this.value = value;
        }
    }
}