@isTest
public class SW_LoginController_LEX_Test {
   static testMethod void testLoginMethod()
    {
        Account acc=new Account(Name='Test',OutletNumber__c='123456',Phone='123456');
        insert  acc;   
        test.Starttest();
        SW_LoginController_LEX.resultWrapper loginWrapper= SW_LoginController_LEX.Login(acc.OutletNumber__c,acc.Phone);
        SW_LoginController_LEX.resultWrapper loginWrapper2= SW_LoginController_LEX.Login('xyz',acc.Phone);
        SW_LoginController_LEX.resultWrapper loginWrapper3= SW_LoginController_LEX.Login(null,null);
        system.assertEquals('Success', loginWrapper.type);
        system.assertEquals('Error', loginWrapper2.type);
        test.stopTest();
    } 
}