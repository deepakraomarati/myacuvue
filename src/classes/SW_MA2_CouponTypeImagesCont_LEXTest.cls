@isTest
Private class SW_MA2_CouponTypeImagesCont_LEXTest 
{
    static testMethod void getCouponTypeListTest()
    {
        List<String> val = new List<String>{'Coupon Banner','Coupon Description Image','Coupon Terms & Condition'};
            test.startTest();
        List< SW_MA2_CouponTypeImagesController_LEX.SelectOptioncls> SelectOptioncls= SW_MA2_CouponTypeImagesController_LEX.getCouponTypeList();
       system.assertEquals(SelectOptioncls[1].label , 'Coupon Banner');
        test.stopTest();
    }
    
    Public Static Testmethod void autoPopulateAccValuesTest()
    {
        Coupon__c acc=new Coupon__c(MA2_CouponName__c='Test');
        insert  acc;
        test.startTest();
        Coupon__c CouponTypeImages = SW_MA2_CouponTypeImagesController_LEX.autoPopulateAccValues(acc.Id);
        system.assertEquals(CouponTypeImages.Id ,acc.Id );
        test.stopTest();
    }
    
    Public Static Testmethod void getAllCouponsTest()
    {
        String uploadFile='iVBORw0KGgoAAAANSUhEUgAAAi8AAAFtCAIAAABAzh3pAAAACXBIWXMAAA7EAAAOxA';
        uploadFile = EncodingUtil.urlDecode(uploadFile, 'UTF-8');
        Coupon__c acc=new Coupon__c(MA2_CouponName__c='Test');
        insert  acc;
        MA2_CouponImage__c CoupImg=new MA2_CouponImage__c(MA2_Coupon__c=acc.id,MA2_Coupon_Type__c='Coupon Banner',MA2_Status__c=true);
        insert  CoupImg;
        Attachment attach=new Attachment(Name='Test' , ParentId=CoupImg.Id,body=EncodingUtil.base64Decode(uploadFile));
        insert  attach;
        test.startTest();
        Coupon__c CouponTypeImages = SW_MA2_CouponTypeImagesController_LEX.autoPopulateAccValues(acc.Id);
        List< SW_MA2_CouponTypeImagesController_LEX.WrapperClass> wrapperList= SW_MA2_CouponTypeImagesController_LEX.getAllCoupons(acc.Id);
        system.debug('wrapperList>>>'+wrapperList);
        
        system.assertEquals(wrapperList[0].couponImageId,CoupImg.Id);
        test.stopTest();
    }
    
    Public Static Testmethod void saveCouponRecordTest()
    {
        Coupon__c Coup=new Coupon__c(MA2_CouponName__c='TestCoup');
        insert  Coup;
        Coupon__c Coup2=new Coupon__c(MA2_CouponName__c='TestCoup');
        insert  Coup2;
        MA2_CouponImage__c CoupImg=new MA2_CouponImage__c(MA2_Coupon__c=Coup.id,MA2_Coupon_Type__c='Coupon Banner',MA2_Status__c=true);
        insert  CoupImg;
        
        test.startTest();
        SW_MA2_CouponTypeImagesController_LEX.resultWrapper CouponTypeImages = SW_MA2_CouponTypeImagesController_LEX.saveCouponRecord(CoupImg,Coup.Id,'Testing',
                                                                                                                                      'iVBORw0KGgoAAAANSUhEUgAAAi8AAAFtCAIAAABAzh3pAAAACXBIWXMAAA7EAAAOxA',
                                                                                                                                      'image/jpeg');
        SW_MA2_CouponTypeImagesController_LEX.resultWrapper CouponTypeImagesCont = SW_MA2_CouponTypeImagesController_LEX.saveCouponRecord(CoupImg,Coup2.id,'Testing',
                                                                                                                                          'iVBORw0KGgoAAAANSUhEUgAAAi8AAAFtCAIAAABAzh3pAAAACXBIWXMAAA7EAAAOxA',
                                                                                                                                          'image/jpeg');
        system.debug('CouponTypeImagesCont>>>'+CouponTypeImagesCont);
        system.assertEquals(CouponTypeImagesCont.type , 'success');
        test.stopTest();
    }    
}