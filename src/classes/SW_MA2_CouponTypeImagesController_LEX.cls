public without sharing class SW_MA2_CouponTypeImagesController_LEX {
   
    public List<WrapperClass> wrapperList{get;set;}

    
    @AuraEnabled
    public static List<SelectOptioncls> getCouponTypeList(){
        List<SelectOptioncls> couponTypeList = new List<SelectOptioncls>();
        couponTypeList.add(new SelectOptioncls('','--None--'));
        Schema.sObjectType sobject_type = MA2_CouponImage__c.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get('MA2_Coupon_Type__c').getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for(Schema.PicklistEntry value : pick_list_values) { //for all values in the picklist list
            couponTypeList.add(new SelectOptioncls(value.getValue(), value.getLabel())); //add the value and label to our final list
        }
        return couponTypeList;
    }
    
    public class SelectOptioncls{
        @AuraEnabled public string val{get;set;}
        @AuraEnabled public string label{get;set;}
        public SelectOptioncls(string val, string label){
            this.val = val;
            this.label= label;
        }
    }
    
    @AuraEnabled 
    public static Coupon__c autoPopulateAccValues(String recordID)
    {
        Coupon__c Name = new Coupon__c();
        Name = [SELECT Name FROM Coupon__c WHERE Id=: recordID];
        return Name;
    }
    
    @AuraEnabled
    public static resultWrapper  saveCouponRecord(MA2_CouponImage__c couponImage ,
                                                             String  couponId,String  fileName,String uploadFile,
                                                             String fileType){
        system.debug('couponImage>>>' + couponImage);
        system.debug('couponId>>>' + couponId);
        system.debug('fileName>>>' + fileName);
        system.debug('uploadFile>>>' + uploadFile);
        system.debug('fileType>>>' + fileType);
        List<String> errList = new List<String>();       
        resultWrapper resultWrapperData=new resultWrapper(' ',errList);                                                         
        Integer couponCount = 0;
        if(String.isNotBlank(couponImage.MA2_Coupon_Type__c))
        {
        	couponCount =  [SELECT count() FROM MA2_CouponImage__c 
                            WHERE MA2_Coupon_Type__c =: couponImage.MA2_Coupon_Type__c 
                            AND MA2_Coupon__c =: couponId];
        }
        if(couponCount != 0){
            errList.add('Please Select any other coupon Type for this coupon as Coupon Type already exist.');
        }
        if(fileName == null || fileName == ''){
        	errList.add('Please Enter File Name');
		}
        if(uploadFile == null || uploadFile == ''){
        	errList.add('Please upload File');
		}
		system.debug('errList:: '+errList);
                                                                 
		if(!errList.isEmpty()){
			resultWrapperData = new resultWrapper('error' , errList);
		}
		else{
            if(fileName != null && fileName != '' && fileType != '' && couponCount == 0){
                MA2_CouponImage__c couponImgRecord = new MA2_CouponImage__c();
                couponImgRecord.MA2_Coupon_Type__c = couponImage.MA2_Coupon_Type__c;
                couponImgRecord.MA2_Coupon__c = couponId;
                couponImgRecord.MA2_Status__c = couponImage.MA2_Status__c;
                insert couponImgRecord;
              string  uploadFiledecode = EncodingUtil.urlDecode(uploadFile, 'UTF-8');
                if(couponImgRecord.Id != null){
                    Attachment attachFile = new Attachment();
                    attachFile.Name = fileName;
                    attachFile.body = EncodingUtil.base64Decode(uploadFiledecode);
                    attachFile.ParentId = couponImgRecord.Id;
                    attachFile.contentType = fileType;
                    insert attachFile;
                    resultWrapperData = new resultWrapper('success' , null);
                }
            }
		}
                                                                 
        return resultWrapperData;
    }
   
    public class resultWrapper{
        @AuraEnabled public String type{get;set;}
        @AuraEnabled public List<String> value{get;set;}
        public resultWrapper(String type, List<String> value){
            this.type = type;
            this.value = value;
        }
    }
    
    
    @AuraEnabled 
    public static List<WrapperClass> getAllCoupons( Id couponId) {
        List<WrapperClass> wrapperList = new List<WrapperClass>();
        Set<Id> couponImgIdList = new Set<Id>();
        List<MA2_CouponImage__c> couponImageList = [select Id,MA2_Coupon_Type__c,MA2_Status__c,MA2_Coupon__c 
                                                    from MA2_CouponImage__c 
                                                    where MA2_Coupon__c =: couponId];
        for(MA2_CouponImage__c ci : couponImageList){
            couponImgIdList.add(ci.Id);
        }
        List<Attachment> attachmentList = [select Name , ParentId , createdDate , createdBy.Name , CreatedById 
                                           from Attachment where ParentId in: couponImgIdList];
        for(Attachment attach : attachmentList){
            for(MA2_CouponImage__c couponImage : couponImageList){    
                if(couponImage.Id == attach.ParentId){
                    wrapperList.add(new WrapperClass(attach.Id,attach.Name,couponImage.Id,couponImage.MA2_Coupon_Type__c,couponImage.MA2_Status__c,attach.createdDate.format(),attach.createdBy.Name,attach.CreatedById));    
                }
            }    
        }
        return wrapperList;
    }
    
    
    @AuraEnabled 
    public static void deleteRecord( Id couponImageId) {
        MA2_CouponImage__c couponImage = new MA2_CouponImage__c();
        couponImage.Id = couponImageId;
        delete couponImage;
    }
        
    //Inner class for setting and getting all the file info along with file Type
    public class WrapperClass{
        @AuraEnabled  public Id fileId{get; set;}
        @AuraEnabled public String fileName{get; set;}
        @AuraEnabled public Id couponImageId{get; set;}
        @AuraEnabled public String couponType{get; set;}
        @AuraEnabled  public Boolean status{get;set;}
        @AuraEnabled public String createdDate{get; set;}
        @AuraEnabled  public String createdBy{get; set;}
        @AuraEnabled  public Id createdById{get; set;}
        public WrapperClass(Id fileId,String fileName,Id couponImageId,String couponType,Boolean status,String createdDate,String createdBy,Id createdById){
            this.fileId = fileId;
            this.fileName = fileName;
            this.couponType = couponType;
            this.status = status;
            this.createdDate = createdDate;
            this.createdBy = createdBy;
            this.createdById = createdById;
            this.couponImageId = couponImageId;
        }
    }
}