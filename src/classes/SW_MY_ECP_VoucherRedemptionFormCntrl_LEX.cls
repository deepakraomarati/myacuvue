public without sharing class SW_MY_ECP_VoucherRedemptionFormCntrl_LEX
{
    public static String Name;
    public static String NRIC;
    public static String fileName;
    public static String SelectedBrand1;
    public static String SelectedBrand2;
    public static String SelectedBrand3;
    public static String Box1;
    public static String Box2;
    public static String Box3;
    public static list<CampaignMember> lstCampMember;   
    public static Id conId;
    public static String LoggedInEcpId;
    public static string MYCampaign;
    public static String ECPId;
    public static String ContactName;
    public static String AccountId;
    public static Integer fileSize;
    public static list<string> errorlist;
    @AuraEnabled
    public static String Onload(String ECPId)
    {
        List<Account> loggedinECP = [SELECT Id,Name,Phone,OutletNumber__c FROM Account WHERE Id=:ECPId LIMIT 1];
        return loggedinECP[0].Name;
    }
    @AuraEnabled
    public static CampaignMember setNRIC(String SelectedID)
    {
        List<CampaignMember> NRICnum = [SELECT Contact.Name, Contact.NRIC__c FROM CampaignMember WHERE Id =:SelectedID LIMIT 1];
        return NRICnum[0];
    }
    @AuraEnabled
    public static String getName(String SelectedID)
    {
        List<CampaignMember> Name = [SELECT ContactId__r.Name FROM CampaignMember WHERE Id =:SelectedID LIMIT 1];
        return Name[0].ContactId__r.Name;
    }
    @AuraEnabled
    public static resultWrapper RedeemVoucher(String fileName, String base64Data, String contentType,String AccountId,String Name ,String NRIC ,String Box1 ,String Box2 ,String Box3)
    {
        String tempAccountId = AccountId;
        boolean check=false;
        SelectedBrand1='1D4';
        SelectedBrand2='1MA';
        SelectedBrand3='1DL';
        errorlist= New List<String>();
        List<Contact> lstcontact = new List<Contact>();
        try {
            Map<String,ExactTarget_Integration_Settings__c> MapcampId = ExactTarget_Integration_Settings__c.getAll();
            MYCampaign = MapcampId.get('MYCampaign').Value__c;
            // lstcontact = [SELECT Id,Name,FirstName,LastName,NRIC__c FROM Contact WHERE Name=:Name AND NRIC__c=:NRIC LIMIT 1];
            string searchquery = 'FIND \'' + String.escapeSingleQuotes(Name) + ' AND ' + String.escapeSingleQuotes(NRIC) + '\' IN ALL FIELDS RETURNING Contact(Id,Name,NRIC__c) LIMIT 1';
            system.debug('string'+searchquery);
            List<List<sObject>> searchConList = search.query(searchQuery);
            system.debug('searchlist'+ searchConList);
            lstcontact = ((List<Contact>)searchConList[0]);
            system.debug('listcontact'+lstcontact);
            if(lstcontact.isEmpty() && lstcontact.size()==0)
            {
                resultWrapper result = new resultWrapper('Error' ,System.Label.Invalid_User);
                return result;
            }
            if(lstcontact.size()>0)
            {
                conId=lstcontact[0].Id;
                String  MemberqryString;
                MemberqryString = 'SELECT Id,Name,SAP_ID__c,Account__c,Receipt_number__c,contactId,AttachmentId__c,CampaignId,Voucher_Status__c FROM CampaignMember WHERE CampaignId=:MYCampaign AND contactId=:conId LIMIT 1';
                lstCampMember=  Database.Query(MemberqryString);
                if(AccountId==''||AccountId==Null)
                {
                    //AccountId = LoggedInEcpId;
                    tempAccountId = LoggedInEcpId;
                }
                //Check extension type for Receipt Image
                if((fileName != Null && fileName != '') )
                { 
                    String fileExtension = fileName.substringAfterLast('.');
                    
                    if((fileExtension != 'jpg') && (fileExtension != 'jpeg') && (fileExtension != 'bmp') && (fileExtension != 'png') 
                       && (fileExtension != 'gif') && (fileExtension != 'pdf'))
                    {
                        check=true;
                        resultWrapper result = new resultWrapper('Error' ,System.Label.Receipt_File_Extension_Check);
                        return result;
                    }
                }
               
                //Validate Consumer selected store-location
                if((!lstCampMember.isEmpty()&& lstCampMember.size()>0)&&(lstCampMember[0].Account__c!=tempAccountId))
                {   
                    check=true;
                    resultWrapper result = new resultWrapper('Error' ,System.Label.Not_Valid_Store);
                    return result;
                }
                if(lstCampMember[0].Voucher_Status__c =='Not Used' && lstCampMember[0].AttachmentId__c == Null && String.isNotBlank(base64Data))
                {
                    String urlbase64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
                    Database.SaveResult attachmentResult;
                    Attachment attachment = new Attachment();
                    attachment.body = EncodingUtil.base64Decode(urlbase64Data);
                    attachment.name = fileName;
                    attachment.parentId = conId;
                    attachmentResult = Database.insert(attachment);
                    if (attachmentResult == null || !attachmentResult.isSuccess())
                    {   
                        check=true;
                        resultWrapper result = new resultWrapper('Error' ,System.Label.Image_Upload_Error);
                        return result;
                    }
                    else {
                        if(!check)
                        { 
                            if(Box1!=null && Box1!='')
                            {
                                lstCampMember[0].Product1__c= SelectedBrand1;lstCampMember[0].No_of_Boxes1__c= Integer.ValueOf(Box1);
                            }
                            if(Box2!=null && Box2!='')
                            {
                                lstCampMember[0].Product2__c= SelectedBrand2;lstCampMember[0].No_of_Boxes2__c= Integer.ValueOf(Box2);
                            }
                            if(Box3!=null && Box3!='')
                            {
                                lstCampMember[0].Product3__c= SelectedBrand3;lstCampMember[0].No_of_Boxes3__c= Integer.ValueOf(Box3);
                            }
                            lstCampMember[0].AttachmentId__c = attachmentResult.Id;
                            lstCampMember[0].Voucher_Status__c='Redeemed';
                            lstCampMember[0].Redeemed_Date_Time__c = System.Now();
                            update lstCampMember;
                            resultWrapper result = new resultWrapper('Success' ,System.Label.Thank_You);
                            return result;
                        }
                    }
                }
                else if(lstCampMember[0].Voucher_Status__c =='Redeemed'){
                    resultWrapper result = new resultWrapper('Error' ,System.Label.Voucher_Code_Redeemed);
                    return result;
                }
                else{
                    resultWrapper result = new resultWrapper('Error' ,System.Label.Invalid_Voucher_Code);
                    return result;
                }
            }
        }
        catch (Exception e)
        {
            system.debug(e.getStackTraceString());
            resultWrapper result = new resultWrapper('Error',e.getMessage());
            return result;
        }
        return null;
    }
    public class resultWrapper
    {
        @AuraEnabled public String type{get;set;}
        @AuraEnabled public String value{get;set;}
        public resultWrapper(String type, String value)
        {
            this.type = type;
            this.value = value;
        }
    }
}