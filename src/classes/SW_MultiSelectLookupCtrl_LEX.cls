public without sharing class SW_MultiSelectLookupCtrl_LEX {
    
    @AuraEnabled
    Public static List<User> lookupResults(string keyword, List<User> excludeList){
        String searchedWord = '%'+ keyword + '%';
        
        List<Id> excludedVal = new List<Id>();
        for(User u : excludeList){
            excludedVal.add(u.id);
        }
        
        string searchQuery = 'SELECT Id, Name FROM User WHERE name LIKE: searchedWord AND id NOT IN : excludedVal order by createdDate DESC limit 5';
        List<User> lookupReturnList = Database.query(searchQuery);
        return lookupReturnList;
    }
    
    @AuraEnabled
    public static Map<String,String> saveAttendeeList ( Id eventRecId, List<Id> attendeeIds) {
        // Forming a  string map to return response
        Map<String,String> resultMap = new Map<String,String>();
        system.debug('eventRecId + attendeeIds========>' + eventRecId+'---'+attendeeIds);
        // Adding try catch for exception handling
        try {
            List<EventRelation> eventRelationList =  new List<EventRelation>();
            // Adding Attendee Ids to the Event
            for(Id attendee : attendeeIds){
                EventRelation  ea = new EventRelation();
                ea.EventId  = eventRecId ;
                ea.RelationId = attendee;
                
                eventRelationList.add(ea);
            }
            
            Database.insert(eventRelationList,false);
            system.debug('eventRelationLists========>' + eventRelationList);
            
            // Setting the success status and message in resultMap
            resultMap.put('status', 'success');
            resultMap.put('message', 'Attendee Added Successfully');        
        }
        catch(Exception e) {
            // Setting the error status and message in resultMap
            resultMap.put('status', 'error');
            system.debug(e.getMessage());
            resultMap.put('message',e.getMessage());
        }
        // Returning the result string map
        return resultMap;
    } 
    
    @AuraEnabled
    public static List<recordValue> getAllEventAttendees ( Id eventRecId) {
        List<recordValue> response = new List<recordValue>();
        // Forming a  string map to return response
        //The final wrapper response to return to component
        try{
            
            for(EventRelation eventrel :[SELECT Id, RelationId,Relation.name,Response FROM 
                                         EventRelation WHERE EventId=:eventRecId ]){
                                             string st=eventrel.RelationId;
                                             if(st.startsWith('005')){
                                             response.add(new recordValue(eventrel.Id,eventrel.RelationId,eventrel.Relation.name,eventrel.Response));
                                         }
                                         }
        }
        catch(Exception e) {
            
        }
        // Returning the result string map
        return response;
    } 
    
    
    //Wrapper class to hold Columns with headers
    public class recordValue {
        @AuraEnabled
        public String Id {get;set;}
        @AuraEnabled
        public String RelationId {get;set;}
        @AuraEnabled       
        public String username {get;set;}
        @AuraEnabled
        public String response {get;set;}
        
        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public recordValue(String Id, String RelationId, String username, String response){
            this.Id =Id;
            this.RelationId ='User';
            this.username = username;
            this.response = response;            
        }
    }
    
    //Wrapper class to hold Columns with headers
    public class DataTableColumns {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled       
        public String fieldName {get;set;}
        @AuraEnabled
        public String type {get;set;}
        
        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public DataTableColumns(String label, String fieldName, String type){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;            
        }
    }
    
    //Wrapper calss to hold response - This response is used in the lightning:datatable component
    public class DataTableResponse {
        @AuraEnabled
        public List<DataTableColumns> lstDataTableColumns {get;set;}
        @AuraEnabled
        public List<sObject> lstDataTableData {get;set;}                
        
        public DataTableResponse(){
            lstDataTableColumns = new List<DataTableColumns>();
            lstDataTableData = new List<sObject>();
        }
    }
    
    @AuraEnabled 
    public static void deleteRecord( Id EventRecordId) {
        system.debug('EventRecordId'+EventRecordId);
        EventRelation EventRecord = new EventRelation();
        EventRecord.Id = EventRecordId;
             system.debug('EventRecord##'+EventRecord);
        delete EventRecord;
    }
}