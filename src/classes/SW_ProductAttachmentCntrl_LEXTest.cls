@istest
public class SW_ProductAttachmentCntrl_LEXTest {
    
    Public Static Testmethod void uploadAttachmentTest()
    {
        Product2 Prod=new Product2(Name='TestCoup');
        insert  Prod;
        test.startTest();
        SW_ProductAttachmentCntrl_LEX.resultWrapper resultWrapper = SW_ProductAttachmentCntrl_LEX.uploadAttachment(Prod.Id,'Testing',
                                                                                                                   'iVBORw0KGgoAAAANSUhEUgAAAi8AAAFtCAIAAABAzh3pAAAACXBIWXMAAA7EAAAOxA',
                                                                                                                   'image/jpeg');
        system.debug('resultWrapper>>>'+resultWrapper);
        system.assertEquals(resultWrapper.type , 'success');
        test.stopTest();
    }     
    
    Public Static Testmethod void getAllAttachmentTest()
    {
        String uploadFile='iVBORw0KGgoAAAANSUhEUgAAAi8AAAFtCAIAAABAzh3pAAAACXBIWXMAAA7EAAAOxA';
        uploadFile = EncodingUtil.urlDecode(uploadFile, 'UTF-8');
        
        Product2 Prod=new Product2(Name='TestCoup');
        insert  Prod;
        Attachment attach=new Attachment(Name='Test' , ParentId=Prod.Id,body=EncodingUtil.base64Decode(uploadFile));
        insert  attach;
        test.startTest();
        List< SW_ProductAttachmentCntrl_LEX.WrapperClass> wrapperList= SW_ProductAttachmentCntrl_LEX.getAllAttachment(Prod.Id);
        system.debug('wrapperList>>>'+wrapperList);
        
        system.assertEquals(wrapperList[0].Id,attach.Id);
        test.stopTest();
    }
    
    Public Static Testmethod void deleteRecordTest()
    {
        String uploadFile='iVBORw0KGgoAAAANSUhEUgAAAi8AAAFtCAIAAABAzh3pAAAACXBIWXMAAA7EAAAOxA';
        uploadFile = EncodingUtil.urlDecode(uploadFile, 'UTF-8');
        Product2 Prod=new Product2(Name='TestCoup');
        insert  Prod;
        Attachment attach=new Attachment(Name='Test' , ParentId=Prod.Id,body=EncodingUtil.base64Decode(uploadFile));
        insert  attach;
        
        test.startTest();
        SW_ProductAttachmentCntrl_LEX.deleteRecord(attach.Id);
       Integer  Count = [SELECT count() from Attachment where id =:attach.id];
        system.debug('Count>>>'+Count);
      system.assertEquals(Count,0);
        test.stopTest();
    } 
    
}