public with sharing class SW_RecordTypeSelector_LEX {
    @AuraEnabled
    public static List<RecordType> getListOfRecordType(){
        List<RecordType> rtNames = new List<RecordType>();  
        Schema.SObjectType  objType = Event.SObjectType;        
        
        for(RecordTypeInfo rt : objType.getDescribe().getRecordTypeInfos()){
            system.debug('rt :: ' + rt);
            // Ignore the Master Record Type, whose Id always ends with 'AAA'.
            if (rt.isAvailable() && !String.valueOf(rt.getRecordTypeId()).endsWith('AAA'))
                rtNames.add(new RecordType(Id = rt.getRecordTypeId(),Name = rt.getName(), DeveloperName = rt.getDeveloperName()));
        }    
        return rtNames;    
    }
}