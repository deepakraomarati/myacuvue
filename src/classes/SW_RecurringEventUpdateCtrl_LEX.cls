global without sharing  class SW_RecurringEventUpdateCtrl_LEX {

    @AuraEnabled
    public static ReturnEventValues onLoadValue(Id recordId){
       	ReturnEventValues wrap = new ReturnEventValues();
        Event myEvent =  new Event();
        myEvent = [Select RecurrenceStartDateTime,RecurrenceEndDateOnly,RecurrenceInterval,IsRecurrence,OwnerId,
                   Subject,RecurrenceInstance, WhatId,StartDateTime,EndDateTime,RecurrenceActivityId,
                   RecurrenceType,RecurrenceDayOfWeekMask, 
                   RecurrenceDayOfMonth,RecurrenceMonthOfYear From Event 
                   where id=:recordId];
        
        if(myEvent.RecurrenceActivityId == null){
            wrap.msg = 'It is not a Recurring Event. Hence it cannot be updated.';       
        }
        else{
            Event parentRecurEvent = new Event();
            parentRecurEvent = [Select RecurrenceStartDateTime,RecurrenceEndDateOnly,RecurrenceInterval,IsRecurrence,OwnerId,
                                    Subject,RecurrenceInstance, WhatId,StartDateTime,EndDateTime,RecurrenceActivityId,
                                RecurrenceType,RecurrenceDayOfWeekMask, 
                                RecurrenceDayOfMonth,RecurrenceMonthOfYear From Event 
                                where id =:myEvent.RecurrenceActivityId];
            
            wrap.evt=  parentRecurEvent;
            wrap.IsRecurrence = true;
            wrap.recDayOfWeek = parentRecurEvent.RecurrenceDayOfWeekMask;
            integer RECURRENCEDAYOFWEEKMASK = parentRecurEvent.RecurrenceDayOfWeekMask;
            if(parentRecurEvent.RecurrenceType == 'RecursWeekly')
            {
                if(RECURRENCEDAYOFWEEKMASK >= 64) {
                    RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK-64;
                    wrap.sat = true;
                }
                if(RECURRENCEDAYOFWEEKMASK >= 32 ){
                    RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK-32;
                    wrap.fri = true;
                }
                if(RECURRENCEDAYOFWEEKMASK >= 16 ){
                    RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK-16;
                    wrap.thu = true;
                }
                if(RECURRENCEDAYOFWEEKMASK >= 8 ){
                    RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK-8;
                    wrap.wed = true;
                }
                if(RECURRENCEDAYOFWEEKMASK >= 4 ){
                    RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK-4;
                    wrap.tue = true;
                }
                if(RECURRENCEDAYOFWEEKMASK >= 2 ){
                    RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK-2;
                    wrap.mon = true;
                }
                if(RECURRENCEDAYOFWEEKMASK >= 1 ){
                    RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK-1;
                    wrap.sun = true;
                }
            }
    	}
        system.debug('wrap ::: ' + wrap);
        return wrap;
        
    }
    @AuraEnabled
    public static saveResultWrapper saveRecRecord(boolean mon, boolean tue, boolean wed, boolean thu, boolean fri,
                                     boolean sat, boolean sun,  Event evt){                                 
        integer RECURRENCEDAYOFWEEKMASK ;
        system.debug('masterRecuEvent');
        if(evt.RecurrenceType == 'RecursWeekly')
        {
            evt.RecurrenceDayOfMonth = null;
            evt.RecurrenceMonthOfYear = null;
            evt.RecurrenceInstance  = null;
            RECURRENCEDAYOFWEEKMASK = 0;
            if(sun==true){
                RECURRENCEDAYOFWEEKMASK = 1;
            }
            if(mon== true){
                RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK+2;
            }
            if(tue== true){
                RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK+4;
            }
            if(wed== true){
                RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK+8;
            }
            if(thu== true){
                RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK+16;
            }
            if(fri== true){
                RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK+32;
            }
            if(sat== true){
                RECURRENCEDAYOFWEEKMASK= RECURRENCEDAYOFWEEKMASK+64;
            }
		}
		if(evt.RecurrenceType == 'RecursYearly')
		{
            RECURRENCEDAYOFWEEKMASK = evt.RECURRENCEDAYOFWEEKMASK;
            evt.RecurrenceInterval = null;
            evt.RecurrenceInstance = null;
		}
		if(evt.RecurrenceType == 'RecursYearlyNth')
		{
            RECURRENCEDAYOFWEEKMASK = evt.RECURRENCEDAYOFWEEKMASK;
            evt.RecurrenceInterval = null;
		}
		if(evt.RecurrenceType == 'RecursEveryWeekday')
		{
            RECURRENCEDAYOFWEEKMASK = evt.RECURRENCEDAYOFWEEKMASK;
            evt.RecurrenceDayOfMonth = null;
            evt.RecurrenceMonthOfYear = null;
            evt.RecurrenceInterval = null;
            evt.RecurrenceInstance = null;
		}
		if(evt.RecurrenceType == 'RecursMonthly')
		{
            RECURRENCEDAYOFWEEKMASK = evt.RECURRENCEDAYOFWEEKMASK;
            evt.RecurrenceMonthOfYear = null;
            evt.RecurrenceInstance = null;
		}
		if(evt.RecurrenceType == 'RecursMonthlyNth')
		{
            RECURRENCEDAYOFWEEKMASK = evt.RECURRENCEDAYOFWEEKMASK;
            evt.RecurrenceMonthOfYear = null;
            evt.RecurrenceDayOfMonth = null;
		}                         
		if(evt.RecurrenceType == 'RecursDaily')
		{
            evt.RecurrenceDayOfMonth = null;
            evt.RecurrenceMonthOfYear = null;
            evt.RecurrenceInstance = null;
		}
		Event masterRecuEvent=new Event ();
		if(!Test.isrunningTest()){
         masterRecuEvent = [Select Id,RecurrenceStartDateTime,RecurrenceEndDateOnly,RecurrenceInterval,
                                 IsRecurrence,OwnerId,Subject,WhatId,StartDateTime,EndDateTime,RecurrenceActivityId,
                                 RecurrenceDayOfMonth,RecurrenceMonthOfYear,RecurrenceDayOfWeekMask
                                 From Event 
                                 where id=:evt.RecurrenceActivityId];
                                         }
        System.debug('RECURRENCEDAYOFWEEKMASK'+RECURRENCEDAYOFWEEKMASK);
        masterRecuEvent.RecurrenceDayOfWeekMask = RECURRENCEDAYOFWEEKMASK;
        masterRecuEvent.RecurrenceDayOfMonth 	= evt.RecurrenceDayOfMonth;
        masterRecuEvent.RecurrenceInstance 		= evt.RecurrenceInstance;
        masterRecuEvent.RecurrenceMonthOfYear 	= evt.RecurrenceMonthOfYear;
        masterRecuEvent.RecurrenceEndDateOnly   = evt.RecurrenceEndDateOnly;
        masterRecuEvent.RecurrenceStartDateTime = evt.RecurrenceStartDateTime;                                 
        masterRecuEvent.RecurrenceType   		= evt.RecurrenceType;
                                         
        //if(evt.RecurrenceInterval != null)                                 
		masterRecuEvent.RecurrenceInterval      = evt.RecurrenceInterval;                                 
        system.debug('masterRecuEvent-->'+masterRecuEvent);
        try{
        	upsert masterRecuEvent;
            saveResultWrapper saveResultWrapperData = new saveResultWrapper('SUCCESS',masterRecuEvent.Id);
        	return saveResultWrapperData;
        }
        catch(DmlException ex){
        	system.debug(ex);
            saveResultWrapper saveResultWrapperData = new saveResultWrapper('ERROR',ex.getDmlMessage(0));
            return saveResultWrapperData;
        }
    }
    global class saveResultWrapper{
        @AuraEnabled String status {get; set;}
        @AuraEnabled String msg {get; set;}
        global saveResultWrapper(String status, String msg){
            this.status = status;
            this.msg = msg;
        }
    }
    public class ReturnEventValues{
        @AuraEnabled Event evt {get; set;}
        @AuraEnabled integer recDayOfWeek {get; set;}
        @AuraEnabled boolean day {get; set;}
        @AuraEnabled boolean mon {get; set;}
        @AuraEnabled boolean tue {get; set;}
        @AuraEnabled boolean wed {get; set;}
        @AuraEnabled boolean thu {get; set;}
        @AuraEnabled boolean fri {get; set;}
        @AuraEnabled boolean sat {get; set;}
        @AuraEnabled boolean sun {get; set;}
        @AuraEnabled string msg {get; set;}
        @AuraEnabled boolean IsRecurrence {get; set;}
    }
}