public without sharing class SW_SignatureController_LEX {
    
    //This method will save the signature  into the notes and attachments. 
    @AuraEnabled
    public static String uploadSignature(String contractId, String b64SignData){
        try {
            List<Attachment> attachedFiles = new List<Attachment>();
            Attachment emptySign = new Attachment();
            emptySign.Body = EncodingUtil.base64Decode('iVBORw0KGgoAAAANSUhEUgAAAV4AAABkCAYAAADOvVhlAAADOklEQVR4Xu3UwQkAAAgDMbv/0m5xr7hAIcjtHAECBAikAkvXjBEgQIDACa8nIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECDweoABlt2MJjgAAAABJRU5ErkJggg==');
            Attachment IpadSign = new Attachment();
            IpadSign.Body = EncodingUtil.base64Decode('iVBORw0KGgoAAAANSUhEUgAAAV4AAABkCAYAAADOvVhlAAAAAXNSR0IArs4c6QAAAnpJREFUeAHt0AENAAAAwqD3T+3sAREoDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAgQ8MI2IAATBw6SgAAAAASUVORK5CYII=');
            Contract cont =  [select recordtypeid,Send_Email__c FROM Contract WHERE Id = :contractId LIMIT 1];
            RecordType objRecType = [select id,name from Recordtype where id = :cont.recordtypeid limit 1];
            string RecName = objRecType.name;
            string errorMessage='';
            Attachment attach = new Attachment();
            system.debug('signature'+b64SignData);
            
            //You will want to tie your attachment to some type of custom or standard object
            attach.ParentId = contractId;
            //attach.Name = 'Signature'+String.valueOf(Date.today()).substring(0,10);
            attach.Body =  EncodingUtil.base64Decode(b64SignData);
            attach.OwnerId = UserInfo.getUserId();
            //If we were saving a PDF as an attachment the ContentType would be 'pdf'
            attach.contentType = 'image/png';
             if(RecName == '아큐브 멀티포컬 취급 약정서')
             {
                  attach.Name = 'Signature Contract 1';
                 attachedFiles = [select Id from Attachment where parentId =:contractId AND Name ='Signature Contract 1' order By LastModifiedDate DESC limit 1];
                 
             }
            else if(RecName == '마이아큐브 프로그램 제공 및 사용 약정서')
            {
               attach.Name = 'Signature Contract 2';
                attachedFiles = [select Id from Attachment where parentId =:contractId AND Name ='Signature Contract 2' order By LastModifiedDate DESC limit 1];
                
            }
             else if(RecName == '아큐브 매출 및 지원 약정서')
            {
               attach.Name = 'Signature Contract 3';
                attachedFiles = [select Id from Attachment where parentId =:contractId AND Name ='Signature Contract 3' order By LastModifiedDate DESC limit 1];
            }
             else if(RecName == '공급계약서')
            {
               attach.Name = 'Signature Contract 4';
               attachedFiles = [select Id from Attachment where parentId =:contractId AND Name ='Signature Contract 4' order By LastModifiedDate DESC limit 1]; 
            }
            if (attach.Body != emptySign.Body && attach.Body != IpadSign.Body)
            {
                if( attachedFiles != null && attachedFiles.size() > 0)
                    {
                        attachedFiles[0].Body = EncodingUtil.base64Decode(b64SignData);
                        update attachedFiles[0];
                    }
                else
                    {
                        insert attach;
                    }
            //insert attach;
           
            }
             return '';
        }
        catch (Exception e)
        { 
            //throw new AuraHandledException(string.valueof(e.getMessage() +','+ e.getStackTraceString()));
           throw e;
        }
    }
    //This method will return the account and contract details to the component.
    @AuraEnabled
    public static contract accountDetails(string contrctId )
    {
        try{
            contract  contractObj = [SELECT AccountId,Local_Alias__c,Send_Email__c, BC_Card_Merchant_Number__c, 
                                     Business_Registration_Number__c,Account.OutletNumber__c,Account.Name,
                                     Account.ShippingStreet,Account.ShippingPostalCode,Account.ShippingCountry 
                                     FROM Contract 
                                     WHERE Id = :contrctId];
            return contractObj;
        }
        
        catch (Exception e) { 
            //throw new AuraHandledException(string.valueof(e.getMessage() +','+ e.getStackTraceString())); 
            throw e;
        }
    }
}