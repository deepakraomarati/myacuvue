@isTest(SeeallData=true)
public class SW_SignatureController_LEXTest {
    
    static testMethod void uploadSignatureTest()
    {
        Account acc=new Account();
        acc.Name='Ryan';
        insert acc;
        
        Id conId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('아큐브 멀티포컬 취급 약정서').getRecordTypeId();
        Id conId1 = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('마이아큐브 프로그램 제공 및 사용 약정서').getRecordTypeId();
        Id conId2 = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('아큐브 매출 및 지원 약정서').getRecordTypeId();
        Id conId3 = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('공급계약서').getRecordTypeId();
        Contract cont=new Contract();
        cont.RecordTypeId=conId;
        cont.AccountId=acc.id;
        
        insert cont;
        SW_SignatureController_LEX.uploadSignature(cont.id,'Test');
        Contract cont1=new Contract();
        cont1.RecordTypeId=conId1;
        cont1.AccountId=acc.id;
        insert cont1;
        string sign=SW_SignatureController_LEX.uploadSignature(cont1.id,'Test');
        
        System.assertEquals(sign,'');
        Contract cont2=new Contract();
        cont2.RecordTypeId=conId2;
        cont2.AccountId=acc.id;
        insert cont2;
        SW_SignatureController_LEX.uploadSignature(cont2.id,'Test');
        Contract cont3=new Contract();
        cont3.RecordTypeId=conId3;
        cont3.AccountId=acc.id;
        insert cont3;
        SW_SignatureController_LEX.uploadSignature(cont3.id,'Test');
        try{
            SW_SignatureController_LEX.uploadSignature('Test','Test');
        }
        Catch(Exception e){}
        SW_SignatureController_LEX.accountDetails(cont.Id);
        try{
            SW_SignatureController_LEX.accountDetails('Test');
        }
        Catch(Exception e){}
        
    }
    
}