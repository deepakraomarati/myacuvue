public without sharing  class SW_TransactionListController_LEX {
    public class finalFOC{
        @AuraEnabled public DateTime FromDate{get;set;}
        @AuraEnabled public MA2_TransactionListController.FOCFinalWrapper finWrapper{get;set;}
        public finalFOC(DateTime FromDate, MA2_TransactionListController.FOCFinalWrapper finWrapper){
            this.FromDate = FromDate;
            this.finWrapper = finWrapper;
        }
    }
    
    @AuraEnabled
    public static MA2_TransactionListController.FOCFinalWrapper getFOCReport(String FilterAcNme, Datetime ToDat, Datetime FromDat,string accNumber,string pubzone,string AccouOwner){
        MA2_TransactionListController.FOCFinalWrapper finalWrapper = new MA2_TransactionListController.FOCFinalWrapper();
        MA2_TransactionListController transactionList = new MA2_TransactionListController('LEX');
        finalWrapper = transactionList.setSearchParam(FilterAcNme, ToDat, FromDat,accNumber,pubzone,AccouOwner,'LEX');
        
        finalFOC finalFOCWrapper = new finalFOC(FromDat, finalWrapper);
        // return finalFOCWrapper;  
        String themesss = UserInfo.getUiThemeDisplayed(); 
        system.debug('finalWrapper ::123 ' + themesss);
        
        return finalWrapper;      
    }
    
    @AuraEnabled
    
    public static string  pushValuesToAccount(String FilterAcNme, Datetime ToDat, Datetime FromDat,string accNumber,string pubzone,string AccouOwner)
    {
        
        string pushResult='';
        MA2_TransactionListController transactionList = new MA2_TransactionListController();
        
        MA2_TransactionListController.FOCFinalWrapper finalWrapper = new MA2_TransactionListController.FOCFinalWrapper();
        
        finalWrapper = transactionList.setSearchParam(FilterAcNme, ToDat, FromDat,accNumber,pubzone,AccouOwner,'LEX');
        
        pushResult=transactionList.pushingToAccount();
        
        return  pushResult;   
        
    }
    @AuraEnabled
    public static   string mailSending(String FilterAcNme, Datetime ToDat, Datetime FromDat,string accNumber,string pubzone, string AccouOwner)
    {
        MA2_TransactionListController.FOCFinalWrapper finalWrapper = new MA2_TransactionListController.FOCFinalWrapper();
        MA2_TransactionListController transactionList = new MA2_TransactionListController();
        finalWrapper = transactionList.setSearchParam(FilterAcNme, ToDat, FromDat,accNumber,pubzone,AccouOwner,'LEX');
        string mailData=transactionList.SendingEmail();
        system.debug('mailData'+mailData);
        return mailData;
        
    }
    @AuraEnabled
    public static   void  SendPDF(String FilterAcNme, Datetime ToDat, Datetime FromDat,string accNumber,string pubzone,string AccouOwner)
    {
        MA2_TransactionListController.FOCFinalWrapper finalWrapper = new MA2_TransactionListController.FOCFinalWrapper();
        MA2_TransactionListController transactionList = new MA2_TransactionListController();
        finalWrapper = transactionList.setSearchParam(FilterAcNme, ToDat, FromDat,accNumber,pubzone,AccouOwner,'LEX');
        transactionList.SendPDF();
        
        
        
    }
}