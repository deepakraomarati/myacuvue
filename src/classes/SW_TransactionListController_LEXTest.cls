@isTest(seealldata=true)
public class SW_TransactionListController_LEXTest {    
    static testMethod void getFocReportTest()
    {
        
        RecordType rec = [Select id from RecordType where Name = 'Cash Discount' limit 1];
        RecordType rec1 = [Select id from RecordType where Name = 'Consumer' limit 1];
        RecordType rec2 = [Select id from RecordType where Name = 'ECP' limit 1];
        Test.startTest();
        Account acc = new Account();
        acc.Name='Test Accout1';
        acc.ECP_Name__c='Test ECP1';
        acc.OutletNumber__c='000123';
        acc.MA2_PriceGroup__c='Group A';
        acc.My_Acuvue__c = 'Yes';
        
        insert acc;
        
        Contact con = new Contact();
        con.FirstName='Test';
        con.LastName='Contact';
        con.accountId=acc.id;
        con.RecordTypeId=rec1.id;
        insert con;
        
        Contact con2 = new Contact();
        con2.FirstName='Test';
        con2.LastName='Contact';
        con2.accountId=acc.id;
        con2.RecordTypeId=rec2.id;
        con2.MembershipNo__c='eC1666N';
        insert con2;
        /*con2.FirstName='Test1';
con2.LastName='Contact1';
con2.accountId=acc.id;
con2.RecordTypeId=rec2.id;
// con2.MembershipNo__c='eC166eN';
insert con2;*/
        
        Transaction__c transact = new Transaction__c();
        transact.AccountID__c=acc.Id;
        transact.ContactID__c=con.Id;
        //insert transact;
        
        TransactionTd__c trans = new TransactionTd__c();
        trans.AccountID__c=acc.Id;
        trans.TransTD_createddate__c = system.now();
        trans.MA2_CancelDate__c= system.now();
        trans.MA2_CountryCode__c='HKG';
        trans.MA2_TransactionType__c='Products';
        trans.MA2_Price__c=120.50;
        trans.MA2_Contact__c=con.id;
        trans.MA2_Points__c=120;  
        trans.MA2_ContactId__c=con.id;
        trans.MA2_ECPContact__c=con2.id;
        insert trans;
        
        
        TransactionTd__c trans2 = new TransactionTd__c();
        trans2.AccountID__c=acc.Id;
        trans2.TransTD_createddate__c = system.now();
        trans2.MA2_CancelDate__c= system.now();
        trans2.MA2_CountryCode__c='HKG';
        trans2.MA2_TransactionType__c='Products';
        trans2.MA2_Price__c=120.50;
        trans2.MA2_Contact__c=con.id;
        trans2.MA2_Points__c=120;  
        trans2.MA2_ContactId__c=con.id;
        trans2.MA2_ECPContact__c=con2.id;
        insert trans2;
        
        Coupon__c coupon = new Coupon__c();
        coupon.MA2_CountryCode__c='HKG';
        coupon.CouponName__c='test Coupon';
        coupon.CouponType__c='Cash Discount';
        coupon.Price__c=50;
        coupon.RecordTypeId=rec.id;
        coupon.MA2_CouponValidity__c=40;
        coupon.StartDate__c=System.Today();
        coupon.EndDate__c=System.Today()+1;
        insert coupon; 
        
        CouponContact__c coupwalt = new CouponContact__c();
        coupwalt.ContactId__c=con.id;
        coupwalt.AccountId__c=acc.Id;
        coupwalt.CouponId__c=coupon.Id;
        coupwalt.MA2_ContactId__c=con.Id;
        coupwalt.TransactionId__c= trans.id;
        insert coupwalt; 
        
        CouponContact__c coupwalt1 = new CouponContact__c();
        coupwalt1.ContactId__c=con.id;
        coupwalt1.AccountId__c=acc.Id;
        coupwalt1.CouponId__c=coupon.Id;
        coupwalt1.MA2_ContactId__c=con.Id;
        coupwalt1.TransactionId__c= trans2.id;
        insert coupwalt1; 
        SW_TransactionListController_LEX.getFOCReport('Test Accout', system.now(), system.now(), '123456', '', '') ;
        String pushValues = SW_TransactionListController_LEX.pushValuesToAccount('Test Accout', system.now(), system.now(), '123456', '', '') ;
        String mail 	  = SW_TransactionListController_LEX.mailSending('Test Accout', system.now(), system.now(), '123456', '', '') ;
        SW_TransactionListController_LEX.SendPDF('Test Accout', system.now(), system.now(), '123456', '', '');
        Test.stopTest();
        System.assertEquals(pushValues, pushValues);
        System.assertEquals(mail, mail);
        
    }
    
}