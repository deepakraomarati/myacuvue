@isTest
public class SW_customLookUpController_LEXTest {
    
    static testMethod void fetchLookUpValuesTest()
    {
        Account acc=new Account();
        acc.Name='Ryan';
        acc.AccountNumber=null;
        insert acc;
        SW_customLookUpController_LEX.fetchLookUpValues('Ryan','Account');
        system.assertEquals('Ryan',acc.Name);
    }

}