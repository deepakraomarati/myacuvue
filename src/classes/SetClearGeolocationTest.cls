/**
 * File Name: SetGeolocationTest 
 * Description : Test Class for SetGeolocation and ClearGoogleMap Triggers
 * Copyright : Johnson & Johnson
 * @author : Shobana K G | skg6@its.jnj.com | Shobana.KG@cognizant.com
 * 
 * Modification Log 
 * =============================================================== 
 *  Ver  |Date         |Author                |Modification
 *  1.0  |30-Aug-2018  |skg6@its.jnj.com      |New Class created
 */

@isTest
private class SetClearGeolocationTest {
    
    private static final Integer ACCMAX = 5;
    private static Set<Id> accountIds = new Set<Id>();
    
    private static void setupTestData() {
    	// Insert Accounts
        List<Account> accList = new List<Account>();
        for (Integer i=1; i<=ACCMAX; i++) {
            accList.add(new Account(Name = 'Account'+i, ECP_Name__c = 'Test Outlet'+i, CountryCode__c = 'SGP', ActiveYN__c = true));
        } 
		insert accList;
		List<Account> insertedAccts = [SELECT Id from Account LIMIT :ACCMAX];
        
        for(Account a : insertedAccts) {
        	accountIds.add(a.Id);    
        }
        
        // Create custom setting - TriggerHandler__c
		TriggerHandler__c trgHandler = new TriggerHandler__c(Name = 'HandleTriggers', IsSetGeolocationAcitve__c = true, ClearGoogleMap__c = true);
        insert trgHandler;
        
        // Create custom setting - Canvas_Geolocation_CountryCodes__c
		Canvas_Geolocation_CountryCodes__c countryCodes = new Canvas_Geolocation_CountryCodes__c(Name = 'SGP');
        insert countryCodes;
	}

	@isTest
    private static void TestLoadGeolocation() {
        // Setup Test Data
        setupTestData();
        
        // Execute Test
        Test.startTest();
		List<Account> updAccts = new List<Account>();          
        for (Account a : [SELECT Id, PublicAddress__c, PublicZone__c, PublicState__c FROM Account WHERE Id IN: accountIds]) {
            a.PublicAddress__c = '10 SINARAN DRIVE #03-108, NOVENA SQUARE 2 SINGAPORE 307506';
            a.PublicZone__c = a.PublicState__c = 'THOMSON/UPPER THOMSON/NOVENA';
            updAccts.add(a);
        }        
        update updAccts;
        Test.stopTest();
        
        // Assert
        List<Account> accList = [SELECT Id, Geolocation__Latitude__s, Geolocation__Longitude__s FROM Account WHERE Id IN: accountIds];
        System.assertNotEquals(null, accList[0].Geolocation__Latitude__s, 'Issue in Geolocation loading');
        
    }
    
    @isTest
    private static void TestClearGeolocation() {
        // Setup Test Data
        setupTestData();
        
        // Execute Test
        Test.startTest();
        List<Account> updAccts = new List<Account>();  
        for (Account a : [SELECT Id, PublicAddress__c, PublicZone__c, PublicState__c FROM Account WHERE Id IN: accountIds]) {
            a.PublicAddress__c = a.PublicZone__c = a.PublicState__c = null;
            updAccts.add(a);
        }
        update updAccts;
        Test.stopTest();
        
        // Assert
        List<Account> accList = [SELECT Id, Geolocation__Latitude__s, Geolocation__Longitude__s FROM Account WHERE Id IN: accountIds];
        System.assertEquals(null, accList[0].Geolocation__Latitude__s, 'Issue in Geolocation clearing');
        
    }

}