global with sharing class Sign_Contract_Controller
{
    @TestVisible private Account account;
    @TestVisible private Contract contract;
    
    public Sign_Contract_Controller()
    {
        contract = [select AccountId,Local_Alias__c, BC_Card_Merchant_Number__c, Business_Registration_Number__c FROM Contract WHERE Id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];
        account = [SELECT Id, OutletNumber__c, Name, ShippingStreet, ShippingPostalCode, ShippingCountry, Owner.FirstName, Owner.LastName FROM Account WHERE Id =:contract.AccountId];
    }
    
    public Account getAccount()
    {
        return account;
    }
    public Contract getContract()
    {
        return contract;
    }
    public PageReference save()
    {
        update account;
        update contract;
        return null;
    }
    
    @RemoteAction
    global static String saveSignature(String imageUrl, String contractId)
    {
        try
        {
            List<Attachment> attachedFiles = new List<Attachment>();
            Contract cont =  [select recordtypeid,Send_Email__c FROM Contract WHERE Id = :contractId LIMIT 1];
            RecordType objRecType = [select id,name from Recordtype where id = :cont.recordtypeid limit 1];
            string RecName = objRecType.name;
            Attachment emptySign = new Attachment();
            emptySign.Body = EncodingUtil.base64Decode('iVBORw0KGgoAAAANSUhEUgAAAV4AAABkCAYAAADOvVhlAAADOklEQVR4Xu3UwQkAAAgDMbv/0m5xr7hAIcjtHAECBAikAkvXjBEgQIDACa8nIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECAivHyBAgEAsILwxuDkCBAgIrx8gQIBALCC8Mbg5AgQICK8fIECAQCwgvDG4OQIECDweoABlt2MJjgAAAABJRU5ErkJggg==');
            Attachment IpadSign = new Attachment();
            IpadSign.Body = EncodingUtil.base64Decode('iVBORw0KGgoAAAANSUhEUgAAAV4AAABkCAYAAADOvVhlAAAAAXNSR0IArs4c6QAAAnpJREFUeAHt0AENAAAAwqD3T+3sAREoDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAAQMGDBgwYMCAgQ8MI2IAATBw6SgAAAAASUVORK5CYII=');
            Attachment ConSign = new Attachment();
            ConSign.ParentID = contractId;
            ConSign.Body = EncodingUtil.base64Decode(imageUrl);
            System.Debug('t---'+imageUrl);
            ConSign.contentType = 'image/png';
            ConSign.OwnerId = UserInfo.getUserId();
            
            if(RecName == '아큐브 멀티포컬 취급 약정서')
            {
               ConSign.Name = 'Signature Contract 1';
               attachedFiles = [select Id from Attachment where parentId =:contractId AND Name ='Signature Contract 1' order By LastModifiedDate DESC limit 1];
            }
            else if(RecName == '마이아큐브 프로그램 제공 및 사용 약정서')
            {
               ConSign.Name = 'Signature Contract 2';
               attachedFiles = [select Id from Attachment where parentId =:contractId AND Name ='Signature Contract 2' order By LastModifiedDate DESC limit 1];
            }
            else if(RecName == '아큐브 매출 및 지원 약정서')
            {
               ConSign.Name = 'Signature Contract 3';
               attachedFiles = [select Id from Attachment where parentId =:contractId AND Name ='Signature Contract 3' order By LastModifiedDate DESC limit 1];
            }
            else if(RecName == '공급계약서')
            {
               ConSign.Name = 'Signature Contract 4';
               attachedFiles = [select Id from Attachment where parentId =:contractId AND Name ='Signature Contract 4' order By LastModifiedDate DESC limit 1];
            }
            
            if (ConSign.Body != emptySign.Body && ConSign.Body != IpadSign.Body)
            {
                if( attachedFiles != null && attachedFiles.size() > 0)
                    {
                        attachedFiles[0].Body = EncodingUtil.base64Decode(imageUrl);
                        update attachedFiles[0];
                    }
                else
                    {
                        insert ConSign;
                    }
                return 'Contract Signed and will be emailed to ('+cont.Send_Email__c + '). Please update Email ID if required.';
            }
            else
            {
                    return 'Failed to save the Contract. Please Sign the Contract';
            }
        }
        catch(Exception e)
        {
            system.debug('---------- ' + e.getMessage());
            return JSON.serialize(e.getMessage());
        }
        return null; 
    }
}