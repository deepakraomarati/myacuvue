@RestResource(urlmapping='/apex/storelocator/*')
global without sharing class StoreLocator{
    
    /* --------------------------------------------
* Author: 
* Company: BICS
* Description : service return the store information
* ---------------------------------------------*/
    
    global static final String INPUT_REQUIRED = 'ACE0020';
    global static final String Account_NOT    = 'ACE0021';
    global static final String ERROR_TYPE = 'getStoreInfo';
    
    @HTTPGet
    global static String doGet()
    {
        RestRequest req=RestContext.request;
        RestResponse res = RestContext.response;
        String[] UriKey = RestContext.request.requestURI.split('/');
        
        if(UriKey.size()<6)
        {
            return String.ValueOf(CustomException.getException(INPUT_REQUIRED,ERROR_TYPE));
        }
        
        if(UriKey.size() ==7)
        {
            String DistanceRange = UriKey[6];
            String Longitude = UriKey[5];
            String Latitude = UriKey[4];
            String Country = UriKey[3];    
            //return getLocalPractices(Country,Latitude,Longitude,DistanceRange,'');
        }
        
        if(UriKey.size() == 8)
        {
            String ECPName = UriKey[7];
            String DistanceRange = UriKey[6];
            String Longitude = UriKey[5];
            String Latitude = UriKey[4];
            String Country = UriKey[3];
            //return getLocalPractices(Country,Latitude,Longitude,DistanceRange,ECPName);
        }
        return null;
    }
    
/*  global static String getLocalPractices(String Country,String Latitude,String Longitude,String DistanceRange,String ECPName)
    {
        
        try{
            
            String Qry;
            String UnitsOfMeasure ='mi';
            List<StoreDTO> lsdto = new List<StoreDTO>();
            List<Account> lstaccount=new List<Account>();
            
            if(ECPName !=null && ECPName!='')
            {
                
                String AccountName;
                
                if(ECPName.contains('%20'))
                {
                    AccountName = ECPName.replace('%20',' ');
                }else{
                    AccountName = ECPName;
                }
                
                lstaccount = [Select Id,
                              Name,
                              Email__c,
                              ActiveYN__c,
                              Geolocation__Latitude__s,
                              Geolocation__Longitude__s,
                              Phone,
                              OutletNumber__c,
                              AccountNumber,
                              GoogleMapX__c,
                              eOrderYN__c,
                              GoogleMapY__c,
                              BillingPostalCode,
                              BillingStreet,
                              BillingCity,
                              BillingState,
                              ECP_Name__c,
                              PublicAddress__c,
                              PublicZone__c,
                              PublicState__c,
                              CountryCode__c,
                              BillingCountry
                              From Account Where ActiveYN__c = true and CountryCode__c=:Country and ECP_Name__c Like :AccountName+'%'];
                
                if(lstaccount.size()>0 && !lstaccount.isEmpty())
                {
                    for(Account acc:lstaccount)
                    {
                        StoreDTO std = new StoreDTO(String.ValueOf(acc.Id),acc.ECP_Name__c,acc.OutletNumber__c,acc.PublicAddress__c,acc.PublicZone__c,acc.PublicState__c,acc.CountryCode__c,acc.Geolocation__Latitude__s,acc.Geolocation__Longitude__s,acc.Email__c,acc.Phone,acc.eOrderYN__c);
                        lsdto.add(std);
                    }
                    
                    return Json.serialize(lsdto);
                    
                }else{
                    
                    throw new CustomException(CustomException.getException(Account_NOT,ERROR_TYPE));
                }
                
            }else{
                
                Qry = '';
                Qry = 'Select Id,Name,Email__c,ActiveYN__c,Geolocation__Latitude__s,Geolocation__Longitude__s,PublicAddress__c,PublicZone__c,PublicState__c,CountryCode__c,Phone,OutletNumber__c,AccountNumber,GoogleMapX__c,eOrderYN__c,GoogleMapY__c,BillingPostalCode,BillingStreet,BillingCity,BillingState,ECP_Name__c,BillingCountry From Account'; 
                Qry+= ' Where ActiveYN__c = true  and DISTANCE(GeoLocation__c,GEOLOCATION('+Latitude+','+Longitude+'),\''+UnitsOfMeasure+'\')<'+DistanceRange+''; 
                Qry+= ' ORDER BY DISTANCE(GeoLocation__c,GEOLOCATION('+Latitude+','+Longitude+'),\''+UnitsOfMeasure+'\')  ASC';
                
                lstaccount = Database.query(Qry);
                
                if(lstaccount.size()>0 && !lstaccount.isEmpty())
                {
                    for(Account acc:lstaccount)
                    {
                        StoreDTO std = new StoreDTO(String.ValueOf(acc.Id),acc.ECP_Name__c,acc.OutletNumber__c,acc.PublicAddress__c,acc.PublicZone__c,acc.PublicState__c,acc.CountryCode__c,acc.Geolocation__Latitude__s,acc.Geolocation__Longitude__s,acc.Email__c,acc.Phone,acc.eOrderYN__c);
                        lsdto.add(std);
                    }
                    
                    return Json.serialize(lsdto);
                }else{
                    throw new CustomException(CustomException.getException(Account_NOT,ERROR_TYPE));
                }
            }
            return null;
        }catch(Exception e)
        {
            return e.getMessage();
        }
        return null;
    }
    
    global class StoreDTO
    {
        public String Id {get;set;}
        public String ECPName {get;set;}
        public String OutletNumber {get;set;}
        public String StreetAddress {get;set;}
        public String City {get;set;}
        public String State {get;set;}
        public String Country {get;set;}
        public Decimal Latitude {get;set;}
        public Decimal Longitude {get;set;}
        public String Email {get;set;}
        public String Phone {get;set;}
        public Boolean eOrdering {get;set;}
        
        public StoreDTO(String Id,String ECPName,String OutletNumber,String StreetAddress,String City,String State,String Country,Decimal GoogleMapx,Decimal GoogleMapy,String Email,String Phone,Boolean eOrdering)
        {
            this.Id = Id;
            this.ECPName = ECPName;
            this.OutletNumber = OutletNumber;
            this.StreetAddress = StreetAddress;
            this.City = City;
            this.State = State;
            this.Country = Country;
            this.Latitude = GoogleMapx;
            this.Longitude = GoogleMapy;
            this.Email = Email;
            this.Phone = Phone;
            this.eOrdering = eOrdering;          
        }     
    }*/
}