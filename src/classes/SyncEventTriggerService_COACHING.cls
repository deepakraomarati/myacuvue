Public Class SyncEventTriggerService_COACHING
{   
    
    String RTName;
    Public static final String CompletedStatus = 'Completed';
    Public static final String PlannedStatus = 'Planned';
    Public static final String DeferredStatus = 'Deferred';

    public static void syncEventInsert(List<COACHING__c> coachings)
    {
    String loginUserCountryName = [select country from User where Id =: UserInfo.getUserId()].Country;
        if(loginUserCountryName != 'Japan')
        {
        try
        {
           Id coachingOWO = [SELECT Id FROM RecordType WHERE Name =: 'OWO'].Id;
           Id tot = [SELECT Id FROM RecordType WHERE Name =: 'Time Off Territory ANZ'].Id;
           System.Debug('check---'+loginUserCountryName);
            List<Event> createEvents = new List<Event>();
           System.Debug('Id---'+tot);
            for(COACHING__c eachCOACHING : coachings){
                //Creating Event object and adding fields as per COACHING record.
                               
                Event eachEvent = new Event();               
                eachEvent.WhatId = eachCOACHING.Id;
                eachEvent.Subject = eachCOACHING.Name;
                eachEvent.StartDateTime = eachCOACHING.Start_Date__c;
                eachEvent.EndDateTime = eachCOACHING.End_Date__c;
                if(eachCOACHING.Coaching_Status__c == CompletedStatus)
                {
                   eachEvent.ActivityStatus__c =  'Completed';
                }
                if(eachCOACHING.Coaching_Status__c == 'Open')
                {
                    eachEvent.ActivityStatus__c = PlannedStatus;
                }
                else if(eachCOACHING.Coaching_Status__c == 'Reviewed')
                {
                    eachEvent.ActivityStatus__c = DeferredStatus;
                }
           //Defining Activity Type only For ANZ Users for OWO Coaching
                
            if((loginUserCountryName.contains('Australia') || loginUserCountryName.contains('New Zealand')) && eachCOACHING.RecordTypeId == coachingOWO)
            {   
                eachEvent.OwnerId = eachCOACHING.Coachee__c; 
                eachEvent.ActivityType__c = 'Training/OwO (full day)';
                eachEvent.RecordTypeId = tot;
                System.Debug(' eachEvent.RecordTypeId'+ eachEvent.RecordTypeId);
            } else
            {
                eachEvent.OwnerId = eachCOACHING.OwnerId;
                eachEvent.ActivityType__c = eachCOACHING.Type__c;
            }
                
                
                system.debug('++++++++country ++++ ' + eachCOACHING.User_Country__c);
           if(eachCOACHING.RecordTypeId != coachingOWO){    
                
                if(eachCOACHING.User_Country__c.contains('Korea') || eachCOACHING.User_Country__c.contains('korea')){
                    eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call KR' and SobjectType = 'Event'].Id;
                }else if(eachCOACHING.User_Country__c.contains('Australia') || eachCOACHING.User_Country__c.contains('New Zealand')){
                    eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call ANZ' and SobjectType = 'Event'].Id;    
                 }
                else if(eachCOACHING.User_Country__c.contains('Hong Kong') || eachCOACHING.User_Country__c.contains('hong kong')){
                    eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call HKG' and SobjectType = 'Event'].Id;    
                }
                else if(eachCOACHING.User_Country__c.contains('Singapore') || eachCOACHING.User_Country__c.contains('singapore')){
                     eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call SG' and SobjectType = 'Event'].Id;
                }
                else if(eachCOACHING.User_Country__c.contains('Taiwan') || eachCOACHING.User_Country__c.contains('taiwan')){
                     eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call TW' and SobjectType = 'Event'].Id;
                }
                else if(eachCOACHING.User_Country__c.contains('India') || eachCOACHING.User_Country__c.contains('india')){
                     eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call IN' and SobjectType = 'Event'].Id;
                }
                else if(eachCOACHING.User_Country__c.contains('Indonesia') || eachCOACHING.User_Country__c.contains('indonesia')){
                     eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call ID' and SobjectType = 'Event'].Id;
                }
                else if(eachCOACHING.User_Country__c.contains('China') || eachCOACHING.User_Country__c.contains('china')){
                     eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call CH' and SobjectType = 'Event'].Id;
                }
                /*else if(eachCOACHING.User_Country__c.contains('Malaysia') || eachCOACHING.User_Country__c.contains('malaysia')){
                     eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call MY' and SobjectType = 'Event'].Id;
                }*/
                else if(eachCOACHING.User_Country__c.contains('Malaysia') || eachCOACHING.User_Country__c.contains('malaysia')){
                     eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Customer Development Manager Call MY' and SobjectType = 'Event'].Id;
                }
                else if(eachCOACHING.User_Country__c.contains('Thailand') || eachCOACHING.User_Country__c.contains('thailand')){
                     eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call TH' and SobjectType = 'Event'].Id;
                }
                /*else{
                    eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call HKG' and SobjectType = 'Event'].Id;    
                }*/
             }
                
                createEvents.add(eachEvent);
                system.debug('++++++++Event Values ++++ ' + eachEvent);
                system.debug('++++++++Event Values ++++ ' + eachCOACHING );
            }
            System.Debug('createEvents'+ createEvents);
            try{
                //Bulkify - Inserting records in bulk.
                if(createEvents.size()>0){
                    insert createEvents;
                    //Creating an Invitee for the Owner_ANZ User
                    List<Event> getEventList = [select Id from Event where Id in: createEvents];    
                    List<EventRelation> eventRelationList = new List<EventRelation>();
                    for(COACHING__c eachCOACHING : coachings){
                        EventRelation er = new EventRelation();
                        er.RelationId = eachCOACHING.OwnerId;
                        for(Event e : getEventList){
                            er.EventId = e.Id;
                            eventRelationList.add(er);
                        }
                    }
                    if(eventRelationList.size()>0){
                        insert eventRelationList;
                    }
                 }
                //syncCallInsert() method will update the Event Ids back to call records.
            }catch(Exception insertFailedException){
                system.debug('Exception--> syncEventInsert Insert Failed : '+insertFailedException.getMessage());
            }
        }Catch(Exception ex){
            system.debug('Exception--> syncEventInsert Failed : '+ex.getMessage());
        }
        }
    }
    
    public static void syncEventUpdate(List<COACHING__c> coachings, Map<Id,COACHING__c> coachingsMap){
    
        String ids = UserInfo.getUserId();     
         
           List<Event> getEvents = [Select id,WhatId,ActivityStatus__c from Event where whatid in: coachingsMap.keyset() AND Ischild=false];
           String loginUserCountryName = [select country from User where Id =: UserInfo.getUserId()].Country;
           Id coachingOWO = [SELECT Id FROM RecordType WHERE Name =: 'OWO'].Id;
           Id tot = [SELECT Id FROM RecordType WHERE Name =: 'Time Off Territory ANZ'].Id;
           List<Event> updateEventsList1 = new List<Event>();
 
            for(COACHING__c coach : coachings){
                for(Event eachEvent : getEvents){

                    if(coach.id==eachEvent.whatId){
                        eachEvent.StartDateTime=coach.Start_Date__c;
                        eachEvent.EndDateTime = coach.End_Date__c;
                            if(coach.Coaching_Status__c == 'Open')
                            {
                                eachEvent.ActivityStatus__c = PlannedStatus;
                            }
                            if(coach.Coaching_Status__c == 'Reviewed')
                            {
                                eachEvent.ActivityStatus__c = DeferredStatus;
                            }
                            if(coach.Coaching_Status__c == CompletedStatus)
                            {
                                eachEvent.ActivityStatus__c =  'Completed';
                            }
                        updateEventsList1.add(eachEvent);
                        system.debug(updateEventsList1+ 'updateEventsList1---');
                    }
                    if((loginUserCountryName.contains('Australia') || loginUserCountryName.contains('New Zealand')) && coach.RecordTypeId == coachingOWO){   
                        eachEvent.OwnerId = coach.Coachee__c; 
                        eachEvent.ActivityType__c = 'Training/OwO (full day)';
                        eachEvent.RecordTypeId = tot;
                    } 
                    else{
                        eachEvent.OwnerId = coach.OwnerId;
                        eachEvent.ActivityType__c = coach.Type__c;
                    }
                    
                    if(coach.RecordTypeId != coachingOWO){  
                        //if(eachCOACHING.User_Country__c == '%Korea%')
                        if(coach.User_Country__c.contains('Korea') || coach.User_Country__c.contains('korea')){
                            eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call KR' and SobjectType = 'Event'].Id;
                        }
                        else if(coach.User_Country__c.contains('Australia') || coach.User_Country__c.contains('New Zealand')){
                            eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call ANZ' and SobjectType = 'Event'].Id;    
                         }
                        else if(coach.User_Country__c.contains('Hong Kong') || coach.User_Country__c.contains('hong kong')){
                            eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call HKG' and SobjectType = 'Event'].Id;    
                        }
                        else if(coach.User_Country__c.contains('Singapore') || coach.User_Country__c.contains('singapore')){
                             eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call SG' and SobjectType = 'Event'].Id;
                        }
                        else if(coach.User_Country__c.contains('Taiwan') || coach.User_Country__c.contains('taiwan')){
                             eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call TW' and SobjectType = 'Event'].Id;
                        }
                        else if(coach.User_Country__c.contains('Thailand') || coach.User_Country__c.contains('thailand')){
                             eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call TH' and SobjectType = 'Event'].Id;
                        }
                        /*else if(coach.User_Country__c.contains('Malaysia') || coach.User_Country__c.contains('malaysia')){
                             eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call MY' and SobjectType = 'Event'].Id;
                        }*/
                        else if(coach.User_Country__c.contains('Malaysia') || coach.User_Country__c.contains('malaysia')){
                             eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Customer Development Manager Call MY' and SobjectType = 'Event'].Id;
                        }
                        else if(coach.User_Country__c.contains('Indonesia') || coach.User_Country__c.contains('indonesia')){
                             eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call ID' and SobjectType = 'Event'].Id;
                        }
                        else if(coach.User_Country__c.contains('India') || coach.User_Country__c.contains('india')){
                             eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call IN' and SobjectType = 'Event'].Id;
                        }
                        else if(coach.User_Country__c.contains('China') || coach.User_Country__c.contains('china')){
                             eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call CH' and SobjectType = 'Event'].Id;
                        }
                        /*else{
                            eachEvent.RecordTypeId = [select Id from RecordType where Name = 'Sales Manager Call HKG' and SobjectType = 'Event'].Id;    
                        }*/
                    }
                }
            }
             try{
                    ValidationRule__c myCS = ValidationRule__c.getOrgDefaults();
                    myCS.ActivityStatus__c = TRUE;
                    update myCS;
                    if(!updateEventsList1.isEmpty())
                        upsert updateEventsList1;
             }
             catch(Exception e){
                 system.debug('Exception--> syncEventInsert Insert Failed : '+e.getMessage());
             }
             finally{
                 ValidationRule__c newCS = ValidationRule__c.getOrgDefaults();
                 newCS.ActivityStatus__c = FALSE;
                 upsert newCS;
             }
       } 
 }