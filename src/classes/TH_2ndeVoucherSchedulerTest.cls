@isTest 
private class TH_2ndeVoucherSchedulerTest {
    private static testmethod void validatedailyScheduler() {
        Test.startTest();
        campaign cam=new Campaign(Name='THCampaign',IsActive=true);
        insert cam; 
        ExactTarget_Integration_Settings__c TH = new ExactTarget_Integration_Settings__c();
        TH.Name = 'THCampaign';
        TH.Value__c = cam.Id;
        insert TH;
        
        TH_2ndeVoucherScheduler dailybatch = new TH_2ndeVoucherScheduler();      
        String sch = '0  00 1 3 * ?';
        system.schedule('TH_2ndeVoucherScheduler', sch,dailybatch);
        system.assertEquals(sch,'0  00 1 3 * ?','success');
        Test.stopTest();        
    }
}