global class TH_Assignmentfor2ndeVoucher implements Database.Batchable<sObject>{
    
    global final string query;   
    global Database.QueryLocator start(Database.BatchableContext BC){
        string THCampaign = '';
        Map<String,ExactTarget_Integration_Settings__c> MapcampId = ExactTarget_Integration_Settings__c.getAll();
        THCampaign = MapcampId.get('THCampaign').Value__c;
        return Database.getQueryLocator([select Id,CampaignId,ContactId,Account__c,CompanyOrAccount,Communication_Agreement__c,Voucher_Status__c,FirstName,LastName,Type,Redeemed_Date_Time__c from CampaignMember WHERE CampaignId=:THCampaign 
                                         and TH_2ndVoucher__c = false and Voucher_Status__c = 'Redeemed']);        
    }
    
    global void execute(Database.batchableContext BC,List<CampaignMember> scope){
        string THCampaignII = '';
        Map<String,ExactTarget_Integration_Settings__c> MapcampId = ExactTarget_Integration_Settings__c.getAll();
        THCampaignII = MapcampId.get('THCampaignII').Value__c;
        List<CampaignMember> CampaignMem2List = new List<CampaignMember>();
        List<CampaignMember> CampaignMem1List = new List<CampaignMember>();
        date redeemeddate;
        for(CampaignMember item: scope){
            if(item.Redeemed_Date_Time__c != null){
                redeemeddate = item.Redeemed_Date_Time__c.date();
                if(redeemeddate == (system.today()-21)){
                    item.TH_2ndVoucher__c = true;
                    CampaignMem1List.add(item);
                    CampaignMember campvar = new CampaignMember();
                    campvar.CampaignId = THCampaignII;
                    campvar.ContactId = item.ContactId;
                    campvar.Account__c = item.Account__c;
                    campvar.Communication_Agreement__c = item.Communication_Agreement__c;
                    campvar.Voucher_Status__c = 'Not Used';
                    CampaignMem2List.add(campvar); 
                }
            }    
            
        }
        
        insert CampaignMem2List;
        update CampaignMem1List;
    }
    
    global void finish(Database.batchableContext BC){
        
    }
}