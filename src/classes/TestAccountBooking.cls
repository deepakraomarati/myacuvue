@isTest
public with sharing class TestAccountBooking 
{
    static testmethod void doinsertAccountBooking()
    {
        Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id;
        
        List<AccountBooking__c> lstaccbk =new List<AccountBooking__c>();
        
        account ac = new account();
        ac.name='test';
        ac.Aws_AccountId__c ='123';
        insert ac;
        
        contact con =new contact();
        con.RecordTypeId = consumerRecTypeId;
        con.lastname='test1';
        con.Aws_ContactId__c ='1234';
        con.MA2_Country_Code__c = 'KOR';
        insert con;
        
        AccountBooking__c accbk = new AccountBooking__c();
        accbk.Aws_AccountId__c='123';
        accbk.Aws_ContactId__c='1234';
        lstaccbk.add(accbk);
        
        AccountBooking__c accbk1 = new AccountBooking__c();
        accbk1.Aws_AccountId__c='12345';
        accbk1.Aws_ContactId__c='123467';
        lstaccbk.add(accbk1);
        insert lstaccbk;
        
        AccountBooking__c accbk2 = new AccountBooking__c();
        accbk2.Aws_AccountId__c='00000';
        accbk2.Aws_ContactId__c='00000';
        insert accbk2;
        
        system.assertEquals(accbk1.Aws_AccountId__c,'12345','success');        
    }
}