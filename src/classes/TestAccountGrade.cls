@isTest
public with sharing class TestAccountGrade 
{
    static testmethod void doinsertAccountGrade()
    {
        list<AccountGrade__c> lstaccgrd = new list<AccountGrade__c>();
        
        account ac = new account();
        ac.name='test';
        ac.Aws_AccountId__c ='123';
        insert ac;
        
        AccountGrade__c accgrd = new AccountGrade__c();
        accgrd.Aws_AccountId__c='123';
        accgrd.Aws_ContactId__c='1234';
        lstaccgrd.add(accgrd);
        
        AccountGrade__c accgrd1 = new AccountGrade__c();
        accgrd1.Aws_AccountId__c='12345';
        accgrd1.Aws_ContactId__c='1234567';
        lstaccgrd.add(accgrd1);
        insert lstaccgrd;
        
        AccountGrade__c accgrd2 = new AccountGrade__c();
        accgrd2.Aws_AccountId__c='00000';
        accgrd2.Aws_ContactId__c='00000';
        insert accgrd2;
        system.assertEquals(accgrd1.Aws_AccountId__c,'12345','success');
    }  
}