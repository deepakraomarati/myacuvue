@isTest
public with sharing class TestAccountInfo 
{
    static testmethod void doinsertAccountInfo()
    {
        list<AccountInfo__c> lstaccinfo = new list<AccountInfo__c>();
        
        account ac = new account();
        ac.name='test';
        ac.OutletNumber__c='123';
        insert ac;
        
        AccountInfo__c accinfo = new AccountInfo__c();
        accinfo.Aws_OutletNumber__c='123';
        lstaccinfo.add(accinfo);
        
        AccountInfo__c accinfo1 = new AccountInfo__c();
        accinfo1.Aws_OutletNumber__c='12345';
        lstaccinfo.add(accinfo1);
        insert lstaccinfo;
        
        AccountInfo__c accinfo2 = new AccountInfo__c();
        accinfo2.Aws_OutletNumber__c='00000';
        insert accinfo2;
        system.assertEquals(accinfo1.Aws_OutletNumber__c,'12345','success');
    }
}