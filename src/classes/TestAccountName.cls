@isTest
public with sharing class TestAccountName
{
    static testmethod void doinsertAccountName()
    {   
        list<AccountName__c> lstaccname = new list<AccountName__c>();
        
        account ac = new account();
        ac.name = 'test';
        ac.OutletNumber__c = '123';
        insert ac;
        
        AccountName__c accname = new AccountName__c();
        accname.Aws_OutletNumber__c='123';
        lstaccname.add(accname);
        
        AccountName__c accname1 = new AccountName__c();
        accname1.Aws_OutletNumber__c='12345';
        lstaccname.add(accname1);
        insert lstaccname;
        
        AccountName__c accname2 = new AccountName__c();
        accname2.Aws_OutletNumber__c='00000';
        insert accname2;
        system.assertEquals(accname1.Aws_OutletNumber__c,'12345','success');
    }
}