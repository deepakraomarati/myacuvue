@isTest
public with sharing class TestAccountPayInfo 
{
    static testmethod void doinsertAccountPayInfo()
    {
        list<AccountPayInfo__c> lstaccpay = new list<AccountPayInfo__c>();
        account ac = new account();
        ac.name='test';
        ac.OutletNumber__c='123';
        insert ac;
        
        AccountPayInfo__c accpay = new AccountPayInfo__c();
        accpay.Aws_OutletNumber__c='123';
        lstaccpay.add(accpay);
        
        AccountPayInfo__c accpay1 = new AccountPayInfo__c();
        accpay1.Aws_OutletNumber__c='12345';
        lstaccpay.add(accpay1);
        insert lstaccpay;
        
        AccountPayInfo__c accpay2 = new AccountPayInfo__c();
        accpay2.Aws_OutletNumber__c='00000';
        insert accpay2;
        system.assertEquals(accpay1.Aws_OutletNumber__c,'12345','success');
    }
}