@isTest
public with sharing class TestAccountQuestion 
{
    static testmethod void doinsertAccountQuestion()
    {
        Credientials__c cred=new Credientials__c();
        cred.Name='CouponSendToApigee';
        cred.Api_Key__c='xCP22MdvhMSpPmi2T5SKDq8bwSLvuqRm';
        cred.Client_Id__c='b3U65J1vylrxEeavNcUjU0xIEg';
        cred.Client_Secret__c='b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY';
        cred.Target_Url__c='https://jnj-prod.apigee.net/v1/sfdc/configuration/add';
        insert cred;
        list<AccountQuestion__c> lstaccques = new list<AccountQuestion__c>();
        
        account ac = new account();
        ac.name='test';
        ac.Aws_AccountId__c ='123';
        insert ac;
        
        Coupon__c coup = new Coupon__c();
        coup.DB_ID__c = 789;
        insert coup;
        
        AccountQuestion__c accques = new AccountQuestion__c();
        accques.Aws_AccountId__c='123';
        accques.Aws_ContactId__c='456';
        accques.Aws_CouponId__c='789';
        lstaccques.add(accques);
        
        AccountQuestion__c accques1 = new AccountQuestion__c();
        accques1.Aws_AccountId__c='12345';
        accques1.Aws_ContactId__c='45678';
        accques1.Aws_CouponId__c='78912';
        lstaccques.add(accques1);
        insert lstaccques;
        
        AccountQuestion__c accques2 = new AccountQuestion__c();
        accques2.Aws_AccountId__c='00000';
        accques2.Aws_ContactId__c='00000';
        accques2.Aws_CouponId__c='00000';
        insert accques2;
        system.assertEquals(accques1.Aws_AccountId__c,'12345','success');
    }
}