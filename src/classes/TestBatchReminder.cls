@isTest
private class TestBatchReminder{   
    static testMethod void BatchReminder() {
        
        list<CampaignMember> lstmem = new list<CampaignMember>();
        
        Contact con     = new contact();
        con.LastName    = 'ChandraMouli';
        con.NRIC__c     = '20A199112';
        con.Phone       = '9642324578';
        insert con;
        
        Campaign cam    = new campaign();
        cam.Name        = '$30 e-voucher registration';
        cam.Country__c = 'SINGAPORE';
        cam.SG_Campaign__c  = true;
        cam.SMS_TriggerSend__c = 'ODA6Nzg6MA';
        cam.Code__c    = '91';
        insert cam;
        
        CampaignMember cmem = new campaignMember();
        cmem.ContactId = con.id;
        cmem.campaignId = cam.Id;
        cmem.Reminder_Sent__c= false;
        cmem.Voucher_Status__c ='Not Used';
        insert cmem;
        
        ExactTarget_Integration_Settings__c ex= new ExactTarget_Integration_Settings__c();
        ex.name= 'Default Channel Member ID';
        ex.Value__c='7203707';
        insert ex;  
        
        ExactTarget_Integration_Settings__c ex1= new ExactTarget_Integration_Settings__c();
        ex1.name= 'SECURITY_ENDPOINT';
        ex1.Value__c='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        insert ex1;  
        
        ExactTarget_Integration_Settings__c ex2= new ExactTarget_Integration_Settings__c();
        ex2.name= 'username';
        ex2.Value__c= 'sumanbics';
        insert ex2;  
        
        ExactTarget_Integration_Settings__c ex3= new ExactTarget_Integration_Settings__c();
        ex3.name= 'password';
        ex3.Value__c= 'Thylaksoft@1';
        insert ex3;  
        
        ExactTarget_Integration_Settings__c ex4= new ExactTarget_Integration_Settings__c();
        ex4.name= 'endpoint';
        ex4.Value__c= 'https://webservice.s7.exacttarget.com/Service.asmx';
        insert ex4;  
        
        ExactTarget_Integration_Settings__c ex5= new ExactTarget_Integration_Settings__c();
        ex5.name= 'ET_CLIENT_ID';
        ex5.Value__c= 'b9kth7ojw80erkg6svaq1wnk';
        insert ex5;  
        
        ExactTarget_Integration_Settings__c ex6= new ExactTarget_Integration_Settings__c();
        ex6.name= 'ET_AUTH_ENDPOINT';
        ex6.Value__c='https://auth.exacttargetapis.com/v1/requestToken';
        insert ex6;
        
        ExactTarget_Integration_Settings__c ex7= new ExactTarget_Integration_Settings__c();
        ex7.name= 'ET_CLIENT_SECRET';
        ex7.Value__c='vI9wczXXqf6EwrYdI83Z6lLd';
        insert ex7;
        
        ExactTarget_Integration_Settings__c ex8= new ExactTarget_Integration_Settings__c();
        ex8.name= 'SMS_Endpoint';
        ex8.Value__c='https://www.exacttargetapis.com/sms/v1/messageContact/';
        insert ex8; 
        system.assertEquals(ex8.name,'SMS_Endpoint','success');
        Test.startTest();        
        BatchReminder batch = new BatchReminder ();
        batch.execute(null,lstmem);
        Id str =Database.executeBatch(batch,1);        
        Test.stopTest();     
    }  
}