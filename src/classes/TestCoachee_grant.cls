@isTest
public class TestCoachee_grant {
    @isTest
    public static void TestCoachee(){
        profile p = [Select id from profile where Name =: 'ASPAC JPN Sales Rep'];  
        User u = new User(Alias = 'standt',Email='SM@testorg.com',EmailEncodingKey='UTF-8', LastName='Havish', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US',country='Japan', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='testmail1@testorg.com',Unique_User_Id__c = 'ABC1');
        insert u;
        RecordType rt = [Select Id from RecordType where DeveloperName = 'WORK_WITH'];
        Coaching__c testCoaching1 = New Coaching__c(Name ='Test Account',RecordTypeId =rt.Id, Start_Date__c = datetime.newInstance(2014, 9, 15, 12, 30, 0), End_Date__c = datetime.newInstance(2014, 9, 15, 13, 30, 0),  Coachee__c = u.Id);
        insert testCoaching1;
        system.assertEquals(testCoaching1.Name,'Test Account','success');
        Test.startTest();
        test.stopTest();
    }
}