@isTest
public with sharing class TestCouponContact 
{
    static testmethod void doinsertCouponContact ()
    {  
        CouponContact__c cpn = new CouponContact__c();
        cpn.Aws_ContactId__c = '123';
        cpn.Aws_AccountId__c ='456';
        cpn.Aws_TransactionId__c='12';
        cpn.Aws_CouponId__c='36';
        cpn.Aws_CouponSendId__c='75';
        cpn.CampaignKey__c=91;		
        insert cpn;

        account acc = new account();
        acc.Name='test1';
        acc.Aws_AccountId__c ='456';
        insert acc;
       
        Coupon__c cp = new Coupon__c();
        cp.DB_ID__c=36;  
        insert cp;
        
        CouponSendList__c cns = new CouponSendList__c();
        cns.Name='test4';
        cns.DB_ID__c=75;  
        insert cns;
        
        campaign cm = new campaign();
        cm.Name='test5';
        cm.DB_ID__c =91;
        insert cm;
        
        CouponContact__c cl = new CouponContact__c ();
        cl.id=cpn.id;
        cl.AccountId__c=acc .id;
        cl.CouponId__c=cp.id;
        cl.CouponSendListId__c=cns.id;
        cl.CampaignId__c=cm.id;
        update cl;
        system.assertEquals(cl.AccountId__c,acc.id,'success');
    }    
}