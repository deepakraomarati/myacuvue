@isTest
private class TestCreatedStatusChartRP1 
{
    private static void init(){        
        String Objectname='Account';
        Decimal Objectvalue=3;
        system.assertEquals(Objectname,'Account','success');
        List<CreatedStatusChartRP1.TotalObjectCounts> Totallist = new List<CreatedStatusChartRP1.TotalObjectCounts>();
        CreatedStatusChartRP1.TotalObjectCounts WrapObj = new  CreatedStatusChartRP1.TotalObjectCounts(''+Objectname,objectvalue);
        Totallist.add(WrapObj);
        
    }
    
    static testmethod void CreatedStatusChartRP1Test()
    {       
        String testassertion = 'test';
        Test.startTest();
        CreatedStatusChartRP1 Chart3Obj = new CreatedStatusChartRP1();        
        Chart3Obj.getObjectCountItems();
        system.assertEquals(testassertion,'test','success');
        Test.StopTest();
        
    }
}