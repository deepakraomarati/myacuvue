@isTest
private class TestCreatedStatusChartRP2 
{
    private static void init(){        
        String Objectname='Account';
        Decimal Objectvalue=3;
        system.assertEquals(Objectname,'Account','success');
        List<CreatedStatusChartRP2.TotalObjectCounts> Totallist = new List<CreatedStatusChartRP2.TotalObjectCounts>();
        CreatedStatusChartRP2.TotalObjectCounts WrapObj = new  CreatedStatusChartRP2.TotalObjectCounts(''+Objectname,objectvalue);
        Totallist.add(WrapObj);       
    }
    
    static testmethod void CreatedStatusChartRP2Test()
    {       
        String testassertion = 'test';
        Test.startTest();
        CreatedStatusChartRP2 Chart4Obj = new CreatedStatusChartRP2();        
        Chart4Obj.GetTotalCustomData();
        system.assertEquals(testassertion,'test','success');
        Test.StopTest(); 
    }
}