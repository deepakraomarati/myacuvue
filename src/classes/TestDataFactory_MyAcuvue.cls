/**
* File Name: TestDataFactory_MyAcuvue
* Description : class for creating test records
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |2-Sep-2016  |hsingh53@its.jnj.com  |New Class created
*/

public without sharing class TestDataFactory_MyAcuvue{
    
    /*
     * method for creating DXOrder record
     */
  /* public static List<DxOrder__c> createDXOrder(Integer count){
        final List<DxOrder__c> dxOrderList = new List<DxOrder__c>();
        for(Integer i = 0 ; i <= count ; i++){
            dxOrderList.add(new DxOrder__c(Name  = 'Test' , ACMSOrderNo__c = '12345' , delYN__c = true));
        }
        if(dxOrderList.size() > 0){
            insert dxOrderList;
        }
        return dxOrderList; 
    }*/
    
    /*
     *method for creating DxOrderProduct__c record
     */
  /* public static List<DxOrderProduct__c> createDXOrderProduct(Integer count,List<DxOrder__c> dxOrderList){
        final List<DxOrderProduct__c> dxOrderProdList = new List<DxOrderProduct__c>();
        for(Integer i = 0 ; i <= count ; i++){
            dxOrderProdList.add(new DxOrderProduct__c(Name  = 'Test' , MA2_DxOrder__c = dxOrderList[i].Id));
        }
        if(dxOrderList.size() > 0){
            insert dxOrderProdList;
        }
        return dxOrderProdList;
    } */
    
    /*
     *method for creating Account record
     */  
    public static List<Account> createAccount(Integer count){
        final List<Account> accList = new List<Account>();
        for(Integer i = 0 ; i < count ; i++){
            accList.add(new Account(Name = 'Test' , MA2_AccountId__c = '123456' ,AccountNumber = '54321',OutletNumber__c = '54321',ActiveYN__c = true,My_Acuvue__c = 'Yes'));  
            accList.add(new Account(Name = 'Test' , MA2_AccountId__c = '123456' ,AccountNumber = '54321',OutletNumber__c = '51254',ActiveYN__c = true,My_Acuvue__c = 'Yes'));
            accList.add(new Account(Name = 'Test' , MA2_AccountId__c = '123456' ,AccountNumber = '51234',OutletNumber__c = '51234',ActiveYN__c = true,My_Acuvue__c = 'Yes'));
            accList.add(new Account(Name = 'Test' , MA2_AccountId__c = '123456' ,AccountNumber = '51234',OutletNumber__c = '55555',ActiveYN__c = true,My_Acuvue__c = 'Yes'));    
        }
        if(accList.size() > 0){
            insert accList;
        }
        return accList;
    }
   
    /*
     *method for creating MA2_Business_Hours__c record
     */
   /* public static List<MA2_Business_Hours__c> createBussinessHrRecord(Integer count , List<Account> accountList){
        final List<MA2_Business_Hours__c> bussList = new List<MA2_Business_Hours__c>();
        for(Integer i = 0 ; i < count ; i++){
            bussList.add(new MA2_Business_Hours__c(MA2_WeekStartDate__c = System.Today() - 3 ,  
                                                      MA2_FriBusinesHours__c = '00:00-01:00;02:00-03:00',
                                                      MA2_MonBusinesHours__c = '00:00-01:00;03:00-04:00',
                                                      MA2_SatBusinesHours__c = '00:00-01:00;01:00-02:00',
                                                      MA2_SunBusinesHours__c = '00:00-01:00;05:00-06:00',
                                                      MA2_ThuBusinesHours__c = '00:00-01:00;01:00-02:00',
                                                      MA2_TueBusinesHours__c = '00:00-01:00;06:00-07:00',
                                                      MA2_WedBusinesHours__c = '00:00-01:00;01:00-02:00',
                                                      MA2_AccountId__c =  accountList[i].Id
                                                      ));    
        }
        if(bussList.size() > 0){
            insert bussList;
        }
        return bussList;
    }*/
    
    /*
     * method for creating etrial Record
     */
    /*public static List<Etrial__c> createEtrial(Integer count){
        final List<Etrial__c> etrialList = new List<Etrial__c>();
        for(Integer i = 0; i<count ; i++){
            etrialList.add(new Etrial__c(Name = 'test' , etrial_email__c = 'test@gmail.com' ,etrial_phone__c = '123456789' ,CurrencyIsoCode  = 'HKD', ETrialCheckDate_MyAcuvue__c = System.Today()));
        }
        if(etrialList.size() > 0){
            insert etrialList;
        }
        return etrialList;
    }*/
  
   /*
     * method for creating coupon Record
     */  
    public static List<Coupon__c> createCoupon(Integer count){
        Id etrialRecordTypeId = [select Id from RecordType where Name = 'Etrial' and sObjectType = 'Coupon__c'].Id;
        Id bonusMulRecordTypeId = [select Id from RecordType where Name = 'Bonus Multiplier with Diminishing Value' 
        and sObjectType = 'Coupon__c'].Id;
        Id bonusMulplierRecTypeId = [select Id from RecordType where Name = 'Bonus Multiplier' and sObjectType = 'Coupon__c'].Id;
        Id caseDiscRecTypeId = [select Id from RecordType where Name = 'Cash Discount' 
        and sObjectType = 'Coupon__c'].Id;
        
        final List<Coupon__c> coupon = new List<Coupon__c>();
        for(Integer i = 0; i<count ; i++){
           coupon.add(new Coupon__c(MA2_CouponName__c = 'Etrial',RecordTypeID = etrialRecordTypeId ,MA2_CountryCode__c ='CHN'
           ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Pre-Assessment'));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = 'Etrial',RecordTypeID = etrialRecordTypeId ,MA2_CountryCode__c ='HKG'
           ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Pre-Assessment'));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = '5 days Dx (Triton/Triton A/ Moist A/ Refine/ Multifocal)',RecordTypeID = etrialRecordTypeId ,MA2_CountryCode__c ='HKG'
           ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = '5 days Dx (All Products)',RecordTypeID = etrialRecordTypeId ,MA2_CountryCode__c ='HKG'
           ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = 'Moist',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c ='HKG'
           ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = 'Oasys 1 Day',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c ='HKG'
           ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = 'Moist A',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c ='HKG'
           ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = 'Multifocal',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c ='HKG'
           ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = 'Define',RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c ='HKG'
           ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = 'Etrial',RecordTypeID = etrialRecordTypeId ,MA2_CountryCode__c ='HKG'
           ,MA2_CouponValidity__c = 30, MA2_TimeToAward__c = 'Post-Assessment'));
           
           
                      
           coupon.add(new Coupon__c(MA2_CouponName__c = 'Bonus Multiplier with Diminishing Value',CountdownRate__c = '10',
           DiscountPrice__c = 50,RecordTypeID = bonusMulRecordTypeId ,MA2_CountryCode__c ='CHN',MA2_CouponValidity__c = 30));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = 'Bonus Multiplier with Diminishing Value',CountdownRate__c = '10',
           DiscountPrice__c = 40,RecordTypeID = bonusMulRecordTypeId ,MA2_CountryCode__c ='CHN',MA2_CouponValidity__c = 30));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = 'Bonus Multiplier',CountdownRate__c = '10',
           MA2_BonusMultiplier__c = 50,RecordTypeID = bonusMulplierRecTypeId ,MA2_CountryCode__c ='CHN',MA2_CouponValidity__c = 30));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = 'Bonus Multiplier',CountdownRate__c = '10',
           MA2_BonusMultiplier__c = 40,RecordTypeID = bonusMulplierRecTypeId ,MA2_CountryCode__c ='CHN',MA2_CouponValidity__c = 30));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = '1.2x Multiplier', MA2_TimeToAward__c = 'Post-Assessment',
           DiscountPrice__c = 40,RecordTypeID =bonusMulplierRecTypeId ,MA2_CountryCode__c ='SGP',MA2_CouponValidity__c = 30));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = '$30 Voucher(All Brand)',MA2_TimeToAward__c = 'Post-Assessment',
           DiscountPrice__c = 40,RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c ='SGP',MA2_CouponValidity__c = 30));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = '$30 Voucher(2 Boxes)',MA2_TimeToAward__c = 'Post-Assessment',
           DiscountPrice__c = 40,RecordTypeID = caseDiscRecTypeId ,MA2_CountryCode__c ='SGP',MA2_CouponValidity__c = 30));
           
           coupon.add(new Coupon__c(MA2_CouponName__c = '1.5x multiplier',MA2_TimeToAward__c = 'Campaign',
           DiscountPrice__c = 40,RecordTypeID = bonusMulplierRecTypeId ,MA2_CountryCode__c ='SGP',MA2_CouponValidity__c = 30));
            //coupon.add(new Coupon__c(MA2_CouponName__c = 'Etrial',RecordTypeID = etrialRecordTypeId ,MA2_CountryCode__c ='CHN',MA2_CouponValidity__c = 30));
            //coupon.add(new Coupon__c(MA2_CouponName__c = 'Etrial',RecordTypeID = 'Etrial Premium',MA2_CountryCode__c = 'SGP',MA2_CouponValidity__c = 30));
        }
        if(coupon.size() > 0){
            insert coupon;
        }
        return coupon;
    }
    
    /*
     * method for creating coupon Wallet Record
     */  
    public static List<CouponContact__c> createCouponWallet(Integer count,List<Coupon__c> coupon,List<Contact> contactList){
        final List<CouponContact__c> couponWallet = new List<CouponContact__c>();
        for(Coupon__c record : coupon){
            couponWallet.add(new CouponContact__c(ExpiryDate__c = System.Today() - 10 ,ContactId__c = contactList[0].Id ,
                 CouponId__c = record.Id , MA2_AccountId__c = '12345' , MA2_ContactId__c = '5555'));   
        }
        if(couponWallet.size() > 0){
            insert couponWallet;
        }
        return couponWallet;
    }
    
    /*
     * method for creating Contact Record
     */
    public static List<Contact> createContact(Integer count,List<Account> accList){
        Id consumerRecTypeId = [select Id from RecordType where Name = 'Consumer' and sObjectType= 'Contact'].Id;
        Id ecpRecTypeId = [select Id from RecordType where Name = 'ECP' and sObjectType= 'Contact'].Id;
        final List<Contact> contactList = new List<Contact>();
        for(Integer i = 0; i<count ; i++){
            contactList.add(new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',
            RecordTypeId  = consumerRecTypeId  ,
            AccountId = accList[i].Id, email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'CHN',
            MA2_PreAssessment__c = true));
            
            contactList.add(new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',RecordTypeId = ecpRecTypeId,
            AccountId = accList[i].Id, email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'CHN',
            MA2_PreAssessment__c = true));
            
            contactList.add(new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',RecordTypeId = consumerRecTypeId,
            AccountId = accList[i].Id, email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'HKG',MA2_Contact_lenses__c='No',
            DOB__c  = System.Today(),
            MA2_PreAssessment__c = true));
            
            contactList.add(new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',RecordTypeId = consumerRecTypeId,
            AccountId = accList[i].Id, email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'HKG',MA2_Contact_lenses__c='Acuvue Brand',
            DOB__c  = System.Today(),
            MA2_PreAssessment__c = true));
            
            contactList.add(new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',RecordTypeId = consumerRecTypeId,
            AccountId = accList[i].Id, email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'HKG',MA2_Contact_lenses__c='Other brand',
            DOB__c  = System.Today(),
            MA2_PreAssessment__c = true));  
            
            
            contactList.add(new Contact(LastName = 'test' ,RecordTypeId  = consumerRecTypeId  ,
            MA2_AccountId__c = '12123456',MA2_ContactId__c = '1123456',AccountId = accList[i].Id, 
            email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'CHN',
            MA2_PreAssessment__c = true));
            
            contactList.add(new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',
            AccountId = accList[i].Id, email = 'test@gmail.com', phone = '123456789' ,RecordTypeId = consumerRecTypeId,
            DOB__c  = System.Today(),
            MA2_Country_Code__c = 'SGP' ,MA2_Contact_lenses__c='No',
            MA2_PreAssessment__c = true));
            
            contactList.add(new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',RecordTypeId = consumerRecTypeId,
            AccountId = accList[i].Id, email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'SGP' ,MA2_Contact_lenses__c='Acuvue Brand',
            DOB__c  = System.Today(),
            MA2_PreAssessment__c = true));
            
            contactList.add(new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',RecordTypeId = consumerRecTypeId,
            AccountId = accList[i].Id, email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'SGP' ,MA2_Contact_lenses__c='Other brand',
            DOB__c  = System.Today(),
            MA2_PreAssessment__c = true));
        }
        if(contactList.size() > 0){
            insert contactList;
            System.Debug('contactList--'+contactList);
            System.Debug('contactList.size()--'+contactList.size());
        }
        return contactList;
    }   
    
    /*
     * method for creating Transaction Detail Record
     */
    public static List<TransactionTd__c> createTransaction(Integer count,List<Account> accList,List<Contact> contactList){
        final List<TransactionTd__c> transList = new List<TransactionTd__c>();
        for(Integer i = 0; i<count ; i++){
            transList.add(new TransactionTd__c( AccountID__c = accList[i].Id, MA2_Contact__c = contactList[0].Id,
            MA2_AccountId__c = '123456',MA2_ContactId__c = '23456' , MA2_Points__c = 10 ,MA2_Redeemed__c = 10,MA2_PointsExpired__c = true,
            CreatedDateTest__c = date.newinstance(2015,01,12)));
            transList.add(new TransactionTd__c( AccountID__c = accList[i].Id,MA2_ECPContact__c = contactList[1].Id,MA2_AccountId__c = '12123456',MA2_ECPId__c = '1123456' , MA2_Points__c = 10 , CreatedDateTest__c = date.newinstance(2015,01,12)));
            transList.add(new TransactionTd__c(MA2_AccountId__c = '12345',MA2_ContactId__c = '2345' , MA2_Points__c = 10 ,
            CreatedDateTest__c = date.newinstance(2015,01,12)));
            transList.add(new TransactionTd__c( AccountID__c = accList[i].Id,MA2_ECPContact__c = contactList[1].Id,MA2_AccountId__c = '12123456',MA2_ECPId__c = '1123456' , MA2_Points__c = 10 , CreatedDateTest__c = date.newinstance(2015,01,12)));
        }
        if(transList.size() > 0){
            insert transList;
        }
        return transList;
    }   
    
    public static List<TransactionTd__c> createECPTransaction(Integer count,List<Account> accList,List<Contact> contactList){
        final List<TransactionTd__c> transList = new List<TransactionTd__c>();
        for(Integer i = 0; i<count ; i++){
            transList.add(new TransactionTd__c( AccountID__c = accList[i].Id,MA2_ECPContact__c = contactList[0].Id,
            MA2_AccountId__c = '12123456',MA2_ECPId__c = '1123456' , MA2_Points__c = 10 , CreatedDateTest__c = date.newinstance(2015,01,12)));
        }
        if(transList.size() > 0){
            insert transList;
        }
        return transList;
    }
    
    
    
   /*
    * method for creating Transaction Product Record
    */
    public static List<MA2_TransactionProduct__c> createTransactionProduct(Integer count,List<Account> accList,List<Contact> contactList,List<TransactionTd__c> transList){
        final List<MA2_TransactionProduct__c> transProductList = new List<MA2_TransactionProduct__c>();
        for(Integer i = 0; i<count ; i++){
            transProductList.add(new MA2_TransactionProduct__c( MA2_Transaction__c = transList[i].Id,MA2_Account__c = accList[i].Id, MA2_ECPContact__c = contactList[1].Id,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456'));
            transProductList.add(new MA2_TransactionProduct__c( MA2_Transaction__c = transList[i].Id,MA2_Account__c = accList[i].Id, MA2_Contact__c = contactList[0].Id, MA2_AccountId__c = '12123456',MA2_ECPId__c = '1123456'));
        }
        if(transProductList.size() > 0){
            insert transProductList;
        }
        return transProductList;
    }   
 
   /*
    * method for creating Text Content Record
    */   
    public static List<MA2_TextContent__c> createTextContent(Integer count){
        List<MA2_TextContent__c> textContentList = new List<MA2_TextContent__c>();
        for(Integer i =0 ; i < count ; i++){
            textContentList.add(new MA2_TextContent__c(MA2_ContentType__c = 'Welcome Message' , MA2_CountryCode__c = 'CHN', 
                                                       MA2_DeviceType__c = 'IOS' , MA2_Version__c = '3.0',
                                                       MA2_Language__c = 'Traditional Chinese'));
            textContentList.add(new MA2_TextContent__c(MA2_ContentType__c = 'Welcome Message' , MA2_CountryCode__c = 'CHN', 
                                                       MA2_DeviceType__c = null , MA2_Version__c = null));
        }
        if(textContentList.size() > 0){
            insert textContentList;
        }
        return textContentList;
    }
    
   /*
    * method for creating CountryWise Configuration Record
    */   
    public static List<MA2_CountryWiseConfiguration__c> createCountryConfig(Integer count){
        List<MA2_CountryWiseConfiguration__c> countryConfigList = new List<MA2_CountryWiseConfiguration__c>();
        for(Integer i =0 ; i < count ; i++){
            countryConfigList.add(new MA2_CountryWiseConfiguration__c(Name = 'test' , MA2_ConfigurationType__c = '  Minimum ECP Months',
                                                                        MA2_CountryCode__c = 'CHN'));
        }
        if(countryConfigList.size() > 0){
            insert countryConfigList;
        }
        return countryConfigList;
    }
    
    /*
    * method for creating New Fit Appointment Record
    */
    
    public static List<AccountBooking__c> createFitAppointment(Integer count){
        List<AccountBooking__c> appointmentList = new List<AccountBooking__c>();
        for(Integer i =0 ; i < count ; i++){
            appointmentList.add(new AccountBooking__c(Name = 'test' , MA2_ApprovalStatus__c = 'Pending',MA2_BookingStatus__c='Planned',
                                                                        MA2_CountryCode__c = 'CHN'));
        }
        if(appointmentList.size() > 0){
            insert appointmentList ;
        }
        return appointmentList;
    }
    
   /*
    * method for creating MA2_CouponImage__c Record
    */   
    public static List<MA2_CouponImage__c> createCouponImage(Integer count,List<Coupon__c> couponList){
        List<MA2_CouponImage__c> couponImageList = new List<MA2_CouponImage__c>();
        for(Integer i =0 ; i < count ; i++){
            couponImageList.add(new MA2_CouponImage__c(MA2_Coupon__c = couponList[i].Id, MA2_Coupon_Type__c = ' Coupon Banner'));
        }
        if(couponImageList.size() > 0){
            insert couponImageList;
        }
        return couponImageList;
    }
    
    public static void insertCustomSetting(){
        Credientials__c cred = new Credientials__c(Name = 'CouponSendToApigee' , 
                    Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                    Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                    Target_Url__c = 'https://jnj-dev.apigee.net/v1/consumer/catalog/addcoupon');
       Credientials__c cred1 = new Credientials__c(Name = 'SendCouponWallet' , 
                    Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                    Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                    Target_Url__c = 'https://jnj-dev.apigee.net/v1/sfdc/couponetrial');
       Credientials__c cred2 = new Credientials__c(Name = 'CountryWiseConfiguration' , 
                    Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                    Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                    Target_Url__c = 'https://jnj-dev.apigee.net/v1/sfdc/configuration/add');
       Credientials__c cred3 = new Credientials__c(Name = 'SendContact' , 
                    Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                    Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                    Target_Url__c = 'https://jnj-dev.apigee.net/v1/sfdc/profile/contact');
        Credientials__c cred4 = new Credientials__c(Name = 'TextContentService' , 
                    Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                    Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                    Target_Url__c = 'https://jnj-dev.apigee.net/v1/sfdc/consumer/updatelocale');
        Credientials__c cred5 = new Credientials__c(Name = 'NewAppointment' , 
                    Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                    Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                    Target_Url__c = 'https://jnj-dev.apigee.net/v1/sfdc/consumer/updatelocale');
        Credientials__c cred6 = new Credientials__c(Name = 'SFDCRoleMaster' , 
                    Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                    Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                    Target_Url__c = 'https://jnj-dev.apigee.net/v1/sfdc/consumer/updatelocale');
        Credientials__c cred7 = new Credientials__c(Name = 'ProfileAccess' , 
                    Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                    Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                    Target_Url__c = 'https://jnj-dev.apigee.net/v1/sfdc/consumer/updatelocale');
        Credientials__c cred8 = new Credientials__c(Name = 'SFDCUpdateRole' , 
                    Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                    Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                    Target_Url__c = 'https://jnj-dev.apigee.net/v1/sfdc/consumer/updatelocale');
        Credientials__c cred9 = new Credientials__c(Name = 'SendTransaction' , 
                    Client_Secret__c = 'b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY' , Client_Id__c = 'b3U65J1vylrxEeavNcUjU0xIEg',
                    Api_Key__c = 'VREMbvPnsSE4s3ikefdoUnWgwIeJjwiA' , 
                    Target_Url__c = 'https://jnj-dev.apigee.net/v1/sfdc/consumer/updatelocale');
        insert cred;
        insert cred1;
        insert cred2;
        insert cred3;
        insert cred4;
        insert cred5;
        insert cred6;
        insert cred7;
        insert cred8;
        insert cred9;
    }
    
  
    public static void insertAttachment(List<MA2_CouponImage__c>  couponImgList){
        List<Attachment> insertAttachList = new List<Attachment>();
        for(MA2_CouponImage__c record : couponImgList){
            Attachment attach = new Attachment();     
            attach.Name = 'Unit Test Attachment';
            Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
            attach.body = bodyBlob;
            attach.parentId =record.id; 
            insertAttachList.add(attach);   
        }
        
        insert insertAttachList;
    }
    
    public static List<MA2_UserProfile__c> insertUserProfile(Integer count){
        List<MA2_UserProfile__c> userProfList = new List<MA2_UserProfile__c>();
        for(Integer i = 0 ; i<count ; i++){
            userProfList.add(new MA2_UserProfile__c(MA2_CountryCode__c = 'SGP',MA2_ProfileName__c = 'HQ'));
        }
        insert userProfList;
        return userProfList;
    }
    
    public static List<Contact> createECPContact(Integer count,List<Account> accList){
        Id ecpRecTypeId = [select Id from RecordType where Name = 'ECP' and sObjectType= 'Contact'].Id;
        final List<Contact> contactList = new List<Contact>();
        for(Integer i = 0; i<count ; i++){
            contactList.add(new Contact(LastName = 'test' ,MA2_AccountId__c = '123456',MA2_ContactId__c = '23456',
            AccountId = accList[i].Id, email = 'test@gmail.com', phone = '123456789' ,MA2_Country_Code__c = 'CHN',
            RecordTypeId = ecpRecTypeId ,
            MA2_PreAssessment__c = true));
        }
       insert contactList;
       return contactList;
    }    
    
    public static List<MA2_RelatedAccounts__c> insertRelatedAccount(Integer count,List<Account> accList,List<Contact> conList,List<MA2_UserProfile__c> userProfileList){
        List<MA2_RelatedAccounts__c> relatedAccList = new List<MA2_RelatedAccounts__c>();
        for(Integer i = 0 ; i<count ; i++){
            relatedAccList.add(new MA2_RelatedAccounts__c(MA2_Account__c = accList[i].Id,MA2_Contact__c = conList[i].Id, 
                            MA2_UserProfile__c = userProfileList[i].Id));
        }
        insert relatedAccList; 
        return relatedAccList;
    }
    
}