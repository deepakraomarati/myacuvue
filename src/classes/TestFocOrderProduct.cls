@isTest
public with sharing class TestFocOrderProduct 
{
    static testmethod void doinsertgetFocOrderProduct ()
    {          
        FocOrderProduct__c FOProd = new FocOrderProduct__c();
        FOProd.Name='TestFocOrderProduct';
        FOProd.UPC__c='123';
        insert FOProd;
        
        product2 Prod = new product2();
        Prod.Name='Test';
        Prod.UPC_Code__c='123';
        prod.InternalName__c='Abc';
        insert Prod;
        
        FocOrderProduct__c FOProdUpdate = new FocOrderProduct__c();
        FOProdUpdate.Id=FOProd.Id;
        FOProdUpdate.UPC__c=FOProd.UPC__c;
        FOProdUpdate.ProductId__c=Prod.Id;
        update FOProdUpdate;
        system.assertEquals(FOProdUpdate.Id,FOProd.Id,'success');
    }    
}