@isTest global with sharing class TestGenerate_Contract_Controller
{
    static RecordType objRecType;
    static PageReference pageRef;
    static Generate_Contract_Controller ext;
    
    public static void callContract(RecordType rec,PageReference page)
    {
        Account acc = new Account(OutletNumber__c='cd1235',Name='TestAccount1',ShippingStreet='test street',ShippingPostalCode='5855',ShippingCountry='Korea');
        insert acc;
        string idval = acc.id;
        
        Contract cont = new Contract(AccountId=acc.Id,Email_Sent__c = false, Send_Email__c = 'rkatakwa@its.jnj.com',RecordTypeId = rec.Id, BC_Card_Merchant_Number__c='123', Business_Registration_Number__c ='123244');
        insert cont;
        string ctrid = cont.Id;
        string emailid = cont.Send_Email__c;
        
        Contact con = new Contact(AccountId=acc.id, lastName = 'test',email = 'rkatakwa@its.jnj.com');
        insert con;
        system.assertEquals(con.lastName,'test','success'); 
        
        Attachment attach1 = new Attachment(ParentId = cont.Id, Name = 'Signature Contract 1', body = Blob.ValueOf('UNIT.TEST'));
        insert attach1;
        
        Attachment attach2 = new Attachment(ParentId = cont.Id, Name = 'Signature Contract 2', body = Blob.ValueOf('UNIT.TEST'));
        insert attach2;
        
        Attachment attach3 = new Attachment(ParentId = cont.Id, Name = 'Signature Contract 3', body = Blob.ValueOf('UNIT.TEST'));
        insert attach3;
        
        Attachment attach4 = new Attachment(ParentId = cont.Id, Name = 'Signature Contract 4', body = Blob.ValueOf('UNIT.TEST'));
        insert attach4;
        
        pageRef = page;
        pageRef.getParameters().put('id',cont.id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.CurrentPage().getparameters().put('contractid', cont.id);
        ApexPages.StandardController sc = new ApexPages.standardController(acc);
        ext = new Generate_Contract_Controller(sc);
        ext.getContract();
        pageRef = ext.savePDF();
        pageRef = ext.save();
        
        cont.Email_Sent__c = Generate_Contract_Controller.sendEmail(cont.Id, emailid, attach1);
        cont.Email_Sent__c = Generate_Contract_Controller.sendEmail(cont.Id, emailid, attach2);
        cont.Email_Sent__c = Generate_Contract_Controller.sendEmail(cont.Id, emailid, attach3);
        cont.Email_Sent__c = Generate_Contract_Controller.sendEmail(cont.Id, emailid, attach4);
    }
    
    public static testMethod void generatecontract1()
    {        
        objRecType = [select Id,Name from RecordType where Name = '아큐브 멀티포컬 취급 약정서' limit 1];
        callContract(objRecType,Page.Generate_Contract_1);
        system.assertEquals(objRecType.Name,'아큐브 멀티포컬 취급 약정서','success');
    }
    public static testMethod void generatecontract2()
    {        
        objRecType = [select Id,Name from RecordType where Name = '마이아큐브 프로그램 제공 및 사용 약정서' limit 1];
        callContract(objRecType,Page.Generate_Contract_2);
        system.assertEquals(objRecType.Name,'마이아큐브 프로그램 제공 및 사용 약정서','success');
    }
    
    public static testMethod void generatecontract3()
    {        
        objRecType = [select Id,Name from RecordType where Name = '아큐브 매출 및 지원 약정서' limit 1];
        callContract(objRecType,Page.Generate_Contract_3);
        system.assertEquals(objRecType.Name,'아큐브 매출 및 지원 약정서','success');
    }
    
    public static testMethod void generatecontract4()
    {      
        objRecType = [select Id,Name from RecordType where Name = '공급계약서' limit 1];
        callContract(objRecType,Page.Generate_Contract_4);
        system.assertEquals(objRecType.Name,'공급계약서','success');
    }    
}