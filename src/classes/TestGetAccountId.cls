@isTest
public with sharing class TestGetAccountId
{
    static testmethod void doinsertaccount()
    {      
        Account acc = new Account();
        acc.Name='Test';
        acc.Aws_AccountId__c='123';
        insert acc ;
        system.assertEquals(acc.Name,'Test','success'); 
        
        Contact con = new Contact();
        con.lastName='Test1';
        con.Aws_ContactId__c='57';
        con.Aws_AccountId__c='123';   
        insert con;
        
        Consumer_Relation__c crobj=new Consumer_Relation__c();            
        crobj.AccountId__c = acc.Id ;           
        crobj.ContactID__c = con.Id;            
        crobj.DB_ID__c =Integer.Valueof(con.Aws_ContactId__c);                        
        insert crobj ;
        
        contact conupdate = new contact();
        conupdate.Id= con.Id;
        conupdate.lastName=con.lastName;
        conupdate.Aws_ContactId__c=con.Aws_ContactId__c;
        conupdate.Aws_AccountId__c=con.Aws_AccountId__c;   
        update conupdate;   
        system.assertEquals(conupdate.lastName,con.lastName,'success');
    }   
}