@isTest
public with sharing class TestGetAccountIdForMigration
{
    static testmethod void doinsertProductLotNo()
    {
        TriggerHandler__c mcs = new TriggerHandler__c(Name = 'HandleTriggers', MA2_createUpdateTransaction__c = true);
        insert mcs;
        ProductLotNo__c pln = new ProductLotNo__c();
        pln.Aws_ProductId__c = '123';
        pln.Aws_TransacId__c = '54'; 
        pln.Aws_BatchNumberId__c = '21';
        insert pln;
        
        product2 pdl = new product2();
        pdl.Name='Test';
        pdl.InternalName__c='Abc';
        insert pdl;
        
        TransactionTd__c trn = new TransactionTd__c();
        trn.TransactionId__c='54';
        insert trn;
        
        Batch_Details__c bcn = new Batch_Details__c();
        bcn.Batch_Number__c='21';
        insert bcn;
        
        ProductLotNo__c  pl = new ProductLotNo__c();
        pl.id=pln.id;
        pl.ProductId__c=pdl.id;
        pl.TransactionId__c=trn.id;
        pl.BatchNumber__c=bcn.id;
        update pl;
        system.assertEquals(pl.ProductId__c,pdl.id,'success');
    }  
}