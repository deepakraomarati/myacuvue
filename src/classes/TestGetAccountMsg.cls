@isTest
public with sharing class TestGetAccountMsg
{
    static testmethod void doinsertGetAccountMsg ()
    {
        list<Account_Msg__c> lstaccmsg = new list<Account_Msg__c>();
        account ac = new account();
        ac.name='test';
        ac.Aws_AccountId__c ='123';
        insert ac;
        
        Account_Msg__c accmsg = new Account_Msg__c();
        accmsg.Aws_AccountId__c='123';
        lstaccmsg.add(accmsg);
        
        Account_Msg__c accmsg1 = new Account_Msg__c();
        accmsg1.Aws_AccountId__c='12345';
        lstaccmsg.add(accmsg1);
        insert lstaccmsg;
        
        Account_Msg__c accmsg2 = new Account_Msg__c();
        accmsg2.Aws_AccountId__c='00000';
        insert accmsg2;
        system.assertEquals(accmsg1.Aws_AccountId__c,'12345','success');
    }  
}