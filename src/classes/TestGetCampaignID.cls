@isTest
public with sharing class TestGetCampaignID 
{
    static testmethod void doinsertcampaign()
    {
        Account acc = new Account(Name = 'Test' , AccountNumber = '54321',
                                  Marketing_Program__c = 'AEC',OutletNumber__c = '54321',ActiveYN__c = true,
                                  My_Acuvue__c = 'Yes',CountryCode__c = 'SGP',PublicZone__c = 'Testtt');
        
        insert acc;
        
        contact con = new contact();
        con.LastName='test';
        con.Aws_ContactId__c ='123';
        con.AccountId = acc.id;
        con.MA2_Country_Code__c = 'SGP';
        con.MembershipNo__c = 'SGP-10392';
        con.Aws_ContactId__c = null;
        insert con;
        
        campaign cam = new campaign();
        cam.Name='test1';
        cam.DB_ID__c =258;
        insert cam;
        system.assertEquals(cam.Name,'test1','success');
        

}
}