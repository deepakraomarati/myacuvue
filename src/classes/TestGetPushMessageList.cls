@isTest
public with sharing class TestGetPushMessageList
{
    static testmethod void doinsertGetPushMessageList ()
    {
        List<PushMessageList__c> lstPush =new List<PushMessageList__c>();
        
        PushMessageList__c pshmsg = new PushMessageList__c ();
        pshmsg.Aws_ContactId__c='123';
        lstPush.add(pshmsg);
        
        PushMessageList__c pshmsg2 = new PushMessageList__c ();
        pshmsg2.Aws_ContactId__c='12345';
        lstPush.add(pshmsg2);
        insert lstPush;
        
        PushMessageList__c pshmsg3 = new PushMessageList__c ();
        pshmsg3 .Aws_ContactId__c='0000';
        insert pshmsg3 ;
        system.assertEquals(pshmsg2.Aws_ContactId__c,'12345','success');
    }
}