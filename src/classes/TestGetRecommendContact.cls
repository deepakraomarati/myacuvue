@isTest
public with sharing class TestGetRecommendContact
{
    static testmethod void doinsertGetRecommendContact ()
    {
        list<RecommendContact__c> lstrecmnd = new list<RecommendContact__c>();
        
        RecommendContact__c reccmnd = new RecommendContact__c();
        reccmnd.Aws_ContactId__c='123';
        lstrecmnd.add(reccmnd);
        
        RecommendContact__c reccmnd1 = new RecommendContact__c();
        reccmnd1.Aws_ContactId__c='12345';
        lstrecmnd.add(reccmnd1);
        insert lstrecmnd;
        
        RecommendContact__c reccmnd2 = new RecommendContact__c();
        reccmnd2.Aws_ContactId__c='00000';
        insert reccmnd2;
        system.assertEquals(reccmnd1.Aws_ContactId__c,'12345','success');
    }
}