@isTest
public with sharing class TestGetRecommendReceive
{
    static testmethod void doinsertGetRecommandReceive ()
    {
        list<RecommendReceive__c> lstrecmndrcv = new list<RecommendReceive__c>();
        
        RecommendReceive__c reccmnd = new RecommendReceive__c();
        reccmnd.Aws_ContactId__c = '123';
        lstrecmndrcv.add(reccmnd);
        
        RecommendReceive__c reccmnd1 = new RecommendReceive__c();
        reccmnd1.Aws_ContactId__c = '12345';
        lstrecmndrcv.add(reccmnd1);
        insert lstrecmndrcv;
        
        RecommendReceive__c reccmnd2 = new RecommendReceive__c();
        reccmnd2.Aws_ContactId__c = '00000';
        insert reccmnd2;
        system.assertEquals(reccmnd1.Aws_ContactId__c,'12345','success');
    }
}