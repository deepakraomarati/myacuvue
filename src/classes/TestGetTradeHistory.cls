@isTest
public with sharing class TestGetTradeHistory
{
    static testmethod void doinsertGetTradeHistory ()
    {
        list<TradeHistory__c> lsttrdhsty = new list<TradeHistory__c>();
        TriggerHandler__c mcs = new TriggerHandler__c(Name='HandleTriggers',MA2_createUpdateTransaction__c = true);
        insert mcs;
        account ac = new account();
        ac.name='test';
        ac.OutletNumber__c ='123';
        insert ac;
        
        TradeHistory__c trhsty = new TradeHistory__c();
        trhsty.Aws_OutletNumber__c='123';
        trhsty.Aws_ContactId__c='456';
        lsttrdhsty.add(trhsty);
        
        TradeHistory__c trhsty1 = new TradeHistory__c();
        trhsty1.Aws_OutletNumber__c='12345';
        trhsty1.Aws_ContactId__c='45678';
        lsttrdhsty.add(trhsty1);
        insert lsttrdhsty;
        
        TradeHistory__c trhsty2 = new TradeHistory__c();
        trhsty2.Aws_OutletNumber__c='00000';
        trhsty2.Aws_ContactId__c='00000';
        insert trhsty2;
        system.assertEquals(trhsty1.Aws_OutletNumber__c,'12345','success');
    }    
}