@isTest
public with sharing class TestGetTradeHistoryDetails
{
    static testmethod void doinsertGetTradeHistoryDetails ()
    {
        list<TradeHistoryDetails__c> lsttrhstydetls = new list<TradeHistoryDetails__c>();
        
        account ac = new account();
        ac.name='test';
        ac.OutletNumber__c ='123';
        insert ac;
        
        TradeHistoryDetails__c trhydtls = new TradeHistoryDetails__c();
        trhydtls.Aws_OutletNumber__c='123';
        trhydtls.Aws_ContactId__c='456';
        lsttrhstydetls.add(trhydtls);
        
        TradeHistoryDetails__c trhydtls1 = new TradeHistoryDetails__c();
        trhydtls1.Aws_OutletNumber__c='12345';
        trhydtls1.Aws_ContactId__c='45678';
        lsttrhstydetls.add(trhydtls1);
        insert lsttrhstydetls;
        
        TradeHistoryDetails__c trhydtls2 = new TradeHistoryDetails__c();
        trhydtls2.Aws_OutletNumber__c='00000';
        trhydtls2.Aws_ContactId__c='00000';
        insert trhydtls2;
        system.assertEquals(trhydtls1.Aws_OutletNumber__c,'12345','success');
    }
}