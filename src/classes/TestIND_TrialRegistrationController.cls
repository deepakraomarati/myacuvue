@isTest
private class TestIND_TrialRegistrationController {
    static Account acc;
    private static testMethod void RedeemVoucher()
    {
        acc=new Account(Name='SGCampaign ECP',OutletNumber__c='ECP12345',Phone='123456789');
        Insert acc;
        
        Test.startTest();
        IND_TrialRegistrationController Trial1= new IND_TrialRegistrationController();
        Trial1.SAPID ='ECP1234';
        Trial1.phone ='123456';
        Trial1.Login();
        Trial1.LoggedInEcpId=acc.Id;
        Trial1.initLoad();
        
        IND_TrialRegistrationController Trial2= new IND_TrialRegistrationController();
        Trial2.SAPID ='ECP123';
        Trial2.phone ='123456';
        Trial2.Login();
        
        IND_TrialRegistrationController Trial3= new IND_TrialRegistrationController();
        Trial3.SAPID ='ECP1234';
        Trial3.phone ='';
        Trial3.Login();
        
        IND_TrialRegistrationController voucherRed2= new IND_TrialRegistrationController();
        voucherRed2.SAPID ='ECP12345';
        voucherRed2.phone ='123456789';
        voucherRed2.Login();
        voucherRed2.LoggedInEcpId=acc.Id;
        voucherRed2.initLoad();
        voucherRed2.Logout();
        
        IND_TrialRegistrationController voucherRedeem0 = new IND_TrialRegistrationController();
        voucherRedeem0.SAPID ='ECP12345';
        voucherRedeem0.phone ='123456789';
        voucherRedeem0.Login();
        voucherRedeem0.LoggedInEcpId=acc.Id;
        voucherRedeem0.initLoad();
        voucherRedeem0.mnthSelected ='Jan-17';
        voucherRedeem0.custmerType ='New Wearer';
        voucherRedeem0.trialBrnd ='';
        voucherRedeem0.RedeemVoucher();
        
        IND_TrialRegistrationController voucherRedeem00 = new IND_TrialRegistrationController();
        voucherRedeem00.SAPID ='ECP12345';
        voucherRedeem00.phone ='123456789';
        voucherRedeem00.Login();
        voucherRedeem00.LoggedInEcpId=acc.Id;
        voucherRedeem00.initLoad();
        voucherRedeem00.mnthSelected ='an-17';
        voucherRedeem00.custmerType ='New Wearer';
        voucherRedeem00.trialBrnd ='';
        voucherRedeem00.RedeemVoucher();
        
        IND_TrialRegistrationController voucherRedeem01 = new IND_TrialRegistrationController();
        voucherRedeem01.SAPID ='ECP12345';
        voucherRedeem01.phone ='123456789';
        voucherRedeem01.Login();
        voucherRedeem01.LoggedInEcpId=acc.Id;
        voucherRedeem01.initLoad();
        voucherRedeem01.mnthSelected ='Jan-17';
        voucherRedeem01.custmerType ='New Wearer';
        voucherRedeem01.trialBrnd ='Acuvue Oasys';
        voucherRedeem01.PrdSold ='Acuvue Oasys';
        voucherRedeem01.BoxCount = '120';
        voucherRedeem01.RedeemVoucher();
        
        IND_TrialRegistrationController voucherRedeem02 = new IND_TrialRegistrationController();
        voucherRedeem02.SAPID ='ECP12345';
        voucherRedeem02.phone ='123456789';
        voucherRedeem02.Login();
        voucherRedeem02.LoggedInEcpId=acc.Id;
        voucherRedeem02.initLoad();
        voucherRedeem02.mnthSelected ='Jan-17';
        voucherRedeem02.custmerType ='New Wearer';
        voucherRedeem02.trialBrnd ='Acuvue Oasys';
        voucherRedeem02.PrdSold ='Acuvue Oasys';
        voucherRedeem02.BoxCount = '20';
        system.assertEquals(voucherRedeem02.trialBrnd,'Acuvue Oasys','success');
        voucherRedeem02.RedeemVoucher();
        voucherRedeem02.Refresh();
        Test.stopTest();
    }
}