@isTest
Public class TestMemberQuickList{
    static Campaign cam;
    static Contact c;
    static CampaignMember cmember;
    static PageReference pageRef;
    static MemberQuickList memlist;
    static Integer OffsetValue;
    static Integer queryLimit;
    static boolean firstLoad;
    
    private static void init()
    {
        
        cam=new Campaign(Name='Test Gold Campaign',IsActive=true);
        Insert cam;
        
        c = new Contact(FirstName='Test',LastName='Gold');
        Insert c;
        system.assertEquals(cam.Name,'Test Gold Campaign','success');
        
        cmember=new CampaignMember(CampaignId=cam.Id,ContactId=c.Id,Status='Subscribed',Receipt_number__c='Test1234');
        Insert cmember;
        
        OffsetValue = 0;
        queryLimit =100;
        firstLoad = true;
        pageRef = page.MemberQuickList; 
        pageRef.getParameters().put('id',cam.Id);
        Test.setCurrentPage(pageRef);
        List<MemberQuickList.Memberwrapper> mmlist=new List<MemberQuickList.Memberwrapper>();
        ApexPages.StandardController con = new ApexPages.StandardController(cam);
        memlist=new MemberQuickList(con);
        MemberQuickList.Memberwrapper memwrap=new MemberQuickList.Memberwrapper(cmember);
        memwrap.selected =true;
        mmlist.add(memwrap);
    }
    
    static testMethod void testCampaignMemberList() {
        init();
        Test.startTest();
        String  dt='2015/06/18';
        system.assertEquals(dt,'2015/06/18','success');
        List<CampaignMember> cmlist=new List<CampaignMember>();
        cmlist.add(cmember);
        memlist.getCampaignMemberList();
        memlist.Load();
        memlist.GO();
        memlist.EmptyList();
        memlist.formateInputDate(dt);
        memlist.getSelected();
        memlist.Approve();
        memlist.Reject();
        memlist.UpdateCampaignMember(cmlist);
        memlist.previous();
        memlist.next();
        memlist.getPreviousDisable();
        memlist.getNextDisable();
        memlist.toggleSort();
        memlist.getSortDirection();
        memlist.getColumn();
        memlist.setColumn('CreatedDate');
        memlist.CollapseExpand();
        memlist.ClearFilter();
        Test.stopTest();
    }
    
    static testMethod void testLoad() {
        init();
        Test.startTest();
        memlist.SelectedValue='Today';
        memlist.Load();
        memlist.SelectedField1='acu_LastModifiedDate__c';
        memlist.SelectedOperation1 ='=';
        memlist.filterValue1 ='2015/06/18';
        system.assertEquals(memlist.SelectedValue,'Today','success');
        memlist.GO();
        memlist.SelectedValue='ThisWeek';
        memlist.Load();
        Test.stopTest();
    }
    
    static testMethod void testGo() {
        init();
        Test.startTest();
        memlist.SelectedValue='Yesterday';
        memlist.Load();
        memlist.SelectedField2='acu_CreatedDate__c';
        memlist.SelectedOperation2 ='>';
        memlist.filterValue2 ='2015/06/18';
        system.assertEquals(memlist.SelectedField2,'acu_CreatedDate__c','success');
        memlist.GO();
        memlist.SelectedValue='LastWeek';
        memlist.Load();
        Test.stopTest();
    }
    
    static testMethod void testGoLoad() {
        init();
        Test.startTest();
        memlist.SelectedValue='Last3days';
        memlist.Load();
        memlist.SelectedField1='acu_LastModifiedDate__c';
        memlist.SelectedOperation1 ='=';
        memlist.filterValue1 ='2015/06/18';
        memlist.SelectedField2='acu_CreatedDate__c';
        memlist.SelectedOperation2 ='>';
        memlist.filterValue2 ='2015/06/18';        
        memlist.GO();
        system.assertEquals(memlist.SelectedField2,'acu_CreatedDate__c','success');
        Test.stopTest();
    }
    
    static testMethod void testSelectedMembers() {
        init();
        Test.startTest();
        memlist.SelectedMemberId='1234567';
        memlist.getSelected();    
        memlist.SelectedField1='acu_LastModifiedDate__c';
        memlist.SelectedOperation1 ='=';
        memlist.filterValue1 ='abcdefg';
        memlist.SelectedField2='acu_CreatedDate__c';
        memlist.SelectedOperation2 ='=';
        memlist.filterValue2 ='xyz';        
        memlist.GO();
        system.assertEquals(memlist.SelectedField2,'acu_CreatedDate__c','success');
        Test.stopTest();
    }    
}