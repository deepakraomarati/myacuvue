@isTest(seealldata = true)
public class TestMyAccountProfile{
    static testMethod void updateProfile(){
        Etrial__c et = new Etrial__c();
        et.Name = 'TestTrial';
        et.etrial_email__c = 'test@trail.com';
        et.Country__c = 'AU';
        et.etrial_gender__c = 'Male';
        et.etrial_first_name__c = 'Ftest';
        et.etrial_last_name__c  = 'Ltest';
        et.etrial_phone__c = '1234567890';
        et.acu_address__c = 'testaddr';   
        et.acu_birthdate__c = Datetime.now().date();
        et.acu_enewsletter__c = true;
        et.acu_question__c = 'Qtest';
        insert et;
        
        Etrial__c et1 = new Etrial__c();
        et1.Name = 'TestTrial1';
        et1.etrial_email__c = 'test@trail.com';
        et1.Country__c = 'IN';
        et1.etrial_gender__c = 'Male';
        et1.etrial_first_name__c = 'Ftest1';
        et1.etrial_last_name__c  = 'Ltest1';
        et1.etrial_phone__c = '1234567890';
        et1.acu_address__c = 'testaddr1';   
        et1.acu_birthdate__c = Datetime.now().date();
        et1.acu_enewsletter__c = true;
        et1.acu_question__c = 'Qtest1';
        insert et1;
        system.assertEquals(et.etrial_email__c,et1.etrial_email__c,'success');
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        String jsonreq = '{"EtrialPhone":"1234567890","EtrialLastName":"User","EtrialGender":"Male","EtrialFirstName":"Test","Email":"test@trail.com","Country":"test","AcuQuestion":"test","AcuEnewsletter":false,"AcuBirthDate":"24/09/1989","AcuAddress":"test"}';
        req.requestURI = 'https://cs5.salesforce.com/services/apexrest/apex/Profile/'; 
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(jsonreq);
        RestContext.request = req;
        RestContext.response = res;    
        MyAccountProfile.doPost();
    }
}