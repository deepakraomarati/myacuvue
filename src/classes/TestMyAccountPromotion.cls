@isTest
private class TestMyAccountPromotion {

 static testmethod void dopost(){
    
    id rt =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
    
    ExactTarget_Integration_Settings__c EI = new ExactTarget_Integration_Settings__c();
    EI.Name = 'RecordType';
    EI.Value__c = rt;
    insert EI;
    
    Account a   = New Account();
    a.Name      ='TestGold';
    insert a;
    
    Lead ld     = new Lead ();       
    ld.Email    = 'Mouli@Lead1.com';
    ld.Company  = 'BICS';
    ld.Lastname = 'Test';  
    ld.status   = 'open';
    ld.Phone    = '+91 123456789';
    ld.IsConverted = false;
    insert ld;
    
    Contact c         = new contact();
    c.Lastname        = 'Test+Non_SG';
    c.Email           = 'mouli@Con.com';
    c.Phone           = '+91 987654321';
    c.RecordTypeId    = rt;
    c.NRIC__c         = 'NRIC_12';
    insert c;
    
    Contact c2         = new contact();
    c2.Lastname        = 'Test+2';
    c2.Email           = 'mouli+2@Con.com';
    c2.Phone           = '+91 987654321';
    c2.RecordTypeId    = rt;
    c2.NRIC__c         = 'NRIC_12+2';
    insert c2;
    
    Contact c3         = new contact();
    c3.Lastname        = 'Test+3';
    c3.Email           = 'mouli+3@Con.com';
    c3.Phone           = '+91 987654321';
    c3.RecordTypeId    = rt;
    c3.NRIC__c         = 'NRIC_12+3';
    insert c3;
    
    Campaign ca = new Campaign();
    ca.Name     = 'Non_SG_Campaign';
    ca.Type     = 'Basic+ECP';
    ca.IsActive =true;
    ca.Country__c ='AUSTRALIA';
    ca.SG_Campaign__c=False;
    insert ca;
    
    Campaign ca2 = new Campaign();
    ca2.Name     = 'SG_20$';
    ca2.Type     = 'Basic+ECP';
    ca2.IsActive =true;
    ca2.Country__c ='AUSTRALIA';
    ca2.SG_Campaign__c=True;
    insert ca2;
    
    Campaign ca3 = new Campaign();
    ca3.Name     = 'SG_30$';
    ca3.Type     = 'Basic+ECP';
    ca3.IsActive =true;
    ca3.Country__c ='AUSTRALIA';
    ca3.SG_Campaign__c=True;
    insert ca3;
    
    /*CampaignMember cmem2 = new CampaignMember();
    cmem2.CampaignId     = ca2.id;
    cmem2.ContactId      = c2.id;
    cmem2.Voucher_status__c ='Not Used';
    insert cmem2;*/
    
    string Expdate='2016-01-19T00:00:00Z';
    
    /*CampaignMember cmem3 = new CampaignMember();
    cmem3.CampaignId     = ca2.id;
    cmem3.ContactId      = c3.id;
    cmem3.Voucher_status__c ='Redeemed';
    cmem3.Expiry_Date__c = Date.valueOf(Expdate);
    insert cmem3;
    
    CampaignMember cmem4 = new CampaignMember();
    cmem4.CampaignId     = ca3.id;
    cmem4.ContactId      = c.id;
    cmem4.Voucher_status__c ='Not Used';
    cmem4.Expiry_Date__c = Date.valueOf(Expdate);
    insert cmem4;
    
    CampaignMember cmem5 = new CampaignMember();
    cmem5.CampaignId     = ca3.id;
    cmem5.ContactId      = c2.id;
    cmem5.Voucher_status__c ='Redeemed';
    cmem5.Expiry_Date__c = Date.valueOf(Expdate);
    insert cmem5;*/
    
    
    ExactTarget_Integration_Settings__c SG = new ExactTarget_Integration_Settings__c();
    SG.Name = 'SGCampaign';
    SG.Value__c = ca2.id;
    insert SG;
    
    ExactTarget_Integration_Settings__c SG2 = new ExactTarget_Integration_Settings__c();
    SG2.Name = 'SGCampaignII';
    SG2.Value__c = ca3.id;
    insert SG2;
    
    
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();
    
    test.startTest();
    
    //PROMOTIONID is Empty
    String json1 ='{"PromotionId":"","Surname":"Test+surnames","Email": "Recordtype789@Gold.com","purchasestore":"001O000000jkqvW","Receiptnumber":"AcuveGold_21","ContactEmail":"true","Contactsms":"true","AgreeTC":"true","AgreePP":"true","ContactNumber":"+91 786789","NRIC":"NRIC_1","CommunicationAgreement":true}';    
    req.requestURI = 'services/apexrest/apex/Promotions/'; 
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueof(json1);
    RestContext.request = req;
    RestContext.response = res;    
    MyAccountPromotion.dopost();  
   
    //EMAIL is Empty
    String json3 ='{"PromotionId":"'+ca.id+'","Surname":"Test+surnames","Email":"","purchasestore":"001O000000jkqvW","Receiptnumber":"AcuveGold_21","ContactEmail":"true","Contactsms":"true","AgreeTC":"true","AgreePP":"true","ContactNumber":"+91 786789","NRIC":"NRIC_1","CommunicationAgreement":true}';    
    req.requestURI = 'services/apexrest/apex/Promotions/'; 
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueof(json3);
    RestContext.request = req;
    RestContext.response = res;    
    MyAccountPromotion.dopost(); 
    
    //Receiptnumber is Empty
    String json4 ='{"PromotionId":"'+ca.id+'","Surname":"Test+surnames","Email":"Recordtype789@Gold.com","purchasestore":"001O000000jkqvW","Receiptnumber": "","ContactEmail":"true","Contactsms":"true","AgreeTC":"true","AgreePP":"true","ContactNumber":"+91 786789","NRIC":"NRIC_1","CommunicationAgreement":true}';    
    req.requestURI = 'services/apexrest/apex/Promotions/'; 
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueof(json4);
    RestContext.request = req;
    RestContext.response = res;    
    MyAccountPromotion.dopost();
    
    //LEAD EXIST
    String json5 ='{"PromotionId":"'+ca.id+'","Surname":"Test123","Email":"Mouli@Lead1.com","purchasestore":"001O000000jkqvW","Receiptnumber": "WIN_TRIP","ContactEmail":"true","Contactsms":"true","AgreeTC":"true","AgreePP":"true","ContactNumber":"+91 123456789","NRIC":"NRIC_1","CommunicationAgreement":true}';    
    req.requestURI = 'services/apexrest/apex/Promotions/'; 
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueof(json5);
    RestContext.request = req;
    RestContext.response = res;    
    MyAccountPromotion.dopost();
    
    //CONTACT EXIST FOR SG FOR Non SG
    String json6 ='{"PromotionId":"'+ca.id+'","Surname": "Test+Non_SG","Email":"mouli@Con.com","purchasestore":"001O000000jkqvW","Receiptnumber":"WIN_TRIP","ContactEmail":"true","Contactsms":"true","AgreeTC":"true","AgreePP":"true","ContactNumber":"+91 987654321","NRIC":"NRIC_1","CommunicationAgreement":true}';    
    req.requestURI = 'services/apexrest/apex/Promotions/'; 
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueof(json6);
    RestContext.request = req;
    RestContext.response = res;    
    MyAccountPromotion.dopost();
    
    //CREATE A NEW CONTACT For Non SG
    String json7 ='{"PromotionId":"'+ca.id+'","Surname":"Non_SG","Email":"Non_SG@NEW.com","purchasestore":"'+a.Id+'","Receiptnumber": "WIN_TRIP","ContactEmail":"true","Contactsms":"true","AgreeTC":"true","AgreePP":"true","ContactNumber":"+91 987987987","NRIC":"Non_1","CommunicationAgreement":true}';    
    req.requestURI = 'services/apexrest/apex/Promotions/'; 
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueof(json7);
    RestContext.request = req;
    RestContext.response = res;    
    MyAccountPromotion.dopost();
   
    //subscribed $20 and the status=Redeemed
    String json9 ='{"PromotionId":"'+ca3.id+'","Surname":"Test+3","Email":"Non_SG@NEW.com","purchasestore":"'+a.Id+'","Receiptnumber":"WIN_TRIP","ContactEmail":"true","Contactsms":"true","AgreeTC":"true","AgreePP":"true","ContactNumber":"+91 987987987","NRIC":"NRIC_12+3","CommunicationAgreement":true}';    
    req.requestURI = 'services/apexrest/apex/Promotions/'; 
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueof(json9);
    RestContext.request = req;
    RestContext.response = res;    
    MyAccountPromotion.dopost();
    
    //subscribed $30 and the status=Not used
    String json10 ='{"PromotionId":"'+ca3.id+'","Surname":"Test+Non_SG","Email":"Non_SG@NEW.com","purchasestore":"'+a.Id+'","Receiptnumber":"WIN_TRIP","ContactEmail":"true","Contactsms":"true","AgreeTC":"true","AgreePP":"true","ContactNumber":"+91 987987987","NRIC":"NRIC_12","CommunicationAgreement":true}';    
    req.requestURI = 'services/apexrest/apex/Promotions/'; 
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueof(json10);
    RestContext.request = req;
    RestContext.response = res;    
    MyAccountPromotion.dopost();
    
    String json11 ='{"PromotionId":"'+ca3.id+'","Surname":"Test+2","Email":"Non_SG@NEW.com","purchasestore":"'+a.Id+'","Receiptnumber":"WIN_TRIP","ContactEmail":"true","Contactsms":"true","AgreeTC":"true","AgreePP":"true","ContactNumber":"+91 987987987","NRIC":"NRIC_12+2","CommunicationAgreement":true}';    
    req.requestURI = 'services/apexrest/apex/Promotions/'; 
    req.httpMethod = 'POST';
    req.requestBody = Blob.valueof(json11);
    RestContext.request = req;
    RestContext.response = res;    
    MyAccountPromotion.dopost();
    system.assertEquals(a.Name, 'TestGold', 'success');
    test.stopTest();
 }

}