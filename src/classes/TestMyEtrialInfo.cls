@isTest
private class TestMyEtrialInfo{   
    static testmethod void doget(){
        
        ExactTarget_Integration_Settings__c ex= new ExactTarget_Integration_Settings__c();
        ex.name='Default Channel Member ID';
        ex.Value__c='7203707';
        insert ex;  
        
        ExactTarget_Integration_Settings__c ex1= new ExactTarget_Integration_Settings__c();
        ex1.name='endpoint';
        ex1.Value__c='https://webservice.s7.exacttarget.com/Service.asmx';
        insert ex1;  
        
        ExactTarget_Integration_Settings__c ex2= new ExactTarget_Integration_Settings__c();
        ex2.name='ET_AUTH_ENDPOINT';
        ex2.Value__c='https://auth.exacttargetapis.com/v1/requestToken';
        insert ex2;  
        
        ExactTarget_Integration_Settings__c ex3= new ExactTarget_Integration_Settings__c();
        ex3.name='ET_CLIENT_ID';
        ex3.Value__c='27faf9xez3cak2wm7kex73ky';
        insert ex3;  
        
        ExactTarget_Integration_Settings__c ex4= new ExactTarget_Integration_Settings__c();
        ex4.name='ET_CLIENT_SECRET';
        ex4.Value__c='CDGBbns2tCX5ea6kqa8CFK7y';
        insert ex4;  
        
        ExactTarget_Integration_Settings__c ex5= new ExactTarget_Integration_Settings__c();
        ex5.name='messagekey';
        ex5.Value__c='0C1D6FC4-B45E-45F4-A8AD-64AEFDACDAB3';
        insert ex5;  
        
        ExactTarget_Integration_Settings__c ex6= new ExactTarget_Integration_Settings__c();
        ex6.name='username';
        ex6.Value__c='SalesforceService@Acuvue';
        insert ex6;  
        
        ExactTarget_Integration_Settings__c ex7= new ExactTarget_Integration_Settings__c();
        ex7.name='password';
        ex7.Value__c='Welcome@1';
        insert ex7;  
        
        ExactTarget_Integration_Settings__c ex8= new ExactTarget_Integration_Settings__c();
        ex8.name='SECURITY_ENDPOINT';
        ex8.Value__c='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        insert ex8;
        
        ExactTarget_Integration_Settings__c ex9= new ExactTarget_Integration_Settings__c();
        ex9.name='DataEvent_endpoint';
        ex9.Value__c='https://www.exacttargetapis.com/hub/v1/dataevents/';
        insert ex9; 
        
        ExactTarget_Integration_Settings__c ex10= new ExactTarget_Integration_Settings__c();
        ex10.name='SMS_Endpoint';
        ex10.Value__c='https://www.exacttargetapis.com/sms/v1/messageContact/';
        insert ex10;  
        
        ETRIAL_Trigger_Send_setting__c ets= new ETRIAL_Trigger_Send_setting__c();
        ets.Name ='TH';
        ets.RegistrationThankyou__c ='12345';
        ets.SMSMessage__c ='MjI6Nzg6MA';
        insert ets;     
        
        Account a   = New Account();
        a.Name      ='test+Etrial';
        a.ecp_name__c = 'test+Etrial';
        a.PublicAddress__c = 'TestPublic';
        a.PublicPhone__c = '9877677676';
        a.PublicState__c = 'publicstate';
        a.Email__c = 'suman@thylaksoft.com';
        a.OutletNumber__c = '989898';
        a.ActiveYN__c = true;
        a.CountryCode__c = 'TH';
        insert a;
        
        Product2 p= new Product2();
        p.Name='Test one';
        p.CurrencyIsoCode='USD';
        p.IsActive=True;
        p.UPC_Code__c='123512test';
        p.InternalName__c='Abc';
        insert p;
        
        list<Etrial__c> etlist = new list<Etrial__c>();
        
        Etrial__c et = new Etrial__c();
        et.Name = 'TestTrial';
        et.etrial_email__c = 'ramesh@test.com';
        et.Country__c = 'TH';
        et.etrial_gender__c = 'Male';
        et.etrial_first_name__c = 'Ftest';
        et.etrial_last_name__c  = 'Ltest';
        et.etrial_phone__c = '9840782431';
        et.acu_address__c = 'testaddr';   
        et.acu_birthdate__c = Datetime.now().date();
        et.acu_enewsletter__c = true;
        et.acu_question__c = 'Qtest';
        et.etrial_registration_check_1__c =true;
        et.etrial_registration_check_2__c =true;
        et.etrial_registration_check_3__c =true;
        et.etrial_registration_check_4__c =true;
        et.etrial_tnc__c                 =true;
        et.Account__c                  =a.id;
        et.etrial_product__c           =p.id;
        et.Booking_date__c             =Datetime.now().date(); 
        et.Booking_time__c='4';
        et.etrial_phone__c = '09916766235';
        insert et;
        etlist.add(et);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        list<string> strlist = new list<string>();
        strlist.add('test1');
        strlist.add('test2');
        
        ET_AdministrationService.datavalue dt=new ET_AdministrationService.datavalue(et.ecp_name__c,et.ecp_address__c,et.ecp_phone__c,et.ecp_province__c,et.ecp_state__c,et.Account__r.Email__c,et.Name,et.Account__r.OutletNumber__c,et.Account__r.AccountNumber,et.Account__r.ActiveYN__c,et.Account__r.Owner.Name,et.Account__r.CountryCode__c);
        ET_AdministrationService.smsSendRequest dt1=new ET_AdministrationService.smsSendRequest(strlist,true,true,'123456','123456');
        ET_AdministrationService.datasendrequest dt2=new ET_AdministrationService.datasendrequest(dt);
        ET_AdministrationService.result res2= new ET_AdministrationService.result('200','OK','123456','234567');
        ET_AdministrationService.smsSendResponse ct1 = new ET_AdministrationService.smsSendResponse('success');
        ET_AdministrationService.CL_keys ctk1 = new ET_AdministrationService.CL_keys('123456');
        ET_AdministrationService.datavalue2 dv2 = new ET_AdministrationService.datavalue2('123456789','testfirstname','testlastname','test@testmail.com');
        
        
        //Email exit
        req.requestURI = 'services/apexrest/apex/etrialinfo/'+et.etrial_email__c; 
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;    
        MyEtrialInfo.DoEtrial();  
        
        //Email does not exit
        req.requestURI = 'services/apexrest/apex/etrialinfo/'+'test@gmail.com'; 
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;    
        MyEtrialInfo.DoEtrial();        
        ET_AdministrationService.createTriggeredDataEventSend(etlist);
        ET_AdministrationService.createTriggeredEmailSend(etlist);
        ET_AdministrationService.createTriggeredSMSSend(etlist);
        List<Campaignmember> campmemlist = new  List<Campaignmember>([select id, name,campaign.country__c from campaignmember where campaign.country__c!=null]); 
        ET_AdministrationService.CampaignMemberTriggeredSMSSend(campmemlist);
		system.assertEquals(et.etrial_tnc__c,true,'success');
    }  
}