@isTest(SeeAllData=TRUE)
private class TestObjectUpdateBatch   
{
    static testmethod void ObjectUpdateBatchTest()
    {
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        List<Account> AcctList = new List<Account>();  
        List<Contact> ConList= new List<Contact>(); 
        List<Batch_Details__c > BDList = new List<Batch_Details__c >();  
        List<campaign> CampList= new List<campaign >(); 
        List<CampaignMember> CampMemList = new List<CampaignMember>();  
        List<PointHistory__c > PHList= new List<PointHistory__c >(); 
        List<Product2> ProdList = new List<Product2>();  
        List<TransactionTd__c > TransList= new List<TransactionTd__c >(); 
        
      Test.StartTest();
        Account IAcc= new Account();
        IAcc.Name='TestBICS';
        insert IAcc;
         system.assertEquals( IAcc.Name,'TestBICS','success');
        ObjectUpdateBatch WrapObj = new  ObjectUpdateBatch('Select id from Account limit 10',1);
        QL= WrapObj.start(BC);             
        Database.QueryLocatorIterator QIT =  QL.iterator();
        while (QIT.hasNext())
        {
            Account Acc = (Account)QIT.next();            
            System.debug(Acc);
            AcctList.add(Acc);
        } 
        
        WrapObj.execute(BC, AcctList);
        WrapObj.finish(BC); 
        WrapObj = new  ObjectUpdateBatch('Select id,Name from Contact limit 10',2);
        QL= WrapObj.start(BC);                
        QIT =  QL.iterator();
      
        while (QIT.hasNext())
        {
            Contact Acc = (Contact)QIT.next();            
            System.debug(Acc);
            ConList.add(Acc);
        }   
        
        WrapObj.execute(BC, ConList);
        WrapObj.finish(BC); 
        
        WrapObj = new  ObjectUpdateBatch('Select id from Batch_Details__c limit 10',3);
        QL= WrapObj.start(BC);                
        QIT =  QL.iterator();
        
        while (QIT.hasNext())
        {
            Batch_Details__c Acc = (Batch_Details__c)QIT.next();            
            System.debug(Acc);
            BDList.add(Acc);
        }   
        
        WrapObj.execute(BC, BDList);
        WrapObj.finish(BC); 
        
        WrapObj = new  ObjectUpdateBatch('Select id from campaign limit 10',5);
        QL= WrapObj.start(BC);                
        QIT =  QL.iterator();
        
        while (QIT.hasNext())
        {
            campaign Acc = (campaign)QIT.next();            
            System.debug(Acc);
            CampList.add(Acc);
        }   
        
        WrapObj.execute(BC, CampList);
        WrapObj.finish(BC); 
        
        WrapObj = new  ObjectUpdateBatch('Select id from CampaignMember limit 10',6);
        QL= WrapObj.start(BC);                
        QIT =  QL.iterator();
        
        while (QIT.hasNext())
        {
            CampaignMember Acc = (CampaignMember)QIT.next();            
            System.debug(Acc);
            CampMemList.add(Acc);
        }   
        
        WrapObj.execute(BC, CampMemList);
        WrapObj.finish(BC); 
        
        WrapObj = new  ObjectUpdateBatch('Select id from PointHistory__c limit 10',7);
        QL= WrapObj.start(BC);                
        QIT =  QL.iterator();
        
        while (QIT.hasNext())
        {
            PointHistory__c Acc = (PointHistory__c)QIT.next();            
            System.debug(Acc);
            PHList.add(Acc);
        }   
          
        WrapObj.execute(BC, PHList);
        WrapObj.finish(BC); 
        WrapObj.execute(BC, TransList);
        WrapObj.finish(BC);
        Test.StopTest();
    }
}