@isTest(SeeAllData=true)
public class TestOrderUpdateStartDate {
    
    static testMethod void TestUpdateOrderStartDateOne()
    {
        
        User user= [ select id ,name from User where Profile.Name='System Administrator' and isactive = True limit 1 ];
        System.runAs(user){
            
            Account acct = new Account(Name='tAccount');
            insert acct;
            
            Contract ct = new Contract();
            ct.AccountId = acct.Id;
            ct.Status = 'Draft';
            ct.StartDate = Date.Today();
            ct.ContractTerm = 12;
            insert ct;
            
            ct.Status = 'Activated';
            update ct;
            
            
            Product2 pd = new Product2(Name='Pord A',isActive=true);
            pd.IsActive = true;
            insert pd;
            
            
            //PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
            //Id standardPriceBookId = pb2Standard.Id;
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = pd.Id,
            UnitPrice = 10000, IsActive = true);
        	insert standardPrice;
            
            Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        	insert customPB;
            
             PricebookEntry pbe = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = pd.Id, UnitPrice = 12000, IsActive = true);
        	insert pbe;
        
            
            //PricebookEntry pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=pd.Id, UnitPrice=99, isActive=true);
            //insert pbe;
            Product2 p2ex = [SELECT Name FROM Product2 WHERE Id = :pd.Id];
            System.assertEquals(pd.Name,p2ex.Name);
            
            
            Order od = new Order();
            od.Name = '14TPE000001';
            od.AccountId = acct.Id;
            od.EffectiveDate = Date.Today();
            od.ContractId = ct.Id;
            od.Status = 'Draft';
            od.Pricebook2Id = pricebookId;
            insert od;
            
            OrderItem ordPd = new OrderItem(PriceBookEntryId=standardPrice.ID, OrderId=od.Id, Quantity=1, UnitPrice=99);
            insert ordPd;
            
            Date effDate = date.newinstance(2020, 2, 17);
            List<Order> odt = [ SELECT Id,Status,ActivatedDate,EffectiveDate FROM Order WHERE Name = '14TPE000001' LIMIT 1 ];
            
            Map<String, Order> mapOrd = new Map<String, Order>();
            mapOrd.put(od.Status, od);
            mapOrd.values().Status = System.Label.OrderStatusActivated;
            mapOrd.values().ActivatedDate = effDate;
            mapOrd.values().EffectiveDate = System.Today();
            update mapOrd.values();
           
       }
    }
}