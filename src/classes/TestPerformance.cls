global class TestPerformance implements Database.Batchable<Sobject>{
    
    global static final List<CouponContact__c> couponContactList = new List<CouponContact__c>();
    global static final List<String> countryCodeList = new List<String>();
        
    global Database.QueryLocator start(Database.batchableContext BC){
        
        Integer CurrentMonth = System.Today().Month();
        Map<String,MA2_CountryCode__c> countryCodeMap = MA2_CountryCode__c.getAll();
        for(MA2_CountryCode__c conCode : countryCodeMap.values()){
            countryCodeList.add(conCode.MA2_CountryCodeValue__c);
        }
        if(countryCodeList.size() > 0){
             return Database.getQueryLocator([select Id,AccountId,DOB__c,MA2_Grade__c,MA2_Country_Code__c, MA2_PreAssessment__c from Contact 
                    where MA2_PreAssessment__c = true AND MA2_Country_Code__c in: countryCodeList]);
        }else{
            return null;
        }
    }
    
    global static void execute(Database.batchableContext BC , List<Contact> contactList){
    
        Map<Id,Set<CouponContact__c>> activeContactCouponMap = new Map<Id,Set<CouponContact__c>>();
        Map<String,MA2_CountryCode__c> countryCodeMap = MA2_CountryCode__c.getAll();
        Map<String,String> countrCodeMap = new Map<String,String>();
        Map<Id,CouponContact__c> IgnoreCoupnContMap = new Map<Id,CouponContact__c>();
        List<Contact> FinalConList = new List<Contact>();
        Date CurrentDate = System.Today();
        Integer ThisYear = CurrentDate.year();
        List<contact> TempContactList = new list<contact>();
        Integer CurrentMonth = System.Today().Month();
        
        for(MA2_CountryCode__c conCode : countryCodeMap.values()){
            countryCodeList.add(conCode.MA2_CountryCodeValue__c);
            countrCodeMap.put(conCode.MA2_CountryCodeValue__c,conCode.MA2_CountryCodeValue__c);
        }
        
        for(Contact item: contactlist){
            Datetime BirthdayMonth = item.DOB__c;
            if(item.DOB__c != null && BirthdayMonth.Month() == CurrentMonth){
                TempContactList.add(item);        
            }
        }    
    }
    
    global void finish(Database.batchableContext BC){
   
    } 
}