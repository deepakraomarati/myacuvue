@isTest
Public class TestSGCampaignController {
    
    static Campaign cam;
    static Contact c;
    static CampaignMember cmember;
    static PageReference pageRef;
    
    private static testMethod void ValidateVoucher()
    {
        SGCampaignController sgcamp = new SGCampaignController();
        sgcamp.SAPID ='SAP123';
        sgcamp.VoucherCode ='VoucherCode_1';
        sgcamp.fileName ='test';
        sgcamp.fileBody =null;
        sgcamp.status ='Not Used';
        sgcamp.showpanel =true;
        sgcamp.fontcolor ='Red';
        cam=new Campaign(Name='SGCampaign',IsActive=true);
        Insert cam;
        
        c = new Contact(FirstName='Test',LastName='Gold');
        Insert c;
        
        ExactTarget_Integration_Settings__c SG = new ExactTarget_Integration_Settings__c();
        SG.Name = 'SGCampaign';
        SG.Value__c = cam.Id;
        insert SG;
        
        cmember=new CampaignMember(CampaignId=cam.Id,ContactId=c.Id,Status='Subscribed',Receipt_number__c='VoucherCode_1',SAP_ID__c='SAP123',Voucher_Status__c ='Not Used');
        Insert cmember;
        system.assertEquals(cmember.CampaignId,cam.Id,'success');
        pageRef= sgcamp.ValidateVoucher();
    }
    
    private static testMethod void ValidateVoucher2()
    {
        String myString = 'StringToBlob';
        Blob myBlob = Blob.valueof(myString);
        SGCampaignController sgcamp = new SGCampaignController();
        sgcamp.SAPID ='SAP1234';
        sgcamp.VoucherCode ='12345';
        sgcamp.fileName ='test';
        sgcamp.fileBody =myBlob;
        sgcamp.status ='Not Used';
        sgcamp.showpanel =true;
        sgcamp.fontcolor ='Red';
        
        cam=new Campaign(Name='SGCampaign',IsActive=true);
        Insert cam;
        
        c = new Contact(FirstName='Test2',LastName='Gold2');
        Insert c;
        
        ExactTarget_Integration_Settings__c SG = new ExactTarget_Integration_Settings__c();
        SG.Name = 'SGCampaign';
        SG.Value__c = cam.Id;
        insert SG;
        
        cmember=new CampaignMember(CampaignId=cam.Id,ContactId=c.Id,Status='Subscribed',Receipt_number__c='12345',SAP_ID__c='SAP1234',Voucher_Status__c ='Redeemed');
        Insert cmember;
        system.assertEquals(cmember.CampaignId,cam.Id,'success');
        Attachment attachment = new Attachment();
        attachment.body = myBlob;
        attachment.name = 'test';
        attachment.parentId = c.Id;
        attachment.description = 'SG_Voucher_Receipt';
        insert attachment;
        pageRef= sgcamp.ValidateVoucher();
    }
    
    private static testMethod void ValidateVoucher3()
    {
        String myString = 'StringToBlob';
        Blob myBlob = Blob.valueof(myString);        
        SGCampaignController sgcamp = new SGCampaignController();
        sgcamp.SAPID ='SAP1234';
        sgcamp.VoucherCode ='12345';
        sgcamp.fileName ='test';
        sgcamp.fileBody =myBlob;
        sgcamp.status ='Not Used';
        sgcamp.showpanel =true;
        sgcamp.fontcolor ='Red';
        
        cam=new Campaign(Name='SGCampaign',IsActive=true);
        Insert cam;
        
        c = new Contact(FirstName='Test2',LastName='Gold2');
        Insert c;
        
        ExactTarget_Integration_Settings__c SG = new ExactTarget_Integration_Settings__c();
        SG.Name = 'SGCampaign';
        SG.Value__c = cam.Id;
        insert SG;
        
        cmember=new CampaignMember(CampaignId=cam.Id,ContactId=c.Id,Status='Subscribed',Receipt_number__c='12345',SAP_ID__c='SAP1234',Voucher_Status__c ='Redeemed');
        Insert cmember;
        system.assertEquals(cmember.CampaignId,cam.Id,'success');
        Attachment attachment = new Attachment();
        attachment.body = myBlob;
        attachment.name = 'test';
        attachment.parentId = cam.Id;
        attachment.description = 'SG_Voucher_Receipt';
        insert attachment;
        pageRef= sgcamp.ValidateVoucher();
    }
}