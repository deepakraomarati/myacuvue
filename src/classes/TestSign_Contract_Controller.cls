@isTest global with sharing class TestSign_Contract_Controller
{
    static Account account;
    static Contract contract;
    static Attachment atch;
    static RecordType recName;
    
    public static void callContrct(RecordType rec,PageReference page)
    {
        account = new Account(OutletNumber__c='cd1235', Name='TestAccount1', ShippingStreet='test street', ShippingPostalCode='5855', ShippingCountry='Korea');
        insert account;
        string accountid = account.id;
        
        contract = new Contract(AccountId=account.Id, BC_Card_Merchant_Number__c = '12356666', Business_Registration_Number__c = '848543', RecordTypeId = rec.Id,Contract_Start_Date__c = System.Today(), Contract_End_Date__c = System.Today()+1, Status='Draft');
        insert contract;
        
        atch = new Attachment(ParentId = contract.Id, Name = 'test', body = Blob.ValueOf(''));
        insert atch;
        
        String id= contract.Id;
        string imageUrl = 'https://www.google.com/search?q=signature&biw=1366&bih=643&source=lnms&tbm=isch&sa=X&sqi=2&ved=0ahUKEwj3yNCQn9zNAhULHD4KHZ_KALQQ_AUIBigB#imgrc=Cn1C3wUzK9VnRM%3A';
        PageReference pageRef = page;
        pageRef.getParameters().put('accountid', contract.AccountId);
        pageRef.getParameters().put('id', contract.id);
        Test.setCurrentPage(pageRef);
        
        Sign_Contract_Controller controller = new Sign_Contract_Controller();
        controller.save();
        controller.getAccount();
        controller.getContract();
        
        Sign_Contract_Controller.saveSignature(contract.AccountId, id);
        System.assertEquals('https://www.google.com/search?q=signature&biw=1366&bih=643&source=lnms&tbm=isch&sa=X&sqi=2&ved=0ahUKEwj3yNCQn9zNAhULHD4KHZ_KALQQ_AUIBigB#imgrc=Cn1C3wUzK9VnRM%3A',imageUrl);
        System.assertEquals(contract.id, id);
        System.assertEquals(account.id, accountid);
    }
    
    public static testMethod void saveSignature1()
    {
        String name= '아큐브 멀티포컬 취급 약정서';
        recName = [select Id from RecordType where Name = '아큐브 멀티포컬 취급 약정서' limit 1];
        system.assertEquals(Name,'아큐브 멀티포컬 취급 약정서','success');
        callContrct(recName,Page.Sign_Contract_1);
    }
    
    public static testMethod void saveSignature2()
    {
        String name= '마이아큐브 프로그램 제공 및 사용 약정서';
        recName = [select Id from RecordType where Name = '마이아큐브 프로그램 제공 및 사용 약정서' limit 1];
        system.assertEquals(Name,'마이아큐브 프로그램 제공 및 사용 약정서','success');
        callContrct(recName,Page.Sign_Contract_2);
    }
    
    public static testMethod void saveSignature3()
    {
        String name= '아큐브 매출 및 지원 약정서';
        recName = [select Id from RecordType where Name = '아큐브 매출 및 지원 약정서' limit 1];
        system.assertEquals(Name, '아큐브 매출 및 지원 약정서','success');
        callContrct(recName,Page.Sign_Contract_3);
    }
    
    public static testMethod void saveSignature4()
    {
        String name='공급계약서';
        recName = [select Id from RecordType where Name = '공급계약서' limit 1];
        system.assertEquals(Name,'공급계약서','success');
        callContrct(recName,Page.Sign_Contract_4);
    }    
}