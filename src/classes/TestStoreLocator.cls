@isTest
private class TestStoreLocator{
    
    static testMethod void stlocator() {
        
        Account acc=new account();
        acc.name='test';
        acc.CountryCode__c='AUS';   
        acc.ECP_Name__c='s';
        acc.ActiveYN__c=true;
        acc.Latitude__c='1.3473573';   
        acc.Longitude__c='103.7573836';
        acc.OutletNumber__c='123456';
        acc.PublicAddress__c='test address';
        acc.PublicZone__c='1234';
        acc.PublicState__c='XYZ';
        acc.CountryCode__c='SGP';
        acc.Email__c='testemail@test.com';
        acc.Phone='88612345678';
        acc.eOrderYN__c=true;
        insert acc;
        system.assertEquals(acc.name,'test');
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = 'apexrest/apex/storelocator/AUS/1.3473573/103.7573836/1'; 
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;    
        StoreLocator.doGet();
                
        req.requestURI = 'apexrest/apex/storelocator/AUS/1.3473573/103.7573836/1/s/'; 
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;    
        StoreLocator.doGet();  
        //StoreLocator.StoreDTO(acc.Id,acc.ECP_Name__c,acc.OutletNumber__c,acc.PublicAddress__c,acc.PublicZone__c,acc.PublicState__c,acc.CountryCode__c,acc.Geolocation__Latitude__s,acc.Geolocation__Longitude__s,acc.Email__c,acc.Phone,acc.eOrderYN__c)
    }   
}