@isTest
public with sharing class TestSurveyQuestionItem 
{
    static testmethod void doinsertSurveyQuestionItem ()
    {  
        SurveyQuestionItem__c SQItem = new SurveyQuestionItem__c();
        SQItem.Name='TestQuestionItem';
        SQItem.AWS_Survey__c=1;     
        SQItem.AWS_SurveyQuestion__c  =2;   
        insert SQItem;
    
        Survey__c Svey = new Survey__c();
        Svey.Name='TestSurvey';
        Svey.s_seq__c=1;
        insert Svey;
        
        SurveyQuestion__c SQuestion= new SurveyQuestion__c();
        SQuestion.Aws_s_seq__c=1; 
        SQuestion.q_seq__c=2; 
        SQuestion.q_type__c=3;    
        insert SQuestion; 
        system.assertEquals(SQuestion.Aws_s_seq__c,1,'success');
        
        SurveyQuestionItem__c SQItemUpdate = new SurveyQuestionItem__c();
        SQItemUpdate.Id= SQItem.Id;     
        SQItemUpdate.s_seq__c=Svey.Id;
        SQItemUpdate.q_seq__c=SQuestion.Id;     
        update SQItemUpdate;
    }    
}