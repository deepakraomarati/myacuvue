@isTest
private class TestTH_Assignmentfor2ndeVoucher {
    static Account acc1;
    static Contact con1;
    static Campaign cam1,cam2;
    static CampaignMember cmember1;
    
    private static testMethod void TestTH_Assignmentfor2ndeVoucher(){
        
        acc1=new Account(Name='THCampaign ECP1',OutletNumber__c='ECP12345',Phone='123456789');
        Insert acc1;
        cam1=new Campaign(Name='THCampaignI',IsActive=true);
        Insert cam1;
        cam2=new Campaign(Name='THCampaignII',IsActive=true);
        Insert cam2;
        con1 = new Contact(LastName='TestContact1',NRIC__c='242K');
        Insert con1;
        datetime redeemeddate = Datetime.now().addDays(-21);
        cmember1=new CampaignMember(CampaignId=cam1.Id,ContactId=con1.Id,Status='Subscribed',Receipt_number__c='12345',Voucher_Status__c ='Redeemed',TH_2ndVoucher__c = false,Account__c=acc1.Id,Redeemed_Date_Time__c = redeemeddate);
        Insert cmember1;
        Datetime pstdate = Datetime.now().addDays(-21);
        Test.setCreatedDate(cmember1.id, pstdate);
        ExactTarget_Integration_Settings__c TH = new ExactTarget_Integration_Settings__c();
        TH.Name = 'THCampaign';
        TH.Value__c = cam1.Id;
        insert TH;    
        ExactTarget_Integration_Settings__c TH2 = new ExactTarget_Integration_Settings__c();
        TH2.Name = 'THCampaignII';
        TH2.Value__c = cam2.Id;
        insert TH2;
        Test.startTest();
        TH_Assignmentfor2ndeVoucher obj = new TH_Assignmentfor2ndeVoucher();
        DataBase.executeBatch(obj);
		system.assertEquals(TH2.Name, 'THCampaignII', 'Success');
        Test.stopTest();
    }
}