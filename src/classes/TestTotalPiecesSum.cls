@isTest(seealldata = true)
private class TestTotalPiecesSum {
    
    static testMethod void myUnitTest() {
    
        //Test Account Insert
        String countryname_HK = 'HKG';
        final Account accList = new Account(Name = 'Test' , MA2_AccountId__c = '978394' ,AccountNumber = '54321',
                                            OutletNumber__c = '54321',ActiveYN__c = true,
                                            My_Acuvue__c = 'Yes',CountryCode__c = countryname_HK ,PublicZone__c = 'Testtt');
        insert accList;
        
        Product2 p = new Product2();
        p.Name = ' Test Product ';
        p.Description='Test Product Entry 1';
        p.productCode = 'ABC';
        p.isActive = true;
        insert p;
        
        Pricebook2  standardPb = [select id, name, isActive from Pricebook2 limit 1];
        PricebookEntry standardPrice = new PricebookEntry();
        standardPrice.Pricebook2Id = standardPb.Id;
        standardPrice.Product2Id = p.Id;
        standardPrice.UnitPrice = 1;
        standardPrice.IsActive = true;
        standardPrice.UseStandardPrice = false;
        insert standardPrice ;
        
        //Test Order Insert
        Order o = new Order();
        o.Name = 'Test Order ';
        o.Status = 'Draft';
        o.EffectiveDate = system.today();
        o.EndDate = system.today() + 4;
        o.AccountId = accList.id;
        o.Pricebook2Id =  standardPb.Id ;
        insert o;
        
        OrderItem i = new OrderItem();
        i.OrderId = o.id;
        i.Quantity = 24;
        i.UnitPrice = 240;
        i.Product2id = p.id;
        i.PricebookEntryId=standardPrice.id;
        i.Order_Line_Number__c = 2;
        i.Order_Line_Number_Text__c = 'ABC';
        insert i;
        
        //Test OrderItem on update
        OrderItem op1 = new OrderItem();
        op1.Order_Line_Number_Text__c = 'ABC';
        op1.id = i.id;
        op1.Order_Line_Number__c = 24;
        update op1;
        
        //Test OrderItem on second insert
        OrderItem i2 = new OrderItem();
        i2.OrderId = o.id;
        i2.Quantity = 24;
        i2.UnitPrice = 240;
        i2.PricebookEntryId=standardPrice.id;
        insert i2;
        system.assertEquals(i2.PricebookEntryId,standardPrice.id,'success');
    }  
}