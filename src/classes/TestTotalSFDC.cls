@isTest
private class TestTotalSFDC  
{
    private static void init(){
        
        String Objectname='Account';
        Decimal Objectvalue=3;
        system.assertEquals(Objectname,'Account','success');
        List<TotalSFDC.TotalObjectCounts> Totallist = new List<TotalSFDC.TotalObjectCounts>();
        TotalSFDC.TotalObjectCounts WrapObj = new  TotalSFDC.TotalObjectCounts(''+Objectname,objectvalue);
        Totallist.add(WrapObj);
    }
    
    static testmethod void TotalSFDCTest()
    {
        Test.startTest();
        TotalSFDC TotalSFDCObj = new TotalSFDC (); 
        CountObject__c Tobj = new CountObject__c();
        Tobj.Name='Account';
        Tobj.Count__c=2500;
        insert Tobj;
        
        Tobj = new CountObject__c();
        Tobj.Name='Contact';
        Tobj.Count__c=2500;
        insert Tobj;
        
        Tobj = new CountObject__c();
        Tobj.Name='BatchDetails';
        Tobj.Count__c=2500;
        insert Tobj;
        
        Tobj = new CountObject__c();
        Tobj.Name='Product';
        Tobj.Count__c=2500;
        insert Tobj;
        
        Tobj = new CountObject__c();
        Tobj.Name='Campaign';
        Tobj.Count__c=2500;
        insert Tobj;
        
        Tobj = new CountObject__c();
        Tobj.Name='CampaignMember';
        Tobj.Count__c=2500;
        insert Tobj;
        
        Tobj = new CountObject__c();
        Tobj.Name='PointHistory';
        Tobj.Count__c=2500;
        insert Tobj;
        
        Tobj = new CountObject__c();
        Tobj.Name='TransactionTds';
        Tobj.Count__c=2500;
        insert Tobj;
        system.assertEquals(Tobj.Name,'TransactionTds','success');
        TotalSFDCObj.GetObjectCountItems(); 
        Test.StopTest(); 
    }
}