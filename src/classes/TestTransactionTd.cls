@isTest
public with sharing class TestTransactionTd 
{
    static testmethod void doinsertGetTransactionTd ()
    {
         TriggerHandler__c mcs = TriggerHandler__c.getvalues('HandleTriggers');
      String name='xCP22MdvhMSpPmi2T5SKDq8bwSLvuqRm';
        Credientials__c cred=new Credientials__c();
        cred.Name='CouponSendToApigee';
        cred.Api_Key__c='xCP22MdvhMSpPmi2T5SKDq8bwSLvuqRm';
        cred.Client_Id__c='b3U65J1vylrxEeavNcUjU0xIEg';
        cred.Client_Secret__c='b3U6xFGVXdXUZQM4OCnN0K2RGgUesnY';
        cred.Target_Url__c='https://jnj-prod.apigee.net/v1/sfdc/configuration/add';
        insert cred; 
        system.assertEquals(name,'xCP22MdvhMSpPmi2T5SKDq8bwSLvuqRm','success');
        
      
        Account Acct = new Account();
        Acct.Name='TestAccount';
        Acct.Aws_AccountId__c='123';
        insert Acct;
        
        Contact Con= new Contact();
        Con.LastName='testcouponsendlist'; 
        Con.Aws_ContactId__c='556';   
        Con.Aws_AccountId__c='123';
        insert Con; 
        
        Coupon__c Coup = new Coupon__c();      
        Coup.DB_ID__c=52;        
        insert Coup;
        
        Campaign Camp = new Campaign();
        Camp.Name='TestCamp';
        Camp.DB_ID__c=23;
        insert Camp;
     
    }    
}