@isTest
private class TestVoucherRedeemptionControllerHK {
    
    static Account acc;
    static Campaign cam,cam2;
    static Contact c,c1,c2;
    static CampaignMember cmember1, cmember2;
    
    private static testMethod void RedeemVoucher()
    {
        acc=new Account(Name='SGCampaign ECP',OutletNumber__c='ECP12345',Phone='123456789',Marketing_Program__c='AAP');
        Insert acc;
        
        cam=new Campaign(Name='SGCampaign2',IsActive=true);
        Insert cam;
        
        cam2=new Campaign(Name='SGCampaign',IsActive=true);
        Insert cam2;
        
        c1 = new Contact(LastName='TestContact',NRIC__c='89700514');
        Insert c1;
        c2 = new Contact(LastName='Test2',NRIC__c='98653728');
        Insert c2;
        
        cmember1=new CampaignMember(CampaignId=cam.Id,ContactId=c1.Id,Status='Subscribed',Receipt_number__c='12345',Voucher_Status__c ='Not Used',Account__c=acc.Id);
        Insert cmember1;
        system.assertEquals(cmember1.CampaignId,cam.Id,'success');
        Test.setCreatedDate(cmember1.Id, DateTime.newInstance(2016,5,5));
        
        cmember2=new CampaignMember(CampaignId=cam.Id,ContactId=c2.Id,Status='Subscribed',Receipt_number__c='01234',Voucher_Status__c ='Not Used',Account__c=acc.Id,Dummy_LastModifiedDate__c=DateTime.newInstance(2016, 1, 20, 12, 30, 2));
        Insert cmember2;
        
        ExactTarget_Integration_Settings__c SG = new ExactTarget_Integration_Settings__c();
        SG.Name = 'HKCampaign';
        SG.Value__c = cam.Id;
        insert SG; 
        list<contact> lstcon = new list<contact>();
        for(integer i=1; i<=16;i++){
            c1 = new Contact(LastName='Testloop'+i,NRIC__c='loop'+i);
            lstcon.add(c1);
        }
        insert lstcon;
        list<CampaignMember> lstcampmem = new list<CampaignMember>();
        for(integer i=1; i<=15;i++){
            
            cmember2=new CampaignMember(CampaignId=cam.Id,ContactId=lstcon[i].Id,Status='Subscribed',Receipt_number__c='Rec'+i,Voucher_Status__c ='Not Used',Account__c=acc.Id,
                                        No_of_Boxes1__c=i,No_of_Boxes2__c=i,No_of_Boxes3__c=i,No_of_Boxes4__c=i);
            lstcampmem.add(cmember2);
        }
        
        insert lstcampmem ;
        list<CampaignMember> upcampmem = new list<CampaignMember>();
        integer i=5;
        for(CampaignMember ccm:lstcampmem){            
            ccm.Voucher_Status__c ='Redeemed';
            ccm.Dummy_LastModifiedDate__c=DateTime.newInstance(2016, i, 20, 12, 30, 2);
            upcampmem.add(ccm);
            i++;
        }
        update upcampmem;
        
        Test.startTest();
        VoucherRedeemptionControllerHK voucherRed= new VoucherRedeemptionControllerHK();
        voucherRed.SAPID ='ECP1234';
        voucherRed.phone ='123456';
        voucherRed.Login();
        voucherRed.LoggedInEcpId=acc.Id;
        voucherRed.initLoad();
        
        VoucherRedeemptionControllerHK voucherRed2= new VoucherRedeemptionControllerHK();
        voucherRed2.SAPID ='ECP12345';
        voucherRed2.phone ='123456789';
        voucherRed2.Login();
        voucherRed2.LoggedInEcpId=acc.Id;
        voucherRed2.initLoad();
        voucherRed2.Logout();
        voucherRed2.getSelectBrands();
        
        VoucherRedeemptionControllerHK voucherRedeem0 = new VoucherRedeemptionControllerHK();
        voucherRedeem0.Name='Test2';
        voucherRedeem0.NRIC='S14642422';
        voucherRedeem0.HKCampaign=cam.Id;
        voucherRedeem0.RedeemVoucher();
        
        VoucherRedeemptionControllerHK voucherRedeem01 = new VoucherRedeemptionControllerHK();
        voucherRedeem01.Name='TestContact';
        voucherRedeem01.NRIC='S1464242K';
        voucherRedeem01.HKCampaign=cam.id;
        voucherRedeem01.RedeemVoucher();
        
        VoucherRedeemptionControllerHK voucherRedeem2 = new VoucherRedeemptionControllerHK();
        voucherRedeem2.fileName = 'Receipt';
        Blob ImageFile2 = Blob.ValueOf('Test.jpg');
        voucherRedeem2.fileBody = ImageFile2;
        voucherRedeem2.Name='Test2';
        voucherRedeem2.NRIC='98653728';
        voucherRedeem2.status ='Not Used';
        voucherRedeem2.showpanel =true;
        voucherRedeem2.fontcolor ='Red';
        voucherRedeem2.Box1 ='2';
        voucherRedeem2.Box2 ='3';
        voucherRedeem2.Box3 ='4';
        voucherRedeem2.Box4 ='5';
        voucherRedeem2.Box5 ='2';
        voucherRedeem2.ReceiptNo='';
        voucherRedeem2.LoggedInEcpId=acc.Id;
        voucherRedeem2.HKCampaign=cam.Id;
        voucherRedeem2.RedeemVoucher();
        
        VoucherRedeemptionControllerHK voucherRedeem3 = new VoucherRedeemptionControllerHK();
        voucherRedeem3.Name='TestContact';
        voucherRedeem3.NRIC='89700514';
        voucherRedeem3.status ='Not Used';
        voucherRedeem3.showpanel =true;
        voucherRedeem3.fontcolor ='Red';
        voucherRedeem3.Box1 ='2';
        voucherRedeem3.Box2 ='3';
        voucherRedeem3.Box3 ='4';
        voucherRedeem3.Box4 ='5';
        voucherRedeem3.Box5 ='2';
        voucherRedeem3.ReceiptNo='Rec22';
        voucherRedeem3.LoggedInEcpId=acc.Id;
        voucherRedeem3.HKCampaign= cam.Id;
        voucherRedeem3.RedeemVoucher();
        
        //To cover AggregateResult in Refresh method
        VoucherRedeemptionControllerHK voucherRedeem4 = new VoucherRedeemptionControllerHK();
        voucherRedeem4.fileName = 'Receipt';
        Blob ImageFile4 = Blob.ValueOf('Test.jpg');
        voucherRedeem4.fileBody = ImageFile4;
        voucherRedeem4.Name='Testloop5';
        voucherRedeem4.NRIC='S1464oop5';
        voucherRedeem4.status ='Not Used';
        voucherRedeem4.showpanel =true;
        voucherRedeem4.fontcolor ='Red';
        voucherRedeem4.Box1 ='2';
        voucherRedeem4.Box2 ='3';
        voucherRedeem4.Box3 ='4';
        voucherRedeem4.Box4 ='5';
        voucherRedeem4.Box5 ='2';
        voucherRedeem4.ReceiptNo='';
        voucherRedeem4.LoggedInEcpId=acc.Id;
        voucherRedeem4.HKCampaign=cam.Id;
        voucherRedeem4.Refresh();
        Test.stopTest();
        
    } 
}