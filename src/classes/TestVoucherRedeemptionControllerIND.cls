@isTest
private class TestVoucherRedeemptionControllerIND {
    private static Account acc;
    private static Campaign cam;
    static List<CampaignMember> lstCampMemb = new List<CampaignMember>();

	private static void setupTestData() {     
        
        // Create Account
        acc = new Account(Name = 'INDCampaign ECP', OutletNumber__c = 'ECP12345', Phone = '123456789');
        insert acc;
        
        // Create IND Campaign
        cam = new Campaign(Name = 'INDCampaign', IsActive = true);
        insert cam;
        
        // Create Custom setting record
        ExactTarget_Integration_Settings__c settings = new ExactTarget_Integration_Settings__c(Name = 'INDCampaign', Value__c = cam.Id);
        insert settings; 
        
        // Create Contacts and Campaign Memebers for voucher redemption
        List<Contact> lstContact = new List<Contact>();
        for (integer i=1; i<=2; i++) {
            lstContact.add(new Contact(LastName = 'Test Con'+i, NRIC__c = 'Test'+i));
        }
        insert lstContact;
        
        for (integer i=1; i<=2; i++) {
            lstCampMemb.add(new CampaignMember(CampaignId = cam.Id, ContactId = lstContact[i-1].Id, Status = 'Subscribed',
                                                Voucher_Status__c = 'Not Used', Account__c = acc.Id));
        }
        insert lstCampMemb;
        lstCampMemb = [SELECT Id, User_eVoucher_Code__c FROM CampaignMember LIMIT 2];
        
        // Create Contacts for redemption status
        List<Contact> lstCon = new List<Contact>();
        List<Contact> lstCon2 = new List<Contact>();
        for (integer i=1; i<=12; i++) {            
            Contact con = new Contact(LastName = 'Testloop'+i, NRIC__c = 'loop'+i);
            lstCon.add(con);
        }
        insert lstCon;        
        for (integer i=1; i<=12; i++) {            
            Contact con = new Contact(LastName = 'Testloop'+i, NRIC__c = 'loop'+i);
            lstCon2.add(con);
        }
        insert lstCon2;
        
        // Create Campaign Members with status - Redeemed and Archived for redemption status
        List<CampaignMember> lstCampMem = new List<CampaignMember>();
        List<CampaignMember> lstCampMemArch = new List<CampaignMember>();
        for (integer i=1; i<=12; i++) {            
            CampaignMember cmember = new CampaignMember(CampaignId = cam.Id, ContactId = lstcon[i-1].Id, Status = 'Subscribed', 
                                                        Account__c = acc.Id, Voucher_Status__c = 'Redeemed', Receipt_number__c = 'Rec'+i, 
                                                        No_of_Boxes1__c = i,No_of_Boxes2__c = i, No_of_Boxes3__c = i, No_of_Boxes4__c = i,
                                                        Redeemed_Date_Time__c = DateTime.newInstance(Date.today().year(), i, 20, 12, 30, 2));
            lstCampMem.add(cmember);
        }
        insert lstCampMem;
        for (integer i=1; i<=12; i++) {
        	Test.setCreatedDate(lstCampMem[i-1].Id, Date.newInstance(Date.today().year(),i,1));
        }
		        
        
        for (integer i=1; i<=12; i++) {            
            CampaignMember cmember = new CampaignMember(CampaignId = cam.Id, ContactId = lstcon2[i-1].Id, Status = 'Subscribed', 
                                                        Account__c = acc.Id, Voucher_Status__c ='Archived', Receipt_number__c = 'Rec'+i, 
                                                        No_of_Boxes1__c = i, No_of_Boxes2__c = i, No_of_Boxes3__c = i, No_of_Boxes4__c = i,
                                                        Redeemed_Date_Time__c = DateTime.newInstance(Date.today().year(), i, 20, 12, 30, 2));
            lstCampMemArch.add(cmember);
        }
        insert lstCampMemArch;
        for (integer i=1; i<=12; i++) {
        	Test.setCreatedDate(lstCampMemArch[i-1].Id, Date.newInstance(Date.today().year(),i,5));
        }   
        
    }
    
    private static testMethod void TestRedeemVoucher() {
        
        // Create test data
        setupTestData();
        
        // Start Test
        Test.startTest();		
        // Login Redemption portal with incorrect credentials
        IND_VoucherRedeemptionController voucherRed = new IND_VoucherRedeemptionController();
        voucherRed.SAPID = 'ECP1234';
        voucherRed.phone = '123456';
        voucherRed.Login();
        
        // Login portal, submit redemption form without any details
        IND_VoucherRedeemptionController voucherRed2 = new IND_VoucherRedeemptionController();
        voucherRed2.SAPID = 'ECP12345';
        voucherRed2.phone = '123456789';
        voucherRed2.Login();
        voucherRed2.LoggedInEcpId = acc.Id;
        voucherRed2.getSelectBrands();
        voucherRed2.INDCampaign = cam.Id;
        voucherRed2.RedeemVoucher();
        voucherRed2.initLoad();
        voucherRed2.Logout();
        
        // Login portal, submit redemption form with eVoucher code, uploading receipt and no.of boxes purchased entered
        IND_VoucherRedeemptionController voucherRed3 = new IND_VoucherRedeemptionController();
        voucherRed3.SAPID = 'ECP12345';
        voucherRed3.phone = '123456789';
        voucherRed3.Login();
        voucherRed3.LoggedInEcpId = acc.Id;
        voucherRed3.getSelectBrands();
        voucherRed3.INDCampaign=cam.Id;
        voucherRed3.eVoucher = lstCampMemb[0].User_eVoucher_Code__c;
		voucherRed3.fileName = 'Receipt';
        Blob ImageFile2 = Blob.ValueOf('Test.jpg');
        voucherRed3.fileBody = ImageFile2;        
		voucherRed3.Box1 = voucherRed3.Box2 = '2';
        voucherRed3.RedeemVoucher();
        voucherRed3.initLoad();
        voucherRed3.Logout();
        
        // Login portal, submit redemption form with eVoucher code, Receipt No. and no.of boxes purchased
        IND_VoucherRedeemptionController voucherRed4 = new IND_VoucherRedeemptionController();
        voucherRed4.SAPID = 'ECP12345';
        voucherRed4.phone = '123456789';
        voucherRed4.Login();
        voucherRed4.LoggedInEcpId = acc.Id;
        voucherRed4.getSelectBrands();
        voucherRed4.INDCampaign = cam.Id;
        voucherRed4.eVoucher = lstCampMemb[1].User_eVoucher_Code__c;
        voucherRed4.ReceiptNo = 'Rec123';       
		voucherRed4.Box1 = voucherRed4.Box2 = '2';
        voucherRed4.RedeemVoucher();
        voucherRed4.initLoad();
        voucherRed4.Logout();
        
        // Login portal, check redemption status by Voucher/Product status
        IND_VoucherRedeemptionController voucherRed5 = new IND_VoucherRedeemptionController();
        voucherRed5.SAPID ='ECP12345';
        voucherRed5.phone ='123456789';
        voucherRed5.Login();
        voucherRed5.LoggedInEcpId=acc.Id;
        voucherRed5.Refresh();
        voucherRed5.initLoad();
        voucherRed5.Logout();        
        Test.stopTest();
        
        // Assert
        CampaignMember updCampMemb = [Select Id, Voucher_Status__c FROM CampaignMember WHERE Id =: lstCampMemb[0].Id];
        System.assert(updCampMemb.Voucher_Status__c == 'Redeemed', 'Voucher is not redeemed');
    }
    
}