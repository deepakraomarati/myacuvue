@isTest
private class TestVoucherRedeemptionControllerMY {   
    static Account acc;
    static Campaign cam,cam2;
    static Contact c,c1,c2,c3;
    static CampaignMember cmember1, cmember2, cmember3,cmember2017 ,cmember3_2017 ;
    
    private static testMethod void RedeemVoucher()
    {
        acc=new Account(Name='MYCampaign ECP',OutletNumber__c='ECP12345',Phone='123456789');
        Insert acc;
        
        cam=new Campaign(Name='MYCampaign',IsActive=true);
        Insert cam;
        
        cam2=new Campaign(Name='MYCampaign2',IsActive=true);
        Insert cam2;
        
        c1 = new Contact(LastName='TestContact',NRIC__c='242K');
        Insert c1;
        c2 = new Contact(LastName='Test2',NRIC__c='242L');
        Insert c2;
        c3 = new Contact(LastName='Test3',NRIC__c='242M');
        Insert c3;
        
        cmember1=new CampaignMember(CampaignId=cam.Id,ContactId=c1.Id,Status='Subscribed',Receipt_number__c='12345',Voucher_Status__c ='Not Used',Account__c=acc.Id);
        Insert cmember1;
        Test.setCreatedDate(cmember1.Id, DateTime.newInstance(2016,5,5));
        
        cmember2=new CampaignMember(CampaignId=cam.Id,ContactId=c2.Id,Status='Subscribed',Receipt_number__c='01234',Voucher_Status__c ='Not Used',Account__c=acc.Id,Dummy_LastModifiedDate__c=DateTime.newInstance(2016, 1, 20, 12, 30, 2));
        Insert cmember2;
        
        cmember3=new CampaignMember(CampaignId=cam.Id,ContactId=c3.Id,Status='Subscribed',Receipt_number__c='01235',Voucher_Status__c ='Archived',Account__c=acc.Id,Dummy_LastModifiedDate__c=DateTime.newInstance(2016, 1, 25, 12, 30, 2));
        Insert cmember3;
        
        ExactTarget_Integration_Settings__c MY = new ExactTarget_Integration_Settings__c();
        MY.Name = 'MYCampaign';
        MY.Value__c = cam.Id;
        insert MY; 
        
        list<contact> lstcon = new list<contact>();
        list<contact> lstcon2 = new list<contact>();
        for(integer i=1; i<=16;i++){
            
            c1 = new Contact(LastName='Testloop'+i,NRIC__c='loop'+i);
            lstcon.add(c1);
        }
        insert lstcon;
        
        for(integer i=0; i<=15;i++){
            
            c2 = new Contact(LastName='Testloop'+i,NRIC__c='loop'+i);
            lstcon2.add(c2);
        }
        insert lstcon2;
        
        list<CampaignMember> lstcampmem = new list<CampaignMember>();
        list<CampaignMember> lstcampmemArch = new list<CampaignMember>();
        for(integer i=1; i<=15;i++){
            
            cmember2=new CampaignMember(CampaignId=cam.Id,ContactId=lstcon[i].Id,Status='Subscribed',Receipt_number__c='Rec'+i,Voucher_Status__c ='Not Used',Account__c=acc.Id,
                                        No_of_Boxes1__c=i,No_of_Boxes2__c=i);
            lstcampmem.add(cmember2);
        }
        insert lstcampmem ;
        for(integer i=0; i<15;i++){
            
            cmember3=new CampaignMember(CampaignId=cam.Id,ContactId=lstcon2[i].Id,Status='Subscribed',Receipt_number__c='Rec'+i,Voucher_Status__c ='Not used',Account__c=acc.Id,
                                        No_of_Boxes1__c=i+1,No_of_Boxes2__c=i+1);
            lstcampmemArch.add(cmember3);
        }
        insert lstcampmemArch ;
        
        list<CampaignMember> upcampmem = new list<CampaignMember>();
        list<CampaignMember> upcampmem2 = new list<CampaignMember>();
        integer i=5;
        
        for(CampaignMember ccm:lstcampmem){
            
            
            ccm.Voucher_Status__c ='Redeemed';
            ccm.Dummy_LastModifiedDate__c=DateTime.newInstance(2016, i, 20, 12, 30, 2);
            upcampmem.add(ccm);
            i++;
        }
        
        update upcampmem;
        integer j=5;
        for(CampaignMember ccm: lstcampmemArch){
            
            
            ccm.Voucher_Status__c ='Archived';
            ccm.Dummy_LastModifiedDate__c=DateTime.newInstance(2016, j, 25, 12, 30, 2);
            upcampmem2.add(ccm);
            j++;
        }
        
        update upcampmem2;
        
        
        list<contact> con2017 = new list<contact>();
        list<contact> con2_2017 = new list<contact>();
        for(integer k=1; k<=16;k++){
            
            c1 = new Contact(LastName='Testloop'+k,NRIC__c='loop'+k);
            con2017.add(c1);
        }
        insert con2017;
        
        for(integer k=0; k<=15;k++){
            
            c2 = new Contact(LastName='Testloop'+k,NRIC__c='loop'+k);
            con2_2017.add(c2);
        }
        insert con2_2017;
        
        list<CampaignMember> campmem2017 = new list<CampaignMember>();
        list<CampaignMember> campmem2017Arch = new list<CampaignMember>();
        for(integer k=1; k<=15;k++){
            
            cmember2017=new CampaignMember(CampaignId=cam.Id,ContactId=con2017[k].Id,Status='Subscribed',Receipt_number__c='Rec'+k,Voucher_Status__c ='Not Used',Account__c=acc.Id,
                                           No_of_Boxes1__c=k,No_of_Boxes2__c=k,No_of_Boxes3__c=k,No_of_Boxes4__c=k,No_of_Boxes5__c=k,No_of_Boxes6__c=k);
            campmem2017.add(cmember2017);
        }
        insert campmem2017 ;
        for(integer k=0; k<15;k++){
            
            cmember3_2017=new CampaignMember(CampaignId=cam.Id,ContactId=con2_2017[k].Id,Status='Subscribed',Receipt_number__c='Rec'+k,Voucher_Status__c ='Not used',Account__c=acc.Id,
                                             No_of_Boxes1__c=k+1,No_of_Boxes2__c=k+1,No_of_Boxes3__c=k+1,No_of_Boxes4__c=k+1,No_of_Boxes5__c=k+1,No_of_Boxes6__c=k+1);
            campmem2017Arch.add(cmember3_2017);
        }
        insert campmem2017Arch ;
        
        list<CampaignMember> upcampmem_2017 = new list<CampaignMember>();
        list<CampaignMember> upcampmem2_2017 = new list<CampaignMember>();
        integer l=5;
        
        for(CampaignMember ccm:campmem2017){
            
            
            ccm.Voucher_Status__c ='Redeemed';
            ccm.Dummy_LastModifiedDate__c=DateTime.newInstance(2017, l, 20, 12, 30, 2);
            upcampmem_2017.add(ccm);
            l++;
        }
        
        update upcampmem_2017;
        integer m=5;
        for(CampaignMember ccm: campmem2017Arch){
            
            
            ccm.Voucher_Status__c ='Archived';
            ccm.Dummy_LastModifiedDate__c=DateTime.newInstance(2017, j, 25, 12, 30, 2);
            upcampmem2_2017.add(ccm);
            m++;
        }
        
        update upcampmem2_2017;
        Test.startTest();        
        MY_VoucherRedeemptionController voucherRed= new MY_VoucherRedeemptionController();
        voucherRed.SAPID ='ECP1234';
        voucherRed.phone ='123456';
        voucherRed.selectedyear = 2017;
        voucherRed.Login();
        voucherRed.LoggedInEcpId=acc.Id;
        voucherRed.initLoad();
        
        MY_VoucherRedeemptionController voucherRed2= new MY_VoucherRedeemptionController();
        voucherRed2.SAPID ='ECP12345';
        voucherRed2.phone ='123456789';
        voucherRed2.selectedyear = 2017;
        voucherRed2.Login();
        voucherRed2.LoggedInEcpId=acc.Id;
        voucherRed2.initLoad();
        voucherRed2.Logout();
        voucherRed2.getSelectBrands();
        
        MY_VoucherRedeemptionController voucherRedeem0 = new MY_VoucherRedeemptionController();
        voucherRedeem0.Name='Test2';
        voucherRedeem0.NRIC='S14642422';
        voucherRedeem0.MYCampaign=cam.Id;
        voucherRedeem0.selectedyear = 2017;
        voucherRedeem0.RedeemVoucher();
        
        MY_VoucherRedeemptionController voucherRedeem01 = new MY_VoucherRedeemptionController();
        voucherRedeem01.Name='TestContact';
        voucherRedeem01.NRIC='S1464242K';
        //voucherRedeem01.ReceiptNo='';
        voucherRedeem01.MYCampaign=cam.id;
        voucherRedeem01.selectedyear = 2017;
        voucherRedeem01.RedeemVoucher();
        
        MY_VoucherRedeemptionController voucherRedeem02 = new MY_VoucherRedeemptionController();
        voucherRedeem02.Name='TestContact2';
        voucherRedeem02.NRIC='S1464240K';
        //voucherRedeem02.ReceiptNo='';
        voucherRedeem02.MYCampaign=cam.id;
        voucherRedeem02.selectedyear = 2017;
        voucherRedeem02.RedeemVoucher();
        
        //if Voucher_Status__c =='Not Used' && AttachmentId__c ==Null && fileBody!= null          
        MY_VoucherRedeemptionController voucherRedeem2 = new MY_VoucherRedeemptionController();
        voucherRedeem2.fileName = 'Receipt';
        Blob ImageFile2 = Blob.ValueOf('Test.jpg');
        voucherRedeem2.fileBody = ImageFile2;
        voucherRedeem2.Name='Test2';
        voucherRedeem2.NRIC='S1464242L';
        voucherRedeem2.status ='Not Used';
        voucherRedeem2.showpanel =true;
        voucherRedeem2.fontcolor ='Red';
        voucherRedeem2.Box1 ='2';
        voucherRedeem2.Box2 ='3';
        voucherRedeem2.LoggedInEcpId=acc.Id;
        voucherRedeem2.MYCampaign=cam.Id;
        voucherRedeem2.selectedyear = 2017;
        voucherRedeem2.RedeemVoucher();
        voucherRedeem2.linkupdate();
        
        // if Voucher_Status__c =='Not Used' && fileBody== null && ReceiptNo != null
        MY_VoucherRedeemptionController voucherRedeem3 = new MY_VoucherRedeemptionController();
        voucherRedeem3.ContactName='TestLoop1';
        voucherRedeem3.NRIC='Test1';
        voucherRedeem3.status ='Not Used';
        voucherRedeem3.showpanel =true;
        voucherRedeem3.fontcolor ='Red';
        voucherRedeem3.Box1 ='2';
        voucherRedeem3.Box2 ='3';
        voucherRedeem3.LoggedInEcpId=acc.Id;
        voucherRedeem3.EcpId=acc.Id;     
        voucherRedeem3.MYCampaign= cam.Id;
        voucherRedeem3.selectedyear = 2017;
        voucherRedeem3.getcontactDetails();
        voucherRedeem3.RedeemVoucher();
        
        // if Voucher_Status__c =='Archived' && fileBody== null && ReceiptNo != null
        MY_VoucherRedeemptionController voucherRedeem6 = new MY_VoucherRedeemptionController();
        voucherRedeem6.Name='TestContact2';
        voucherRedeem6.NRIC='S1464240K';
        voucherRedeem6.status ='Archived';
        voucherRedeem6.showpanel =true;
        voucherRedeem6.fontcolor ='Red';
        voucherRedeem6.Box1 ='2';
        voucherRedeem6.Box2 ='3';
        voucherRedeem6.LoggedInEcpId=acc.Id;
        voucherRedeem6.MYCampaign= cam2.Id;
        voucherRedeem6.selectedyear = 2017;
        voucherRedeem6.RedeemVoucher();
        
        // if Voucher_Status__c =='Archived' && fileBody== null && ReceiptNo != null
        MY_VoucherRedeemptionController voucherRedeem5 = new MY_VoucherRedeemptionController();
        voucherRedeem5.Name='TestContact2';
        voucherRedeem5.NRIC='S1464240K';
        voucherRedeem5.status ='Archived';
        voucherRedeem5.showpanel =true;
        voucherRedeem5.fontcolor ='Red';
        voucherRedeem5.Box1 ='2';
        voucherRedeem5.Box2 ='3';
        voucherRedeem5.LoggedInEcpId=acc.Id;
        voucherRedeem5.MYCampaign= cam.Id;
        voucherRedeem5.selectedyear = 2017;
        voucherRedeem5.RedeemVoucher();
        
        
        //To cover AggregateResult in Refresh method
        MY_VoucherRedeemptionController voucherRedeem4 = new MY_VoucherRedeemptionController();
        voucherRedeem4.fileName = 'Receipt';
        Blob ImageFile4 = Blob.ValueOf('Test.jpg');
        voucherRedeem4.fileBody = ImageFile4;
        voucherRedeem4.ContactName='Testloop1';
        voucherRedeem4.NRIC='loop1';
        voucherRedeem4.status ='Not Used';
        voucherRedeem4.showpanel =true;
        voucherRedeem4.fontcolor ='Red';
        voucherRedeem4.Box1 ='2';
        voucherRedeem4.Box2 ='3';
        voucherRedeem4.LoggedInEcpId=acc.Id;
        voucherRedeem4.MYCampaign=cam.Id;
        voucherRedeem4.selectedyear = 2017;
        voucherRedeem4.Refresh();
        voucherredeem4.setselectedyear(2017);
        voucherredeem4.getyearlist();
        voucherredeem4.getselectedyear();
        voucherredeem4.setContactName(string.valueof(voucherRedeem4.ContactName));
        system.assertEquals(voucherRedeem4.selectedyear,2017);
        Test.stopTest();
    } 
}