@isTest
private class TestVoucherRedeemptionControllerTH {
    
    static Account acc;
    static Campaign cam,cam2;
    static Contact c,c1,c2,c3;
    static CampaignMember cmember1, cmember2, cmember3,cmember2017 ,cmember3_2017;
    
    private static testMethod void RedeemVoucher()
    {
        
        acc=new Account(Name='THCampaign ECP',OutletNumber__c='ECP12345',Phone='123456789');
        Insert acc;
        
        cam=new Campaign(Name='THCampaign',IsActive=true);
        Insert cam;
        
        cam2=new Campaign(Name='THCampaign2',IsActive=true);
        Insert cam2;
        
        c1 = new Contact(LastName='TestContact',NRIC__c='242K');
        Insert c1;
        c2 = new Contact(LastName='Test2',NRIC__c='242L');
        Insert c2;
        c3 = new Contact(LastName='Test3',NRIC__c='242M');
        Insert c3;
        
        cmember1=new CampaignMember(CampaignId=cam.Id,ContactId=c1.Id,Status='Subscribed',Receipt_number__c='12345',Voucher_Status__c ='Not Used',Account__c=acc.Id);
        Insert cmember1;
        system.assertEquals(cmember1.CampaignId,cam.Id,'success');
        Test.setCreatedDate(cmember1.Id, DateTime.newInstance(2016,5,5));
        
        cmember2=new CampaignMember(CampaignId=cam.Id,ContactId=c2.Id,Status='Subscribed',Receipt_number__c='01234',Voucher_Status__c ='Not Used',Account__c=acc.Id,Redeemed_Date_Time__c=DateTime.newInstance(2016, 1, 20, 12, 30, 2));
        Insert cmember2;
        
        cmember3=new CampaignMember(CampaignId=cam.Id,ContactId=c3.Id,Status='Subscribed',Receipt_number__c='01235',Voucher_Status__c ='Archived',Account__c=acc.Id,Redeemed_Date_Time__c=DateTime.newInstance(2016, 1, 25, 12, 30, 2));
        Insert cmember3;
        
        ExactTarget_Integration_Settings__c TH = new ExactTarget_Integration_Settings__c();
        TH.Name = 'THCampaign';
        TH.Value__c = cam.Id;
        insert TH; 
        
        list<contact> lstcon = new list<contact>();
        list<contact> lstcon2 = new list<contact>();
        for(integer i=1; i<=16;i++){
            
            c1 = new Contact(LastName='Testloop'+i,NRIC__c='loop'+i);
            lstcon.add(c1);
        }
        insert lstcon;
        
        for(integer i=0; i<=15;i++){
            
            c2 = new Contact(LastName='Testloop'+i,NRIC__c='loop'+i);
            lstcon2.add(c2);
        }
        insert lstcon2;
        
        list<CampaignMember> lstcampmem = new list<CampaignMember>();
        list<CampaignMember> lstcampmemArch = new list<CampaignMember>();
        for(integer i=1; i<=12;i++){
            
            cmember2=new CampaignMember(CampaignId=cam.Id,ContactId=lstcon[i].Id,Status='Subscribed',Receipt_number__c='Rec'+i,Voucher_Status__c ='Not Used',Account__c=acc.Id,
                                        No_of_Boxes1__c=i,No_of_Boxes2__c=i,No_of_Boxes3__c=i);
            lstcampmem.add(cmember2);
        }
        insert lstcampmem;
        for(integer i=0; i<lstcampmem.size();i++){
            Datetime customtime = DateTime.newInstance(2016, i, 20, 12, 30, 2);
            Test.setCreatedDate(lstcampmem[i].Id,customtime);
        }
        
        for(integer i=1; i<=12;i++){
            
            cmember3=new CampaignMember(CampaignId=cam.Id,ContactId=lstcon2[i].Id,Status='Subscribed',Receipt_number__c='Rec'+i,Voucher_Status__c ='Not used',Account__c=acc.Id,
                                        No_of_Boxes1__c=i+1,No_of_Boxes2__c=i+1,No_of_Boxes3__c=i+1);
            lstcampmemArch.add(cmember3);
        }
        insert lstcampmemArch ;
        for(integer i=0; i<lstcampmemArch.size();i++){
            Datetime customtime = DateTime.newInstance(2016, i, 20, 12, 30, 2);
            Test.setCreatedDate(lstcampmemArch[i].Id,customtime);
        }
        list<CampaignMember> upcampmem = new list<CampaignMember>();
        list<CampaignMember> upcampmem2 = new list<CampaignMember>();
        integer i=5;
        
        for(CampaignMember ccm:lstcampmem){   
            ccm.Voucher_Status__c ='Redeemed';
            ccm.Redeemed_Date_Time__c=DateTime.newInstance(2016, i, 25, 12, 30, 2);
            upcampmem.add(ccm);
            i++;
        }
        
        update upcampmem;
        integer j=5;
        for(CampaignMember ccm: lstcampmemArch){   
            ccm.Voucher_Status__c ='Archived';
            ccm.Redeemed_Date_Time__c=DateTime.newInstance(2016, j, 25, 12, 30, 2);
            upcampmem2.add(ccm);
            j++;
        }
        
        update upcampmem2;
        
        list<contact> con2017 = new list<contact>();
        list<contact> con2_2017 = new list<contact>();
        for(integer k=1; k<=16;k++){
            
            c1 = new Contact(LastName='Testloop'+k,NRIC__c='loop'+k);
            con2017.add(c1);
        }
        insert con2017;
        for(integer k=0; k<=15;k++){
            
            c2 = new Contact(LastName='Testloop'+k,NRIC__c='loop'+k);
            con2_2017.add(c2);
        }
        insert con2_2017;
        list<CampaignMember> campmem2017 = new list<CampaignMember>();
        list<CampaignMember> campmem2017Arch = new list<CampaignMember>();
        for(integer k=1; k<=12;k++){
            
            cmember2017=new CampaignMember(CampaignId=cam.Id,ContactId=con2017[k].Id,Status='Subscribed',Receipt_number__c='Rec'+k,Voucher_Status__c ='Not Used',Account__c=acc.Id,
                                           No_of_Boxes1__c=k,No_of_Boxes2__c=k,No_of_Boxes3__c=k,No_of_Boxes4__c=k,No_of_Boxes5__c=k,No_of_Boxes6__c=k);
            campmem2017.add(cmember2017);
        }
        insert campmem2017;
        for(integer k=0; k<campmem2017.size();k++){
            Datetime customtime = DateTime.newInstance(2016, k, 20, 12, 30, 2);
            Test.setCreatedDate(campmem2017[k].Id,customtime);
        }
        for(integer k=1; k<12;k++){
            
            cmember3_2017=new CampaignMember(CampaignId=cam.Id,ContactId=con2_2017[k].Id,Status='Subscribed',Receipt_number__c='Rec'+k,Voucher_Status__c ='Not used',Account__c=acc.Id,
                                             No_of_Boxes1__c=k+1,No_of_Boxes2__c=k+1,No_of_Boxes3__c=k+1,No_of_Boxes4__c=k+1,No_of_Boxes5__c=k+1,No_of_Boxes6__c=k+1);
            campmem2017Arch.add(cmember3_2017);
        }
        insert campmem2017Arch ;
        for(integer k=0; k<campmem2017Arch.size();k++){
            Datetime customtime = DateTime.newInstance(2016, k, 20, 12, 30, 2);
            Test.setCreatedDate(campmem2017Arch[k].Id,customtime);
        }
        system.debug('CampaingMember' + campmem2017Arch);
        for(CampaignMember item: campmem2017Arch){
            system.debug('UsereVoucherCode' + item.User_eVoucher_Code__c);
        }
        list<CampaignMember> upcampmem_2017 = new list<CampaignMember>();
        list<CampaignMember> upcampmem2_2017 = new list<CampaignMember>();
        integer l=5;
        
        for(CampaignMember ccm:campmem2017){
            ccm.Voucher_Status__c ='Redeemed';
            ccm.Redeemed_Date_Time__c=DateTime.newInstance(2017, l, 20, 12, 30, 2);
            upcampmem_2017.add(ccm);
            l++;
        }
        
        update upcampmem_2017;
        integer m=5;
        for(CampaignMember ccm: campmem2017Arch){
            ccm.Voucher_Status__c ='Archived';
            ccm.Redeemed_Date_Time__c=DateTime.newInstance(2017, j, 25, 12, 30, 2);
            upcampmem2_2017.add(ccm);
            m++;
        }
        update upcampmem2_2017;
        
        Test.startTest();
        TH_VoucherRedeemptionController voucherRed= new TH_VoucherRedeemptionController();
        voucherRed.SAPID ='ECP1234';
        voucherRed.phone ='123456';
        voucherRed.Login();
        voucherRed.LoggedInEcpId=acc.Id;
        voucherRed.eVoucher = 'ECPlo';
        voucherRed.initLoad();
        voucherRed.ValidateCode();
        
        TH_VoucherRedeemptionController voucherRed2= new TH_VoucherRedeemptionController();
        voucherRed2.SAPID ='ECP12345';
        voucherRed2.phone ='123456789';
        voucherRed2.Login();
        voucherRed2.LoggedInEcpId=acc.Id;
        voucherRed2.eVoucher = 'ECPlo';
        voucherRed2.initLoad();
        voucherRed2.Logout();
        voucherRed2.getSelectBrands();
        voucherRed2.ValidateCode();
        
        TH_VoucherRedeemptionController voucherRedeem0 = new TH_VoucherRedeemptionController();
        voucherRedeem0.Name='Test2';
        voucherRedeem0.NRIC='S14642422';
        voucherRedeem0.THCampaign=cam.Id;
        voucherRedeem0.eVoucher = 'ECPlo';
        voucherRedeem0.RedeemVoucher();
        voucherRedeem0.ValidateCode();
        
        TH_VoucherRedeemptionController voucherRedeem01 = new TH_VoucherRedeemptionController();
        voucherRedeem01.Name='TestContact';
        voucherRedeem01.NRIC='S1464242K';
        voucherRedeem01.ReceiptNo='';
        voucherRedeem01.THCampaign=cam.id;
        voucherRedeem01.RedeemVoucher();
        voucherRedeem01.eVoucher = 'ECPlo';
        voucherRedeem01.ValidateCode();
        
        TH_VoucherRedeemptionController voucherRedeem02 = new TH_VoucherRedeemptionController();
        voucherRedeem02.Name='TestContact2';
        voucherRedeem02.NRIC='S1464240K';
        voucherRedeem02.ReceiptNo='';
        voucherRedeem02.THCampaign=cam.id;
        voucherRedeem02.RedeemVoucher();
        voucherRedeem02.eVoucher = 'ECPlo';  
        voucherRedeem02.ValidateCode();
        
        TH_VoucherRedeemptionController voucherRedeem2 = new TH_VoucherRedeemptionController();
        voucherRedeem2.fileName = 'Receipt';
        Blob ImageFile2 = Blob.ValueOf('Test.jpg');
        voucherRedeem2.fileBody = ImageFile2;
        voucherRedeem2.Name='Test2';
        voucherRedeem2.NRIC='S1464242L';
        voucherRedeem2.status ='Not Used';
        voucherRedeem2.showpanel =true;
        voucherRedeem2.fontcolor ='Red';
        voucherRedeem2.Box1 ='2';
        voucherRedeem2.Box2 ='3';
        voucherRedeem2.Box3 ='4';
        voucherRedeem2.ReceiptNo='';
        voucherRedeem2.LoggedInEcpId=acc.Id;
        voucherRedeem2.THCampaign=cam.Id;
        voucherRedeem2.eVoucher = 'ECP24';
        voucherRedeem2.RedeemVoucher();
        voucherRedeem2.ValidateCode();
        
        TH_VoucherRedeemptionController voucherRedeem3 = new TH_VoucherRedeemptionController();
        voucherRedeem3.Name='TestContact';
        voucherRedeem3.NRIC='S1464242k';
        voucherRedeem3.status ='Not Used';
        voucherRedeem3.showpanel =true;
        voucherRedeem3.fontcolor ='Red';
        voucherRedeem3.Box1 ='2';
        voucherRedeem3.Box2 ='3';
        voucherRedeem3.Box3 ='4';
        voucherRedeem3.ReceiptNo = '123456';
        voucherRedeem3.LoggedInEcpId=acc.Id;
        voucherRedeem3.THCampaign= cam.Id;
        voucherRedeem3.eVoucher = 'ECP24';
        voucherRedeem3.RedeemVoucher();
        voucherRedeem3.ValidateCode();
        
        TH_VoucherRedeemptionController voucherRedeem6 = new TH_VoucherRedeemptionController();
        voucherRedeem6.Name='TestContact2';
        voucherRedeem6.NRIC='S1464240K';
        voucherRedeem6.status ='Archived';
        voucherRedeem6.showpanel =true;
        voucherRedeem6.fontcolor ='Red';
        voucherRedeem6.Box1 ='2';
        voucherRedeem6.Box2 ='3';
        voucherRedeem6.Box3 ='4';
        voucherRedeem6.ReceiptNo='Rec25';
        voucherRedeem6.LoggedInEcpId=acc.Id;
        voucherRedeem6.THCampaign= cam.Id;
        voucherRedeem6.eVoucher = 'ECP24';
        voucherRedeem6.lstCampMember = lstcampmem;
        voucherRedeem6.RedeemVoucher();
        voucherRedeem6.ValidateCode();
        
        TH_VoucherRedeemptionController voucherRedeem5 = new TH_VoucherRedeemptionController();
        voucherRedeem5.Name='TestContact2';
        voucherRedeem5.NRIC='S1464240K';
        voucherRedeem5.status ='Archived';
        voucherRedeem5.showpanel =true;
        voucherRedeem5.fontcolor ='Red';
        voucherRedeem5.Box1 ='2';
        voucherRedeem5.Box2 ='3';
        voucherRedeem5.Box3 ='4';
        voucherRedeem5.ReceiptNo='Rec25';
        voucherRedeem5.LoggedInEcpId=acc.Id;
        voucherRedeem5.THCampaign= cam.Id;
        voucherRedeem5.eVoucher = 'ECP24';
        voucherRedeem5.RedeemVoucher();
        voucherRedeem5.ValidateCode();
        
        //To cover AggregateResult in Refresh method
        TH_VoucherRedeemptionController voucherRedeem4 = new TH_VoucherRedeemptionController();
        voucherRedeem4.fileName = 'Receipt';
        Blob ImageFile4 = Blob.ValueOf('Test.jpg');
        voucherRedeem4.fileBody = ImageFile4;
        voucherRedeem4.Name='Testloop5';
        voucherRedeem4.NRIC='S1464oop5';
        voucherRedeem4.status ='Not Used';
        voucherRedeem4.showpanel =true;
        voucherRedeem4.fontcolor ='Red';
        voucherRedeem4.Box1 ='2';
        voucherRedeem4.Box2 ='3';
        voucherRedeem4.Box3 ='4';
        voucherRedeem4.ReceiptNo='12345';
        voucherRedeem4.LoggedInEcpId=acc.Id;
        voucherRedeem4.THCampaign=cam.Id;
        voucherRedeem4.RedeemVoucher();
        voucherRedeem4.refresh();
        Test.stopTest();       
    } 
    
    static Account acc1;
    static Campaign cam3,cam4;
    static Contact c4,c5,c6,c7;
    static CampaignMember cmember4, cmember5, cmember6,cmember2018 ,cmember3_2018 ;
    
    private static testMethod void Redeem2ndVoucher()
    {
        acc1=new Account(Name='THCampaign ECP',OutletNumber__c='ECP12345',Phone='123456789');
        Insert acc1;
        
        cam3=new Campaign(Name='THCampaign',IsActive=true);
        Insert cam3;
        
        cam4=new Campaign(Name='THCampaign2',IsActive=true);
        Insert cam4;
        
        c4 = new Contact(LastName='TestContact',NRIC__c='242K');
        Insert c4;
        c5 = new Contact(LastName='Test2',NRIC__c='242L');
        Insert c5;
        c6 = new Contact(LastName='Test3',NRIC__c='242M');
        Insert c6;
        
        cmember4=new CampaignMember(CampaignId=cam3.Id,ContactId=c4.Id,Status='Subscribed',Receipt_number__c='12345',Voucher_Status__c ='Not Used',Account__c=acc1.Id);
        Insert cmember4;
        system.assertEquals(cmember4.CampaignId,cam3.Id,'success');
        Test.setCreatedDate(cmember4.Id, DateTime.newInstance(2016,5,5));
        
        cmember5=new CampaignMember(CampaignId=cam3.Id,ContactId=c5.Id,Status='Subscribed',Receipt_number__c='01234',Voucher_Status__c ='Not Used',Account__c=acc1.Id,Redeemed_Date_Time__c=DateTime.newInstance(2016, 1, 20, 12, 30, 2));
        Insert cmember5;
        
        cmember6=new CampaignMember(CampaignId=cam3.Id,ContactId=c6.Id,Status='Subscribed',Receipt_number__c='01235',Voucher_Status__c ='Archived',Account__c=acc1.Id,Redeemed_Date_Time__c=DateTime.newInstance(2016, 1, 25, 12, 30, 2));
        Insert cmember6;
        
        ExactTarget_Integration_Settings__c TH2 = new ExactTarget_Integration_Settings__c();
        TH2.Name = 'THCampaignII';
        TH2.Value__c = cam3.Id;
        insert TH2; 
        
        list<contact> lstcon = new list<contact>();
        list<contact> lstcon2 = new list<contact>();
        for(integer i=1; i<=16;i++){
            
            c4 = new Contact(LastName='Testloop'+i,NRIC__c='loop'+i);
            lstcon.add(c4);
        }
        insert lstcon;
        
        for(integer i=0; i<=15;i++){
            
            c5 = new Contact(LastName='Testloop'+i,NRIC__c='loop'+i);
            lstcon2.add(c5);
        }
        insert lstcon2;
        list<CampaignMember> lstcampmem = new list<CampaignMember>();
        list<CampaignMember> lstcampmemArch = new list<CampaignMember>();
        for(integer i=1; i<=12;i++){
            
            cmember5=new CampaignMember(CampaignId=cam3.Id,ContactId=lstcon[i].Id,Status='Subscribed',Receipt_number__c='Rec'+i,Voucher_Status__c ='Not Used',Account__c=acc1.Id,
                                        No_of_Boxes1__c=i,No_of_Boxes2__c=i,No_of_Boxes3__c=i);
            lstcampmem.add(cmember5);
        }
        insert lstcampmem ;
        
        for(integer i=0; i<lstcampmem.size();i++){
            Datetime customtime = DateTime.newInstance(2016, i, 20, 12, 30, 2);
            Test.setCreatedDate(lstcampmem[i].Id,customtime);
        }
        
        for(integer i=1; i<=12;i++){
            
            cmember6=new CampaignMember(CampaignId=cam3.Id,ContactId=lstcon2[i].Id,Status='Subscribed',Receipt_number__c='Rec'+i,Voucher_Status__c ='Not used',Account__c=acc1.Id,
                                        No_of_Boxes1__c=i+1,No_of_Boxes2__c=i+1,No_of_Boxes3__c=i+1);
            lstcampmemArch.add(cmember6);
        }
        insert lstcampmemArch ;
        
        for(integer i=0; i<lstcampmemArch.size();i++){
            Datetime customtime = DateTime.newInstance(2016, i, 20, 12, 30, 2);
            Test.setCreatedDate(lstcampmemArch[i].Id,customtime);
        }
        
        list<CampaignMember> upcampmem = new list<CampaignMember>();
        list<CampaignMember> upcampmem2 = new list<CampaignMember>();
        integer i=5;
        
        for(CampaignMember ccm:lstcampmem){
            
            ccm.Voucher_Status__c ='Redeemed';
            ccm.Redeemed_Date_Time__c=DateTime.newInstance(2016, i, 20, 12, 30, 2);
            upcampmem.add(ccm);
            i++;
        }
        
        update upcampmem;
        integer j=5;
        for(CampaignMember ccm: lstcampmemArch){
            
            ccm.Voucher_Status__c ='Archived';
            ccm.Redeemed_Date_Time__c=DateTime.newInstance(2016, j, 25, 12, 30, 2);
            upcampmem2.add(ccm);
            j++;
        }
        
        update upcampmem2;
        list<contact> con2017 = new list<contact>();
        list<contact> con2_2017 = new list<contact>();
        for(integer k=1; k<=16;k++){
            
            c4 = new Contact(LastName='Testloop'+k,NRIC__c='loop'+k);
            con2017.add(c4);
        }
        insert con2017;
        
        for(integer k=0; k<=15;k++){
            
            c5 = new Contact(LastName='Testloop'+k,NRIC__c='loop'+k);
            con2_2017.add(c5);
        }
        insert con2_2017;
        
        list<CampaignMember> campmem2017 = new list<CampaignMember>();
        list<CampaignMember> campmem2017Arch = new list<CampaignMember>();
        for(integer k=1; k<=12;k++){
            
            cmember2017=new CampaignMember(CampaignId=cam3.Id,ContactId=con2017[k].Id,Status='Subscribed',Receipt_number__c='Rec'+k,Voucher_Status__c ='Not Used',Account__c=acc1.Id,
                                           No_of_Boxes1__c=k,No_of_Boxes2__c=k,No_of_Boxes3__c=k,No_of_Boxes4__c=k,No_of_Boxes5__c=k,No_of_Boxes6__c=k);
            campmem2017.add(cmember2017);
        }
        insert campmem2017 ;
        
        for(integer k=0; k<campmem2017.size();k++){
            Datetime customtime = DateTime.newInstance(2016, k, 20, 12, 30, 2);
            Test.setCreatedDate(campmem2017[k].Id,customtime);
        }
        for(integer k=1; k<12;k++){
            
            cmember3_2017=new CampaignMember(CampaignId=cam3.Id,ContactId=con2_2017[k].Id,Status='Subscribed',Receipt_number__c='Rec'+k,Voucher_Status__c ='Not used',Account__c=acc1.Id,
                                             No_of_Boxes1__c=k+1,No_of_Boxes2__c=k+1,No_of_Boxes3__c=k+1,No_of_Boxes4__c=k+1,No_of_Boxes5__c=k+1,No_of_Boxes6__c=k+1);
            campmem2017Arch.add(cmember3_2017);
        }
        insert campmem2017Arch ;
        
        for(integer k=0; k<campmem2017Arch.size();k++){
            Datetime customtime = DateTime.newInstance(2016, k, 20, 12, 30, 2);
            Test.setCreatedDate(campmem2017Arch[k].Id,customtime);
        }
        
        list<CampaignMember> upcampmem_2017 = new list<CampaignMember>();
        list<CampaignMember> upcampmem2_2017 = new list<CampaignMember>();
        integer l=5;
        
        for(CampaignMember ccm:campmem2017){
            
            ccm.Voucher_Status__c ='Redeemed';
            ccm.Redeemed_Date_Time__c=DateTime.newInstance(2017, l, 20, 12, 30, 2);
            upcampmem_2017.add(ccm);
            l++;
        }
        
        update upcampmem_2017;
        integer m=5;
        for(CampaignMember ccm: campmem2017Arch){
            
            ccm.Voucher_Status__c ='Archived';
            ccm.Redeemed_Date_Time__c=DateTime.newInstance(2017, j, 25, 12, 30, 2);
            upcampmem2_2017.add(ccm);
            m++;
        }
        
        update upcampmem2_2017;
        
        Test.startTest();
        TH_VoucherRedeemptionController voucherRed= new TH_VoucherRedeemptionController();
        voucherRed.SAPID ='ECP1234';
        voucherRed.phone ='123456';
        voucherRed.Login();
        voucherRed.LoggedInEcpId=acc1.Id;
        voucherRed.initLoad();
        voucherRed.Validate2ndCode();
        voucherRed.RedeemVoucher();
        voucherRed.Redeem2ndVoucher();
        
        TH_VoucherRedeemptionController voucherRed2= new TH_VoucherRedeemptionController();
        voucherRed2.SAPID ='ECP12345';
        voucherRed2.phone ='123456789';
        voucherRed2.Login();
        voucherRed2.LoggedInEcpId=acc1.Id;
        voucherRed2.eVoucher = 'ECPlo';
        voucherRed2.initLoad();
        voucherRed2.RedeemVoucher();
        voucherRed2.Redeem2ndVoucher();
        voucherRed2.Logout();
        voucherRed2.getSelectBrands();
        
        TH_VoucherRedeemptionController voucherRedeem0 = new TH_VoucherRedeemptionController();
        voucherRedeem0.Name='Test2';
        voucherRedeem0.NRIC='S14642422';
        voucherRedeem0.THCampaignII=cam3.Id;
        voucherRedeem0.Redeem2ndVoucher();
        
        TH_VoucherRedeemptionController voucherRedeem01 = new TH_VoucherRedeemptionController();
        voucherRedeem01.Name='TestContact';
        voucherRedeem01.NRIC='S1464242K';
        voucherRedeem01.ReceiptNo='';
        voucherRedeem01.THCampaignII=cam3.id;
        voucherRedeem01.eVoucher = 'ECPlo';
        voucherRedeem01.Redeem2ndVoucher();
        
        TH_VoucherRedeemptionController voucherRedeem02 = new TH_VoucherRedeemptionController();
        voucherRedeem02.Name='TestContact2';
        voucherRedeem02.NRIC='S1464240K';
        voucherRedeem02.ReceiptNo='';
        voucherRedeem02.THCampaignII=cam3.id;
        voucherRedeem02.eVoucher = 'ECPlo';
        voucherRedeem02.Redeem2ndVoucher();
        
        TH_VoucherRedeemptionController voucherRedeem2 = new TH_VoucherRedeemptionController();
        voucherRedeem2.fileName = 'Receipt';
        Blob ImageFile2 = Blob.ValueOf('Test.jpg');
        voucherRedeem2.fileBody = ImageFile2;
        voucherRedeem2.Name='Test2';
        voucherRedeem2.NRIC='S1464242L';
        voucherRedeem2.status ='Not Used';
        voucherRedeem2.showpanel =true;
        voucherRedeem2.fontcolor ='Red';
        voucherRedeem2.Box1 ='2';
        voucherRedeem2.Box2 ='3';
        voucherRedeem2.Box3 ='4';
        voucherRedeem2.ReceiptNo='';
        voucherRedeem2.LoggedInEcpId=acc1.Id;
        voucherRedeem2.THCampaignII=cam3.Id;
        voucherRedeem2.eVoucher = 'ECPlo';
        voucherRedeem2.Redeem2ndVoucher();
        
        TH_VoucherRedeemptionController voucherRedeem3 = new TH_VoucherRedeemptionController();
        voucherRedeem3.Name='TestContact';
        voucherRedeem3.NRIC='S1464242k';
        voucherRedeem3.status ='Not Used';
        voucherRedeem3.showpanel =true;
        voucherRedeem3.fontcolor ='Red';
        voucherRedeem3.Box1 ='2';
        voucherRedeem3.Box2 ='3';
        voucherRedeem3.Box3 ='4';
        voucherRedeem3.LoggedInEcpId=acc1.Id;
        voucherRedeem3.THCampaignII= cam3.Id;
        voucherRedeem3.eVoucher = 'ECPlo';
        voucherRedeem3.Redeem2ndVoucher();
        
        TH_VoucherRedeemptionController voucherRedeem6 = new TH_VoucherRedeemptionController();
        voucherRedeem6.Name='TestContact2';
        voucherRedeem6.NRIC='S1464240K';
        voucherRedeem6.status ='Archived';
        voucherRedeem6.showpanel =true;
        voucherRedeem6.fontcolor ='Red';
        voucherRedeem6.Box1 ='2';
        voucherRedeem6.Box2 ='3';
        voucherRedeem6.Box3 ='4';
        voucherRedeem6.ReceiptNo='Rec25';
        voucherRedeem6.LoggedInEcpId=acc1.Id;
        voucherRedeem6.THCampaignII= cam4.Id;
        voucherRedeem6.eVoucher = 'ECPlo';
        voucherRedeem6.Redeem2ndVoucher();
        
        TH_VoucherRedeemptionController voucherRedeem5 = new TH_VoucherRedeemptionController();
        voucherRedeem5.Name='TestContact2';
        voucherRedeem5.NRIC='S1464240K';
        voucherRedeem5.status ='Archived';
        voucherRedeem5.showpanel =true;
        voucherRedeem5.fontcolor ='Red';
        voucherRedeem5.Box1 ='2';
        voucherRedeem5.Box2 ='3';
        voucherRedeem5.Box3 ='4';
        voucherRedeem5.ReceiptNo='Rec25';
        voucherRedeem5.LoggedInEcpId=acc1.Id;
        voucherRedeem5.THCampaignII= cam3.Id;
        voucherRedeem5.eVoucher = 'ECPlo';
        voucherRedeem5.Redeem2ndVoucher();
        voucherRedeem5.Validate2ndCode();
        
        //To cover AggregateResult in Refresh method
        TH_VoucherRedeemptionController voucherRedeem4 = new TH_VoucherRedeemptionController();
        voucherRedeem4.fileName = 'Receipt';
        Blob ImageFile4 = Blob.ValueOf('Test.jpg');
        voucherRedeem4.fileBody = ImageFile4;
        voucherRedeem4.Name='Testloop5';
        voucherRedeem4.NRIC='S1464oop5';
        voucherRedeem4.status ='Not Used';
        voucherRedeem4.showpanel =true;
        voucherRedeem4.fontcolor ='Red';
        voucherRedeem4.Box1 ='2';
        voucherRedeem4.Box2 ='3';
        voucherRedeem4.Box3 ='4';
        voucherRedeem4.ReceiptNo='';
        voucherRedeem4.LoggedInEcpId=acc1.Id;
        voucherRedeem4.THCampaignII=cam3.Id;
        voucherRedeem4.eVoucher = 'ECPlo';
        voucherRedeem4.Refresh2nd();
        voucherRedeem4.setwearer('Existing Wearer');
        voucherRedeem4.getItems();
        Test.stopTest();        
    }
}