@istest
public class Test_CustomLookupHelper_TVA {
    
    Public Static Testmethod void recallApprovalpermasgnTest()
    {
        Event evt=new Event(Subject='Test',DurationInMinutes=60,RecurrenceInterval = 1,RecurrenceType = 'RecursDaily',RecurrenceEndDateOnly = System.today()+30,RecurrenceStartDateTime = System.today(),IsRecurrence = true,ActivityDateTime=datetime.newInstance(2014, 9, 15, 12, 30, 0));
        insert evt;
        Contact c1 = new Contact(LastName='TestContact',NRIC__c='242K');
        Insert c1;
        Contact c2 = new Contact(LastName='TestContact1',NRIC__c='242K');
        Insert c2;
        
        List<id>con=new List<id>();
        con.add(c1.id);
        con.add(c2.id);
        EventRelation er=new EventRelation(EventId=evt.id,RelationId=c1.id);
        insert er;
        
        test.startTest();
        
        Campaign campNew= new Campaign (Name='Test');
        insert campNew;
        Account acc=new Account(Name='Test',OutletNumber__c='123456',Phone='123456');
        insert  acc;
        
        List<String> lookup=new List<String>();
        lookup.add(String.valueof(acc.id));
        
        ExactTarget_Integration_Settings__c exTcam=new ExactTarget_Integration_Settings__c(name='MYCampaign',Value__c=campNew.id);
        insert exTcam;
        
        Contact newCon=new Contact (LastName='test',MA2_Country_Code__c='SGP',MembershipNo__c='Test');
        insert newCon;
        Lead l = new Lead(lastname='11', company='11');
        insert l;
        
        
        CampaignMember campMem=new CampaignMember(CampaignId= campNew.id,leadid=l.id ,ContactID=newCon.id,Account__c=acc.id,Voucher_Status__c='Not Used',Dummy_LastModifiedDate__c=datetime.newInstance(2018, 11, 11, 12, 30, 0),No_of_Boxes1__c=11 );
        insert campMem; 
        Event_RecordType_Setting__c evtrs=new Event_RecordType_Setting__c(Composite_Key__c='Test',Field_Api__c='Subject',Record_Type__c='Call JPN',Default_Value__c='Other',Values__c='Other,Email,Call');
        insert evtrs;
        String search= CustomLookupHelper_TVA.searchDB('Event_RecordType_Setting__c','Default_Value__c',evtrs.Field_Api__c,4, evtrs.Default_Value__c,'Other',lookup,'false',evtrs.Record_Type__c,'payer');
        CustomLookupHelper_TVA.searchDB('Event_RecordType_Setting__c','Default_Value__c',evtrs.Field_Api__c,4, evtrs.Default_Value__c,'Other',lookup,'true',evtrs.Record_Type__c,'payer');
        test.stopTest();
        System.assertEquals(search, search);
    }
    
    Public Static Testmethod void recallApprovalpermasgnTest1()
    {
        Event evt=new Event(Subject='Test',DurationInMinutes=60,RecurrenceInterval = 1,RecurrenceType = 'RecursDaily',RecurrenceEndDateOnly = System.today()+30,RecurrenceStartDateTime = System.today(),IsRecurrence = true,ActivityDateTime=datetime.newInstance(2014, 9, 15, 12, 30, 0));
        insert evt;
        Contact c1 = new Contact(LastName='TestContact',NRIC__c='242K');
        Insert c1;
        Contact c2 = new Contact(LastName='TestContact1',NRIC__c='242K');
        Insert c2;
        
        List<id>con=new List<id>();
        con.add(c1.id);
        con.add(c2.id);
        EventRelation er=new EventRelation(EventId=evt.id,RelationId=c1.id);
        insert er;
        
        test.startTest();
        
        Campaign campNew= new Campaign (Name='Test');
        insert campNew;
        Account acc=new Account(Name='Test',OutletNumber__c='123456',Phone='123456');
        insert  acc;
        
        List<String> lookup=new List<String>();
        lookup.add(String.valueof(acc.id));
        
        ExactTarget_Integration_Settings__c exTcam=new ExactTarget_Integration_Settings__c(name='MYCampaign',Value__c=campNew.id);
        insert exTcam;
        
        Contact newCon=new Contact (LastName='test',MA2_Country_Code__c='SGP',MembershipNo__c='Test');
        insert newCon;
        Lead l = new Lead(lastname='11', company='11');
        insert l;
        
        CampaignMember campMem=new CampaignMember(CampaignId= campNew.id,leadid=l.id ,ContactID=newCon.id,Account__c=acc.id,Voucher_Status__c='Not Used',Dummy_LastModifiedDate__c=datetime.newInstance(2018, 11, 11, 12, 30, 0),No_of_Boxes1__c=11 );
        insert campMem; 
        Event_RecordType_Setting__c evtrs=new Event_RecordType_Setting__c(Composite_Key__c='Test',Field_Api__c='Subject',Record_Type__c='Call JPN',Default_Value__c='Other',Values__c='Email,Call', SW_Language__c = 'en_US');
        insert evtrs; 
        
        String search = CustomLookupHelper_TVA.searchDB('Event_RecordType_Setting__c','Values__c','Subject',1,'Values__c', '',lookup,'true','Call JPN','');
        CustomLookupHelper_TVA.searchDB('Account','Name','Id',1,'Name', '',lookup,'true',String.valueof(acc.id),'');
        CustomLookupHelper_TVA.searchDB('Account','Name','Id',1,'Name', '',lookup,'true',String.valueof(acc.id),'payer');
        CustomLookupHelper_TVA.searchDB('CampaignMember','Name','Id',1,'Name', '',lookup,'true',String.valueof(acc.id),'');
        
        CustomLookupHelper_TVA.searchDB('Account','Name','Id',1,'Name', '',lookup,'false',String.valueof(acc.id),'');
      	CustomLookupHelper_TVA.searchDB('Account','Name','Id',1,'Name', '',lookup,'false',String.valueof(acc.id),'payer');
        CustomLookupHelper_TVA.searchDB('CampaignMember','Name','Id',1,'Name', '',lookup,'false',String.valueof(acc.id),'');
        CustomLookupHelper_TVA.searchDB('Event_RecordType_Setting__c','Values__c','Subject',1,'Values__c', '',lookup,'false','Call JPN','');
        
        CustomLookupHelper_TVA.ResultWrapper wrp=new  CustomLookupHelper_TVA.ResultWrapper();
        wrp.objName='Account';
        wrp.text='Name';
        wrp.val=String.valueof(acc.id);
        wrp.crDate=datetime.newInstance(2018, 11, 11, 12, 30, 0);
        test.stopTest();
        System.assertEquals(wrp.objName, wrp.objName);
        System.assertEquals(wrp.text, wrp.text);
        System.assertEquals(search, search);
    }
    
}