@isTest
public class Test_CustomMultiSelectLookup_TVA {

    Public Static Testmethod void recallApprovalpermasgnTest1()
    {
        Event evt=new Event(Subject='Test',DurationInMinutes=60,RecurrenceInterval = 1,RecurrenceType = 'RecursDaily',RecurrenceEndDateOnly = System.today()+30,RecurrenceStartDateTime = System.today(),IsRecurrence = true,ActivityDateTime=datetime.newInstance(2014, 9, 15, 12, 30, 0));
        insert evt;
        Contact c1 = new Contact(LastName='TestContact',NRIC__c='242K');
        Insert c1;
        Contact c2 = new Contact(LastName='TestContact1',NRIC__c='242K');
        Insert c2;
     
        List<id>con=new List<id>();
        con.add(c1.id);
        con.add(c2.id);
        EventRelation er=new EventRelation(EventId=evt.id,RelationId=c1.id);
        insert er;
        
        List<String> lookup=new List<String>();
        lookup.add('test');
        
        test.startTest();
       
       String Result1= CustomMultiSelectLookup_TVA.getUITheme();
        system.assertEquals('Theme3', Result1);
       String Result2=CustomMultiSelectLookup_TVA.saveEvent(evt.id,'[]');
        system.assertEquals('[]', Result2);
         CustomMultiSelectLookup_TVA.deleteEvent(evt.id,String.valueof(c1.id),String.valueof(er.id));
        String Result3=CustomMultiSelectLookup_TVA.searchDB('user','Name','Test',1,'Name', 'test',lookup,String.valueof(evt.id));
        system.assertEquals(Result3, Result3);
         String Result4=CustomMultiSelectLookup_TVA.searchDB('user2','Name','Test',1,'Name', '',lookup,String.valueof(evt.id));
         system.assertEquals(Result4, Result4);
        String Result5=CustomMultiSelectLookup_TVA.searchDB('user','Name','Test',1,'Name', '',lookup,String.valueof(evt.id));
         system.assertEquals(Result5, Result5);
        test.stopTest();
    }
    
}