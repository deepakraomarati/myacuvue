@isTest()
public  class Test_SW_ANZ_VoucherRedemptionStatusCntrl
{
    static testmethod void doinsertoptidlst ()
    {   
        Campaign campNew= new Campaign (Name='Test');
        insert campNew;        
        ExactTarget_Integration_Settings__c tokenCustomSetting = new ExactTarget_Integration_Settings__c();         
        tokenCustomSetting.Name = 'ANZCampaign';        
        tokenCustomSetting.Value__c = campNew.id;      
        insert tokenCustomSetting;
        
        Account acc=new Account(Name='Test',OutletNumber__c='123456',Phone='123456');
        insert  acc;  
        Contact newCon=new Contact (LastName='test',MA2_Country_Code__c='SGP',MembershipNo__c='Test');
        insert newCon;
        Lead l = new Lead(lastname='11', company='11');
        insert l;
        
        CampaignMember campMem=new CampaignMember(CampaignId= campNew.id,leadid=l.id ,ContactID=newCon.id,Account__c=acc.id,Voucher_Status__c='Archived',Dummy_LastModifiedDate__c=datetime.newInstance(2018, 1, 1, 12, 30, 0));
        insert campMem;
        system.assertEquals(campMem.CampaignId,campNew.id,'success');
        Test.setCreatedDate(campMem.id, Date.newInstance(Date.today().year(),1,1));
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 1, 1, 12, 30, 0);
        campMem.Voucher_Status__c='Archived';
        campMem.No_of_Boxes1__c=1;
        campMem.No_of_Boxes2__c=1;
        campMem.No_of_Boxes3__c=1;
        campMem.No_of_Boxes4__c=1;
        update campMem;
       
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 2, 2, 12, 30, 0);
        campMem.Voucher_Status__c='Archived';
        campMem.No_of_Boxes1__c=2;
        campMem.No_of_Boxes2__c=2;
        campMem.No_of_Boxes3__c=2;
        campMem.No_of_Boxes4__c=2;
        update campMem;
        
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);   
        
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 3, 3, 12, 30, 0);
        campMem.Voucher_Status__c='Archived';
        campMem.No_of_Boxes1__c=3;
        campMem.No_of_Boxes2__c=3;
        campMem.No_of_Boxes3__c=3;
        campMem.No_of_Boxes4__c=3;
        update campMem;
        
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 4, 4, 12, 30, 0);
        campMem.Voucher_Status__c='Archived';
        campMem.No_of_Boxes1__c=4;
        campMem.No_of_Boxes2__c=4;
        campMem.No_of_Boxes3__c=4;
        campMem.No_of_Boxes4__c=4;
        update campMem;
        
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 5, 5, 12, 30, 0);
        campMem.Voucher_Status__c='Archived';
        campMem.No_of_Boxes1__c=5;
        campMem.No_of_Boxes2__c=5;
        campMem.No_of_Boxes3__c=5;
        campMem.No_of_Boxes4__c=5;
        update campMem;
        
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.resultWrapper wrp1=new SW_ANZ_VoucherRedemptionStatusCntrl_LEX.resultWrapper('Test','Test');
        
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 6, 6, 12, 30, 0);
        campMem.No_of_Boxes1__c=6;
        campMem.No_of_Boxes2__c=6;
        campMem.No_of_Boxes3__c=6;
        campMem.No_of_Boxes4__c=6;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 7, 7, 12, 30, 0);
        campMem.No_of_Boxes1__c=7;
        campMem.No_of_Boxes2__c=7;
        campMem.No_of_Boxes3__c=7;
        campMem.No_of_Boxes4__c=7;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 8, 8, 12, 30, 0);
        
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 9, 9, 12, 30, 0);
        campMem.No_of_Boxes1__c=9;
        campMem.No_of_Boxes2__c=9;
        campMem.No_of_Boxes3__c=9;
        campMem.No_of_Boxes4__c=9;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 10, 10, 12, 30, 0);
        campMem.No_of_Boxes1__c=2;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 11, 11, 12, 30, 0);
        campMem.No_of_Boxes1__c=2;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 12, 12, 12, 30, 0);
        campMem.No_of_Boxes1__c=2;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);   
    }
    
    static testmethod void doinsertoptidlst1 ()
    {   
        
        Campaign campNew= new Campaign (Name='Test');
        insert campNew;
        ExactTarget_Integration_Settings__c tokenCustomSetting = new ExactTarget_Integration_Settings__c();         
        tokenCustomSetting.Name = 'ANZCampaign';        
        tokenCustomSetting.Value__c = campNew.id;      
        insert tokenCustomSetting;
        
        Account acc=new Account(Name='Test',OutletNumber__c='123456',Phone='123456');
        insert  acc;  
        Contact newCon=new Contact (LastName='test',MA2_Country_Code__c='SGP',MembershipNo__c='Test');
        insert newCon;
        Lead l = new Lead(lastname='11', company='11');
        insert l;
        
        CampaignMember campMem=new CampaignMember(CampaignId= campNew.id,leadid=l.id ,ContactID=newCon.id,Account__c=acc.id,Voucher_Status__c='Redeemed',Dummy_LastModifiedDate__c=datetime.newInstance(2018, 1, 1, 12, 30, 0));
        insert campMem;
        system.assertEquals(campMem.CampaignId,campNew.id,'success');
        Test.setCreatedDate(campMem.id, Date.newInstance(Date.today().year(),1,1));
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 1, 1, 12, 30, 0);
        campMem.Voucher_Status__c='Redeemed';
        campMem.No_of_Boxes1__c=1;
        
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        Test.setCreatedDate(campMem.id, Date.newInstance(Date.today().year(),2,2));
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 2, 2, 12, 30, 0);
        campMem.Voucher_Status__c='Redeemed';
        campMem.No_of_Boxes1__c=2;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        Test.setCreatedDate(campMem.id, Date.newInstance(Date.today().year(),3,3));
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 3, 3, 12, 30, 0);
        campMem.Voucher_Status__c='Redeemed';
        campMem.No_of_Boxes1__c=2;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        Test.setCreatedDate(campMem.id, Date.newInstance(Date.today().year(),4,4));
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 4, 4, 12, 30, 0);
        campMem.Voucher_Status__c='Redeemed';
        campMem.No_of_Boxes1__c=2;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        Test.setCreatedDate(campMem.id, Date.newInstance(Date.today().year(),5,5));
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 5, 5, 12, 30, 0);
        campMem.Voucher_Status__c='Redeemed';
        campMem.No_of_Boxes1__c=2;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        Test.setCreatedDate(campMem.id, Date.newInstance(Date.today().year(),6,6));
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 6, 6, 12, 30, 0);
        campMem.Voucher_Status__c='Redeemed';
        campMem.No_of_Boxes1__c=2;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        Test.setCreatedDate(campMem.id, Date.newInstance(Date.today().year(),7,7));
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 7, 7, 12, 30, 0);
        campMem.Voucher_Status__c='Redeemed';
        campMem.No_of_Boxes1__c=2;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        Test.setCreatedDate(campMem.id, Date.newInstance(Date.today().year(),8,8));
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 8, 8, 12, 30, 0);
        campMem.Voucher_Status__c='Redeemed';
        campMem.No_of_Boxes1__c=2;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        Test.setCreatedDate(campMem.id, Date.newInstance(Date.today().year(),9,9));
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 9, 9, 12, 30, 0);
        campMem.Voucher_Status__c='Redeemed';
        campMem.No_of_Boxes1__c=2;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        Test.setCreatedDate(campMem.id, Date.newInstance(Date.today().year(),10,10));
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 10, 10, 12, 30, 0);
        campMem.Voucher_Status__c='Redeemed';
        campMem.No_of_Boxes1__c=2;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        Test.setCreatedDate(campMem.id, Date.newInstance(Date.today().year(),11,11));
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 11, 11, 12, 30, 0);
        campMem.Voucher_Status__c='Redeemed';
        campMem.No_of_Boxes1__c=2;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);
        
        Test.setCreatedDate(campMem.id, Date.newInstance(Date.today().year()-1,12,12));
        campMem.Redeemed_Date_Time__c=datetime.newInstance(2018, 12, 12, 12, 30, 0);
        campMem.Voucher_Status__c='Redeemed';
        campMem.No_of_Boxes1__c=2;
        update campMem;
        SW_ANZ_VoucherRedemptionStatusCntrl_LEX.Refresh(acc.Id);        
    }
}