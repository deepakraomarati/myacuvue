@isTest
public with sharing class Test_SW_CMREditAccessController_LEX 
{
    
    static testMethod void testLoginMethod()
    {
        Account acc=new Account(Name='Test',OutletNumber__c='123456',Phone='123456');
        insert  acc;  
        JJ_JPN_CustomerMasterRequest__c mas=new JJ_JPN_CustomerMasterRequest__c(Name='Test',JJ_JPN_ApprovalStatus__c ='Pending for Approval');
        insert mas;
        JJ_JPN_CustomerMasterRequest__c mas1=new JJ_JPN_CustomerMasterRequest__c(Name='Test1');
        JJ_JPN_CustomerMasterRequest__c mas2=new JJ_JPN_CustomerMasterRequest__c();
       
        test.Starttest();
        SW_CMREditAccessController_LEX.cmrAccess(mas.id);    
        SW_CMREditAccessController_LEX.CMRSave(mas);
        SW_CMREditAccessController_LEX.accessDenied();
        SW_CMREditAccessController_LEX.accessDeniedEdit();
        SW_CMREditAccessController_LEX.accessCheck(mas.id);
        List<String>Test1=new List<String>();
        Test1.add('JJ_JPN_PaymentCondition__c');
        Map<String, List<String>> labelMap = new Map<String,List<String>>();
        labelMap.put('JJ_JPN_CustomerMasterRequest__c', Test1);
        String a=JSON.serialize(labelMap);
        SW_CMREditAccessController_LEX.CMRWrapper wrapper1= SW_CMREditAccessController_LEX.autoPopulateAccValues(String.valueof(mas.id),a);
        SW_CMREditAccessController_LEX.errorWrapper wrapper2=new SW_CMREditAccessController_LEX.errorWrapper ('t','f');
        SW_CMREditAccessController_LEX.CMRSave(mas1);
        SW_CMREditAccessController_LEX.CMRSave(mas2);
        test.stopTest();
        System.assertEquals(a, a);
        
    } 
}