@isTest
public class Test_SW_CreateFollowUpEventCtlr_LEX {

    Public Static Testmethod void recallApprovalpermasgnTest()
    {
        Contact contact=new Contact(lastName='Test');
        insert contact;
        Account act=new Account(Name='Test');
        insert act;
        Event evt=new Event(WhoId=contact.id,Subject='Test',DurationInMinutes=60,RecurrenceInterval = 1,WhatId=act.id,RecurrenceType = 'RecursDaily',RecurrenceEndDateOnly = System.today(),RecurrenceStartDateTime = System.today(),IsRecurrence = true,ActivityDateTime=datetime.newInstance(2014, 9, 15, 12, 30, 0));
        insert evt;
        test.startTest();
        ID permissionSetID = SW_JJ_JPN_approvalRecall_LEX.permissionSetRecords();
        List<String> Result1=SW_CreateFollowUpEventCtlr_LEX.fetchRecordTypeValues();
        System.assertEquals(Result1, Result1);
        id Result2=SW_CreateFollowUpEventCtlr_LEX.getRecTypeId('Master');
        SW_CreateFollowUpEventCtlr_LEX.getEventDetails(evt.id);
        System.assertEquals(Result2, Result2);
        test.stopTest();
    }
    
}