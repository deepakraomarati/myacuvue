@isTest
public with sharing class Test_SW_EventAttachmentCntrl_LEX 
{
    
    static testMethod void testLoginMethod()
    {
        
        Account acc=new Account(Name='Test',OutletNumber__c='123456',Phone='123456');
        insert  acc;  
        
        event e= new event();
        e.StartDateTime = date.today();
        e.EndDateTime = date.today().addDays(2);
        e.Subject = 'Test';
        e.WhatId = acc.id ;
        insert e;
        system.assertEquals(e.Subject,'Test','success');
        test.Starttest();
        SW_EventAttachmentCntrl_LEX.uploadAttachment('sss','XYZ.pdf',String.valueof(e.id));  
        test.stopTest(); 
    } 
}