@isTest
public with sharing class Test_SW_IND_ECP_VoucherFormCntrl_LEX 
{
    
    static testMethod void testLoginMethod()
    {
        Account acc=new Account(Name='Test',OutletNumber__c='123456',Phone='123456');
        insert  acc;  
        Contact newCon=new Contact (LastName='test',MA2_Country_Code__c='SGP',MembershipNo__c='Test');
        insert newCon;
        Lead l = new Lead(lastname='11', company='11');
        insert l;
        Campaign campNew= new Campaign (Name='Test');
        insert campNew;
        
        ExactTarget_Integration_Settings__c exTcam=new ExactTarget_Integration_Settings__c(name='INDCampaign',Value__c=campNew.id);
        insert exTcam;
        CampaignMember campMem=new CampaignMember(CampaignId=campNew.id,leadid=l.id ,ContactID=newCon.id,Account__c=acc.id,Voucher_Status__c='Not Used');
        insert campMem;
        system.assertEquals(campMem.CampaignId,campNew.id,'success');
        CampaignMember CampRecord=[Select User_eVoucher_Code__c from CampaignMember where id=:campMem.id ];
        
        test.Starttest();
        
        SW_IND_ECP_VoucherFormCntrl_LEX.resultWrapper redeemWrapper= SW_IND_ECP_VoucherFormCntrl_LEX.RedeemVoucher('fileXYZ', 'xyx','IMG', 'sadadsd',CampRecord.User_eVoucher_Code__c ,acc.id,'21123122' ,'2' ,'2');
        CampaignMember campMem2=new CampaignMember(id=campMem.id,CampaignId=campNew.id,leadid=l.id ,ContactID=newCon.id,Account__c=acc.id,Voucher_Status__c='Not Used');
        Update campMem2;
        SW_IND_ECP_VoucherFormCntrl_LEX.resultWrapper redeemWrapper2= SW_IND_ECP_VoucherFormCntrl_LEX.RedeemVoucher('fileXYZ', '','IMG', 'sadadsd','' ,acc.id,'21123122' ,'2' ,'2');
        SW_IND_ECP_VoucherFormCntrl_LEX.Onload(acc.Id);  
        test.stopTest();
    } 
       
    static testMethod void testLoginMethod1()
    {
        Account acc=new Account(Name='Test',OutletNumber__c='123456',Phone='123456');
        insert  acc;  
        Contact newCon=new Contact (LastName='test',MA2_Country_Code__c='SGP',MembershipNo__c='Test');
        insert newCon;
        Lead l = new Lead(lastname='11', company='11');
        insert l;
        
        Campaign campNew1= new Campaign (Name='Test1');
        insert campNew1;
        ExactTarget_Integration_Settings__c exTcam=new ExactTarget_Integration_Settings__c(name='INDCampaign',Value__c=campNew1.id);
        insert exTcam;
        
        CampaignMember campMem1=new CampaignMember(CampaignId=campNew1.id,leadid=l.id ,ContactID=newCon.id,Account__c=acc.id,Voucher_Status__c='Not Used',AttachmentId__c='1122333');
        insert campMem1;
        system.assertEquals(campMem1.CampaignId,campNew1.id,'success');
        CampaignMember CampRecord1=[Select User_eVoucher_Code__c from CampaignMember where id=:campMem1.id ];
        test.Starttest();
        SW_IND_ECP_VoucherFormCntrl_LEX.resultWrapper redeemWrapper1= SW_IND_ECP_VoucherFormCntrl_LEX.RedeemVoucher('fileXYZ', '','IMG', 'sadadsd',CampRecord1.User_eVoucher_Code__c ,acc.id,'21123123' ,'2' ,'2');
        SW_IND_ECP_VoucherFormCntrl_LEX.Onload(acc.Id);  
        test.stopTest();
    } 
}