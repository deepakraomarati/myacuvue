@isTest
public class Test_SW_JJ_JPN_approvalRecall_LEX {
    
    Public Static Testmethod void recallApprovalpermasgnTest()
    {  
        String Name='Test';
        Id userId = UserInfo.getUserId();
        test.startTest();
        ID permissionSetID = SW_JJ_JPN_approvalRecall_LEX.permissionSetRecords();
        system.assertEquals(Name,'Test','success');
        SW_JJ_JPN_approvalRecall_LEX.recallApproval(userid);
        SW_JJ_JPN_approvalRecall_LEX.checkPermission();
        SW_JJ_JPN_approvalRecall_LEX.recallApprovalpermasgn(permissionSetID,userId);
        SW_JJ_JPN_approvalRecall_LEX.recallApprovalpermdel(permissionSetID,userId);
        test.stopTest();
    }
}