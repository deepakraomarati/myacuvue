@isTest
public with sharing class Test_SW_MY_ECP_VoucherRedemptionFormCn 
{
    
    /* static testMethod void testLoginMethod3()
{

Account acc=new Account(Name='Test',OutletNumber__c='123456',Phone='123456');
insert  acc;  
Account acc1=new Account(Name='Test1',OutletNumber__c='123455',Phone='123455');
insert  acc1;
Contact newCon=new Contact (LastName='test',MA2_Country_Code__c='SGP',MembershipNo__c='Test',NRIC__c='Test');
insert newCon;
Contact newCon2=new Contact (LastName='test1',MA2_Country_Code__c='SGP',MembershipNo__c='Test1',NRIC__c='Test1');
insert newCon2;
Lead l = new Lead(lastname='11', company='11');
insert l;

Campaign campNew1= new Campaign (Name='Test1');
insert campNew1;
ExactTarget_Integration_Settings__c exTcam=new ExactTarget_Integration_Settings__c(name='MYCampaign',Value__c=campNew1.id);
insert exTcam;

CampaignMember campMem1=new CampaignMember(CampaignId=campNew1.id,leadid=l.id ,ContactID=newCon.id,Account__c=acc1.id,Voucher_Status__c='Not Used');
insert campMem1;
CampaignMember campMem2=new CampaignMember(CampaignId=campNew1.id,leadid=l.id ,ContactID=newCon2.id,Account__c=acc1.id,Voucher_Status__c='Not Used');
insert campMem2;
list<campaignmember> campmem = new list<campaignmember>([Select User_eVoucher_Code__c from CampaignMember where id=:campMem1.id and contactId =:  campMem1.contactID]);
test.Starttest();
SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.LoggedInEcpId = '';
SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.lstCampMember = campmem;   
//SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.resultWrapper redeemWrapper1= SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.RedeemVoucher('xyz','xyz','','','Test','Test','','2' ,'2');
SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.resultWrapper redeemWrapper2= SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.RedeemVoucher('xyz.jpg','xxx','',string.valueof(acc1.id),'Test','Test','1','2' ,'2');       
SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.Onload(acc1.Id);  
test.stopTest();

} 

static testMethod void testLoginMethod()
{

Account acc=new Account(Name='Test',OutletNumber__c='123456',Phone='123456');
insert  acc;  
Contact newCon=new Contact (LastName='test',MA2_Country_Code__c='SGP',MembershipNo__c='Test',NRIC__c='Test');
insert newCon;
Lead l = new Lead(lastname='11', company='11');
insert l;
Campaign campNew= new Campaign (Name='Test');
insert campNew;

ExactTarget_Integration_Settings__c exTcam=new ExactTarget_Integration_Settings__c(name='MYCampaign',Value__c=campNew.id);
insert exTcam;
CampaignMember campMem=new CampaignMember(CampaignId=campNew.id,leadid=l.id ,ContactID=newCon.id,Account__c=acc.id,Voucher_Status__c='Not Used');
insert campMem;
//  CampaignMember CampRecord=[Select User_eVoucher_Cgode__c from CampaignMember where id=:campMem.id ];

test.Starttest();

SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.resultWrapper redeemWrapper= SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.RedeemVoucher('fileXYZ.jpg', 'xyx','IMG', acc.id,'Tes','Tes','21123122' ,'2' ,'2');
CampaignMember campMem2=new CampaignMember(id=campMem.id,CampaignId=campNew.id,leadid=l.id ,ContactID=newCon.id,Account__c=acc.id,Voucher_Status__c='Not Used');
Update campMem2;
//  SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.resultWrapper redeemWrapper2= SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.RedeemVoucher('fileXYZ', '','IMG', 'sadadsd','Tes','Tes','21123122' ,'2' ,'2');
SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.Onload(acc.Id);  
test.stopTest();
} 

static testMethod void testLoginMethod2()
{


Account acc=new Account(Name='Test',OutletNumber__c='123456',Phone='123456');
insert  acc;  
Account acc1=new Account(Name='Test1',OutletNumber__c='123455',Phone='123455');
insert  acc1;
Contact newCon=new Contact (LastName='test',MA2_Country_Code__c='SGP',MembershipNo__c='Test',NRIC__c='Test');
insert newCon;
Contact newCon2=new Contact (LastName='test1',MA2_Country_Code__c='SGP',MembershipNo__c='Test1',NRIC__c='Test1');
insert newCon2;
Lead l = new Lead(lastname='11', company='11');
insert l;

Campaign campNew1= new Campaign (Name='Test1');
insert campNew1;
ExactTarget_Integration_Settings__c exTcam=new ExactTarget_Integration_Settings__c(name='MYCampaign',Value__c=campNew1.id);
insert exTcam;

CampaignMember campMem1=new CampaignMember(CampaignId=campNew1.id,leadid=l.id ,ContactID=newCon.id,Account__c=acc1.id,Voucher_Status__c='Redeemed');
insert campMem1;
CampaignMember campMem2=new CampaignMember(CampaignId=campNew1.id,leadid=l.id ,ContactID=newCon2.id,Account__c=acc1.id,Voucher_Status__c='Redeemed');
insert campMem2;
list<campaignmember> campmem = new list<campaignmember>([Select User_eVoucher_Code__c from CampaignMember where id=:campMem1.id and contactId =:  campMem1.contactID]);
test.Starttest();
SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.LoggedInEcpId = '';
SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.lstCampMember = campmem;   
//SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.resultWrapper redeemWrapper1= SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.RedeemVoucher('xyz','xyz','','','Test','Test','','2' ,'2');
SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.resultWrapper redeemWrapper2= SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.RedeemVoucher('xyz.jpg','xxx','',string.valueof(acc1.id),'Test','Test','1','2' ,'2');       
SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.Onload(acc1.Id);  
test.stopTest();
} 

*/
    static testMethod void testLoginMethod1()
    {        
        Account acc=new Account(Name='Test',OutletNumber__c='123456',Phone='123456');
        insert  acc;
        Account acc1=new Account(Name='Test1',OutletNumber__c='234567',Phone='234567');
        insert  acc1;
        
        Contact newCon=new Contact (LastName='test',MA2_Country_Code__c='SGP',MembershipNo__c='Test',NRIC__c='1234');
        insert newCon;
        Lead l = new Lead(lastname='11', company='11');
        insert l;
        
        Campaign campNew1= new Campaign (Name='Test1',IsActive=true);
        insert campNew1;
        ExactTarget_Integration_Settings__c exTcam=new ExactTarget_Integration_Settings__c(Name='MYCampaign',Value__c=campNew1.id);
        insert exTcam;
        
        CampaignMember campMem1=new CampaignMember(CampaignId=campNew1.id,leadid=l.id ,ContactID=newCon.id,Account__c=acc.id,Voucher_Status__c='Not Used');
        insert campMem1;
        list<campaignmember> campmem = new list<campaignmember>([Select User_eVoucher_Code__c from CampaignMember where id=:campMem1.id and contactId =:  campMem1.contactID]);
		
        CampaignMember CampRecord1=[Select User_eVoucher_Code__c from CampaignMember where id=:campMem1.id ];
        test.Starttest();
        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = newcon.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        
        String uploadFile='iVBORw0KGgoAAAANSUhEUgAAAi8AAAFtCAIAAABAzh3pAAAACXBIWXMAAA7EAAAOxA';
        List<List<SObject>> searchList = search.query('FIND \'' + newcon.Name + ' AND ' + newcon.NRIC__c + '\' IN ALL FIELDS RETURNING Contact(Id,Name,NRIC__c) LIMIT 1');
        SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.resultWrapper redeemWrapper1= SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.RedeemVoucher('fileXYZ.jpg', uploadFile,'IMG', acc.id,newCon.lastname,newCon.NRIC__c,'21123123' ,'2' ,'2');        
        SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.resultWrapper redeemWrapper2= SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.RedeemVoucher('fileXYZ', uploadFile,'IMG', acc.id,'','','21123123' ,'2' ,'2');        
        SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.resultWrapper redeemWrapper3= SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.RedeemVoucher('fileXYZ', uploadFile,'IMG', '','','','21123123' ,'2' ,'2');        

        SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.lstCampMember = campmem;   

        SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.Onload(acc.Id); 
        SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.setNRIC(campMem1.Id);
        SW_MY_ECP_VoucherRedemptionFormCntrl_LEX.getName(campMem1.Id); 
        test.stopTest();
        System.assertEquals(redeemWrapper1.Type, 'Error');
    }
  
}