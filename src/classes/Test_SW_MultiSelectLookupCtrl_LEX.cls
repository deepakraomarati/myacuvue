@isTest
public class Test_SW_MultiSelectLookupCtrl_LEX {
    
    Public Static Testmethod void recallApprovalpermasgnTest()
    {
        Event evt=new Event(Subject='Test',DurationInMinutes=60,RecurrenceInterval = 1,RecurrenceType = 'RecursDaily',RecurrenceEndDateOnly = System.today()+30,RecurrenceStartDateTime = System.today(),IsRecurrence = true,ActivityDateTime=datetime.newInstance(2014, 9, 15, 12, 30, 0));
        insert evt;
         system.assertEquals(evt.Subject,'Test','success');
        Contact c1 = new Contact(LastName='TestContact',NRIC__c='242K');
        Insert c1;
        Contact c2 = new Contact(LastName='TestContact1',NRIC__c='242K');
        Insert c2;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'sta112', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing44', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id,Unique_User_Id__c='sta112', 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduserst5@testorg456.com');
        insert u;
        User u1 = new User(Alias = 's214dt', Email='standarduser2@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p.Id,Unique_User_Id__c='s214dt', 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduserst7@testorg231.com');
        insert u1;
        List<User>users=new List<User>();
        users.add(u);
        users.add(u1);
        List<id>con=new List<id>();
        con.add(c1.id);
        con.add(c2.id);
        EventRelation er=new EventRelation(EventId=evt.id,RelationId=c1.id);
        insert er;
        
        test.startTest();
        
        SW_MultiSelectLookupCtrl_LEX.getAllEventAttendees(evt.id);
        SW_MultiSelectLookupCtrl_LEX.saveAttendeeList(evt.id,con);
        SW_MultiSelectLookupCtrl_LEX.saveAttendeeList(er.id,con);
        SW_MultiSelectLookupCtrl_LEX.deleteRecord(er.id);
        SW_MultiSelectLookupCtrl_LEX.DataTableColumns test1=new SW_MultiSelectLookupCtrl_LEX.DataTableColumns('ttt','ddd','hhh');
        SW_MultiSelectLookupCtrl_LEX.DataTableResponse test2=new SW_MultiSelectLookupCtrl_LEX.DataTableResponse(); 
        SW_MultiSelectLookupCtrl_LEX.lookupResults('Test',users);
        
        test.stopTest();
    }
    
}