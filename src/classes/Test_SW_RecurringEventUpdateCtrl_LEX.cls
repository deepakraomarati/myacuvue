@isTest
public class Test_SW_RecurringEventUpdateCtrl_LEX {
    
    Public Static Testmethod void recallApprovalpermasgnTest()
    {
        try{
            Event evt=new Event(Subject='Test',DurationInMinutes=60,RecurrenceInterval = 1,RecurrenceType = 'RecursDaily',RecurrenceEndDateOnly = System.today(),RecurrenceStartDateTime = System.today(),IsRecurrence = true,ActivityDateTime=datetime.newInstance(2014, 9, 15, 12, 30, 0));
            insert evt;
            system.assertEquals(evt.Subject,'Test','success');
            
            test.startTest();
            SW_RecurringEventUpdateCtrl_LEX.onLoadValue(evt.id);
            evt.RecurrenceType = 'RecursWeekly';
            evt.RecurrenceDayofWeekMask = 20;
            update evt;
            SW_RecurringEventUpdateCtrl_LEX.onLoadValue(evt.id);
            evt.RecurrenceDayofWeekMask = 65;
            update evt;
            SW_RecurringEventUpdateCtrl_LEX.onLoadValue(evt.id);
            evt.RecurrenceDayofWeekMask = 33;
            update evt;
            SW_RecurringEventUpdateCtrl_LEX.onLoadValue(evt.id);
            evt.RecurrenceDayofWeekMask = 9;
            update evt;
            SW_RecurringEventUpdateCtrl_LEX.onLoadValue(evt.id);
            evt.RecurrenceDayofWeekMask = 3;
            update evt;
            SW_RecurringEventUpdateCtrl_LEX.onLoadValue(evt.id);
            Event evt1=new Event(Subject='Test1',DurationInMinutes=60,RecurrenceInterval =NULL,RecurrenceDayOfMonth=09,RecurrenceMonthOfYear='40',RecurrenceType = 'RecursYearly',RecurrenceEndDateOnly = System.today()+30,RecurrenceStartDateTime = System.today(),IsRecurrence = true,ActivityDateTime=datetime.newInstance(2018, 9, 15, 12, 30, 0));
            //insert evt1;
            
            SW_RecurringEventUpdateCtrl_LEX.saveRecRecord(true,true,true,true,true,true,true,evt);
            test.stopTest();
        }
        catch(Exception e)
        {
            
        }
    }  
    Public Static Testmethod void recallApprovalpermasgnTest1()
    {
        try{
            Event evt=new Event(Subject='Test',DurationInMinutes=60,RecurrenceInterval = 1,RecurrenceType = 'RecursDaily',RecurrenceEndDateOnly = System.today(),RecurrenceStartDateTime = System.today(),IsRecurrence = true,ActivityDateTime=datetime.newInstance(2014, 9, 15, 12, 30, 0));
            insert evt;
            system.assertEquals(evt.Subject,'Test','success');
            
            test.startTest();
            SW_RecurringEventUpdateCtrl_LEX.onLoadValue(evt.id);
            evt.RecurrenceType='RecursYearly';
            evt.RecurrenceInterval=NULL;
            evt.RecurrenceDayOfWeekMask=NULL;
            evt.RecurrenceDayOfMonth=09;
            evt.RecurrenceMonthOfYear='June' ; 
            
            update evt;
            SW_RecurringEventUpdateCtrl_LEX.saveRecRecord(true,true,true,true,true,true,true,evt);
            evt.RecurrenceType='RecursYearlyNth';
            evt.RecurrenceInterval=NULL;
            evt.RecurrenceDayOfWeekMask=NULL;
            evt.RecurrenceDayOfMonth=NULL;
            evt.RecurrenceMonthOfYear='June' ;  
            evt.RecurrenceDayOfWeekMask=1;
            evt.RecurrenceInstance='First';
            update evt;
            SW_RecurringEventUpdateCtrl_LEX.saveRecRecord(true,true,true,true,true,true,true,evt);
            test.stopTest();
        }
        catch(Exception e)
        {
            
        }
    }  
    Public Static Testmethod void recallApprovalpermasgnTest2()
    {
        try{
            Event evt=new Event(Subject='Test',DurationInMinutes=60,RecurrenceInterval = 1,RecurrenceType = 'RecursDaily',RecurrenceEndDateOnly = System.today(),RecurrenceStartDateTime = System.today(),IsRecurrence = true,ActivityDateTime=datetime.newInstance(2014, 9, 15, 12, 30, 0));
            insert evt;
            system.assertEquals(evt.Subject,'Test','success');
            
            test.startTest();
            SW_RecurringEventUpdateCtrl_LEX.onLoadValue(evt.id);
            evt.RecurrenceType='RecursEveryWeekday';
            evt.RecurrenceInterval=NULL;
            evt.RecurrenceDayOfWeekMask=NULL;
            evt.RecurrenceDayOfMonth=NULL;
            evt.RecurrenceMonthOfYear=null;
            evt.RecurrenceDayOfWeekMask=1;
            evt.RecurrenceInstance=null;
            update evt;
            SW_RecurringEventUpdateCtrl_LEX.saveRecRecord(true,true,true,true,true,true,true,evt);
            evt.RecurrenceType='RecursMonthly';
            evt.RecurrenceInterval=NULL;
            evt.RecurrenceDayOfWeekMask=NULL;
            evt.RecurrenceDayOfMonth=09;
            evt.RecurrenceMonthOfYear=null;
            evt.RecurrenceDayOfWeekMask=Null;
            evt.RecurrenceInstance=null;
            evt.RecurrenceInterval=1;
            
            update evt;
            SW_RecurringEventUpdateCtrl_LEX.saveRecRecord(true,true,true,true,true,true,true,evt);
            evt.RecurrenceType='RecursMonthlyNth';
            
            evt.RecurrenceDayOfWeekMask=1;
            evt.RecurrenceDayOfMonth=null;
            evt.RecurrenceMonthOfYear=null;
            
            evt.RecurrenceInstance='First';
            evt.RecurrenceInterval=1;
            update evt;
            SW_RecurringEventUpdateCtrl_LEX.saveRecRecord(true,true,true,true,true,true,true,evt);
            evt.RecurrenceType='RecursDaily';
            evt.RecurrenceDayOfWeekMask=null;
            evt.RecurrenceDayOfMonth=null;
            evt.RecurrenceMonthOfYear=null;
            evt.RecurrenceInstance=null;
            evt.RecurrenceInterval=1;
            update evt;
            SW_RecurringEventUpdateCtrl_LEX.saveRecRecord(true,true,true,true,true,true,true,evt);
            test.stopTest();
        }
        catch(Exception e)
        {
            
        }
    }  
}