@isTest
public class Test_UpdateOrderStartDateOnActivation {
    
    public static testMethod void validateUpdateOrder(){
        List<Order> lstOrder = new List<Order>();
        Account act = new Account();
        act.Name='Test';
        insert act;
        Contract c = new Contract(Name='test',StartDate=Date.Today(),Status = 'Draft', AccountId = act.Id,  ContractTerm = 4);
        insert c;
        Order o = new Order();
        o.Name = 'Test';
        o.AccountId = act.Id;
        o.ContractId = c.Id;
        o.EffectiveDate = Date.Today();
//o.ActivatedDate =datetime.newInstance(2018, 9, 15, 12, 30, 0);
        o.Status = 'Draft';
        lstOrder.add(o);
        insert lstOrder;
        List<Order> lstOrder1 = new List<Order>{ [select id from Order where id in :lstOrder]};
            for(Order order:lstOrder1)
            order.Name = 'Test2';
        update lstOrder1;
       System.assertEquals(lstOrder1.size(), lstOrder1.size());
    }

}