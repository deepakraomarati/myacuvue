@isTest
public with sharing class Testcouponsendlist
{
    static testmethod void doinsertCouponsendlist()
    {
        CouponSendList__c CSendlist = new CouponSendList__c();      
        CSendlist.Aws_CouponId__c= '52';
        CSendlist.DB_ID__c= 21;      
        CSendlist.Aws_ContactId__c='51';        
        insert CSendlist;   
        
        Coupon__c Coup = new Coupon__c();
        //Coup.Name='TestCoupon';        
        Coup.DB_ID__c=52;        
        insert Coup;
        
        Contact Con= new Contact();
        Con.LastName='testcouponsendlist';      
        Con.Aws_AccountId__c='123456';
        insert Con;     
        system.assertEquals(Con.LastName,'testcouponsendlist','success');
        
        CouponSendList__c CSendlistUpdate = new CouponSendList__c();        
        CSendlistUpdate.Id=CSendlist.Id;
        CSendlistUpdate.CouponId__c= Coup.Id;        
        CSendlistUpdate.contactid__c=Con.Id;        
        update CSendlistUpdate;
        
    }
}