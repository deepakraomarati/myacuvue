@isTest
public with sharing class TestgetProductPrice 
{
    static testmethod void doinsertProductPrice()
    {
        ProductPrice__c prdpr = new ProductPrice__c();
        prdpr.Aws_AccountId__c = '123';
        prdpr.Aws_ProductId__c = '456';
        insert prdpr;
        
        account acc = new account();
        acc.Name='test';
        acc.Aws_AccountId__c ='123';
        insert acc;
        
        product2 pdl = new product2();
        pdl.Name ='Test';
        pdl.UPC_Code__c ='456';
        pdl.InternalName__c='Abc';
        insert pdl;
        system.assertEquals(pdl.InternalName__c,'Abc','success');
        
        ProductPrice__c prd = new ProductPrice__c();
        prd.id=prdpr.id;
        prd.AccountId__c = acc.id;
        prd.ProductId__c = pdl.id;
        update prd;
    }
}