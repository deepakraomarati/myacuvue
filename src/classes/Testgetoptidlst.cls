@isTest
public with sharing class Testgetoptidlst 
{
    static testmethod void doinsertoptidlst ()
    {
        OptIdList__c optlst = new OptIdList__c();
        optlst.Aws_OpticianBoardId__c = '456';
        optlst.Aws_AccountId__c ='12';
        
        insert optlst;
        
        OpticianBoard__c optbd = new OpticianBoard__c();
        optbd.Name='test';
        optbd.DB_ID__c=456;
        insert optbd;
        
        account acc = new account();
        acc.Name='test';
        acc.Aws_AccountId__c='12';
        insert acc;
        system.assertEquals(acc.Name,'test','success');
        
        OptIdList__c opls = new OptIdList__c();
        opls.id=optlst.id;
        opls.OpticianBoardId__c=optbd .id;
        opls.AccountId__c=acc.id;
        update opls;
    }
}