@isTest 
public class TestpopulateAccountFromContact {
    static testMethod void insertNewEvent() {
        Account acc = New Account(Name ='Test Account1',PublicAddress__c='Bangalore', SalesRep__c = 'test1', 
                                  AccountNumber = '828135', OutletNumber__c = '828135', CountryCode__c = 'AUS');
        insert acc;
        system.assertEquals(acc.Name,'Test Account1','success');
        Contact cont = New Contact(FirstName = 'Lemu',LastName = 'Edward',Email = 'ledward4@its.jnj.com',Phone = '123456789',Account=acc);
        insert cont;
        
        Event newEvent = new Event();
        newEvent.whoid=cont.id;
        RecordType rt = [Select id from RecordType where sobjecttype='Event' Limit 1];
        newEvent.RecordTypeId=rt.id;
        newEvent.Subject = 'Testing trigger';
        newEvent.StartDateTime=date.newinstance(2015, 10, 28);
        newEvent.EndDateTime=date.newinstance(2015, 10, 29);
        insert newEvent;
    }
}