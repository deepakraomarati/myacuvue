@isTest
private class TestschduleBatchCountObject1 {
    
    static testmethod void schduleBatchCountObject1Test(){
        CountObject__c tc1 = new CountObject__c();
        tc1.Name='Account';
        tc1.Count__c=2500;
        insert tc1;
        
        CountObject__c tc2 = new CountObject__c();
        tc2.Name='Contact';
        tc2.Count__c=2500;
        insert tc2;
        
        CountObject__c tc3 = new CountObject__c();
        tc3.Name='Product';
        tc3.Count__c=2500;
        insert tc3;
        
        CountObject__c tc4 = new CountObject__c();
        tc4.Name='Campaign';
        tc4.Count__c=2500;
        insert tc4;
        system.assertEquals(tc4.Name,'Campaign','success');
 
        CountObject__c tc8 = new CountObject__c();
        tc8.Name='BatchDetails';
        tc8.Count__c=2500;
        insert tc8;
        
        Test.StartTest();
        schduleBatchCountObject1 SBCObj = new schduleBatchCountObject1 ();
        String sch = '0  00 1 3 * ?';
        system.schedule('Test', sch, SBCObj );        
        Test.stopTest();
    }
    
}