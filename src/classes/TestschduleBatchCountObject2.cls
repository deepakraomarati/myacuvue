@isTest
private class TestschduleBatchCountObject2  {
    
    static testmethod void schduleBatchCountObject2Test(){
        
        CountObject__c tc5 = new CountObject__c();
        tc5.Name='CampaignMember';
        tc5.Count__c=2500;
        insert tc5;
        system.assertEquals(tc5.Name,'CampaignMember','success');
        
        CountObject__c tc6 = new CountObject__c();
        tc6.Name='PointHistory';
        tc6.Count__c=2500;
        insert tc6;
        
        CountObject__c tc7 = new CountObject__c();
        tc7.Name='TransactionTds';
        tc7.Count__c=2500;
        insert tc7;
        
        Test.StartTest();
        schduleBatchCountObject2 SBC2Obj = new schduleBatchCountObject2 ();
        String sch = '0  00 1 3 * ?';
        system.schedule('Test', sch, SBC2Obj );        
        Test.stopTest();
    }
    
}