public with sharing class TotalSFDC {

   /*/ public String getObjectCountItems() {
        return null;
    }*/
    
    public List<TotalObjectCounts> GetObjectCountItems() {
        List<TotalObjectCounts> AnnualRecurrRevenueItems = new List<TotalObjectCounts>();
        TotalObjectCounts AnnualRecRevenue;
        List<AggregateResult> dataItems;
        Integer totalShipments = 0;
        
      
        
        totalShipments = 0;
        dataItems= new List<AggregateResult> ();
        List<CountObject__c> dataItems1 = [Select Count__C from CountObject__c where Name = 'Account'];
        system.debug('BatchDetails>>'+dataItems1 );
        
        for(CountObject__c Ts :dataItems1 ){
          
         totalShipments= Integer.ValueOf(Ts.Count__c);      
        }
        AnnualRecRevenue = new TotalObjectCounts ('Account',totalShipments );
        AnnualRecurrRevenueItems.add(AnnualRecRevenue);
        
        totalShipments = 0;
        dataItems= new List<AggregateResult> ();
        dataItems1 = [Select Count__C from CountObject__c where Name = 'Contact'];
        system.debug('BatchDetails>>'+dataItems1 );
        
        for(CountObject__c Ts :dataItems1 ){
          
         totalShipments= Integer.ValueOf(Ts.Count__c);      
        }
        AnnualRecRevenue = new TotalObjectCounts ('Contact',totalShipments );
        AnnualRecurrRevenueItems.add(AnnualRecRevenue);
        
        
        totalShipments = 0;
        dataItems= new List<AggregateResult> ();
        dataItems1 = [Select Count__C from CountObject__c where Name = 'BatchDetails'];
        system.debug('BatchDetails>>'+dataItems1 );
        
        for(CountObject__c Ts :dataItems1 ){
          
         totalShipments= Integer.ValueOf(Ts.Count__c);      
        }
        AnnualRecRevenue = new TotalObjectCounts ('BahDetils',totalShipments );
        AnnualRecurrRevenueItems.add(AnnualRecRevenue);
        
         totalShipments = 0;
        dataItems= new List<AggregateResult> ();
        dataItems1 = [Select Count__C from CountObject__c where Name = 'Product'];
        system.debug('BatchDetails>>'+dataItems1 );
        
        for(CountObject__c Ts :dataItems1 ){
          
         totalShipments= Integer.ValueOf(Ts.Count__c);      
        }
        AnnualRecRevenue = new TotalObjectCounts ('Prodt',totalShipments );
        AnnualRecurrRevenueItems.add(AnnualRecRevenue);
        
         totalShipments = 0;
        dataItems= new List<AggregateResult> ();
        dataItems1 = [Select Count__C from CountObject__c where Name = 'Campaign'];
        system.debug('BatchDetails>>'+dataItems1 );
        
        for(CountObject__c Ts :dataItems1 ){
          
         totalShipments= Integer.ValueOf(Ts.Count__c);      
        }
        AnnualRecRevenue = new TotalObjectCounts ('Camp',totalShipments );
        AnnualRecurrRevenueItems.add(AnnualRecRevenue);
        
         totalShipments = 0;
        dataItems= new List<AggregateResult> ();
        dataItems1 = [Select Count__C from CountObject__c where Name = 'CampaignMember'];
        system.debug('BatchDetails>>'+dataItems1 );
        
        for(CountObject__c Ts :dataItems1 ){
          
         totalShipments= Integer.ValueOf(Ts.Count__c);      
        }
        AnnualRecRevenue = new TotalObjectCounts ('CampMem',totalShipments );
        AnnualRecurrRevenueItems.add(AnnualRecRevenue);
        
         totalShipments = 0;
        dataItems= new List<AggregateResult> ();
        dataItems1 = [Select Count__C from CountObject__c where Name = 'PointHistory'];
        system.debug('BatchDetails>>'+dataItems1 );
        
        for(CountObject__c Ts :dataItems1 ){
          
         totalShipments= Integer.ValueOf(Ts.Count__c);      
        }
        AnnualRecRevenue = new TotalObjectCounts ('PntHsty',totalShipments );
        AnnualRecurrRevenueItems.add(AnnualRecRevenue);
        
         totalShipments = 0;
        dataItems= new List<AggregateResult> ();
        dataItems1 = [Select Count__C from CountObject__c where Name = 'TransactionTds'];
        system.debug('BatchDetails>>'+dataItems1 );
        
        for(CountObject__c Ts :dataItems1 ){
          
         totalShipments= Integer.ValueOf(Ts.Count__c);      
        }
        AnnualRecRevenue = new TotalObjectCounts ('TransTds',totalShipments );
        AnnualRecurrRevenueItems.add(AnnualRecRevenue);

        
        /*for(AggregateResult AR:dataItems ){
        totalShipments = Integer.valueof(AR.get('Count__C '));
        }
        
        AnnualRecRevenue = new TotalObjectCounts ('Product',totalShipments );
        AnnualRecurrRevenueItems.add(AnnualRecRevenue);*/
     

       /*  Decimal dataItems = [Select ID, Count__C from CountObject__c where Name = 'BatchDetails' ];
        
        system.debug('Count>>'+dataItems );

                TotalObjectCounts AnnualRecRevenue = new TotalObjectCounts ('BatchDetails',
                                                             dataItems );
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);               
               
                
               dataItems = [SELECT COUNT() FROM Product2 ];
                
                AnnualRecRevenue = new TotalObjectCounts ('Product',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                ApexPages.StandardSetController Ret = new  ApexPages.StandardSetController(Database.getQueryLocator([Select id from Batch_Details__c limit 100]));                
                 
                System.debug('Size>> '+ Ret.getResultSize());
                
                //Integer Count = Database.CountQuery(Query);
                
                AnnualRecRevenue = new TotalObjectCounts ('Batch Details',
                                                             Query.size());
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM contact];
                
                AnnualRecRevenue = new TotalObjectCounts ('Contact',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM Campaign ];
                
                AnnualRecRevenue = new TotalObjectCounts ('Camp',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);
                
                dataItems = [SELECT COUNT() FROM CampaignMember ];
                
                AnnualRecRevenue = new TotalObjectCounts ('CampMem',
                                                             dataItems);
                AnnualRecurrRevenueItems.add(AnnualRecRevenue);*/
                

            
            return AnnualRecurrRevenueItems;
        }

 public class TotalObjectCounts {
        public String Objects { get; set; }        
        public Decimal UCount { get; set; }

        public TotalObjectCounts(String Objects, Decimal UCount) {
            this.Objects = Objects;
            this.UCount = UCount;           
        }
    }


}