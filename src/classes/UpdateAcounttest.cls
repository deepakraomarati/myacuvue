@isTest
public class UpdateAcounttest {
    
    static testMethod void UpdateAccounttest() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];  
            Account acc = New Account(Name ='Test Account',PublicAddress__c='Bangalore', SalesRep__c = 'ABC', AccountNumber = '12345', OutletNumber__c = '12345', CountryCode__c = 'AUS');
            insert acc;
            system.assertEquals(acc.Name,'Test Account','success');
        
            event e= new event();
            e.StartDateTime = date.today();
            e.EndDateTime = date.today().addDays(2);
            e.Subject = 'Test';
            e.WhatId = acc.id ;
            insert e;
                
            e.Account_Name__c='Test Account';
            e.Account_Number__c='12345';
            e.ActivityStatus__c='Open';
            e.Strategic_Cust__c='ANT1234';
            e.Cust_Group_No__c='AUT005';
            update e;
    }
}