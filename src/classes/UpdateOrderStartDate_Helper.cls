public class UpdateOrderStartDate_Helper {
    List<Order> listOrders = new List<Order>();
    private static boolean run = true;
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }
    
    public static void UpdateOrderStartDate_Method(List<Order> newOrders){
        newOrders = [SELECT ID, ActivatedDate,ActivatedbyID, EffectiveDate, Status FROM Order  where ID =: trigger.newmap.keyset()];
                
        System.debug('newOrders>>'+newOrders);
        
        Map<Id, Order> mapOrders = new Map<Id, Order>();
        
        if(newOrders.size() > 0){
            for (Order o : newOrders){
                mapOrders.put(o.id, o);
            }
        }
        //To change Order Start Date,  Deactive activated account on click on "Activate" button
        //This is required because Start Date can't be changed for order whose status is Active.
        //
        if(mapOrders.values().status == System.Label.OrderStatusActivated){
            try{
                if(newOrders.size() > 0)       
                for (Order mo : mapOrders.values()){
                    System.debug('mo.ActivatedDate>>'+mo.ActivatedDate);
                    mo.Status = System.Label.OrderStatusDraft;
                    if(mo.ActivatedDate != null && mo.ActivatedDate > System.today() && mo.EffectiveDate != null){
                        mo.EffectiveDate = System.today();
                    }
                }
                System.debug('newOrders>>'+newOrders);
                update(newOrders);
            }catch(Exception e){
                Trigger.new[0].addError('Order Start Date is not updated');
            }
            
            
            //To activate the deactived order after Order Start Date has been updated.
            
            
            System.debug('mapOrders.values().status>>'+mapOrders.values().status);
            if(mapOrders.values().status == System.Label.OrderStatusDraft && mapOrders.values().EffectiveDate == System.today()){
                try{
                    for (Order m : mapOrders.values()){
                        //System.debug('mo.ActivatedDate>>'+mm.ActivatedDate);
                        m.Status = System.Label.OrderStatusActivated;
                    }
                    update(mapOrders.values());
                }catch(Exception e){
                    Trigger.new[0].addError('Order Status is not updated to activated');
                }  
            }
        }
    }
}