@isTest
public class UpdateOutlookSyncFieldsTest {
    static testMethod void validateUpdateOutlookSyncFields(){
        Profile p = [SELECT Id FROM Profile WHERE Name='ASPAC ANZ SM'];
        UserRole role = [SELECT Id FROM UserRole WHERE Name='Admin Role'];
        User u = new User(Alias = 'ANZCDM', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Lemu', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id, Country = 'Australia', UserRoleId = role.id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='Lemu@abc.com',Unique_User_Id__c = 'Lemu');
        insert u;
        system.runas(u){
            Account acc = New Account(Name ='Test Account1',PublicAddress__c='Bangalore', SalesRep__c = 'test1', 
                                      AccountNumber = '828135', OutletNumber__c = '828135', CountryCode__c = 'AUS');
            insert acc;
            system.assertEquals(acc.Name,'Test Account1','success');
            Contact cont = New Contact(FirstName = 'Lemu',LastName = 'Edward',Email = 'ledward4@its.jnj.com',Phone = '123456789',Account=acc);
            insert cont;
            Event newEvent = new Event();
            newEvent.whoid=cont.id;
            newEvent.whatid=acc.id;
            RecordType rt = [Select id from RecordType where sobjecttype='Event' and Name='Customer Development Manager Call ANZ'];
            newEvent.RecordTypeId=rt.id;
            newEvent.Subject = 'Testing trigger';
            newEvent.StartDateTime=date.newinstance(2016, 1, 20);
            newEvent.EndDateTime=date.newinstance(2016, 1, 20);
            newEvent.ActivityStatus__c = 'Planned';
            newEvent.Objective1__c = 'Test Objective1__c';
            newEvent.ObjectivesSMART__c = 'Test ObjectivesSMART__c';
            newEvent.PotentialObstacle__c = 'Test PotentialObstacle__c';
            newEvent.Open3W__c = 'Test Open3W__c';
            newEvent.Brand_Name__c = 'Test Brand_Name__c';
            newEvent.Clarify__c = 'Test Clarify__c';
            newEvent.Influence__c = 'Test Influence__c';
            newEvent.Close__c = 'Test Close__c';
            newEvent.Notes1__c = 'Test Notes1__c';
            newEvent.CallObjectiveAchieved__c = 'Test CallObjectiveAchieved__c';
            newEvent.CallOutcomeCallTargetComments__c = 'Test CallOutcomeCallTargetComments__c';
            insert newEvent;
            newEvent.Notes1__c = 'Test Notes update';
            newEvent.Description = 'Special update'+ newEvent.Description;
            update newEvent;
        }
    }
}