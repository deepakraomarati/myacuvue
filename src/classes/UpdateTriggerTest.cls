@isTest
public class UpdateTriggerTest{
    static testMethod void validateUpdateTerritory () {
        
        Account TestTriggerAcc1 = new Account(Name='Test Account1',Territory1__c='AUD001');
        Account TestTriggerAcc2 = new Account(Name='Test Account2',Territory1__c='AUD002');
        List<Account> AccList =  new List<Account>();
        AccList.add(TestTriggerAcc1);
        AccList.add(TestTriggerAcc2);
        insert AccList;
        AccList.get(0).Territory1__c='AUD001';
        AccList.get(1).Territory1__c='AUD002';
        update AccList;
        System.debug('Territory value  after trigger fired: ' + TestTriggerAcc1.Territory__c);
        System.debug('Territory value  after trigger fired: ' + TestTriggerAcc2.Territory__c);
        system.assertEquals(TestTriggerAcc1.Name,'Test Account1','success');
    }
}