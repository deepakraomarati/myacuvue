@isTest
public class UpdateTriggerTestClass {
    static testMethod void validateUpdateTrigger() {
        Account TestTriggerAcc1 = new Account(Name='Test Account1',Territory1__c='KRT801');
        Account TestTriggerAcc2 = new Account(Name='Test Account2',Territory1__c='KRT305');
        List<Account> AccList =  new List<Account>();
        AccList.add(TestTriggerAcc1);
        AccList.add(TestTriggerAcc2);
        insert AccList;
        AccList.get(0).Territory1__c='KRT305';
        AccList.get(1).Territory1__c='KRT801';
        update AccList;
        System.debug('Territory value  after trigger fired: ' + TestTriggerAcc1.Territory__c);
        System.debug('Territory value  after trigger fired: ' + TestTriggerAcc2.Territory__c);
        system.assertEquals(TestTriggerAcc1.Name,'Test Account1','success');   
    }
}