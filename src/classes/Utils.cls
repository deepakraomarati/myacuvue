global class Utils {
    global static String getSessionIdFromVFPage(PageReference visualforcePage){
        
String content;
        if(!test.isRunningTest()){
    content = visualforcePage.getContent().toString();
  }else{
   content = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> <html><head><script src="/static/111213/js/perf/stub.js" type="text/javascript"></script><script src="/jslibrary/1539646802000/sfdc/main.js" type="text/javascript"></script><script src="/jslibrary/jslabels/1540418410000/en_US.js" type="text/javascript"></script><link class="user" href="/sCSS/44.0/sprites/1539205178000/Theme3/default/gc/zen-componentsCompatible.css" rel="stylesheet" type="text/css" /><link class="user" href="/sCSS/44.0/sprites/1539205178000/Theme3/default/gc/elements.css" rel="stylesheet" type="text/css" /><link class="user" href="/sCSS/44.0/sprites/1539205178000/Theme3/default/gc/common.css" rel="stylesheet" type="text/css" /><link class="user" href="/sCSS/44.0/sprites/1539133570000/Theme3/gc/dStandard.css" rel="stylesheet" type="text/css" /><link class="user" href="/sCSS/44.0/sprites/1540365582000/Theme3/00D5D00000013WI/0055D000001hvbA/gc/dCustom0.css" rel="stylesheet" type="text/css" /><link class="user" href="/sCSS/44.0/sprites/1540365582000/Theme3/00D5D00000013WI/0055D000001hvbA/gc/dCustom1.css" rel="stylesheet" type="text/css" /><link class="user" href="/sCSS/44.0/sprites/1539205178000/Theme3/default/gc/extended.css" rel="stylesheet" type="text/css" /><link class="user" href="/sCSS/44.0/sprites/1539205178000/Theme3/default/gc/setup.css" rel="stylesheet" type="text/css" /><script>(function(UITheme) { UITheme.getUITheme = function() {  return UserContext.uiTheme;  };}(window.UITheme = window.UITheme || {}));</script></head><body>Start_Of_Session_IdSESSION_ID_REMOVEDEnd_Of_Session_Id<script>if (this.SfdcApp && this.SfdcApp.projectOneNavigator) { SfdcApp.projectOneNavigator.sendTitleToParent(\'VF_ForSessionID\'); } </script></body></html>';
  }
        
        system.debug('content'+content);
        Integer s = content.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
            e = content.indexOf('End_Of_Session_Id');
        system.debug('content.substring(s, e)'+content.substring(s, e));
        return content.substring(s, e);
    }
}