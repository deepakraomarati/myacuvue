@isTest()
public class UtilsTest {
    
    static Testmethod void getSessionIdFromVFPagetest()
    {
       
        test.startTest();
      MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        //service.SessionHeader.sessionId = UserInfo.getSessionId();
        //system.debug('service.SessionHeader.sessionId'+service.SessionHeader.sessionId);
        Utils.getSessionIdFromVFPage(Page.VF_ForSessionID);
        system.assertEquals('SESSION_ID_REMOVED', Utils.getSessionIdFromVFPage(Page.VF_ForSessionID));
        test.stopTest();
        
    }

}