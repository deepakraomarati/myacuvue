/**
 * File Name:  courseUpdate
 * Author: Sathishkumar | BICSGLBOAL
 * Date Last Modified:  25-Oct-2018
 * Description: Batch class to call LMS API Service to get courses information and update & insert 
 * Copyright (c) $2018 Johnson & Johnson
 */

public with sharing class courseUpdate extends BaseRestResponse implements Database.Batchable<sObject>, Database.AllowsCallouts, schedulable
{
	static Map<Integer, String> mapForActiveStatus = new Map<Integer, String>();
	static Map<Integer, String> mapForExpireType = new Map<Integer, String>();
	public void execute(SchedulableContext SC)
	{
		courseUpdate executeCourseUpdation = new courseUpdate();
		Database.executeBatch(executeCourseUpdation, 1);
	}
	public override void restResponse()
	{
		/*override method*/
	}
	public Database.QueryLocator start(Database.BatchableContext BC)
	{
		String query = 'SELECT Id, Name, Value__c FROM LMS_Settings__c WHERE name IN (';
		for (Role__c rl : [SELECT ID FROM Role__c])
		{
			query = query + '\'ja_JP-' + rl.id + '\',';
		}
		query = query + ')';
		query = query.replace(',)', ')');
		return Database.getQueryLocator(query);
	}
	public void execute(Database.BatchableContext BC, List<LMS_Settings__c> lmsFakeusers)
	{
		mapForActiveStatus.put(0, 'Active');
		mapForActiveStatus.put(1, 'Inactive');
		mapForActiveStatus.put(2, 'Pending');
		mapForExpireType.put(0, 'None');
		mapForExpireType.put(1, 'Date');
		mapForExpireType.put(2, 'Duration');
		List<Course__c> courseUpdate = new List<Course__c>();
		List<Curriculum__c> curriculumUpdate = new List<Curriculum__c>();
		List<LmsUtils.CourseDTOFrUpdate> availableCourseForFakeUser = LmsUtils.getAvailabeCourses(lmsFakeusers[0].value__c);
		List<CurriculumDTOForUpdate> holdsCurriculumList = getCurriculum();
		Map<String, String> curriculumDTOMapForUpdate = new Map<String, String>();
		for (CurriculumDTOForUpdate curriculumListVariable : holdsCurriculumList)
		{
			curriculumDTOMapForUpdate.put(curriculumListVariable.Id, curriculumListVariable.ActiveStatus);
		}
		List<OnlineCourseDTOForUpdate> holdsOnlineCoursesList = getOnlineCourse();
		Map<String, String> onlineCourseDTOMapForUpdate = new Map<String, String>();
		for (OnlineCourseDTOForUpdate onlineCoursesListVariable : holdsOnlineCoursesList)
		{
			onlineCourseDTOMapForUpdate.put(onlineCoursesListVariable.Id, onlineCoursesListVariable.ActiveStatus);
		}
		List<ILTCourseDTOForUpdate> holdsILTCoursesList = getILTCourse();
		Map<String, String> iltCourseDTOMapForUpdate = new Map<String, String>();
		for (ILTCourseDTOForUpdate iltCourseListVariable : holdsILTCoursesList)
		{
			iltCourseDTOMapForUpdate.put(iltCourseListVariable.Id, iltCourseListVariable.ActiveStatus);
		}
		for (LmsUtils.CourseDTOFrUpdate holdsAvailableCourse : availableCourseForFakeUser)
		{
			List<String> holdsLanguageToSplit = lmsFakeusers[0].name.split('-');
			if (holdsAvailableCourse.CourseType == 'OnlineCourse' || holdsAvailableCourse.CourseType == 'InstructorLedCourse')
			{
				Map<String, String> mapForImages = new Map<String, String>();
				List<ResourcesDTOForUpdate> availableResource = getResourcesForCourse(holdsAvailableCourse.id);
				for (ResourcesDTOForUpdate resourceUpdateVariable : availableResource)
				{
					mapForImages.put(resourceUpdateVariable.Name, resourceUpdateVariable.File);
				}
				courseUpdate.add(courseBuild(mapForImages, holdsAvailableCourse, holdsLanguageToSplit));
			}
			else if (holdsAvailableCourse.CourseType == 'Curriculum')
			{
				Map<String, String> mapForImages = new Map<String, String>();
				List<ResourcesDTOForUpdate> availableResource = getResourcesForCourse(holdsAvailableCourse.id);
				for (ResourcesDTOForUpdate resourceUpdateVariable : availableResource)
				{
					mapForImages.put(resourceUpdateVariable.Name, resourceUpdateVariable.File);
				}
				curriculumUpdate.add(curriculumBuild(mapForImages, holdsAvailableCourse, holdsLanguageToSplit));
			}
		}
		if (!courseUpdate.isEmpty())
		{
			upsert courseUpdate LMS_ID__c;
		}
		if (!curriculumUpdate.isEmpty())
		{
			upsert curriculumUpdate LMS_ID__c;
		}
		statusupdate(CurriculumDTOMapForUpdate, OnlineCourseDTOMapForUpdate, ILTCourseDTOMapForUpdate, availableCourseForFakeUser);
	}
	public void finish(Database.BatchableContext BC)
	{
		//finish method
	}
	/**
	* @description  Helper method to build course record for creation
	*/
	private static Course__c courseBuild(Map<String, String> mapForImages, LmsUtils.CourseDTOFrUpdate availableCourseForFakeUser, List<String> holdsLanguageToSplit)
	{
		Course__c courseObj = new Course__c();
		courseObj.Main_Course_Image__c = mapForImages.get('Main_Course_Image');
		courseObj.Related_Content_Image__c = mapForImages.get('Related_Content_Image');
		courseObj.Landing_page_Image__c = mapForImages.get('Landing_page_Image');
		courseObj.Online_CoursePage_Image__c = mapForImages.get('Online_CoursePage_Image');
		courseObj.Name = availableCourseForFakeUser.Name;
		courseObj.LMS_ID__c = availableCourseForFakeUser.Id;
		courseObj.Description__c = availableCourseForFakeUser.Description;
		if (availableCourseForFakeUser.AccessDate != null)
		{
			courseObj.AccessDate__c = Date.valueOf(availableCourseForFakeUser.AccessDate);
		}
		courseObj.Notes__c = availableCourseForFakeUser.Notes;
		courseObj.CategoryId__c = availableCourseForFakeUser.CategoryId;
		courseObj.ExpireType__c = mapForExpireType.get(availableCourseForFakeUser.ExpireType);
		courseObj.Duration__c = availableCourseForFakeUser.LearnerTime;
		if (availableCourseForFakeUser.ExpiryDate != null)
		{
			courseObj.ExpiryDate__c = Date.valueOf(availableCourseForFakeUser.ExpiryDate);
		}
		courseObj.ActiveStatus__c = mapForActiveStatus.get(availableCourseForFakeUser.ActiveStatus);
		courseObj.Type__c = availableCourseForFakeUser.CourseType;
		courseObj.Language__c = holdsLanguageToSplit[0];
		return courseObj;
	}
	/**
	* @description  Helper method to build curriculum record for creation
	*/
	private static Curriculum__c curriculumBuild(Map<String, String> mapForImages, LmsUtils.CourseDTOFrUpdate availableCourseForFakeUser, List<String> holdsLanguageToSplit)
	{
		Curriculum__c courseObj = new Curriculum__c();
		courseObj.Main_Course_Image__c = mapForImages.get('Main_Course_Image');
		courseObj.Related_Content_Image__c = mapForImages.get('Related_Content_Image');
		courseObj.Landing_page_Image__c = mapForImages.get('Landing_page_Image');
		courseObj.Online_CoursePage_Image__c = mapForImages.get('Online_CoursePage_Image');
		courseObj.Name = availableCourseForFakeUser.Name;
		courseObj.LMS_ID__c = availableCourseForFakeUser.Id;
		courseObj.Description__c = availableCourseForFakeUser.Description;
		if (availableCourseForFakeUser.AccessDate != null)
		{
			courseObj.AccessDate__c = Date.valueOf(availableCourseForFakeUser.AccessDate);
		}
		courseObj.Notes__c = availableCourseForFakeUser.Notes;
		courseObj.CategoryId__c = availableCourseForFakeUser.CategoryId;
		courseObj.ExpireType__c = mapForExpireType.get(availableCourseForFakeUser.ExpireType);
		courseObj.Duration__c = availableCourseForFakeUser.LearnerTime;
		if (availableCourseForFakeUser.ExpiryDate != null)
		{
			courseObj.ExpiryDate__c = Date.valueOf(availableCourseForFakeUser.ExpiryDate);
		}
		courseObj.ActiveStatus__c = mapForActiveStatus.get(availableCourseForFakeUser.ActiveStatus);
		courseObj.Type__c = availableCourseForFakeUser.CourseType;
		courseObj.Language__c = holdsLanguageToSplit[0];
		return courseObj;
	}
	/**
	* @description  Helper method to course and curriculum status update
	*/
	private static void statusUpdate(Map<String, String> mapForCurriculumDTOUpdate, Map<String, String> mapForOnlineCourseDTOUpdate, Map<String, String> mapForILTCourseDTOUpdate, List<LmsUtils.CourseDTOFrUpdate> availableCourseForFakeUser)
	{
		List<Categories_Tag__c> listForTags = [SELECT Id, LMS_ID__c, Name FROM Categories_Tag__c WHERE Type__c = 'Tag'];
		Map<String, Categories_Tag__c> mapForTags = new Map<String, Categories_Tag__c>();
		for (Categories_Tag__c holdsTag : listForTags)
		{
			mapForTags.put(holdsTag.LMS_ID__c, holdsTag);
		}
		List<Course__c> listForCourse = [SELECT Id, LMS_ID__c, Name, ActiveStatus__c, Type__c FROM Course__c WHERE Language__c != ''];
		Map<String, Course__c> mapForCourse = new Map<String, Course__c>();
		for (Course__c holdsCourse : listForCourse)
		{
			mapForCourse.put(holdsCourse.LMS_ID__c, holdsCourse);
		}
		List<Curriculum__c> listForCurriculum = [SELECT Id, LMS_ID__c, Name, ActiveStatus__c FROM Curriculum__c WHERE Language__c != ''];
		Map<String, Curriculum__c> mapForCurriculum = new Map<String, Curriculum__c>();
		for (Curriculum__c holdsCurriculum : listForCurriculum)
		{
			mapForCurriculum.put(holdsCurriculum.LMS_ID__c, holdsCurriculum);
		}
		List<CourseTagAssignment__c> listForCourseTagAssignment = [SELECT Id, CourseTagAssignedId__c, CustomCourse__c, CustomCurriculum__c, CustomTag__c FROM CourseTagAssignment__c];
		Map<String, CourseTagAssignment__c> mapForCourseTagAssignment = new Map<String, CourseTagAssignment__c>();
		for (CourseTagAssignment__c holdsCourseTagAssignment : listForCourseTagAssignment)
		{
			mapForCourseTagAssignment.put(holdsCourseTagAssignment.courseTagAssignedId__c, holdsCourseTagAssignment);
		}
		List<Curriculum__c> listForCurriculumStatusUpdate = new List<Curriculum__c>();
		for (Curriculum__c holdsCurriculum : listForCurriculum)
		{
			if (mapForCurriculumDTOUpdate.get(holdsCurriculum.LMS_ID__c) != null)
			{
				holdsCurriculum.ActiveStatus__c = mapForActiveStatus.get(Integer.valueOf(mapForCurriculumDTOUpdate.get(holdsCurriculum.LMS_ID__c)));
			}
			listForCurriculumStatusUpdate.add(holdsCurriculum);
		}
		List<Course__c> ListForCourseStatusUpdate = new List<Course__c>();
		for (Course__c holdsCourse : listForCourse)
		{
			if (holdsCourse.Type__c == 'OnlineCourse')
			{
				if (mapForOnlineCourseDTOUpdate.get(holdsCourse.LMS_ID__c) != null)
				{
					holdsCourse.ActiveStatus__c = mapForActiveStatus.get(Integer.valueOf(mapForOnlineCourseDTOUpdate.get(holdsCourse.LMS_ID__c)));
				}
				ListForCourseStatusUpdate.add(holdsCourse);
			}
			else if (holdsCourse.Type__c == 'InstructorLedCourse')
			{
				if (mapForILTCourseDTOUpdate.get(holdsCourse.LMS_ID__c) != null)
				{
					holdsCourse.ActiveStatus__c = mapForActiveStatus.get(Integer.valueOf(mapForILTCourseDTOUpdate.get(holdsCourse.LMS_ID__c)));
				}
				ListForCourseStatusUpdate.add(holdsCourse);
			}
		}
		if (!listForCurriculumStatusUpdate.isEmpty())
		{
			update listForCurriculumStatusUpdate;
		}
		if (!ListForCourseStatusUpdate.isEmpty())
		{
			update ListForCourseStatusUpdate;
		}
		assignmentsupdate(availableCourseForFakeUser, mapForTags, mapForCourse, mapForCurriculum);
	}
	/**
	* @description  Helper method to coursetag assignments
	*/
	private static void assignmentsupdate(List<LmsUtils.CourseDTOFrUpdate> availableCourseForFakeUser, Map<String, Categories_Tag__c> mapForTags, Map<String, Course__c> mapForCourse, Map<String, Curriculum__c> mapForCurriculum)
	{
		List<CourseTagAssignment__c> listForCourseTagAssignment = new List<CourseTagAssignment__c>();
		for (LmsUtils.CourseDTOFrUpdate availableCourseForFakeUservariable : availableCourseForFakeUser)
		{
			if (availableCourseForFakeUservariable.CourseType == 'OnlineCourse' || availableCourseForFakeUservariable.CourseType == 'InstructorLedCourse')
			{
				List<CourseTagAssignment__c> listForCourseAssignment = new List<CourseTagAssignment__c>();
				Course__c holdsCourses = [SELECT Id, (SELECT Id, CourseTagAssignedId__c, CustomCourse__c, CustomCurriculum__c, CustomTag__c FROM CourseTagAssignments__r) FROM Course__c WHERE LMS_ID__c = :availableCourseForFakeUservariable.Id];
				for (CourseTagAssignment__c holdsCourseAssignment : holdsCourses.CourseTagAssignments__r)
				{
					listForCourseAssignment.add(holdsCourseAssignment);
				}
				if (!listForCourseAssignment.isEmpty())
				{
					delete listForCourseAssignment;
				}
				for (String holdsTagId : availableCourseForFakeUservariable.TagIds)
				{
					CourseTagAssignment__c holdsCourseTagAssignment = new CourseTagAssignment__c();
					holdsCourseTagAssignment.CustomTag__c = mapForTags.get(holdsTagId).Id;
					holdsCourseTagAssignment.CustomCourse__c = mapForCourse.get(availableCourseForFakeUservariable.Id).id;
					holdsCourseTagAssignment.courseTagAssignedId__c = availableCourseForFakeUservariable.Id + '__' + holdsTagId;
					holdsCourseTagAssignment.name = availableCourseForFakeUservariable.Id + '__' + holdsTagId;
					listForCourseTagAssignment.add(holdsCourseTagAssignment);
				}
			}
			else if (availableCourseForFakeUservariable.CourseType == 'Curriculum')
			{
				List<CourseTagAssignment__c> listForCourseAssignment = new List<CourseTagAssignment__c>();
				Curriculum__c holdsCurriculums = [SELECT Id, (SELECT Id, CourseTagAssignedId__c, CustomCourse__c, CustomCurriculum__c, CustomTag__c FROM CourseTagAssignments__r) FROM Curriculum__c WHERE LMS_ID__c = :availableCourseForFakeUservariable.Id];
				for (CourseTagAssignment__c holdsCourseAssignment : holdsCurriculums.CourseTagAssignments__r)
				{
					listForCourseAssignment.add(holdsCourseAssignment);
				}
				if (!listForCourseAssignment.isEmpty())
				{
					delete listForCourseAssignment;
				}
				for (String holdsTagId : availableCourseForFakeUservariable.TagIds)
				{
					CourseTagAssignment__c holdsCourseTagAssignment = new CourseTagAssignment__c();
					holdsCourseTagAssignment.CustomTag__c = mapForTags.get(holdsTagId).Id;
					holdsCourseTagAssignment.CustomCurriculum__c = mapForCurriculum.get(availableCourseForFakeUservariable.Id).id;
					holdsCourseTagAssignment.courseTagAssignedId__c = availableCourseForFakeUservariable.Id + '__' + holdsTagId;
					holdsCourseTagAssignment.name = availableCourseForFakeUservariable.Id + '__' + holdsTagId;
					listForCourseTagAssignment.add(holdsCourseTagAssignment);
				}
			}
		}
		if (!listForCourseTagAssignment.isEmpty())
		{
			upsert listForCourseTagAssignment courseTagAssignedId__c;
		}
	}
	/**
	* @description  Method for getting Resources based on ChapterId from absorb lms
	*/
	public static List<ResourcesDTOForUpdate> getResourcesForCourse(String courseId)
	{
		HttpResponse response;
		List<ResourcesDTOForUpdate> resourcesList = new List<ResourcesDTOForUpdate>();
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mockResponseObject = new MockHttpResponseGenerator1();
			response = mockResponseObject.respond6();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues('EndPoint').Value__c + 'v1/courses/' + courseId + '/resources', '', 'GET', lmsUtils.authenticate());
		}
		if (response.getStatusCode() == 200)
		{
			resourcesList = (List<ResourcesDTOForUpdate>) JSON.deserialize(response.getBody(), List<ResourcesDTOForUpdate>.class);
		}
		return resourcesList;
	}
	/**
	* @description  Method for getting Resources based on ChapterId from absorb lms
	*/
	public static List<CurriculumDTOForUpdate> getCurriculum()
	{
		HttpResponse response;
		List<CurriculumDTOForUpdate> curriculumObjList = new List<CurriculumDTOForUpdate>();
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mockResponseObject = new MockHttpResponseGenerator1();
			response = mockResponseObject.respond6();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues('EndPoint').Value__c + 'v1/Curriculums', '', 'GET', LmsUtils.authenticate());
		}
		if (response.getStatusCode() == 200)
		{
			curriculumObjList = (List<CurriculumDTOForUpdate>) JSON.deserialize(response.getBody(), List<CurriculumDTOForUpdate>.class);
		}
		return curriculumObjList ;
	}
	/**
	* @description  Method for getting Resources based on ChapterId from absorb lms
	*/
	public static List<OnlineCourseDTOForUpdate> getOnlineCourse()
	{
		HttpResponse response;
		List<OnlineCourseDTOForUpdate> OnlineCourseObjList = new List<OnlineCourseDTOForUpdate>();
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mockResponseObject = new MockHttpResponseGenerator1();
			response = mockResponseObject.respond6();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues('EndPoint').Value__c + 'v1/OnlineCourses', '', 'GET', LmsUtils.authenticate());
		}
		if (response.getStatusCode() == 200)
		{
			OnlineCourseObjList = (List<OnlineCourseDTOForUpdate>) JSON.deserialize(response.getBody(), List<OnlineCourseDTOForUpdate>.class);
		}
		return OnlineCourseObjList;
	}
	/**
	* @description  Method for getting Resources based on ChapterId from absorb lms
	*/
	public static List<ILTCourseDTOForUpdate> getILTCourse()
	{
		HttpResponse response;
		List<ILTCourseDTOForUpdate> ILTCourseObjLst = new List<ILTCourseDTOForUpdate>();
		if (Test.isRunningTest())
		{
			MockHttpResponseGenerator1 mockResponseObject = new MockHttpResponseGenerator1();
			response = mockResponseObject.respond6();
		}
		else
		{
			response = LmsUtils.processRequest(LMS_Settings__c.getValues('EndPoint').Value__c + 'v1/InstructorLedCourses', '', 'GET', LmsUtils.authenticate());
		}
		if (response.getStatusCode() == 200)
		{
			ILTCourseObjLst = (List<ILTCourseDTOForUpdate>) JSON.deserialize(response.getBody(), List<ILTCourseDTOForUpdate>.class);
		}
		return ILTCourseObjLst;
	}
	public class ILTCourseDTOForUpdate extends BaseRestResponse
	{
		public String Id { get; set; }
		public String ActiveStatus { get; set; }
		public override void restResponse()
		{/*override method*/
		}
	}
	public class ResourcesDTOForUpdate extends BaseRestResponse
	{
		public String Name { get; set; }
		public String File { get; set; }
		public override void restResponse()
		{/*override method*/
		}
	}
	public class OnlineCourseDTOForUpdate extends BaseRestResponse
	{
		public String Id { get; set; }
		public String ActiveStatus { get; set; }
		public override void restResponse()
		{/*override method*/
		}
	}
	public class CurriculumDTOForUpdate extends BaseRestResponse
	{
		public String Id { get; set; }
		public String ActiveStatus { get; set; }
		public override void restResponse()
		{/*override method*/
		}
	}
}