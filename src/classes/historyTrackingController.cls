/**
*      @author Kavita Dodamani
*      @date   27/09/2016
@description    Class Handles the logic used in custom History tracking 
Modification Log:
------------------------------------------------------------------------------------
Developer                       Date                Description
Kavita Dodamani                 27/09/2016          initial version 
------------------------------------------------------------------------------------
*/
Public class historyTrackingController{
    Public ID RecordID {set;get;}
    
    Public historyTrackingController(){
        RecordID = String.valueOf((Id)(ApexPages.currentPage().getParameters().get('id')));
    }
    
    public list<cHistories> getHistories() {
        list<cHistories> list_ch = new list<cHistories>();
        for (JJ_JPN_FieldHistoryTracking__c fh: [SELECT Name ,JJ_JPN_apiName__c,JJ_JPN_NewValue__c,JJ_JPN_OldValue__c,JJ_JPN_Object__c,JJ_JPN_RecordID__c,CreatedDate,CreatedById,CreatedBy.Name FROM JJ_JPN_FieldHistoryTracking__c where JJ_JPN_RecordID__c  =:RecordID order by CreatedDate desc])
        {
            // Create a new wrapper object
            cHistories ch = new cHistories();
            ch.theDate = String.valueOf(fh.createddate);
            ch.who = fh.createdby.name;
            if (fh.JJ_JPN_OldValue__c!= null && fh.JJ_JPN_NewValue__c== null){ // when deleting a value from a field
                try {
                    ch.action = 'Deleted ' + Date.valueOf(fh.JJ_JPN_OldValue__c).format() + ' in <b>' + String.valueOf(fh.name) + '</b>.';
                } catch (Exception e){
                    ch.action = 'Deleted ' + String.valueOf(fh.JJ_JPN_OldValue__c) + ' in <b>' + String.valueOf(fh.name) + '</b>.';
                }
             } else {
                String fromText = '';
                if (fh.JJ_JPN_OldValue__c!= null) {
                    try {
                        fromText = ' from ' + Date.valueOf(fh.JJ_JPN_OldValue__c).format();
                    } catch (Exception e) {
                        fromText = ' from ' + String.valueOf(fh.JJ_JPN_OldValue__c);
                    }
                }
                String toText = '';
                try {
                    toText =   Date.valueOf(fh.JJ_JPN_NewValue__c).format();
                } catch (Exception e) {
                    toText = '' + String.valueOf(fh.JJ_JPN_NewValue__c);
                }
                ch.action = 'Changed <b>' + String.valueOf(fh.name) + '</b>' + fromText + ' to <b>' + toText + '</b>.';
            }
            list_ch.add(ch);
         }
        system.debug('list_ch'+list_ch);
        return list_ch;
    }
    
    public class cHistories {
        // Class properties
        public String theDate {get; set;}
        public String who {get; set;}
        public String action {get; set;}
    }
    
    public PageReference back(){
        PageReference pageRef = new PageReference('/'+RecordID);
        return   pageRef ;
    }
}