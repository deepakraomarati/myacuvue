@isTest
public class historyTrackingControllerTest {
    
     Public Static Testmethod void historyTrackingControllerTest1()
    {
        test.startTest();
        historyTrackingController htc=new historyTrackingController();
        JJ_JPN_FieldHistoryTracking__c f=new JJ_JPN_FieldHistoryTracking__c (name='Test');
        insert f;
        
        htc.RecordID=f.id;
        f.JJ_JPN_RecordID__c=htc.RecordID;
        update f;
        htc.back();
        htc.getHistories();
         f.JJ_JPN_OldValue__c=htc.RecordID;
         update f;
        htc.back();
        htc.getHistories();
        test.stopTest();
    }

}