@isTest
public class lastCallOutcomeTest{
    @isTest
    public static void updateLastCallOutcome(){
        
        Profile pro = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'testing1', Email='standardtest1@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Jain', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = pro.Id,
                          TimeZoneSidKey='America/Los_Angeles', UserName='testing1@test.com.staging',Unique_User_Id__c = 'testing1',country='India');
        insert u;
        List<RecordType> eveRec = [select id from RecordType  where name='Customer Development Manager Call KR' limit 1];
        Account acc= new Account(name= 'ABC Company');
        insert acc;
        system.assertEquals(acc.Name,'ABC Company','success');
        system.debug('Userrrrr----'+u.id);
        
        Event eve = new Event(RecordTypeId = eveRec[0].id ,WhatId=acc.id,Subject='Call1', ActivityType__c= 'Customer Visit (1 call)',
                              OwnerId =u.Id,StartDateTime=datetime.newInstance(2016, 10, 03, 05, 00, 0),
                              EndDateTime=datetime.newInstance(2016, 10, 03, 06, 00, 0),ActivityStatus__c='Completed',
                              CallOutcomeCallTargetComments__c='Testing');
        insert eve;
        Test.startTest();
        system.debug('eveeeeeeeeeeeeee'+eve.id);
        Event eve2 = new Event(RecordTypeId = eveRec[0].id ,WhatId=acc.id,Subject='Call2', ActivityType__c='Customer Visit (1 call)',
                               OwnerId =u.Id,StartDateTime=datetime.newInstance(2016, 10, 17, 05, 00, 0),
                               EndDateTime=datetime.newInstance(2016, 10, 17, 06, 00, 0),ActivityStatus__c='Planned');
        insert eve2;
        system.debug('eve+++'+eve);
        try{
            eve.CallOutcomeCallTargetComments__c='Testing update';
            update eve;
            update eve2;
        }
        catch(exception e){
        }
        test.stopTest();
    }
}