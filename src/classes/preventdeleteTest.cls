@istest
public class preventdeleteTest
{
    static testmethod void preventdeleteTest()
    {
        Profile p1 = [SELECT Id FROM Profile WHERE Name='ASPAC JPN PA'];
        User u1 = new User(Alias = 'test1', Email='standard1@testorg.com', EmailEncodingKey='UTF-8', LastName='Test1', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p1.Id,  TimeZoneSidKey='America/Los_Angeles', UserName='test1@test.com.staging',Unique_User_Id__c = 'test1');
        insert u1;
        
        system.runAs(u1)
        {
            Id devRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Dr').getRecordTypeId();
            Account acc = new Account();
            acc.name = 'Test Account';
            acc.CountryCode__c = 'JPN';
            insert acc;
            system.assertEquals(acc.Name,'Test Account','success');    
            
            contact con = new Contact();
            con.LastName = 'Test Contact';
            con.RecordTypeId = devRecordTypeId;
            insert con;
            
            con.LastName = 'test update';
            update con;
            
            con.LastName = 'test delete';
            delete con;
        }        
    }
}