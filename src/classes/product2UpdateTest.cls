@isTest
public class product2UpdateTest {
    static testMethod void validateProductUpdate(){
        Event newEvent = new Event();
        RecordType rt = [Select id from RecordType where sobjecttype='Event' and Name='KAM Call SG'];
        newEvent.RecordTypeId=rt.id;
        newEvent.Subject = 'Testing trigger';
        newEvent.StartDateTime=date.newinstance(2016, 1, 20);
        newEvent.EndDateTime=date.newinstance(2016, 1, 20);
        newEvent.Objective2__c='Any Other Business';
        insert newEvent;
        system.assertEquals(newEvent.Objective2__c,'Any Other Business','success');  
    }
}