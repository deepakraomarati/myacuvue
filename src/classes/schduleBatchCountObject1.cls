global class schduleBatchCountObject1 implements Schedulable {

   global void execute(SchedulableContext ctx) {
      
            String query='select Id from account';   
            ObjectUpdateBatch  BP= new ObjectUpdateBatch(query,1);
            database.executebatch(BP);
            
            query='select Id from contact';   
            ObjectUpdateBatch  BP1= new ObjectUpdateBatch(query,2);
            database.executebatch(BP1);   
            
            query='select Id from Batch_Details__c';   
            ObjectUpdateBatch  BP2= new ObjectUpdateBatch(query,3);
            database.executebatch(BP2);

            query='select Id from Product2';   
            ObjectUpdateBatch  BP3= new ObjectUpdateBatch(query,4);
            database.executebatch(BP3);   

            query='select Id from campaign';   
            ObjectUpdateBatch  BP4= new ObjectUpdateBatch(query,5);
            database.executebatch(BP4);  
            

      

   } 
      
}