@isTest

public with sharing class testGetPointHistory
{
  static testmethod void doinsertGetPointHistory ()
  {
    PointHistory__c phs = new PointHistory__c();
    phs.Aws_AccountId__c ='123';
    phs.Aws_ContactId__c ='1234';
    phs.Aws_TransactionTd__c='12345';
    insert phs;
    
    account acc = new account();
    acc.name='test';
    acc.Aws_AccountId__c ='123';
    insert acc;
    system.assertEquals(acc.name,'test','success');  
    
   
  }
}