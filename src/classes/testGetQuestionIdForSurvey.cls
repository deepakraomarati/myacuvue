@isTest

public with sharing class testGetQuestionIdForSurvey 
{
    static testmethod void doinsertGetQuestionIdForSurvey   ()
    {
        SurveyAnswer__c surv=new  SurveyAnswer__c();
        surv.Aws_Survey__c ='12';
        surv.Aws_SurveyQuestion__c ='123';
        surv.Aws_SurveyQuestionItem__c ='1234';
        surv.Aws_ContactId__c ='12345';  
        insert surv;
        system.assertEquals(surv.Aws_Survey__c,'12','success');  
        
        Survey__c s =new Survey__c ();
        s.name='test1';
        s.s_seq__c =12;
        insert s;
        
        SurveyQuestion__c sq = new SurveyQuestion__c ();
        //sq.name ='test2';
        sq.q_seq__c =123;
        insert sq;
        
        SurveyQuestionItem__c sql=new SurveyQuestionItem__c();
        sql.name='test3';
        sql.i_seq__c =1234;
        insert sql;
      
        contact con = new contact();
        con.lastname='test4';
        con.Aws_ContactId__c ='12345';
        con.MA2_Country_Code__c='TWN';
        //insert con;
        
        SurveyAnswer__c surv1=new  SurveyAnswer__c();
        surv1.id=surv.id;
        surv1.s_seq__c= s.id;
        surv1.q_seq__c = sq.id;
        surv1.i_seq__c = sql.id;
        update surv1;   
    }
}