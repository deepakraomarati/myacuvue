@isTest
private class testLenspalProfile{
    
    static testmethod void profileLenspal(){
        
        Account ac       = new Account();
        ac.name     = 'Test';
        ac.OutletNumber__c ='56789';
        insert ac;
        system.assertEquals(ac.name,'Test','success');  
        
        contact con = new contact();
        con.firstname='test';
        con.lastname='raj';
        con.Email='testsekhar@bics.com';
        con.accountid=ac.id;
        insert con;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        String json1 ='{"ContactID":"","OutLetNumber":"12345","Email":"testraj@bics.com"}'; 
        req.requestURI = 'services/apexrest/Apex/LenspalProfile/';   
        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueof(json1);
        RestContext.request = req;
        RestContext.response = res;    
        LenspalProfile.doput();
        
        String json2 ='{"ContactId":"'+con.Id+'","OutLetNumber":"56789","Email":"testsekhar@bics.com"}'; 
        req.requestURI = 'services/apexrest/Apex/LenspalProfile/';   
        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueof(json2);
        RestContext.request = req;
        RestContext.response = res;    
        LenspalProfile.doput();
    }
}