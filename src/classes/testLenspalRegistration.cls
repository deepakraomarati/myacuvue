@isTest
private class testLenspalRegistration{
    
    static testmethod void registerLenspal(){

        id rt =Schema.SObjectType.contact.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();         
        Configuration_Setting__c cs = new Configuration_Setting__c();
        cs.name='Contact_RecordType_Id';
        cs.Value__c=rt;
        insert cs;
        system.assertEquals(cs.name,'Contact_RecordType_Id','success');
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        String json1 ='{"FirstName":"","Email":"sekhar@thylaksoft.com","MobilePhone":"345678","LensType":true,"Gender":"0","Country":"AUSTRALIA","PrivacyPolicy":true,"CommunicationAgreement":true,"DateOfBirth": "02/3/1985"}';    
        req.requestURI = 'services/apexrest/Apex/LenspalRegistration/';   
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(json1);
        RestContext.request = req;
        RestContext.response = res;    
        LenspalRegistration.dopost();
        
        String json2 ='{"FirstName":"testraj","Email":"Raja@thylaksoft.com","MobilePhone":"3456789","LensType":true,"Gender":"0","Country":"UNITED KINGDOM","PrivacyPolicy":true,"CommunicationAgreement":true,"DateOfBirth": "05/5/1989"}';  
        req.requestURI = 'services/apexrest/Apex/LenspalRegistration/';   
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(json2);
        RestContext.request = req;
        RestContext.response = res;    
        LenspalRegistration.dopost();
        
        LenspalRegistration.LensDTO lr= new LenspalRegistration.LensDTO('test','test@bics.com','9848022338',true,'','UNITED KINGDOM',true,true,null);   
    }
}