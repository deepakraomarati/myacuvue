@isTest
private class testLenspalTIP{
  
    static testmethod void TIPLenspal(){
        
        Account ac       = new Account();
        ac.name     = 'Test';
        ac.OutletNumber__c ='1235';
        insert ac;
        system.assertEquals(ac.name,'Test','success');  
        
        contact con = new contact();
        con.firstname='test';
        con.lastname='raj';
        con.Email='testsekhar@bics.com';
        con.accountid=ac.id;
        insert con;            
        
        Account ac1       = new Account();
        ac1.name     = 'Testraja';
        ac1.OutletNumber__c ='56789';
        insert ac1;
        
        contact con1 = new contact();
        con1.firstname='test';
        con1.lastname='sekhar';
        con1.Email='testsekhart@bics.com';
        con1.accountid=ac1.id;
        insert con1; 
        
        TipsRaw__c tr=new TipsRaw__c();
        tr.TIP_Aggregator__c='testagg';
        tr.TIP_Category__c='testcatgry';
        tr.TIP_Description__c='testdescrption';
        tr.TIP_Id__c='testid';
        tr.TIP_Image__c='www.gmail.com';
        tr.TIP_LinkId__c='www.bics.com';
        insert tr;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = 'services/apexrest/Apex/LenspalTIP/testid';  
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;  
        LenspalTIP.doGet();
        req.requestURI = 'services/apexrest/Apex/LenspalTIP/2';  
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;  
        LenspalTIP.doGet();
        
        req.requestURI = 'services/apexrest/Apex/LenspalTIP//';  
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;  
        LenspalTIP.doGet();
        
        String json1 ='[{"ContactId":"'+con1.Id+'","TIPID":"testid","SymptomId":"2","Status":false,"RegisteredDate":"11/12/1986"}]';    
        req.requestURI = 'services/apexrest/Apex/LenspalTIP';   
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(json1);
        RestContext.request = req;
        RestContext.response = res;    
        LenspalTIP.dopost();
        
        String json3 ='[{"ContactId":"","TIPID":"testid","SymptomId":"2","Status":false,"RegisteredDate":"11/12/1986"}]';    
        req.requestURI = 'services/apexrest/Apex/LenspalTIP';   
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(json3);
        RestContext.request = req;
        RestContext.response = res;    
        LenspalTIP.dopost();
        
        String json2 ='[{"ContactId":"'+con1.Id+'","TIPID":"2","SymptomId":"2","Status":false,"RegisteredDate":"11/12/1985"}]';    
        req.requestURI = 'services/apexrest/Apex/LenspalTIP';   
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(json2);
        RestContext.request = req;
        RestContext.response = res;    
        LenspalTIP.dopost();
        
        LenspalTIP.TIPInfo lr= new LenspalTIP.TIPInfo('con1.Id','testid','2','true','');   
    }
}