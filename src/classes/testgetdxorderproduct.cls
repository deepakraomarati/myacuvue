@isTest

public with sharing class testgetdxorderproduct 
{
    static testmethod void doinsertgetdxorderproduct  ()
    {
        DxOrderProduct__c dxorder = new DxOrderProduct__c ();
        dxorder .name='test';
        dxorder .UPC__c='123';
        dxorder.Aws_product__c='123';   
        insert dxorder ;
        system.assertEquals(dxorder.name,'test','success'); 
        
        Product2 p =new Product2();
        p.name='test1';
        //p.UPC_Code__c='123';
        p.InternalName__c='Abc';
        insert p;
        
        DxOrderProduct__c dx1 = new DxOrderProduct__c ();
        dx1.id=dxorder .id;
        dx1.Product__c=p.id;
        update dx1;
    }
}