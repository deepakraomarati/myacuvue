public with sharing class updateavailabledays{
    
public static void updtavldays(list<event> evt){
date totaldays;
date i;
datetime j;
integer counter=0;
date stratdate=null;
date enddate=null;
list<event>listtoinsert=new list <event>();
id usr=UserInfo.getUserId();

list<event> nonwrkingday=[select id,StartDateTime,EndDateTime from event where ActivityType__c='Non Working Day' and  Country__c !='Japan' and ownerid=:usr ];
list<event> annualleave=[select id,StartDateTime,EndDateTime from event where ActivityType__c in ('Sick leave (full day)','Annual leave (full day)') and  Country__c !='Japan' and ownerid=:usr ];
set<date>annualleaveset=new set<date>();

set<date>nonwrkingdayset=new set<date>();

for(event e : nonwrkingday){
stratdate=date.valueof(string.valueof(e.StartDateTime).substring(0,string.valueof(e.StartDateTime).indexof(' ')));
enddate=date.valueof(string.valueof(e.EndDateTime).substring(0,string.valueof(e.EndDateTime).indexof(' ')));
i=stratdate;
while(i<=enddate){

            nonwrkingdayset.add(i);
            i=i+1;
        }

}
for(event e : annualleave){
stratdate=date.valueof(string.valueof(e.StartDateTime).substring(0,string.valueof(e.StartDateTime).indexof(' ')));
enddate=date.valueof(string.valueof(e.EndDateTime).substring(0,string.valueof(e.EndDateTime).indexof(' ')));
i=stratdate;
while(i<=enddate){

            annualleaveset.add(i);
            i=i+1;
        }

 }
system.debug('annualleaveset>>>'+annualleaveset);



for (event e : evt){
    if((e.ActivityType__c=='Sick leave (full day)'|| e.ActivityType__c=='Annual leave (full day)') && e.recordtype.developername!='JJ_JPN_CustomerDevelopmentManagerCallJPN' && e.recordtype.developername!='JJ_JPN_TimeOffTerritoryJPN'){
        if(!nonwrkingdayset.isempty() && nonwrkingdayset.contains(date.valueof(string.valueof(e.StartDateTime).substring(0,string.valueof(e.StartDateTime).indexof(' '))))){
            e.adderror('User Cannot apply leave on Non-Working day');
        }
        else if(annualleaveset.contains(date.valueof(string.valueof(e.StartDateTime).substring(0,string.valueof(e.StartDateTime).indexof(' '))))){
            e.adderror('You have already applied for leave on this day');
        }
        else
        {
    stratdate=date.valueof(string.valueof(e.StartDateTime).substring(0,string.valueof(e.StartDateTime).indexof(' ')));
    enddate=date.valueof(string.valueof(e.EndDateTime).substring(0,string.valueof(e.EndDateTime).indexof(' ')));
    if(stratdate!=enddate){        
        j=e.StartDateTime+1;
        while(j<e.EndDateTime){
            if(!nonwrkingdayset.contains(date.valueof(string.valueof(j).substring(0,string.valueof(j).indexof(' ')))) && !annualleaveset.contains(date.valueof(string.valueof(j).substring(0,string.valueof(j).indexof(' '))))){
            event ev=e.clone();
            ev.StartDateTime=j;
            ev.ActivityDateTime =j;
            ev.ActivityDate=date.valueof(j);
            ev.EndDateTime=j.addMinutes(30);
            ev.DurationInMinutes=integer.valueof((ev.EndDateTime.gettime()-ev.StartDateTime.gettime())/(1000*60));

              listtoinsert.add(ev);
            }
                            j=j+1;
        }
        e.EndDateTime=e.StartDateTime.addMinutes(30);
        e.DurationInMinutes=integer.valueof((e.EndDateTime.gettime()-e.StartDateTime.gettime())/(1000*60));
        
        database.saveresult[] sv=database.insert(listtoinsert,false);
     }
      } 
    }
}
    }

}