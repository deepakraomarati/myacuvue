@istest
public class updateavailabledays_Test {
    public TestMethod static void updtavldays_Test(){
        User user = [SELECT Id FROM User WHERE IsActive = True and Profile.Name='System Administrator' and Country !='Japan' limit 1];
        system.debug('---usr---'+user);
        String name='test';
        system.assertEquals(name,'test','success'); 
        //User user = [SELECT Id FROM User WHERE id=:usr];
        System.runAs(user)
        {    
            List<Event> event3= new List<Event>();
            for (Integer m=0;m<5; m++ )
            {
                event3.add(new Event(StartDateTime=system.now(),EndDateTime=system.now()+1,ActivityType__c='Annual leave (full day)',Notes2__c='Event'+m));
            }
            insert event3;
            List<Event> event= new List<Event>();
            for (Integer m=0;m<5; m++ )
            {
                event.add(new Event(StartDateTime=system.now(),EndDateTime=system.now()+1,ActivityType__c='Non Working Day',Notes2__c='Event'+m));
            }
            insert event;
            updateavailabledays.updtavldays(event);
            
            List<Event> eventNew= new List<Event>();
            id recordtypes1=[select id from recordtype where developername='CustomerDevelopmentManagerCallANZ'].id;
            for (Integer m=0;m<5; m++ )
            {
                eventNew.add(new Event(StartDateTime=system.now(),EndDateTime=system.now()+1,ActivityType__c='Sick leave (full day)',Notes2__c='Event'+m, recordtypeId=recordtypes1));
            }
            insert eventNew;
        
        }
    } 
    
}