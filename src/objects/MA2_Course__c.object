<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>MA2_Active__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Status(2 draft, 3 published)</description>
        <externalId>false</externalId>
        <label>Active</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>MA2_Amount__c</fullName>
        <description>Thumb up amount</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Amount</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MA2_Content__c</fullName>
        <description>Body content</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Content</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MA2_CountryCode__c</fullName>
        <description>3 Digit Country Code</description>
        <externalId>false</externalId>
        <label>Country Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>Country_Code</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>MA2_CourseDate__c</fullName>
        <description>Course Planned Date</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Course Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>MA2_CourseType__c</fullName>
        <description>Course level. Such as primary course, intermediate course</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Course Type</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MA2_Detailed_address__c</fullName>
        <description>Detailed address</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Detailed address</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>MA2_EducationCategory__c</fullName>
        <description>Education category</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Education Category</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MA2_EndTime__c</fullName>
        <description>End time</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Course End Time</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>MA2_InCharge__c</fullName>
        <description>Speaker Details related</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>In Charge</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MA2_Introduction__c</fullName>
        <description>Speaker introduction</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Speaker Introduction</label>
        <length>10000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>MA2_Productseries__c</fullName>
        <description>Product series</description>
        <externalId>false</externalId>
        <label>Product Series</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MA2_Region__c</fullName>
        <description>Region / Zone details</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Region</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MA2_SeqNo__c</fullName>
        <description>Course ID</description>
        <externalId>true</externalId>
        <label>Course Seq No</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>MA2_ShareAmount__c</fullName>
        <description>Share amount</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Share Amount</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MA2_StartTime__c</fullName>
        <description>Start time</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Course Start Time</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>MA2_Title__c</fullName>
        <description>Title of the course</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Course Title</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Course</label>
    <nameField>
        <encrypted>false</encrypted>
        <label>Course Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Courses</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
