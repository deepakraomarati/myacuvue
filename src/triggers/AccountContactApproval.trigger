/**
* File Name: AccountContactApproval
* Author : Venkata Mahesh | BICSGLOBAL
* Date Last Modified:  18-Oct-2018
* Description : create approval record for staff member registrations send email to Store manager
* Copyright (c) $2018 Johnson & Johnson
*/
trigger AccountContactApproval on AccountContactRole__c (after insert, after update)
{
	if (trigger.isinsert)
	{
		B2B_Utils BU = new B2B_Utils();
		BU.CreateApproval(trigger.new);
	}
}