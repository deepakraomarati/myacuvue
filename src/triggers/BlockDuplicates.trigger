trigger BlockDuplicates on Event_Attendee__c (before insert, before update) {

    // 2016.09.23 Maciej Szymczak mszymcz1@its.jnj.com +48604224658 Created 

    Id[] ids = new List<Id>();
    for (Event_Attendee__c ea :trigger.new) {
        ids.add(ea.Custom_Event__c); 
    }
    
    Map<Id,Custom_Event__c> customEvents = new map<Id,Custom_Event__c>([
      select Id
           , Subject__c
           , Invitees_Country__c 
        from Custom_Event__c 
       where Id in :ids]);

    for (Event_Attendee__c ea :trigger.new) {
      if (customEvents.containsKey(ea.Custom_Event__c) &&  'Attended;Confirmed;Pending;Wait List'.contains(ea.Invitee_Status__c) && ea.Allow_Duplicate__c==false) {
        ea.UniqueId__c = customEvents.get(ea.Custom_Event__c).Subject__c + ':' + ((string)ea.Contact__c).left(15); 
      }
      else {
        //uniqueness is disabled
        ea.UniqueId__c = ea.Id; 
      } 
    }

}