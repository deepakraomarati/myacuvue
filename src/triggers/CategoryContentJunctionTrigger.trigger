trigger CategoryContentJunctionTrigger on JnJDSF4__Cat_Content_Junction__c (before insert, before update) {
List<String> contentDocumentIds = new List<String>();
     
    for(SObject newItem : Trigger.new) {
        JnJDSF4__Cat_Content_Junction__c catContentJunction = (JnJDSF4__Cat_Content_Junction__c) newItem;
        if(catContentJunction.JnJDSF4__ContentId__c != null) {
            contentDocumentIds.add(catContentJunction.JnJDSF4__ContentId__c);
        }
    }
    
    Map<Id, ContentDocument> contentDocuments = new Map<Id, ContentDocument>([SELECT Title FROM ContentDocument WHERE Id IN :contentDocumentIds]);
    
    for(SObject newItem : Trigger.new) {
        JnJDSF4__Cat_Content_Junction__c catContentJunction = (JnJDSF4__Cat_Content_Junction__c) newItem;
        ContentDocument contentDocument = contentDocuments.get(catContentJunction.JnJDSF4__ContentId__c);
        if(contentDocument != null) {
            catContentJunction.ContentTitle__c = contentDocument.Title;
        }
    }
}