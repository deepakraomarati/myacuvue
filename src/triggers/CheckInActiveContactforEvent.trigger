trigger CheckInActiveContactforEvent on Event (before insert) {
integer counter=0;//Restricting this logic for Japan
for(Event evtJPN : Trigger.new)
    if(evtJPN.Country__c=='Japan'){
    counter++;
}
    if(counter>0){
    return;
}

 List<TriggerHandler__c> lstTrigger = TriggerHandler__c.getall().values();
 if(!lstTrigger.isEmpty()){
 if (lstTrigger[0].IsCheckInActiveContactforEventActive__c == true) {
List<Event> listEvents = trigger.new;
List<Id> listIds = new List<Id>();
        
        for(Event e : listEvents)
        {
          if(e.whoid != null){
          listIds.add(e.whoid);
          }
        }
 
        Map<Id,Contact> mapContacts = new Map<Id,Contact>([Select id,inactive__c from Contact where id in:listIds]);


        if(mapContacts.size()>0)
           {
            for(Event e:listEvents)
            { 
   
                  Contact c = mapContacts.get(e.whoid);
                      if(c.inactive__c==true)
                       {
                            e.addError('Event Cannot be created for an InActive Contact');
                          }
                }
           }

}
}
}