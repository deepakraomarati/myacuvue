trigger CheckInActiveContactforTask on Task (before insert) {
List<Task> listTasks = trigger.new;
List<Id> listIds = new List<Id>();
for(Task t : listTasks)
{
  listIds.add(t.whoid);
}
 
Map<Id,Contact> mapContacts = new Map<Id,Contact>([Select id,inactive__c from Contact where id in:listIds]);
 if(mapContacts.size()>0)
   {
for(Task t:listTasks)
{
  Contact c = mapContacts.get(t.whoid);
  if(c.inactive__c==true)
  {
    t.whoid.addError('Task cannot be created for an InActive Contact');
  }
}
}
}