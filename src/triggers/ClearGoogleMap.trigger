trigger ClearGoogleMap on Account (before update) {
    List<TriggerHandler__c> lstTrigger = TriggerHandler__c.getall().values();
    // Get country codes eligible to have Geolocation loaded or cleared
    Map<String,Canvas_Geolocation_CountryCodes__c> countryCodeMap = Canvas_Geolocation_CountryCodes__c.getAll(); 
    
    if(!lstTrigger.isEmpty()){
    if (lstTrigger[0].ClearGoogleMap__c == true) {


     if(Trigger.isUpdate){
         for(Account a:trigger.new){
            if(a.PublicAddress__c==null && a.PublicState__c==null && a.PublicZone__c==null ){
                a.GoogleMapX__c=0;
                a.GoogleMapY__c=0;
                // Update Geolocation for accounts with eligible country codes
                a.Geolocation__Latitude__s = (a.CountryCode__c != null && countryCodeMap.containsKey(a.CountryCode__c.toUpperCase())) ? null : a.Geolocation__Latitude__s;
                a.Geolocation__Longitude__s = (a.CountryCode__c != null && countryCodeMap.containsKey(a.CountryCode__c.toUpperCase())) ? null : a.Geolocation__Longitude__s;
                System.debug('Inside ' + a.GoogleMapX__c + ',' + a.GoogleMapY__c + ',' + a.Geolocation__Latitude__s + ',' + a.Geolocation__Longitude__s);
                }
          }
       
     }
 }
 }
 
}