trigger Coachee_grant on Coaching__c (after insert,after Update)
{
    if(trigger.isInsert || trigger.isUpdate )
    {
        String loginUserCountryName = [select country from User where Id =: UserInfo.getUserId()].Country;
        List<Coaching__Share> CoachShares  = new List<Coaching__Share>();
        for(Coaching__c coach: trigger.new)
        {
            Coaching__Share CoacheeShare = new Coaching__Share();
            CoacheeShare.ParentId = coach.Id;
            CoacheeShare.UserOrGroupId = coach.Coachee__c;
            if(loginUserCountryName =='Japan'){
                CoacheeShare.AccessLevel = 'Edit';
            }
            else{
            CoacheeShare.AccessLevel = 'Read';
            }
            CoacheeShare.RowCause = Schema.Coaching__Share.RowCause.Coachee_Access__c;
            //Schema.CoacheeShare.rowCause.Coachee_Access__c;
            CoachShares.add(CoacheeShare);
        }
        Database.SaveResult[] jobShareInsertResult = Database.insert(CoachShares,false);
    }
}