trigger CoachingEvent on Coaching__c(after insert,after update){
    SyncEventTriggerService_COACHING sc = new SyncEventTriggerService_COACHING();
    
    
    List<TriggerHandler__c> lstTrigger = TriggerHandler__c.getall().values();
    if(!lstTrigger.isEmpty()){
        if (lstTrigger[0].IsCoachingEventActive__c == true) {
                if (Trigger.isInsert) {
                     // Calling Service layer to sync Call data with Event record.
                     SyncEventTriggerService_COACHING.syncEventInsert(trigger.new);
                }
        if(Trigger.isupdate){
                      SyncEventTriggerService_COACHING.syncEventUpdate(trigger.new, trigger.newmap);
                }
            
        }
  }
  }