/**
* File Name: ContactTrigger
* Description : create Accountcontact record for primary when there is no Accountcontact record
* Copyright (c) $2018 Johnson & Johnson
* @author : Venkata Mahesh D | BICSGLOBAL
* Date Last Modified:  18-Oct-2018
*/
trigger ContactTrigger on Contact (after insert, after update)
{
	if (Trigger.isAfter && Trigger.isInsert)
	{
		ContactHandler.createOrUpdatePrimaryAccCon(Trigger.New);
	}
}