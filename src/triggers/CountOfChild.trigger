trigger CountOfChild on Satisfaction_Surveys__c (after insert, after update, after delete, after undelete){




List<Satisfaction_Surveys__c > VLstss = new List<Satisfaction_Surveys__c >(trigger.new) ;
set<String> vcoachingId = new set<String>();


           
    if(trigger.isInsert || trigger.isUndelete || trigger.isdelete){
            if(!trigger.isdelete){
                 VLstss = trigger.new;
            }
            else{
                VLstss = trigger.old;
            }



           for(Satisfaction_Surveys__c vSS : VLstss){
                
                if(vSS.Coaching__c != null){

                vcoachingId.add(vSS.Coaching__c);
                }
            }
    }
    else{
        
            if(trigger.isUpdate){           
           
                for(Satisfaction_Surveys__c vNewSS : VLstss){

                    Satisfaction_Surveys__c oldSS = trigger.oldMap.get(vNewSS.Id);
                    
                    if(vNewSS.Actual_Score_GetComm__c!=oldSS.Actual_Score_GetComm__c ||
                       vNewSS.Actual_Score_Clarify__c!=oldSS.Actual_Score_Clarify__c ||
                       vNewSS.Actual_Score_DGV__c!=oldSS.Actual_Score_DGV__c||
                       vNewSS.Actual_Score_CER__c!=oldSS.Actual_Score_CER__c||
                       vNewSS.Actual_Score_BeSupportive__c!=oldSS.Actual_Score_BeSupportive__c ||
                       vNewSS.Actual_Score_DTN__c!=oldSS.Actual_Score_DTN__c ||
                       vNewSS.Actual_Score_EI__c!=oldSS.Actual_Score_EI__c||
                       vNewSS.Actual_Score_IP__c!=oldSS.Actual_Score_IP__c){
                        vcoachingId.add(vNewSS.Coaching__c);
                    
                    }
                        if(vNewSS.Coaching__c != oldSS.Coaching__c){
        
                            if(vNewSS.Coaching__c != null){

                                    vcoachingId.add(vNewSS.Coaching__c);
                             }

                            if(oldSS.Coaching__c != null){

                                    vcoachingId.add(oldSS.Coaching__c);
                            }
                       }
                 }
           
            }
    }

    system.debug('besupportive++++++++' + vcoachingId );
    
    List<AggregateResult> vLstAggr = [SELECT SUM(Actual_Score_GetComm__c) NoOfCommitment,
                                      SUM(Actual_Score_Clarify__c) clarify,
                                      SUM(Actual_Score_DGV__c) dontgiveup,
                                      SUM(Actual_Score_CER__c) CER, 
                                      SUM(Actual_Score_BeSupportive__c) BeSupportive,
                                      SUM(Actual_Score_DTN__c) DTN,
                                      SUM(Actual_Score_EI__c) EI,
                                      SUM(Actual_Score_IP__c) IP, 
                                      Coaching__c FROM Satisfaction_Surveys__c WHERE Coaching__c In :vcoachingId GROUP BY Coaching__c];
                                      
                                   
    
        Coaching__c vSSrec;
        list<Coaching__c> vLsCoach = new list<Coaching__c>();
        
        for(AggregateResult vAggr : vLstAggr){       

            string coachId = (string) vAggr.get('Coaching__c');
            
            decimal commitment = (decimal) vAggr.get('NoOfCommitment');
            decimal clarify = (decimal) vAggr.get('clarify');
            decimal dontgiveup = (decimal) vAggr.get('dontgiveup');
            decimal CER = (decimal) vAggr.get('CER');
            decimal BeSupportive = (decimal) vAggr.get('BeSupportive');
            decimal DTN = (decimal) vAggr.get('DTN');
            decimal EI = (decimal) vAggr.get('EI');
            decimal IP = (decimal) vAggr.get('IP');
            
            decimal commitment1 = commitment/3;
            decimal clarify1 = clarify/3;
            decimal dontgiveup1 = dontgiveup/4;
            decimal CER1 = CER/5;
            decimal BeSupportive1 = BeSupportive/4;
            decimal DTN1 = DTN/6;
            decimal EI1 = EI/5;
            decimal IP1 = IP/5;
            
            list<decimal> feedbackformvalues = new list<Decimal>();
            feedbackformvalues.add(commitment1 );
            feedbackformvalues.add(clarify1);
            feedbackformvalues.add(dontgiveup1 );
            feedbackformvalues.add(CER1 );
            feedbackformvalues.add(DTN1 );
            feedbackformvalues.add(BeSupportive1 );
            feedbackformvalues.add(EI1 );
            feedbackformvalues.add(IP1 );
            
            
            list<decimal> count = new list<Decimal>();
            for(Decimal i:feedbackformvalues ){
                if(i>0){
                count.add(1);            
                }
                       
            }
            
          
            decimal nocount = count.size(); 
            decimal OverAllScore;
            if(nocount>0){
            OverAllScore = (commitment1  + clarify1 + dontgiveup1 + CER1 + BeSupportive1 + DTN1 +EI1 +IP1)/nocount;        
            
            }else{
             OverAllScore = 0;         
             }
                       
             

    vSSrec = new Coaching__c(Id = coachId , 
                                 Commitment_Satisfaction_Survey__c = commitment1, 
                                 Clarify_Satisfaction_Survey__c = clarify1, 
                                 Don_t_Give_Up__c = dontgiveup1,
                                 Confront_Excuses_Resistance__c = CER1,
                                Be_Supportive_Num__c = BeSupportive1,
                                Define_Topic_And_Needs_Num__c = DTN1,
                                Establish_Impact_Num__c = EI1,
                               Initiate_a_Plan_Num__c = IP1,
                               Overall_Score_SS_Num__c = OverAllScore );
                           
                          
    vLsCoach.add(vSSrec);
}

update vLsCoach;
}