trigger CountOfChild_CallScript on Call_Scripts__c (after insert, after update, after delete, after undelete){




List<Call_Scripts__c> VLstss = new List<Call_Scripts__c >(trigger.new) ;
set<String> vcoachingId = new set<String>();


           
    if(trigger.isInsert || trigger.isUndelete || trigger.isdelete){
            if(!trigger.isdelete){
                 VLstss = trigger.new;
            }
            else{
                VLstss = trigger.old;
            }



           for(Call_Scripts__c  vSS : VLstss){
                
                if(vSS.Coaching__c != null){

                vcoachingId.add(vSS.Coaching__c);
                }
            }
    }
    else{
        
            if(trigger.isUpdate){           
           
                for(Call_Scripts__c  vNewSS : VLstss){

                    Call_Scripts__c oldSS = trigger.oldMap.get(vNewSS.Id);
                    
                    if(vNewSS.Actual_Score_Clarify__c!=oldSS.Actual_Score_Clarify__c ||
                       vNewSS.Actual_Score_Commitment__c!=oldSS.Actual_Score_Commitment__c ||
                       vNewSS.Actual_Score_Influence__c!=oldSS.Actual_Score_Influence__c||
                       vNewSS.Actual_Score_OH__c!=oldSS.Actual_Score_OH__c||
                       vNewSS.Actual_Score_Open__c!=oldSS.Actual_Score_Open__c ||
                       vNewSS.Actual_Score_Precall__c!=oldSS.Actual_Score_Precall__c ){
                        vcoachingId.add(vNewSS.Coaching__c);
                    
                    }
                        if(vNewSS.Coaching__c != oldSS.Coaching__c){
        
                            if(vNewSS.Coaching__c != null){

                                    vcoachingId.add(vNewSS.Coaching__c);
                             }

                            if(oldSS.Coaching__c != null){

                                    vcoachingId.add(oldSS.Coaching__c);
                            }
                       }
                 }
           
            }
    }


    List<AggregateResult> vLstAggr = [SELECT SUM(Actual_Score_Clarify__c) clarify,
                                      SUM(Actual_Score_Commitment__c) Commitment,
                                      SUM(Actual_Score_Influence__c) Influence, 
                                      SUM(Actual_Score_OH__c) OH,
                                      SUM(Actual_Score_Open__c) Open,                                     
                                      SUM(Actual_Score_Precall__c) Precall, 
                                      Coaching__c FROM Call_Scripts__c WHERE Coaching__c In :vcoachingId GROUP BY Coaching__c];
    
        Coaching__c vSSrec;
        list<Coaching__c> vLsCoach = new list<Coaching__c>();

        for(AggregateResult vAggr : vLstAggr){       

            string coachId = (string) vAggr.get('Coaching__c');
            decimal commitment = (decimal) vAggr.get('Commitment');
            decimal clarify= (decimal) vAggr.get('clarify');
            decimal Influence= (decimal) vAggr.get('Influence');
            decimal OH= (decimal) vAggr.get('OH');
            decimal Open= (decimal) vAggr.get('Open');
            decimal Precall= (decimal) vAggr.get('Precall');
            
            
            decimal commitment1 = commitment/5;
            decimal clarify1 = clarify/8;
            decimal Influence1 = Influence/6;
            decimal OH1 = OH/8;
            decimal Open1 = Open/3;
            decimal Precall1 = Precall/8;
            
            list<decimal> feedbackformvalues = new list<Decimal>();
            feedbackformvalues.add(commitment1);
            feedbackformvalues.add(clarify1);
            feedbackformvalues.add(Influence1);
            feedbackformvalues.add(OH1);
            feedbackformvalues.add(Open1);
            feedbackformvalues.add(Precall1);
            
            list<decimal> count = new list<Decimal>();
            for(Decimal i:feedbackformvalues ){
                if(i>0){
                count.add(1);            
                }
                       
            }
            
          
            decimal nocount = count.size();            
            decimal OverAllScore;
           
           if(nocount>0){
            OverAllScore = (commitment1 + clarify1 + Influence1 + OH1 + Open1 + Precall1)/nocount ;
            }
            else
            OverAllScore = 0;
              
            vSSrec = new Coaching__c(Id = coachId , 
                                         Commitment_Num__c = Commitment1, 
                                         Open_Num__c = Open1, 
                                         Clarify_Num__c= clarify1,
                                         Influence_Num__c = Influence1,
                                         Objection_Handling_Num__c = OH1,
                                         Pre_Call_Plan_Num__c = Precall1,
                                         Overall_Score_Num__c = OverAllScore);
                                       
            vLsCoach.add(vSSrec);
        }

update vLsCoach;
}