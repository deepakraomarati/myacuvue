trigger CouponContact on CouponContact__c (before insert,before update) {

    
    if(Trigger.isBefore){
        
       List<String> lstConId =new List<String>();
       List<String> lstAccId =new List<String>();
       List<String> lstTranId =new List<String>();
       List<Integer> lstCoupId =new List<Integer>();
       List<Integer> lstcsendId =new List<Integer>(); 
       List<Integer> lstcampId =new List<Integer>();
       List<CouponContact__c> lstcc=new List<CouponContact__c>();
       
       Map<Id,Contact> MapContact =new Map<Id,Contact>();
       Map<Id,Account> MapAccount = new Map<Id,Account> ();
       Map<Id,TransactionTd__c> MapTrans = new  Map<Id,TransactionTd__c>();
       Map<Id,Coupon__c> MapCoupen =new  Map<Id,Coupon__c>();
       Map<Id,CouponSendList__c> MapCoupenSend = new Map<Id,CouponSendList__c>();
       Map<Id,Campaign> MapCamp =new  Map<Id,Campaign>();
       
       for(CouponContact__c cc:Trigger.New){
        
            lstcc.add(cc);
            
            if(cc.Aws_ContactId__c !=null){
                lstConId.add(cc.Aws_ContactId__c);
            }
            if(cc.Aws_AccountId__c !=null){
                lstAccId.add(cc.Aws_AccountId__c);
            }
            if(cc.Aws_TransactionId__c !=null){
                lstTranId.add(cc.Aws_TransactionId__c);
            }
            if(cc.Aws_CouponId__c !=null){
                lstCoupId.add(Integer.ValueOf(cc.Aws_CouponId__c));
            }
            if(cc.Aws_CouponSendId__c !=null){
                lstcsendId.add(Integer.ValueOf(cc.Aws_CouponSendId__c));
            }
            if(cc.CampaignKey__c !=null){
                lstcampId.add(Integer.ValueOf(cc.CampaignKey__c));
            }
       }
       
       System.debug('lstConId ###'+lstConId);
       System.debug('lstAccId ###'+lstAccId);
       System.debug('lstTranId ###'+lstTranId);
       System.debug('lstCoupId ###'+lstCoupId);
       System.debug('lstcsendId ###'+lstcsendId);
       System.debug('lstcampId ###'+lstcampId);
       
       if(lstAccId.size() >0 && !lstAccId.isEmpty()){
            MapAccount = new Map<Id,Account>([SELECT id,Aws_AccountId__c FROM Account WHERE Aws_AccountId__c IN: lstAccId LIMIT 50000]);
            System.debug('MapAccount ###'+MapAccount);
       }
       
       if(lstConId.size() >0 && !lstConId.isEmpty()){
            MapContact = new Map<Id,Contact>([SELECT id,Aws_ContactId__c FROM Contact WHERE Aws_ContactId__c IN:lstConId LIMIT 50000]);
            System.debug('MapContact ###'+MapContact);
       }
       if(lstTranId.size()>0 && !lstTranId.isEmpty()){
            MapTrans = new Map<Id,TransactionTd__c>([SELECT Id,DB_ID__c,TransactionId__c FROM TransactionTd__c WHERE TransactionId__c IN:lstTranId LIMIT 50000]);
            System.debug('MapTrans ###'+MapTrans);
       }
       if(lstCoupId.size() >0 && !lstCoupId.isEmpty()){
           MapCoupen = new Map<Id,Coupon__c>([SELECT Id,DB_ID__c FROM Coupon__c WHERE DB_ID__c IN:lstCoupId LIMIT 50000]);
           System.debug('MapCoupen ####'+lstCoupId);
       }
       if(lstcsendId.size() >0 && !lstcsendId.isEmpty()){
          MapCoupenSend =new Map<Id,CouponSendList__c>([SELECT Id,DB_ID__c FROM CouponSendList__c WHERE DB_ID__c IN:lstcsendId LIMIT 50000]);
          System.debug('MapCoupenSend ###:'+MapCoupenSend);
       }
       
       if(lstcampId.size() >0 && !lstcampId.isEmpty()){
          MapCamp =new Map<Id,Campaign>([SELECT Id,DB_ID__c FROM Campaign WHERE DB_ID__c IN:lstcampId LIMIT 50000]);
          System.debug('MapCoupenSend ###:'+MapCoupenSend);
       }
       
       for(CouponContact__c cc:lstcc){
            
            if(MapAccount.size()>0 && !MapAccount.isEmpty()){
                for(Account acc:MapAccount.Values()){
                    if(acc.Aws_AccountId__c !=null && acc.Aws_AccountId__c == cc.Aws_AccountId__c){
                        cc.AccountId__c = acc.Id;
                    }
                }
            }
            if(MapContact.size()>0 && !MapContact.isEmpty()){
                for(Contact con:MapContact.Values()){
                    if(con.Aws_ContactId__c !=null && con.Aws_ContactId__c == cc.Aws_ContactId__c){
                        cc.ContactId__c = con.Id;
                    }
                }
            }
            
            if(MapTrans.size()>0 && !MapTrans.isEmpty() && cc.Aws_TransactionId__c!=null){
                for(TransactionTd__c ts:MapTrans.Values()){
                    if(ts.TransactionId__c !=null && ts.TransactionId__c == cc.Aws_TransactionId__c){
                        cc.TransactionId__c = ts.Id;
                    }
                }
            }
            
            if(MapCoupen.size()>0 && !MapCoupen.isEmpty() && cc.Aws_CouponId__c!=null ){
                for(Coupon__c cp:MapCoupen.Values()){
                    if(cp.DB_ID__c !=null && cp.DB_ID__c == Integer.ValueOf(cc.Aws_CouponId__c)){
                        cc.CouponId__c = cp.Id;
                    }
                }
            }
            
            if(MapCoupenSend.size()>0 && !MapCoupenSend.isEmpty()&& cc.Aws_CouponSendId__c!=null){
                for(CouponSendList__c cs:MapCoupenSend.Values()){
                    if(cs.DB_ID__c !=null && cs.DB_ID__c == Integer.ValueOf(cc.Aws_CouponSendId__c)){
                        cc.CouponSendListId__c = cs.Id;
                    }
                }
            }
            if(MapCamp.size() >0 && !MapCamp.isEmpty() && cc.CampaignKey__c!=null ){
                for(Campaign cam:MapCamp.Values()){
                    if(cam.DB_ID__c !=null && cam.DB_ID__c == cc.CampaignKey__c){
                        cc.CampaignId__c = cam.Id;
                    }
                }
            }
       }
    }
}