trigger GetAccountBooking on AccountBooking__c (before insert,before update) {

    if(Trigger.isBefore)
    {
    
     List<String> lstAccId= new List<String>();
     List<String> lstConId= new List<String>();
     
     List<AccountBooking__c> lstaccbokobj= new List<AccountBooking__c>();
      
     Map<id,Account> MapAccount=new map<id,Account>();
     Map<id,Contact> MapContact=new map<id,Contact>();
        
        for(AccountBooking__c Abobj:Trigger.New)
      {
        lstaccbokobj.add(Abobj);
        
        if(Abobj.Aws_AccountId__c!=null && Abobj.MA2_AccountId__c == null){
            lstAccId.add(Abobj.Aws_AccountId__c);
        }
        
         if(Abobj.Aws_ContactId__c!=null && Abobj.MA2_ConatctId__c == null){
            lstConId.add(Abobj.Aws_ContactId__c);
        }
        
      }
      
      if(lstAccId.size()>0 && !lstAccId.isEmpty())
      {    
         MapAccount=new Map<Id,Account>([select id,Name,Aws_AccountId__c from account where Aws_AccountId__c IN:lstAccId limit 50000]);
      } 
         
      if(lstConId.size()>0 && !lstConId.isEmpty())
      {
        MapContact =new Map<Id,Contact>([select id,LastName,Aws_ContactId__c from contact where Aws_ContactId__c IN:lstConId limit 50000]);
      }
       
      Map<String,Id> mapAccountIds= new Map<String,Id>();
      for(Account acc:MapAccount.Values()){
          mapAccountIds.put(acc.Aws_AccountId__c,acc.Id);
      }
         
         
      Map<String,Id> mapContactIds= new Map<String,Id>();
      for(Contact con: MapContact.Values()){
          mapContactIds.put(con.Aws_ContactId__c,con.Id);
      }
         
         
      List<AccountBooking__c> lstAccountBooking = new List<AccountBooking__c>();

      for(AccountBooking__c ab:lstaccbokobj)
      {
            if(ab.Aws_AccountId__c!=null && mapAccountIds.get(ab.Aws_AccountId__c)!=null && ab.MA2_AccountId__c == null){
            try{
                 ab.Account__c= mapAccountIds.get(ab.Aws_AccountId__c);
                 system.debug('Account '+ ab.Account__c);
                 ab.MA2_CountryCode__c='KOR';
                 ab.MA2_VisitReason__c=ab.VisitReason__c;
                 ab.Name=ab.Name__c; 
                 DateTime bDate = ab.BookingDate__c;
                 Date bookDate = date.newinstance(bDate.year(), bDate.month(), bDate.day());
                 ab.MA2_BookingDate__c=bookDate;
                 DateTime vDate = ab.VisitDate__c;
                 Date visitDate = date.newinstance(vDate.year(), vDate.month(), vDate.day());
                 ab.MA2_VisitDate__c=visitDate; 
                }
                catch(exception e)
                {
                }           
                   
            }
            else if(ab.MA2_AccountId__c == null){
                 //ab.Account__c=null;
                 system.debug('inside else statement1');
            }
            
            if(ab.Aws_ContactId__c!=null && mapContactIds.get(ab.Aws_ContactId__c)!=null && ab.MA2_ConatctId__c == null){
                
                 ab.Contact__c= mapContactIds.get(ab.Aws_ContactId__c);
                 system.debug('contact '+ ab.Contact__c);
                 
            }
            else if(ab.MA2_ConatctId__c == null){
                 //ab.Contact__c=null;
                 system.debug('inside else statement2');
            }  
           
            lstAccountBooking.add(ab);
       }
           
    }
}