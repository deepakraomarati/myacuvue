trigger GetAccountGrade on AccountGrade__c(before insert,before update) {

    if(Trigger.isBefore)
    {
    
     List<String> lstAccId= new List<String>();
     List<String> lstConId= new List<String>();
     
     List<AccountGrade__c> lstaccgrd= new List<AccountGrade__c>();
     
     Map<id,Account> MapAccount=new map<id,Account>();
     Map<id,Contact> MapContact=new map<id,Contact>();
     
     for(AccountGrade__c Agobj:Trigger.New)
     
     {
     
        lstaccgrd.add(Agobj);
        
        if(Agobj.Aws_AccountId__c!=null){
            lstAccId.add(Agobj.Aws_AccountId__c);
        }
        
        if(Agobj.Aws_ContactId__c!=null){
            lstConId.add(Agobj.Aws_ContactId__c);
        }
        
      }
      
       if(lstAccId.size()>0 && !lstAccId.isEmpty())
       {
          MapAccount=new Map<Id,Account>([select id,Name,Aws_AccountId__c from account where Aws_AccountId__c IN:lstAccId limit 50000]);
       } 
         
       if(lstConId.size()>0 && !lstConId.isEmpty())
       {
          MapContact =new Map<Id,Contact>([select id,LastName,Aws_ContactId__c from contact where Aws_ContactId__c IN:lstConId limit 50000]);
       }
       
       Map<String,Id> mapAccountIds= new Map<String,Id>();
       for(Account acc:MapAccount.Values()){
            mapAccountIds.put(acc.Aws_AccountId__c,acc.Id);
       }
       
       Map<String,Id> mapContactIds= new Map<String,Id>();
       for(Contact con: MapContact.Values()){
            mapContactIds.put(con.Aws_ContactId__c,con.Id);
       }

        
       list<AccountGrade__c>  lstaccountgradee = new list<AccountGrade__c>();
       
       for(AccountGrade__c ag:lstaccgrd)
       {
             if(ag.Aws_AccountId__c!=null && mapAccountIds.get(ag.Aws_AccountId__c)!=null)
             {
                     ag.Account__c= mapAccountIds.get(ag.Aws_AccountId__c);
                       
             }
             
             else
             {
             
                  ag.Account__c=null;
             }
             
             
            if(ag.Aws_ContactId__c!=null && mapContactIds.get(ag.Aws_ContactId__c)!=null){
                     ag.Contact__c= mapContactIds.get(ag.Aws_ContactId__c);
            }
            else{
                 ag.Contact__c=null;
            }
        
           lstaccountgradee.add(ag);    
               
      }
   
   }

}