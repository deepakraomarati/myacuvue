trigger GetAccountId on Contact (before insert,after insert,before update) {
    
    List<String> lstAccId =new List<String>();
    List<Contact> lstcon =new List<Contact>();
    List<Consumer_Relation__c> lstconsumer =new List<Consumer_Relation__c>();
    
    Map<Id,Account> MapAccount = new Map<Id,Account>();
    
  if(Trigger.isBefore){
    
    for(Contact con:Trigger.New)
    {
        if(con.Aws_AccountId__c !=null)
        {
            lstAccId.add(con.Aws_AccountId__c);
        }
        lstcon.add(con);
    }
    
    if(lstAccId.size()>0 && !lstAccId.isEmpty()){
        MapAccount =new Map<Id,Account>([SELECT Id,Aws_AccountId__c FROM Account WHERE Aws_AccountId__c IN :lstAccId]);
    }
    
    
    
    for(Contact c:lstcon){
        if(MapAccount .size()>0 && !MapAccount .isEmpty()){
            for(Account acc:MapAccount .Values()){
                if(acc.Aws_AccountId__c !=null && acc.Aws_AccountId__c == c.Aws_AccountId__c){
                    //c.AccountId = acc.Id;
                    //c.Account.name='';
                    c.RecordTypeId='01290000000uId7';                                      
                    
                
                }
            }            
        }
    }
  }
     
     if(Trigger.isAfter){ 
    
    for(Contact con:Trigger.New)
    {
        if(con.Aws_AccountId__c !=null)
        {
            lstAccId.add(con.Aws_AccountId__c);
        }
        lstcon.add(con);
    }
    
    if(lstAccId.size()>0 && !lstAccId.isEmpty()){
        MapAccount =new Map<Id,Account>([SELECT Id,Aws_AccountId__c FROM Account WHERE Aws_AccountId__c IN :lstAccId]);
    }
        
        for(Contact con:Trigger.New){
        
        if(MapAccount .size()>0 && !MapAccount .isEmpty()){
            for(Account acc:MapAccount .Values()){
                if(acc.Aws_AccountId__c !=null && acc.Aws_AccountId__c == con.Aws_AccountId__c){
                Consumer_Relation__c crobj=new Consumer_Relation__c();            
                crobj.AccountId__c = acc.id ;
                 crobj.ContactID__c = con.Id;            
                crobj.DB_ID__c =Integer.Valueof(con.Aws_ContactId__c);         
                lstconsumer.add(crobj);
            
                }
         
            }
         }
       }
        
            if(lstconsumer.size()>0){
                insert lstconsumer;
                
            }
            
            
     } 
}