trigger GetAccountIdForMigration on ProductLotNo__c (before insert,before update) {

List<ProductLotNo__c> lstProduct= new List<ProductLotNo__c>();

List<string> PrdId = new List<string>();
List<string> transId = new List<string>();
List<string> BachId = new List<string>();

Map<Id,Product2 > MapProduct = new Map<Id,Product2 >();
Map<Id,TransactionTd__c> MapTransaction = new Map<Id,TransactionTd__c>();
Map<Id,Batch_Details__c> MapBatch = new Map<Id,Batch_Details__c>();


if(Trigger.isBefore)
 {
     for(ProductLotNo__c  PL:trigger.new){
         lstProduct.add(PL);
         
         if(PL.Aws_ProductId__c !=null){
            PrdId .add(PL.Aws_ProductId__c);
         }
         
         if(PL.Aws_TransacId__c !=null){
            transId.add(PL.Aws_TransacId__c);
        }
        
        if(PL.Aws_BatchNumberId__c!=null){
            BachId.add(PL.Aws_BatchNumberId__c);
        }
     }
     
     if(PrdId.size()>0 && !PrdId.isEmpty())
     {
        MapProduct = new Map<Id,Product2 >([select id,UPC_Code__c from Product2 where UPC_Code__c IN:PrdId LIMIT 50000]);
     }
     
     if(transId.size()>0 && !transId.isEmpty())
     {
        MapTransaction = new Map<Id,TransactionTd__c>([select id,DB_ID__c,TransactionId__c  from TransactionTd__c where TransactionId__c IN:transId LIMIT 50000]);
     }
     
     if(BachId.size()>0 && !BachId.isEmpty())
     {
        MapBatch = new Map<Id,Batch_Details__c>([select id,Batch_Number__c from Batch_Details__c where Batch_Number__c IN:BachId LIMIT 50000]);
     }
     
          
     for(ProductLotNo__c  PNL:lstProduct)
     {
         if(MapProduct.size()>0 && !MapProduct.isEmpty() && PNL.Aws_ProductId__c !=null){
                for(Product2  pr: MapProduct.Values()){
                    if(pr.UPC_Code__c !=null && pr.UPC_Code__c == PNL.Aws_ProductId__c){
                        PNL.ProductId__c = pr.Id;
                    }
                }
            }
         
                
         if(MapTransaction.size()>0 && !MapTransaction.isEmpty() && PNL.Aws_TransacId__c !=null){
           for(TransactionTd__c tr: MapTransaction.values()){
               if(tr.TransactionId__c !=null && tr.TransactionId__c ==PNL.Aws_TransacId__c){
                  PNL.TransactionId__c= tr.Id;
               }
            }
         }
         
         if(MapBatch.size()>0 && !MapBatch.isEmpty() && PNL.Aws_BatchNumberId__c!=null){
           for(Batch_Details__c B: MapBatch.values()){
               if(B.Batch_Number__c !=null && B.Batch_Number__c ==PNL.Aws_BatchNumberId__c){
                  PNL.BatchNumber__c= B.Id;
               }
            }
         }
     }
     
    
 }    
    
}