trigger GetAccountInfo on AccountInfo__c(before insert,before update) {

 if(Trigger.isBefore)
 {
 
 List<String> lstAccId= new List<String>();
 
 Map<id,Account> MapAccount=new map<id,Account>();
 
 List<AccountInfo__c> lstaccinfoobj= new List<AccountInfo__c>();
 
 
   for(AccountInfo__c Acinfobj:Trigger.New)
      {
        lstaccinfoobj.add(Acinfobj);
        
        if(Acinfobj.Aws_OutletNumber__c!=null){
            lstAccId.add(Acinfobj.Aws_OutletNumber__c);
        }
        
      }
     
     if(lstAccId.size()>0 && !lstAccId.isEmpty())
     {
          MapAccount=new Map<Id,Account>([select id,Name,OutletNumber__c from account where OutletNumber__c IN:lstAccId limit 50000]);
     }
     
     Map<String,Id> mapAccountIds= new Map<String,Id>();
         for(Account acc:MapAccount.Values()){
             mapAccountIds.put(acc.OutletNumber__c,acc.Id);
         }    
           
     
     list<AccountInfo__c> lstaccountinfoo = new list<AccountInfo__c>();
     
     for(AccountInfo__c ab:lstaccinfoobj)
     {
        if(ab.Aws_OutletNumber__c!=null && mapAccountIds.get(ab.Aws_OutletNumber__c)!=null){
            ab.Account__c= mapAccountIds.get(ab.Aws_OutletNumber__c);
        }
                 
        else{
         
             ab.Account__c=null;
        }
        
        lstaccountinfoo.add(ab);
                    
     }
         
   }
}