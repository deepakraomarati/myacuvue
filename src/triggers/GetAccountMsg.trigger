trigger GetAccountMsg on Account_Msg__c(before insert,before update) {

     if(Trigger.isBefore)
    {
    
         List<String> lstAccId= new List<String>();
         
         List<Account_Msg__c> lstaccmsgobj= new List<Account_Msg__c>();
         
         Map<id,Account> MapAccount=new map<id,Account>();
         
             for(Account_Msg__c Amobj:Trigger.New)
             {
             
                 lstaccmsgobj.add(Amobj);
                 
                 if(Amobj.Aws_AccountId__c!=null){
                 lstAccId.add(Amobj.Aws_AccountId__c);
                 
                 }
             
             }
             
         if(lstAccId.size()>0 && !lstAccId.isEmpty())
         {
             MapAccount=new Map<Id,Account>([select id,Name,Aws_AccountId__c from account where Aws_AccountId__c IN:lstAccId limit 50000]);
         }
         
         Map<String,Id> mapAccountIds= new Map<String,Id>();
         for(Account acc:MapAccount.Values()){
            mapAccountIds.put(acc.Aws_AccountId__c,acc.Id);
         }
         
         List<Account_Msg__c> lstAccountmsgg = new List<Account_Msg__c>();
         
         for(Account_Msg__c amsg:lstaccmsgobj)
         {
             if(amsg.Aws_AccountId__c!=null && mapAccountIds.get(amsg.Aws_AccountId__c)!=null){
                     amsg.Account__c= mapAccountIds.get(amsg.Aws_AccountId__c);
                      
             }
             else{
             
                  amsg.Account__c=null;
                     
             }
             
             lstAccountmsgg.add(amsg);
         }        
             
     }
}