trigger GetAccountName on AccountName__c(before insert,before update) {

    if(Trigger.isBefore)
    {
    
    List<String> lstAccId= new List<String>();
    
    Map<id,Account> MapAccount=new map<id,Account>();
    
    List<AccountName__c> lstaccnamobj= new List<AccountName__c>();
    
    
    for(AccountName__c Accnmobj:Trigger.New)
      {
        lstaccnamobj.add(Accnmobj);
        
        if(Accnmobj.Aws_OutletNumber__c!=null){
            lstAccId.add(Accnmobj.Aws_OutletNumber__c);
        }
        
      }
      
     if(lstAccId.size()>0 && !lstAccId.isEmpty())
     {
            MapAccount=new Map<Id,Account>([select id,Name,OutletNumber__c from account where OutletNumber__c IN:lstAccId limit 50000]);
     }
     
     
      Map<String,Id> mapAccountIds= new Map<String,Id>();
         for(Account acc:MapAccount.Values()){
             mapAccountIds.put(acc.OutletNumber__c,acc.Id);
         }    
         
    
     list<AccountName__c> lstaccountnamee = new list<AccountName__c>();
    
     for(AccountName__c an:lstaccnamobj)
     {
        if(an.Aws_OutletNumber__c!=null && mapAccountIds.get(an.Aws_OutletNumber__c)!=null){
            an.Account__c= mapAccountIds.get(an.Aws_OutletNumber__c);
        }
                 
        else{
         
             an.Account__c=null;
        }
       
        lstaccountnamee.add(an);   
            
     }
    
   }
 
}