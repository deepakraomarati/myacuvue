trigger GetAccountPointHistory on AccountPointHistory__c (before insert,before update) {


if(Trigger.isBefore)
 {
 
    
    List<String> lstAccId= new List<String>();
    List<String> lstConId= new List<String>();
    List<String> lstTranId= new List<String>();
    
    List<AccountPointHistory__c > lstaccphobj= new List<AccountPointHistory__c >();
    
 Map<id,Account> MapAccount=new map<id,Account>();
    Map<id,Contact> MapContact=new map<id,Contact>();
    Map<id,TransactionTd__c> MapTrans=new map<id,TransactionTd__c>();
    
  
      for(AccountPointHistory__c tobj:Trigger.New)
    {
        lstaccphobj.add(tobj);
        
        
        if(tobj.Aws_AccountId__c !=null){
            lstAccId.add(tobj.Aws_AccountId__c);
        }
        if(tobj.Aws_ContactId__c !=null){
            lstConId.add(tobj.Aws_ContactId__c);
        }
        if(tobj.Aws_Transactionid__c!=null){
        
              lstTranId.add(tobj.Aws_Transactionid__c);
            }
    }
    
    if(lstAccId.size()>0 && !lstAccId.isEmpty())
     {
        MapAccount=new Map<Id,Account>([select id,Aws_AccountId__c from account where Aws_AccountId__c IN:lstAccId limit 50000]);
     } 
    
     
     if(lstConId.size()>0 && !lstConId.isEmpty())
     {
         MapContact =new Map<Id,Contact>([select id,Aws_ContactId__c from contact where Aws_ContactId__c IN:lstConId limit 50000]);
     }
   
     
     if(lstTranId.size()>0 && !lstTranId.isEmpty())
     {
       
       MapTrans=new Map<Id,TransactionTd__c>([select id,DB_ID__c,TransactionId__c from TransactionTd__c where TransactionId__c IN:lstTranId limit 50000]);
     }
   
  
 
      for(AccountPointHistory__c ps:lstaccphobj)
      {
        if(MapAccount.size()>0 && !MapAccount.isEmpty() && ps.Aws_AccountId__c!=null){
           for(Account acc:MapAccount.Values()){
               if(acc.Aws_AccountId__c !=null && acc.Aws_AccountId__c == ps.Aws_AccountId__c){
                        ps.AccountId__c = acc.Id;
                    }
                }
            }
         
         
         if(MapContact.size()>0 && !MapContact.isEmpty() && ps.Aws_ContactId__c!=null){
            for(Contact con:MapContact.Values()){
                 if(con.Aws_ContactId__c !=null && con.Aws_ContactId__c == ps.Aws_ContactId__c){
                        ps.ContactId__c = con.Id;
                    }
                }
         
         }
         if(MapTrans.size()>0 && !MapTrans.isEmpty() && ps.Aws_Transactionid__c!=null){
             for(TransactionTd__c ts:MapTrans.Values()){
                    if(ts.TransactionId__c !=null && ts.TransactionId__c == ps.Aws_Transactionid__c){
                        ps.TransacId__c= ts.Id;
                    }
                }
            }
    }
}

}