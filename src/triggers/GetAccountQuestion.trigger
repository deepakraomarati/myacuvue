trigger GetAccountQuestion on AccountQuestion__c(before insert,before update) {

     if(Trigger.isBefore)
     {
     
     List<String> lstAccId= new List<String>();
     List<String> lstConId= new List<String>();
     List<integer> coupId = new List<integer>();
     
      List<AccountQuestion__c> lstaccquesobj= new List<AccountQuestion__c>();
      
      Map<id,Account> MapAccount=new map<id,Account>();
      Map<id,Contact> MapContact=new map<id,Contact>();
      Map<Id,Coupon__c> MapCoupen =new Map<Id,Coupon__c>();
      
      
       for(AccountQuestion__c Aqobj:Trigger.New)
      {
        lstaccquesobj.add(Aqobj);
        
        if(Aqobj.Aws_AccountId__c!=null){
            lstAccId.add(Aqobj.Aws_AccountId__c);
        }
        
         if(Aqobj.Aws_ContactId__c!=null){
            lstConId.add(Aqobj.Aws_ContactId__c);
        }
        
         if(Aqobj.Aws_CouponId__c !=null){
            coupId.add(integer.ValueOf(Aqobj.Aws_CouponId__c));
        }
        
      }
      
      
        if(lstAccId.size()>0 && !lstAccId.isEmpty())
         {
            MapAccount=new Map<Id,Account>([select id,Name,Aws_AccountId__c from account where Aws_AccountId__c IN:lstAccId limit 50000]);
         } 
         
         if(lstConId.size()>0 && !lstConId.isEmpty())
         {
         MapContact =new Map<Id,Contact>([select id,LastName,Aws_ContactId__c from contact where Aws_ContactId__c IN:lstConId limit 50000]);
         }
         
          if(coupId .size()>0 && !coupId .isEmpty())
         {
          MapCoupen=new Map<Id,Coupon__c>([select id,DB_ID__c from Coupon__c where DB_ID__c IN:coupId]);
         }
         
         Map<String,Id> mapAccountIds= new Map<String,Id>();
         for(Account acc:MapAccount.Values()){
            mapAccountIds.put(acc.Aws_AccountId__c,acc.Id);
         }
         
         Map<String,Id> mapContactIds= new Map<String,Id>();
         for(Contact con: MapContact.Values()){
            mapContactIds.put(con.Aws_ContactId__c,con.Id);
         }
         
         Map<integer,id> mapCouponIds= new Map<integer,id>();
         for(Coupon__c coup:MapCoupen.Values()){
             mapCouponIds.put(integer.valueof(coup.DB_ID__c),coup.Id);
         }
         
        
        list<AccountQuestion__c> lstaccountquestionn = new list<AccountQuestion__c>();
         
        for(AccountQuestion__c ab:lstaccquesobj)
        {
           if(ab.Aws_AccountId__c!=null && mapAccountIds.get(ab.Aws_AccountId__c)!=null){
                     ab.Account__c= mapAccountIds.get(ab.Aws_AccountId__c);
                      
            }
            else{
                 ab.Account__c=null;
            }
            
            if(ab.Aws_ContactId__c!=null && mapContactIds.get(ab.Aws_ContactId__c)!=null){
                     ab.Contact__c= mapContactIds.get(ab.Aws_ContactId__c);
            }
            else{
                 ab.Contact__c=null;
            }
            
            if(ab.Aws_CouponId__c!=null && mapCouponIds.get(integer.valueof(ab.Aws_CouponId__c))!=null){
                     ab.Coupon__c= mapCouponIds.get(integer.valueof(ab.Aws_CouponId__c));
            }
            else{
                 ab.Coupon__c=null;
            }
           
       lstaccountquestionn.add(ab);    
             
      }
    
    }

}