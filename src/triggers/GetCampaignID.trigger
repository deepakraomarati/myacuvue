/**
* File Name: GetCampaignID
* Author:
* Date Last Modified:  11-Nov-2018
* Last Modified Description: refactored the code and added trigger context to support trigger send emails for JJVPRO
* Description : 
* Copyright (c) $2018 Johnson & Johnson
*/
trigger GetCampaignID on CampaignMember (before insert,before update) 
{

    if(Trigger.isBefore)
    {
        if(Trigger.isUpdate){
            for(CampaignMember cc : Trigger.new){
                if(cc.ContactID != null){
                    cc.ContactId__c = cc.ContactID;
                }
            }
        }
        List<CampaignMember> lstcc=new List<CampaignMember >();
        
        List<String> lstConId =new List<String>();
        List<Decimal> lstcampId =new List<Decimal>();
        List<Contact> NewContactObj = new List<Contact> ();

        Map<Id,Contact> Mapcon  = new Map<Id,Contact>();
        Map<Id,Campaign> Mapcamp  = new Map<Id,Campaign>();
 

        for(CampaignMember cc:Trigger.New)
        {   
            cc.ContactId__c = cc.ContactID;
            lstcc.add(cc);
  
            if(cc.AWS_ContactId__c!=null){
                lstConId.add(cc.AWS_ContactId__c);
            }
  
            if(cc.CampaignKey__c!=null){
                lstcampId.add(cc.CampaignKey__c);
            }
        }
        if(lstConId.size() >0 && !lstConId.isEmpty())
        {
            Mapcon = new Map<Id,Contact>([SELECT id,Aws_ContactId__c FROM Contact WHERE Aws_ContactId__c IN: lstConId LIMIT 50000]);            
        }
     
        if(lstcampId.size() >0 && !lstcampId .isEmpty()){
     
            Mapcamp  = new Map<Id,Campaign>([SELECT id,DB_ID__c FROM Campaign WHERE DB_ID__c IN: lstcampId LIMIT 50000]);
        }
     
        for(CampaignMember cm: lstcc){
            if(Mapcon.size()>0 && !Mapcon .isEmpty() && cm.AWS_ContactId__c !=null){
                for(Contact con: Mapcon .Values()){
                    if(con.Aws_ContactId__c !=null  && con.Aws_ContactId__c == cm.AWS_ContactId__c){ 
                        cm.ContactId= con.Id;
                    }
                }
            }
            if(Mapcamp.size()>0 && !Mapcamp.isEmpty() && cm.CampaignKey__c!=null){
                for(Campaign cam: Mapcamp.Values()){
                    if(cam.DB_ID__c !=null && cam.DB_ID__c == cm.CampaignKey__c){ 
                        cm.CampaignId=cam.Id;
                    }
                }
            }
        }
    }
   
}