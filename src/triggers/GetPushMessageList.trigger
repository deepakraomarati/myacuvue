trigger GetPushMessageList on PushMessageList__c(before insert,before update) {

      if(Trigger.isBefore)
      {
      
          List<String> lstConId= new List<String>();
          List<PushMessageList__c> lstpushobj= new List<PushMessageList__c>();
          Map<id,Contact> MapContact=new map<id,Contact>();
          
          for(PushMessageList__c pobj:Trigger.New)
          {
              lstpushobj.add(pobj);
              
              if(pobj.Aws_ContactId__c !=null){
              lstConId.add(pobj.Aws_ContactId__c);
              }
          }
          
          if(lstConId.size()>0 && !lstConId.isEmpty())
          {
              MapContact =new Map<Id,Contact>([select id,Aws_ContactId__c from contact where Aws_ContactId__c IN:lstConId limit 50000]);
          }
          
          Map<String,Id> mapContactIds= new Map<String,Id>();
              for(Contact con: MapContact.Values()){
                 mapContactIds.put(con.Aws_ContactId__c,con.Id);
          }
          
          list<PushMessageList__c> lstpushmessagelstt = new list<PushMessageList__c>();
          
          for(PushMessageList__c ps:lstpushobj)
          {
          
              if(ps.Aws_ContactId__c!=null && mapContactIds.get(ps.Aws_ContactId__c)!=null){
                     ps.Contact__c= mapContactIds.get(ps.Aws_ContactId__c);
              }
              else{
                   ps.Contact__c=null;
             }
                        
             lstpushmessagelstt.add(ps);
            
          }
     }

}