trigger GetQuestionIdForSurvey on SurveyAnswer__c (before insert, before update) {
if(Trigger.isInsert || Trigger.isUpdate)
     {
        
        List<SurveyAnswer__c> SAObj = new List<SurveyAnswer__c>();
        List<Survey__c> SurveyObject= new List<Survey__c>();
        List<SurveyQuestion__c> SQuestionObject = new List<SurveyQuestion__c>();
        List<SurveyQuestionItem__c> SQItemObject= new List<SurveyQuestionItem__c>(); 
        List<contact> lstcon= new List<contact>() ;
        
        List<String> conId = new List<String>();
        List<Integer> surveyId= new List<Integer>();
        List<Integer> sqId= new List<Integer>();
        List<Integer> sqiId= new List<Integer>();
        
        Map<String,Id> MapSurvey =new Map<String,Id>();
        Map<String,Id> MapQuestion =new Map<String,Id>();
        Map<String,Id> MapItem =new Map<String,Id>();
        Map<String,Id> MapContact =new Map<String,Id>();
        
        
        for(SurveyAnswer__c SAData:Trigger.New)
        {
            SAObj.add(SAData);
            
            if(SAData.Aws_Survey__c != null)
            {
                surveyId.add(Integer.ValueOf(SAData.Aws_Survey__c));
            }
                    
            if(SAData.Aws_SurveyQuestion__c != null)
            {
                sqId.add(Integer.ValueOf(SAData.Aws_SurveyQuestion__c));
            }
            if(SAData.Aws_SurveyQuestionItem__c  != null)
            {
                sqiId.add(Integer.ValueOf(SAData.Aws_SurveyQuestionItem__c));
            }
            if(SAData.Aws_ContactId__c !=null){
                conId.add(SAData.Aws_ContactId__c);    
            }
          }
            
            if(surveyId.size()>0 && !surveyId.isEmpty())
            {
                SurveyObject=[select id,s_seq__c from Survey__c where s_seq__c IN :surveyId LIMIT 50000];
                
                for(Survey__c sy:SurveyObject){
                    MapSurvey.put(String.ValueOf(sy.s_seq__c),sy.Id);
                 }
                 System.debug('MapContact ###'+MapSurvey);
            }
            
            if(sqId.size()>0 && !sqId.isEmpty())
            {
                SQuestionObject =[select id,q_seq__c from SurveyQuestion__c where q_seq__c IN :sqId LIMIT 50000];
                
                for(SurveyQuestion__c sq:SQuestionObject){
                    MapQuestion.put(String.ValueOf(sq.q_seq__c),sq.Id);
                 }
                 System.debug('MapContact ###'+MapQuestion);
            }
            if(sqiId.size()>0 && !sqiId.isEmpty())
            {
                SQItemObject=[select id,i_seq__c from SurveyQuestionItem__c where i_seq__c IN :sqiId LIMIT 50000];
               
                for(SurveyQuestionItem__c sqi:SQItemObject){
                    MapItem.put(String.ValueOf(sqi.i_seq__c),sqi.Id);
                 }
                 System.debug('MapContact ###'+MapItem);
            }
             
            if(conId.size()>0 && !conId.isEmpty()){
                lstcon=[Select Id,Aws_ContactId__c FROM Contact Where Aws_ContactId__c IN :conId LIMIT 50000];   
                System.debug('LSTCON ###'+lstcon);
                
                for(Contact con:lstcon){
                    MapContact.put(con.Aws_ContactId__c,con.Id);
                 }
                 System.debug('MapContact ###'+MapContact);
            }   
             
            for(SurveyAnswer__c SAsave:SAObj)
            {
                if(MapSurvey.size()>0 && !MapSurvey.isEmpty()){
                    if(MapSurvey.containskey(SAsave.Aws_Survey__c)){
                        Id sId = MapSurvey.get(SAsave.Aws_Survey__c);
                        SAsave.s_seq__c = sId;
                    }
                 }
                
                 if(MapQuestion.size()>0 && !MapQuestion.isEmpty()){
                    if(MapQuestion.containskey(SAsave.Aws_SurveyQuestion__c)){
                        Id questionId = MapQuestion.get(SAsave.Aws_SurveyQuestion__c);
                        SAsave.q_seq__c = questionId ;
                    }
                 }
                 
                 if(MapItem.size()>0 && !MapItem.isEmpty()){
                    if(MapItem.containskey(SAsave.Aws_SurveyQuestionItem__c)){
                        Id ItemId = MapItem.get(SAsave.Aws_SurveyQuestionItem__c);
                        SAsave.i_seq__c = ItemId;
                    }
                 }
                 
                if(MapContact.size()>0 && !MapContact.isEmpty()){
                    if(MapContact.containskey(SAsave.Aws_ContactId__c)){
                        Id ContactId = MapContact.get(SAsave.Aws_ContactId__c);
                        SAsave.ContactId__c = ContactId;
                    }
                 }
            }
        
     }
}