trigger GetRecommendContact on RecommendContact__c(before insert,before update) {

    if(Trigger.isBefore)
    {
      
        List<String> lstConId= new List<String>();
        List<RecommendContact__c> lsttrcmndobj= new List<RecommendContact__c>();
        Map<id,Contact> MapContact=new map<id,Contact>(); 
         
        for(RecommendContact__c recmnd:Trigger.New)
        {
        
             lsttrcmndobj.add(recmnd);
             
             if(recmnd.Aws_ContactId__c!=null)
             {
             lstConId.add(recmnd.Aws_ContactId__c);
             }
        }
        
        if(lstConId.size()>0 && !lstConId.isEmpty())
        {
            MapContact =new Map<Id,Contact>([select id,Aws_ContactId__c from contact where Aws_ContactId__c IN:lstConId limit 50000]);
        }
        
        Map<String,Id> mapContactIds= new Map<String,Id>();
        for(Contact con: MapContact.Values()){
            mapContactIds.put(con.Aws_ContactId__c,con.Id);
        }
        
        List<RecommendContact__c> lstrecommandcontact = new list<RecommendContact__c>();
        
        for(RecommendContact__c RC:lsttrcmndobj)
        {
        
             if(RC.Aws_ContactId__c!=null && mapContactIds.get(RC.Aws_ContactId__c)!=null){
                     RC.Contact__c= mapContactIds.get(RC.Aws_ContactId__c);
             }
             else{
                  RC.Contact__c=null;
             }
         
             lstrecommandcontact.add(RC); 
          
        }  
    }
}