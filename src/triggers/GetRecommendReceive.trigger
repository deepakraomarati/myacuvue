trigger GetRecommendReceive on RecommendReceive__c(before insert,before update) {

     if(Trigger.isBefore)
     {
     
        List<String> lstConId= new List<String>();
        List<RecommendReceive__c> lsttrcmrvobj= new List<RecommendReceive__c>();
        Map<id,Contact> MapContact=new map<id,Contact>();
        
          for(RecommendReceive__c rcv:Trigger.New)
        {
        
             lsttrcmrvobj.add(rcv);
             
             if(rcv.Aws_ContactId__c!=null)
             {
             lstConId.add(rcv.Aws_ContactId__c);
             }
        }
        
        if(lstConId.size()>0 && !lstConId.isEmpty())
        {
            MapContact =new Map<Id,Contact>([select id,Aws_ContactId__c from contact where Aws_ContactId__c IN:lstConId limit 50000]);
        }
        
        Map<String,Id> mapContactIds= new Map<String,Id>();
        for(Contact con: MapContact.Values()){
            mapContactIds.put(con.Aws_ContactId__c,con.Id);
        }
         
        List<RecommendReceive__c> lstrecommandreceivee = new list<RecommendReceive__c>();
         
        for(RecommendReceive__c RR:lsttrcmrvobj)
        {
        
            if(RR.Aws_ContactId__c!=null && mapContactIds.get(RR.Aws_ContactId__c)!=null){
                     RR.Contact__c= mapContactIds.get(RR.Aws_ContactId__c);
            }
            else{
                 RR.Contact__c=null;
            }
         
            lstrecommandreceivee.add(RR);
          
        }    
     
    }
}