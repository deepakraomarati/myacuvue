trigger GetTradeHistory on TradeHistory__c (before insert,before update) {

     if(Trigger.isBefore)
      {
      
        List<String> lstAccId= new List<String>();
        List<String> lstConId= new List<String>();
      
        
        List<TradeHistory__c> lsttrhstryobj= new List<TradeHistory__c>();
        
        Map<id,Account> MapAccount=new map<id,Account>();
        Map<id,Contact> MapContact=new map<id,Contact>();
    
        
        
        for(TradeHistory__c trdhstry:Trigger.New)
        {
        
            lsttrhstryobj.add(trdhstry);
            
             if(trdhstry.Aws_OutletNumber__c !=null)
             {
             lstAccId.add(trdhstry.Aws_OutletNumber__c);
             }
         
            if(trdhstry.Aws_ContactId__c !=null)
            {
            lstConId.add(trdhstry.Aws_ContactId__c);
            }
            
          
        }
        
        
        if(lstAccId.size()>0 && !lstAccId.isEmpty())
         {
             MapAccount=new Map<Id,Account>([select id,OutletNumber__c from account where OutletNumber__c IN:lstAccId limit 50000]);
         } 
         
     
        
        if(lstConId.size()>0 && !lstConId.isEmpty())
        {
            MapContact =new Map<Id,Contact>([select id,Aws_ContactId__c from contact where Aws_ContactId__c IN:lstConId limit 50000]);
        }
       
        Map<String,Id> mapAccountIds= new Map<String,Id>();
        for(Account acc:MapAccount.Values()){
            mapAccountIds.put(acc.OutletNumber__c,acc.Id);
         }
         
        Map<String,Id> mapContactIds= new Map<String,Id>();
        for(Contact con: MapContact.Values()){
            mapContactIds.put(con.Aws_ContactId__c,con.Id);
         }
         
        List<TradeHistory__c> lsttradehistoryy = new list<TradeHistory__c>();
         
        for(TradeHistory__c TH:lsttrhstryobj)
        {
        
                if(TH.Aws_OutletNumber__c!=null && mapAccountIds.get(TH.Aws_OutletNumber__c)!=null){
                     TH.Account__c= mapAccountIds.get(TH.Aws_OutletNumber__c);
                }
                
                else{
                     TH.Account__c=null;
                     
                }
                
                if(TH.Aws_ContactId__c!=null && mapContactIds.get(TH.Aws_ContactId__c)!=null){
                     TH.Contact__c= mapContactIds.get(TH.Aws_ContactId__c);
                }
                else{
                     TH.Contact__c=null;
                }
          
                lsttradehistoryy.add(TH);
        
        }
        
    }

}