trigger GetTradeHistoryDetails on TradeHistoryDetails__c(before insert,before update) {

     if(Trigger.isBefore)
     {
         
          List<String> lstAccId= new List<String>();
          List<String> lstConId= new List<String>();
         
          
          List<TradeHistoryDetails__c> lsttrhstdtlobj= new List<TradeHistoryDetails__c>();
          
          Map<id,Account> MapAccount=new map<id,Account>();
          Map<id,Contact> MapContact=new map<id,Contact>();
         
          
          for(TradeHistoryDetails__c trdhstrydtl:Trigger.New)
          {
              
              lsttrhstdtlobj.add(trdhstrydtl);
              
              if(trdhstrydtl.Aws_OutletNumber__c !=null){
              lstAccId.add(trdhstrydtl.Aws_OutletNumber__c);
             }
              
              if(trdhstrydtl.Aws_ContactId__c !=null)
              {
              lstConId.add(trdhstrydtl.Aws_ContactId__c);
              } 
              
          }
          
         if(lstAccId.size()>0 && !lstAccId.isEmpty())
         {
             MapAccount=new Map<Id,Account>([select id,OutletNumber__c from account where OutletNumber__c IN:lstAccId limit 50000]);
         } 
          
         if(lstConId.size()>0 && !lstConId.isEmpty())
         {
            MapContact =new Map<Id,Contact>([select id,Aws_ContactId__c from contact where Aws_ContactId__c IN:lstConId limit 50000]);
         }
         
         
         Map<String,Id> mapAccountIds= new Map<String,Id>();
         for(Account acc:MapAccount.Values()){
             mapAccountIds.put(acc.OutletNumber__c,acc.Id);
         }
         
         Map<String,Id> mapContactIds= new Map<String,Id>();
         for(Contact con: MapContact.Values()){
            mapContactIds.put(con.Aws_ContactId__c,con.Id);
         } 
        
        List<TradeHistoryDetails__c> lsttradehstrdetailss = new list<TradeHistoryDetails__c>();
         
        for(TradeHistoryDetails__c THD:lsttrhstdtlobj)
        {
        
                 if(THD.Aws_OutletNumber__c!=null && mapAccountIds.get(THD.Aws_OutletNumber__c)!=null){
                         THD.Account__c= mapAccountIds.get(THD.Aws_OutletNumber__c);
                 }
                 
                  else{
                     THD.Account__c=null;
                 }
                 
                 if(THD.Aws_ContactId__c!=null && mapContactIds.get(THD.Aws_ContactId__c)!=null){
                         THD.Contact__c= mapContactIds.get(THD.Aws_ContactId__c);
                 }
                 else{
                     THD.Contact__c=null;
                 }
         
                 lsttradehstrdetailss.add(THD);
        }
    }
    
}