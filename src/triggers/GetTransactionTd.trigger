trigger GetTransactionTd on TransactionTd__c(before insert,before update) {

if(Trigger.isBefore)
 { 
    List<integer> coupId = new List<integer>();
    List<String> accId = new List<String>();
    List<String> conId = new List<String>();
    List<integer> camId = new List<integer>();
     
    Map<Id,Account> MapAccount =new Map<Id,Account>();
    Map<Id,Contact> MapContact =new Map<Id,Contact>();
    Map<Id,Campaign> MapCamp = new Map<Id,Campaign>();
    Map<Id,Coupon__c> MapCoupen =new Map<Id,Coupon__c>();
     
    List<TransactionTd__c> lsttrnsobj= new List<TransactionTd__c>();
    
    for(TransactionTd__c tobj:Trigger.New)
    {
        lsttrnsobj.add(tobj);
        
        if(tobj.Aws_CouponId__c !=null){
            coupId.add(integer.ValueOf(tobj.Aws_CouponId__c));
        }
        if(tobj.Aws_AccountId__c !=null){
            accId.add(tobj.Aws_AccountId__c);
        }
        if(tobj.Aws_ContactId__c !=null){
            conId.add(tobj.Aws_ContactId__c);
        }
        if(tobj.Aws_CampaignId__c !=null){
            camId.add(Integer.Valueof(tobj.Aws_CampaignId__c));
        }
    }
    
    if(accId .size()>0 && !accId .isEmpty())
     {
        MapAccount =new Map<Id,Account>([select id,Aws_AccountId__c from account where Aws_AccountId__c IN:accId]);
     } 
     
     if(conId .size()>0 && !conId .isEmpty())
     {
         MapContact=new Map<Id,Contact>([select id,Aws_ContactId__c from contact where Aws_ContactId__c IN:conId]);
     }
     
     if(coupId .size()>0 && !coupId .isEmpty())
     {
       MapCoupen=new Map<Id,Coupon__c>([select id,DB_ID__c from Coupon__c where DB_ID__c IN:coupId]);
     }
     
     if(camId.size()>0 && !camId.isEmpty())
     {
          MapCamp=new Map<Id,Campaign>([select id,DB_ID__c from campaign where DB_ID__c IN:camId]);
     }
     
      for(TransactionTd__c ts:lsttrnsobj)
      {
        if(MapAccount.size()>0 && !MapAccount.isEmpty() && ts.Aws_AccountId__c !=null){
           for(Account acc:MapAccount.Values()){
               if(acc.Aws_AccountId__c !=null && acc.Aws_AccountId__c ==ts.Aws_AccountId__c ){
                  ts.AccountID__c = acc.Id;
               }
            }
         }
         if(MapContact.size()>0 && !MapContact.isEmpty() && ts.Aws_ContactId__c!=null){
            for(Contact con:MapContact.Values()){
                if(con.Aws_ContactId__c !=null && con.Aws_ContactId__c == ts.Aws_ContactId__c){
                     ts.ContactId__c = con.Id;
                }
            }
         }
         if(MapCoupen.size()>0 && ! MapCoupen.isEmpty() && ts.Aws_CouponId__c !=null){
            for(Coupon__c cp:MapCoupen.Values()){
                if(cp.DB_ID__c !=null && cp.DB_ID__c == Integer.ValueOf(ts.Aws_CouponId__c)){
                    ts.CouponId__c = cp.Id;
               }
            }
         }
         if(MapCamp.size()>0 && !MapCamp.isEmpty() && ts.Aws_CampaignId__c !=null){
            for(Campaign camp:MapCamp.Values()){
                if(camp.DB_ID__c !=null && camp.DB_ID__c == Integer.ValueOf(ts.Aws_CampaignId__c)){
                     ts.CampaignId__c = camp.Id;
                    }
                }
            }
         }
      }    
 }