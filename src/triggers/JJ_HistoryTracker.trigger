// Trigger for inserting the data related to field changes 
trigger JJ_HistoryTracker on JJ_JPN_CustomerMasterRequest__c (after update) {
    
    final List<Schema.FieldSetMember> trackedFields = SObjectType.JJ_JPN_CustomerMasterRequest__c.FieldSets.JJ_JPN_FieldHistoryTracking.getFields();

    if (trackedFields.isEmpty()) return;
    
    JJ_HistoryTrackingFunctions.insertHistory(trigger.new,trackedFields,'JJ_JPN_CustomerMasterRequest__c');
    
}