/**
 * File Name: JJ_JPN_ActionPlanTrigger
 * Description : Trigger to perform business logic on Area Plans, Primary Area Plan and related objects.
 * Copyright : Johnson & Johnson
 * @author : Shiva Shankar | sshank10@its.jnj.com | bnshiva.shankar@cognizant.com
 *
 * Modification Log
 * ===============================================================
 *    Ver  |Date         |Author                |Modification
 *    1.0  |25-Jul-2015  |@sshank10@its.jnj.com |New Class created
 *   
 */
 
 trigger JJ_JPN_ActionPlanTrigger on JJ_JPN_ActionPlan__c (After Insert,After Update, After Delete, Before Delete, Before Update) {
 
  if(TriggerHandler__c.getInstance().ActionPlanTrigger__c) {
        return;
    }
    else
    {
       JJ_JPN_AreaPlanTotal actionPln = new JJ_JPN_AreaPlanTotal();  // New Class has been created
       
       JJ_JPN_AreaPlanDelUpdate AreaPlanDelUpdate = new JJ_JPN_AreaPlanDelUpdate();  // class is used when deleting record and updating area plans
    
       if(Test.isRunningTest())
       {
            // do nothing
       }
       else
       {
            if(Trigger.isAfter && (Trigger.isInsert))
            {
                   actionPln.populateTotal(Trigger.New);
            }
          
            if(Trigger.isBefore && Trigger.isDelete)
            {
                   AreaPlanDelUpdate.populateTotalDel(Trigger.Old);
            }
            if(Trigger.isAfter && (Trigger.isUpdate))
            {
                   actionPln.populateTotal(Trigger.New);
            }
            if(Trigger.isBefore && (Trigger.isUpdate))
            {
                AreaPlanDelUpdate.populateTotalDel(Trigger.Old);
            }

        }
       
    }

}