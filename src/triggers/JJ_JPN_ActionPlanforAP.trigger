trigger JJ_JPN_ActionPlanforAP on JJ_JPN_ActionPlanforAP__c (After Insert, After Update, Before Delete,After Delete, Before Insert, Before Update) 
{

    if(TriggerHandler__c.getInstance().ActionPlanAPTrigger__c) {
            return;
        }
        else
        {
            //new class created
            JJ_JPN_AccountPlanTotal acountPlan = new JJ_JPN_AccountPlanTotal();
            
            //new class created for primaryAreaPlan Indentifier
            JJ_JPN_PrimaryAreaPlan primaryAreaPlan = new JJ_JPN_PrimaryAreaPlan();
                                    
            // New Class created to delete action plan AP and update account Plan
            JJ_JPN_DelAPAndPriAreaPlanTotal delAPPAPTotal = new JJ_JPN_DelAPAndPriAreaPlanTotal();
            
        if(Test.isRunningTest()) {
            //do nothing for now
        }
        else
        {                       
            if(Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert))
            {
                primaryAreaPlan.populateAreaPlan(Trigger.New);
            }
            
            if(Trigger.isAfter && (Trigger.isInsert))
            {
                   acountPlan.populateTotal(Trigger.New);
            }
          
            if(Trigger.isBefore && Trigger.isDelete)
            {
                   delAPPAPTotal.DelAPAllTotal(Trigger.Old);
            }
            if(Trigger.isAfter && (Trigger.isUpdate))
            {
                acountPlan.populateTotal(Trigger.New);
            }
            if(Trigger.isBefore && (Trigger.isUpdate))
            {
                delAPPAPTotal.DelAPAllTotal(Trigger.Old);
            }
        }
    }
}