trigger JJ_JPN_AddressPopulateForConPostalCode on Contact (before insert,before update,after insert,after update) {
    
    if(trigger.IsInsert && trigger.Isafter)
    {
        JJ_JPN_ContactHospitalInsert_Handler.Insertingcallcontact(trigger.new);
    }    
    if(trigger.IsUpdate && trigger.Isafter)
    {
        JJ_JPN_ContactHospitalInsert_Handler.updatecallcontact(trigger.new,trigger.oldmap,trigger.newmap);
    }     
  if(trigger.isbefore) {     
  Set < String > PostalCodes = new Set < String> ();
 // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
  Id jjvproRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('HCP Contacts').getRecordTypeId();
  
  for (Contact contacts: trigger.new) {
     // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
      if(contacts.RecordTypeId != jjvproRecordTypeId)
      {
        PostalCodes.add(contacts.MailingPostalCode);
      }
  }
  List < JJ_JPN_Address__c > address = [select id, Name,JJ_JPN_State__c,JJ_JPN_City__c, JJ_JPN_Street__c,JJ_JPN_PostalCodes__c,JJ_JPN_StateJapan__c from JJ_JPN_Address__c where JJ_JPN_PostalCodes__c IN: PostalCodes];
  for (Contact contactsToUpdate: trigger.new ) {
     // Added this for JP JJVPRO to restrict records and not to pick and Contacts that are related to HCP Recordtype
      if(contactsToUpdate.RecordTypeId != jjvproRecordTypeId){
       for (JJ_JPN_Address__c addr: address) {
         if (contactsToUpdate.MailingPostalCode== addr.JJ_JPN_PostalCodes__c) {
          String street=addr.JJ_JPN_Street__c;
          System.debug('----street---'+street);
          Integer count=street.indexof('（',1);
          System.debug('----count---'+count);
          if (count>=1){
          street=street.substring(0,count);
          System.debug('----street(condition)---'+street);
          }
          else{
          street=addr.JJ_JPN_Street__c;
          } 
             if(contactsToUpdate.MailingCity==null || contactsToUpdate.MailingCity=='' )
                 {
                    contactsToUpdate.MailingCity= addr.JJ_JPN_City__c;
                 }         
             contactsToUpdate.MailingState= addr.JJ_JPN_State__c;   //Name is here state
    
            if( contactsToUpdate.MailingStreet==null ||  contactsToUpdate.MailingStreet=='' )
                {   
                    contactsToUpdate.MailingStreet=street;
                 }
                 
                 contactsToUpdate.JJ_JPN_StateCognos__c=addr.JJ_JPN_StateJapan__c;
          
         // contactsToUpdate.MailingState= addr.StateDummy__c;
         }
       }
      }
  }
  }
}