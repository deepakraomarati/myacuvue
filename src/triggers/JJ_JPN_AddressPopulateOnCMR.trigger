trigger JJ_JPN_AddressPopulateOnCMR on JJ_JPN_CustomerMasterRequest__c (before insert,before update) {
 
  //  JJ_JPN_CMR_approvalHelper.processapprovalstep(trigger.new);
 Set < String > PayerPostalCodes  = new Set < String > ();
 for (JJ_JPN_CustomerMasterRequest__c CMR: trigger.new) {
     PayerPostalCodes.add(CMR.JJ_JPN_PayerPostalCode__c);
     PayerPostalCodes.add(CMR.JJ_JPN_BillToPostalCode__c);
     PayerPostalCodes.add(CMR.JJ_JPN_SoldToPostalCode__c);
 }
 List < JJ_JPN_Address__c > address = [select id, Name, JJ_JPN_StateJapan__c, JJ_JPN_City__c, JJ_JPN_Street__c, JJ_JPN_PostalCodes__c from JJ_JPN_Address__c where JJ_JPN_PostalCodes__c IN: PayerPostalCodes];
 for (JJ_JPN_CustomerMasterRequest__c CMRToUpdate: trigger.new) {
  for (JJ_JPN_Address__c addr: address) {
    if (CMRToUpdate.JJ_JPN_PayerPostalCode__c == addr.JJ_JPN_PostalCodes__c) {
       String street=addr.JJ_JPN_Street__c;
       Integer count=street.indexof('（',1);
          if (count>1){
          street=street.substring(0,count);
          }
          else{
          street=addr.JJ_JPN_Street__c;
          }
        if(CMRToUpdate.JJ_JPN_PayerTownshipCity__c ==null || CMRToUpdate.JJ_JPN_PayerTownshipCity__c =='' )
        {
            CMRToUpdate.JJ_JPN_PayerTownshipCity__c = addr.JJ_JPN_City__c;
        }
        if( CMRToUpdate.JJ_JPN_PayerStreet__c ==null ||  CMRToUpdate.JJ_JPN_PayerStreet__c =='' )
        {
             CMRToUpdate.JJ_JPN_PayerStreet__c =street;
        }           
            CMRToUpdate.JJ_JPN_PayerState__c=addr.JJ_JPN_StateJapan__c;
    }
    if (CMRToUpdate.JJ_JPN_BillToPostalCode__c == addr.JJ_JPN_PostalCodes__c) {
           String street=addr.JJ_JPN_Street__c;
           Integer count=street.indexof('（',1);
          if (count>1){
          street=street.substring(0,count);
          }
          else{
          street=addr.JJ_JPN_Street__c;
          }
        
        if(CMRToUpdate.JJ_JPN_BillToTownshipCity__c  ==null || CMRToUpdate.JJ_JPN_BillToTownshipCity__c  =='' )
        {
           CMRToUpdate.JJ_JPN_BillToTownshipCity__c = addr.JJ_JPN_City__c;
        }
        if( CMRToUpdate.JJ_JPN_BillToStreet__c==null ||  CMRToUpdate.JJ_JPN_BillToStreet__c =='' )
        {
             CMRToUpdate.JJ_JPN_BillToStreet__c=street;
        }           
           CMRToUpdate.JJ_JPN_BillToState__c= addr.JJ_JPN_StateJapan__c;

    }
    if (CMRToUpdate.JJ_JPN_SoldToPostalCode__c == addr.JJ_JPN_PostalCodes__c) {
           String street=addr.JJ_JPN_Street__c;
       Integer count=street.indexof('（',1);
          if (count>1){
          street=street.substring(0,count);
          }
          else{
          street=addr.JJ_JPN_Street__c;
          }
        if(CMRToUpdate.JJ_JPN_SoldToTownshipCity__c==null || CMRToUpdate.JJ_JPN_SoldToTownshipCity__c =='' )
        {
           CMRToUpdate.JJ_JPN_SoldToTownshipCity__c= addr.JJ_JPN_City__c;
        }
        if( CMRToUpdate.JJ_JPN_SoldToStreet__c==null ||  CMRToUpdate.JJ_JPN_SoldToStreet__c =='' )
        {
            CMRToUpdate.JJ_JPN_SoldToStreet__c=street;
        } 
        
          CMRToUpdate.JJ_JPN_SoldToState__c= addr.JJ_JPN_StateJapan__c;
        
    }
   }
  }
  
    
//This class will capture outlet number from soldtocode(AlreadyRegistered):START
     if(trigger.isbefore){    
        list<JJ_JPN_CustomerMasterRequest__c> CMRlist=new list<JJ_JPN_CustomerMasterRequest__c>();
        list<Account>Accnt= new list<Account>();
        
        STRING SENDTOCODE ;
        set<string> sndcd=new set<string>();
        system.debug('-----id-----');
        system.debug('-----name----');
        for(JJ_JPN_CustomerMasterRequest__c CMR:trigger.new  ){
                sndcd.add(CMR.JJ_JPN_SendtoCode__c);
        }
        //CMRlist= [select id,Name,JJ_JPN_Send_to_Code_SAP__c,JJ_JPN_SendtoCode__c,JJ_JPN_SendtoCode__r.name,JJ_JPN_SendtoCode__r.OutletNumber__c from JJ_JPN_CustomerMasterRequest__c where id in:trigger.new ];
        Accnt=[select id,OutletNumber__c from Account where id in :sndcd ];
        
        if(Accnt.size()<1){
            for(JJ_JPN_CustomerMasterRequest__c CMR :trigger.new){
                CMR.JJ_JPN_Send_to_Code_SAP__c=null;
            }
        
        }
        for(JJ_JPN_CustomerMasterRequest__c CMR :trigger.new)
        {
        for(Account acn :Accnt)
        {
        if(CMR.JJ_JPN_SendtoCode__c==acn.id){
            if (CMR.JJ_JPN_SendToUnderRegistration__c==FALSE){
                //CMR.JJ_JPN_Send_to_Code_SAP__c=SENDTOCODE;
                CMR.JJ_JPN_Send_to_Code_SAP__c=acn.OutletNumber__c;
            }
            else if (CMR.JJ_JPN_SendToUnderRegistration__c==TRUE){
                //CMR.JJ_JPN_Send_to_Code_SAP__c=SENDTOCODE;
                CMR.JJ_JPN_Send_to_Code_SAP__c=null;
            }
        }
        
        }
        }
        
    }
    if(trigger.isbefore){ 
    for(JJ_JPN_CustomerMasterRequest__c CMR:trigger.new  ){
    if(CMR.JJ_JPN_SalesItem__c=='blank'){
    CMR.JJ_JPN_SalesItemCog__c='D';
    }
    else {
    CMR.JJ_JPN_SalesItemCog__c=CMR.JJ_JPN_SalesItem__c;
    }        
    }
    }
    
    if(trigger.isbefore){ 
    for(JJ_JPN_CustomerMasterRequest__c CMR:trigger.new  ){
    if(CMR.JJ_JPN_NoDeliveryCharges__c=='01'){
    CMR.No_Delivery_ChargesSAP__c=null;
    }
    if(CMR.JJ_JPN_NoDeliveryCharges__c=='02'){
    CMR.No_Delivery_ChargesSAP__c='B';
    }
    if(CMR.JJ_JPN_NoDeliveryCharges__c=='15'){
    CMR.No_Delivery_ChargesSAP__c='A';
    }
    if(CMR.JJ_JPN_NoDeliveryCharges__c=='16'){
    CMR.No_Delivery_ChargesSAP__c='C';
    }
           
    }
    }
    
   
    
//END  
 
 }