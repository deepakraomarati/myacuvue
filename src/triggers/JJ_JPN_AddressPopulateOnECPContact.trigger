trigger JJ_JPN_AddressPopulateOnECPContact on JJ_JPN_ECPContact__c(before insert,before update) {
 // Map<JJ_JPN_PostalCodes__c,JJ_JPN_Address__c> AddressMap=new Map<JJ_JPN_PostalCodes__c,JJ_JPN_Address__c>();
 Set < String > PostalCodes = new Set < String > ();
 for (JJ_JPN_ECPContact__c ecpcontacts: trigger.new) {
  PostalCodes.add(ecpcontacts.JJ_JPN_PostalCode__c);
 }
 List < JJ_JPN_Address__c > address = [select id, Name,JJ_JPN_StateJapan__c, JJ_JPN_State__c, JJ_JPN_City__c, JJ_JPN_Street__c, JJ_JPN_PostalCodes__c from JJ_JPN_Address__c where JJ_JPN_PostalCodes__c IN: PostalCodes];
 for (JJ_JPN_ECPContact__c ecpcontactsToUpdate: trigger.new) {
  for (JJ_JPN_Address__c addr: address) {
   if (ecpcontactsToUpdate.JJ_JPN_PostalCode__c == addr.JJ_JPN_PostalCodes__c) {
   String street=addr.JJ_JPN_Street__c;
      Integer count=street.indexof('（',1);
      if (count>1){
      street=street.substring(0,count);
      }
      else{
      street=addr.JJ_JPN_Street__c;
      }
        if(ecpcontactsToUpdate.JJ_JPN_TownshipsCity__c==null || ecpcontactsToUpdate.JJ_JPN_TownshipsCity__c=='' )
        {
                ecpcontactsToUpdate.JJ_JPN_TownshipsCity__c = addr.JJ_JPN_City__c;
        }
        if(ecpcontactsToUpdate.JJ_JPN_Street__c ==null || ecpcontactsToUpdate.JJ_JPN_Street__c  =='' )
        {
                 ecpcontactsToUpdate.JJ_JPN_Street__c =street;
        }
    //ecpcontactsToUpdate.MailingState= addr.JJ_JPN_State__c;//State field is not present on ecp contact.
   }
  }

 }
}