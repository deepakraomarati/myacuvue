/**
 * File Name: JJ_JPN_AreaPlanTriger
 * Description : Trigger to perform business logic on Area Plans, Primary Area Plan and related objects.
 * Copyright : Johnson & Johnson
 * @author : Shiva Shankar | sshank10@its.jnj.com | bnshiva.shankar@cognizant.com
 *
 * Modification Log
 * ===============================================================
 *    Ver  |Date         |Author                |Modification
 *    1.0  |29-Jul-2015  |@sshank10@its.jnj.com |New Class created
 *   
 */
 
trigger JJ_JPN_AreaPlanTriger on JJ_JPN_AreaPlan__c (After Insert, After Update) 
{
    if(TriggerHandler__c.getInstance().AreaPlanTriger__c) 
    {
       return;
    }
    else
    {
       //JJ_JPN_UpdateAreaPlanToRMDM areaplanhandler = New JJ_JPN_UpdateAreaPlanToRMDM();  // New Class has been created
       if(Test.isRunningTest())
       {
            // do nothing
       }
       else
       {
          if(trigger.IsInsert && trigger.IsAfter)
          {
              JJ_JPN_UpdateAreaPlanToRMDM.populateTotal(trigger.new);
          }
            
          if(trigger.IsUpdate && trigger.IsAfter){
          system.debug('stop recursion is-->'+JJ_JPN_UtilityClassRecursion.stoprecursionbefore);
              if(JJ_JPN_UtilityClassRecursion.stoprecursionbefore == false){
                  //JJ_JPN_UtilityClassRecursion.stoprecursionbefore = true;
                  
                  JJ_JPN_UpdateAreaPlanToRMDM.DescreaseTotal(Trigger.new);
              }
            }
       }
    }

}