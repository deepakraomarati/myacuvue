/*-------------------------------------------------------------------------------------------------------------------------------------
Created for :JPN Skywalker
Functionality:
On creation of Account from SAP, below updates take place
1.    It will update NON-SAP fields from Customer Master Request and create Contact from ECP 
2.    Address fields are updated based on Country Code(JPN)


----------------------------------------------------------------------------------------------------------------------------------------*/
trigger JJ_JPN_ContactCreationOnAccount on Account (before insert, before update , After Insert, After Update) {
    
     if(TriggerHandler__c.getInstance().JJ_JPN_ContactCreationOnAccount__c) 
    {
       return;
    }
    else
    {
     
        if(Test.isRunningTest())
       {
            // do nothing
       }
       else
       {
       
        if(trigger.isbefore){
            JJ_JPN_addressValidationonAccount.addressValidate();// Address fields are updated based on Country Code(JPN)        
        List<String> DaysInfo2= new List<String>();
            for(Account accnt :trigger.new)
                {
                DaysInfo2.clear();
                    //Incoming Individual Delivery Comments will get divided into 7 field values based on week days      
                    JJ_JPN_IndividualDeliveryPossibleDate obj2= new JJ_JPN_IndividualDeliveryPossibleDate(); 
                   
                    if(accnt.JJ_JPN_IndividualDeliveryPossibleDate__c!=''){ 
                        system.debug('===Inside If condition====='+accnt.JJ_JPN_IndividualDeliveryPossibleDate__c);
                        system.debug('====Before calling class===='+DaysInfo2);
                        DaysInfo2.addall(obj2.splitDaysInformation(accnt.JJ_JPN_IndividualDeliveryPossibleDate__c)); 
                        system.debug('===after calling class====='+DaysInfo2);
                        if(DaysInfo2.size()>0){ 
                            system.debug('===DaysInfoMonday===='+DaysInfo2[0]);
                            string str1;
                            str1=' あり ';
                            if(DaysInfo2[0]== ' あり '){                            
                            system.debug('===DaysInfoMonday2===='+DaysInfo2[0]);                              
                            DaysInfo2[0]='1'; 
                            system.debug('===DaysInfoMonday3===='+DaysInfo2[0]);     
                            accnt.JJ_JPN_IndividualDeliveryPossible__c=DaysInfo2[0];
                            system.debug('===Monday====='+accnt.JJ_JPN_IndividualDeliveryPossible__c);
                            }
                            else if(DaysInfo2[0]== ' なし '){
                            DaysInfo2[0]='0';
                            accnt.JJ_JPN_IndividualDeliveryPossible__c=DaysInfo2[0];      
                            }
                            
                            
                            if(DaysInfo2[1]== ' あり '){                                                                                     
                            DaysInfo2[1]='1'; 
                            accnt.JJ_JPN_IndividualDeliveryPossibleTues__c=DaysInfo2[1];
                            }
                            else if(DaysInfo2[1]== ' なし '){
                            DaysInfo2[1]='0';
                            accnt.JJ_JPN_IndividualDeliveryPossibleTues__c=DaysInfo2[1];      
                            }                            
                            
                            if(DaysInfo2[2]== ' あり '){                                                                                     
                            DaysInfo2[2]='1'; 
                            accnt.JJ_JPN_IndividualDeliveryPossibleWed__c=DaysInfo2[2];
                            }
                            else if(DaysInfo2[2]== ' なし '){
                            DaysInfo2[2]='0';
                            accnt.JJ_JPN_IndividualDeliveryPossibleWed__c=DaysInfo2[2];      
                            } 
                            
                            if(DaysInfo2[3]== ' あり '){                                                                                     
                            DaysInfo2[3]='1'; 
                            accnt.JJ_JPN_IndividualDeliveryPossibleThurs__c=DaysInfo2[3];
                            }
                            else if(DaysInfo2[3]== ' なし '){
                            DaysInfo2[3]='0';
                            accnt.JJ_JPN_IndividualDeliveryPossibleThurs__c=DaysInfo2[3];
                            }
                            
                            if(DaysInfo2[4]== ' あり '){                                                                                     
                            DaysInfo2[4]='1'; 
                            accnt.JJ_JPN_IndividualDeliveryPossibleDa__c=DaysInfo2[4];
                            }
                            else if(DaysInfo2[4]== ' なし '){
                            DaysInfo2[4]='0';
                            accnt.JJ_JPN_IndividualDeliveryPossibleDa__c=DaysInfo2[4];
                            }
                            
                            if(DaysInfo2[5]== ' あり '){                                                                                     
                            DaysInfo2[5]='1';
                            accnt.JJ_JPN_IndividualDeliveryPossibleDaSat__c=DaysInfo2[5];
                            }
                            else if(DaysInfo2[5]== ' なし '){
                            DaysInfo2[5]='0';
                            accnt.JJ_JPN_IndividualDeliveryPossibleDaSat__c=DaysInfo2[5];
                            }
                            
                            if(DaysInfo2[6]== ' あり '){                                                                                     
                            DaysInfo2[6]='1';
                            accnt.JJ_JPN_IndividualDeliveryPossibleSun__c=DaysInfo2[6];
                            }
                            else if(DaysInfo2[6]== ' なし '){
                            DaysInfo2[6]='0';
                            accnt.JJ_JPN_IndividualDeliveryPossibleSun__c=DaysInfo2[6];
                            }
                        }
                    }
                    
                    if(accnt.CountryCode__c=='jpn' && accnt.OutletNumber__c!='' && accnt.OutletNumber__c!=null)
                    {
                        if(Integer.ValueOf(accnt.OutletNumber__c) >=30000 &&  Integer.ValueOf(accnt.OutletNumber__c) <= 39999)
                        {
                          accnt.JJ_JPN_AccountOutetSA__c = true;
                        }
                    }
                    
                }
                
     
        
        }
        
       
                    
        if(trigger.isbefore ){           
        List<String> DaysInfo1= new List<String>();
            for(Account accnt:trigger.new)        
                {
                    DaysInfo1.clear();
                    //Incoming Individual Delivery Comments will get divided into 7 field values based on week days      
                    JJ_JPN_IndividualDeliveryComments obj2= new JJ_JPN_IndividualDeliveryComments(); 
                   
                    if(accnt.JJ_JPN_IndividualDeliveryComments__c!=''){ 
                        system.debug('===Inside If condition====='+accnt.JJ_JPN_IndividualDeliveryComments__c);
                        system.debug('====Before calling class===='+DaysInfo1);
                        DaysInfo1.addall(obj2.splitDaysInformation(accnt.JJ_JPN_IndividualDeliveryComments__c)); 
                        system.debug('===after calling class====='+DaysInfo1);
                        if(DaysInfo1.size()>0){           
                            accnt.JJ_JPN_IndividualDeliveryCommentsMon__c=DaysInfo1[0];
                            accnt.IndividualDeliveryCommentsTues__c=DaysInfo1[1];
                            accnt.IndividualDeliveryCommentsWed__c=DaysInfo1[2];
                            accnt.JJ_JPN_IndividualDeliveryCommentsThu__c=DaysInfo1[3];
                            accnt.JJ_JPN_IndividualDeliveryCommentsFri__c=DaysInfo1[4];
                            accnt.JJ_JPN_IndividualDeliveryCommentsSat__c=DaysInfo1[5];
                            accnt.JJ_JPN_IndividualDeliveryCommentsSun__c=DaysInfo1[6];
                        }
                    }
                }                 
        }
        
          if(trigger.isinsert && trigger.isbefore ){
          for(Account accnt: trigger.new){
              DateTime dT = System.now();
              Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());         
              accnt.JJ_JPN_MasterRegistrationDate__c=myDate; 
              system.debug('date==>'+accnt.JJ_JPN_MasterRegistrationDate__c);             
              }          
          }
          
           if(trigger.isbefore){
           for(Account accnt: trigger.new){
           if (accnt.JJ_JPN_SoldToNumberOfPagesReceived__c == '01'){
           accnt.JJ_JPN_SoldToNumberOfPagesReceived__c= '1';
           }
           if(accnt.JJ_JPN_BillSubmission__c=='ZYE0'){
           accnt.JJ_JPN_BillSubmission__c='0';
           }
           }
          }
        
        // Non SAP Fields updating from CMR   
       if((trigger.isinsert )&& trigger.isbefore){
            JJ_JPN_ContactCreationOnAccount_handler.accountupdate();
            
        }
        
        set <id> accid= new set<id>();
        set <id> exacid=new set<id>();
        list<Account> accList =new list <Account>();
        map<string,string> segmap=new map<string,string>();
        if( trigger.isafter && !test.isrunningtest())
        {
           for(Account accnt: trigger.new){
           accid.add(accnt.id);
           } 
           List<Customer_Segmentation_Matrix__c> CAID= [select id, AccountName__c from Customer_Segmentation_Matrix__c where  AccountName__r.id in :accid];
           system.debug('----CAID'+CAID);
           if(CAID.size()>0)
           {
               for(Customer_Segmentation_Matrix__c c : CAID){
                   segmap.put(c.id,c.AccountName__c);
                   exacid.add(c.AccountName__c);
               }
           }
           for(Account accnt: trigger.new){    
               if(!exacid.contains(accnt.id))
                   accList.add(accnt);   
           }
           if(!accList.isempty())
               JJ_JPN_CustomerAssessment.newRecord(accList);    
        }
        
        List<account> accts=new List<account>();
        set <id> accountid= new set<id>(); 
        set <id> cmrids= new set<id>();
        set <id> ecpids= new set<Id>();
        set <id> ecpcons= new set<Id>();
        set <String> accountNumber1= new set<String>();
        list<contact>contactToInsert=new list<contact>();
        list<JJ_JPN_ECPContact__c> ECPLIST= new list<JJ_JPN_ECPContact__c>();
        list <Contact>conlist=new list<Contact>();
        list<AccountContactRelation> ACCCONRELLIST=new list <AccountContactRelation>();
        map <id,id>conmap=new map<id,id>();
        //conlist=[Select Id,JJ_JPN_Contact_Unique__c from Contact];
        if(trigger.isInsert && Trigger.isAfter )
        {              
            for(Account a:trigger.new)
            {
                accountid.add(a.id);   
                accountNumber1.add(a.OutletNumber__c);            
            }
            List<JJ_JPN_CustomerMasterRequest__c> CMRID= [ select id from JJ_JPN_CustomerMasterRequest__c where  ((JJ_JPN_SoldToCode__c= :accountNumber1) or (JJ_JPN_PayerCode__c = :accountNumber1) or (JJ_JPN_BillToCode__c= :accountNumber1 )  )];
            system.debug('----cmrid'+cmrid);
            for ( JJ_JPN_CustomerMasterRequest__c cmr : CMRID)
            {
                cmrids.add(cmr.id);   
            }
            List<JJ_JPN_ECPContact__c> ECPCON= [ select id,Name,JJ_JPN_DoctorNamekanji__c,JJ_JPN_ECPNameKana__c,JJ_JPN_University__c,JJ_JPN_Hobby__c,JJ_JPN_FamilyStructure__c,
                                                JJ_JPN_Birthplace__c,JJ_JPN_MedicalSocietyExecutives__c,JJ_JPN_CustomerID__c,JJ_JPN_Fax__c,JJ_JPN_JobCategory__c, JJ_JPN_Department__c, 
                                                JJ_JPN_Phone__c ,JJ_JPN_Street__c, JJ_JPN_Email__c, JJ_JPN_PostalCode__c , JJ_JPN_Prefectures__c, JJ_JPN_TownshipsCity__c, 
                                                JJ_JPN_OtherAddress__c, JJ_JPN_AccountName__c,JJ_JPN_ReportsTo__c,JJ_JPN_AffiliationSociety__c,JJ_JPN_CLHistoryOfUseDrinkingSmoking__c,JJ_JPN_S4Type__c,
                                                JJ_JPN_InterestInCL__c,JJ_JPN_InterestInAOS__c,JJ_JPN_OperationsCarriedOut__c,JJ_JPN_Specialist__c,JJ_JPN_KOL__c,JJ_JPN_Birthday__c,
                                                ecp__r.JJ_JPN_SoldToCode__c,ecp__r.JJ_JPN_PayerCode__c,ecp__r.JJ_JPN_BillToCode__c from JJ_JPN_ECPContact__c where ecp__c in :cmrid];
            
            //id recordtypeid=[select recordtype.id from contact where recordtype.developername='JJ_JPN_ECPJapan' limit 1].recordtype.id;
            Id recordTypeId=[SELECT Id,developerName,Name,SobjectType  FROM RecordType where SobjectType ='Contact' and developerName='JJ_JPN_ECPJapan'].Id;
            if(!ECPCON.ISEMPTY()){
            for(JJ_JPN_ECPContact__c ecp2 : ECPCON){
                ecpids.add(ecp2.id);   
           } 
           }
           system.debug('ecpids>>>'+ecpids);
           if(!ecpids.ISEMPTY()&& ecpids!=null){
            conlist=[Select Id,JJ_JPN_Contact_Unique__c from Contact where Country__c='Japan' and JJ_JPN_Contact_Unique__c in :ecpids];
            
            for(Contact con1:conlist){
             if(con1.JJ_JPN_Contact_Unique__c !=null){
                 ecpcons.add(con1.JJ_JPN_Contact_Unique__c);
                 conmap.put(con1.JJ_JPN_Contact_Unique__c, con1.id);
             }
            } 
            
            }
            
            system.debug('ecpcons>>>'+ecpcons);
            system.debug('ECPCON Test==>'+ECPCON);
            for(Account acnt:trigger.new){
            //system.debug()
                for(JJ_JPN_ECPContact__c ecp :ECPCON ){
                 if(ecpcons!=null){
                 if(!conmap.containskey(ecp.id)){
                    contact con= new contact ();
                    if(ecp.ecp__r.JJ_JPN_SoldToCode__c==acnt.OutletNumber__c || ecp.ecp__r.JJ_JPN_PayerCode__c==acnt.OutletNumber__c|| ecp.ecp__r.JJ_JPN_BillToCode__c==acnt.OutletNumber__c){
                        con.AccountId=acnt.id;
                        con.JJ_JPN_Contact_Unique__c=ecp.id;
                        con.LastName= ecp.JJ_JPN_DoctorNamekanji__c;
                        con.RecordTypeId= recordTypeId;
                        con.JJ_JPN_NameKana__c=ecp.JJ_JPN_ECPNameKana__c;
                        system.debug('NameECPNameKanji-->'+ecp.Name);                                               
                        con.JJ_JPN_ClinicName__c=ecp.Name;
                        system.debug('con.JJ_JPN_ClinicName__c-->'+con.JJ_JPN_ClinicName__c); 
                        con.MailingPostalCode=ecp.JJ_JPN_PostalCode__c;
                        con.JJ_JPN_Prefectures__c=ecp.JJ_JPN_Prefectures__c;
                        con.MailingCity= ecp.JJ_JPN_TownshipsCity__c;
                        con.MailingStreet= ecp.JJ_JPN_Street__c;
                        con.JJ_JPN_OtherAddress__c= ecp.JJ_JPN_OtherAddress__c;
                        con.JJ_JPN_CustomerID__c=ecp.JJ_JPN_CustomerID__c;
                        con.Department= ecp.JJ_JPN_Department__c;
                        con.Occupation__c= ecp.JJ_JPN_JobCategory__c;
                        con.ReportsToId = ecp.JJ_JPN_ReportsTo__c;
                        con.Phone= ecp.JJ_JPN_Phone__c;
                        //      system.debug('===>>ecp--'+ecp.JJ_JPN_Phone__c+'=====>contact'+con.MobilePhone);
                        con.Fax=ecp.JJ_JPN_Fax__c;
                        con.JJ_JPN_University__c=ecp.JJ_JPN_University__c;
                        con.JJ_JPN_MedicalSocietyExecutives__c=ecp.JJ_JPN_MedicalSocietyExecutives__c;    
                        con.JJ_JPN_AffiliationSociety__c=ecp.JJ_JPN_AffiliationSociety__c;
                        con.JJ_JPN_Birthplace__c=ecp.JJ_JPN_Birthplace__c;
                        con.JJ_JPN_FamilyStructure__c=ecp.JJ_JPN_FamilyStructure__c;
               //       system.debug('===>>familystructure--'+con.JJ_JPN_FamilyStructure__c);
                        con.JJ_JPN_Hobby__c=ecp.JJ_JPN_Hobby__c;
                        con.Birthdate=ecp.JJ_JPN_Birthday__c;
                        con.Email= ecp.JJ_JPN_Email__c;
                        con.JJ_JPN_CLHistoryOfUseDrinkingSmoking__c=ecp.JJ_JPN_CLHistoryOfUseDrinkingSmoking__c;
                        con.JJ_JPN_S4Type__c=ecp.JJ_JPN_S4Type__c;
                        con.JJ_JPN_InterestInCL__c=ecp.JJ_JPN_InterestInCL__c;
                        con.JJ_JPN_InterestInAOS__c=ecp.JJ_JPN_InterestInAOS__c;
                        con.JJ_JPN_OperationsCarriedOut__c=ecp.JJ_JPN_OperationsCarriedOut__c;
                        con.JJ_JPN_Specialist__c=ecp.JJ_JPN_Specialist__c;
                        con.JJ_JPN_KOL__c=ecp.JJ_JPN_KOL__c;          
                        contactToInsert.add(con);
                    }
                }
                else
                {
                    if(conmap.containskey(ecp.id)){
                        AccountContactRelation ACCCONREL= new AccountContactRelation ();
                        ACCCONREL.AccountId=acnt.Id;
                        ACCCONREL.ContactId=conmap.get(ecp.id);
                        ACCCONRELLIST.add(ACCCONREL);
                    }
                    
                
                }
                }
                }
            if(!contactToInsert.isempty()){
                database.saveresult[] sr=Database.insert (contactToInsert,false);
                system.debug('contact>>>>'+sr);
                
            }
            if(!ACCCONRELLIST.isempty()){
                Database.insert (ACCCONRELLIST, false);
            }
        }
        
       
        
        
     }    
    
    }
    }
}