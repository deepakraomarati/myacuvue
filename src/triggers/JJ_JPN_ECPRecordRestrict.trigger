trigger JJ_JPN_ECPRecordRestrict on JJ_JPN_ECPContact__c (before insert, before update) {
set<ID> ids= new set<ID>();
for(JJ_JPN_ECPContact__c c:trigger.new)
{
if(c.ECP__c!=null)
ids.add(c.ECP__c);
}
List<JJ_JPN_ECPContact__c> cts1=[select id,name from JJ_JPN_ECPContact__c where ECP__c in :ids];
System.debug('contacts are ======>'+cts1);
if((Trigger.isInsert &&cts1.size()>0)||(Trigger.isUpdate &&cts1.size()>1))
{
trigger.new[0].addError('Cannot add more than 1 ECP\'s ');
}
}