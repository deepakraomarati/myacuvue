/*
* File Name: JJ_JPN_OpportunityTrigger 
* Description : Trigger to perform business logic on Primary Account Plans and Primary Area Plans.
* Copyright : Johnson & Johnson
* @author : Shikha Jain
*
*/
trigger JJ_JPN_OpportunityTrigger on Opportunity  (After Insert,After Update, After Delete, Before Delete, Before Update) 
{
    If(!test.isRunningTest() && !TriggerHandler__c.getInstance('HandleTriggers').JJ_JPN_Opportunity__c) {
        return;    
    }
	else{
        
        if(Trigger.isAfter)
        {
            if (Trigger.isInsert || Trigger.isUpdate) 
            {
                JJ_JPN_OpportunityPlanTotal optyPln = new JJ_JPN_OpportunityPlanTotal ();  // New Class has been created
                optyPln.populateTotal(Trigger.New);
            }
        }
        
        if(Trigger.isBefore)
        {            
            //if (Trigger.isDelete|| Trigger.isUpdate) 
            if (Trigger.isUpdate) 
            {
                JJ_JPN_OpportunityPlanDelUpdate OpptyPlanDelUpdate = new JJ_JPN_OpportunityPlanDelUpdate ();  // class is used when deleting record and updating area plans
                OpptyPlanDelUpdate.populateTotalDel(Trigger.Old);
            }
        }
    }
}