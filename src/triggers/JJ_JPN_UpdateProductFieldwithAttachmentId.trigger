/**
* File Name: JJ_JPN_UpdateProductFieldwithAttachmentId 
* Description : This trigger will update the attchement id on product detail object
* Copyright : Johnson & Johnson
* @author : Shikha Jain
* 
*/
trigger JJ_JPN_UpdateProductFieldwithAttachmentId on Attachment (before insert,after insert,after delete) 
{
    String ParentId;
    Set<Id> setOfProductIdwithAttchment= new Set<Id>();    
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            ParentId= Trigger.New[0].ParentId;
        } if(Trigger.isDelete){   
            ParentId= Trigger.old[0].ParentId;
        }
        List<Product2> prd = [select id, Id_of_Picture__c from Product2 where id =: ParentId];
        if(prd.size()>0)
        {
             if(Trigger.isInsert){
                prd[0].Id_of_Picture__c  = Trigger.New[0].id;
            }
            if(Trigger.isDelete){   
                prd[0].Id_of_Picture__c  ='';
                
            }
            update prd; 
        }
    }
    if(Trigger.isBefore)
    {
            for(Product2  PrdId:[SELECT Id, (SELECT Id FROM Attachments) FROM Product2 WHERE Id =:Trigger.New[0].ParentId])
            {
                if(!PrdId.Attachments.isEmpty())
                {
                     setOfProductIdwithAttchment.add(PrdId.Id);
                }
            }
            
            for(Attachment attchObj: Trigger.New)
            {
                if(setOfProductIdwithAttchment.contains(attchObj.ParentId))
                {
                    attchObj.addError('Only one Attachment can be added');
                }
            }
    }
}