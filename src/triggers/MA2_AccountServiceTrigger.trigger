/**
* File Name: MA2_AccountServiceTrigger 
* Description : Trigger for Sending Account Info to Apigee System
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |21-Mar-2016 |hsingh53@its.jnj.com  |New Class created
*/
trigger MA2_AccountServiceTrigger on Account (after insert, after update) {
    if(!system.isbatch() && !System.isFuture())    // chaitu
    {
        if(checkRecursive.runOnce()){
            MA2_AccountService accService = new MA2_AccountService();
            accService.accountInfo(Trigger.New);
        }
    }
}