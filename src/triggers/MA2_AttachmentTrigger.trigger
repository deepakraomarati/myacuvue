/**
* File Name: MA2_AttachmentTrigger
* Description : Class for sending Coupon Info to Apigee System
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |2-Sep-2016  |hsingh53@its.jnj.com   |New Class created
*/
trigger MA2_AttachmentTrigger on Attachment (after insert,after update) {
     MA2_CouponSendToApigee.sendData(Trigger.New);
}