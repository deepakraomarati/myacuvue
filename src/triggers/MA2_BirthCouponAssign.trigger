trigger MA2_BirthCouponAssign on Contact(after insert,after update){
    
    if(Trigger.isInsert){
        MA2_BirthdayCoupon.insertCoupon(Trigger.New,false);    
    }
    
    if(Trigger.isUpdate){
        MA2_BirthdayCoupon.updateContact(Trigger.New , Trigger.Old);
    }
    
}