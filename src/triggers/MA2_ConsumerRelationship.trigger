trigger MA2_ConsumerRelationship on Consumer_Relation__c (before insert,before Update,after insert , after update) {
    if(Trigger.isBefore && Trigger.isInsert){
        if(MA2_checkRecursive.runOnce()) {
            MA2_Consumer.apigeeMap(Trigger.new);
            MA2_Consumer.consumerActive(Trigger.new, Trigger.isUpdate, Trigger.isInsert, Trigger.isBefore);
        }
    }
    if(Trigger.isInsert && Trigger.isAfter){
    List<Consumer_Relation__c > cnList = new List<Consumer_Relation__c>();
        for(Consumer_Relation__c con : Trigger.new){
            if(con.MA2_ApigeeUpdate__c == False){
                cnList.add(con);
                //MA2_ConsumerService.sendData(Trigger.New);
            }
        }
        MA2_ConsumerService.sendData(cnlist);
    }
     if(Trigger.isBefore && Trigger.isUpdate){
            MA2_cnsumerfldupdate.cnsmrupdateValidation(Trigger.new);
        }
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            List<Consumer_Relation__c> conList = new List<Consumer_Relation__c>();
            for(Consumer_Relation__c con : Trigger.new){
                if(con.MA2_ApigeeUpdate__c == False){
                    conList.add(con);
                }
            }
            MA2_cnsumerfldupdate.cnsmrupdate(conList);
        }
    }
    Set<Id> ContactIds = new Set<Id>();
    Set<Id> AccountIds = new Set<Id>();
    if(Trigger.IsAfter && Trigger.IsInsert){
        for(Consumer_Relation__c item : Trigger.New){
            if(item.MA2_MyECP__c == true){
                ContactIds.add(item.ContactId__c);
                AccountIds.add(item.AccountId__c);
            }
        }
        MA2_PopulateStoreName.StoreName(ContactIds,AccountIds);
    }
}