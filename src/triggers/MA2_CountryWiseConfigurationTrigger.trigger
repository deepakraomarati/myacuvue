/**
* File Name: MA2_CountryWiseConfigurationTrigger 
* Description : Sending CountryWiseConfiguration to Apigee System
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |14-Oct-2016 |hsingh53@its.jnj.com  |New Class created
*/
trigger MA2_CountryWiseConfigurationTrigger on MA2_CountryWiseConfiguration__c (before insert,before update,after insert , after update) {
  /*  if(Trigger.isBefore){
        MA2_CountryConfigClass.mapConfig(Trigger.new);
    }  */
   
    if(Trigger.isBefore)
    {
    MA2_CountryConfigClass.mapConfig_validation(Trigger.new);
    }
    
    if(Trigger.isAfter){
        MA2_CountryWiseConfiguration.sendData(Trigger.New);
    }
}