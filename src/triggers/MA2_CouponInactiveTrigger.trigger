trigger MA2_CouponInactiveTrigger on CouponContact__c (after update) {
    MA2_CouponInactive.updateCouponWallet(Trigger.New);        
}