trigger MA2_CouponModify on MA2_CouponImage__c (after update , after insert) {
    
    Set<Id> couponId = new Set<Id>();
    List<Coupon__c> couponList = new List<Coupon__c>();
    for(MA2_CouponImage__c couponImg : Trigger.New){
        if(couponImg.MA2_Coupon__c != null){
            couponId.add(couponImg.MA2_Coupon__c); 
        }
    }
    
    if(couponId.size() > 0){
           couponList.addAll([select Id from coupon__c where Id in: couponId]);     
    }
    if(couponList.size() > 0){
        update couponList;
    }

}