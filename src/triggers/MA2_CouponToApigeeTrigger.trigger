/**
* File Name: MA2_CouponToApigeeTrigger
* Description : Class for sending Coupon Info to Apigee System
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |2-Sep-2016  |hsingh53@its.jnj.com   |New Class created
*/
trigger MA2_CouponToApigeeTrigger on Coupon__c (after insert,after update) {
    if(trigger.isinsert){
        //MA2_CouponToApigee.sendData(Trigger.New);
        MA2_CouponServicetoApigee.sendData(Trigger.new);
    }
    if(trigger.isupdate){
        MA2_CouponUpdateService.sendData(Trigger.New);
    }
}