/*
* File Name: MA2_CouponWalletForApigee
* Description : Sending data to Apigee System
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |27-Sep-2016 |hsingh53@its.jnj.com  |New Class created
*/
trigger MA2_CouponWalletForApigee on CouponContact__c (before insert,before update,after insert,after update) {
    if(Trigger.isBefore){
        MA2_CouponWalletMapping.walletMap(Trigger.new);
    }
    
    if(Trigger.isAfter && !System.isBatch()){
        if(Trigger.isInsert){
            MA2_CouponWallet.sendData(Trigger.New,'trigger' , 'insert');
        }else if(Trigger.isUpdate){
            MA2_CouponWallet.sendData(Trigger.New,'trigger' , 'update');
        }
    }
}