trigger MA2_GetAccount on Contact (before insert,before update) {

    if(Trigger.isBefore)
    {
    
     List<String> lstAccId= new List<String>();
   
     List<Contact> conList= new List<Contact>();
      
     Map<id,Account> MapAccount=new map<id,Account>();
    
      for(Contact Con:Trigger.New)
      {
        conList.add(Con);
        
        if(Con.Aws_AccountId__c!=null){
            lstAccId.add(Con.Aws_AccountId__c);
        }
        
      }
      
      if(lstAccId.size()>0 && !lstAccId.isEmpty())
      {    
         MapAccount=new Map<Id,Account>([select id,Name,Aws_AccountId__c,OutletNumber__c from account where Aws_AccountId__c IN:lstAccId limit 50000]);
      } 
         
      Map<String,String> mapAccountIds= new Map<String,String>();
      for(Account acc:MapAccount.Values()){
          mapAccountIds.put(acc.Aws_AccountId__c,acc.OutletNumber__c);
      }
         
          
      List<Contact> contactList= new List<Contact>();

      for(Contact con:conList)
      {
            if(con.Aws_ContactId__c!=null){
               con.MA2_Country_Code__c='KOR';
               if(con.Gender__c != null){
                   if(con.Gender__c.toLowerCase() == 'm'){
                       con.MA2_Gender__c = 'Male';
                   }else if(con.Gender__c.toLowerCase() == 'f'){
                       con.MA2_Gender__c = 'Female';
                   }
               }
               if(con.Status__c != null){
                   if(con.Status__c == 'N'){
                       con.MA2_Status__c= false;
                   }else {
                       con.MA2_Status__c = true;
                   }
               }
               con.MA2_Uuid__c = con.Uuid__c;
               con.MA2_GrandPoint__c=con.MembershipPoint__c;
               con.MA2_UsePoint__c=con.UsePoint__c;
               con.MA2_TotalPoint__c = con.TotalPoint__c;
               con.Email = con.email__c;
               con.MA2_AccountID__c = mapAccountIds.get(con.Aws_AccountId__c);
            }
       }
           
    }
}