trigger MA2_GetNewFitAppointment on AccountBooking__c (before insert,before update,After insert, After Update)
{
  
    if(trigger.isBefore)
    {
      MA2_NewFitAppointment.apigeeMap(Trigger.New);
      
      if(trigger.isUpdate)
       MA2_AppointmentECP.Before_AccBookingUpdate(Trigger.New,Trigger.oldMap); 
    }
    
    if(trigger.isAfter && trigger.isInsert){
        MA2_AppointmentECP.Afterinsert(trigger.new);
     
    }
    
    if(trigger.isAfter && trigger.isUpdate)
    {
          MA2_AppointmentECP.AfterUpdate(trigger.new, trigger.old);
          MA2_AppointmentService.sendData(trigger.new);
       
    }
    
}