/**
* File Name: MA2_RelatedAccountTrigger 
* Description : Class for sending Related Accounts Record to Apigee System
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |2-Sep-2016  |hsingh53@its.jnj.com   |New Class created
*/
trigger MA2_RelatedAccountTrigger on MA2_RelatedAccounts__c (after insert ,after update,before delete,before insert,before update, after delete) {
    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
        MA2_SFDCUpdateRoleService.sfdcsendData(Trigger.New);
       
    }
    if(Trigger.isAfter && Trigger.isInsert){
        MA2_RelatedChildAccount.afterinsert(Trigger.New);
         MA2_ProfileAccessonECP.ProfileAccessonECP(Trigger.new);        
    }
    if(Trigger.isBefore && Trigger.isDelete){
        if(MA2_checkRecursive.runOnce()){
            MA2_RelatedChildAccount.beforeDelete(Trigger.Old);
        }
    }
    if(Trigger.isBefore && Trigger.isInsert){
        MA2_RelatedAccounts.apigeeMap(Trigger.new);
        
    }
    if(Trigger.isBefore && Trigger.isUpdate){
        MA2_RelatedAccounts.apigeeMap(Trigger.new);
        
    }
    if(Trigger.isAfter && Trigger.isDelete){
        MA2_ECPrelationshipDeleteService.sendECPRelDataToApigee(Trigger.Old);
    }
}