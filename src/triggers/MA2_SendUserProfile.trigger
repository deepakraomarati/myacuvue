trigger MA2_SendUserProfile on MA2_UserProfile__c (after insert,after update) {
    if(trigger.isAfter)
    {
        MA2_SFDCRoleMasterService.sendData(trigger.new);
    }
}