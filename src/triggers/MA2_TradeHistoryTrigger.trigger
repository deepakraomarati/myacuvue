trigger MA2_TradeHistoryTrigger on TradeHistory__c (before insert,after insert,after update,before update){
    if(Trigger.isBefore){
        ProductExternalId prod = new ProductExternalId();
        prod.insertproductexternalId(Trigger.New);    
    }else{
        if(Trigger.isInsert){
            MA2_TradeHistoryDetails.createTransaction(Trigger.New); 
        }
        if(Trigger.isUpdate){
            MA2_TradeHistoryDetails.updateTransaction(Trigger.New); 
        }
    }   
}