/**
* File Name: MA2_createContact
* Description : Trigger for creating etrial / premium coupon
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |25-Aug-2016  |hsingh53@its.jnj.com  |New Class created
*/
trigger MA2_createContact on Contact (after insert,after update , before insert , before update, after delete) {
    //after update delete and assign new Coupon
    //after insert/update send records to Apigee System
    TriggerHandler__c mcs = TriggerHandler__c.getvalues('HandleTriggers');
    if(mcs!= null &&  mcs.MA2_createContact__c==true){ 
        
        if(Trigger.isAfter && Trigger.IsUpdate){
            MA2_Coupon.afterUpdate(Trigger.New);
            MA2_ContactSendtoApigee.sendData(Trigger.New);
            MA2_ConsumerECPSGPCheck.MA2_ECPSGPCheck(Trigger.new);
        }
        
        if(Trigger.isAfter && Trigger.IsInsert){
            MA2_Coupon.afterInsert(Trigger.New);
            MA2_ConsumerECPSGPCheck.MA2_ECPSGPCheck(Trigger.new);    
        }
        
        if(Trigger.isBefore && Trigger.IsUpdate){
            MA2_Coupon.beforeUpdate(Trigger.New,Trigger.Old);
            MA2_ConsumerECPSGPCheck.MA2_ConsumerSGPCheck(trigger.new);
            //before update Calculating Age
            MA2_ConsumerAge.sendAgeData(trigger.new);
        }
        
        if(Trigger.isBefore && Trigger.IsInsert){
            MA2_Coupon.beforeInsert(Trigger.New);
            MA2_ConsumerECPSGPCheck.MA2_ConsumerSGPCheck(trigger.new);
        }
        
        if(Trigger.isAfter && Trigger.IsDelete){
            MA2_ContactDeleteService.sendContactDataToApigee(Trigger.Old);
            MA2_ConsumerECPSGPCheck.MA2_ECPSGPDelete(trigger.old);
        }
    }
}