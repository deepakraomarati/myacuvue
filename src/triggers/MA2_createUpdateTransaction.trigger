/**
* File Name: MA2_createUpdateTransaction 
* Description : class for updating points in Contact and Account along with assigning the account and contact
                based on Apigee external Id for the transaction Record
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date        |Author                |Modification
*    1.0  |25-Sep-2016 |hsingh53@its.jnj.com  |New Class created
*/
trigger MA2_createUpdateTransaction on TransactionTd__c(before insert,before update,after insert,after update) {
    TriggerHandler__c mcs = TriggerHandler__c.getvalues('HandleTriggers');
   if(mcs.MA2_createUpdateTransaction__c== True)
   {   
    if(Trigger.isBefore){ 
          MA2_TransactionRecord.beforeCreateUpdate(Trigger.New);  
          if(Trigger.isUpdate){  
          MA2_TransactionCreatedDateupdate.updatecreatedate(Trigger.new); 
          }       
     }
     
     if(Trigger.isAfter){  
        if(MA2_checkRecursive.DoNotRun()) {
             MA2_TransactionAPI.sendData(Trigger.New);
         }
     }
   }
 }