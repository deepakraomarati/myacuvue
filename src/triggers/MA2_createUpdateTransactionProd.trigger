/**
* File Name: MA2_createUpdateTransactionProd
* Description : Class for mapping AccountId/ContactIds with MYACUVUE ACCOUNTID/CONTACTID
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |2-Sep-2016  |hsingh53@its.jnj.com   |New Class created
*/
trigger MA2_createUpdateTransactionProd on MA2_TransactionProduct__c (before insert , before update ,after insert, after update , after delete,before delete) {
     TriggerHandler__c mcs = TriggerHandler__c.getvalues('HandleTriggers');
   if(mcs.MA2_createUpdateTransactionProd__c== True)
    {  
    if(Trigger.isBefore && !(Trigger.isDelete)){
        MA2_TransactionProdRecord.beforeCreateUpdate(Trigger.New);
       
    }
          if(Trigger.isBefore && Trigger.isDelete){
              Set<Id> ids = Trigger.oldmap.keyset();
    MA2_TransactionProductDeleteAPI.sendData([select id, lastmodifiedBy.Name, MA2_CountryCode__c, MA2_ProductQuantity__c, UPC_Code__c,MA2_LotNo__c, MA2_ProductId__c, 
                                                  MA2_Transaction__r.MA2_AccountId__c, MA2_Transaction__r.MA2_ECPId__c, MA2_Transaction__r.MA2_TransactionId__c, 
                                                  MA2_Transaction__r.MA2_Contact__r.MembershipNo__c from MA2_TransactionProduct__c where id in : ids and (MA2_Transaction__r.MA2_TransactionType__c = 'Products')]);
    }
    if(Trigger.isAfter)
        {
        if(Trigger.isDelete)
            MA2_TransactionProdRecord.updateTransactionRecord (Trigger.Old);
        
        if(Trigger.isUpdate || Trigger.isInsert)
            MA2_TransactionProdRecord.updateTransactionRecord (Trigger.New);
            
         if(Trigger.isInsert){
             MA2_TransactionProductAPI.sendProductData(Trigger.New);   
             } 
        }
    }
}