/**
* File Name: MA2_sendTextContent
* Description : Class for sending Text Content Record to Apigee System
* Copyright : Johnson & Johnson
* @author : Harsh Singh | hsingh53@its.jnj.com | Harsh.Singh2@cognizant.com
* 
* Modification Log 
* =============================================================== 
*    Ver  |Date         |Author                |Modification
*    1.0  |2-Sep-2016  |hsingh53@its.jnj.com   |New Class created
*/
trigger MA2_sendTextContent on MA2_TextContent__c (after insert , after update) {
    MA2_TextContentService.createJsonBody(Trigger.New);
}