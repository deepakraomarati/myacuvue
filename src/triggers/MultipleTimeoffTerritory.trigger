trigger MultipleTimeoffTerritory on Event (before update,before insert) {
map<ID,event> oldmapevent=trigger.oldmap;
list<event> newvalevent=trigger.new;
list<event> evtupdatelist=new list <event>();
if(trigger.isbefore && (trigger.isinsert))
    if(checkRecursive.runOnce()){
    updateavailabledays.updtavldays(trigger.new);
    }
if(trigger.isbefore && (trigger.isupdate))
    if(checkRecursive.runOnce()){
        for(event e : newvalevent){
            if(e.StartDateTime!=oldmapevent.get(e.id).StartDateTime||e.EndDateTime !=oldmapevent.get(e.id).EndDateTime){
                evtupdatelist.add(e);
            
            }
         }
         if(!evtupdatelist.isempty()){
             updateavailabledays.updtavldays(evtupdatelist);
         }
    }
}