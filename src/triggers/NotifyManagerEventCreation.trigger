trigger NotifyManagerEventCreation on Event (after Update, after Insert) {
    //Project Skywalker: Trigger sends email notification on Event Creation and Event completion when Subscribe field is checked for the related account
    //Check if trigger is enabled
integer counter=0;//Restricting this logic for Japan
for(Event evtJPN : Trigger.new)
    if(evtJPN.Country__c=='Japan'){
    counter++;
}
    if(counter>0){
    return;
}
    
    List<TriggerHandler__c> lstTrigger = TriggerHandler__c.getall().values();
    if(!lstTrigger.isEmpty()){
    if (lstTrigger[0].IsNotifyManagerEventCreation__c == true) {
        Set<Id> ownerIds = new Set<Id>();
        Set<Id> AccIds = New Set<Id>();
        list<String> toAddresses = new list<String>();

        Boolean checkSubscribe = false;
        for(Event evt: Trigger.New){
            ownerIds.add(evt.OwnerId);    //Assigned TO
            AccIds.add(evt.WhatId);    //What ID for Account
        }
        //Get the ownerID and Account ID of the event
        Map<Id, User> userMap = new Map<Id,User>([select Id, Name, Email, ManagerId from User where Id in :ownerIds]);
        Map<id, Account> AccMap = new Map<Id,Account>([select Name, Subscribe__c from Account where Id in :AccIds]);
               
        for(Event evt : Trigger.New)  {
            String Whatid ;
            User TheAssignee = userMap.get(evt.ownerId);
            
            if(AccMap.containsKey(evt.whatid)){
            Whatid = AccMap.get(evt.WhatId).Name;
            checkSubscribe = AccMap.get(evt.WhatId).Subscribe__c;
            }
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            toAddresses.add(evt.OwnerManager__c); 
            mail.setToAddresses(toAddresses);    // Set the TO addresses
           // mail.setTargetObjectId(evt.OwnerID);
            //mail.setSaveAsActivity(false);
            if (Trigger.isInsert) {
               if(evt.OwnerManager__c!=null){
                   if(checkSubscribe == true){
                        if(evt.ActivityStatus__c=='Planned' || evt.ActivityStatus__c=='Deferred'){
                            
                            mail.setSubject('New Call Plan Notification');    // Set the subject
                            String template = 'Dear Sales managers - there is a call plan(s) created by your team members. Please check in SFDC. \n\n';
                            template+= 'Link - {0}\n';
                            List<String> args = new List<String>();
                            args.add('https://'+System.URL.getSalesforceBaseURL().getHost()+'/'+evt.Id);
                            String formattedHtml = String.format(template, args);
                            mail.setPlainTextBody(formattedHtml);
                            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail}); 
                        }
                                   
                   else if(evt.ActivityStatus__c=='Completed' && evt.CompletedCheck__c==true){
                            
                            mail.setSubject('Call Plan Completion notification');    // Set the subject
                            String template = 'Dear Sales managers - there is a call plan(s) completed by your team members. Please check in SFDC. \n\n';
                            template+= 'Link - {0}\n';
                            List<String> args = new List<String>();
                            args.add('https://'+System.URL.getSalesforceBaseURL().getHost()+'/'+evt.Id);
                            String formattedHtml = String.format(template, args);
                            mail.setPlainTextBody(formattedHtml);
                            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});  
                    }
                    
                    else {}
                    }  
                }
            }
            if (Trigger.isUpdate) {
                if(evt.OwnerManager__c!=null){
                  if(checkSubscribe==true){
                    Event oldEvent = Trigger.oldMap.get(evt.ID);
                    if(evt.ActivityStatus__c != oldEvent.ActivityStatus__c){  
                        if(evt.ActivityStatus__c=='Completed' && evt.CompletedCheck__c==true){
                                
                                mail.setSubject('Call Plan Completion notification');    // Set the subject
                                String template = 'Dear Sales managers - there is a call plan(s) completed by your team members. Please check in SFDC. \n\n';
                                template+= 'Link - {0}\n';
                                List<String> args = new List<String>();
                                args.add('https://'+System.URL.getSalesforceBaseURL().getHost()+'/'+evt.Id);
                                String formattedHtml = String.format(template, args);
                                mail.setPlainTextBody(formattedHtml);
                                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});  
                        }
                    
                    else {}
                    }     
                } 
                }
            }
            
             
        }
    }
}
}