trigger OrderLineNumberUpdate on OrderItem (after insert) {
    List<OrderItem> listOrderItem = new List<OrderItem>();
    
    Map<Id, OrderItem> mapOrderItem = new Map<Id, OrderItem>();
    
    listOrderItem = [SELECT ID, Order_Line_Number__c, Order_Line_Number_Text__c ,  Order.OrderLineQty__c FROM OrderItem WHERE Id =:trigger.newMap.keySet()];
    
    for(OrderItem o : listOrderItem){
        mapOrderItem.put(o.id,o);
    }
    
    System.debug('mapOrderItem>>'+mapOrderItem.values());
    try{
        if(mapOrderItem.size() > 0){
            Decimal temp;
            for(Orderitem oi : mapOrderItem.values()){
                if(temp == null){
                    temp = 	oi.Order.OrderLineQty__c + 1;
                }
                else{
                    temp = temp + 1;
                }
                oi.Order_Line_Number__c = temp;
                System.debug('oi.Order_Line_Number__c>>'+oi.Order_Line_Number__c);
                if(oi.Order_Line_Number__c != null){
                    if(oi.Order_Line_Number__c < 10){
                       oi.Order_Line_Number_Text__c = '00'+String.valueOf(oi.Order_Line_Number__c); 
                    }
                    else{
                        oi.Order_Line_Number_Text__c = '0'+String.valueOf(oi.Order_Line_Number__c);
                    }
                }
            }
        }
        update(listOrderItem);
    }catch (Exception e){
        Trigger.new[0].addError('There was a problem in record creation');
    }
}