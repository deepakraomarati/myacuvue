trigger POAObjective on Event (before insert, before update) {
integer counter=0;//Restricting this logic for Japan
for(Event evtJPN : Trigger.new)
    if(evtJPN.Country__c=='Japan'){
    counter++;
}
    if(counter>0){
    return;
}

    for(Event e : Trigger.new){
      Schema.sObjectType sobject_type = Event.getSObjectType(); //grab the sobject that was passed
      Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
      Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
      List<Schema.PicklistEntry> pick_list_values = field_map.get('POA_Objective_List__c').getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
      e.HasPOAObjective1__c = false;
      e.HasPOAObjective2__c = false;
      if(e.Objective1__c <> NULL){
          for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
              if(e.Objective1__c == a.getValue()){
                e.HasPOAObjective1__c = true;
              }
          }
      }
      if(e.Objective2__c <> NULL){
          for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
              if(e.Objective2__c == a.getValue()){
                e.HasPOAObjective2__c = true;
              }
          }
      }
  }
}