trigger PreventEventDeletion on Event (before delete)
{
integer counter=0;//Restricting this logic for Japan
for(Event evtJPN : Trigger.old)
    if(evtJPN.Country__c=='Japan'){
    counter++;
}
    if(counter>0){
    return;
}
    Set<Id> allowedProfileIds = null;
    for (Event event : Trigger.old)           
    {           
        if(event.ActivityStatus__c== 'Completed' || event.CompletedCheck__c==true){
        if (allowedProfileIds == null) {
            allowedProfileIds = new Map<Id, Profile>([SELECT Id FROM Profile WHERE Name in ('System Administrator', 'ASPAC ANZ Country Champion', 'ASPAC HK Country Champion', 'ASPAC KR Country Champion', 'ASPAC SG Country Champion', 'ASPAC IN Country Champion', 'ASPAC CH Country Champion', 'ASPAC ID Country Champion', 'ASPAC MY Country Champion', 'ASPAC TH Country Champion', 'ASPAC TW Country Champion' )]).keySet();
        }
        
        if (!allowedProfileIds.contains(UserInfo.getProfileId()))
        {
                event.addError('You do not have permission to delete this event');
        }
        }
    }
}