trigger SendEmailOnManagerComment on Event (after Update) {
integer counter=0;//Restricting this logic for Japan
for(Event evtJPN : Trigger.new)
    if(evtJPN.Country__c=='Japan'){
    counter++;
}
    if(counter>0){
    return;
}

    List<TriggerHandler__c> lstTrigger = TriggerHandler__c.getall().values();
    if(!lstTrigger.isEmpty()){
    if (lstTrigger[0].IsSendEmailOnManagerComment__c == true) {
        Set<Id> ownerIds = new Set<Id>();
        Set<Id> initiatorIds = new Set<Id>();

        for(Event evt: Trigger.New)
         ownerIds.add(evt.OwnerId);    //Assigned TO
         
        Map<Id, User> userMap = new Map<Id,User>([select Id, Name, Email from User where Id in :ownerIds]);
       
        for(Event evt : Trigger.New)  {
            User TheAssignee = userMap.get(evt.ownerId);
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {TheAssignee.Email};
            mail.setToAddresses(toAddresses);    // Set the TO addresses
           // mail.setTargetObjectId(evt.OwnerID);
            //mail.setSaveAsActivity(false);
            
            Event oldEvent = Trigger.oldMap.get(evt.ID);
            if(evt.ManagersComments__c != oldEvent.ManagersComments__c) 
            {  
                if(toAddresses[0]!=''){
                    mail.setSubject('Feedback from Manager');    // Set the subject
                    String template = 'Dear SFDC users - Pls check your managers feedback/comments. \n\n';
                    template+= 'Link - {0}\n';
                    List<String> args = new List<String>();
                    args.add('https://'+System.URL.getSalesforceBaseURL().getHost()+'/'+evt.Id);
                    String formattedHtml = String.format(template, args);
                    mail.setPlainTextBody(formattedHtml);
                    Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail}); 
                }
            }
        }
    }
}
}