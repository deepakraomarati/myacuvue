trigger SetOwnerId on NPS__c (before insert,before update) {
    List<Id> accountIds = new List<Id>{};

    for(NPS__c c: Trigger.new)
      accountIds.add(c.Account__c);

    Map<Id, Account> userMap = new Map<Id, Account>([select Id, OwnerId from Account where Id IN :accountIds]);
  
    NPS__c[] npss = Trigger.new;
    for ( NPS__c c : npss ) {
        if(c.Account__c!=null){
        Account acc = userMap.get(c.Account__c);
        system.debug('Account ID returned' + acc);
        if(acc.OwnerId!=null){
        c.OwnerId = acc.OwnerId;
        }
       }
    }   
  }