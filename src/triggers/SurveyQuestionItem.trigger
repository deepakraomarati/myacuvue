trigger SurveyQuestionItem on SurveyQuestionItem__c (before insert,before update){
    
    List<Integer> lstsurvey =new List<Integer>();
    List<Integer> lstsurq =new List<Integer>();
    List<SurveyQuestionItem__c> lstsq=new List<SurveyQuestionItem__c>();
    
    Map<Id,Survey__c> MapSurvey =new Map<Id,Survey__c>();
    Map<Id,SurveyQuestion__c> MapSurQuestion = new Map<Id,SurveyQuestion__c>();
    
    for(SurveyQuestionItem__c sq:Trigger.New){
        
        lstsq.add(sq);
        
        if(sq.AWS_Survey__c !=null){
            lstsurvey.add(Integer.ValueOf(sq.AWS_Survey__c));
        }
        if(sq.AWS_SurveyQuestion__c !=null){
            lstsurq.add(Integer.ValueOf(sq.AWS_SurveyQuestion__c));
        }   
    }
    
    System.debug('lstsurvey ###'+lstsurvey);
    System.debug('lstsurq ###'+lstsurq);
    
    
    if(lstsurvey.size()>0 && !lstsurvey.isEmpty()){
        MapSurvey = new Map<Id,Survey__c>([SELECT Id,s_seq__c FROM Survey__c WHERE  s_seq__c IN:lstsurvey]);
    }
    
    System.debug('MAPSURVEY ###'+MapSurvey);
    
    if(lstsurq.size()>0 && !lstsurq.isEmpty()){
        MapSurQuestion = new Map<Id,SurveyQuestion__c>([SELECT Id,q_seq__c FROM SurveyQuestion__c WHERE q_seq__c IN:lstsurq]);
    }
    
    System.debug('MAPSUREYQUESTION ####'+MapSurQuestion);
    
    for(SurveyQuestionItem__c sq:lstsq){
        
        if(MapSurvey.size()>0 && !MapSurvey.isEmpty() && sq.AWS_Survey__c !=null){
            for(Survey__c sv:MapSurvey.Values()){
                if(sv.s_seq__c !=null && sv.s_seq__c == sq.AWS_Survey__c){
                    sq.s_seq__c = sv.Id;
                }
            }
        }
        
        if(MapSurQuestion.size()>0 && !MapSurQuestion.isEmpty() && sq.AWS_SurveyQuestion__c !=null){
            for(SurveyQuestion__c squ:MapSurQuestion.Values()){
                if(squ.q_seq__c!=null && squ.q_seq__c== sq.AWS_SurveyQuestion__c){
                    sq.q_seq__c = squ.Id;
                }
            }
        }
    }
}