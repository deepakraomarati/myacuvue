trigger UpdateAccountFields on event (before insert, before update) {
integer counter=0;//Restricting this logic for Japan
for(Event evtJPN : Trigger.new)
    if(evtJPN.Country__c=='Japan'){
    counter++;
}
    if(counter>0){
    return;
}

List<event> listevents = new List<event>();
Set<Id> listIds = new set<Id>();
list<Event> listevent = new list<Event>();


for(event t : Trigger.new)
{
  if(t.whatid!=null){
  listIds.add(t.whatId);
  listevents.add(t);
  }
}

Map<Id,Account> mapAccount= new Map<Id,Account>([Select id,OutletNumber__c,Channel_Type__c,Cust_Group_No__c,Cust_Group_Desc__c,Sub_Customer_Group__c,Sub_CustGroupNo__c,Strategic_Cust__c,Territory_Key__c,Curry_District__c,District_Name__c,Region_Key__c,Region_Name__c,Territory_Description__c,Name,ShippingCity,ShippingState,ShippingStreet,ShippingStateCode,ShippingPostalCode from Account where id in:listIds]);
//if(acc!=null){
for(event t:listevents)
{
  String objectType = String.valueOf(t.WhatId.getSObjectType());
    system.debug('****' + objectType );
  
  if(t.whatid!=null){
  if(objectType == 'Account'){
  Account acc = mapAccount.get(t.whatid);
  
  t.Account_Number__c = acc.OutletNumber__c;
  t.Channel_Type__c = acc.Channel_Type__c;
   t.Cust_Group_No__c = acc.Cust_Group_No__c;
    t.Cust_Group_Desc__c = acc.Cust_Group_Desc__c;
     t.Sub_Customer_Group__c = acc.Sub_Customer_Group__c;
      t.Sub_CustGroupNo__c = acc.Sub_CustGroupNo__c;
       t.Strategic_Cust__c = acc.Strategic_Cust__c;
        t.Territory_Key__c = acc.Territory_Key__c;
         t.Curry_District__c = acc.Curry_District__c;
          t.District_Name__c = acc.District_Name__c;
           t.Region_Key__c = acc.Region_Key__c;
            t.Region_Name__c = acc.Region_Name__c;
             t.Account_Name__c = acc.Name;
              t.Account_City__c = acc.ShippingCity;
               t.Account_State__c = acc.ShippingState;
                t.Account_Addresss__c = acc.ShippingStreet;
                 t.Account_Province__c = acc.ShippingStateCode;
                  t.Account_ZIP__c = acc.ShippingPostalCode;
                     system.debug('&&&&&inside the loop&&&&');
    }
    // listevent.add(t);
}
//if(!listevent.isempty()){
    
//Insert listevent;
//}
}
}