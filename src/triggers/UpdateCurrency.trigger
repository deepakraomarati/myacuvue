trigger UpdateCurrency on Account (before Update, before Insert) {


list<Account> Accountlist = trigger.new;

for(Account eachacc: Accountlist){
    
    
    if(eachacc.CountryCode__c != null){
    
        if(eachacc.CountryCode__c == 'KOR'){
            eachacc.CurrencyIsoCode = 'KRW';    
        }
        
        if(eachacc.CountryCode__c == 'AUS'){
            eachacc.CurrencyIsoCode = 'AUD';    
        }
        
        if(eachacc.CountryCode__c == 'NZL'){
            eachacc.CurrencyIsoCode = 'NZD';    
        }
        
        if(eachacc.CountryCode__c == 'SGP'){
            eachacc.CurrencyIsoCode = 'SGD';    
        }
        
        if(eachacc.CountryCode__c == 'HKG'){
            eachacc.CurrencyIsoCode = 'HKD';    
        }
    if(eachacc.CountryCode__c == 'IND'){
            eachacc.CurrencyIsoCode = 'INR';    
        }
        if(eachacc.CountryCode__c == 'CHN'){
            eachacc.CurrencyIsoCode = 'CNY';    
        }
        if(eachacc.CountryCode__c == 'MYS'){
            eachacc.CurrencyIsoCode = 'MYR';    
        }
        if(eachacc.CountryCode__c == 'TWN'){
            eachacc.CurrencyIsoCode = 'TWD';    
        }
        if(eachacc.CountryCode__c == 'THA'){
            eachacc.CurrencyIsoCode = 'THB';    
        }
        if(eachacc.CountryCode__c == 'IDN'){
            eachacc.CurrencyIsoCode = 'IDR';    
        }
    }
    
    system.debug('++++country code++++++'  + eachacc.CountryCode__c + '+++++currency++++++++' +  eachacc.CurrencyIsoCode );
    
  
}


}