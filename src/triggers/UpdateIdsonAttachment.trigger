trigger UpdateIdsonAttachment on Attachment (after insert, after delete, after undelete) 
{
  if(Trigger.isInsert)
  {
        MA2_AttachmentIDUpdate.updateAttachmentid(Trigger.new);
  }
    if(Trigger.isDelete || Trigger.isundelete)
    {
        MA2_AttachmentIDUpdate.updateAttachmentid(Trigger.old);
    }
}