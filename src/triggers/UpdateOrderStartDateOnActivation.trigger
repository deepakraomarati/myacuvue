trigger UpdateOrderStartDateOnActivation on Order (before insert, after update) {
    if (trigger.isAfter && trigger.isUpdate){
        if(UpdateOrderStartDate_Helper.runOnce()){
            UpdateOrderStartDate_Helper.UpdateOrderStartDate_Method(trigger.new);
          }
    }
    //28/11/2018 Payal : POP Product: Added for defaulting Order currency to USD, as in LEX Order currency = User Locale currency
    if (trigger.isBefore && trigger.isInsert){
        for (Order o : trigger.new){
            o.CurrencyIsoCode = System.Label.SW_Currency_LEX;
        }
    }
}