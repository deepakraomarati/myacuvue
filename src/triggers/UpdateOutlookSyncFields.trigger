trigger UpdateOutlookSyncFields on event ( before update,before insert) {
integer counter2=0;//Restricting this logic for Japan
for(Event evtJPN : Trigger.new)
    if(evtJPN.Country__c=='Japan'){
    counter2++;
}
    if(counter2>0){
    return;
}

List<event> listevents = new List<event>();
Set<Id> listConIds = new set<Id>();
Set<Id> listAccIds = new set<Id>();
String TempDesc;

for(event t : Trigger.new)
{
  listevents.add(t);
  if(t.whoid!=null){
  listConIds.add(t.whoid);
  }
  if(t.whatid!=null){
  listAccIds.add(t.whatid);
  }
}
Map<Id,Contact> mapContact= new Map<Id,Contact>([Select id,Name from Contact where id in:listConIds]);
Map<Id,Account> mapAccount= new Map<Id,Account>([Select id,Name,OutletNumber__c from Account where id in:listAccIds]);

for(event t:listevents)
{
    
    if(trigger.isinsert){
        if(t.Description!=null && !t.Description.contains('-----')){
            TempDesc=t.Description;
            }
            }
    if(trigger.isupdate){
        if(t.Description!=null && t.Description.contains('-----')){
            integer counter =t.Description.indexof('-----');
            if(counter>0){
                TempDesc=t.Description.substring(0,counter);
                TempDesc=TempDesc.subString(0,TempDesc.length()-1);
                }
            }else if(t.Description!=null){
                TempDesc=t.Description;
            }
        }
    if(t.whatid!=null){
        String objectType = String.valueOf(t.whatid.getSObjectType());
        system.debug('****' + objectType );
        if(objectType == 'Account'){
            Account acc = mapAccount.get(t.whatid);
            if(t.Country__c == 'Australia' || t.Country__c == 'New Zealand'){
                t.Subject = acc.Name;
            }
            if(TempDesc == null){
                TempDesc = '---------------------'+'\nAccount : ' + acc.Name + ' & ' + 'Outlet Number : ' + acc.OutletNumber__c;
                t.Location = 'Account : ' + acc.Name;
            }
            else if(TempDesc != null){
                TempDesc = TempDesc +'\n---------------------' + '\n Account : ' + acc.Name + ' & ' + 'Outlet Number : ' + acc.OutletNumber__c;
                t.Location = 'Account : ' + acc.Name;
            }
            system.debug('&&&&&inside the loop&&&&');
        }
        }
    if(t.whoid!=null){
            Contact cont = mapContact.get(t.whoid);
            
            if(TempDesc == null){
                TempDesc = '---------------------'+'\nContact : ' + cont.Name;
            }else if(TempDesc != null && !TempDesc.contains('-----')){
                TempDesc = TempDesc +'\n---------------------'+'\nContact : ' + cont.Name;
            }else if(TempDesc != null && TempDesc.contains('-----')){
                TempDesc = TempDesc + '\nContact : ' + cont.Name;
            }
            
            if(t.Location != null){
                t.Location = t.Location + ' & ' + ' Contact : ' + cont.Name;
            }else{
                t.Location = ' Contact : ' + cont.Name;
            }
            system.debug('&&&&&inside the loop&&&&');
        }
    if(TempDesc != null && !TempDesc.contains('-----')){
            TempDesc = TempDesc +'\n---------------------'+'\n';
    }else if(TempDesc == null){
            TempDesc = '---------------------';
    }       
    if(t.ActivityStatus__c!=null){
            TempDesc = TempDesc + '\n Activity Status: ' + t.ActivityStatus__c;
    }
    if(t.Objective1__c!=null){
            TempDesc = TempDesc + '\n Objectives: ' + t.Objective1__c;
    }
     if(t.ObjectivesSMART__c!=null){
            TempDesc = TempDesc + '\n Objectives Smart: ' + t.ObjectivesSMART__c;
    }
     if(t.PotentialObstacle__c!=null){
            TempDesc = TempDesc + '\n Potential Obstacle : ' + t.PotentialObstacle__c;
    }
     if(t.Open3W__c!=null){
            TempDesc = TempDesc + '\n Open 3W: ' + t.Open3W__c;
    }
     if(t.Brand_Name__c!=null){
            TempDesc = TempDesc + '\n Brand Name: ' + t.Brand_Name__c;
    }
     if(t.Clarify__c!=null){
            TempDesc = TempDesc + '\n Clarify : ' + t.Clarify__c;
    }
     if(t.Influence__c!=null){
            TempDesc = TempDesc + '\n Influence: ' + t.Influence__c;
    }
     if(t.Close__c!=null){
            TempDesc = TempDesc + '\n Close : ' + t.Close__c;
    } if(t.Notes1__c!=null){
            TempDesc = TempDesc + '\n Notes 1: ' + t.Notes1__c;
    }
    if(t.MainCompetitor__c!=null){
            TempDesc = TempDesc + '\n Main Competitor: ' + t.MainCompetitor__c;
    }
    if(t.CallObjective1__c!=null){
            TempDesc = TempDesc + '\n Call Objective #1: ' + t.CallObjective1__c;
    }
    if(t.CallObjective2__c!=null){
            TempDesc = TempDesc + '\n Call Objective #2: ' + t.CallObjective2__c;
    }
    if(t.Product__c!=null){
            TempDesc = TempDesc + '\n Product : ' + t.Product__c;
    }
    if(t.TargetAudience__c!=null){
            TempDesc = TempDesc + '\n Target Audience : ' + t.TargetAudience__c;
    }
    if(t.CustomerExpectationforthiscall__c!=null){
            TempDesc = TempDesc + '\n Customer Expectation for this call : ' + t.CustomerExpectationforthiscall__c;
    }
    if(t.PossibleObjection__c!=null){
            TempDesc = TempDesc + '\n Possible Objection : ' + t.PossibleObjection__c;
    }
    if(t.CallObjectiveAchieved__c!=null){
            TempDesc = TempDesc + '\n Call Objective Achieved: ' + t.CallObjectiveAchieved__c;
    }
    if(t.LastCallOutcome__c!=null){
            TempDesc = TempDesc + '\n Last Call Outcome: ' + t.LastCallOutcome__c;
    }
    if(t.CallOutcomeCallTargetComments__c!=null){
            TempDesc = TempDesc + '\n Call Outcome/Call Target/Comments: ' + t.CallOutcomeCallTargetComments__c;
    }
    if(t.id!=null){
        String link = 'https://jjskywalker--staging.cs31.my.salesforce.com/'+t.Id;
        TempDesc = TempDesc + '\n Event Link: ' + link;
    }
    t.Description=TempDesc;
    }
}