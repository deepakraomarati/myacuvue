trigger UpdateParentAndOwner on Account (before insert, before update) {

List<TriggerHandler__c> lstTrigger = TriggerHandler__c.getall().values();
 if(!lstTrigger.isEmpty())
    {
if (lstTrigger[0].IsUpdateParentAndOwnerActive__c == true) {
if(Trigger.isinsert || Trigger.isUpdate  ){
    
    list<User> userlist = new list<User>([select id,Unique_User_Id__c from User where Unique_User_Id__c != null]);
    list<Account> acclist = new list<Account>(trigger.new);
    list<Account> acclist2 = new list<Account>();  
    List<String> checkstring1= new list<String>();
    List<String> salesrep = new list<String>();
    set<Id> updateacc = new set<Id>();
    set<Id> updateacc2 = new set<Id>();
    set<String> userlist12 = new set<String>();
    
        
    for(Account a:acclist){
            if(a.SalesRep__c!= null){
                salesrep.add(a.SalesRep__c);
               // system.debug('salesrep list ++' + salesrep );
                }
            
           }
    
        /*for(User u:[select Unique_User_Id__c from User]){
               userlist12.add(u.Unique_User_Id__c);
              // system.debug('size of the users' + userlist12.size());
           }*/
          for(User u: userlist){
               userlist12.add(u.Unique_User_Id__c);
              // system.debug('size of the users' + userlist12.size());
           } 
       list<User> userlist1 = [select id,Unique_User_Id__c from User where Unique_User_Id__c != null AND Unique_User_Id__c IN:salesrep];    
     //  system.debug('Userlist1 list ++' + Userlist1);
       
                
        for(Account a:acclist){            
            for(User u:userlist1){
                if(u.Unique_User_Id__c == a.SalesRep__c){
                    a.OwnerId = u.Id;
                    updateacc.add(a.Id);
                   } 
                   //system.debug('Userlist1 list ++' + updateacc + a.OwnerId + u.Id);                           
         }
         }
         
         /*for(Account a: acclist){
                 if(!userlist12.contains(a.salesRep__c)){       
                     system.debug('check');
                     a.Adderror('Account cannot be saved when there is no matching User Id for the Sales Rep Alias');   
                 }    
            system.debug('salesrep' +salesrep ); 
         }*/
         
         for(Account newacc1: acclist){
           if(newacc1.CountryCode__c!=null && newacc1.AccountNumber!=null && newacc1.OutletNumber__c!=null){ 
           newacc1.External_Account_Id__c = newacc1.CountryCode__c+newacc1.AccountNumber+newacc1.OutletNumber__c;           
           if(newacc1.AccountNumber != newacc1.OutletNumber__c){
               string check = newacc1.CountryCode__c+newacc1.AccountNumber+newacc1.AccountNumber;
               checkstring1.add(check);
               acclist2.add(newacc1);               
               }
           }
         }
         
         //For parent account mapping
          List<String> listAccountNumbers = new List<String>();
      
           for(Account beforeUpdateAcc : Trigger.new)
            {
              listAccountNumbers.add(beforeUpdateAcc.AccountNumber);
            }
    
         
         List<Account> Parentacclist = [select id,AccountNumber,OutletNumber__c,External_Account_Id__c from Account where OutletNumber__c IN:listAccountNumbers];
         Map<string,Account> MapAccounts = new Map<String,Account>();
         for(Account a:Parentacclist)
         {
             MapAccounts.put(a.OutletNumber__c,a);
         }
        for(Account updateAccount : Trigger.new)
           {
           
         //  system.debug(' for loop'+updateAccount);
           if(updateAccount.AccountNumber != updateAccount.OutletNumber__c){
            // if(updateAccount.ParentId==null)
             //{
             
             Account parentAccount = MapAccounts.get(updateAccount.AccountNumber);
             if(parentAccount!=null){
        
                if(updateAccount.AccountNumber==parentAccount.OutletNumber__c){     
                   updateAccount.ParentId = parentAccount.Id;
                   
             //      system.debug('Changing of the Account Parent id' + updateAccount.ParentId + ' / ' + parentAccount.Id + ' / ' + updateAccount.OutletNumber__c); 
                }
                else{
                updateAccount.ParentId = null;
                }
             }
           //}
           }
           else{
           updateAccount.ParentId = null;
           }
           
          // system.debug(' for loop after update'+updateAccount);
           }
        
      
    
        }
        }     
    }
    }