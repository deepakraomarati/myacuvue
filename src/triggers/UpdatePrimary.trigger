trigger UpdatePrimary on Contact(after Insert, after Update,before Update,Before Delete) {

List<ID> AccountIds = New List<ID>();

String contactname;
List<Account> accounts = new List<Account>();
list<Account> UpdateAccounts = new List<Account>();
list <contact> listcon= new list<contact>();
List<Account> changeacc = new list<Account>();
List<Account> AccList = new list<Account>();
Set<Id> accID = new Set<Id>();
integer count=0;
Boolean oldisPrimary;
Boolean newisPrimary;
list<account> listacc = new list<account>();
Public String StrAccID;
String space = '  ';
String type;
List<TriggerHandler__c> lstTrigger = TriggerHandler__c.getall().values();
 if(!lstTrigger.isEmpty()){

if (lstTrigger[0].IsUpdatePrimaryActive__c == true) { 
Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Contact.getRecordTypeInfosById();
if (trigger.isInsert || trigger.isUpdate){
for (Contact con : Trigger.new){
type = rtMap.get(con.RecordTypeId).getName();
}
}
  
if (type == 'ECP' && type <> NULL){
 if(Trigger.IsAfter) { 
 StrAccID = Trigger.new[0].accountid;
         if(trigger.isInsert ||trigger.isUpdate){
           
            
    
                for(contact c:Trigger.new ){                            
                    accid.add(c.AccountId);
             
                 }

 listcon = [SELECT AccountId,isPrimary__c,firstname,lastname FROM Contact WHERE isPrimary__c = true AND AccountId = :accid];
    
    for(contact c : listcon){
        if(c.isPrimary__c){    
             count = count+1;                        
        }       
     }
     
     if(count>1)
     trigger.new[0].adderror('Only one Primary Contact is allowed for an Account.');


    for (Contact t:System.Trigger.new){
        if(StrAccID != null){
            AccountIds.add(t.accountid);

            if(t.firstname == null)
                contactname = t.lastname;
            else
                contactname = t.firstname+space+t.lastname;
            
        }
    }


Map<ID,Account> accsToUpdate = new Map<ID,Account>();
Map<ID,Account> m = new Map<ID,Account>([SELECT Id, Primary_Customer_Contact__c FROM Account WHERE id in :AccountIds AND SalesRep__c!=NULL Limit 1]);

    for(Contact conupd : trigger.new){
        if(m.containsKey(conupd.AccountId)){
            Account tmpAcc = m.get(conupd.AccountId);  
           
            if ((trigger.isInsert||trigger.isUpdate) && conupd.AccountId!=null && conupd.isPrimary__c == TRUE){
                tmpAcc.Primary_Customer_Contact__c = contactname;                
                accsToUpdate.put(tmpAcc.id,tmpAcc);
                
            }  
        Update accsToUpdate.Values();
        }
    }
  
  }
}
    
If(trigger.isBefore){
if(trigger.IsUpdate){
   Map<String,Contact> OldMap = new Map<String,Contact>([select id, accountid from Contact where Id IN: Trigger.Old]);
   system.debug('Old Map' + OldMap);
   List<Id> AccOld = new List<Id>();
   Map<String,String> UpdateAcc = new Map<String,String>();
   
  for(contact c:Trigger.new ){
        
        accid.add(c.AccountId);
        Contact oldcontact = Trigger.oldMap.get(c.Id);  
        oldisPrimary = oldcontact.isPrimary__c;
          newisPrimary = c.isPrimary__c;
          Account changeId = oldcontact.account;
   
     
           if(string.valueof(c.AccountId) != string.valueof(OldMap.get(c.Id).Accountid)){
               AccOld.add(OldMap.get(c.Id).Accountid); 
               system.debug('Check Account'+ c.AccountId);
               UpdateAcc.put(c.Id, c.AccountId);                   
           }
           

     }
     
    AccList = [select Id,Primary_Customer_Contact__c from Account where Id IN :AccOld];
    for(Account item: AccList){
    item.Primary_Customer_Contact__c = null;    
    
    }
    update AccList;
     update changeacc;
     
    //listcon = [SELECT id,AccountId,isPrimary__c,firstname,lastname FROM Contact WHERE AccountId = :accid];
    listacc = [select Id,Primary_Customer_Contact__c from Account where SalesRep__c!=NULL AND Id IN :accId];
    
    //List<Account> acctoupdate = new List<Account>();
      map<Id, Account> AccountMapupdate = new map<id, Account>();
     
     for(Account a:listacc){
     for(contact c : listcon){
     a.Primary_Customer_Contact__c = '';
       if(((oldisPrimary!=newisPrimary)&& newisPrimary == FALSE)  ) {    
             a.Primary_Customer_Contact__c = '';  
             system.debug('$$$$values$$$' + a.Primary_Customer_Contact__c + c.lastname + c.isPrimary__c);
                          
        }
            
         //acctoupdate.add(a);
         AccountMapupdate.put(a.Id, a);
         
         
     }
     
          
  } 
  
  if(!AccountMapupdate.isEmpty())
    {
        //update acctoupdate;
        update AccountMapupdate.values(); 
    }

 
 }
 If(trigger.isBefore){
 if(trigger.isDelete){
 
     for(contact c:Trigger.old ){ 
         if(c.isPrimary__c == TRUE)       
            accid.add(c.AccountId);
       } 
     
     list<Account> delacc = [select Id, Primary_Customer_Contact__c from Account where Id IN :accId];
     List<Account> ac= new List<Account>();
     
     
     for(Account a:delacc){
         a.Primary_Customer_Contact__c = '';
         ac.add(a);
     }
     
    if(!ac.isEmpty())
    {
        update ac;
    }

 
 }
  
}    

}
}
}

}
}