trigger UpdateTerritory on Account (before update)   
{            
    Set<String> territoryIdList = new Set<string>();
    for(Account acc : Trigger.New){  
        territoryIdList.add(acc.Territory1__c);
    }
    if(territoryIdList.size()>0){
        List<LUT_Territory__c> terList = [select Name,id from LUT_Territory__c where Name =: territoryIdList];
        if(terList.size()>0){
            Map<String,String> territoryNameIdMap = new Map<String,String>();
            for(LUT_Territory__c eachTer : terList){
                territoryNameIdMap.put(eachTer.Name,eachTer.Id);
            }
            for(Account acc : Trigger.New){  
                if(acc.Territory1__c!=null){
                    acc.Territory__c = territoryNameIdMap.get(acc.Territory1__c);
                }
               
            }
        }
    }
}