trigger ValidateContact on Contact(before update){
List<TriggerHandler__c> lstTrigger = TriggerHandler__c.getall().values();
if(!lstTrigger.isEmpty()){
if (lstTrigger[0].ValidateContact__c == true) {
Contact[] oContact = Trigger.new;
 
map<id, contact> overduecontact = new map<id, contact>();
List<Id> listIds = new List<id>();
 
for(contact c:oContact){
if(c.inactive__c)
{
overduecontact.put(c.id, c);
listIds.add(c.id);
}
}

 
list<task> overduetasks = new list<task>([select id,ActivityDate,whoid from task where whoid In: listIds and ActivityDate >= today ]);
list<event> overdueevents = new list<event>([select id,ActivityDate,whoid from Event where whoid In: listIds and ActivityDate >= today ]);
 
for(task t :overduetasks){
if(overduecontact.containskey(t.whoid))
overduecontact.get(t.whoid).adderror('Cannot inactivate the contact due to open activity associated to this contact');
}
 
for(Event e :overdueevents){
if(overduecontact.containskey(e.whoid))
overduecontact.get(e.whoid).adderror('Cannot inactivate the contact due to open activity associated to this contact');
}
 
}
}
}