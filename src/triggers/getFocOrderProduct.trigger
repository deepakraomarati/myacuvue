trigger getFocOrderProduct on FocOrderProduct__c (before insert,before update) {

    if(Trigger.isInsert || Trigger.isUpdate)
    {
        List<FocOrderProduct__c > lstfocodrdObj = new List<FocOrderProduct__c >();
        List<String> upclist= new List<String>();
        List<product2> prodObject=new List<product2>();
        Map<String,Id> MapProduct =new Map<String,Id>();
        
   
        for(FocOrderProduct__c fop:Trigger.New)
        {
            lstfocodrdObj.add(fop);
            if(fop.UPC__c!= null)
            {
                upclist.add(fop.UPC__c);
            }
        }
        System.debug('upclist::'+upclist);
        
        if(upclist.size()>0 && !upclist.isEmpty())
        {
            prodObject=[select id,UPC_Code__c from product2 where UPC_Code__c IN :upclist LIMIT 50000];
            System.debug('List of products::'+prodObject);
            
            for(Product2 prod:prodObject)
            {
                MapProduct.put(prod.UPC_Code__c,prod.Id);       
            }
            System.debug('MapProduct ###'+MapProduct);
        }
            
        for(FocOrderProduct__c foc:lstfocodrdObj)
        {
             if(MapProduct.size()>0 && !MapProduct.isEmpty()){
                   if(MapProduct.containskey(foc.UPC__c)){
                        Id ProductId = MapProduct.get(foc.UPC__c);
                        foc.ProductId__c = ProductId;
                    }
             }
        }
    }
}