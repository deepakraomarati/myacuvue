trigger getProductPrice on ProductPrice__c (before insert,before update) {

if(Trigger.isBefore)
 {

List<String> upc_id= new List<String>();
List<String> accId = new List<String>();

Map<String,Id> MapAccount =new Map<String,Id>();
Map<String,Id> MapProduct =new Map<String,Id>();


List<account> lstacc =new List<account>() ;
List<product2> lstupc= new List<product2>() ;
List<ProductPrice__c> lstprice= new List<ProductPrice__c>() ;

for(ProductPrice__c pobj:Trigger.New)
    {
        lstprice.add(pobj);
        
        if(pobj.Aws_ProductId__c !=null){
            upc_id.add(pobj.Aws_ProductId__c);
        }
        if(pobj.Aws_AccountId__c !=null){
            accId.add(pobj.Aws_AccountId__c);
        }
     }

   if(accId .size()>0 && !accId .isEmpty())
     {
        lstacc=[select id,Aws_AccountId__c from account where Aws_AccountId__c IN:accId];
     } 
     System.debug('LSTACC ###'+lstacc);
     
     if(upc_id.size()>0 && !upc_id.isEmpty())
     {
         lstupc=[select id,UPC_Code__c from product2 where UPC_Code__c IN:upc_id];
     }
     System.debug('LSTCON ###'+lstupc);
     
 for(Account acc:lstacc){
        MapAccount.put(acc.Aws_AccountId__c,acc.Id);
     }
     
     for(product2 prod:lstupc){
        MapProduct.put(prod.UPC_Code__c,prod.Id);
     }
     System.debug('MapContact ###'+MapProduct);

 for(ProductPrice__c ppc:lstprice)
      {
        if(MapAccount.size()>0 && !MapAccount.isEmpty()){
           for(Account acc:lstacc){
               if(MapAccount.containskey(ppc.Aws_AccountId__c)){
                  Id AccountId = MapAccount.get(ppc.Aws_AccountId__c);
                  ppc.AccountID__c = AccountId;
               }
            }
         }
         if(MapProduct.size()>0 && !MapProduct.isEmpty()){
            for(product2 produ:lstupc){
                if(MapProduct.containskey(ppc.Aws_ProductId__c)){
                    Id upc_id1 = MapProduct.get(ppc.Aws_ProductId__c);
                        ppc.ProductId__c = upc_id1;
                    }
                }
         }
         
         
}

}

}