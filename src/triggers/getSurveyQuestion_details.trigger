trigger getSurveyQuestion_details on SurveyQuestion__c (before insert, before update) {
    if(Trigger.isInsert || Trigger.isUpdate)
    {
        List<SurveyQuestion__c > squestionlst = new list<SurveyQuestion__c>();
        List<Integer> surveyid = new List<Integer>();
        Map<Id,Survey__c> MapServey =new Map<Id,Survey__c>();
        
        for(SurveyQuestion__c sq:Trigger.New){
            squestionlst .add(sq);    
            
            if(sq.Aws_s_seq__c !=null){
                surveyid.add(Integer.ValueOf(sq.Aws_s_seq__c));
            }
        }
        System.debug('surveyid###'+surveyid);
        
        if(surveyid.size() >0 && !surveyid.isEmpty()){
            MapServey = new Map<Id,Survey__c>([SELECT id,s_seq__c FROM Survey__c WHERE s_seq__c IN: surveyid LIMIT 50000]);
            System.debug('MapServey###'+MapServey);
        }
         
        for(SurveyQuestion__c sqq:squestionlst){
            if(MapServey.size()>0 && !MapServey.isEmpty()){
                for(Survey__c sur:MapServey.Values()){
                    if(sur.s_seq__c !=null && sur.s_seq__c == sqq.Aws_s_seq__c){
                        sqq.s_seq__c = sur.Id;
                    }
                }
            }
        }
    }
}