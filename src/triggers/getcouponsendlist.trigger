trigger getcouponsendlist on CouponSendList__c (before insert, before update) {

if(Trigger.isBefore)
 {
 
  List<Integer> lstCoupId =new List<Integer>();
  List<CouponSendList__c > lstcc= new List<CouponSendList__c >();
    Map<Id,Coupon__c> MapCoupen =new  Map<Id,Coupon__c>();
 
   for(CouponSendList__c  cc:Trigger.New)
  {
        
            lstcc.add(cc);
         
         if(cc.Aws_CouponId__c !=null){
                lstCoupId.add(Integer.ValueOf(cc.Aws_CouponId__c));
            }       
      
   }
 
    if(lstCoupId.size() >0 && !lstCoupId.isEmpty()){
           MapCoupen = new Map<Id,Coupon__c>([SELECT Id,DB_ID__c FROM Coupon__c WHERE DB_ID__c IN:lstCoupId LIMIT 50000]);
           System.debug('MapCoupen ####'+lstCoupId);
       }
  
   for(CouponSendList__c cc:lstcc){
  
     if(MapCoupen.size()>0 && !MapCoupen.isEmpty()&& cc.Aws_CouponId__c !=null)
     {
                for(Coupon__c cp:MapCoupen.Values())
                {
                    if(cp.DB_ID__c !=null && cp.DB_ID__c == Integer.ValueOf(cc.Aws_CouponId__c))
                    {
                        cc.CouponId__c = cp.Id;
                    }
                }
      }

}

 }

}