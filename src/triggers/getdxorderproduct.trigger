trigger getdxorderproduct on DxOrderProduct__c (before insert,before update) {

if(Trigger.isBefore)
 {
     
    List<string> prodid= new List<string>();
    Map<Id,Product2> MapProduct =new Map<Id,Product2>();
       
    List<DxOrderProduct__c > lsttrnsobj= new List<DxOrderProduct__c >();
    
     for(DxOrderProduct__c tobj:Trigger.New)
    {
      
      lsttrnsobj.add(tobj);
      
      if(tobj.UPC__c!=null){
            prodid.add(tobj.UPC__c);
        }
    }
   
   if(prodid.size()>0 && !prodid.isEmpty())
     {
       
         MapProduct = new Map<Id,Product2>([SELECT id,UPC_Code__c FROM Product2 WHERE UPC_Code__c IN:prodid Limit 50000]);
     } 
     
          
     for(DxOrderProduct__c DOP :lsttrnsobj)
     {
        if(MapProduct.size()>0 && !MapProduct.isEmpty() && dop.UPC__c!=null){
                for(Product2 pr:MapProduct.Values()){
                    if(pr.UPC_Code__c !=null && pr.UPC_Code__c == dop.UPC__c){
                        dop.Product__c = pr.Id;
                    }
                }
            }
     
     }
 
 }


}