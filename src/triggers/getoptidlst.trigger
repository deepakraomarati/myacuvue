trigger getoptidlst on OptIdList__c (before insert, before update) {
if(Trigger.isBefore)
 {
List<integer> optbId = new List<integer>();
List<String> accId = new List<String>();

Map<String,Id> MapAccount =new Map<String,Id>();
Map<String,Id> MapOptb =new Map<String,Id>();

List<account> lstacc =new List<account>() ;
List<OpticianBoard__c> lstoptbrd = new List<OpticianBoard__c>() ;
List<OptIdList__c> lstoptid= new List<OptIdList__c>();

 for(OptIdList__c oobj:Trigger.New)
    {
        lstoptid.add(oobj);
        
        if(oobj.Aws_OpticianBoardId__c !=null){
            optbId.add(integer.ValueOf(oobj.Aws_OpticianBoardId__c));
        }
        if(oobj.Aws_AccountId__c !=null){
            accId.add(oobj.Aws_AccountId__c);
        }
        
}

if(accId .size()>0 && !accId .isEmpty())
     {
        lstacc=[select id,Aws_AccountId__c from account where Aws_AccountId__c IN:accId];
     } 
     System.debug('LSTACC ###'+lstacc);
     
     if(optbId.size()>0 && !optbId.isEmpty())
     {
         lstoptbrd =[select id, DB_ID__c from OpticianBoard__c where    DB_ID__c IN:optbId];
     }
     System.debug('LSTCON ###'+lstoptbrd );
 
 
    for(Account acc:lstacc){
        MapAccount.put(acc.Aws_AccountId__c,acc.Id);
     }
     
     for(OpticianBoard__c ops:lstoptbrd ){
        MapOptb.put(String.ValueOf(ops.DB_ID__c),ops.Id);
     }
     System.debug('MapContact ###'+MapOptb);
 
 for(OptIdList__c opss:lstoptid)
      {
        if(MapAccount.size()>0 && !MapAccount.isEmpty()){
           for(Account acc:lstacc){
               if(MapAccount.containskey(opss.Aws_AccountId__c)){
                  Id AccountId = MapAccount.get(opss.Aws_AccountId__c);
                  opss.AccountID__c = AccountId;
               }
            }
         }
         if(MapOptb.size()>0 && !MapOptb.isEmpty()){
            for(OpticianBoard__c opstd:lstoptbrd ){
                if(MapOptb.containskey(opss.Aws_OpticianBoardId__c)){
                    Id opitboardid= MapOptb.get(opss.Aws_OpticianBoardId__c);
                        opss.OpticianBoardId__c = opitboardid;
                    }
                }
         }
 
 }
 
 }
 
 }