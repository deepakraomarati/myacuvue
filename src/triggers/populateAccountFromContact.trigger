trigger populateAccountFromContact on Event (before insert) {

List<Id> Contactids = new List<id>();
for(Event e : Trigger.new)
{
    Contactids.add(e.whoid);
}

Map<Id,Contact> mapContacts = new Map<Id,Contact>([Select id,accountid from contact where id in:Contactids]);

for(Event updateEvent : Trigger.new)
{
  if(updateEvent.whoid!=null && updateEvent.whatid==null)
  {
    Contact c = mapContacts.get(updateEvent.whoid);
    updateEvent.whatid = c.accountid;
  }
}

}