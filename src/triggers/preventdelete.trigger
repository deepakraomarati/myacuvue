trigger preventdelete on Contact (after update, before delete) 
{   
    Id profileId = userinfo.getProfileId();
    String profileName = [Select Name from Profile where Id=:profileId].Name;
    
    Id devRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Dr').getRecordTypeId();
    
    if(profileName=='ASPAC JPN PA')
    {
        if(Trigger.IsBefore) 
        {
            for(Contact con : Trigger.old) 
            {
                if(con.RecordTypeId != devRecordTypeId )
                {
                    con.adderror('You do not have the permission to Delete the Contact');
                }
            }
        }
        if(Trigger.isUpdate) 
        {
            for(Contact cont : Trigger.new) 
            {
                if(cont.RecordTypeId != devRecordTypeId )
                {
                    cont.adderror('You do not have the permission to Edit the Contact');
                }
            }
        }        
    }
}