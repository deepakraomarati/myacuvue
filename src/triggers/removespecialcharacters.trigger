trigger removespecialcharacters on Event (before insert, before update) {
    integer counter=0;//Restricting this logic for Japan
    RecordType rt = [Select id from RecordType where sobjecttype='Event' and Name='Telesupport Call JPN'];
    for(Event record: Trigger.new) {
        if(record.RecordtypeId != rt.id){
        if(record.CallOutcomeCallTargetComments__c != null && record.CallOutcomeCallTargetComments__c != '') {
            record.CallOutcomeCallTargetComments__c = record.CallOutcomeCallTargetComments__c.replaceAll( '\r\n',' ');
        }}
    }
}