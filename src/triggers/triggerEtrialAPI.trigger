trigger triggerEtrialAPI on Etrial__c(after insert)   
{            
   Map<String,ETRIAL_Trigger_Send_setting__c> eTrialMap = ETRIAL_Trigger_Send_setting__c.getAll();
   if(Trigger.isAfter)
   {
     List<Id> lsteveId=new List<Id>();
     List<Id> lstEmailId = new List<Id>();
     List<Id> lstSMSId =new List<Id>();
     
     for(Etrial__c et:Trigger.New)
     {
        if(et.etrial_email__c !=null && et.etrial_email__c !='' && eTrialMap.containsKey(et.Country__c))
        {
                
                lstEmailId.add(et.Id);
                lsteveId.add(et.Id); 
        }
        
        if(et.etrial_phone__c !=null && et.etrial_phone__c !='' && eTrialMap.containsKey(et.Country__c))
        {
            lstSMSId.add(et.Id);
        }   
     }
     
     System.debug('lsteveId ###:'+lsteveId);
     System.debug('lstEmailId ###:'+lstEmailId);
     System.debug('lstSMSId ###:'+lstSMSId);
     
     if(lsteveId.size()>0 && !lsteveId.isEmpty()){
        ET_CallWebservice.callDataEventAdmin(lsteveId);
     }
     
     if(lstEmailId.size()>0 && !lstEmailId.isEmpty()){
        ET_CallWebservice.callEmailAdmin(lstEmailId);
     }
     
     if(lstSMSId.size()>0 && !lstSMSId.isEmpty()){
        ET_CallWebservice.callSMSAdmin(lstSMSId);
     }
     
   }
}