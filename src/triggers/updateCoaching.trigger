trigger updateCoaching on Event (after Update) {
    List<Event> getEvents = new List<Event>();
    for(Event eve : Trigger.new){
        getEvents.add(eve);
    }
    List<Coaching__c> coachings = [Select id,Start_Date__c,End_Date__c,Coaching_Status__c  from coaching__c where id =: getEvents[0].whatId Limit 1];
    if(Trigger.isUpdate && !coachings.isEmpty())
    {
        if(CheckRecursive_Coaching.runOnce())
        {
            for(Event eachEvent : getEvents){
                for(COACHING__c coach : coachings){
                    if(eachEvent.whatId==coach.id){
                        if(coach.Start_Date__c!=eachEvent.StartDateTime || coach.End_Date__c!=eachEvent.EndDateTime){
                            coach.Start_Date__c=eachEvent.StartDateTime;
                            coach.End_Date__c=eachEvent.EndDateTime;
                        }
                        system.debug('eachEvent.ActivityStatus__c----'+eachEvent.ActivityStatus__c);
                        if(eachEvent.ActivityStatus__c == 'Planned'){
                            coach.Coaching_Status__c = 'Open';
                        }
                        if(eachEvent.ActivityStatus__c == 'Deferred'){
                            coach.Coaching_Status__c = 'Reviewed';
                        }
                        if(eachEvent.ActivityStatus__c ==  'Completed')
                        {
                            coach.Coaching_Status__c = 'Completed';
                        }
                        system.debug('coach.Coaching_Status__c---'+coach.Coaching_Status__c);
                    }
                }
            }
            try{
                ValidationRule__c myCS = ValidationRule__c.getOrgDefaults();
                myCS.CoachingStatus__c = TRUE;
                update myCS;
                if(!coachings.isEmpty())
                upsert coachings;
            }
            catch(Exception e){
            }
            finally{
                ValidationRule__c newCS = ValidationRule__c.getOrgDefaults();
                newCS.CoachingStatus__c = FALSE;
                upsert newCS;
            }
        }
    }
}