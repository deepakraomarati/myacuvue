trigger updateLastCall on Event (before insert,before update) {
integer counter=0;//Restricting this logic for Japan
for(Event evtJPN : Trigger.new)
    if(evtJPN.Country__c=='Japan'){
    counter++;
}
    if(counter>0){
    return;
}    
    List<Event> events = new List<Event>();
    List<Id> acc = new List<Id>();
    for(Event eve : Trigger.new){
        events.add(eve);
        if(eve.whatid!=null){
            acc.add(eve.whatId);
        }
    }
    List<Event> prevEvents = [select id,CallOutcomeCallTargetComments__c from event where whatId in:acc AND ActivityStatus__c='Completed' order by StartDateTime desc Limit 1];
    if(Trigger.isInsert){
        for(Event obj : events){
            if(!prevEvents.isEmpty() ){
                obj.LastCallOutcome__c = prevEvents[0].CallOutcomeCallTargetComments__c;
            }  
        }    
    }
    if(Trigger.isUpdate){
        for(Event obj : events){
            if(!prevEvents.isEmpty() && prevEvents[0].id!=obj.id ){
                obj.LastCallOutcome__c = prevEvents[0].CallOutcomeCallTargetComments__c;
            }       
        }    
    }
}