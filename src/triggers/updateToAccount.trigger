trigger updateToAccount on Customer_Segmentation_Matrix__c (After Insert,After Update) {
If (Trigger.isInsert || Trigger.isUpdate){
Map<ID,Account> UpdateFields = new Map<ID,Account>();
List<Id> listIds= new List<Id>();

for(Customer_Segmentation_Matrix__c childObj:Trigger.new){
    listIds.add(childObj.AccountName__c);
}

UpdateFields = new Map<Id,Account>([SELECT id,Name,How_Many_CL_Fittings_done_per_Month__c FROM Account WHERE ID IN:listIds]);
for(Customer_Segmentation_Matrix__c csm:Trigger.new){

    Account myParentAcc = UpdateFields.get(csm.AccountName__c);
    
    myParentAcc.How_Many_CL_Fittings_done_per_Month__c=    csm.CL_fitting_are_you_doing_per_month__c;
    myParentAcc.out_of_this_what_would_be_new_fits__c    = csm.Out_of_this_what_would_be_new_fits__c;
    myParentAcc.Overall_Metrics__c    =                 csm.Overall_Metrics__c;
    myParentAcc.Upgrade_Hydrogel_wearers_to_SiHy_CL__c    = csm.Up_grade_systematicly_Hydrogel_Wearers_i__c;
    myParentAcc.CSM_What_of_total_fits_Beauty__c    = csm.What_of_total_fits_Beauty__c;
    myParentAcc.CSM_What_of_total_fits_Daily__c    = csm.What_of_total_fits_Daily__c;
    myParentAcc.What_of_total_fits_Multifocal__c    = csm.What_of_total_fits_Multifocal__c;
    myParentAcc.CSM_What_of_total_fits_Reusable__c    = csm.What_of_total_fits_Reusable__c;
    myParentAcc.What_of_total_fits_Toric__c    = csm.of_total_fits_Toric__c;
    myParentAcc.CSM_What_is_Acuvue_Market_Value_share__c    = csm.Acuvue_market_share_in_the_account__c;
    myParentAcc.Out_of_your_New_fits_what_is_Acuvue__c    = csm.New_Acuvue_fits_per_week__c;
    myParentAcc.CSM_New_Wearers_of_Beauty__c  = String.valueof(csm.New_Wearers_of_Beauty__c);
    myParentAcc.CSM_New_Wearers_of_Daily__c  = String.valueof(csm.New_Wearers_of_Daily__c);
    myParentAcc.CSM_New_wearers_of_Multifocal__c  = String.valueof(csm.New_wearers_of_Multifocal__c);
    myParentAcc.CSM_New_wearers_of_Reusable__c = String.valueof(csm.New_wearers_of_Reusable__c);
    myParentAcc.CSM_New_wearers_of_Toric__c  = String.valueof(csm.New_Wearers_of_Toric__c);
    myParentAcc.CSM_Ranking_for_Beauty__c  = String.valueof(csm.Ranking_for_Beauty__c);
    myParentAcc.CSM_Ranking_for_Daily__c  = String.valueof(csm.Ranking_for_Daily__c);
    myParentAcc.CSM_Ranking_for_Multifocal__c  = String.valueof(csm.Ranking_for_Multifocal__c);
    myParentAcc.CSM_Rankings_for_Reusable__c = String.valueof(csm.Ranking_for_Reusable__c);
    myParentAcc.CSM_Ranking_for_Toric__c  = String.valueof(csm.Ranking_for_Toric__c);
    myParentAcc.CSM_Indication_of_Beauty__c  = String.valueof(csm.Indication_of_Beauty__c);
    myParentAcc.CSM_Indication_of_Daily__c  = String.valueof(csm.Indication_of_Daily__c);
    myParentAcc.CSM_Indication_of_Multifocal__c  = String.valueof(csm.Indication_of_Multifocal__c);
    myParentAcc.CSM_Indication_of_Reusable__c = String.valueof(csm.Indication_of_Reusable__c);
    myParentAcc.CSM_Indication_of_Toric__c  = String.valueof(csm.Indication_of_Toric__c);
    myParentAcc.CSM_What_is_exact_number_of_CL_fittings__c= String.valueof(csm.What_is_the_exact_number__c);
  //  myParentAcc.CSM_Customer_segmentation_Master_Type__c = String.valueof(csm.Customer_Segmentation_Master__c);
    
}
  update UpdateFields.values();
}
}