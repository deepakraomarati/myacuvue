<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_Map</fullName>
        <description>Account Mapping with Outlet Number</description>
        <field>MA2_AccountId__c</field>
        <formula>OutletNumber__c</formula>
        <name>Account Map</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Billing_City</fullName>
        <field>BillingCity</field>
        <name>Billing City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Billing_Country</fullName>
        <field>BillingCountry</field>
        <name>Billing Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Billing_Zip_Code</fullName>
        <field>BillingPostalCode</field>
        <name>Billing Zip Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Billing_street</fullName>
        <field>BillingStreet</field>
        <name>Billing street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Group</fullName>
        <field>JJ_JPN_Rank__c</field>
        <literalValue>A1</literalValue>
        <name>Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>PreviousValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_Phone_Update</fullName>
        <field>JJ_JPN_Phone_Update__c</field>
        <formula>SUBSTITUTE( SUBSTITUTE( SUBSTITUTE( SUBSTITUTE( SUBSTITUTE( Phone, &quot;-&quot;,&quot;&quot;), &quot;+&quot;,&quot;&quot;), &quot;(&quot;,&quot;&quot;), &quot;)&quot;,&quot;&quot;), &quot; &quot;,&quot;&quot;)</formula>
        <name>JJ_JPN Phone Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_UpdateGroupBasedOnRank</fullName>
        <description>Update Group Based on Rank</description>
        <field>JJ_JPN_Group__c</field>
        <literalValue>Partner</literalValue>
        <name>Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_UpdateGroupBasedOnRankDeal</fullName>
        <description>Update Group Based On Rank - Dealer</description>
        <field>JJ_JPN_Group__c</field>
        <literalValue>Dealer</literalValue>
        <name>Update Group Based On Rank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_UpdateGroupBasedOnRankFD</fullName>
        <description>Update Group Based On Rank Partner</description>
        <field>JJ_JPN_Group__c</field>
        <literalValue>Partner</literalValue>
        <name>Update Group Based On Rank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_UpdateGroupBasedOnRankOther</fullName>
        <field>JJ_JPN_Group__c</field>
        <literalValue>Other</literalValue>
        <name>Other</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_UpdateGroupBasedOnRankReg</fullName>
        <field>JJ_JPN_Group__c</field>
        <literalValue>Regular</literalValue>
        <name>Regular</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MyAcuvue_Activation_Date</fullName>
        <field>MA2_MyAcuvueActivationDate__c</field>
        <formula>TODAY()</formula>
        <name>MyAcuvue Activation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MyAcuvue_Deactivation_Date</fullName>
        <field>MA2_MyAcuvueDeactivationDate__c</field>
        <formula>TODAY()</formula>
        <name>MyAcuvue Deactivation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NullShippingStreet</fullName>
        <field>ShippingStreet</field>
        <name>NullShippingStreet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Old_value_of_Sales_Rep_Name_En</fullName>
        <field>JJ_JPN_PreviousPersonInchargeName__c</field>
        <formula>(PRIORVALUE(custGroup__c))</formula>
        <name>Old value of Sales Rep Name En</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Shipping_Street</fullName>
        <field>ShippingStreet</field>
        <formula>Address1__c +&apos; &apos;+ Address2__c +&apos; &apos;+ Address3__c</formula>
        <name>Shipping Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Street_Address</fullName>
        <field>BillingStreet</field>
        <formula>Address1__c +&apos; &apos;+ Address2__c +&apos; &apos;+ Address3__c</formula>
        <name>Street Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateIntroductionPriority_88</fullName>
        <field>IntroductionPriority__c</field>
        <formula>&quot;88&quot;</formula>
        <name>UpdateIntroductionPriority_88</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateIntroductionPriority_99</fullName>
        <field>IntroductionPriority__c</field>
        <formula>&quot;99&quot;</formula>
        <name>UpdateIntroductionPriority_99</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateShippingCountry</fullName>
        <field>ShippingCountry</field>
        <name>UpdateShippingCountry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Group_Type</fullName>
        <field>JJ_JPN_Group_Type__c</field>
        <formula>TEXT(JJ_JPN_Group__c)</formula>
        <name>Update Group Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Block</fullName>
        <description>Update Order Block to X if Status is Changed to H</description>
        <field>JJ_JPN_OrderBlock__c</field>
        <literalValue>X</literalValue>
        <name>Update Order Block</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account Mapping</fullName>
        <actions>
            <name>Account_Map</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Aws_AccountId__c</field>
            <operation>notEqual</operation>
            <value>Null</value>
        </criteriaItems>
        <description>Account Mapping for MyAcuvue Analytics</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Shipping Street</fullName>
        <actions>
            <name>Shipping_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>Account.Address1__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Address2__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Address3__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CountryCode__c</field>
            <operation>notEqual</operation>
            <value>JPN</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN Phone Update</fullName>
        <actions>
            <name>JJ_JPN_Phone_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>JJ_JPN Phone Update</description>
        <formula>CountryCode__c = &apos;JPN&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_Account Shipping Street</fullName>
        <actions>
            <name>NullShippingStreet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.CountryCode__c</field>
            <operation>equals</operation>
            <value>JPN</value>
        </criteriaItems>
        <description>Shipping Address will only have postal for JPN records</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_Change Group Type to value of Group</fullName>
        <actions>
            <name>Update_Group_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.JJ_JPN_Group__c</field>
            <operation>equals</operation>
            <value>Partner,Regular,Other,Dealer</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_CountryNameblank</fullName>
        <actions>
            <name>UpdateShippingCountry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ShippingCountry</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CountryCode__c</field>
            <operation>equals</operation>
            <value>JPN</value>
        </criteriaItems>
        <description>It will update Country Name as blank for JPN records</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_OldValueofPersonInchargeName</fullName>
        <actions>
            <name>Old_value_of_Sales_Rep_Name_En</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checks if Sales Rep name is new and stores its old value in &apos;Previous Person Incharge Name&apos;</description>
        <formula>AND(     ISCHANGED(custGroup__c ),     NOT(ISBLANK((custGroup__c ))),      (CountryCode__c ==&apos;JPN&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_Update Order Block to X if Status H</fullName>
        <actions>
            <name>Update_Order_Block</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Status__c</field>
            <operation>equals</operation>
            <value>H</value>
        </criteriaItems>
        <description>Update Order Block based on Status value H</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_UpdateGroupBasedOnRankDealer</fullName>
        <actions>
            <name>JJ_JPN_UpdateGroupBasedOnRankDeal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1  AND (3 OR 2)</booleanFilter>
        <criteriaItems>
            <field>Account.JJ_JPN_Rank__c</field>
            <operation>equals</operation>
            <value>A3,B3,C3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JJ_JPN_SubTradeChannel__c</field>
            <operation>contains</operation>
            <value>STCD21</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JJ_JPN_SubTradeChannel__c</field>
            <operation>contains</operation>
            <value>STCD22</value>
        </criteriaItems>
        <description>Update Group Based on Rank- Dealer</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_UpdateGroupBasedOnRankOther</fullName>
        <actions>
            <name>JJ_JPN_UpdateGroupBasedOnRankOther</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 and (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Account.JJ_JPN_Rank__c</field>
            <operation>equals</operation>
            <value>A3,B3,C3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JJ_JPN_SubTradeChannel__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JJ_JPN_SubTradeChannel__c</field>
            <operation>notContain</operation>
            <value>STCD21,STCD22</value>
        </criteriaItems>
        <description>Update Group Based on Rank</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_UpdateGroupBasedOnRankPartner</fullName>
        <actions>
            <name>JJ_JPN_UpdateGroupBasedOnRankFD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Account.JJ_JPN_Rank__c</field>
            <operation>equals</operation>
            <value>A1,B1,C1</value>
        </criteriaItems>
        <description>Update Group Based on Rank Partner</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_UpdateGroupBasedOnRankReg</fullName>
        <actions>
            <name>JJ_JPN_UpdateGroupBasedOnRankReg</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Account.JJ_JPN_Rank__c</field>
            <operation>equals</operation>
            <value>A2,B2,C2</value>
        </criteriaItems>
        <description>Update Group Based on Rank</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_UpdateIntroductionPriorityto_%2788%27</fullName>
        <actions>
            <name>UpdateIntroductionPriority_88</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Account.JJ_JPN_FacilityInformation__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.JJ_JPN_FacilityInformation__c</field>
            <operation>equals</operation>
            <value>3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.IntroductionPriority__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>This field is updated to 88 if Facility Information is either 1 or 3 and Introduction priority is blank.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_UpdateIntroductionPriorityto_%2799%27</fullName>
        <actions>
            <name>UpdateIntroductionPriority_99</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.JJ_JPN_FacilityInformation__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.IntroductionPriority__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>This field is updated to 99 if Facility Information is 0 and Introduction Priority is Blank</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MyAcuvue Activation Date</fullName>
        <actions>
            <name>MyAcuvue_Activation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNEW()),PRIORVALUE(My_Acuvue__c)=&apos;&apos;,ISPICKVAL(My_Acuvue__c,&apos;Yes&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MyAcuvue Deactivation Date</fullName>
        <actions>
            <name>MyAcuvue_Deactivation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL( My_Acuvue__c ,&apos;No&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
