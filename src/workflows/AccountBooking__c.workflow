<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_Mapping</fullName>
        <description>Account Mapping</description>
        <field>MA2_AccountId__c</field>
        <formula>Account__r.OutletNumber__c</formula>
        <name>Account Mapping</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Appointment_Mapping</fullName>
        <description>Appointment Mapping for MyAcuvue Analytics</description>
        <field>MA2_BookingId__c</field>
        <formula>AWS_ID__c</formula>
        <name>Appointment Mapping</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Mapping</fullName>
        <description>Contact Mapping</description>
        <field>MA2_ConatctId__c</field>
        <formula>Contact__r.MembershipNo__c</formula>
        <name>Contact Mapping</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Appointment Mapping</fullName>
        <actions>
            <name>Account_Mapping</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Appointment_Mapping</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Contact_Mapping</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>AccountBooking__c.Aws_ContactId__c</field>
            <operation>notEqual</operation>
            <value>Null</value>
        </criteriaItems>
        <criteriaItems>
            <field>AccountBooking__c.MA2_CountryCode__c</field>
            <operation>equals</operation>
            <value>KOR</value>
        </criteriaItems>
        <description>Appointment Mapping-Used for MyAcuvue Analytics</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
