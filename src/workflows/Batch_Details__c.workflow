<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>LOTUpdate</fullName>
        <field>LOT_Number__c</field>
        <formula>RIGHT(LEFT(LOT_Number__c,LEN(LOT_Number__c)-1),LEN(LOT_Number__c)-2)</formula>
        <name>LOTUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>LOTUpdate</fullName>
        <actions>
            <name>LOTUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Batch_Details__c.LOT_Number__c</field>
            <operation>contains</operation>
            <value>&apos;</value>
        </criteriaItems>
        <description>Admin Workflow</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
