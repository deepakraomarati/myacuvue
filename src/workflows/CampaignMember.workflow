<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Updating_Shield_Voucher_Code</fullName>
        <field>Shield_User_eVoucher_Code__c</field>
        <formula>IF(ISBLANK(X2ndTHVoucherCode__c),LEFT( Outlet_Number__c , 3) &amp; LEFT(Email , 2) &amp; LEFT(Contact.NRIC__c, 2) &amp; RIGHT(Id,3),X2ndTHVoucherCode__c)</formula>
        <name>Updating Shield Voucher Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CM_Update User EVoucher Code</fullName>
        <actions>
            <name>Updating_Shield_Voucher_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update User EVoucher Code</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
