<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Notification_Mail_on_Coaching_Creation</fullName>
        <description>Send Notification Mail on Coaching Creation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Coaching_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Notification_Mail_to_Coachee_on_Coaching_Creation</fullName>
        <description>Send Notification Mail to Coachee on Coaching Creation</description>
        <protected>false</protected>
        <recipients>
            <field>Coachee__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Coaching_Notification_for_Coachee</template>
    </alerts>
    <rules>
        <fullName>Notification Mail to Coach and Coachee</fullName>
        <actions>
            <name>Send_Notification_Mail_on_Coaching_Creation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_Notification_Mail_to_Coachee_on_Coaching_Creation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Skywalker - Sends a notification mail to the coach and coachee whenever Coaching record is created</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
