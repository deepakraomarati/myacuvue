<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Apigee_Account_Id</fullName>
        <field>MA2_AccountId__c</field>
        <formula>AccountId__r.OutletNumber__c</formula>
        <name>Apigee Account Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apigee_ContactId</fullName>
        <field>MA2_ContactId__c</field>
        <formula>ContactID__r.MembershipNo__c</formula>
        <name>Apigee ContactId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Consumer Relation ID Update</fullName>
        <actions>
            <name>Apigee_Account_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Apigee_ContactId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>API User Profile</value>
        </criteriaItems>
        <description>Used to populate Apigee Account and Consumer Id</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
