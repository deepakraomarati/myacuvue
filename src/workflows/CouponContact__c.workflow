<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Apigee_Account_ID</fullName>
        <field>MA2_AccountId__c</field>
        <formula>TransactionId__r.AccountID__r.OutletNumber__c</formula>
        <name>Apigee Account ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apigee_ContactId</fullName>
        <field>MA2_ContactId__c</field>
        <formula>TransactionId__r.MA2_Contact__r.MembershipNo__c</formula>
        <name>Apigee ContactId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apigee_Coupon_ID</fullName>
        <field>MA2_CouponId__c</field>
        <formula>CASESAFEID(CouponId__r.Id)</formula>
        <name>Apigee Coupon ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apigee_Transaction_ID</fullName>
        <field>MA2_TransactionId__c</field>
        <formula>CASESAFEID( TransactionId__r.Id)</formula>
        <name>Apigee Transaction ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Apigee Account Id</fullName>
        <actions>
            <name>Apigee_Account_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Apigee_ContactId</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Apigee_Coupon_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Apigee_Transaction_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>MyAcuvue : Populate the Account Name based on Apigee Transaction ID in Coupon Wallet</description>
        <formula>NOT(ISBLANK(TransactionId__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
