<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Default_Confirm_Date</fullName>
        <field>Confirm_Date__c</field>
        <formula>Event_Date__c -21</formula>
        <name>Default Confirm Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Default Confirm Date</fullName>
        <actions>
            <name>Default_Confirm_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName =&apos;MedEd&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
