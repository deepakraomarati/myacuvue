<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CA_Customer_Assement_Expired</fullName>
        <description>CA: Customer Assement Expired</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_Alert_After_the_Assessment_Expires</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_on_Approval</fullName>
        <description>Email Alert on Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CA_Email_Alert_After_Approval</template>
    </alerts>
    <fieldUpdates>
        <fullName>Share_Space_Field_update</fullName>
        <field>Share_Space__c</field>
        <formula>(Customer_Segmentation_Master__r.QWT_Acuvue_Fits_Per_week__c *  Acuvue_Fits__c )+(Customer_Segmentation_Master__r.QWT_Acuvue_Market_Share__c *  Market_Share__c )+(Customer_Segmentation_Master__r.QWT_Acuvue_POSM_Materials__c *  Acuvue_POSM_Materials__c)+ (Customer_Segmentation_Master__r.QWT_shell_space__c *  shell_space__c )+(Customer_Segmentation_Master__r.QWT_Space_Dedicated_Acuvue_POSM__c*Share_Space_Dedicated_Acuvue_POSM__c)</formula>
        <name>Share Space Field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Field_Update</fullName>
        <description>After Completion of two years the status field is changed</description>
        <field>Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Status Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Threshold_CL_Potential</fullName>
        <field>Threshold_Score_Contact_Lens_Potential__c</field>
        <formula>(Customer_Segmentation_Master__r.QWT_CL_fitting_are_done_per_month__c  * WT_CL_fitting_are_done_per_month__c ) + 
(Customer_Segmentation_Master__r.QWT_what_would_be_new_fits__c * WT_what_would_be_new_fits__c ) + 
(Customer_Segmentation_Master__r.QWT_CL_revenew_as_a_of_total__c * WT_CL_revenew_as_a_of_total__c ) + 
(Customer_Segmentation_Master__r.QWT_people_proactively_talking_about_CL__c * WT_people_proactively_talking_about_CL__c ) + 
(Customer_Segmentation_Master__r.QWT_Dedicated_Space_for_CL_Fitting__c * WT_Dedicated_Space_for_CL_Fitting__c ) + 
(Customer_Segmentation_Master__r.QWT_Protocol_with_eye_exam_prior_to_CL__c * WT_Protocol_with_eye_exam_prior_to_CL__c ) + 
(Customer_Segmentation_Master__r.QWT_Regd_Optometrists_dedicated_for_CL__c * WT_Regd_Optometrists_dedicated_for_CL__c ) + 
(Customer_Segmentation_Master__r.QWT_of_POSM_in_store_dedicated_to_CL__c * WT_of_POSM_in_store_dedicated_to_CL__c ) + 
(Customer_Segmentation_Master__r.QWT_Proactively_suggest_to_try_CL__c * WT_Proactively_suggest_to_try_CL__c ) + 
(Customer_Segmentation_Master__r.QWT_Upgrade_Hydrogel_Wearer__c * WT_Upgrade_Hydrogel_Wearer__c ) + 
(Customer_Segmentation_Master__r.QWT_of_total_fits_Multifocal__c * WT_of_total_fits_Multifocal__c ) + 
(Customer_Segmentation_Master__r.QWT_of_total_fits_Beauty__c * WT_of_total_fits_Beauty__c ) + 
(Customer_Segmentation_Master__r.QWT_of_total_fits_Daily__c * WT_of_total_fits_Daily__c ) + 
(Customer_Segmentation_Master__r.QWT_of_total_fits_Reusable__c * WT_of_total_fits_Reusable__c ) + 
(Customer_Segmentation_Master__r.QWT_of_total_fits_Toric__c * WT_of_total_fits_Toric__c )</formula>
        <name>Threshold CL Potential</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Assessment Expiry</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Customer_Segmentation_Matrix__c.Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>CA_Customer_Assement_Expired</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Status_Field_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Customer_Segmentation_Matrix__c.CreatedDate</offsetFromField>
            <timeLength>730</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Share Space Update</fullName>
        <actions>
            <name>Share_Space_Field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Customer_Segmentation_Matrix__c.Name</field>
            <operation>notEqual</operation>
            <value>100</value>
        </criteriaItems>
        <description>Share space trigger replacements</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Threshold CLP</fullName>
        <actions>
            <name>Threshold_CL_Potential</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Customer_Segmentation_Matrix__c.Name</field>
            <operation>notEqual</operation>
            <value>100</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
