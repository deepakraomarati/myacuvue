<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ETrial_SFMC_Phone</fullName>
        <field>MA2_SFMC_eTrial_Phone__c</field>
        <formula>case(Country__c,&quot;TH&quot;,&quot;66&quot;,&quot;SG&quot;,&quot;65&quot;,&quot;IN&quot;,&quot;91&quot;,&quot;ID&quot;,&quot;62&quot;,&quot;&quot;) 
+ if(left(etrial_phone__c,1)&lt;&gt;&quot;0&quot;,etrial_phone__c, right(etrial_phone__c, len(etrial_phone__c)-1))</formula>
        <name>ETrial SFMC Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JB SFMC ETrial Phone Number</fullName>
        <actions>
            <name>ETrial_SFMC_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Etrial__c.etrial_phone__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
