<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Updated_Completed_Check</fullName>
        <field>CompletedCheck__c</field>
        <literalValue>1</literalValue>
        <name>Updated Completed Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Completed Check</fullName>
        <actions>
            <name>Updated_Completed_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event.ActivityStatus__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Event.Country__c</field>
            <operation>notEqual</operation>
            <value>Japan</value>
        </criteriaItems>
        <description>Skywalker: Updates the field Completed check when a call is completed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
