<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Inform_atendee_when_cancelled</fullName>
        <description>Inform atendee when cancelled</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Custom_Events/Attendee_Cancelled</template>
    </alerts>
    <rules>
        <fullName>Inform atendee when cancelled</fullName>
        <actions>
            <name>Inform_atendee_when_cancelled</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Custom_Event__r.RecordType.DeveloperName=&apos;MedEd&apos; &amp;&amp; ( ISPICKVAL(Invitee_Status__c,&apos;Cancelled&apos;) || ISPICKVAL(Invitee_Status__c,&apos;Wait List&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
