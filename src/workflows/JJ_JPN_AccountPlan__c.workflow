<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JJ_JPN_AccountPlanFieldUpdate</fullName>
        <field>JJ_JPN_AccountPlanID__c</field>
        <formula>(JJ_JPN_PrimaryAccountPlan__r.Id)+(JJ_JPN_PrimaryAccountPlan__r.JJ_JPN_Year__c)+text(JJ_JPN_AccountPlanName__c)</formula>
        <name>Account Plan Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>JJ_JPN_UpdateAccountPlanID</fullName>
        <actions>
            <name>JJ_JPN_AccountPlanFieldUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update the field with Account Plan id to avoid duplicate.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
