<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Approach_Act</fullName>
        <field>JJ_JPN_ApproachAct__c</field>
        <formula>today()</formula>
        <name>Approach Act</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apr_Update</fullName>
        <field>JJ_JPN_Apr__c</field>
        <formula>0</formula>
        <name>Apr Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Aug_Update</fullName>
        <field>JJ_JPN_Aug__c</field>
        <formula>0</formula>
        <name>Aug Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BehavePlan</fullName>
        <field>JJ_JPN_BehavePlan__c</field>
        <formula>today()</formula>
        <name>Behave Plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Behave_Act</fullName>
        <field>JJ_JPN_BehaveAct__c</field>
        <formula>today()</formula>
        <name>Behave Act</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>DecUpdate</fullName>
        <field>JJ_JPN_Dec__c</field>
        <formula>0</formula>
        <name>DecUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dec_Update</fullName>
        <field>JJ_JPN_Dec__c</field>
        <formula>0</formula>
        <name>Dec Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Feb_Update</fullName>
        <field>JJ_JPN_Feb__c</field>
        <formula>0</formula>
        <name>Feb Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_AgreementAct</fullName>
        <field>JJ_JPN_AgreementAct__c</field>
        <formula>today()</formula>
        <name>Field update Agreement Act</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_AgreementActupdate</fullName>
        <field>JJ_JPN_AgreementAct__c</field>
        <formula>today()</formula>
        <name>Agreement Act update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Jan_update</fullName>
        <field>JJ_JPN_Jan__c</field>
        <formula>0</formula>
        <name>Jan update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Jul_Update</fullName>
        <field>JJ_JPN_Jul__c</field>
        <formula>0</formula>
        <name>Jul Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Jun_Update</fullName>
        <field>JJ_JPN_Jun__c</field>
        <formula>0</formula>
        <name>Jun Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mar_Update</fullName>
        <field>JJ_JPN_Mar__c</field>
        <formula>0</formula>
        <name>Mar Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>May_Update</fullName>
        <field>JJ_JPN_May__c</field>
        <formula>0</formula>
        <name>May Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Nego_Act</fullName>
        <field>JJ_JPN_NegoAct__c</field>
        <formula>today()</formula>
        <name>Nego Act</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NovUpdate</fullName>
        <field>JJ_JPN_Nov__c</field>
        <formula>0</formula>
        <name>NovUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Nov_Update</fullName>
        <field>JJ_JPN_Nov__c</field>
        <formula>0</formula>
        <name>Nov Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Oct_Update</fullName>
        <field>JJ_JPN_Oct__c</field>
        <formula>0</formula>
        <name>Oct Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sep_Update</fullName>
        <field>JJ_JPN_Sep__c</field>
        <formula>0</formula>
        <name>Sep Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trial_Act</fullName>
        <field>JJ_JPN_TrialAct__c</field>
        <formula>today()</formula>
        <name>Trial Act</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_To_Unique_Field</fullName>
        <field>JJ_JPN_UniqueActionPlanID__c</field>
        <formula>Name+ JJ_JPN_PrimaryAreaPlan__r.Name</formula>
        <name>Update To Unique Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>JJ_JPN_ActionPlan_Canceled2</fullName>
        <actions>
            <name>DecUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NovUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>JJ_JPN_ActionPlan__c.JJ_JPN_ActionPlanPhase__c</field>
            <operation>equals</operation>
            <value>Canceled</value>
        </criteriaItems>
        <description>Canceled picklist update for ActionPlan</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_ActionPlan_Cancelled</fullName>
        <actions>
            <name>Apr_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Aug_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Feb_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Jan_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Jul_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Jun_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Mar_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>May_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Oct_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sep_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>JJ_JPN_ActionPlan__c.JJ_JPN_ActionPlanPhase__c</field>
            <operation>equals</operation>
            <value>Canceled</value>
        </criteriaItems>
        <description>Canceled picklist update for ActionPlan</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_Agreement Act</fullName>
        <actions>
            <name>JJ_JPN_AgreementAct</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>JJ_JPN_ActionPlan__c.JJ_JPN_ActionPlanPhase__c</field>
            <operation>equals</operation>
            <value>Agreement</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_ApproachAct</fullName>
        <actions>
            <name>Approach_Act</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>JJ_JPN_ActionPlan__c.JJ_JPN_ActionPlanPhase__c</field>
            <operation>equals</operation>
            <value>Approach</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_Behave Act</fullName>
        <actions>
            <name>Behave_Act</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>JJ_JPN_ActionPlan__c.JJ_JPN_ActionPlanPhase__c</field>
            <operation>equals</operation>
            <value>Behave</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_Nego Act</fullName>
        <actions>
            <name>Nego_Act</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>JJ_JPN_ActionPlan__c.JJ_JPN_ActionPlanPhase__c</field>
            <operation>equals</operation>
            <value>Nego</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_Trial Act</fullName>
        <actions>
            <name>Trial_Act</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>JJ_JPN_ActionPlan__c.JJ_JPN_ActionPlanPhase__c</field>
            <operation>equals</operation>
            <value>Trial</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JJ_JPN_UpdateToActionPlanUnique</fullName>
        <actions>
            <name>Update_To_Unique_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Restrict User to create duplicate Action plan on Primary Area Plan</description>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
