<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Distinct_Area_Plan_ID</fullName>
        <description>External ID comprises of owned and Area Plan Name</description>
        <field>JJ_JPN_DistinctArea__c</field>
        <formula>CASESAFEID(JJ_JPN_PrimaryAreaPlan__r.OwnerId)&amp;text((JJ_JPN_AreaPlanName__c))&amp;(JJ_JPN_PrimaryAreaPlan__r.JJ_JPN_Year__c)</formula>
        <name>Distinct Area Plan ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>JJ_JPN_DistinctAreaPlanID</fullName>
        <actions>
            <name>Distinct_Area_Plan_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update the field which store unique id for Area Plan</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
