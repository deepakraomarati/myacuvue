<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_email_on_rejection</fullName>
        <description>Send email on rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>emailtemplates/JJ_JPN_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Send_email_on_request_approval</fullName>
        <description>Send email on request approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>emailtemplates/JJ_JPN_Approval</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved_Status</fullName>
        <field>JJ_JPN_ApprovalStatus__c</field>
        <literalValue>Approved</literalValue>
        <name>Approved Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FinanceManager</fullName>
        <field>JJ_JPN_FinanceManager__c</field>
        <literalValue>0</literalValue>
        <name>Finance Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FinanceMgrApproved</fullName>
        <field>JJ_JPN_FinanceManager__c</field>
        <literalValue>1</literalValue>
        <name>FinanceMgrApproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Finance_Manager_Rejected</fullName>
        <field>JJ_JPN_FinanceManager__c</field>
        <literalValue>0</literalValue>
        <name>Finance Manager Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Finance_Manager_Rejection</fullName>
        <field>JJ_JPN_FinanceManager__c</field>
        <literalValue>0</literalValue>
        <name>Finance Manager Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_AllReadyRegisteredPayerAccount</fullName>
        <field>JJ_JPN_FinanceManager__c</field>
        <literalValue>1</literalValue>
        <name>All ready Registered Payer Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_SalesManagerChecked</fullName>
        <description>Sales Rep&apos;s manager Sales Manager Checked</description>
        <field>JJ_JPN_SAMManagerCB__c</field>
        <literalValue>1</literalValue>
        <name>Sales Manager Checked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_SnrSalesManager</fullName>
        <field>JJ_JPN_SrSalesManager__c</field>
        <literalValue>1</literalValue>
        <name>Snr Sales Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_StatusUpdate</fullName>
        <field>JJ_JPN_ApprovalStatus__c</field>
        <literalValue>Pending for Approval</literalValue>
        <name>Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_StatusUpdate1</fullName>
        <field>JJ_JPN_ApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_StatusUpdate2</fullName>
        <field>JJ_JPN_ApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_StatusUpdate4</fullName>
        <field>JJ_JPN_ApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_StatusUpdate5</fullName>
        <field>JJ_JPN_ApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_StatusUpdate6</fullName>
        <field>JJ_JPN_ApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_StatusUpdate7</fullName>
        <field>JJ_JPN_ApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_StatusUpdate8</fullName>
        <field>JJ_JPN_ApprovalStatus__c</field>
        <literalValue>Recalled</literalValue>
        <name>Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_StatusUpdateApprovedEditDelAcc</fullName>
        <description>Status Update Approved Edit Del Acc</description>
        <field>JJ_JPN_ApprovalStatus__c</field>
        <literalValue>Approved</literalValue>
        <name>Status Update Approved Edit Del Acc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_StatusUpdateEditDelAcc</fullName>
        <description>Status Update Edit Del Acc</description>
        <field>JJ_JPN_ApprovalStatus__c</field>
        <literalValue>Pending for Approval</literalValue>
        <name>Status Update Edit Del Acc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_StatusUpdateRejectEditDelAcc</fullName>
        <description>Status Update Reject Edit Del Acc</description>
        <field>JJ_JPN_ApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status Update Reject Edit Del Acc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>J_JPN_StatusUpdate3</fullName>
        <field>JJ_JPN_ApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RGS_Approved</fullName>
        <field>JJ_JPN_RGS_Approved__c</field>
        <literalValue>1</literalValue>
        <name>RGS Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RGS_Rejected</fullName>
        <field>JJ_JPN_RGS_Approved__c</field>
        <literalValue>0</literalValue>
        <name>RGS Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RGS_Rejection</fullName>
        <field>JJ_JPN_RGS_Approved__c</field>
        <literalValue>0</literalValue>
        <name>RGS Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Update</fullName>
        <field>JJ_JPN_SalesAdminUser__c</field>
        <literalValue>0</literalValue>
        <name>Recall Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Update_1</fullName>
        <field>JJ_JPN_SalesAdminUser__c</field>
        <literalValue>0</literalValue>
        <name>Recall Update 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recall_Updation</fullName>
        <field>JJ_JPN_SalesAdminUser__c</field>
        <literalValue>1</literalValue>
        <name>Recall Updation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SAMManagerApproved</fullName>
        <field>JJ_JPN_SAMManagerCB__c</field>
        <literalValue>1</literalValue>
        <name>SAM Manager Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SAMManagerRejected</fullName>
        <field>JJ_JPN_SAMManagerCB__c</field>
        <literalValue>0</literalValue>
        <name>SAM Manager Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SAM_Manager_Rejection</fullName>
        <field>JJ_JPN_SAMManagerCB__c</field>
        <literalValue>0</literalValue>
        <name>SAM Manager Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SAM_Manager_Unchecking</fullName>
        <field>JJ_JPN_SAMManagerCB__c</field>
        <literalValue>0</literalValue>
        <name>SAM Manager Unchecking</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SalesAdminManagerApproved</fullName>
        <field>JJ_JPN_SalesAdminManager__c</field>
        <literalValue>1</literalValue>
        <name>Sales Admin Manager Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SalesAdminUserApproved</fullName>
        <field>JJ_JPN_SalesAdminUser__c</field>
        <literalValue>1</literalValue>
        <name>Sales Admin User Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Admin_Manager</fullName>
        <field>JJ_JPN_SalesAdminManager__c</field>
        <literalValue>0</literalValue>
        <name>Sales Admin Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Admin_Manager_Rejected</fullName>
        <field>JJ_JPN_SalesAdminManager__c</field>
        <literalValue>0</literalValue>
        <name>Sales Admin Manager Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Admin_Manager_rejection</fullName>
        <field>JJ_JPN_SalesAdminUser__c</field>
        <literalValue>0</literalValue>
        <name>Sales Admin Manager rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Admin_User_Recall</fullName>
        <field>JJ_JPN_SalesAdminUser__c</field>
        <literalValue>0</literalValue>
        <name>Sales Admin User Recall</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Admin_User_Rejected</fullName>
        <field>JJ_JPN_SalesAdminUser__c</field>
        <literalValue>0</literalValue>
        <name>Sales Admin User Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Admin_User_Rejection</fullName>
        <field>JJ_JPN_SalesAdminUser__c</field>
        <literalValue>0</literalValue>
        <name>Sales Admin User Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Manager_Rejected</fullName>
        <field>JJ_JPN_SalesManager__c</field>
        <literalValue>0</literalValue>
        <name>Sales Manager Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Manager_Rejection</fullName>
        <field>JJ_JPN_SalesManager__c</field>
        <literalValue>0</literalValue>
        <name>Sales Manager Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Manager_reject</fullName>
        <field>JJ_JPN_SalesManager__c</field>
        <literalValue>0</literalValue>
        <name>Sales Manager reject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Rep</fullName>
        <field>JJ_JPN_SalesRep__c</field>
        <literalValue>1</literalValue>
        <name>Sales Rep</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SnrSAMManager</fullName>
        <field>JJ_JPN_SrSAMManager__c</field>
        <literalValue>1</literalValue>
        <name>Snr SAM Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Snr_SAM_Manager_Rejected</fullName>
        <field>JJ_JPN_SrSAMManager__c</field>
        <literalValue>0</literalValue>
        <name>Snr SAM Manager Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Snr_Sales_Manger_Rejected</fullName>
        <field>JJ_JPN_SrSalesManager__c</field>
        <literalValue>0</literalValue>
        <name>Snr Sales Manger Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sr_SAM_Manager_Rejection</fullName>
        <field>JJ_JPN_SrSAMManager__c</field>
        <literalValue>0</literalValue>
        <name>Sr SAM Manager Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sr_Sales_Manager</fullName>
        <field>JJ_JPN_SrSalesManager__c</field>
        <literalValue>0</literalValue>
        <name>Sr Sales Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_Recall</fullName>
        <field>JJ_JPN_ApprovalStatus__c</field>
        <literalValue>Recalled</literalValue>
        <name>Status Update Recall</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unchecking</fullName>
        <field>JJ_JPN_SalesRep__c</field>
        <literalValue>0</literalValue>
        <name>Unchecking</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>salesManager</fullName>
        <field>JJ_JPN_SalesManager__c</field>
        <literalValue>1</literalValue>
        <name>sales Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JJ_JPN_IsPayerAccountResgistered</fullName>
        <actions>
            <name>JJ_JPN_AllReadyRegisteredPayerAccount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checking Payer Account is already registered or not. if Yes then approval will not go to Finance Manager.</description>
        <formula>if( JJ_JPN_InterfaceCheck__c==5 ||   JJ_JPN_InterfaceCheck__c==6 ||  JJ_JPN_InterfaceCheck__c==7,TRUE,FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>dummy</fullName>
        <actions>
            <name>Approved_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>JJ_JPN_CustomerMasterRequest__c.Name</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
