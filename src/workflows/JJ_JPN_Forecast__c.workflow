<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JJ_JPN_UpdateSAMUniqueID</fullName>
        <field>JJ_JPN_FSUniqueID__c</field>
        <formula>TEXT(JJ_JPN_ForecastMonth__c) + TEXT(JJ_JPN_ForecastYear__c) + JJ_JPN_SAMOutlet__c + OwnerId</formula>
        <name>Update to SAM Unique ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JJ_JPN_UpdatetoFSUniqueID</fullName>
        <field>JJ_JPN_FSUniqueID__c</field>
        <formula>TEXT(JJ_JPN_ForecastMonth__c) + TEXT(JJ_JPN_ForecastYear__c) + TEXT(JJ_JPN_Customer__c) +  OwnerId</formula>
        <name>Update to FS Unique ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Forecast FS Type update</fullName>
        <actions>
            <name>JJ_JPN_UpdatetoFSUniqueID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName = &apos;JJ_JPN_FSForecast&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Forecast SAM Type update</fullName>
        <actions>
            <name>JJ_JPN_UpdateSAMUniqueID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName = &apos;SAM_Forecast&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
