<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JJ_JPN_UniqueId</fullName>
        <description>JJ_JPN_UniqueRecords__c: Field update with Unique Id</description>
        <field>JJ_JPN_UniqueRecords__c</field>
        <formula>JJ_JPN_Doctors__c + JJ_JPN_Hospitals__c</formula>
        <name>Unique Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>JJ_JPN_UniqueHospitalContact</fullName>
        <actions>
            <name>JJ_JPN_UniqueId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>field update to Unique field with unique id</description>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
