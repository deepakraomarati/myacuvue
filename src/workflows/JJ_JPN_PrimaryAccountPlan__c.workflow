<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JJ_JPN_UniqueIDforPrimaryAccountPlan</fullName>
        <field>JJ_JPN_UniqueIDforPrimaryAccountPlan__c</field>
        <formula>OwnerId+JJ_JPN_Outlet__r.Id+JJ_JPN_Year__c</formula>
        <name>Unique ID for Primary Account Plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>JJ_JPN_FieldUpdateToUniquePrimaryAccountPlan</fullName>
        <actions>
            <name>JJ_JPN_UniqueIDforPrimaryAccountPlan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>field update to Unique ID for Primary Account Plan</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
