<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>JJ_JPN_PrimaryAreaPlanUniqueID</fullName>
        <description>Update to field with Unique Id</description>
        <field>JJ_JPN_PrimaryAreaPlanUniqueId__c</field>
        <formula>(CASESAFEID(OwnerId)+JJ_JPN_Year__c)</formula>
        <name>Primary Area Plan UniqueID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>JJ_JPN_Update to Unique ID for Primary Area Plan</fullName>
        <actions>
            <name>JJ_JPN_PrimaryAreaPlanUniqueID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update to field (Unique ID for Primary Area Plan)</description>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
