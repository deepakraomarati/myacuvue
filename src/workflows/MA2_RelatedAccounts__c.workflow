<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Apigee_Account_Id</fullName>
        <field>MA2_AccountId__c</field>
        <formula>MA2_Account__r.OutletNumber__c</formula>
        <name>Apigee Account Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apigee_Contact_ID</fullName>
        <field>MA2_ContactId__c</field>
        <formula>MA2_Contact__r.MembershipNo__c</formula>
        <name>Apigee Contact ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apigee_User_Profile</fullName>
        <field>MA2_ProfileId__c</field>
        <formula>CASESAFEID(MA2_UserProfile__r.Id)</formula>
        <name>Apigee User Profile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Apigee Related Account Field Updates</fullName>
        <actions>
            <name>Apigee_Account_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Apigee_Contact_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Apigee_User_Profile</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MA2_RelatedAccounts__c.MA2_AccountId__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>MyAcuvue External ID updates</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
