<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Apigee_TransactionID</fullName>
        <field>MA2_TransactionId__c</field>
        <formula>MA2_Transaction__r.MA2_TransactionId__c</formula>
        <name>Apigee TransactionID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apigee_productID</fullName>
        <field>MA2_ProductId__c</field>
        <formula>MA2_ProductName__r.UPC_Code__c</formula>
        <name>Apigee productID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Transaction Product External ID</fullName>
        <actions>
            <name>Apigee_TransactionID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Apigee_productID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>API User Profile</value>
        </criteriaItems>
        <criteriaItems>
            <field>TransactionTd__c.MA2_TransactionType__c</field>
            <operation>equals</operation>
            <value>Products,Manual Adjustment</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
