<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_TO_ASM</fullName>
        <description>Email TO ASM</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Hot_Sheet_Notification</template>
    </alerts>
    <rules>
        <fullName>Send Email Notification On Chek of Hot Sheet Generated</fullName>
        <actions>
            <name>Email_TO_ASM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>NPS__c.Hot_Sheet_Generated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Send_NPS_Hot_Sheet_to_my_Manager__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Skywalker - Rule to send Email notification when Hot Sheet Generated is checked</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
