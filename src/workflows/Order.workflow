<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_FlagofSendtoSAP_Order</fullName>
        <field>Flag_of_Send_to_SAP__c</field>
        <literalValue>0</literalValue>
        <name>Update FlagofSendtoSAP Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JJ JPN Order Submit for FlagofSendtoSAP</fullName>
        <actions>
            <name>Update_FlagofSendtoSAP_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.Flag_of_Send_to_SAP__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <description>When new Order record is created, this Workflow Rule runs.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
