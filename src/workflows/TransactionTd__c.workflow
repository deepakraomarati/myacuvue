<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Apigee_Account_Id</fullName>
        <field>MA2_AccountId__c</field>
        <formula>AccountID__r.OutletNumber__c</formula>
        <name>Apigee Account Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apigee_Contact_Id</fullName>
        <field>MA2_ContactId__c</field>
        <formula>MA2_Contact__r.MembershipNo__c</formula>
        <name>Apigee Contact Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Apigee_ECP_ID</fullName>
        <field>MA2_ECPId__c</field>
        <formula>MA2_ECPContact__r.MembershipNo__c</formula>
        <name>Apigee ECP ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Transaction Details External Id</fullName>
        <actions>
            <name>Apigee_Account_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Apigee_Contact_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Apigee_ECP_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>API User Profile</value>
        </criteriaItems>
        <criteriaItems>
            <field>TransactionTd__c.MA2_TransactionType__c</field>
            <operation>contains</operation>
            <value>Products,Manual Adjustment</value>
        </criteriaItems>
        <description>MyAcuvue : To populate the External System Id for the manual Transaction created from SFDC</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
