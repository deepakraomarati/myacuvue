<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>JJ_JPN_PopulateRegion</fullName>
        <active>false</active>
        <criteriaItems>
            <field>User.Country</field>
            <operation>equals</operation>
            <value>Japan</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
